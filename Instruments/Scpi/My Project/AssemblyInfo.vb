﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("VISA SCPI Instruments")> 
<Assembly: AssemblyDescription("VISA SCPI Instruments")> 
<Assembly: AssemblyProduct("Visa.Library.Instruments.SCPI.2013")>
<Assembly: CLSCompliant(True)> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM

<Assembly: Guid("138faa4c-2d6a-4b54-85c6-023c7980eb04")> 
