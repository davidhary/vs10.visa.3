Namespace K500

    ''' <summary>
    ''' Holds a single set of 27xx instrument reading elements.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class Readings
        Inherits isr.Scpi.Readings(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="elements">Specifies the 
        ''' <see cref="isr.Scpi.ReadingElements">reading elements</see>
        ''' expected in the data strings.
        ''' </param>
        ''' <remarks>Reading elements are added in the order they are returned by
        ''' the instrument so as to automate parsing of these data.</remarks>
        Public Sub New(ByVal elements As isr.Scpi.ReadingElements)

            ' instantiate the base class
            MyBase.New()

            Me._elements = elements

            If (Me._elements And isr.Scpi.ReadingElements.Voltage) <> 0 Then
                Me._voltageReading = New isr.Scpi.MeasurandDouble()
                Me._voltageReading.DisplayCaption.ShowUnits = True
                Me._voltageReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Voltage
                Me._voltageReading.DisplayCaption.Units.ValueFormat = "0.0"
                Me._voltageReading.SaveCaption.Units = Me._voltageReading.DisplayCaption.Units
                Me._voltageReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._voltageReading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._voltageReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._voltageReading, 15)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Current) <> 0 Then
                Me._currentReading = New isr.Scpi.MeasurandDouble()
                Me._currentReading.DisplayCaption.ShowUnits = True
                Me._currentReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
                Me._currentReading.DisplayCaption.Units.ValueFormat = "0.00"
                Me._currentReading.DisplayCaption.Units.UnitsScale = isr.Scpi.UnitsScale.Milli
                Me._currentReading.SaveCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
                Me._currentReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._currentReading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._currentReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._currentReading, 15)
            End If

        End Sub

        ''' <summary>
        ''' Create a copy of the model.
        ''' </summary>
        Public Sub New(ByVal model As Readings)

            ' instantiate the base class
            MyBase.New()
            If model IsNot Nothing Then
                Me._elements = model._elements

                If (Me._elements And isr.Scpi.ReadingElements.Voltage) <> 0 Then
                    Me._voltageReading = New isr.Scpi.MeasurandDouble(model._voltageReading)
                    MyBase.AddReading(Me._voltageReading, 15)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Current) <> 0 Then
                    Me._currentReading = New isr.Scpi.MeasurandDouble(model._currentReading)
                    MyBase.AddReading(Me._currentReading, 15)
                End If
            End If

        End Sub

        ''' <summary>
        ''' Clones this class.
        ''' </summary>
        Public Function Clone() As Readings
            Return New Readings(Me)
        End Function

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If Me._currentReading IsNot Nothing Then
                            Me._currentReading = Nothing
                        End If

                        If Me._voltageReading IsNot Nothing Then
                            Me._voltageReading = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PARSE "

        ''' <summary>
        ''' Parse reading data into a readings array.
        ''' </summary>
        ''' <param name="baseReading">Specifies the base reading which includes the limits for all reading
        ''' elements.</param>
        ''' <param name="readingRecords">The reading records.</param>
        ''' <returns>Readings[][].</returns>
        ''' <exception cref="System.ArgumentNullException">readingRecords</exception>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Object is returned")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMulti(ByVal baseReading As Readings, ByVal readingRecords As String) As Readings()

            If readingRecords Is Nothing Then
                Throw New ArgumentNullException("readingRecords")
            ElseIf readingRecords.Length = 0 Then
                Dim r As Readings() = {}
                Return r
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If

            ' TO_DO: This assumes 5 elements per reading.
            '  Use Me._elements.Set to set the number of elements per reading.
            Dim readings As String() = readingRecords.Split(","c)
            If readings.Length < baseReading.ElementsCount Then
                Dim r As Readings() = {}
                Return r
            End If

            Dim readingsArray(readings.Length \ baseReading.ElementsCount - 1) As Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readings.Length - 1 Step baseReading.ElementsCount
                Dim reading As New Readings(baseReading)
                reading.Parse(readings, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

#End Region

#Region " PROPERTIES "

        Private _elements As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Gets the reading elements.
        ''' </summary>
        Public ReadOnly Property Elements() As isr.Scpi.ReadingElements
            Get
                Return Me._elements
            End Get
        End Property

        Private _currentReading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the current <see cref="isr.Scpi.MeasurandDouble">reading</see>.</summary>
        Public Property CurrentReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._currentReading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._currentReading = value
            End Set
        End Property

        Private _voltageReading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the Voltage <see cref="isr.Scpi.MeasurandDouble">reading</see>.</summary>
        Public Property VoltageReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._voltageReading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._voltageReading = value
            End Set
        End Property

#End Region

    End Class

End Namespace