Imports isr.Core.ControlExtensions
Namespace K500

    ''' <summary>Provides a user interface for the Keithley 27XX instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    <System.ComponentModel.DisplayName("K500 Panel"),
      System.ComponentModel.Description("KEPCO BHK-MG Instrument Family - Windows Forms Custom Control"),
      System.Drawing.ToolboxBitmap(GetType(K500.Panel))>
    Public Class Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New K500.Instrument(Me, "K500")

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                Me._messagesBox.PrependMessage(e.ExtendedMessage)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

        ''' <summary>Update the display based on the current instrument values.</summary>
        Private Sub refreshDisplay()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If

            Me._currentNumeric.Value = CDec(Me.Instrument.CurrentLevelCache.Value.Value * 1000)
            Me._currentLimitNumeric.Value = CDec(Me.Instrument.CurrentLimitCache.Value.Value * 1000)
            Me._currentProtectionNumeric.Value = CDec(Me.Instrument.CurrentProtectionCache.Value.Value * 1000)
            Me._voltageNumeric.Value = CDec(Me.Instrument.VoltageLevelCache.Value.Value)
            Me._voltageLimitNumeric.Value = CDec(Me.Instrument.VoltageLimitCache.Value.Value)
            Me._voltageProtectionNumeric.Value = CDec(Me.Instrument.VoltageProtectionCache.Value.Value)

        End Sub

#End Region

#Region " I Connectible "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me._tabControl.Enabled = value
                If value Then
                    refreshDisplay()
                End If
            End Set
        End Property

#End Region

#Region " PROPERTIES "

        Private WithEvents _instrument As K500.Instrument
        ''' <summary>Gets a reference to the KEPCO BHK-500 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As K500.Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As K500.Instrument)
                If value IsNot Nothing Then
                    Me._instrument = value
                    MyBase.ConnectableResource = value
                End If
            End Set
        End Property

#End Region

#Region " BULK SETTINGS "

        ''' <summary>
        ''' Applies the selected measurements settings.
        ''' </summary>
        Private Sub applyLimits()

            If Not Me.Instrument.IsConnected Then
                Return
            End If

            Me.Instrument.CurrentLimitSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._currentLimitNumeric.Value / 1000)
            Me.Instrument.CurrentProtectionSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._currentProtectionNumeric.Value / 1000)
            Me.Instrument.VoltageLimitSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._voltageLimitNumeric.Value)
            Me.Instrument.VoltageProtectionSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._voltageProtectionNumeric.Value)

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: INTERFACE "

        Private Sub interfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _interfaceClearButton.Click
            Me.Instrument.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary>
        ''' Issue RST.
        ''' </summary>
        Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _resetButton.Click

            Me.Instrument.ResetKnownState()

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub _applyLimitsButtonButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _applyLimitsButton.Click
            Me.applyLimits()
        End Sub

        Private Sub _currentNumeric_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _currentNumeric.ValueChanged
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Me.Instrument.CurrentLevelSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._currentNumeric.Value / 1000)
            End If
        End Sub

        Private Sub _dwellToggle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _dwellToggle.CheckedChanged
            Me._outputToggle.Enabled = Not Me._dwellToggle.Checked
            Me._measureButton.Enabled = Not Me._dwellToggle.Checked
            Me._measureToggle.Enabled = Not Me._dwellToggle.Checked
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                If Me._dwellToggle.Checked Then
                    If Me._dwellTimeNumeric.Value > 1 Then
                        Me.Instrument.Dwell(Me._dwellTimeNumeric.Value, 250, 0.1F * Me._dwellTimeNumeric.Value)
                    End If
                Else
                    Me.Instrument.AbortDwell()
                End If
            End If
        End Sub

        Private Sub _outputToggle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _outputToggle.Click, _outputToggle.CheckedChanged
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Me.Instrument.ToggleOutput(Me._outputToggle.Checked)
            End If
            Me._dwellToggle.Enabled = Not (Me._outputToggle.Checked Or Me._measureToggle.Checked)
        End Sub

        Private Sub _measureToggle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _measureToggle.Click, _measureToggle.CheckedChanged
            Me._measureButton.Enabled = Not Me._measureToggle.Checked
            Me._measureTimer.Enabled = Me._measureToggle.Checked
            Me._dwellToggle.Enabled = Not (Me._outputToggle.Checked Or Me._measureToggle.Checked)
        End Sub

        Private Sub _measureButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _measureButton.Click
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Me.Instrument.MeasureCurrent()
                Me.Instrument.MeasureVoltage()
            End If
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _measureTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _measureTimer.Tick
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Try
                    Me.Instrument.MeasureCurrent()
                    Me.Instrument.MeasureVoltage()
                Catch ex As Exception
                    Me._measureTimer.Enabled = False
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING INSTRUMENT", "Exception occurred reading instrument. Details: {0}", ex)
                    ' My.MyLibrary.ProcessException(ex, "Exception occurred reading instrument", isr.WindowsForms.ExceptionDisplayButtons.Continue)
                End Try
            End If
        End Sub

        Private Sub _voltageNumeric_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _voltageNumeric.ValueChanged
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Me.Instrument.VoltageLevelSetter(isr.Scpi.ResourceAccessLevels.Verify, Me._voltageNumeric.Value)
            End If
        End Sub

#End Region

#Region " INSTRUMENT EVENT HANDLERS "

        Private Sub _instrument_DwellAborted(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.DwellAborted
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "DWELL ABORTED", "Dwell Aborted")
        End Sub

        Private Sub _instrument_DwellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.DwellChanged
            ' display the remaining time.
            Me._dwellTimeRemainingLabel.SafeTextSetter("{0:0.0}", Me._instrument.RemainingDwellTime)
        End Sub

        Private Sub _instrument_DwellCompleted(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.DwellCompleted
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "DWELL COMPLETED", "Dwell Completed")
        End Sub

        Private Sub _instrument_DwellStarted(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.DwellStarted
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "DWELL STARTED", "Dwell Started")
        End Sub

        Private Sub _instrument_DwellStarting(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _instrument.DwellStarting
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "DWELL STARTING", "Dwell Starting")
        End Sub

        Private Sub _instrument_FailureOccurred(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.FailureOccurred
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "INSTRUMENT FAILED", "Instrument failed")
        End Sub

        Private Sub _instrument_ServiceRequest(ByVal sender As Object, ByVal e As isr.Scpi.BaseServiceEventArgs) Handles _instrument.ServiceRequest
            Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "INSTRUMENT SERVICE REQUESTED", "Instrument Service Requested")
        End Sub

#End Region

    End Class

End Namespace
