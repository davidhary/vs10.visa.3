Namespace K500

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class Panel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try

        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me._tabControl = New System.Windows.Forms.TabControl
            Me._readingTabPage = New System.Windows.Forms.TabPage
            Me._dwellTimeRemainingLabel = New System.Windows.Forms.Label
            Me._dwellTimeNumeric = New System.Windows.Forms.NumericUpDown
            Me._dwellToggle = New isr.Controls.PushButton
            Me._currentNumeric = New System.Windows.Forms.NumericUpDown
            Me._voltageNumeric = New System.Windows.Forms.NumericUpDown
            Me._measureButton = New System.Windows.Forms.Button
            Me._measureToggle = New isr.Controls.PushButton
            Me._outputToggle = New isr.Controls.PushButton
            Me._currentReadingLabel = New System.Windows.Forms.Label
            Me._voltageReadingLabel = New System.Windows.Forms.Label
            Me._interfaceTabPage = New System.Windows.Forms.TabPage
            Me._resetButton = New System.Windows.Forms.Button
            Me._interfaceClearButton = New System.Windows.Forms.Button
            Me._channelTabPage = New System.Windows.Forms.TabPage
            Me._limitsTabPage = New System.Windows.Forms.TabPage
            Me._voltageProtectionNumeric = New System.Windows.Forms.NumericUpDown
            Me._voltageLimitNumeric = New System.Windows.Forms.NumericUpDown
            Me._currentProtectionNumeric = New System.Windows.Forms.NumericUpDown
            Me._currentLimitNumeric = New System.Windows.Forms.NumericUpDown
            Me._voltageProtectionLabel = New System.Windows.Forms.Label
            Me._voltageLimitLabel = New System.Windows.Forms.Label
            Me._currentProtectionLabel = New System.Windows.Forms.Label
            Me._currentLimitLabel = New System.Windows.Forms.Label
            Me._applyLimitsButton = New System.Windows.Forms.Button
            Me._messagesTabPage = New System.Windows.Forms.TabPage
            Me._messagesBox = New isr.Controls.MessagesBox
            Me._measureTimer = New System.Windows.Forms.Timer(Me.components)
            Me._tabControl.SuspendLayout()
            Me._readingTabPage.SuspendLayout()
            CType(Me._dwellTimeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._currentNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._voltageNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            Me._interfaceTabPage.SuspendLayout()
            Me._limitsTabPage.SuspendLayout()
            CType(Me._voltageProtectionNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._voltageLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._currentProtectionNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._currentLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            Me._messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            Me.Connector.Searchable = True
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'StatusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'IdentityPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            '_tabControl
            '
            Me._tabControl.Controls.Add(Me._readingTabPage)
            Me._tabControl.Controls.Add(Me._interfaceTabPage)
            Me._tabControl.Controls.Add(Me._channelTabPage)
            Me._tabControl.Controls.Add(Me._limitsTabPage)
            Me._tabControl.Controls.Add(Me._messagesTabPage)
            Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me._tabControl.Enabled = False
            Me._tabControl.ItemSize = New System.Drawing.Size(52, 18)
            Me._tabControl.Location = New System.Drawing.Point(0, 0)
            Me._tabControl.Multiline = True
            Me._tabControl.Name = "_tabControl"
            Me._tabControl.SelectedIndex = 0
            Me._tabControl.Size = New System.Drawing.Size(312, 233)
            Me._tabControl.TabIndex = 15
            '
            '_readingTabPage
            '
            Me._readingTabPage.Controls.Add(Me._dwellTimeRemainingLabel)
            Me._readingTabPage.Controls.Add(Me._dwellTimeNumeric)
            Me._readingTabPage.Controls.Add(Me._dwellToggle)
            Me._readingTabPage.Controls.Add(Me._currentNumeric)
            Me._readingTabPage.Controls.Add(Me._voltageNumeric)
            Me._readingTabPage.Controls.Add(Me._measureButton)
            Me._readingTabPage.Controls.Add(Me._measureToggle)
            Me._readingTabPage.Controls.Add(Me._outputToggle)
            Me._readingTabPage.Controls.Add(Me._currentReadingLabel)
            Me._readingTabPage.Controls.Add(Me._voltageReadingLabel)
            Me._readingTabPage.Location = New System.Drawing.Point(4, 22)
            Me._readingTabPage.Name = "_readingTabPage"
            Me._readingTabPage.Size = New System.Drawing.Size(304, 207)
            Me._readingTabPage.TabIndex = 0
            Me._readingTabPage.Text = "Reading"
            Me._readingTabPage.UseVisualStyleBackColor = True
            '
            '_dwellTimeRemainingLabel
            '
            Me._dwellTimeRemainingLabel.AutoSize = True
            Me._dwellTimeRemainingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._dwellTimeRemainingLabel.Location = New System.Drawing.Point(202, 164)
            Me._dwellTimeRemainingLabel.Name = "_dwellTimeRemainingLabel"
            Me._dwellTimeRemainingLabel.Size = New System.Drawing.Size(72, 20)
            Me._dwellTimeRemainingLabel.TabIndex = 9
            Me._dwellTimeRemainingLabel.Text = "Seconds"
            Me._dwellTimeRemainingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_dwellTimeNumeric
            '
            Me._dwellTimeNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._dwellTimeNumeric.Location = New System.Drawing.Point(136, 160)
            Me._dwellTimeNumeric.Maximum = New Decimal(New Integer() {3600, 0, 0, 0})
            Me._dwellTimeNumeric.Name = "_dwellTimeNumeric"
            Me._dwellTimeNumeric.Size = New System.Drawing.Size(61, 29)
            Me._dwellTimeNumeric.TabIndex = 8
            Me.TipsToolTip.SetToolTip(Me._dwellTimeNumeric, "Sets dwell time in seconds")
            Me._dwellTimeNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
            '
            '_dwellToggle
            '
            Me._dwellToggle.BackColor = System.Drawing.Color.Transparent
            Me._dwellToggle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._dwellToggle.ButtonText = "Dwell"
            Me._dwellToggle.Caption = "DWELLING"
            Me._dwellToggle.CaptionDockStyle = System.Windows.Forms.DockStyle.Right
            Me._dwellToggle.CaptionWidth = 70
            Me._dwellToggle.CheckedCaption = "DWELLING"
            Me._dwellToggle.Location = New System.Drawing.Point(8, 155)
            Me._dwellToggle.Name = "_dwellToggle"
            Me._dwellToggle.Padding = New System.Windows.Forms.Padding(3)
            Me._dwellToggle.Size = New System.Drawing.Size(122, 38)
            Me._dwellToggle.TabIndex = 7
            Me._dwellToggle.UncheckedCaption = "IDLE"
            '
            '_currentNumeric
            '
            Me._currentNumeric.DecimalPlaces = 1
            Me._currentNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._currentNumeric.Location = New System.Drawing.Point(230, 67)
            Me._currentNumeric.Maximum = New Decimal(New Integer() {80, 0, 0, 0})
            Me._currentNumeric.Name = "_currentNumeric"
            Me._currentNumeric.Size = New System.Drawing.Size(68, 29)
            Me._currentNumeric.TabIndex = 3
            Me.TipsToolTip.SetToolTip(Me._currentNumeric, "Sets current")
            '
            '_voltageNumeric
            '
            Me._voltageNumeric.DecimalPlaces = 1
            Me._voltageNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._voltageNumeric.Location = New System.Drawing.Point(230, 20)
            Me._voltageNumeric.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
            Me._voltageNumeric.Name = "_voltageNumeric"
            Me._voltageNumeric.Size = New System.Drawing.Size(68, 29)
            Me._voltageNumeric.TabIndex = 1
            Me._voltageNumeric.Value = New Decimal(New Integer() {500, 0, 0, 0})
            '
            '_measureButton
            '
            Me._measureButton.Location = New System.Drawing.Point(140, 108)
            Me._measureButton.Name = "_measureButton"
            Me._measureButton.Size = New System.Drawing.Size(75, 27)
            Me._measureButton.TabIndex = 5
            Me._measureButton.Text = "&Measure"
            Me._measureButton.UseVisualStyleBackColor = True
            '
            '_measureToggle
            '
            Me._measureToggle.BackColor = System.Drawing.Color.Transparent
            Me._measureToggle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._measureToggle.ButtonText = "Measure"
            Me._measureToggle.Caption = "MANUAL"
            Me._measureToggle.CaptionDockStyle = System.Windows.Forms.DockStyle.Right
            Me._measureToggle.CaptionWidth = 60
            Me._measureToggle.CheckedCaption = "AUTO"
            Me._measureToggle.Location = New System.Drawing.Point(8, 103)
            Me._measureToggle.Name = "_measureToggle"
            Me._measureToggle.Padding = New System.Windows.Forms.Padding(3)
            Me._measureToggle.Size = New System.Drawing.Size(126, 37)
            Me._measureToggle.TabIndex = 4
            Me._measureToggle.UncheckedCaption = "MANUAL"
            '
            '_outputToggle
            '
            Me._outputToggle.BackColor = System.Drawing.Color.Transparent
            Me._outputToggle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._outputToggle.ButtonText = "Out"
            Me._outputToggle.Caption = "-OFF-"
            Me._outputToggle.CaptionDockStyle = System.Windows.Forms.DockStyle.Top
            Me._outputToggle.CaptionWidth = 52
            Me._outputToggle.CheckedCaption = "-ON-"
            Me._outputToggle.Location = New System.Drawing.Point(232, 99)
            Me._outputToggle.Name = "_outputToggle"
            Me._outputToggle.Padding = New System.Windows.Forms.Padding(3)
            Me._outputToggle.Size = New System.Drawing.Size(62, 50)
            Me._outputToggle.TabIndex = 6
            Me._outputToggle.UncheckedCaption = "-OFF-"
            '
            '_currentReadingLabel
            '
            Me._currentReadingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me._currentReadingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._currentReadingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me._currentReadingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._currentReadingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me._currentReadingLabel.Location = New System.Drawing.Point(8, 63)
            Me._currentReadingLabel.Name = "_currentReadingLabel"
            Me._currentReadingLabel.Size = New System.Drawing.Size(221, 36)
            Me._currentReadingLabel.TabIndex = 2
            Me._currentReadingLabel.Text = "80.0040 mA"
            Me._currentReadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_voltageReadingLabel
            '
            Me._voltageReadingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me._voltageReadingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._voltageReadingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me._voltageReadingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._voltageReadingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me._voltageReadingLabel.Location = New System.Drawing.Point(8, 16)
            Me._voltageReadingLabel.Name = "_voltageReadingLabel"
            Me._voltageReadingLabel.Size = New System.Drawing.Size(221, 36)
            Me._voltageReadingLabel.TabIndex = 0
            Me._voltageReadingLabel.Text = "500.000 V"
            Me._voltageReadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_interfaceTabPage
            '
            Me._interfaceTabPage.Controls.Add(Me._resetButton)
            Me._interfaceTabPage.Controls.Add(Me._interfaceClearButton)
            Me._interfaceTabPage.Location = New System.Drawing.Point(4, 22)
            Me._interfaceTabPage.Name = "_interfaceTabPage"
            Me._interfaceTabPage.Size = New System.Drawing.Size(304, 207)
            Me._interfaceTabPage.TabIndex = 2
            Me._interfaceTabPage.Text = "Interface"
            Me._interfaceTabPage.UseVisualStyleBackColor = True
            '
            '_resetButton
            '
            Me._resetButton.Location = New System.Drawing.Point(67, 92)
            Me._resetButton.Name = "_resetButton"
            Me._resetButton.Size = New System.Drawing.Size(156, 23)
            Me._resetButton.TabIndex = 1
            Me._resetButton.Text = "&Reset to Known State"
            Me._resetButton.UseVisualStyleBackColor = True
            '
            '_interfaceClearButton
            '
            Me._interfaceClearButton.Location = New System.Drawing.Point(91, 50)
            Me._interfaceClearButton.Name = "_interfaceClearButton"
            Me._interfaceClearButton.Size = New System.Drawing.Size(116, 23)
            Me._interfaceClearButton.TabIndex = 0
            Me._interfaceClearButton.Text = "&Clear Interface"
            Me._interfaceClearButton.UseVisualStyleBackColor = True
            '
            '_channelTabPage
            '
            Me._channelTabPage.Location = New System.Drawing.Point(4, 22)
            Me._channelTabPage.Name = "_channelTabPage"
            Me._channelTabPage.Size = New System.Drawing.Size(304, 207)
            Me._channelTabPage.TabIndex = 1
            Me._channelTabPage.Text = "List"
            Me._channelTabPage.UseVisualStyleBackColor = True
            '
            '_limitsTabPage
            '
            Me._limitsTabPage.Controls.Add(Me._voltageProtectionNumeric)
            Me._limitsTabPage.Controls.Add(Me._voltageLimitNumeric)
            Me._limitsTabPage.Controls.Add(Me._currentProtectionNumeric)
            Me._limitsTabPage.Controls.Add(Me._currentLimitNumeric)
            Me._limitsTabPage.Controls.Add(Me._voltageProtectionLabel)
            Me._limitsTabPage.Controls.Add(Me._voltageLimitLabel)
            Me._limitsTabPage.Controls.Add(Me._currentProtectionLabel)
            Me._limitsTabPage.Controls.Add(Me._currentLimitLabel)
            Me._limitsTabPage.Controls.Add(Me._applyLimitsButton)
            Me._limitsTabPage.Location = New System.Drawing.Point(4, 22)
            Me._limitsTabPage.Name = "_limitsTabPage"
            Me._limitsTabPage.Size = New System.Drawing.Size(304, 207)
            Me._limitsTabPage.TabIndex = 4
            Me._limitsTabPage.Text = "Limits"
            Me._limitsTabPage.UseVisualStyleBackColor = True
            '
            '_voltageProtectionNumeric
            '
            Me._voltageProtectionNumeric.Location = New System.Drawing.Point(164, 113)
            Me._voltageProtectionNumeric.Maximum = New Decimal(New Integer() {550, 0, 0, 0})
            Me._voltageProtectionNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me._voltageProtectionNumeric.Name = "_voltageProtectionNumeric"
            Me._voltageProtectionNumeric.Size = New System.Drawing.Size(44, 20)
            Me._voltageProtectionNumeric.TabIndex = 8
            Me._voltageProtectionNumeric.Value = New Decimal(New Integer() {550, 0, 0, 0})
            '
            '_voltageLimitNumeric
            '
            Me._voltageLimitNumeric.Location = New System.Drawing.Point(164, 87)
            Me._voltageLimitNumeric.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
            Me._voltageLimitNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me._voltageLimitNumeric.Name = "_voltageLimitNumeric"
            Me._voltageLimitNumeric.Size = New System.Drawing.Size(44, 20)
            Me._voltageLimitNumeric.TabIndex = 6
            Me._voltageLimitNumeric.Value = New Decimal(New Integer() {500, 0, 0, 0})
            '
            '_currentProtectionNumeric
            '
            Me._currentProtectionNumeric.Location = New System.Drawing.Point(164, 61)
            Me._currentProtectionNumeric.Maximum = New Decimal(New Integer() {88, 0, 0, 0})
            Me._currentProtectionNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me._currentProtectionNumeric.Name = "_currentProtectionNumeric"
            Me._currentProtectionNumeric.Size = New System.Drawing.Size(44, 20)
            Me._currentProtectionNumeric.TabIndex = 4
            Me._currentProtectionNumeric.Value = New Decimal(New Integer() {88, 0, 0, 0})
            '
            '_currentLimitNumeric
            '
            Me._currentLimitNumeric.Location = New System.Drawing.Point(164, 35)
            Me._currentLimitNumeric.Maximum = New Decimal(New Integer() {80, 0, 0, 0})
            Me._currentLimitNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me._currentLimitNumeric.Name = "_currentLimitNumeric"
            Me._currentLimitNumeric.Size = New System.Drawing.Size(44, 20)
            Me._currentLimitNumeric.TabIndex = 2
            Me._currentLimitNumeric.Value = New Decimal(New Integer() {80, 0, 0, 0})
            '
            '_voltageProtectionLabel
            '
            Me._voltageProtectionLabel.Location = New System.Drawing.Point(40, 114)
            Me._voltageProtectionLabel.Name = "_voltageProtectionLabel"
            Me._voltageProtectionLabel.Size = New System.Drawing.Size(121, 18)
            Me._voltageProtectionLabel.TabIndex = 7
            Me._voltageProtectionLabel.Text = "Voltage Protection [V]:"
            Me._voltageProtectionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_voltageLimitLabel
            '
            Me._voltageLimitLabel.Location = New System.Drawing.Point(40, 88)
            Me._voltageLimitLabel.Name = "_voltageLimitLabel"
            Me._voltageLimitLabel.Size = New System.Drawing.Size(121, 18)
            Me._voltageLimitLabel.TabIndex = 5
            Me._voltageLimitLabel.Text = "Voltage Limit [V]:"
            Me._voltageLimitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_currentProtectionLabel
            '
            Me._currentProtectionLabel.Location = New System.Drawing.Point(40, 62)
            Me._currentProtectionLabel.Name = "_currentProtectionLabel"
            Me._currentProtectionLabel.Size = New System.Drawing.Size(121, 18)
            Me._currentProtectionLabel.TabIndex = 3
            Me._currentProtectionLabel.Text = "Current Protection [ma]:"
            Me._currentProtectionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_currentLimitLabel
            '
            Me._currentLimitLabel.Location = New System.Drawing.Point(40, 36)
            Me._currentLimitLabel.Name = "_currentLimitLabel"
            Me._currentLimitLabel.Size = New System.Drawing.Size(121, 18)
            Me._currentLimitLabel.TabIndex = 1
            Me._currentLimitLabel.Text = "Current Limit [ma]:"
            Me._currentLimitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_applyLimitsButton
            '
            Me._applyLimitsButton.Location = New System.Drawing.Point(239, 173)
            Me._applyLimitsButton.Name = "_applyLimitsButton"
            Me._applyLimitsButton.Size = New System.Drawing.Size(50, 23)
            Me._applyLimitsButton.TabIndex = 0
            Me._applyLimitsButton.Text = "&Apply"
            Me._applyLimitsButton.UseVisualStyleBackColor = True
            '
            '_messagesTabPage
            '
            Me._messagesTabPage.Controls.Add(Me._messagesBox)
            Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me._messagesTabPage.Name = "_messagesTabPage"
            Me._messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me._messagesTabPage.TabIndex = 3
            Me._messagesTabPage.Text = "Messages"
            Me._messagesTabPage.UseVisualStyleBackColor = True
            '
            '_messagesBox
            '
            Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
            Me._messagesBox.Delimiter = "; "
            Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
            Me._messagesBox.Location = New System.Drawing.Point(0, 0)
            Me._messagesBox.Multiline = True
            Me._messagesBox.Name = "_messagesBox"
            Me._messagesBox.PresetCount = 50
            Me._messagesBox.ReadOnly = True
            Me._messagesBox.ResetCount = 100
            Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._messagesBox.Size = New System.Drawing.Size(304, 207)
            Me._messagesBox.TabIndex = 0
            Me._messagesBox.TimeFormat = "HH:mm:ss.f"
            '
            '_measureTimer
            '
            Me._measureTimer.Interval = 250
            '
            'Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me._tabControl)
            Me.Name = "Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me._tabControl, 0)
            Me._tabControl.ResumeLayout(False)
            Me._readingTabPage.ResumeLayout(False)
            Me._readingTabPage.PerformLayout()
            CType(Me._dwellTimeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._currentNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._voltageNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            Me._interfaceTabPage.ResumeLayout(False)
            Me._limitsTabPage.ResumeLayout(False)
            CType(Me._voltageProtectionNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._voltageLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._currentProtectionNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._currentLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            Me._messagesTabPage.ResumeLayout(False)
            Me._messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents _tabControl As System.Windows.Forms.TabControl
        Friend WithEvents _readingTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _outputToggle As isr.Controls.PushButton
        Friend WithEvents _voltageReadingLabel As System.Windows.Forms.Label
        Friend WithEvents _interfaceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _interfaceClearButton As System.Windows.Forms.Button
        Friend WithEvents _channelTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _limitsTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _applyLimitsButton As System.Windows.Forms.Button
        Friend WithEvents _messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _messagesBox As isr.Controls.MessagesBox
        Friend WithEvents _currentLimitLabel As System.Windows.Forms.Label
        Friend WithEvents _currentProtectionLabel As System.Windows.Forms.Label
        Friend WithEvents _voltageLimitLabel As System.Windows.Forms.Label
        Friend WithEvents _resetButton As System.Windows.Forms.Button
        Friend WithEvents _currentLimitNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _voltageProtectionLabel As System.Windows.Forms.Label
        Friend WithEvents _voltageProtectionNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _voltageLimitNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _currentProtectionNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _measureButton As System.Windows.Forms.Button
        Friend WithEvents _measureToggle As isr.Controls.PushButton
        Friend WithEvents _currentReadingLabel As System.Windows.Forms.Label
        Friend WithEvents _currentNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _voltageNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _measureTimer As System.Windows.Forms.Timer
        Friend WithEvents _dwellTimeRemainingLabel As System.Windows.Forms.Label
        Friend WithEvents _dwellTimeNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents _dwellToggle As isr.Controls.PushButton

    End Class

End Namespace
