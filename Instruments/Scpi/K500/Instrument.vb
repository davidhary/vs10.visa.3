Imports isr.Core.EventHandlerExtensions
Imports isr.Core.ControlExtensions
Imports isr.Core.NumericUpDownExtensions
Namespace K500
    ''' <summary>Implements a VISA interface to a KEPCO BHK-MG instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class Instrument
        Inherits isr.Visa.Instruments.Controller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)
            MyBase.New(resourceTitle, NationalInstruments.VisaNS.HardwareInterfaceType.Gpib)
            MyBase.UsingDevices = True
        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As Panel, ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If DwellAbortedEvent IsNot Nothing Then
                            For Each d As [Delegate] In DwellAbortedEvent.GetInvocationList
                                RemoveHandler DwellAborted, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If DwellChangedEvent IsNot Nothing Then
                            For Each d As [Delegate] In DwellChangedEvent.GetInvocationList
                                RemoveHandler DwellChanged, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If DwellCompletedEvent IsNot Nothing Then
                            For Each d As [Delegate] In DwellCompletedEvent.GetInvocationList
                                RemoveHandler DwellCompleted, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If DwellStartedEvent IsNot Nothing Then
                            For Each d As [Delegate] In DwellStartedEvent.GetInvocationList
                                RemoveHandler DwellStarted, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If DwellStartingEvent IsNot Nothing Then
                            For Each d As [Delegate] In DwellStartingEvent.GetInvocationList
                                RemoveHandler DwellStarting, CType(d, Global.System.EventHandler(Of Global.System.ComponentModel.CancelEventArgs))
                            Next
                        End If

                        If Me._reading IsNot Nothing Then
                            Me._reading.Dispose()
                            Me._reading = Nothing
                        End If

                        If Me._dwellTimer IsNot Nothing Then
                            Me._dwellTimer.Dispose()
                            Me._dwellTimer = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " I DISPLAY "

        Private _gui As Panel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As Panel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " I Resettable "

        ''' <summary>
        ''' Clears the measurement status and event registers.
        ''' </summary>
        Public Overrides Function ClearExecutionState() As Boolean

            MyBase.ClearExecutionState()

            ' clear Resettable value
            Me._resettableValues.ClearExecutionState()

            Return Not MyBase.HadError

        End Function

        ''' <summary>Initialize Resettable values, units, scales.
        ''' This is to be called once upon connecting but before reset and clear..</summary>
        Public Overrides Function InitializeExecutionState() As Boolean

            MyBase.InitializeExecutionState()

            ' instantiate the reading elements.
            Me._reading = New Readings(isr.Scpi.ReadingElements.Voltage Or isr.Scpi.ReadingElements.Current)

            Me._resettableValues = New isr.Scpi.ResettableCollection(Of isr.Scpi.IResettable)
            Me._resettableValues.Add(Me._currentLevelCache)
            Me._resettableValues.Add(Me._currentLimitCache)
            Me._resettableValues.Add(Me._currentProtectionCache)
            Me._resettableValues.Add(Me._voltageLevelCache)
            Me._resettableValues.Add(Me._voltageLimitCache)
            Me._resettableValues.Add(Me._voltageProtectionCache)

            Me._currentLevelCache.Value = 0.001024
            Me._currentLevelCache.ResetValue = 0.001024
            Me._currentLevelCache.Tolerance = 0.00005
            Me._currentLimitCache.ResetValue = 0.08
            Me._currentLimitCache.Tolerance = 0.005
            Me._currentProtectionCache.ResetValue = 0.088
            Me._currentProtectionCache.Tolerance = 0.002

            Me._voltageLevelCache.Value = 0
            Me._voltageLevelCache.Tolerance = 0.1
            Me._voltageLimitCache.ResetValue = 500
            Me._voltageLimitCache.Tolerance = 2
            Me._voltageProtectionCache.ResetValue = 550
            Me._voltageProtectionCache.Tolerance = 2
            Return True

        End Function

        ''' <summary>
        ''' Sets subsystem to its default system preset values.
        ''' </summary>
        Public Overrides Function Preset() As Boolean

            MyBase.Preset()

            ' clear the messages from the error queue
            isr.Scpi.SystemSubsystem.ClearErrorQueue(Me)

            ' clear Resettable value
            Me._resettableValues.PresetKnownState()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            MyBase.ResetAndClear()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Function ResetKnownState() As Boolean

            MyBase.ResetKnownState()

            ' clear Resettable value
            Me._resettableValues.ResetKnownState()

            Me.CurrentLevelSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.CurrentLevelGetter(isr.Scpi.ResourceAccessLevels.Cache))
            ' set protection before setting the limit. Protection must be higher than the limit
            Me.CurrentProtectionSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.CurrentProtectionGetter(isr.Scpi.ResourceAccessLevels.Cache))
            Me.CurrentLimitSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.CurrentLimitGetter(isr.Scpi.ResourceAccessLevels.Cache))
            Me.VoltageLevelSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.VoltageLevelGetter(isr.Scpi.ResourceAccessLevels.Cache))
            ' set protection before setting the limit. Protection must be higher than the limit
            Me.VoltageProtectionSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.VoltageProtectionGetter(isr.Scpi.ResourceAccessLevels.Cache))
            Me.VoltageLimitSetter(isr.Scpi.ResourceAccessLevels.Verify, Me.VoltageLimitGetter(isr.Scpi.ResourceAccessLevels.Cache))

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

#End Region

#Region " CUSTOM COMMANDS "

        ''' <summary>Opens all channels</summary>
        Public Sub ToggleOutput(ByVal turnOn As Boolean)
            If MyBase.IsConnected Then
                If turnOn Then
                    MyBase.WriteLine(":OUTP:STAT 1")
                Else
                    MyBase.WriteLine(":OUTP:STAT 0")
                End If
            End If
            If Me.Visible Then
                Me.Gui._outputToggle.SafeCheckedSetter(turnOn)
            End If
        End Sub

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Holds all Resettable values for this subsystem.
        ''' </summary>
        Private _resettableValues As isr.Scpi.ResettableCollection(Of isr.Scpi.IResettable)

        Private _isConstantCurrent As New Nullable(Of Boolean)
        Public ReadOnly Property IsConstantCurrent() As Nullable(Of Boolean)
            Get
                Return Me._isConstantCurrent
            End Get
        End Property
        ''' <summary>Gets condition of constant current sourcing.</summary>
        Public ReadOnly Property IsConstantCurrent(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._isConstantCurrent.HasValue Then
                    Me._isConstantCurrent = MyBase.QueryTrimEnd(":FUNC:MODE?") = "CURR"
                End If
                Return Me._isConstantCurrent.Value
            End Get
        End Property

        Private _isOutputOn As New Nullable(Of Boolean)
        Public ReadOnly Property IsOutputOn() As Nullable(Of Boolean)
            Get
                Return Me._isOutputOn
            End Get
        End Property
        ''' <summary>Gets condition of the output.</summary>
        Public ReadOnly Property IsOutputOn(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._isOutputOn.HasValue Then
                    Me._isOutputOn = MyBase.QueryBoolean(":OUTP:STAT?")
                End If
                Return Me._isOutputOn.Value
            End Get
        End Property

#End Region

#Region " CURRENT LEVEL "

        Private _currentLevelCache As New isr.Scpi.ResettableDouble
        ''' <summary>
        ''' Gets the current level cached.
        ''' </summary>
        Public ReadOnly Property CurrentLevelCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._currentLevelCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the current level from the instrument or the cache.</summary>
        ''' <remarks>
        ''' The instrument automatically switches between constant current and constant voltage modes
        ''' depending on the current and voltage settings. For example, setting voltage to 1 V and current to 1mA 
        ''' on a resistor of 100 Ohms yields constant current below the maximum voltage.  If the resistor
        ''' were larger than 1000 Ohms, the source will generate 1 volt with varying current.
        ''' </remarks>
        Public Function CurrentLevelGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._currentLevelCache.Value.HasValue Then
                Me._currentLevelCache.Value = MyBase.QueryDouble(":SOUR:CURR?")
            End If
            Return Me._currentLevelCache.Value.Value
        End Function

        ''' <summary>
        ''' Writes the current level to the instrument or the cache.</summary>
        ''' <remarks>
        ''' The instrument automatically switches between constant current and constant voltage modes
        ''' depending on the current and voltage settings. For example, setting voltage to 1 V and current to 1mA 
        ''' on a resistor of 100 Ohms yields constant current below the maximum voltage.  If the resistor
        ''' were larger than 1000 Ohms, the source will generate 1 volt with varying current.
        ''' </remarks>
        Public Function CurrentLevelSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> CurrentLevelCache)) Then
                MyBase.WriteLine(":SOUR:CURR {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._currentLevelCache.Value = Me.CurrentLevelGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._currentLevelCache.Value = value
            End If
            If Not Me._currentLevelCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._currentLevelCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Current Level", value, Me._currentLevelCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Current Level not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._currentNumeric.SafeValueSetter(1000 * value)
            End If
        End Function

#End Region

#Region " CURRENT LIMIT "

        Private _currentLimitCache As New isr.Scpi.ResettableDouble
        Public ReadOnly Property CurrentLimitCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._currentLimitCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the current Limit from the instrument or the cache.
        ''' </summary>
        Public Function CurrentLimitGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._currentLimitCache.Value.HasValue Then
                Me._currentLimitCache.Value = MyBase.QueryDouble(":SOUR:CURR:LIM:HIGH?")
            End If
            Return Me._currentLimitCache.Value.Value
        End Function

        ''' <summary>
        ''' Writes the current Limit to the instrument or the cache.
        ''' </summary>
        Public Function CurrentLimitSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso
                (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> CurrentLimitCache)) Then
                MyBase.WriteLine(":SOUR:CURR:LIM:HIGH {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._currentLimitCache.Value = Me.CurrentLimitGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._currentLimitCache.Value = value
            End If
            If Not Me._currentLimitCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._currentLimitCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Current Limit", value, Me._currentLimitCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Current Limit not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._currentLimitNumeric.SafeValueSetter(1000 * value)
            End If
        End Function

#End Region

#Region " CURRENT PROTECTION "

        Private _currentProtectionCache As New isr.Scpi.ResettableDouble
        Public ReadOnly Property CurrentProtectionCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._currentProtectionCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the current Protection from the instrument or the cache.
        ''' </summary>
        Public Function CurrentProtectionGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._currentProtectionCache.Value.HasValue Then
                Me._currentProtectionCache.Value = MyBase.QueryDouble(":SOUR:CURR:PROT?")
            End If
            Return Me._currentProtectionCache.Value.Value
        End Function

        ''' <summary>
        ''' Writes the current Protection to the instrument or the cache.
        ''' </summary>
        Public Function CurrentProtectionSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso
                (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> CurrentProtectionCache)) Then
                MyBase.WriteLine(":SOUR:CURR:PROT {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._currentProtectionCache.Value = Me.CurrentProtectionGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._currentProtectionCache.Value = value
            End If
            If Not Me._currentProtectionCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._currentProtectionCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Current Protection", value, Me._currentProtectionCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Current Protection not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._currentProtectionNumeric.SafeValueSetter(1000 * value)
            End If
        End Function

#End Region

#Region " MEASURE CURRENT "

        ''' <summary>
        ''' Gets a current reading and updates the <see cref="k500.Readings.CurrentReading">reading</see>
        ''' </summary>
        ''' <remarks>The instrument may return the following sample values: 
        ''' 0.0000E0, or 2.0600E-4, or 7.3000E-5
        ''' </remarks>
        Public Function MeasureCurrent() As Double
            Me.Reading.CurrentReading.Parse(MyBase.QueryTrimEnd(":MEAS:CURR?"))
            If Me.Visible Then
                Me.Gui._currentReadingLabel.SafeTextSetter(Me.Reading.CurrentReading.ToDisplayString)
            End If
            Return Me.Reading.CurrentReading.Value.Value
        End Function

#End Region

#Region " VOLTAGE LEVEL "

        Private _voltageLevelCache As New isr.Scpi.ResettableDouble
        ''' <summary>
        ''' Gets the voltage level cached.
        ''' </summary>
        Public ReadOnly Property VoltageLevelCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._voltageLevelCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the voltage level from the instrument or the cache.</summary>
        ''' <remarks>
        ''' The instrument automatically switches between constant voltage and constant voltage modes
        ''' depending on the voltage and voltage settings. For example, setting voltage to 1 V and voltage to 1mA 
        ''' on a resistor of 100 Ohms yields constant voltage below the maximum voltage.  If the resistor
        ''' were larger than 1000 Ohms, the source will generate 1 volt with varying voltage.
        ''' </remarks>
        Public Function VoltageLevelGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._voltageLevelCache.Value.HasValue Then
                Me._voltageLevelCache.Value = MyBase.QueryDouble(":SOUR:VOLT?")
            End If
            Return Me._voltageLevelCache.Value.Value
        End Function

        ''' <summary>
        ''' Writes the voltage level to the instrument or the cache.</summary>
        ''' <remarks>
        ''' The instrument automatically switches between constant voltage and constant voltage modes
        ''' depending on the voltage and voltage settings. For example, setting voltage to 1 V and voltage to 1mA 
        ''' on a resistor of 100 Ohms yields constant voltage below the maximum voltage.  If the resistor
        ''' were larger than 1000 Ohms, the source will generate 1 volt with varying voltage.
        ''' </remarks>
        Public Function VoltageLevelSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> VoltageLevelCache)) Then
                MyBase.WriteLine(":SOUR:VOLT {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._voltageLevelCache.Value = Me.VoltageLevelGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._voltageLevelCache.Value = value
            End If
            If Not Me._voltageLevelCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._voltageLevelCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Voltage Level", value, Me._voltageLevelCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Voltage Level not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._voltageNumeric.SafeValueSetter(value)
            End If
        End Function

#End Region

#Region " VOLTAGE LIMIT "

        Private _voltageLimitCache As New isr.Scpi.ResettableDouble
        Public ReadOnly Property VoltageLimitCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._voltageLimitCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the voltage Limit from the instrument or the cache.
        ''' </summary>
        Public Function VoltageLimitGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._voltageLimitCache.Value.HasValue Then
                Me._voltageLimitCache.Value = MyBase.QueryDouble(":SOUR:VOLT:LIM:HIGH?")
            End If
            Return Me._voltageLimitCache.Value.Value
        End Function
        ''' <summary>
        ''' Writes the voltage Limit to the instrument or the cache.
        ''' </summary>
        Public Function VoltageLimitSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso
                (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> VoltageLimitCache)) Then
                MyBase.WriteLine(":SOUR:VOLT:LIM:HIGH {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._voltageLimitCache.Value = Me.VoltageLimitGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._voltageLimitCache.Value = value
            End If
            If Not Me._voltageLimitCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._voltageLimitCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Voltage Limit", value, Me._voltageLimitCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Voltage Limit not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._voltageLimitNumeric.SafeValueSetter(value)
            End If
        End Function

#End Region

#Region " VOLTAGE PROTECTION "

        Private _voltageProtectionCache As New isr.Scpi.ResettableDouble
        Public ReadOnly Property VoltageProtectionCache() As isr.Scpi.ResettableDouble
            Get
                Return Me._voltageProtectionCache
            End Get
        End Property

        ''' <summary>
        ''' Reads the voltage Protection from the instrument or the cache.
        ''' </summary>
        Public Function VoltageProtectionGetter(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not Me._voltageProtectionCache.Value.HasValue Then
                Me._voltageProtectionCache.Value = MyBase.QueryDouble(":SOUR:VOLT:PROT?")
            End If
            Return Me._voltageProtectionCache.Value.Value
        End Function

        ''' <summary>
        ''' Writes the voltage Protection to the instrument or the cache.
        ''' </summary>
        Public Function VoltageProtectionSetter(ByVal access As isr.Scpi.ResourceAccessLevels, ByVal value As Double) As Double
            If Not isr.Scpi.Subsystem.IsCacheAccess(access) AndAlso
                (isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> VoltageProtectionCache)) Then
                MyBase.WriteLine(":SOUR:VOLT:PROT {0}", value)
            End If
            If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                Me._voltageProtectionCache.Value = Me.VoltageProtectionGetter(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Me._voltageProtectionCache.Value = value
            End If
            If Not Me._voltageProtectionCache.Approximates(value).GetValueOrDefault(False) Then
                If Me._voltageProtectionCache.Value.HasValue Then
                    Throw New isr.Visa.VerificationException("Voltage Protection", value, Me._voltageProtectionCache.Value.Value)
                Else
                    Throw New isr.Visa.VerificationException("Voltage Protection not set")
                End If
            End If
            If Me.Visible Then
                Me.Gui._voltageProtectionNumeric.SafeValueSetter(value)
            End If
        End Function

#End Region

#Region " VOLTAGE - MEASURE "

        ''' <summary>
        ''' Gets a Voltage reading and updates the <see cref="k500.Readings.VoltageReading">reading</see>
        ''' </summary>
        ''' <remarks>The instrument may return the following sample values: 
        ''' 0.0000E0, 1.3000E-1
        ''' </remarks>
        Public Function MeasureVoltage() As Double
            Me.Reading.VoltageReading.Parse(MyBase.QueryTrimEnd(":MEAS:VOLT?"))
            If Me.Visible Then
                Me.Gui._voltageReadingLabel.SafeTextSetter(Me.Reading.VoltageReading.ToDisplayString)
            End If
            Return Me.Reading.VoltageReading.Value.Value
        End Function

#End Region

#Region " IDENTITY / VERSION INFO "

        ''' <summary>
        ''' Queries the GPIB instrument and returns the string save the termination character.
        ''' </summary>
        Public Overrides Function ReadIdentity() As String

            If Me._versionInfo Is Nothing OrElse String.IsNullOrWhiteSpace(Me._versionInfo.Identity) Then

                Me._versionInfo = New K500.VersionInfo(MyBase.ReadIdentity)

            End If

            Return Me._versionInfo.Identity

        End Function

        Private _versionInfo As K500.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public Overloads ReadOnly Property VersionInfo() As K500.VersionInfo
            Get
                Return Me._versionInfo
            End Get
        End Property

#End Region

#Region " READINGS "

        Private _reading As Readings
        ''' <summary>
        ''' Returns the readings from the instrument.</summary>
        Public Function Reading() As Readings
            Return Me._reading
        End Function

#End Region

#Region " REGISTERS "

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Return isr.Scpi.SystemSubsystem.ReadLastError(Me)
            End Get
        End Property

        ''' <summary>Sets the measurement service status registers to request a service
        '''   request upon measurement events.</summary>
        Public Sub RequestMeasurementService()

            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.All
            MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.ServiceRequests.All And
                (Not isr.Scpi.ServiceRequests.MessageAvailable)
        End Sub

#End Region

#Region " DWELL "

        Private _dwellTimer As Threading.Timer
        Private _dwellEndTime As DateTime = DateTime.Now
        ''' <summary>
        ''' Gets the dwell end time.
        ''' </summary>
        Public ReadOnly Property DwellEndTime() As DateTime
            Get
                Return Me._dwellEndTime
            End Get
        End Property

        Private _dwellStartTime As DateTime
        ''' <summary>
        ''' Gets the dwell start time.
        ''' </summary>
        Public ReadOnly Property DwellStartTime() As DateTime
            Get
                Return Me._dwellStartTime
            End Get
        End Property

        ''' <summary>
        ''' Get the dwell elapsed time.
        ''' </summary>
        Public ReadOnly Property DwellElapsedTime() As Single
            Get
                If Me._dwellStartTime >= Me._dwellEndTime Then
                    Return CSng(DateTime.Now.Subtract(Me._dwellStartTime).TotalSeconds)
                Else
                    Return CSng(Me._dwellEndTime.Subtract(Me._dwellStartTime).TotalSeconds)
                End If
            End Get
        End Property

        Private _dwellTimeToEnd As DateTime
        ''' <summary>
        ''' Returns the time remaining in the dwell.
        ''' </summary>
        Public ReadOnly Property RemainingDwellTime() As Double
            Get
                Return Me._dwellTimeToEnd.Subtract(Now).TotalSeconds
            End Get
        End Property

        ''' <summary>
        ''' Marks the end of the dwell.
        ''' </summary>
        Private Sub markEndDwell()

            If Me._isDwellActive Then

                ' set the end time if not already set.
                Me._dwellEndTime = DateTime.Now

            End If

            ' turn on the sentinel to indicate that dwell ended.
            Me._isDwellActive = False

        End Sub
        ''' <summary>
        ''' Aborts an ongoing dwell.
        ''' </summary>
        Public Sub AbortDwell()

            Try

                ' mark the end of the dwell.
                markEndDwell()

                ' immediately turn off the output.
                Me.ToggleOutput(False)

                If Me._dwellTimer IsNot Nothing Then

                    ' turn off the timer.
                    Me._dwellTimer.Change(Threading.Timeout.Infinite, Threading.Timeout.Infinite)

                End If

                If Me.Visible Then
                    Me.Gui._dwellTimeRemainingLabel.SafeTextSetter("{0:0.00}", Me.DwellElapsedTime)
                    Me.Gui._dwellToggle.SafeCheckedSetter(False)
                End If

            Catch

                Throw

            Finally

                ' raise the Dwell Aborted event.
                OnDwellAborted(System.EventArgs.Empty)

            End Try

        End Sub

        ''' <summary>
        ''' Ends an ongoing dwell.
        ''' </summary>
        Public Sub EndDwell()

            ' mark the end of the dwell.
            markEndDwell()

            ' immediately turn off the output.
            Me.ToggleOutput(False)

            If Me._dwellTimer IsNot Nothing Then

                ' turn off the timer.
                Me._dwellTimer.Change(Threading.Timeout.Infinite, Threading.Timeout.Infinite)

            End If

            ' raise the Dwell Aborted event.
            OnDwellCompleted(System.EventArgs.Empty)

            If Me.Visible Then
                Me.Gui._dwellTimeRemainingLabel.SafeTextSetter("{0:0.00}", Me.DwellElapsedTime)
                Me.Gui._dwellToggle.SafeCheckedSetter(False)
            End If

        End Sub

        ''' <summary>
        ''' Gets or sets the condition indicating that the dwell is active. 
        ''' This has become necessary because when in debug, the timer will not stop
        ''' causing endless end of dwell events.
        ''' </summary>
        Private _isDwellActive As Boolean

        ''' <summary>
        ''' Gets or sets the time for the next measurement.
        ''' </summary>
        Private _nextMeasurementTime As DateTime

        ''' <summary>
        ''' Gets or sets the measurement interval.
        ''' </summary>
        Private _measurementInterval As Single

        ''' <summary>
        ''' Turn on power for the specified time.
        ''' </summary>
        ''' <param name="duration">Specifies the dwell duration in seconds</param>
        ''' <param name="refreshInterval">Specifies the refresh interval in MS.</param>
        ''' <param name="measurementInterval">Specifies the measurement interval in seconds.</param>
        Public Function Dwell(ByVal duration As Double, ByVal refreshInterval As Integer, ByVal measurementInterval As Single) As Boolean

            Dim startingEventArgs As New System.ComponentModel.CancelEventArgs(False)
            Me.OnDwellStarting(startingEventArgs)
            If startingEventArgs.Cancel Then
                Return False
            End If

            ' save the measurement interval
            Me._measurementInterval = measurementInterval
            Me._nextMeasurementTime = DateTime.Now

            ' Create the delegate that invokes methods for the timer.
            Dim timerDelegate As Threading.TimerCallback = AddressOf Me.dwellTimerCallback

            ' turn on the power supply
            Me.ToggleOutput(True)

            ' check if the output is on
            If Me.IsOutputOn(isr.Scpi.ResourceAccessLevels.Device) Then

                Me.OnMessageAvailable(TraceEventType.Verbose, "CREATING DWELL TIMER", "Creating dwell timer.")
                Dim autoEvent As New Threading.AutoResetEvent(False)
                Me._dwellTimer = New Threading.Timer(timerDelegate, autoEvent, refreshInterval, refreshInterval)

                ' mark the dwell start and set end time to same.
                Me._dwellStartTime = DateTime.Now

                ' turn on the sentinel to indicate that dwell started
                Me._isDwellActive = True

                ' set the end time
                Me._dwellTimeToEnd = Me._dwellStartTime.Add(TimeSpan.FromSeconds(duration))

                ' get the first measurement.
                takeMeasurement()

                Me.OnDwellStarted(System.EventArgs.Empty)

            Else

                Me.AbortDwell()

            End If

        End Function

        Private Sub takeMeasurement()

            ' if not time to stop, get a new measurement.
            ' wait for the instrument to complete the previous operation.
            MyBase.WriteLine("*OPC?")
            Threading.Thread.Sleep(10)
            Dim reply As Nullable(Of Integer) = MyBase.ReadInteger()
            If reply.HasValue AndAlso reply.Value = 1 Then
                Me.MeasureCurrent()
                Me.MeasureVoltage()
            End If

            ' protect the instrument from re-entering this method before it is ready.
            Threading.Thread.Sleep(Me._interumentRecoveryMilliseconds)

            ' update new measurement time
            Me._nextMeasurementTime = DateTime.Now.AddSeconds(Me._measurementInterval)

        End Sub

        Private _interumentRecoveryMilliseconds As Integer = 20
        Private _dwellLock As New Object

        ''' <summary> Handles the dwell timer events. </summary>
        ''' <remarks> Dwell call back fails if the dwell timer interval is too short because the instrument
        ''' is unable to recover from the previous call causing a timeout due to not having a value in
        ''' the read buffer.  To protect against that, a sleep time of 20 mS is added after the last
        ''' read. </remarks>
        ''' <param name="stateInfo"> . </param>
        Private Sub dwellTimerCallback(ByVal stateInfo As Object)

            SyncLock Me._dwellLock

                Dim autoEvent As Threading.AutoResetEvent = TryCast(stateInfo, Threading.AutoResetEvent)

                ' check if it is time to end.
                If DateTime.Now >= Me._dwellTimeToEnd Then

                    ' signal to stop the timer.
                    If autoEvent IsNot Nothing Then
                        autoEvent.Set()
                    End If

                    If Me._isDwellActive Then
                        ' end the dwell
                        Me.EndDwell()
                    End If

                Else

                    ' if not time to stop, get a new measurement.
                    If DateTime.Now < Me._nextMeasurementTime Then
                        takeMeasurement()
                    End If

                    ' display the remaining time.
                    Me.OnDwellChanged(System.EventArgs.Empty)

                End If

            End SyncLock

        End Sub

#End Region

#Region " OPERATION AND MEASUREMENT EVENTS "

        ''' <summary>Raised after the dwell was aborted.</summary>
        Public Event DwellAborted As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Dwell Aborted event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnDwellAborted(ByVal e As System.EventArgs)
            DwellAbortedEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised after the dwell ended.</summary>
        Public Event DwellCompleted As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Dwell Completed event after the dwell ended.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnDwellCompleted(ByVal e As System.EventArgs)
            DwellCompletedEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised whenever a dwell refresh time elapsed.</summary>
        Public Event DwellChanged As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Dwell Changed event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnDwellChanged(ByVal e As System.EventArgs)
            DwellChangedEvent.SafeBeginInvoke(Me, e)
        End Sub

        ''' <summary>Raised after the dwell started.</summary>
        Public Event DwellStarted As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Dwell Started event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnDwellStarted(ByVal e As System.EventArgs)
            DwellStartedEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised just before the dwell is started.</summary>
        Public Event DwellStarting As EventHandler(Of System.ComponentModel.CancelEventArgs)

        ''' <summary>Raises the Dwell Starting event.</summary>
        ''' <param name="e">Passes reference to the 
        ''' <see cref="System.ComponentModel.CancelEventArgs">cancel event arguments</see>.</param>
        Protected Overridable Sub OnDwellStarting(ByVal e As System.ComponentModel.CancelEventArgs)
            DwellStartingEvent.SafeInvoke(Me, e)
        End Sub

#End Region

#Region " SERVICE REQUEST HANDLER "

        ''' <summary>Updates the screen if we have a new measurement and raises the service
        '''   request event. New measurements are parsed including setting the threshold
        '''   levels for testing for level compliance.
        ''' Note that on a two point sweep we get to service request.  The first service request
        ''' Appears before the second reading is on screen but already has the values for both sweeps.
        ''' </summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then

                If (e.MeasurementEventStatus And MeasurementEvents.ReadingAvailable) <> 0 Then
                End If

            End If

            ' raise the service request event
            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace

