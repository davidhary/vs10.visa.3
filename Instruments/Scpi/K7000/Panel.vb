Imports isr.Core.ControlExtensions
Namespace K7000

    ''' <summary>Provides a user interface for the Keithley 70XX instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.DisplayName("K7000 Panel"),
      System.ComponentModel.Description("Keithley 7000 Family Switch - Windows Forms Custom Control"),
      System.Drawing.ToolboxBitmap(GetType(K7000.Panel))>
    Public Class Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New Instrument(Me, "K7000")

            ' set combo
            Me._serviceRequestFlagsComboBox.DataSource = Nothing
            Me._serviceRequestFlagsComboBox.Items.Clear()
            Me._serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Scpi.ServiceRequests))


        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
            If Me._channelListBuilder IsNot Nothing Then
                Me._channelListBuilder.Dispose()
                Me._channelListBuilder = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                Me._messagesBox.PrependMessage(e.ExtendedMessage)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function


        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="isr.Scpi.ServiceRequests">service request flags</see></param>
        Friend Sub ToggleEndServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Scpi.ServiceRequests)
            If Me.Visible Then
                Me._serviceRequestMaskTextBox.Text = serviceRequestMask.ToString()
                Me._enableEndOfSettlingRequestToggle.Enabled = False
                Me._enableEndOfSettlingRequestToggle.Checked = turnOn
                Me._enableEndOfSettlingRequestToggle.Enabled = True
                Me._enableEndOfSettlingRequestToggle.Visible = True
                Me._enableEndOfSettlingRequestToggle.Invalidate()
            End If
        End Sub

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="isr.Scpi.ServiceRequests">service request flags</see></param>
        Friend Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Scpi.ServiceRequests)
            If Me.Visible Then
                Me._serviceRequestMaskTextBox.Text = serviceRequestMask.ToString()
                Me._enableServiceRequestToggle.Enabled = False
                Me._enableServiceRequestToggle.Checked = turnOn
                Me._enableServiceRequestToggle.Enabled = True
                Me._enableServiceRequestToggle.Visible = True
                Me._enableServiceRequestToggle.Invalidate()
            End If
        End Sub

#End Region

#Region " I Connectible "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me._tabControl.Enabled = value
                If value Then
                    refreshDisplay()
                    ' set combo
                    Me._serviceRequestFlagsComboBox.DataSource = Nothing
                    Me._serviceRequestFlagsComboBox.Items.Clear()
                    Me._serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Scpi.ServiceRequests))
                End If
            End Set
        End Property

#End Region

#Region " PROPERTIES "

        Private _instrument As Instrument
        ''' <summary>Gets a reference to the Keithley 7000 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As Instrument)
                If value IsNot Nothing Then
                    Me._instrument = value
                    MyBase.ConnectableResource = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE SETTINGS "

#End Region

#Region " BULK SETTINGS "

        ''' <summary>Gets or sets the channel list.</summary>
        Private _channelListBuilder As isr.Scpi.ChannelListBuilder

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

                ' check if we are asking for a new scan list
                If Me._scanListComboBox IsNot Nothing AndAlso Me._scanListComboBox.Text.Length > 0 AndAlso
                    Me._scanListComboBox.FindString(Me._scanListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the scan list
                    Me._scanListComboBox.Items.Add(Me._scanListComboBox.Text)
                End If

                ' update the current scan list
                Me._scanTextBox.Text = Me.Instrument.Route.ScanList(Me.Instrument.AccessLevel)

            End If

        End Sub

        ''' <summary>Adds new items to the combo box.</summary>
        Friend Sub updateChannelListComboBox()

            If MyBase.Visible Then
                ' check if we are asking for a new channel list
                If Me._channelListComboBox IsNot Nothing AndAlso Me._channelListComboBox.Text.Length > 0 AndAlso
                    Me._channelListComboBox.FindString(Me._channelListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the channel list
                    Me._channelListComboBox.Items.Add(Me._channelListComboBox.Text)
                End If
            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SLOT "

        Private Sub readSlotConfigurationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _readSlotConfigurationButton.Click
            Dim slotNumber As Int32 = Int32.Parse(Me._slotNumberComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            Me._cardTypeTextBox.Text = isr.Scpi.RouteSubsystem.SlotCardType(Me.Instrument, slotNumber)
            Me._settlingTimeTextBoxTextBox.Text =
                isr.Scpi.RouteSubsystem.SlotSettlingTime(Me.Instrument, slotNumber).ToString(Globalization.CultureInfo.CurrentCulture)
        End Sub

        Private Sub updateSlotConfigurationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _updateSlotConfigurationButton.Click
            Dim slotNumber As Int32 = Int32.Parse(Me._slotNumberComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            isr.Scpi.RouteSubsystem.SlotCardType(Me.Instrument, slotNumber) = Me._cardTypeTextBox.Text
            isr.Scpi.RouteSubsystem.SlotSettlingTime(Me.Instrument, slotNumber) = Double.Parse(Me._settlingTimeTextBoxTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SCAN "

        Private Sub stepButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _stepButton.Click

            ' clear registers
            Me.Instrument.ClearExecutionState()

            ' close the scan list
            Me.Instrument.Trigger.Initiate()

        End Sub

        Private Sub updateScanListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _updateScanListButton.Click

            Me.Instrument.Route.ScanList(isr.Scpi.ResourceAccessLevels.None) = Me._scanListComboBox.Text
            refreshDisplay()

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: TOGGLE "

        Private Sub addChannelToList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _addChannelToList.Click
            If Me._channelListBuilder Is Nothing Then
                Me._channelListBuilder = New isr.Scpi.ChannelListBuilder
            End If
            Me._channelListBuilder.AddChannel(Int32.Parse(Me._slotNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture),
                Int32.Parse(Me._relayNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture))
            Me._channelListComboBox.Text = Me._channelListBuilder.ChannelList
        End Sub

        Private Sub addMemoryLocationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _addMemoryLocationButton.Click
            If Me._channelListBuilder Is Nothing Then
                Me._channelListBuilder = New isr.Scpi.ChannelListBuilder
            End If
            Me._channelListBuilder.AddChannel(Int32.Parse(Me._memoryLocationChannelItemTextBox.Text, Globalization.CultureInfo.CurrentCulture))
            Me._channelListComboBox.Text = Me._channelListBuilder.ChannelList
        End Sub

        Private Sub channelCloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _channelCloseButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearExecutionState()
            isr.Scpi.RouteSubsystem.CloseChannels(Me.Instrument, Me._channelListComboBox.Text)
        End Sub

        Private Sub channelOpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _channelOpenButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearExecutionState()
            isr.Scpi.RouteSubsystem.OpenChannels(Me.Instrument, Me._channelListComboBox.Text)
        End Sub

        Private Sub clearChannelListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _clearChannelListButton.Click
            Me._channelListBuilder = New isr.Scpi.ChannelListBuilder
            Me._channelListComboBox.Text = Me._channelListBuilder.ChannelList
        End Sub

        Private Sub memoryLocationTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _memoryLocationTextBox.Validating

            If Int32.Parse(Me._memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 1 OrElse
                Int32.Parse(Me._memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture) > 100 Then
                e.Cancel = True
            End If

        End Sub

        Private Sub memoryLocationChannelItemTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _memoryLocationChannelItemTextBox.Validating
            Dim dataValue As Int32
            If Not String.IsNullOrWhiteSpace(Me._memoryLocationChannelItemTextBox.Text.Trim) AndAlso
                Int32.TryParse(Me._memoryLocationChannelItemTextBox.Text, System.Globalization.NumberStyles.Integer,
                              Globalization.CultureInfo.CurrentCulture, dataValue) Then
                e.Cancel = (dataValue < 1 Or dataValue > 100)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub openAllChannelsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _openAllChannelsButton.Click

            Me.Instrument.ClearExecutionState()
            Me.Instrument.OpenAll()

        End Sub

        Private Sub relayNumberTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _relayNumberTextBox.Validating
            Dim dataValue As Int32
            If Not String.IsNullOrWhiteSpace(Me._relayNumberTextBox.Text.Trim) AndAlso
                Int32.TryParse(Me._relayNumberTextBox.Text, System.Globalization.NumberStyles.Integer,
                              Globalization.CultureInfo.CurrentCulture, dataValue) Then
                e.Cancel = (dataValue < 1 Or dataValue > 10)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub saveToMemoryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _saveToMemoryButton.Click

            Me.Instrument.ClearExecutionState()
            isr.Scpi.RouteSubsystem.MemorySave(Me.Instrument,
                Int32.Parse(Me._memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture))

        End Sub

        Private Sub slotNumberTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _slotNumberTextBox.Validating
            Dim dataValue As Int32
            If Not String.IsNullOrWhiteSpace(Me._slotNumberTextBox.Text.Trim) AndAlso
                Int32.TryParse(Me._slotNumberTextBox.Text, System.Globalization.NumberStyles.Integer,
                              Globalization.CultureInfo.CurrentCulture, dataValue) Then
                e.Cancel = (dataValue < 1 Or dataValue > 10)
            Else
                e.Cancel = True
            End If
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SRQ "

        Private Sub enableServiceRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _enableServiceRequestToggle.CheckedChanged
            If MyBase.Visible AndAlso Me._enableServiceRequestToggle.Enabled Then
                Me.Instrument.ClearExecutionState()
                Me.Instrument.ToggleServiceRequest(Me._enableServiceRequestToggle.Checked, CType(Me._serviceRequestMaskTextBox.Text, isr.Scpi.ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _serviceRequestMaskAddButton.Click

            Dim selectedFlag As isr.Scpi.ServiceRequests = CType(Me._serviceRequestFlagsComboBox.SelectedItem, isr.Scpi.ServiceRequests)
            Dim serviceByte As Byte = Byte.Parse(Me._serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte Or selectedFlag)
            Me._serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _serviceRequestMaskRemoveButton.Click

            Dim selectedFlag As isr.Scpi.ServiceRequests = CType(Me._serviceRequestFlagsComboBox.SelectedItem, isr.Scpi.ServiceRequests)
            Dim serviceByte As Byte = Byte.Parse(Me._serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte And (Not selectedFlag))
            Me._serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub enableEndOfSettlingRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _enableEndOfSettlingRequestToggle.CheckedChanged
            If MyBase.Visible AndAlso Me._enableEndOfSettlingRequestToggle.Enabled Then
                Me.Instrument.ToggleEndOfScanService(Me._enableEndOfSettlingRequestToggle.Checked, CType(Me._serviceRequestMaskTextBox.Text, isr.Scpi.ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _serviceRequestMaskTextBox.Validating
            Dim data As String = Me._serviceRequestMaskTextBox.Text.Trim
            Dim dataValue As Int32
            If Not String.IsNullOrWhiteSpace(data) AndAlso
                Int32.TryParse(data, System.Globalization.NumberStyles.Integer,
                              Globalization.CultureInfo.CurrentCulture, dataValue) Then
                If dataValue < 0 Or dataValue > 255 Then
                    Me._serviceRequestMaskTextBox.Text = "0"
                    e.Cancel = True
                    Return
                End If
            Else
                Me._serviceRequestMaskTextBox.Text = "0"
                e.Cancel = True
            End If
        End Sub

#End Region

    End Class
End Namespace
