Namespace K7000

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class Panel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me._tabControl = New System.Windows.Forms.TabControl
            Me._toggleTabPage = New System.Windows.Forms.TabPage
            Me._channelListBuilderGroupBox = New System.Windows.Forms.GroupBox
            Me._relayNumberTextBoxLabel = New System.Windows.Forms.Label
            Me._relayNumberTextBox = New System.Windows.Forms.TextBox
            Me._slotNumberTextBoxLabel = New System.Windows.Forms.Label
            Me._slotNumberTextBox = New System.Windows.Forms.TextBox
            Me._memoryLocationChannelItemTextBoxLabel = New System.Windows.Forms.Label
            Me._memoryLocationChannelItemTextBox = New System.Windows.Forms.TextBox
            Me._addMemoryLocationButton = New System.Windows.Forms.Button
            Me._clearChannelListButton = New System.Windows.Forms.Button
            Me._addChannelToList = New System.Windows.Forms.Button
            Me._memoryLocationTextBox = New System.Windows.Forms.TextBox
            Me._saveToMemoryButton = New System.Windows.Forms.Button
            Me._channelOpenButton = New System.Windows.Forms.Button
            Me._channelCloseButton = New System.Windows.Forms.Button
            Me._channelListComboBox = New System.Windows.Forms.ComboBox
            Me._channelListComboBoxLabel = New System.Windows.Forms.Label
            Me._openAllChannelsButton = New System.Windows.Forms.Button
            Me._scanTabPage = New System.Windows.Forms.TabPage
            Me._updateScanListButton = New System.Windows.Forms.Button
            Me._scanTextBox = New System.Windows.Forms.TextBox
            Me._stepButton = New System.Windows.Forms.Button
            Me._scanListComboBox = New System.Windows.Forms.ComboBox
            Me._scanListComboBoxLabel = New System.Windows.Forms.Label
            Me._slotTabPage = New System.Windows.Forms.TabPage
            Me._updateSlotConfigurationButton = New System.Windows.Forms.Button
            Me._readSlotConfigurationButton = New System.Windows.Forms.Button
            Me._cardTypeTextBox = New System.Windows.Forms.TextBox
            Me._slotNumberComboBoxLabel = New System.Windows.Forms.Label
            Me._slotNumberComboBox = New System.Windows.Forms.ComboBox
            Me._cardTypeTextBoxLabel = New System.Windows.Forms.Label
            Me._settlingTimeTextBoxLabel = New System.Windows.Forms.Label
            Me._settlingTimeTextBoxTextBox = New System.Windows.Forms.TextBox
            Me._serviceRequestTabPage = New System.Windows.Forms.TabPage
            Me._enableEndOfSettlingRequestToggle = New System.Windows.Forms.CheckBox
            Me._serviceRequestRegisterGroupBox = New System.Windows.Forms.GroupBox
            Me._serviceRequestMaskRemoveButton = New System.Windows.Forms.Button
            Me._serviceRequestMaskAddButton = New System.Windows.Forms.Button
            Me._serviceRequestFlagsComboBox = New System.Windows.Forms.ComboBox
            Me._enableServiceRequestToggle = New System.Windows.Forms.CheckBox
            Me._serviceRequestByteTextBoxLabel = New System.Windows.Forms.Label
            Me._serviceRequestMaskTextBox = New System.Windows.Forms.TextBox
            Me._messagesTabPage = New System.Windows.Forms.TabPage
            Me._messagesBox = New isr.Controls.MessagesBox
            Me._tabControl.SuspendLayout()
            Me._toggleTabPage.SuspendLayout()
            Me._channelListBuilderGroupBox.SuspendLayout()
            Me._scanTabPage.SuspendLayout()
            Me._slotTabPage.SuspendLayout()
            Me._serviceRequestTabPage.SuspendLayout()
            Me._serviceRequestRegisterGroupBox.SuspendLayout()
            Me._messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Clearable = True
            Me.Connector.Connectible = True
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            Me.Connector.Searchable = True
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'statusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'idPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            'mainTabControl
            '
            Me._tabControl.Controls.Add(Me._toggleTabPage)
            Me._tabControl.Controls.Add(Me._scanTabPage)
            Me._tabControl.Controls.Add(Me._slotTabPage)
            Me._tabControl.Controls.Add(Me._serviceRequestTabPage)
            Me._tabControl.Controls.Add(Me._messagesTabPage)
            Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me._tabControl.Enabled = False
            Me._tabControl.Location = New System.Drawing.Point(0, 0)
            Me._tabControl.Multiline = True
            Me._tabControl.Name = "_tabControl"
            Me._tabControl.SelectedIndex = 0
            Me._tabControl.Size = New System.Drawing.Size(312, 233)
            Me._tabControl.TabIndex = 15
            '
            'toggleTabPage
            '
            Me._toggleTabPage.Controls.Add(Me._channelListBuilderGroupBox)
            Me._toggleTabPage.Controls.Add(Me._memoryLocationTextBox)
            Me._toggleTabPage.Controls.Add(Me._saveToMemoryButton)
            Me._toggleTabPage.Controls.Add(Me._channelOpenButton)
            Me._toggleTabPage.Controls.Add(Me._channelCloseButton)
            Me._toggleTabPage.Controls.Add(Me._channelListComboBox)
            Me._toggleTabPage.Controls.Add(Me._channelListComboBoxLabel)
            Me._toggleTabPage.Controls.Add(Me._openAllChannelsButton)
            Me._toggleTabPage.Location = New System.Drawing.Point(4, 22)
            Me._toggleTabPage.Name = "_toggleTabPage"
            Me._toggleTabPage.Size = New System.Drawing.Size(304, 207)
            Me._toggleTabPage.TabIndex = 1
            Me._toggleTabPage.Text = "Toggle"
            Me._toggleTabPage.UseVisualStyleBackColor = True
            '
            'channelListBuilderGroupBox
            '
            Me._channelListBuilderGroupBox.Controls.Add(Me._relayNumberTextBoxLabel)
            Me._channelListBuilderGroupBox.Controls.Add(Me._relayNumberTextBox)
            Me._channelListBuilderGroupBox.Controls.Add(Me._slotNumberTextBoxLabel)
            Me._channelListBuilderGroupBox.Controls.Add(Me._slotNumberTextBox)
            Me._channelListBuilderGroupBox.Controls.Add(Me._memoryLocationChannelItemTextBoxLabel)
            Me._channelListBuilderGroupBox.Controls.Add(Me._memoryLocationChannelItemTextBox)
            Me._channelListBuilderGroupBox.Controls.Add(Me._addMemoryLocationButton)
            Me._channelListBuilderGroupBox.Controls.Add(Me._clearChannelListButton)
            Me._channelListBuilderGroupBox.Controls.Add(Me._addChannelToList)
            Me._channelListBuilderGroupBox.Location = New System.Drawing.Point(32, 6)
            Me._channelListBuilderGroupBox.Name = "_channelListBuilderGroupBox"
            Me._channelListBuilderGroupBox.Size = New System.Drawing.Size(240, 97)
            Me._channelListBuilderGroupBox.TabIndex = 0
            Me._channelListBuilderGroupBox.TabStop = False
            Me._channelListBuilderGroupBox.Text = "Channel List Builder"
            '
            'relayNumberTextBoxLabel
            '
            Me._relayNumberTextBoxLabel.Location = New System.Drawing.Point(16, 73)
            Me._relayNumberTextBoxLabel.Name = "_relayNumberTextBoxLabel"
            Me._relayNumberTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._relayNumberTextBoxLabel.TabIndex = 5
            Me._relayNumberTextBoxLabel.Text = "Relay: "
            Me._relayNumberTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'relayNumberTextBox
            '
            Me._relayNumberTextBox.Location = New System.Drawing.Point(68, 71)
            Me._relayNumberTextBox.Name = "_relayNumberTextBox"
            Me._relayNumberTextBox.Size = New System.Drawing.Size(24, 20)
            Me._relayNumberTextBox.TabIndex = 6
            Me._relayNumberTextBox.Text = "1"
            '
            'slotNumberTextBoxLabel
            '
            Me._slotNumberTextBoxLabel.Location = New System.Drawing.Point(16, 49)
            Me._slotNumberTextBoxLabel.Name = "_slotNumberTextBoxLabel"
            Me._slotNumberTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._slotNumberTextBoxLabel.TabIndex = 3
            Me._slotNumberTextBoxLabel.Text = "Slot: "
            Me._slotNumberTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'slotNumberTextBox
            '
            Me._slotNumberTextBox.Location = New System.Drawing.Point(68, 47)
            Me._slotNumberTextBox.Name = "_slotNumberTextBox"
            Me._slotNumberTextBox.Size = New System.Drawing.Size(24, 20)
            Me._slotNumberTextBox.TabIndex = 4
            Me._slotNumberTextBox.Text = "1"
            '
            'memoryLocationChannelItemTextBoxLabel
            '
            Me._memoryLocationChannelItemTextBoxLabel.Location = New System.Drawing.Point(8, 22)
            Me._memoryLocationChannelItemTextBoxLabel.Name = "_memoryLocationChannelItemTextBoxLabel"
            Me._memoryLocationChannelItemTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._memoryLocationChannelItemTextBoxLabel.TabIndex = 0
            Me._memoryLocationChannelItemTextBoxLabel.Text = "Location: "
            '
            'memoryLocationChannelItemTextBox
            '
            Me._memoryLocationChannelItemTextBox.Location = New System.Drawing.Point(60, 20)
            Me._memoryLocationChannelItemTextBox.Name = "_memoryLocationChannelItemTextBox"
            Me._memoryLocationChannelItemTextBox.Size = New System.Drawing.Size(32, 20)
            Me._memoryLocationChannelItemTextBox.TabIndex = 1
            Me._memoryLocationChannelItemTextBox.Text = "1"
            '
            'addMemoryLocationButton
            '
            Me._addMemoryLocationButton.Location = New System.Drawing.Point(101, 18)
            Me._addMemoryLocationButton.Name = "_addMemoryLocationButton"
            Me._addMemoryLocationButton.Size = New System.Drawing.Size(128, 24)
            Me._addMemoryLocationButton.TabIndex = 2
            Me._addMemoryLocationButton.Text = "Add Memory Location"
            Me._addMemoryLocationButton.UseVisualStyleBackColor = True
            '
            'clearChannelListButton
            '
            Me._clearChannelListButton.Location = New System.Drawing.Point(181, 51)
            Me._clearChannelListButton.Name = "_clearChannelListButton"
            Me._clearChannelListButton.Size = New System.Drawing.Size(48, 40)
            Me._clearChannelListButton.TabIndex = 8
            Me._clearChannelListButton.Text = "Clear List"
            Me._clearChannelListButton.UseVisualStyleBackColor = True
            '
            'addChannelToList
            '
            Me._addChannelToList.Location = New System.Drawing.Point(101, 51)
            Me._addChannelToList.Name = "_addChannelToList"
            Me._addChannelToList.Size = New System.Drawing.Size(64, 40)
            Me._addChannelToList.TabIndex = 7
            Me._addChannelToList.Text = "Add Relay to List"
            Me._addChannelToList.UseVisualStyleBackColor = True
            '
            'memoryLocationTextBox
            '
            Me._memoryLocationTextBox.Location = New System.Drawing.Point(164, 167)
            Me._memoryLocationTextBox.MaxLength = 3
            Me._memoryLocationTextBox.Name = "_memoryLocationTextBox"
            Me._memoryLocationTextBox.Size = New System.Drawing.Size(35, 20)
            Me._memoryLocationTextBox.TabIndex = 6
            Me._memoryLocationTextBox.Text = "1"
            '
            'saveToMemoryButton
            '
            Me._saveToMemoryButton.Location = New System.Drawing.Point(8, 166)
            Me._saveToMemoryButton.Name = "_saveToMemoryButton"
            Me._saveToMemoryButton.Size = New System.Drawing.Size(156, 23)
            Me._saveToMemoryButton.TabIndex = 5
            Me._saveToMemoryButton.Text = "&Save To Memory Location # "
            Me._saveToMemoryButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            Me._saveToMemoryButton.UseVisualStyleBackColor = True
            '
            'channelOpenButton
            '
            Me._channelOpenButton.Location = New System.Drawing.Point(253, 110)
            Me._channelOpenButton.Name = "_channelOpenButton"
            Me._channelOpenButton.Size = New System.Drawing.Size(48, 23)
            Me._channelOpenButton.TabIndex = 4
            Me._channelOpenButton.Text = "&Open"
            Me._channelOpenButton.UseVisualStyleBackColor = True
            '
            'channelCloseButton
            '
            Me._channelCloseButton.Location = New System.Drawing.Point(200, 110)
            Me._channelCloseButton.Name = "_channelCloseButton"
            Me._channelCloseButton.Size = New System.Drawing.Size(49, 23)
            Me._channelCloseButton.TabIndex = 3
            Me._channelCloseButton.Text = "&Close"
            Me._channelCloseButton.UseVisualStyleBackColor = True
            '
            'channelListComboBox
            '
            Me._channelListComboBox.Location = New System.Drawing.Point(8, 134)
            Me._channelListComboBox.Name = "_channelListComboBox"
            Me._channelListComboBox.Size = New System.Drawing.Size(293, 21)
            Me._channelListComboBox.TabIndex = 2
            Me._channelListComboBox.Text = "(@ 4!1,5!1)"
            '
            'channelListComboBoxLabel
            '
            Me._channelListComboBoxLabel.Location = New System.Drawing.Point(8, 118)
            Me._channelListComboBoxLabel.Name = "_channelListComboBoxLabel"
            Me._channelListComboBoxLabel.Size = New System.Drawing.Size(72, 16)
            Me._channelListComboBoxLabel.TabIndex = 1
            Me._channelListComboBoxLabel.Text = "Channel List:"
            '
            'openAllChannelsButton
            '
            Me._openAllChannelsButton.Location = New System.Drawing.Point(237, 166)
            Me._openAllChannelsButton.Name = "_openAllChannelsButton"
            Me._openAllChannelsButton.Size = New System.Drawing.Size(64, 23)
            Me._openAllChannelsButton.TabIndex = 7
            Me._openAllChannelsButton.Text = "Open &All"
            Me._openAllChannelsButton.UseVisualStyleBackColor = True
            '
            'scanTabPage
            '
            Me._scanTabPage.Controls.Add(Me._updateScanListButton)
            Me._scanTabPage.Controls.Add(Me._scanTextBox)
            Me._scanTabPage.Controls.Add(Me._stepButton)
            Me._scanTabPage.Controls.Add(Me._scanListComboBox)
            Me._scanTabPage.Controls.Add(Me._scanListComboBoxLabel)
            Me._scanTabPage.Location = New System.Drawing.Point(4, 22)
            Me._scanTabPage.Name = "_scanTabPage"
            Me._scanTabPage.Size = New System.Drawing.Size(304, 207)
            Me._scanTabPage.TabIndex = 0
            Me._scanTabPage.Text = "Scan"
            Me._scanTabPage.UseVisualStyleBackColor = True
            '
            'updateScanListButton
            '
            Me._updateScanListButton.Location = New System.Drawing.Point(212, 82)
            Me._updateScanListButton.Name = "_updateScanListButton"
            Me._updateScanListButton.Size = New System.Drawing.Size(75, 23)
            Me._updateScanListButton.TabIndex = 4
            Me._updateScanListButton.Text = "&Apply"
            Me._updateScanListButton.UseVisualStyleBackColor = True
            '
            'scanTextBox
            '
            Me._scanTextBox.Location = New System.Drawing.Point(18, 114)
            Me._scanTextBox.Multiline = True
            Me._scanTextBox.Name = "_scanTextBox"
            Me._scanTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._scanTextBox.Size = New System.Drawing.Size(269, 42)
            Me._scanTextBox.TabIndex = 3
            Me._scanTextBox.Text = "<scan list>"
            '
            'stepButton
            '
            Me._stepButton.Location = New System.Drawing.Point(18, 82)
            Me._stepButton.Name = "_stepButton"
            Me._stepButton.Size = New System.Drawing.Size(75, 23)
            Me._stepButton.TabIndex = 2
            Me._stepButton.Text = "&Step"
            Me._stepButton.UseVisualStyleBackColor = True
            '
            'scanListComboBox
            '
            Me._scanListComboBox.Location = New System.Drawing.Point(18, 58)
            Me._scanListComboBox.Name = "_scanListComboBox"
            Me._scanListComboBox.Size = New System.Drawing.Size(269, 21)
            Me._scanListComboBox.TabIndex = 1
            Me._scanListComboBox.Text = "(@ M1,M2)"
            '
            'scanListComboBoxLabel
            '
            Me._scanListComboBoxLabel.Location = New System.Drawing.Point(18, 42)
            Me._scanListComboBoxLabel.Name = "_scanListComboBoxLabel"
            Me._scanListComboBoxLabel.Size = New System.Drawing.Size(56, 16)
            Me._scanListComboBoxLabel.TabIndex = 0
            Me._scanListComboBoxLabel.Text = "Scan List:"
            '
            'slotTabPage
            '
            Me._slotTabPage.Controls.Add(Me._updateSlotConfigurationButton)
            Me._slotTabPage.Controls.Add(Me._readSlotConfigurationButton)
            Me._slotTabPage.Controls.Add(Me._cardTypeTextBox)
            Me._slotTabPage.Controls.Add(Me._slotNumberComboBoxLabel)
            Me._slotTabPage.Controls.Add(Me._slotNumberComboBox)
            Me._slotTabPage.Controls.Add(Me._cardTypeTextBoxLabel)
            Me._slotTabPage.Controls.Add(Me._settlingTimeTextBoxLabel)
            Me._slotTabPage.Controls.Add(Me._settlingTimeTextBoxTextBox)
            Me._slotTabPage.Location = New System.Drawing.Point(4, 22)
            Me._slotTabPage.Name = "_slotTabPage"
            Me._slotTabPage.Size = New System.Drawing.Size(304, 207)
            Me._slotTabPage.TabIndex = 2
            Me._slotTabPage.Text = "Slot"
            Me._slotTabPage.UseVisualStyleBackColor = True
            '
            'updateSlotConfigurationButton
            '
            Me._updateSlotConfigurationButton.Location = New System.Drawing.Point(207, 141)
            Me._updateSlotConfigurationButton.Name = "_updateSlotConfigurationButton"
            Me._updateSlotConfigurationButton.Size = New System.Drawing.Size(75, 23)
            Me._updateSlotConfigurationButton.TabIndex = 7
            Me._updateSlotConfigurationButton.Text = "&Apply"
            Me._updateSlotConfigurationButton.UseVisualStyleBackColor = True
            '
            'readSlotConfigurationButton
            '
            Me._readSlotConfigurationButton.Location = New System.Drawing.Point(119, 141)
            Me._readSlotConfigurationButton.Name = "_readSlotConfigurationButton"
            Me._readSlotConfigurationButton.Size = New System.Drawing.Size(80, 23)
            Me._readSlotConfigurationButton.TabIndex = 6
            Me._readSlotConfigurationButton.Text = "&Read"
            Me._readSlotConfigurationButton.UseVisualStyleBackColor = True
            '
            'cardTypeTextBox
            '
            Me._cardTypeTextBox.Location = New System.Drawing.Point(135, 83)
            Me._cardTypeTextBox.Name = "_cardTypeTextBox"
            Me._cardTypeTextBox.Size = New System.Drawing.Size(100, 20)
            Me._cardTypeTextBox.TabIndex = 3
            Me._cardTypeTextBox.Text = "<card type>"
            '
            'slotNumberComboBoxLabel
            '
            Me._slotNumberComboBoxLabel.Location = New System.Drawing.Point(23, 61)
            Me._slotNumberComboBoxLabel.Name = "_slotNumberComboBoxLabel"
            Me._slotNumberComboBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me._slotNumberComboBoxLabel.TabIndex = 0
            Me._slotNumberComboBoxLabel.Text = "Number: "
            Me._slotNumberComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'slotNumberComboBox
            '
            Me._slotNumberComboBox.DisplayMember = "Select slot number"
            Me._slotNumberComboBox.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"})
            Me._slotNumberComboBox.Location = New System.Drawing.Point(135, 59)
            Me._slotNumberComboBox.Name = "_slotNumberComboBox"
            Me._slotNumberComboBox.Size = New System.Drawing.Size(40, 21)
            Me._slotNumberComboBox.TabIndex = 1
            Me._slotNumberComboBox.Text = "1"
            Me._slotNumberComboBox.ValueMember = "Select slot number"
            '
            'cardTypeTextBoxLabel
            '
            Me._cardTypeTextBoxLabel.Location = New System.Drawing.Point(23, 87)
            Me._cardTypeTextBoxLabel.Name = "_cardTypeTextBoxLabel"
            Me._cardTypeTextBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me._cardTypeTextBoxLabel.TabIndex = 2
            Me._cardTypeTextBoxLabel.Text = "Card Type: "
            Me._cardTypeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'settlingTimeTextBoxLabel
            '
            Me._settlingTimeTextBoxLabel.Location = New System.Drawing.Point(23, 110)
            Me._settlingTimeTextBoxLabel.Name = "_settlingTimeTextBoxLabel"
            Me._settlingTimeTextBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me._settlingTimeTextBoxLabel.TabIndex = 4
            Me._settlingTimeTextBoxLabel.Text = "Settling Time [S]: "
            Me._settlingTimeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'settlingTimeTextBoxTextBox
            '
            Me._settlingTimeTextBoxTextBox.Location = New System.Drawing.Point(135, 107)
            Me._settlingTimeTextBoxTextBox.Name = "_settlingTimeTextBoxTextBox"
            Me._settlingTimeTextBoxTextBox.Size = New System.Drawing.Size(100, 20)
            Me._settlingTimeTextBoxTextBox.TabIndex = 5
            Me._settlingTimeTextBoxTextBox.Text = "<Settling Time>"
            '
            'serviceRequestTabPage
            '
            Me._serviceRequestTabPage.Controls.Add(Me._enableEndOfSettlingRequestToggle)
            Me._serviceRequestTabPage.Controls.Add(Me._serviceRequestRegisterGroupBox)
            Me._serviceRequestTabPage.Location = New System.Drawing.Point(4, 22)
            Me._serviceRequestTabPage.Name = "_serviceRequestTabPage"
            Me._serviceRequestTabPage.Size = New System.Drawing.Size(304, 207)
            Me._serviceRequestTabPage.TabIndex = 4
            Me._serviceRequestTabPage.Text = "SRQ"
            Me._serviceRequestTabPage.UseVisualStyleBackColor = True
            '
            'enableEndOfSettlingRequestCheckBox
            '
            Me._enableEndOfSettlingRequestToggle.Location = New System.Drawing.Point(11, 102)
            Me._enableEndOfSettlingRequestToggle.Name = "_enableEndOfSettlingRequestToggle"
            Me._enableEndOfSettlingRequestToggle.Size = New System.Drawing.Size(232, 32)
            Me._enableEndOfSettlingRequestToggle.TabIndex = 4
            Me._enableEndOfSettlingRequestToggle.Text = "Enable End of Settling Request"
            Me._enableEndOfSettlingRequestToggle.UseVisualStyleBackColor = True
            '
            'serviceRequestRegisterGroupBox
            '
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._serviceRequestMaskRemoveButton)
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._serviceRequestMaskAddButton)
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._serviceRequestFlagsComboBox)
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._enableServiceRequestToggle)
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._serviceRequestByteTextBoxLabel)
            Me._serviceRequestRegisterGroupBox.Controls.Add(Me._serviceRequestMaskTextBox)
            Me._serviceRequestRegisterGroupBox.Location = New System.Drawing.Point(8, 8)
            Me._serviceRequestRegisterGroupBox.Name = "_serviceRequestRegisterGroupBox"
            Me._serviceRequestRegisterGroupBox.Size = New System.Drawing.Size(288, 88)
            Me._serviceRequestRegisterGroupBox.TabIndex = 3
            Me._serviceRequestRegisterGroupBox.TabStop = False
            Me._serviceRequestRegisterGroupBox.Text = "Service Request Register"
            '
            'serviceRequestMaskRemoveButton
            '
            Me._serviceRequestMaskRemoveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._serviceRequestMaskRemoveButton.Location = New System.Drawing.Point(222, 55)
            Me._serviceRequestMaskRemoveButton.Name = "_serviceRequestMaskRemoveButton"
            Me._serviceRequestMaskRemoveButton.Size = New System.Drawing.Size(24, 23)
            Me._serviceRequestMaskRemoveButton.TabIndex = 5
            Me._serviceRequestMaskRemoveButton.Text = "-  "
            Me._serviceRequestMaskRemoveButton.UseVisualStyleBackColor = True
            '
            'serviceRequestMaskAddButton
            '
            Me._serviceRequestMaskAddButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._serviceRequestMaskAddButton.Location = New System.Drawing.Point(197, 55)
            Me._serviceRequestMaskAddButton.Name = "_serviceRequestMaskAddButton"
            Me._serviceRequestMaskAddButton.Size = New System.Drawing.Size(24, 23)
            Me._serviceRequestMaskAddButton.TabIndex = 4
            Me._serviceRequestMaskAddButton.Text = "+  "
            Me._serviceRequestMaskAddButton.UseVisualStyleBackColor = True
            '
            'serviceRequestFlagsComboBox
            '
            Me._serviceRequestFlagsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._serviceRequestFlagsComboBox.Location = New System.Drawing.Point(9, 56)
            Me._serviceRequestFlagsComboBox.Name = "_serviceRequestFlagsComboBox"
            Me._serviceRequestFlagsComboBox.Size = New System.Drawing.Size(187, 21)
            Me._serviceRequestFlagsComboBox.TabIndex = 3
            '
            'enableServiceRequestCheckBox
            '
            Me._enableServiceRequestToggle.Location = New System.Drawing.Point(8, 16)
            Me._enableServiceRequestToggle.Name = "_enableServiceRequestToggle"
            Me._enableServiceRequestToggle.Size = New System.Drawing.Size(152, 24)
            Me._enableServiceRequestToggle.TabIndex = 0
            Me._enableServiceRequestToggle.Text = "Enable Service Request"
            Me._enableServiceRequestToggle.UseVisualStyleBackColor = True
            '
            'serviceRequestByteTextBoxLabel
            '
            Me._serviceRequestByteTextBoxLabel.Location = New System.Drawing.Point(8, 40)
            Me._serviceRequestByteTextBoxLabel.Name = "_serviceRequestByteTextBoxLabel"
            Me._serviceRequestByteTextBoxLabel.Size = New System.Drawing.Size(120, 16)
            Me._serviceRequestByteTextBoxLabel.TabIndex = 1
            Me._serviceRequestByteTextBoxLabel.Text = "Service Request Mask: "
            '
            'serviceRequestMaskTextBox
            '
            Me._serviceRequestMaskTextBox.Location = New System.Drawing.Point(247, 56)
            Me._serviceRequestMaskTextBox.MaxLength = 3
            Me._serviceRequestMaskTextBox.Name = "_serviceRequestMaskTextBox"
            Me._serviceRequestMaskTextBox.Size = New System.Drawing.Size(32, 20)
            Me._serviceRequestMaskTextBox.TabIndex = 2
            Me._serviceRequestMaskTextBox.Text = "239"
            '
            'messagesTabPage
            '
            Me._messagesTabPage.Controls.Add(Me._messagesBox)
            Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me._messagesTabPage.Name = "_messagesTabPage"
            Me._messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me._messagesTabPage.TabIndex = 3
            Me._messagesTabPage.Text = "Messages"
            Me._messagesTabPage.UseVisualStyleBackColor = True
            '
            'messagesMessageList
            '
            Me._messagesBox.Bullet = "* "
            Me._messagesBox.Delimiter = "; "
            Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
            Me._messagesBox.Location = New System.Drawing.Point(0, 0)
            Me._messagesBox.Multiline = True
            Me._messagesBox.Name = "_messagesBox"
            Me._messagesBox.PresetCount = 50
            Me._messagesBox.ReadOnly = True
            Me._messagesBox.ResetCount = 100
            Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._messagesBox.Size = New System.Drawing.Size(304, 207)
            Me._messagesBox.TabIndex = 0
            Me._messagesBox.TimeFormat = "HH:mm:ss.f"
            Me._messagesBox.UsingBullet = True
            Me._messagesBox.UsingSynopsis = False
            Me._messagesBox.UsingTimeBullet = True
            Me._messagesBox.UsingTraceLevel = False
            '
            'Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me._tabControl)
            Me.Name = "Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me._tabControl, 0)
            Me._tabControl.ResumeLayout(False)
            Me._toggleTabPage.ResumeLayout(False)
            Me._toggleTabPage.PerformLayout()
            Me._channelListBuilderGroupBox.ResumeLayout(False)
            Me._channelListBuilderGroupBox.PerformLayout()
            Me._scanTabPage.ResumeLayout(False)
            Me._scanTabPage.PerformLayout()
            Me._slotTabPage.ResumeLayout(False)
            Me._slotTabPage.PerformLayout()
            Me._serviceRequestTabPage.ResumeLayout(False)
            Me._serviceRequestRegisterGroupBox.ResumeLayout(False)
            Me._serviceRequestRegisterGroupBox.PerformLayout()
            Me._messagesTabPage.ResumeLayout(False)
            Me._messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Private WithEvents _tabControl As System.Windows.Forms.TabControl
        Private WithEvents _toggleTabPage As System.Windows.Forms.TabPage
        Private WithEvents _channelListBuilderGroupBox As System.Windows.Forms.GroupBox
        Private WithEvents _relayNumberTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _relayNumberTextBox As System.Windows.Forms.TextBox
        Private WithEvents _slotNumberTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _slotNumberTextBox As System.Windows.Forms.TextBox
        Private WithEvents _memoryLocationChannelItemTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _memoryLocationChannelItemTextBox As System.Windows.Forms.TextBox
        Private WithEvents _addMemoryLocationButton As System.Windows.Forms.Button
        Private WithEvents _clearChannelListButton As System.Windows.Forms.Button
        Private WithEvents _addChannelToList As System.Windows.Forms.Button
        Private WithEvents _memoryLocationTextBox As System.Windows.Forms.TextBox
        Private WithEvents _saveToMemoryButton As System.Windows.Forms.Button
        Private WithEvents _channelOpenButton As System.Windows.Forms.Button
        Private WithEvents _channelCloseButton As System.Windows.Forms.Button
        Private WithEvents _channelListComboBox As System.Windows.Forms.ComboBox
        Private WithEvents _channelListComboBoxLabel As System.Windows.Forms.Label
        Private WithEvents _openAllChannelsButton As System.Windows.Forms.Button
        Private WithEvents _scanTabPage As System.Windows.Forms.TabPage
        Private WithEvents _updateScanListButton As System.Windows.Forms.Button
        Private WithEvents _scanTextBox As System.Windows.Forms.TextBox
        Private WithEvents _stepButton As System.Windows.Forms.Button
        Private WithEvents _scanListComboBox As System.Windows.Forms.ComboBox
        Private WithEvents _scanListComboBoxLabel As System.Windows.Forms.Label
        Private WithEvents _slotTabPage As System.Windows.Forms.TabPage
        Private WithEvents _updateSlotConfigurationButton As System.Windows.Forms.Button
        Private WithEvents _readSlotConfigurationButton As System.Windows.Forms.Button
        Private WithEvents _cardTypeTextBox As System.Windows.Forms.TextBox
        Private WithEvents _slotNumberComboBoxLabel As System.Windows.Forms.Label
        Private WithEvents _slotNumberComboBox As System.Windows.Forms.ComboBox
        Private WithEvents _cardTypeTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _settlingTimeTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _settlingTimeTextBoxTextBox As System.Windows.Forms.TextBox
        Private WithEvents _serviceRequestTabPage As System.Windows.Forms.TabPage
        Private WithEvents _enableEndOfSettlingRequestToggle As System.Windows.Forms.CheckBox
        Private WithEvents _serviceRequestRegisterGroupBox As System.Windows.Forms.GroupBox
        Private WithEvents _serviceRequestMaskRemoveButton As System.Windows.Forms.Button
        Private WithEvents _serviceRequestMaskAddButton As System.Windows.Forms.Button
        Private WithEvents _serviceRequestFlagsComboBox As System.Windows.Forms.ComboBox
        Private WithEvents _enableServiceRequestToggle As System.Windows.Forms.CheckBox
        Private WithEvents _serviceRequestByteTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents _serviceRequestMaskTextBox As System.Windows.Forms.TextBox
        Private WithEvents _messagesTabPage As System.Windows.Forms.TabPage
        Private WithEvents _messagesBox As isr.Controls.MessagesBox

    End Class


End Namespace
