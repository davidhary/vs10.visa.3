Imports isr.Core.ControlExtensions
Namespace K7000

    ''' <summary>Implements a VISA interface to a Keithley 70XX instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class Instrument

        ' based on the instrument class
        Inherits isr.Visa.Instruments.Controller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)
            MyBase.New(resourceTitle, NationalInstruments.VisaNS.HardwareInterfaceType.Gpib)
            MyBase.UsingDevices = True
        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As Panel, ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I DISPLAY "

        Private _gui As Panel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As Panel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " I Resettable "

        ''' <summary>
        ''' Clears the measurement status and event registers.
        ''' </summary>
        Public Overrides Function ClearExecutionState() As Boolean
            Return MyBase.ClearExecutionState()
        End Function

        ''' <summary>Initialize Resettable values, units, scales.
        ''' This is to be called once upon connecting but before reset and clear..</summary>
        Public Overrides Function InitializeExecutionState() As Boolean

            MyBase.InitializeExecutionState()

            ' set the line frequency.
            Me.PresetLineFrequency()

            ' add arm layers.
            Me.addLayers()

            ' set reset, clear, and preset values.
            ' Me._armLayer1.AutoDelay.ResetValue = False
            Me._armLayer1.Count.ResetValue = 1
            Me._armLayer1.Count.PresetValue = 1
            Me._armLayer1.Delay.ResetValue = 0
            Me._armLayer1.Bypass.ResetValue = True
            Me._armLayer1.InputLineNumber.ResetValue = 1
            Me._armLayer1.OutputLineNumber.ResetValue = 2
            Me._armLayer1.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me._armLayer1.TimerSeconds.ResetValue = 0.001
            Me._armLayer1.TimerSeconds.PresetValue = 0.001

            ' Me._armLayer2.AutoDelay.ResetValue = False
            Me._armLayer2.Count.ResetValue = 1
            Me._armLayer2.Count.PresetValue = Integer.MaxValue
            Me._armLayer2.Delay.ResetValue = 0
            Me._armLayer2.Bypass.ResetValue = True
            Me._armLayer2.InputLineNumber.ResetValue = 1
            Me._armLayer2.OutputLineNumber.ResetValue = 2
            Me._armLayer2.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me._armLayer2.TimerSeconds.ResetValue = 0.001
            Me._armLayer2.TimerSeconds.PresetValue = 0.001

            ' Me.Calculate2.CompositeLimitsAutoClearEnabled.ResetValue = True
            ' Me.Calculate2.CompositeLimitsFailureBits.ResetValue = 15
            ' Me.Calculate2.FeedSource.ResetValue = isr.Scpi.FeedSource.Voltage
            ' Me.Calculate2.ImmediateBinning.ResetValue = True
            ' Me.Calculate2.gradingControlMode.ResetValue = True

            ' Me.Calculate3.ForcedDigitalOutputPatternEnabled.ResetValue = False
            ' Me.Calculate3.ForcedDigitalOutputPattern.ResetValue = 15

            ' Me.Format.Elements.ResetValue = isr.Scpi.ReadingElements.Reading Or isr.Scpi.ReadingElements.Units 
            '                                 Or isr.Scpi.ReadingElements.ReadingNumber Or isr.Scpi.ReadingElements.Timestamp

            ' Me.Output.FrontRouteTerminals.ResetValue = True
            ' Me.Output.IsOn.ResetValue = False
            ' Me.Output.OffMode.ResetValue = isr.Scpi.OutputOffMode.Normal

            ' Me.Route.FrontRouteTerminals.ResetValue = True

            ' Me.Sense.AverageCount.ResetValue = 10
            ' Me.Sense.AverageState.ResetValue = False
            ' Me.Sense.DeltaIntegrationPeriod.ResetValue = 5 / Me.SystemSubsystem.LineFrequency
            ' Me.Sense.DeltaVoltageRange.ResetValue = 120
            ' Me.Sense.DeltaVoltageAutoRange.ResetValue = True
            ' Me.Sense.FunctionMode.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC
            ' Me.Sense.FunctionsEnabled.ResetValue = isr.Scpi.SenseFunctionModes.CurrentDC
            ' Me.Sense.FunctionsDisabled.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC Or isr.Scpi.SenseFunctionModes.Resistance
            ' Me.Sense.IsFunctionConcurrent.ResetValue = True

            ' Me.Source.AutoClear.ResetValue = False
            ' Me.Source.AutoDelay.ResetValue = True
            ' Me.Source.Delay.ResetValue = 0
            ' Me.Source.FunctionMode.ResetValue = isr.Scpi.SourceFunctionMode.Voltage
            ' Me.Source.SweepPoints.ResetValue = 2500
            ' Me.Source.DeltaArm.ResetValue = False
            ' Me.Source.DeltaHighLevel.ResetValue = 0.001
            ' Me.Source.DeltaCount.ResetValue = Integer.MaxValue ' represents infinity.
            ' Me.Source.DeltaDelay.ResetValue = 0.002
            ' Me.Source.MemoryPoints.ResetValue = 1 ' not affected by reset.

            ' Me.SystemSubsystem.AutoZero.ResetValue = True
            ' Me.SystemSubsystem.BeeperEnabled.ResetValue = True
            ' Me.SystemSubsystem.CableGuard.ResetValue = True
            ' Me.SystemSubsystem.ContactCheckEnabled.ResetValue = False
            ' Me.SystemSubsystem.ContactCheckResistance.ResetValue = 50
            ' Me.SystemSubsystem.RemoteSenseMode.ResetValue = False

            Me.Trace.PointsCount.ResetValue = 100
            Me.Trace.FeedSource.ResetValue = isr.Scpi.FeedSource.Calculate1

            ' Me.Trigger.AutoDelay.ResetValue = False
            Me.Trigger.Count.ResetValue = 1
            Me.Trigger.Count.PresetValue = Integer.MaxValue ' infinity
            Me.Trigger.Delay.ResetValue = 0
            Me.Trigger.Bypass.ResetValue = True
            Me.Trigger.Bypass.PresetValue = False
            ' Me.Trigger.InputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.InputLineNumber.ResetValue = 1
            ' Me.Trigger.OutputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.OutputLineNumber.ResetValue = 2
            Me.Trigger.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me.Trigger.Source.PresetValue = isr.Scpi.ArmSource.Manual
            Me.Trigger.TimerSeconds.ResetValue = 0.001
            Me.Trigger.TimerSeconds.PresetValue = 0.001

            Return True

        End Function

        ''' <summary>
        ''' Sets subsystem to its default system preset values.
        ''' </summary>
        Public Overrides Function Preset() As Boolean

            MyBase.Preset()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            MyBase.ResetAndClear()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Function ResetKnownState() As Boolean

            MyBase.ResetKnownState()

            ' upgrade: Add RST value that are different than the default RST values set via isr.Scpi.

            ' Me.Sense.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)
            ' Me.Source.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)

            ' upgrade: set display properties and control values:

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

#End Region

#Region " ARM LAYERS "

        ''' <summary>
        ''' Adds the Arm Layers.
        ''' </summary>
        Private Sub addLayers()
            Me._armLayer1 = MyBase.Arm.AddLayer("LAY1")
            Me._armLayer2 = MyBase.Arm.AddLayer("LAY2")
        End Sub

        Private _armLayer1 As isr.Scpi.ArmLayer
        ''' <summary>
        ''' Gets reference to the ARM layers.
        ''' </summary>
        Public ReadOnly Property ArmLayer1() As isr.Scpi.ArmLayer
            Get
                Return Me._armLayer1
            End Get
        End Property

        Private _armLayer2 As isr.Scpi.ArmLayer
        ''' <summary>
        ''' Gets reference to the ARM layers.
        ''' </summary>
        Public ReadOnly Property ArmLayer2() As isr.Scpi.ArmLayer
            Get
                Return Me._armLayer2
            End Get
        End Property

#End Region

#Region " CUSTOM COMMANDS "

        ''' <summary>Opens all channels</summary>
        Public Sub OpenAll()
            Const routeOpenAllCommand As String = ":ROUT:OPEN ALL"
            isr.Scpi.RouteSubsystem.OpenAll(Me, routeOpenAllCommand)
        End Sub

#End Region

#Region " GUI PROPERTIES "

        ''' <summary>Sets the scan list and refreshes the display.</summary>
        ''' <param name="scanList"></param>
        Public Function SetScanList(ByVal scanList As String) As Boolean

            If MyBase.Route IsNot Nothing Then

                MyBase.ClearExecutionState()

                ' set the scan list
                MyBase.Route.ScanList(isr.Scpi.ResourceAccessLevels.None) = scanList

                If Me.Visible Then
                    Me.Gui.refreshDisplay()
                End If

            End If

            Return Not MyBase.HadError

        End Function

#End Region

#Region " IDENTITY / VERSION INFO "

        ''' <summary>
        ''' Queries the GPIB instrument and returns the string save the termination character.
        ''' </summary>
        Public Overrides Function ReadIdentity() As String

            If Me._versionInfo Is Nothing OrElse String.IsNullOrWhiteSpace(Me._versionInfo.Identity) Then

                Me._versionInfo = New K7000.VersionInfo(MyBase.ReadIdentity)

            End If

            Return Me._versionInfo.Identity

        End Function

        Private _versionInfo As K7000.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public Overloads ReadOnly Property VersionInfo() As K7000.VersionInfo
            Get
                Return Me._versionInfo
            End Get
        End Property

#End Region

#Region " LINE FREQUENCY "

        ''' <summary>Sets the system line frequency.
        ''' This instrument does not support the line frequency command.</summary>
        Public Function PresetLineFrequency() As Integer
            Static lineFrequency As Integer?
            If Not lineFrequency.HasValue Then
                lineFrequency = isr.Scpi.SystemSubsystem.LineFrequencyGetter
                Me.SystemSubsystem.LineFrequency() = lineFrequency.Value
            End If
        End Function

#End Region

#Region " REGISTERS "

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Return isr.Scpi.SystemSubsystem.ReadLastError(Me)
            End Get
        End Property

        ''' <summary>Returns true if the last operation has settled as signified by the status of the
        '''   operation condition settling flag.</summary>
        Public ReadOnly Property OperationSettled() As Boolean
            Get
                Return (MyBase.LastServiceEventArgs.OperationCondition And OperationEvents.Settling) = 0
            End Get
        End Property

        ''' <summary>Returns true if the operation is settling as signified by the status of the
        '''   operation condition settling flag.</summary>
        Public ReadOnly Property OperationSettling() As Boolean
            Get
                Return (MyBase.LastServiceEventArgs.OperationCondition And OperationEvents.Settling) <> 0
            End Get
        End Property

        ''' <summary>Enables or disables the operation service status registers requesting
        '''   a service request upon a negative transition.</summary>
        Public Sub ToggleEndOfScanService(ByVal isOn As Boolean)
            ToggleEndOfScanService(isOn, isr.Scpi.ServiceRequests.All And (Not isr.Scpi.ServiceRequests.MessageAvailable))
        End Sub

        ''' <summary>Enables or disables the operation service status registers requesting
        '''   a service request upon a negative transition.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref=" isr.Scpi.ServiceRequests">service request flags</see></param>
        Public Sub ToggleEndOfScanService(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Scpi.ServiceRequests)
            MyBase.ClearExecutionState()
            If turnOn Then
                isr.Scpi.StatusSubsystem.EnabledOperationNegativeTransitions(Me) = OperationTransitionEvents.Settling
                isr.Scpi.StatusSubsystem.EnabledOperationPositiveTransitions(Me) = OperationTransitionEvents.None
                isr.Scpi.StatusSubsystem.EnabledOperationEvents(Me) = OperationEvents.Settling
            Else
                isr.Scpi.StatusSubsystem.EnabledOperationNegativeTransitions(Me) = OperationTransitionEvents.None
                isr.Scpi.StatusSubsystem.EnabledOperationPositiveTransitions(Me) = OperationTransitionEvents.None
                isr.Scpi.StatusSubsystem.EnabledOperationEvents(Me) = OperationEvents.None
            End If
            ToggleServiceRequest(turnOn, serviceRequestMask)
            If Me.Visible Then
                Me.Gui.ToggleEndServiceRequest(turnOn, serviceRequestMask)
            End If
        End Sub

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="isr.Scpi.ServiceRequests">service request flags</see></param>
        Public Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Scpi.ServiceRequests)
            MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = serviceRequestMask
            If Me.Visible Then
                Me.Gui.ToggleServiceRequest(turnOn, serviceRequestMask)
            End If
        End Sub

#End Region

#Region " SERVICE REQUEST HANDLER "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then

                ' check if we have an operation request
                If (e.OperationEventStatus And OperationEvents.Settling) <> 0 Then

                    ' read the operation register condition
                    e.OperationCondition = isr.Scpi.StatusSubsystem.OperationEventCondition(Me)

                    If Me.Gui.Visible Then
                        ' display the operation duration
                        '         MyBase.DisplayInfoProvider("New message available under the Messages tab")
                        If Me.OperationSettled Then
                            Me.OnMessageAvailable(TraceEventType.Information, "OPERATION SETTLED", "Operation settled in {0}ms", e.OperationElapsedTime.TotalMilliseconds)
                        ElseIf Me.OperationSettling Then
                            Me.OnMessageAvailable(TraceEventType.Information, "OPERATION SETTLING", "Operation started settling in {0}ms", e.OperationElapsedTime.TotalMilliseconds)
                        End If
                    End If
                End If

            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace