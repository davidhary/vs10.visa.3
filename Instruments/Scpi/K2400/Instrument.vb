Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ControlExtensions
Namespace K2400
    ''' <summary>
    ''' Implements a VISA interface to a Keithley 24XX instrument base on the
    ''' <see cref="Controller">controller</see> base instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/07/05" by="David" revision="1.0.2137.x">
    ''' Create from the K24xx User Control.
    ''' </history>
    Public Class Instrument
        Inherits isr.Visa.Instruments.Controller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle, NationalInstruments.VisaNS.HardwareInterfaceType.Gpib)

            MyBase.UsingDevices = True

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As Panel, ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._reading IsNot Nothing Then
                            Me._reading.Dispose()
                            Me._reading = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I DISPLAY "

        Private _gui As Panel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As Panel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " I Resettable "

        ''' <summary>
        ''' Clears the measurement status and event registers.
        ''' </summary>
        Public Overrides Function ClearExecutionState() As Boolean
            MyBase.ClearExecutionState()
            Me._measurementDone = False
            Return Not MyBase.HadError
        End Function

        ''' <summary>Initialize Resettable values, units, scales.
        ''' This is to be called once upon connecting but before reset and clear..</summary>
        Public Overrides Function InitializeExecutionState() As Boolean

            MyBase.InitializeExecutionState()

            ' instantiate the reading elements.
            Me._reading = New Readings(isr.Scpi.ReadingElements.Current Or isr.Scpi.ReadingElements.Voltage Or
                                    isr.Scpi.ReadingElements.Resistance Or isr.Scpi.ReadingElements.Timestamp Or
                                    isr.Scpi.ReadingElements.Status)

            ' add source and sense functions
            Me.addFunctions()

            ' add arm layers.
            Me.addLayers()

            ' set reset, clear, and preset values.
            ' Me.Arm.AutoDelay.ResetValue = False
            Me.ArmLayer1.Count.ResetValue = 1
            ' Me.ArmLayer1.Delay.ResetValue = 0
            Me.ArmLayer1.Bypass.ResetValue = True
            Me.ArmLayer1.InputLineNumber.ResetValue = 1
            Me.ArmLayer1.OutputLineNumber.ResetValue = 2
            Me.ArmLayer1.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me.ArmLayer1.TimerSeconds.ResetValue = 0.1

            Me.Calculate2.CompositeLimitsAutoClearEnabled.ResetValue = True
            Me.Calculate2.CompositeLimitsFailureBits.ResetValue = 15
            Me.Calculate2.FeedSource.ResetValue = isr.Scpi.FeedSource.Voltage
            Me.Calculate2.ImmediateBinning.ResetValue = True
            Me.Calculate2.GradingControlMode.ResetValue = True

            ' Me.Calculate3.ForcedDigitalOutputPatternEnabled.ResetValue = False
            ' Me.Calculate3.ForcedDigitalOutputPattern.ResetValue = 15

            Me.Format.Elements.ResetValue = isr.Scpi.ReadingElements.Voltage Or isr.Scpi.ReadingElements.Current Or
                                            isr.Scpi.ReadingElements.Resistance Or isr.Scpi.ReadingElements.Time Or
                                            isr.Scpi.ReadingElements.Status

            ' Me.Output.FrontRouteTerminals.ResetValue = True
            Me.Output.IsOn.ResetValue = False
            Me.Output.OffMode.ResetValue = isr.Scpi.OutputOffMode.Normal

            Me.Route.FrontRouteTerminals.ResetValue = True

            Me.Sense.AverageCount.ResetValue = 10
            Me.Sense.AverageState.ResetValue = False
            'Me.Sense.DeltaIntegrationPeriod.ResetValue = 5 / Me.SystemSubsystem.LineFrequency
            'Me.Sense.DeltaVoltageRange.ResetValue = 120
            'Me.Sense.DeltaVoltageAutoRange.ResetValue = True
            Me.Sense.FunctionMode.ResetValue = isr.Scpi.SenseFunctionModes.CurrentDC
            Me.Sense.FunctionsEnabled.ResetValue = isr.Scpi.SenseFunctionModes.CurrentDC
            Me.Sense.FunctionsDisabled.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC Or isr.Scpi.SenseFunctionModes.Resistance
            Me.Sense.IsFunctionConcurrent.ResetValue = True

            Me.Source.AutoClear.ResetValue = False
            Me.Source.AutoDelay.ResetValue = True
            Me.Source.Delay.ResetValue = 0
            Me.Source.FunctionMode.ResetValue = isr.Scpi.SourceFunctionMode.Voltage
            Me.Source.SweepPoints.ResetValue = 2500
            'Me.Source.DeltaArm.ResetValue = False
            'Me.Source.DeltaHighLevel.ResetValue = 0.001
            'Me.Source.DeltaCount.ResetValue = Integer.MaxValue ' represents infinity.
            'Me.Source.DeltaDelay.ResetValue = 0.002
            ' Me.Source.MemoryPoints.ResetValue = 1 ' not affected by reset.

            Me.SystemSubsystem.AutoZero.ResetValue = True
            Me.SystemSubsystem.BeeperEnabled.ResetValue = True
            Me.SystemSubsystem.CableGuard.ResetValue = True
            Me.SystemSubsystem.ContactCheckEnabled.ResetValue = False
            Me.SystemSubsystem.ContactCheckResistance.ResetValue = 50
            Me.SystemSubsystem.RemoteSenseMode.ResetValue = False

            Me.Trace.PointsCount.ResetValue = 0
            ' Me.Trace.FeedSource.ResetValue = isr.Scpi.FeedSource.Calculate1
            ' Me.Trace.NextFeedControl.ResetValue = False

            ' Me.Trigger.AutoDelay.ResetValue = False
            Me.Trigger.Count.ResetValue = 1
            Me.Trigger.Delay.ResetValue = 0
            Me.Trigger.Bypass.ResetValue = True
            Me.Trigger.InputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.InputLineNumber.ResetValue = 1
            Me.Trigger.OutputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.OutputLineNumber.ResetValue = 2
            Me.Trigger.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me.Trigger.TimerSeconds.ResetValue = 0.1

            Return True

        End Function

        ''' <summary>
        ''' Sets subsystem to its default system preset values.
        ''' </summary>
        Public Overrides Function Preset() As Boolean

            MyBase.Preset()

            ' clear the messages from the error queue
            isr.Scpi.SystemSubsystem.ClearErrorQueue(Me)

            ' initialize the memory
            isr.Scpi.SystemSubsystem.InitializeMemory(Me)

            ' set device to auto clear
            MyBase.Source.AutoClear(isr.Scpi.ResourceAccessLevels.None) = True

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function


        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            MyBase.ResetAndClear()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Function ResetKnownState() As Boolean

            MyBase.ResetKnownState()

            ' Me.Sense.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)
            ' Me.Source.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)

            ' upgrade: set display properties and control values:
            Me.SourceFunctionMode(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.Cache)
            Me.ConcurrentSense(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.IsFunctionConcurrent(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseAutoRange(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.AutoRange(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseIntegrationPeriod(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.IntegrationPeriod(isr.Scpi.ResourceAccessLevels.Cache)

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

#End Region

#Region " GUI PROPERTIES "

        ''' <summary>
        ''' Gets or sets the integration period of the current function.
        ''' </summary>
        Public Property SenseIntegrationPeriod(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            Get
                Return MyBase.Sense.IntegrationPeriod(access)
            End Get
            Set(ByVal value As Double)
                If value <> MyBase.Sense.IntegrationPeriod(access) Then
                    MyBase.Sense.IntegrationPeriod(access) = value
                    Me.Gui._integrationPeriodTextBox.SafeTextSetter(MyBase.Sense.ActiveFunction.IntegrationPeriod.DisplayCaption)
                End If
            End Set
        End Property

        ''' <summary>
        ''' Gets or set the sense auto range of the current function.
        ''' </summary>
        Public Property SenseAutoRange(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                Return MyBase.Sense.AutoRange(access)
            End Get
            Set(ByVal value As Boolean)
                If value <> Me.SenseAutoRange(access) Then
                    MyBase.Sense.AutoRange(access) = value
                    If Me.Visible AndAlso (Me.Gui._senseAutoRangeToggle.Checked <> value) Then
                        Me.Gui._senseAutoRangeToggle.SafeCheckedSetter(value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Gets or set the concurrent sense functionality.
        ''' </summary>
        Public Property ConcurrentSense(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                Return MyBase.Sense.IsFunctionConcurrent(access)
            End Get
            Set(ByVal value As Boolean)
                If value <> Me.ConcurrentSense(access) Then
                    MyBase.Sense.IsFunctionConcurrent(access) = value
                    If Me.Visible AndAlso (Me.Gui._concurrentToggle.Checked <> value) Then
                        Me.Gui._concurrentToggle.SafeCheckedSetter(value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the <see cref="isr.Scpi.SourceFunctionMode">source function mode</see>.</summary>
        ''' <remarks>This method is useful with instruments having a single sense function.</remarks>
        Public Property SourceFunctionMode(ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.SourceFunctionMode
            Get
                Return MyBase.Source.FunctionMode(access)
            End Get
            Set(ByVal value As isr.Scpi.SourceFunctionMode)
                If MyBase.Source.ActiveFunction.SenseModality <> value Then
                    MyBase.Source.ActivateFunction(value, access)
                    If Me.Visible AndAlso (Me.Gui.ActualFunctionMode <> value) Then
                        Me.Gui.ApplyFunctionMode(value)
                    End If
                End If
            End Set
        End Property

#End Region

#Region " IDENTITY / VERSION INFO "

        ''' <summary>
        ''' Queries the GPIB instrument and returns the string save the termination character.
        ''' </summary>
        Public Overrides Function ReadIdentity() As String

            If Me._versionInfo Is Nothing OrElse String.IsNullOrWhiteSpace(Me._versionInfo.Identity) Then

                Me._versionInfo = New K2400.VersionInfo(MyBase.ReadIdentity)

                If Me.VersionInfo.BoardRevisions.ContainsKey(InstrumentBoards.ContactCheck.ToString) Then
                    ' if we have a contact check board then check if we have options
                    Me.VersionInfo.Options = MyBase.QueryTrimEnd(isr.Scpi.Syntax.OptionsQueryCommand)
                End If

            End If

            Return Me._versionInfo.Identity

        End Function

        Private _versionInfo As K2400.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public Overloads ReadOnly Property VersionInfo() As K2400.VersionInfo
            Get
                Return Me._versionInfo
            End Get
        End Property

#End Region

#Region " READINGS "

        Private _measurementDone As Boolean
        ''' <summary>
        ''' Gets the measurement done state.  With a 2-point sweep we get two service requests both
        ''' with complete data in the buffer.  With this flag we process these only once.
        ''' </summary>
        Public ReadOnly Property MeasurementDone() As Boolean
            Get
                Return Me._measurementDone
            End Get
        End Property

        ''' <summary>Parses the latest data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        Public Sub ParseReadingElements()

            ' check if we have a fixed or multi mode data
            If (MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Voltage AndAlso
                MyBase.Source.SweepMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SweepMode.Fixed) OrElse
               (MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Current) Then

                ' parse the latest data
                ParseReadingElements(MyBase.Sense.LatestData())

            Else

                ' fetch the data
                Dim lastFetch As String = MyBase.Sense.FetchData()

                ' parse the results of all data sets.
                If MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Current Then

                    Me._reading.CurrentReading.ComplianceLimit = Me.CurrentSense.ProtectionLevel.Value.Value
                    Me._reading.VoltageReading.ComplianceLimit = Me.VoltageSense.ProtectionLevel.Value.Value
                    Me._readings = K2400.Readings.ParseMulti(Me._reading, lastFetch)

                ElseIf MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Voltage Then

                    Me._reading.CurrentReading.ComplianceLimit = Me.CurrentSense.ProtectionLevel.Value.Value
                    Me._reading.VoltageReading.ComplianceLimit = Me.VoltageSense.ProtectionLevel.Value.Value
                    Me._readings = K2400.Readings.ParseMulti(Me._reading, lastFetch)

                End If

                If Me._readings.Length > 0 Then
                    ' select the final set as the valid readings.
                    Me._reading = New Readings(Me._readings(Me._readings.Length - 1))
                Else
                    Me._reading.Reset()
                End If

            End If

        End Sub

        Public Function IsCurrentSource() As Boolean
            Return MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Current
        End Function

        Public Function IsVoltageSource() As Boolean
            Return MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SourceFunctionMode.Voltage
        End Function


        ''' <summary>Parses the provided data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        ''' <param name="data">Specifies the measurement text to parse into the new reading.</param>
        Public Sub ParseReadingElements(ByVal data As String)

            Me._reading.CurrentReading.ComplianceLimit = Me.CurrentSense.ProtectionLevel.Value.Value
            Me._reading.VoltageReading.ComplianceLimit = Me.VoltageSense.ProtectionLevel.Value.Value

            Me._reading.Parse(data)

            If Me.Visible Then
                If Me.Reading.StatusReading.IsHitCompliance Then

                    Me.OnMessageAvailable(TraceEventType.Information, "REAL COMPLIANCE", "Real Compliance.  Instrument sensed an output overflow of the measured value.")

                ElseIf Me.Reading.StatusReading.IsHitRangeCompliance Then

                    Me.OnMessageAvailable(TraceEventType.Information, "RANGE COMPLIANCE", "Range Compliance.  Instrument sensed an output overflow of the measured value.")

                ElseIf Me.IsCurrentSource AndAlso Me.Reading.CurrentReading.Outcome.HitLevelCompliance Then

                    Me.OnMessageAvailable(TraceEventType.Information, "CURRENT LEVEL COMPLIANCE", "Current Level Compliance.  Instrument sensed an output overflow of the measured value.")

                ElseIf Me.IsVoltageSource AndAlso Me.Reading.VoltageReading.Outcome.HitLevelCompliance Then

                    Me.OnMessageAvailable(TraceEventType.Information, "VOLTAGE LEVEL COMPLIANCE", "Voltage Level Compliance.  Instrument sensed an output overflow of the measured value.")

                Else

                    Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT PARSED DATA", "Parsed elements.")

                End If

                ' update the display.
                Me.Gui.DisplaySelectedReading()
            End If

        End Sub

        ''' <summary>Take a single measurement and parse the result.</summary>
        ''' <remarks>This read uses Output Auto Clear.</remarks>
        Public Sub Read()

            ' set device to auto clear
            MyBase.Source.AutoClear(isr.Scpi.ResourceAccessLevels.None) = True

            ' disable service request on measurements
            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.None

            ' clear the status register and prepare for the service request
            MyBase.ClearExecutionState()

            ' Take a reading and parse the results
            Me.ParseReadingElements(MyBase.Sense.Read())

        End Sub

        ''' <summary>
        ''' Gets or sets an array of readings.
        ''' </summary>
        Private _readings As Readings()

        ''' <summary>
        ''' Returns the readings.
        ''' </summary>
        Public Function Readings() As Readings()
            Return Me._readings
        End Function

        Private _reading As Readings
        ''' <summary>
        ''' Returns the readings from the instrument.</summary>
        Public Function Reading() As Readings
            Return Me._reading
        End Function

#End Region

#Region " REGISTERS "

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Return isr.Scpi.SystemSubsystem.ReadLastError(Me)
            End Get
        End Property

        ''' <summary>Sets the measurement service status registers to request a service
        '''   request upon measurement events.</summary>
        Public Sub RequestMeasurementService()

            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.All
            MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.ServiceRequests.All And
                                                                                      (Not isr.Scpi.ServiceRequests.MessageAvailable)

        End Sub

#End Region

#Region " ARM LAYERS "

        ''' <summary>
        ''' Adds the Arm Layers.
        ''' </summary>
        Private Sub addLayers()
            Me._armLayer1 = MyBase.Arm.AddLayer("LAY1")
        End Sub

        Private _armLayer1 As isr.Scpi.ArmLayer
        ''' <summary>
        ''' Gets reference to the ARM layers.
        ''' </summary>
        Public ReadOnly Property ArmLayer1() As isr.Scpi.ArmLayer
            Get
                Return Me._armLayer1
            End Get
        End Property


#End Region

#Region " isr.Scpi FUNCTIONS "

        ''' <summary>
        ''' Adds the source and sense functions.
        ''' </summary>
        Private Sub addFunctions()

            MyBase.Sense.SupportsMultiFunctions = True
            Me._supportedSenseFunctionModes = isr.Scpi.SenseFunctionModes.CurrentDC
            Me._currentSense = MyBase.Sense.AddFunction(isr.Scpi.Syntax.CurrentDC, isr.Scpi.SenseFunctionModes.CurrentDC)

            Me._currentSense.AutoRange.ResetValue = True
            Me._currentSense.IntegrationPeriod.ResetValue = 1 / Me._currentSense.LineFrequency
            Me._currentSense.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._currentSense.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            Me._currentSense.ProtectionLevel.ResetValue = 0.000105
            Me._currentSense.Range.ResetValue = 0.000105

            Me._supportedSenseFunctionModes = Me._supportedSenseFunctionModes Or isr.Scpi.SenseFunctionModes.VoltageDC
            Me._voltageSense = MyBase.Sense.AddFunction(isr.Scpi.Syntax.VoltDC, isr.Scpi.SenseFunctionModes.VoltageDC)

            Me._voltageSense.AutoRange.ResetValue = True
            Me._voltageSense.IntegrationPeriod.ResetValue = 1 / Me._voltageSense.LineFrequency
            Me._voltageSense.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._voltageSense.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            Me._voltageSense.ProtectionLevel.ResetValue = 21
            Me._voltageSense.Range.ResetValue = 21

            Me._supportedSenseFunctionModes = Me._supportedSenseFunctionModes Or isr.Scpi.SenseFunctionModes.Resistance
            Me._resistance = MyBase.Sense.AddFunction(isr.Scpi.Syntax.Resistance, isr.Scpi.SenseFunctionModes.Resistance)

            Me._resistance.AutoRange.ResetValue = True
            Me._resistance.IntegrationPeriod.ResetValue = 1 / Me._resistance.LineFrequency
            Me._resistance.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._resistance.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            Me._resistance.Range.ResetValue = 210000.0

            Me._supportedSourceFunctionModes = isr.Scpi.SourceFunctionMode.Current
            Me._currentSource = MyBase.Source.AddFunction(isr.Scpi.Syntax.Current, isr.Scpi.SourceFunctionMode.Current)

            Me._currentSource.AutoRange.ResetValue = True
            Me._currentSource.IntegrationPeriod.ResetValue = 1 / Me._currentSource.LineFrequency
            Me._currentSource.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._currentSource.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            Me._currentSource.Level.ResetValue = 0
            Me._currentSource.StartLevel.ResetValue = 0
            Me._currentSource.StopLevel.ResetValue = 0
            Me._currentSource.SweepMode.ResetValue = isr.Scpi.SweepMode.Fixed
            Me._currentSource.Range.ResetValue = 0.000105

            Me._supportedSourceFunctionModes = Me._supportedSourceFunctionModes Or isr.Scpi.SourceFunctionMode.Voltage
            Me._voltageSource = MyBase.Source.AddFunction(isr.Scpi.Syntax.Volt, isr.Scpi.SourceFunctionMode.Voltage)

            Me._voltageSource.AutoRange.ResetValue = True
            Me._voltageSource.IntegrationPeriod.ResetValue = 1 / Me._voltageSource.LineFrequency
            Me._voltageSource.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._voltageSource.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            ' Me._voltageSource.ProtectionLevel.ResetValue = 0.000105
            Me._voltageSource.ProtectionLevel.PresetValue = 40.0
            '_voltageSource.Compliance.ResetValue = 21
            Me._voltageSource.Level.ResetValue = 0
            Me._voltageSource.StartLevel.ResetValue = 0
            Me._voltageSource.StopLevel.ResetValue = 0
            Me._voltageSource.SweepMode.ResetValue = isr.Scpi.SweepMode.Fixed
            Me._voltageSource.Range.ResetValue = 21

        End Sub

        Private _currentSense As isr.Scpi.ScpiFunction
        Public ReadOnly Property CurrentSense() As isr.Scpi.ScpiFunction
            Get
                Return Me._currentSense
            End Get
        End Property

        Private _currentSource As isr.Scpi.ScpiFunction
        Public ReadOnly Property CurrentSource() As isr.Scpi.ScpiFunction
            Get
                Return Me._currentSource
            End Get
        End Property

        Private _resistance As isr.Scpi.ScpiFunction
        Public ReadOnly Property Resistance() As isr.Scpi.ScpiFunction
            Get
                Return Me._resistance
            End Get
        End Property

        Private _voltageSense As isr.Scpi.ScpiFunction
        Public ReadOnly Property VoltageSense() As isr.Scpi.ScpiFunction
            Get
                Return Me._voltageSense
            End Get
        End Property

        Private _voltageSource As isr.Scpi.ScpiFunction
        Public ReadOnly Property VoltageSource() As isr.Scpi.ScpiFunction
            Get
                Return Me._voltageSource
            End Get
        End Property

        Private _supportedSenseFunctionModes As isr.Scpi.SenseFunctionModes = isr.Scpi.SenseFunctionModes.None
        ''' <summary>
        ''' Gets or sets the supported sense functions.  This is a subset of the functions supported by the instrument.
        ''' </summary>
        Public Property SupportedSenseFunctionModes() As isr.Scpi.SenseFunctionModes
            Get
                Return Me._supportedSenseFunctionModes
            End Get
            Set(ByVal value As isr.Scpi.SenseFunctionModes)
                Me._supportedSenseFunctionModes = value
            End Set
        End Property

        Private _supportedSourceFunctionModes As isr.Scpi.SourceFunctionMode = isr.Scpi.SourceFunctionMode.None
        ''' <summary>
        ''' Gets or sets the supported source functions. 
        ''' This is a subset of the functions supported by the instrument.
        ''' </summary>
        Public Property SupportedSourceFunctionModes() As isr.Scpi.SourceFunctionMode
            Get
                Return Me._supportedSourceFunctionModes
            End Get
            Set(ByVal value As isr.Scpi.SourceFunctionMode)
                Me._supportedSourceFunctionModes = value
            End Set
        End Property

#End Region

#Region " SERVICE REQUEST HANDLER "

        ''' <summary>Updates the screen if we have a new measurement and raises the service
        '''   request event. New measurements are parsed including setting the threshold
        '''   levels for testing for level compliance.
        ''' Note that on a two point sweep we get to service request.  The first service request
        ''' Appears before the second reading is on screen but already has the values for both sweeps.
        ''' </summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs) ' System.EventArgs)

            If e Is Nothing Then Return
            If Not e.HasError Then

                If (e.MeasurementEventStatus And MeasurementEvents.ReadingAvailable) <> 0 Then
                    If Not Me._measurementDone Then
                        'Debug.WriteLine("M: " & e.MeasurementEventStatus & "; S: " & e.ServiceRequestStatus 
                        '  & "; O: " & e.OperationEventStatus & "; Q: " & e.QuestionableEventStatus 
                        '  & "; SRQ: " & e.StandardEventStatus & "; Done: " & e.OperationCompleted)
                        ' parse the results
                        Me.ParseReadingElements()
                        Me._measurementDone = True
                    End If
                End If

            End If

            ' raise the service request event
            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace