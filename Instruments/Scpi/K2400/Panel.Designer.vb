Namespace K2400

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class Panel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try

        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()

            Me.components = New System.ComponentModel.Container
            Me._tabControl = New System.Windows.Forms.TabControl
            Me._readingTabPage = New System.Windows.Forms.TabPage
            Me._terminalsToggle = New isr.Controls.PushButton
            Me._modalityComboBox = New System.Windows.Forms.ComboBox
            Me._readingLabel = New System.Windows.Forms.Label
            Me._readButton = New System.Windows.Forms.Button
            Me._InitiateButton = New System.Windows.Forms.Button
            Me._interfaceTabPage = New System.Windows.Forms.TabPage
            Me._interfaceClearButton = New System.Windows.Forms.Button
            Me._sourceTabPage = New System.Windows.Forms.TabPage
            Me._sourceDelayTextBox = New System.Windows.Forms.TextBox
            Me._sourceDelayTextBoxLabel = New System.Windows.Forms.Label
            Me._applySourceSettingButton = New System.Windows.Forms.Button
            Me._sourceLevelTextBoxLabel = New System.Windows.Forms.Label
            Me._sourceFunctionComboBox = New System.Windows.Forms.ComboBox
            Me._sourceLevelTextBox = New System.Windows.Forms.TextBox
            Me._sourceLimitTextBox = New System.Windows.Forms.TextBox
            Me._sourceLimitTextBoxLabel = New System.Windows.Forms.Label
            Me._sourceFunctionComboBoxLabel = New System.Windows.Forms.Label
            Me._senseTabPage = New System.Windows.Forms.TabPage
            Me._integrationPeriodTextBox = New System.Windows.Forms.TextBox
            Me._integrationTimeLabel = New System.Windows.Forms.Label
            Me._manualOhmsModeToggle = New System.Windows.Forms.CheckBox
            Me._fourWireSenseToggle = New System.Windows.Forms.CheckBox
            Me._allSenseFunctionsCheckBox = New System.Windows.Forms.CheckBox
            Me._senseAutoRangeToggle = New System.Windows.Forms.CheckBox
            Me._concurrentToggle = New System.Windows.Forms.CheckBox
            Me._applySenseSettingsButton = New System.Windows.Forms.Button
            Me._messagesTabPage = New System.Windows.Forms.TabPage
            Me._messagesBox = New isr.Controls.MessagesBox
            Me._tabControl.SuspendLayout()
            Me._readingTabPage.SuspendLayout()
            Me._interfaceTabPage.SuspendLayout()
            Me._sourceTabPage.SuspendLayout()
            Me._senseTabPage.SuspendLayout()
            Me._messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Clearable = True
            Me.Connector.Connectible = True
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            Me.Connector.Searchable = True
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'StatusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'IdentityPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            '_tabControl
            '
            Me._tabControl.Controls.Add(Me._readingTabPage)
            Me._tabControl.Controls.Add(Me._interfaceTabPage)
            Me._tabControl.Controls.Add(Me._sourceTabPage)
            Me._tabControl.Controls.Add(Me._senseTabPage)
            Me._tabControl.Controls.Add(Me._messagesTabPage)
            Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me._tabControl.Enabled = False
            Me._tabControl.ItemSize = New System.Drawing.Size(52, 18)
            Me._tabControl.Location = New System.Drawing.Point(0, 0)
            Me._tabControl.Multiline = True
            Me._tabControl.Name = "_tabControl"
            Me._tabControl.SelectedIndex = 0
            Me._tabControl.Size = New System.Drawing.Size(312, 233)
            Me._tabControl.TabIndex = 15
            '
            '_readingTabPage
            '
            Me._readingTabPage.Controls.Add(Me._terminalsToggle)
            Me._readingTabPage.Controls.Add(Me._modalityComboBox)
            Me._readingTabPage.Controls.Add(Me._readingLabel)
            Me._readingTabPage.Controls.Add(Me._readButton)
            Me._readingTabPage.Controls.Add(Me._InitiateButton)
            Me._readingTabPage.Location = New System.Drawing.Point(4, 22)
            Me._readingTabPage.Name = "_readingTabPage"
            Me._readingTabPage.Size = New System.Drawing.Size(304, 207)
            Me._readingTabPage.TabIndex = 0
            Me._readingTabPage.Text = "Reading"
            Me._readingTabPage.UseVisualStyleBackColor = True
            '
            '_terminalsToggle
            '
            Me._terminalsToggle.BackColor = System.Drawing.Color.Transparent
            Me._terminalsToggle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._terminalsToggle.CheckedCaption = "FRONT"
            Me._terminalsToggle.Caption = "REAR"
            Me._terminalsToggle.Location = New System.Drawing.Point(199, 147)
            Me._terminalsToggle.Name = "_terminalsToggle"
            Me._terminalsToggle.Size = New System.Drawing.Size(95, 48)
            Me._terminalsToggle.TabIndex = 3
            Me._terminalsToggle.ButtonText = "Rear/Front"
            Me._terminalsToggle.UncheckedCaption = "REAR"
            '
            '_modalityComboBox
            '
            Me._modalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._modalityComboBox.Location = New System.Drawing.Point(165, 56)
            Me._modalityComboBox.Name = "_modalityComboBox"
            Me._modalityComboBox.Size = New System.Drawing.Size(129, 21)
            Me._modalityComboBox.TabIndex = 1
            '
            '_readingLabel
            '
            Me._readingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me._readingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._readingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me._readingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._readingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me._readingLabel.Location = New System.Drawing.Point(8, 16)
            Me._readingLabel.Name = "_readingLabel"
            Me._readingLabel.Size = New System.Drawing.Size(288, 36)
            Me._readingLabel.TabIndex = 2
            Me._readingLabel.Text = "0.0000000 mV"
            Me._readingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_readButton
            '
            Me._readButton.Location = New System.Drawing.Point(8, 56)
            Me._readButton.Name = "_readButton"
            Me._readButton.Size = New System.Drawing.Size(50, 23)
            Me._readButton.TabIndex = 0
            Me._readButton.Text = "&Read"
            Me._readButton.UseVisualStyleBackColor = True
            '
            '_InitiateButton
            '
            Me._InitiateButton.Location = New System.Drawing.Point(64, 56)
            Me._InitiateButton.Name = "_InitiateButton"
            Me._InitiateButton.Size = New System.Drawing.Size(50, 23)
            Me._InitiateButton.TabIndex = 0
            Me._InitiateButton.Text = "&Initiate"
            Me._InitiateButton.UseVisualStyleBackColor = True
            '
            '_interfaceTabPage
            '
            Me._interfaceTabPage.Controls.Add(Me._interfaceClearButton)
            Me._interfaceTabPage.Location = New System.Drawing.Point(4, 22)
            Me._interfaceTabPage.Name = "_interfaceTabPage"
            Me._interfaceTabPage.Size = New System.Drawing.Size(304, 207)
            Me._interfaceTabPage.TabIndex = 2
            Me._interfaceTabPage.Text = "Interface"
            Me._interfaceTabPage.UseVisualStyleBackColor = True
            '
            '_interfaceClearButton
            '
            Me._interfaceClearButton.Location = New System.Drawing.Point(127, 100)
            Me._interfaceClearButton.Name = "_interfaceClearButton"
            Me._interfaceClearButton.Size = New System.Drawing.Size(50, 23)
            Me._interfaceClearButton.TabIndex = 0
            Me._interfaceClearButton.Text = "&Clear"
            Me._interfaceClearButton.UseVisualStyleBackColor = True
            '
            '_sourceTabPage
            '
            Me._sourceTabPage.Controls.Add(Me._sourceDelayTextBox)
            Me._sourceTabPage.Controls.Add(Me._sourceDelayTextBoxLabel)
            Me._sourceTabPage.Controls.Add(Me._applySourceSettingButton)
            Me._sourceTabPage.Controls.Add(Me._sourceLevelTextBoxLabel)
            Me._sourceTabPage.Controls.Add(Me._sourceFunctionComboBox)
            Me._sourceTabPage.Controls.Add(Me._sourceLevelTextBox)
            Me._sourceTabPage.Controls.Add(Me._sourceLimitTextBox)
            Me._sourceTabPage.Controls.Add(Me._sourceLimitTextBoxLabel)
            Me._sourceTabPage.Controls.Add(Me._sourceFunctionComboBoxLabel)
            Me._sourceTabPage.Location = New System.Drawing.Point(4, 22)
            Me._sourceTabPage.Name = "_sourceTabPage"
            Me._sourceTabPage.Size = New System.Drawing.Size(304, 207)
            Me._sourceTabPage.TabIndex = 1
            Me._sourceTabPage.Text = "Source"
            Me._sourceTabPage.UseVisualStyleBackColor = True
            '
            '_sourceDelayTextBox
            '
            Me._sourceDelayTextBox.Location = New System.Drawing.Point(80, 56)
            Me._sourceDelayTextBox.Name = "_sourceDelayTextBox"
            Me._sourceDelayTextBox.Size = New System.Drawing.Size(52, 20)
            Me._sourceDelayTextBox.TabIndex = 8
            '
            '_sourceDelayTextBoxLabel
            '
            Me._sourceDelayTextBoxLabel.Location = New System.Drawing.Point(16, 56)
            Me._sourceDelayTextBoxLabel.Name = "_sourceDelayTextBoxLabel"
            Me._sourceDelayTextBoxLabel.Size = New System.Drawing.Size(64, 16)
            Me._sourceDelayTextBoxLabel.TabIndex = 7
            Me._sourceDelayTextBoxLabel.Text = "Delay [s]: "
            Me._sourceDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_applySourceSettingButton
            '
            Me._applySourceSettingButton.Location = New System.Drawing.Point(224, 107)
            Me._applySourceSettingButton.Name = "_applySourceSettingButton"
            Me._applySourceSettingButton.Size = New System.Drawing.Size(50, 23)
            Me._applySourceSettingButton.TabIndex = 6
            Me._applySourceSettingButton.Text = "&Apply"
            Me._applySourceSettingButton.UseVisualStyleBackColor = True
            '
            '_sourceLevelTextBoxLabel
            '
            Me._sourceLevelTextBoxLabel.Location = New System.Drawing.Point(31, 92)
            Me._sourceLevelTextBoxLabel.Name = "_sourceLevelTextBoxLabel"
            Me._sourceLevelTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._sourceLevelTextBoxLabel.TabIndex = 0
            Me._sourceLevelTextBoxLabel.Text = "Level: "
            '
            '_sourceFunctionComboBox
            '
            Me._sourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._sourceFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
            Me._sourceFunctionComboBox.Location = New System.Drawing.Point(143, 108)
            Me._sourceFunctionComboBox.Name = "_sourceFunctionComboBox"
            Me._sourceFunctionComboBox.Size = New System.Drawing.Size(73, 21)
            Me._sourceFunctionComboBox.TabIndex = 5
            '
            '_sourceLevelTextBox
            '
            Me._sourceLevelTextBox.Location = New System.Drawing.Point(31, 108)
            Me._sourceLevelTextBox.Name = "_sourceLevelTextBox"
            Me._sourceLevelTextBox.Size = New System.Drawing.Size(52, 20)
            Me._sourceLevelTextBox.TabIndex = 1
            Me._sourceLevelTextBox.Text = "0.05"
            '
            '_sourceLimitTextBox
            '
            Me._sourceLimitTextBox.Location = New System.Drawing.Point(87, 108)
            Me._sourceLimitTextBox.Name = "_sourceLimitTextBox"
            Me._sourceLimitTextBox.Size = New System.Drawing.Size(52, 20)
            Me._sourceLimitTextBox.TabIndex = 3
            Me._sourceLimitTextBox.Text = "5.00"
            '
            '_sourceLimitTextBoxLabel
            '
            Me._sourceLimitTextBoxLabel.Location = New System.Drawing.Point(87, 92)
            Me._sourceLimitTextBoxLabel.Name = "_sourceLimitTextBoxLabel"
            Me._sourceLimitTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._sourceLimitTextBoxLabel.TabIndex = 2
            Me._sourceLimitTextBoxLabel.Text = "Limit: "
            '
            '_sourceFunctionComboBoxLabel
            '
            Me._sourceFunctionComboBoxLabel.Location = New System.Drawing.Point(143, 92)
            Me._sourceFunctionComboBoxLabel.Name = "_sourceFunctionComboBoxLabel"
            Me._sourceFunctionComboBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._sourceFunctionComboBoxLabel.TabIndex = 4
            Me._sourceFunctionComboBoxLabel.Text = "Function: "
            '
            '_senseTabPage
            '
            Me._senseTabPage.Controls.Add(Me._integrationPeriodTextBox)
            Me._senseTabPage.Controls.Add(Me._integrationTimeLabel)
            Me._senseTabPage.Controls.Add(Me._manualOhmsModeToggle)
            Me._senseTabPage.Controls.Add(Me._fourWireSenseToggle)
            Me._senseTabPage.Controls.Add(Me._allSenseFunctionsCheckBox)
            Me._senseTabPage.Controls.Add(Me._senseAutoRangeToggle)
            Me._senseTabPage.Controls.Add(Me._concurrentToggle)
            Me._senseTabPage.Controls.Add(Me._applySenseSettingsButton)
            Me._senseTabPage.Location = New System.Drawing.Point(4, 22)
            Me._senseTabPage.Name = "_senseTabPage"
            Me._senseTabPage.Size = New System.Drawing.Size(304, 207)
            Me._senseTabPage.TabIndex = 4
            Me._senseTabPage.Text = "Sense"
            Me._senseTabPage.UseVisualStyleBackColor = True
            '
            '_integrationPeriodTextBox
            '
            Me._integrationPeriodTextBox.Location = New System.Drawing.Point(48, 24)
            Me._integrationPeriodTextBox.Name = "_integrationPeriodTextBox"
            Me._integrationPeriodTextBox.Size = New System.Drawing.Size(52, 20)
            Me._integrationPeriodTextBox.TabIndex = 20
            Me._integrationPeriodTextBox.Text = "16"
            '
            '_integrationTimeLabel
            '
            Me._integrationTimeLabel.Location = New System.Drawing.Point(48, 8)
            Me._integrationTimeLabel.Name = "_integrationTimeLabel"
            Me._integrationTimeLabel.Size = New System.Drawing.Size(112, 16)
            Me._integrationTimeLabel.TabIndex = 19
            Me._integrationTimeLabel.Text = "Integration Period [ms] "
            '
            '_manualOhmsModeToggle
            '
            Me._manualOhmsModeToggle.Location = New System.Drawing.Point(48, 56)
            Me._manualOhmsModeToggle.Name = "_manualOhmsModeToggle"
            Me._manualOhmsModeToggle.Size = New System.Drawing.Size(160, 16)
            Me._manualOhmsModeToggle.TabIndex = 18
            Me._manualOhmsModeToggle.Text = "Manual Ohms Mode"
            '
            '_fourWireSenseToggle
            '
            Me._fourWireSenseToggle.Location = New System.Drawing.Point(48, 80)
            Me._fourWireSenseToggle.Name = "_fourWireSenseToggle"
            Me._fourWireSenseToggle.Size = New System.Drawing.Size(160, 16)
            Me._fourWireSenseToggle.TabIndex = 17
            Me._fourWireSenseToggle.Text = "Four-Wire Sense"
            '
            '_allSenseFunctionsCheckBox
            '
            Me._allSenseFunctionsCheckBox.Location = New System.Drawing.Point(48, 104)
            Me._allSenseFunctionsCheckBox.Name = "_allSenseFunctionsCheckBox"
            Me._allSenseFunctionsCheckBox.Size = New System.Drawing.Size(160, 16)
            Me._allSenseFunctionsCheckBox.TabIndex = 16
            Me._allSenseFunctionsCheckBox.Text = "All Functions On"
            '
            '_senseAutoRangeToggle
            '
            Me._senseAutoRangeToggle.Location = New System.Drawing.Point(48, 152)
            Me._senseAutoRangeToggle.Name = "_senseAutoRangeToggle"
            Me._senseAutoRangeToggle.Size = New System.Drawing.Size(88, 16)
            Me._senseAutoRangeToggle.TabIndex = 15
            Me._senseAutoRangeToggle.Text = "Auto Range"
            '
            '_concurrentToggle
            '
            Me._concurrentToggle.Location = New System.Drawing.Point(48, 128)
            Me._concurrentToggle.Name = "_concurrentToggle"
            Me._concurrentToggle.Size = New System.Drawing.Size(160, 16)
            Me._concurrentToggle.TabIndex = 14
            Me._concurrentToggle.Text = "Concurrent"
            '
            '_applySenseSettingsButton
            '
            Me._applySenseSettingsButton.Location = New System.Drawing.Point(232, 160)
            Me._applySenseSettingsButton.Name = "_applySenseSettingsButton"
            Me._applySenseSettingsButton.Size = New System.Drawing.Size(50, 23)
            Me._applySenseSettingsButton.TabIndex = 13
            Me._applySenseSettingsButton.Text = "&Apply"
            Me._applySenseSettingsButton.UseVisualStyleBackColor = True
            '
            '_messagesTabPage
            '
            Me._messagesTabPage.Controls.Add(Me._messagesBox)
            Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me._messagesTabPage.Name = "_messagesTabPage"
            Me._messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me._messagesTabPage.TabIndex = 3
            Me._messagesTabPage.Text = "Messages"
            Me._messagesTabPage.UseVisualStyleBackColor = True
            '
            '_messagesBox
            '
            Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
            Me._messagesBox.Delimiter = "; "
            Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
            Me._messagesBox.Location = New System.Drawing.Point(0, 0)
            Me._messagesBox.Multiline = True
            Me._messagesBox.Name = "_messagesBox"
            Me._messagesBox.PresetCount = 50
            Me._messagesBox.ReadOnly = True
            Me._messagesBox.ResetCount = 100
            Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._messagesBox.Size = New System.Drawing.Size(304, 207)
            Me._messagesBox.TabIndex = 0
            Me._messagesBox.TimeFormat = "HH:mm:ss.f"
            '
            'Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.BackColor = System.Drawing.Color.Transparent
            Me.Controls.Add(Me._tabControl)
            Me.Name = "Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me._tabControl, 0)
            Me._tabControl.ResumeLayout(False)
            Me._readingTabPage.ResumeLayout(False)
            Me._interfaceTabPage.ResumeLayout(False)
            Me._sourceTabPage.ResumeLayout(False)
            Me._sourceTabPage.PerformLayout()
            Me._senseTabPage.ResumeLayout(False)
            Me._senseTabPage.PerformLayout()
            Me._messagesTabPage.ResumeLayout(False)
            Me._messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents _tabControl As System.Windows.Forms.TabControl
        Friend WithEvents _readingTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _terminalsToggle As isr.Controls.PushButton
        Friend WithEvents _modalityComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _readingLabel As System.Windows.Forms.Label
        Friend WithEvents _readButton As System.Windows.Forms.Button
        Friend WithEvents _InitiateButton As System.Windows.Forms.Button
        Friend WithEvents _interfaceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _interfaceClearButton As System.Windows.Forms.Button
        Friend WithEvents _sourceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _sourceDelayTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _sourceDelayTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _applySourceSettingButton As System.Windows.Forms.Button
        Friend WithEvents _sourceLevelTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _sourceFunctionComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _sourceLevelTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _sourceLimitTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _sourceLimitTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _sourceFunctionComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _senseTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _integrationPeriodTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _integrationTimeLabel As System.Windows.Forms.Label
        Friend WithEvents _manualOhmsModeToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _fourWireSenseToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _allSenseFunctionsCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents _senseAutoRangeToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _concurrentToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _applySenseSettingsButton As System.Windows.Forms.Button
        Friend WithEvents _messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _messagesBox As isr.Controls.MessagesBox

    End Class

End Namespace
