Namespace K2400

    ''' <summary>
    ''' Holds a single set of 2400 source meter reading elements.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    ''' <history date="05/12/06" by="David" revision="1.0.2323.x">
    ''' Confine only to the 2400
    ''' </history>
    ''' <history date="04/09/08" by="David" revision="3.0.3021.x">
    ''' Upgrade using isr.Scpi.Readings
    ''' </history>
    Public Class Readings
        Inherits isr.Scpi.Readings(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="elements">Specifies the 
        ''' <see cref="isr.Scpi.ReadingElements">reading elements</see>
        ''' expected in the data strings.
        ''' </param>
        ''' <remarks>Reading elements are added in the order they are returned by
        ''' the instrument so as to automate parsing of these data.</remarks>
        Public Sub New(ByVal elements As isr.Scpi.ReadingElements)

            ' instantiate the base class
            MyBase.New()

            Me._elements = elements

            If (Me._elements And isr.Scpi.ReadingElements.Voltage) <> 0 Then
                Me._voltageReading = New isr.Scpi.MeasurandDouble()
                Me._voltageReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Voltage
                Me._voltageReading.SaveCaption.Units = Me._voltageReading.DisplayCaption.Units
                Me._voltageReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._voltageReading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._voltageReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._voltageReading, 13)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Current) <> 0 Then
                Me._currentReading = New isr.Scpi.MeasurandDouble()
                Me._currentReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
                Me._currentReading.SaveCaption.Units = Me._currentReading.DisplayCaption.Units
                Me._currentReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._currentReading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._currentReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._currentReading, 13)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Resistance) <> 0 Then
                Me._resistanceReading = New isr.Scpi.MeasurandDouble()
                Me._resistanceReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Resistance
                Me._resistanceReading.DisplayCaption.Units.ShortUnits = isr.Scpi.Units.Omega
                Me._resistanceReading.SaveCaption.Units = Me._resistanceReading.DisplayCaption.Units
                Me._resistanceReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._resistanceReading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._resistanceReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._resistanceReading, 13)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                Me._timeStamp = New isr.Scpi.ReadingDouble()
                Me._timeStamp.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Time
                Me._timeStamp.SaveCaption.Units = Me._timeStamp.DisplayCaption.Units
                MyBase.AddReading(Me._timeStamp, 13)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.Status) <> 0 Then

                Me._statusReading = New isr.Scpi.StatusReading
                Me._statusReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.None
                Me._statusReading.SaveCaption.Units = Me._statusReading.DisplayCaption.Units
                MyBase.AddReading(Me._statusReading, 13)

            End If

        End Sub

        ''' <summary>
        ''' Create a copy of the model.
        ''' </summary>
        Public Sub New(ByVal model As Readings)

            ' instantiate the base class
            MyBase.New()

            If model IsNot Nothing Then
                Me._elements = model._elements

                If (Me._elements And isr.Scpi.ReadingElements.Voltage) <> 0 Then
                    Me._voltageReading = New isr.Scpi.MeasurandDouble(model._voltageReading)
                    MyBase.AddReading(Me._voltageReading, 13)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Current) <> 0 Then
                    Me._currentReading = New isr.Scpi.MeasurandDouble(model._currentReading)
                    MyBase.AddReading(Me._currentReading, 13)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Resistance) <> 0 Then
                    Me._resistanceReading = New isr.Scpi.MeasurandDouble(model._resistanceReading)
                    MyBase.AddReading(Me._resistanceReading, 13)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                    Me._timeStamp = New isr.Scpi.ReadingDouble(model._timeStamp)
                    MyBase.AddReading(Me._timeStamp, 13)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.Status) <> 0 Then

                    Me._statusReading = New isr.Scpi.StatusReading(model._statusReading)
                    Me._statusReading.ComplianceBits = StatusWordBits.HitCompliance
                    Me._statusReading.Limit1Bits = StatusWordBits.LimitResultBit1
                    Me._statusReading.Limit2Bits = StatusWordBits.LimitResultBit2
                    Me._statusReading.RangeComplianceBits = StatusWordBits.HitRangeCompliance

                    MyBase.AddReading(Me._statusReading, 13)

                End If
            End If

        End Sub

        ''' <summary>
        ''' Clones this class.
        ''' </summary>
        Public Function Clone() As Readings
            Return New Readings(Me)
        End Function

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If Me._statusReading IsNot Nothing Then
                            Me._statusReading = Nothing
                        End If

                        If Me._resistanceReading IsNot Nothing Then
                            Me._resistanceReading = Nothing
                        End If

                        If Me._currentReading IsNot Nothing Then
                            Me._currentReading = Nothing
                        End If

                        If Me._voltageReading IsNot Nothing Then
                            Me._voltageReading = Nothing
                        End If

                        If Me._timeStamp IsNot Nothing Then
                            Me._timeStamp = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PARSE "

        ''' <summary>
        ''' Parse reading data into a readings array.
        ''' </summary>
        ''' <param name="baseReading">Specifies the base reading which includes the limits for all reading
        ''' elements.</param>
        ''' <param name="readingRecords">The reading records.</param>
        ''' <returns>Readings[][].</returns>
        ''' <exception cref="System.ArgumentNullException">readingRecords</exception>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Object is returned")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMulti(ByVal baseReading As Readings, ByVal readingRecords As String) As Readings()

            If readingRecords Is Nothing Then
                Throw New ArgumentNullException("readingRecords")
            ElseIf readingRecords.Length = 0 Then
                Dim r As Readings() = {}
                Return r
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If

            ' TO_DO: This assumes 5 elements per reading.
            '  Use Me._elements.Set to set the number of elements per reading.
            Dim readings As String() = readingRecords.Split(","c)
            If readings.Length < baseReading.ElementsCount Then
                Dim r As Readings() = {}
                Return r
            End If

            Dim readingsArray(readings.Length \ baseReading.ElementsCount - 1) As Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readings.Length - 1 Step baseReading.ElementsCount
                Dim reading As Readings = New Readings(baseReading)
                reading.Parse(readings, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

        ''' <summary>
        ''' Parse reading data into a collection.
        ''' </summary>
        ''' <param name="baseReading">The base reading.</param>
        ''' <param name="readingRecords">The reading records.</param>
        ''' <returns>Collections.ObjectModel.Collection{Readings}.</returns>
        ''' <exception cref="System.ArgumentNullException">readingRecords</exception>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMultiCollection(ByVal baseReading As Readings, ByVal readingRecords As String) As Collections.ObjectModel.Collection(Of Readings)

            If readingRecords Is Nothing Then
                Throw New ArgumentNullException("readingRecords")
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If
            Return New Collections.ObjectModel.Collection(Of Readings)(ParseMulti(baseReading, readingRecords))

        End Function

        ''' <summary>Parses the measured data.</summary>
        ''' <param name="firstElementIndex">Zero-based index of first element.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overrides Sub Parse(ByVal measuredData As String(), ByVal firstElementIndex As Integer)

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length < firstElementIndex + 5 Then
                ' indicate that we do not have a valid value
                Me.Reset()
                Return
            End If

            MyBase.Parse(measuredData, firstElementIndex)

            If Me._statusReading IsNot Nothing Then
                If Me._voltageReading IsNot Nothing Then
                    Me._voltageReading.Outcome.HitStatusCompliance = Me._statusReading.IsHitCompliance
                    Me._voltageReading.Outcome.HitRangeCompliance = Me._statusReading.IsHitRangeCompliance
                End If
                If Me._currentReading IsNot Nothing Then
                    Me._currentReading.Outcome.HitStatusCompliance = Me._statusReading.IsHitCompliance
                    Me._currentReading.Outcome.HitRangeCompliance = Me._statusReading.IsHitRangeCompliance
                End If
                If Me._resistanceReading IsNot Nothing Then
                    Me._resistanceReading.Outcome.HitStatusCompliance = Me._statusReading.IsHitCompliance
                    Me._resistanceReading.Outcome.HitRangeCompliance = Me._statusReading.IsHitRangeCompliance
                End If
            End If

        End Sub

#End Region

#Region " PROPERTIES "

        Private _elements As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Gets the reading elements.
        ''' </summary>
        Public ReadOnly Property Elements() As isr.Scpi.ReadingElements
            Get
                Return Me._elements
            End Get
        End Property

        Private _currentReading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the current <see cref="isr.Scpi.MeasurandDouble">reading</see>.</summary>
        Public Property CurrentReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._currentReading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._currentReading = value
            End Set
        End Property

        Private _resistanceReading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the resistance <see cref="isr.Scpi.MeasurandDouble">reading</see>.</summary>
        Public Property ResistanceReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._resistanceReading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._resistanceReading = value
            End Set
        End Property

        Private _statusReading As isr.Scpi.StatusReading
        ''' <summary>Gets or sets the status <see cref="isr.Scpi.StatusReading">reading</see>.</summary>
        Public Property StatusReading() As isr.Scpi.StatusReading
            Get
                Return Me._statusReading
            End Get
            Set(ByVal value As isr.Scpi.StatusReading)
                Me._statusReading = value
            End Set
        End Property

        Private _timeStamp As isr.Scpi.ReadingDouble
        ''' <summary>Gets or sets the timestamp <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property Timestamp() As isr.Scpi.ReadingDouble
            Get
                Return Me._timeStamp

            End Get
            Set(ByVal value As isr.Scpi.ReadingDouble)
                Me._timeStamp = value
            End Set
        End Property

        Private _voltageReading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the Voltage <see cref="isr.Scpi.MeasurandDouble">reading</see>.</summary>
        Public Property VoltageReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._voltageReading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._voltageReading = value
            End Set
        End Property

#End Region

    End Class

End Namespace

