Namespace K2400

    ''' <summary>
    ''' Parses and holds the instrument version information.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' derived from previous isr.Scpi Instrument implementation.
    ''' </history>
    Public Class VersionInfo
        Inherits isr.Scpi.VersionInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="identity">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        Public Sub New(ByVal identity As String)
            MyBase.New()
            MyBase.ParseInstrumentId(identity)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _firmwareLevelMajorRevision As String
        ''' <summary>Returns the instrument firmware level major revision name .</summary>
        Public ReadOnly Property FirmwareLevelMajorRevision() As String
            Get
                Return Me._firmwareLevelMajorRevision
            End Get
        End Property

        Private _firmwareLevelMinorRevision As String
        ''' <summary>Returns the instrument firmware level minor revision name .</summary>
        Public ReadOnly Property FirmwareLevelMinorRevision() As String
            Get
                Return Me._firmwareLevelMinorRevision
            End Get
        End Property

        Private _firmwareLevelDate As Date
        ''' <summary>Returns the instrument firmware level date.</summary>
        Public ReadOnly Property FirmwareLevelDate() As Date
            Get
                Return Me._firmwareLevelDate
            End Get
        End Property

        Private _boardRevisions As System.Collections.Specialized.StringDictionary
        ''' <summary>Returns the list of board revisions.</summary>
        Public ReadOnly Property BoardRevisions() As System.Collections.Specialized.StringDictionary
            Get
                Return Me._boardRevisions
            End Get
        End Property

        Private _options As String
        ''' <summary>Returns the instrument firmware options.</summary>
        Public Property Options() As String
            Get
                Return Me._options
            End Get
            Set(ByVal value As String)
                Me._options = value
                ' check if we have contact check selected
                Me._supportsContactCheck = False
                If Not String.IsNullOrWhiteSpace(Me._options) Then
                Else
                    Me._supportsContactCheck = Me._options.IndexOf("contact-check", StringComparison.OrdinalIgnoreCase) >= 0
                End If
            End Set
        End Property

        Private _supportsContactCheck As Boolean
        ''' <summary>Returns True if the instrument supports contact check.</summary>
        Public ReadOnly Property SupportsContactCheck() As Boolean
            Get
                Return Me._supportsContactCheck
            End Get
        End Property

#End Region

#Region " METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision with the following items:
        '''   <see cref="FirmwareLevelMajorRevision">firmware level major revision</see> 
        '''   <see cref="FirmwareLevelDate">date</see>
        '''   <see cref="FirmwareLevelMinorRevision">firmware level minor revision</see> 
        '''   <see cref="BoardRevisions">board revisions</see>
        '''   e.g., <c>C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.  
        '''   The last three letters separated by "/" indicate the board revisions 
        '''   (i.e. /Analog/Digital/Contact Check). Contact Check board revisions have the following features:<p>
        '''     Revisions A through C have only one resistance range.</p><p>
        '''     Revisions D and above have selectable resistance ranges.</p><p>
        '''   Using the *opt? command this method also checks if the instrument supports 
        '''   contact check.  The command returns 'CONTACT-CHECK' (sans quotes) if the 
        '''   instrument does.  In addition, this method sets the properties 
        '''   <see cref="Options"/>, <see cref="SupportsContactCheck"/></p></param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overrides Sub ParseFirmwareRevision(ByVal revision As String)

            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If

            ' get the revision sections
            Dim revisionSections() As String = revision.Split("/"c)

            ' Rev: C11; revision: Oct 10 1997 09:51:36
            Dim firmwareLevel As String = revisionSections(0).Trim
            Me._firmwareLevelMajorRevision = firmwareLevel.Split(" "c)(0)

            ' date string: Oct 10 1997 09:51:36
            If Not Date.TryParse(firmwareLevel.Substring(Me._firmwareLevelMajorRevision.Length - 1).Trim,
                             Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.AssumeLocal,
                             Me._firmwareLevelDate) Then
                Me._firmwareLevelDate = DateTime.MinValue
            End If
            ' Me._firmwareLevelDate = Date.Parse(firmwareLevel.Substring(Me._firmwareLevelMajorRevision.Length - 1).Trim, Globalization.CultureInfo.CurrentCulture)

            ' Minor revision: A02
            Me._firmwareLevelMinorRevision = revisionSections(1).Trim

            ' set board revisions collection
            Me._boardRevisions = New System.Collections.Specialized.StringDictionary

            ' clear the options
            Me._options = String.Empty
            Me._supportsContactCheck = False

            If revisionSections.Length > 2 Then
                Me._boardRevisions.Add(InstrumentBoards.Analog.ToString, revisionSections(2))
                If revisionSections.Length > 3 Then
                    Me._boardRevisions.Add(InstrumentBoards.Digital.ToString, revisionSections(3))
                    If revisionSections.Length > 4 Then
                        Me._boardRevisions.Add(InstrumentBoards.ContactCheck.ToString, revisionSections(4))
                    End If
                End If
            End If

        End Sub

#End Region

    End Class

End Namespace
