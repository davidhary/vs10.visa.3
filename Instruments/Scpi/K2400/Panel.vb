Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ControlExtensions
Imports isr.Core.ComboBoxExtensions
Namespace K2400

    ''' <summary>Provides a user interface for the Keithley 24XX instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.DisplayName("K2400 Panel"),
      System.ComponentModel.Description("Keithley 2400 Instrument Family - Windows Forms Custom Control"),
      System.Drawing.ToolboxBitmap(GetType(K2400.Panel))>
    Public Class Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New Instrument(Me, "K2400")

            Me._modalityComboBox.DataSource = Nothing
            Me._modalityComboBox.Items.Clear()
            Me._modalityComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(
                        GetType(isr.Scpi.ReadingElements),
                        isr.Scpi.ReadingElements.Current Or isr.Scpi.ReadingElements.Voltage Or
                        isr.Scpi.ReadingElements.Time Or isr.Scpi.ReadingElements.Resistance Or
                        isr.Scpi.ReadingElements.Status)

            If Me._modalityComboBox.Items.Count > 0 Then
                Me._modalityComboBox.SelectedIndex = 0
            End If
            Me._sourceFunctionComboBox.SelectedIndex = 0

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()

            ' instantiate the reference to the instrument
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                Me._messagesBox.PrependMessage(e.ExtendedMessage)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

        ''' <summary>Update the display of the source settings.</summary>
        Private Sub refreshDisplay()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If

            Me._integrationPeriodTextBox.SafeTextSetter(Me.Instrument.Sense.ActiveFunction.IntegrationPeriod.DisplayCaption)
            Me._fourWireSenseToggle.SafeCheckedSetter(Me.Instrument.SystemSubsystem.RemoteSenseMode(isr.Scpi.ResourceAccessLevels.Cache))
            Me._allSenseFunctionsCheckBox.SafeCheckedSetter((Me.Instrument.Sense.FunctionsEnabled(isr.Scpi.ResourceAccessLevels.Cache) And
                                                             (isr.Scpi.SenseFunctionModes.CurrentDC Or isr.Scpi.SenseFunctionModes.VoltageDC)) =
                                                               (isr.Scpi.SenseFunctionModes.CurrentDC Or isr.Scpi.SenseFunctionModes.VoltageDC))
            Me._concurrentToggle.SafeCheckedSetter(Me.Instrument.Sense.IsFunctionConcurrent(isr.Scpi.ResourceAccessLevels.Cache))
            Me._senseAutoRangeToggle.SafeCheckedSetter(Me.Instrument.Sense.AutoRange(Me.Instrument.VoltageSense, isr.Scpi.ResourceAccessLevels.Cache))

            Select Case Me._sourceFunctionComboBox.Text
                Case "I"
                    Me._sourceLevelTextBox.SafeTextSetter(Me.Instrument.Source.Level(Me.Instrument.CurrentSource, isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                    Me._sourceLimitTextBox.SafeTextSetter(Me.Instrument.Sense.ProtectionLevel(Me.Instrument.VoltageSense, isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                Case "V"
                    Me._sourceLevelTextBox.SafeTextSetter(Me.Instrument.Source.Level(Me.Instrument.VoltageSource, isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                    Me._sourceLimitTextBox.SafeTextSetter(Me.Instrument.Sense.ProtectionLevel(Me.Instrument.CurrentSense, isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            End Select

            Dim valueDouble As Double

            ' get the delay time
            If Double.TryParse(Me._sourceDelayTextBox.Text, Globalization.NumberStyles.Float,
                              Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                Me.Instrument.Source.Delay(isr.Scpi.ResourceAccessLevels.Cache) = valueDouble
            Else
                Me.Instrument.Source.AutoDelay(isr.Scpi.ResourceAccessLevels.Cache) = True
            End If

        End Sub

#End Region

#Region " I CONNECT "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me._tabControl.Enabled = value
                If value Then
                    populateControls()
                    refreshDisplay()
                End If
            End Set
        End Property

        ''' <summary>List reading elements and enables source functions.
        ''' </summary>
        Private Sub populateControls()

            Me._modalityComboBox.DataSource = Nothing
            Me._modalityComboBox.Items.Clear()
            Me._modalityComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(
                          GetType(isr.Scpi.ReadingElements),
                          isr.Scpi.ReadingElements.Current Or isr.Scpi.ReadingElements.Voltage Or
                          isr.Scpi.ReadingElements.Time Or isr.Scpi.ReadingElements.Resistance Or
                          isr.Scpi.ReadingElements.Status)
            Me._modalityComboBox.DisplayMember = "Value"
            Me._modalityComboBox.ValueMember = "Key"

            If Me._modalityComboBox.Items.Count > 0 Then
                Me._modalityComboBox.SelectedIndex = 0
            End If
            Me._sourceFunctionComboBox.SelectedIndex = 0

        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As Instrument
        ''' <summary>Gets a reference to the underlying instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As Instrument)
                If value IsNot Nothing Then
                    Me._instrument = value
                    MyBase.ConnectableResource = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE SETTINGS: MODALITY "

        Private _actualModality As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Selects a new modality.
        ''' </summary>
        Friend Function ApplyModality(ByVal value As isr.Scpi.ReadingElements) As isr.Scpi.ReadingElements
            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected AndAlso
                (value <> isr.Scpi.ReadingElements.None) AndAlso (value <> Me._actualModality) Then
                Me._actualModality = value
                Me._modalityComboBox.SafeSelectItem(isr.Core.EnumExtensions.ValueDescriptionPair(value))
            End If
            Return Me._actualModality
        End Function

        ''' <summary>
        ''' Gets or sets the selected modality.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Private ReadOnly Property selectedModality() As isr.Scpi.ReadingElements
            Get
                Return CType(CType(Me._modalityComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, isr.Scpi.ReadingElements)
            End Get
        End Property

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

        Private _actualFunctionMode As isr.Scpi.SourceFunctionMode
        ''' <summary>
        ''' Return the last Source function mode.  This is set only after the 
        ''' actual value is applies whereas the <see cref="SelectedFunctionMode">selected mode</see>
        ''' reflects the status of the combo box.
        ''' </summary>
        Friend ReadOnly Property ActualFunctionMode() As isr.Scpi.SourceFunctionMode
            Get
                Return Me._actualFunctionMode
            End Get
        End Property

        ''' <summary>
        ''' Selects a new Source mode.
        ''' </summary>
        Friend Function ApplyFunctionMode(ByVal value As isr.Scpi.SourceFunctionMode) As isr.Scpi.SourceFunctionMode
            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected AndAlso
                ((Me._instrument.SupportedSenseFunctionModes And value) <> 0) AndAlso
                (value <> Me._actualFunctionMode) Then
                Me._instrument.SourceFunctionMode(isr.Scpi.ResourceAccessLevels.None) = value
                If Me._instrument.SourceFunctionMode(isr.Scpi.ResourceAccessLevels.Cache) = isr.Scpi.SourceFunctionMode.Current Then
                    Me._sourceLevelTextBox.SafeTextSetter(Me.Instrument.CurrentSource.Level.DisplayCaption)
                    Me._sourceLimitTextBox.SafeTextSetter(Me.Instrument.CurrentSource.ProtectionLevel.DisplayCaption)
                Else
                    Me._sourceLevelTextBox.SafeTextSetter(Me.Instrument.VoltageSource.Level.DisplayCaption)
                    Me._sourceLimitTextBox.SafeTextSetter(Me.Instrument.VoltageSource.ProtectionLevel.DisplayCaption)
                End If
                Me._integrationPeriodTextBox.SafeTextSetter("{0:0.##}", Me.Instrument.Source.ActiveFunction.IntegrationPeriod.DisplayCaption)
                Me._actualFunctionMode = value
                Me._sourceFunctionComboBox.SafeSelectItem(value, isr.Core.EnumExtensions.Description(value))
            End If
        End Function

        ''' <summary>
        ''' Gets or sets the selected function mode.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Private ReadOnly Property selectedFunctionMode() As isr.Scpi.SourceFunctionMode
            Get
                Return CType(CType(Me._sourceFunctionComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, isr.Scpi.SourceFunctionMode)
            End Get
        End Property

#End Region

#Region " BULK SETTINGS "

        ''' <summary>Applies the selected sense settings.</summary>
        Private Sub applySense()

            If Me.Instrument.IsConnected Then

                Me.Instrument.ClearExecutionState()

                ' get the Integration Period
                Me.Instrument.Sense.UpdateIntegrationPeriods(
                          Me.Instrument.CurrentSense.IntegrationPeriod.ParseScaledValue(Me._integrationPeriodTextBox.Text),
                          isr.Scpi.ResourceAccessLevels.None)

                If Me._manualOhmsModeToggle.Checked Then
                    Me.Instrument.WriteLine(":SENS:RES:MODE MAN")
                Else
                    Me.Instrument.WriteLine(":SENS:RES:MODE AUTO")
                End If

                Me.Instrument.SystemSubsystem.RemoteSenseMode(isr.Scpi.ResourceAccessLevels.None) = Me._fourWireSenseToggle.Checked

                If Me._allSenseFunctionsCheckBox.Checked Then

                    Me.Instrument.Sense.FunctionsEnabled(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SenseFunctionModes.CurrentDC Or
                                                                                               isr.Scpi.SenseFunctionModes.Resistance Or
                                                                                               isr.Scpi.SenseFunctionModes.VoltageDC

                Else

                    Me.Instrument.Sense.FunctionsEnabled(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.SenseFunctionModes.CurrentDC

                End If

                Me.Instrument.Sense.IsFunctionConcurrent(isr.Scpi.ResourceAccessLevels.None) = Me._concurrentToggle.Checked

                If Me._concurrentToggle.Checked Then

                    Me.Instrument.Sense.UpdateAutoRanges(Me._senseAutoRangeToggle.Checked, isr.Scpi.ResourceAccessLevels.None)

                End If

            End If

            ' update with values from the instrument
            Me.Instrument.StoreResourceAccessLevel()
            Me.Instrument.AccessLevel = isr.Scpi.ResourceAccessLevels.Device
            Me.refreshDisplay()
            Me.Instrument.RestoreResourceAccessLevel()

        End Sub

        ''' <summary>
        ''' Applies the selected source level and limits.
        ''' </summary>
        Private Sub applySource()

            If Me.Instrument.IsConnected Then

                Me.Instrument.ClearExecutionState()

                Select Case Me._sourceFunctionComboBox.Text

                    Case "I"

                        ' activate the current source function and set the voltage protection level
                        Me.Instrument.Source.ActivateFunction(Me.Instrument.CurrentSource, isr.Scpi.ResourceAccessLevels.None)
                        Me.Instrument.Source.Level(isr.Scpi.ResourceAccessLevels.None) = Double.Parse(Me._sourceLevelTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                        Me.Instrument.Sense.ProtectionLevel(Me.Instrument.VoltageSense, isr.Scpi.ResourceAccessLevels.None) = Double.Parse(Me._sourceLimitTextBox.Text, Globalization.CultureInfo.CurrentCulture)

                    Case "V"

                        ' activate the voltage source function and set the current protection level
                        Me.Instrument.Source.ActivateFunction(Me.Instrument.VoltageSource, isr.Scpi.ResourceAccessLevels.None)
                        Me.Instrument.Source.Level(isr.Scpi.ResourceAccessLevels.None) = Double.Parse(Me._sourceLevelTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                        Me.Instrument.Sense.ProtectionLevel(Me.Instrument.CurrentSense, isr.Scpi.ResourceAccessLevels.None) = Double.Parse(Me._sourceLimitTextBox.Text, Globalization.CultureInfo.CurrentCulture)

                End Select

                If Me._sourceDelayTextBox.Text.Length > 0 AndAlso isr.Core.StringNumericExtensions.IsNumber(Me._sourceDelayTextBox.Text) Then

                    Me.Instrument.Source.Delay(isr.Scpi.ResourceAccessLevels.None) = Double.Parse(Me._sourceDelayTextBox.Text, Globalization.CultureInfo.CurrentCulture)

                ElseIf Me._sourceDelayTextBox.Text.Length = 0 Then

                    Me.Instrument.Source.AutoDelay(isr.Scpi.ResourceAccessLevels.None) = True

                End If

            End If

            ' update with values from the instrument
            Me.Instrument.StoreResourceAccessLevel()
            Me.Instrument.AccessLevel = isr.Scpi.ResourceAccessLevels.Device
            Me.refreshDisplay()
            Me.Instrument.RestoreResourceAccessLevel()

        End Sub

        ''' <summary>Update the display of the source settings.</summary>
        Private Sub refreshSourceSettings()

            Select Case Me._sourceFunctionComboBox.Text
                Case "I"
                    Me._sourceLevelTextBoxLabel.Text = "Level [A]"
                    TipsToolTip.SetToolTip(Me._sourceLevelTextBox, "Enter current level in Amperes")
                    Me._sourceLimitTextBoxLabel.Text = "Limit [V]"
                    TipsToolTip.SetToolTip(Me._sourceLimitTextBox, "Enter voltage limit in Volts")
                    If Me.Instrument.IsConnected Then
                        Me._sourceLevelTextBox.Text = Me.Instrument.Source.Level(Me.Instrument.CurrentSource, Me.Instrument.AccessLevel).ToString(Globalization.CultureInfo.CurrentCulture)
                        Me._sourceLimitTextBox.Text = Me.Instrument.Sense.ProtectionLevel(Me.Instrument.VoltageSense, Me.Instrument.AccessLevel).ToString(Globalization.CultureInfo.CurrentCulture)
                    End If
                Case "V"
                    Me._sourceLevelTextBoxLabel.Text = "Level [V]"
                    TipsToolTip.SetToolTip(Me._sourceLevelTextBox, "Enter voltage level in Volts")
                    Me._sourceLimitTextBoxLabel.Text = "Limit [A]"
                    TipsToolTip.SetToolTip(Me._sourceLimitTextBox, "Enter current limit in Amperes")
                    If Me.Instrument.IsConnected Then
                        Me._sourceLevelTextBox.Text = Me.Instrument.Source.Level(Me.Instrument.VoltageSource, Me.Instrument.AccessLevel).ToString(Globalization.CultureInfo.CurrentCulture)
                        Me._sourceLimitTextBox.Text = Me.Instrument.Sense.ProtectionLevel(Me.Instrument.CurrentSense, Me.Instrument.AccessLevel).ToString(Globalization.CultureInfo.CurrentCulture)
                    End If
            End Select

        End Sub

        ''' <summary>Displays a reading based on the selected reading to display.</summary>
        Public Sub DisplaySelectedReading()

            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.Sense IsNot Nothing AndAlso Me.Instrument.Reading IsNot Nothing Then

                Select Case Me.selectedModality
                    Case isr.Scpi.ReadingElements.Current
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.CurrentReading.ToString())
                    Case isr.Scpi.ReadingElements.Resistance
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.ResistanceReading.ToString())
                    Case isr.Scpi.ReadingElements.Status
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.StatusReading.ToString())
                    Case isr.Scpi.ReadingElements.Time
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Timestamp.ToString())
                    Case isr.Scpi.ReadingElements.Voltage
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.VoltageReading.ToString())
                End Select
            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub applySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _applySenseSettingsButton.Click
            applySense()
        End Sub

        Private Sub applySourceSettingButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _applySourceSettingButton.Click
            applySource()
        End Sub


        ''' <summary>Initiates a reading for retrieval by way of the service request event.</summary>
        Private Sub initButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitiateButton.Click

            ' clear execution state before enabling events
            Me.Instrument.ClearExecutionState()

            ' set the service request
            Me.Instrument.RequestMeasurementService()

            ' set output to auto clear
            Me.Instrument.Source.AutoClear(isr.Scpi.ResourceAccessLevels.None) = True

            ' trigger the initiation of the measurement letting the service request do the rest.
            Me.Instrument.ClearExecutionState()
            Me.Instrument.Trigger.Initiate()

        End Sub

        Private Sub interfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _interfaceClearButton.Click
            Me.Instrument.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary>
        ''' Selects a new reading to display.</summary>
        Private Sub modalityComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _modalityComboBox.SelectedIndexChanged

            If Me._modalityComboBox.Enabled AndAlso Me._modalityComboBox.SelectedIndex >= 0 AndAlso
                Not String.IsNullOrWhiteSpace(Me._modalityComboBox.Text) Then
                Me.DisplaySelectedReading()
            End If

        End Sub

        ''' <summary>Query the instrument for a reading.</summary>
        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _readButton.Click
            Me.Instrument.Read()
            Me.DisplaySelectedReading()
            ' check if we are in sense auto mode
            '      If MyBase.Sense.AutoCurrentRange Then
            '     Me.Instrument.OnMessageAvailable(TraceEventType.Information,"Sense in Auto Range")
            '    Else
            '     Me.Instrument.OnMessageAvailable(TraceEventType.Information,"Sense in Manual Range")
            '  End If
        End Sub

        Private Sub sourceFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sourceFunctionComboBox.SelectedIndexChanged
            refreshSourceSettings()
        End Sub

        ''' <summary>Toggles the terminals</summary>
        Private Sub terminalsToggleButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _terminalsToggle.CheckedChanged
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected AndAlso Me._terminalsToggle.Enabled Then
                Me.Instrument.Route.FrontRouteTerminals(isr.Scpi.ResourceAccessLevels.None) = Me._terminalsToggle.Checked
            End If
            ' Me._terminalsToggle.Text = My.MyLibrary.ImmediateIf(Me._terminalsToggle.Checked, "FRONT", "REAR")
        End Sub


#End Region

    End Class
End Namespace

