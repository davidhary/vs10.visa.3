#Region " EVENT FLAGS "

''' <summary>Enumerates the units corresponding to the readings.</summary>
<System.Flags()>
Public Enum ReadingUnits
    <ComponentModel.Description("Volts")> Volts = 1
    <ComponentModel.Description("Amperes")> Amperes = 2
    <ComponentModel.Description("Ohms")> Ohms = 4
    <ComponentModel.Description("Seconds")> Seconds = 8
    <ComponentModel.Description("Unity")> Unity = 16
End Enum

''' <summary>Boards included in the instrument.</summary>
<System.Flags()>
Public Enum InstrumentBoards
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Analog")> Analog = 1
    <ComponentModel.Description("Digital")> Digital = 2
    <ComponentModel.Description("Contact Check")> ContactCheck = 4
    <ComponentModel.Description("Display")> Display = 8
    <ComponentModel.Description("Firmware")> Firmware = 16
    <ComponentModel.Description("Led Display")> LedDisplay = 32
End Enum

#End Region

