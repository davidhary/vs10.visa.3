Namespace K6220

    ''' <summary>
    ''' Holds a single set of 6220 source meter reading elements.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class Readings
        Inherits isr.Scpi.Readings(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set its properties.</remarks>
        Public Sub New(ByVal elements As isr.Scpi.ReadingElements)

            ' instantiate the base class
            MyBase.New()

            Me._elements = elements
            If (Me._elements And isr.Scpi.ReadingElements.Reading) <> 0 Then
                Me._senseVoltage = New isr.Scpi.MeasurandDouble()
                Me._senseVoltage.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Voltage
                Me._senseVoltage.SaveCaption.Units = Me._senseVoltage.DisplayCaption.Units
                Me._senseVoltage.ComplianceLimit = isr.Scpi.Syntax.Infinity
                MyBase.AddReading(Me._senseVoltage, 15)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                Me._timeStamp = New isr.Scpi.ReadingDouble()
                Me._timeStamp.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Time
                Me._timeStamp.SaveCaption.Units = Me._timeStamp.DisplayCaption.Units
                MyBase.AddReading(Me._timeStamp, 6)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.Source) <> 0 Then

                Me._sourceCurrent = New isr.Scpi.ReadingDouble()
                Me._sourceCurrent.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
                Me._sourceCurrent.SaveCaption.Units = Me._sourceCurrent.DisplayCaption.Units
                MyBase.AddReading(Me._sourceCurrent, 11)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.ReadingNumber) <> 0 Then

                Me._readingNumber = New isr.Scpi.ReadingLong()
                Me._readingNumber.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.None
                Me._readingNumber.SaveCaption.Units = Me._readingNumber.DisplayCaption.Units
                MyBase.AddReading(Me._readingNumber, 6)

            End If

        End Sub

        ''' <summary>
        ''' Create a copy of the model.
        ''' </summary>
        Public Sub New(ByVal model As Readings)

            ' instantiate the base class
            MyBase.New()

            If model IsNot Nothing Then

                Me._elements = model._elements
                If (Me._elements And isr.Scpi.ReadingElements.Reading) <> 0 Then
                    Me._senseVoltage = New isr.Scpi.MeasurandDouble(model._senseVoltage)
                    MyBase.AddReading(Me._senseVoltage, 15)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                    Me._timeStamp = New isr.Scpi.ReadingDouble(model._timeStamp)
                    MyBase.AddReading(Me._timeStamp, 6)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.Source) <> 0 Then

                    Me._sourceCurrent = New isr.Scpi.ReadingDouble(model._sourceCurrent)
                    MyBase.AddReading(Me._sourceCurrent, 11)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.ReadingNumber) <> 0 Then

                    Me._readingNumber = New isr.Scpi.ReadingLong(model._readingNumber)
                    MyBase.AddReading(Me._readingNumber, 6)

                End If
            End If

        End Sub

        ''' <summary>
        ''' Clones this class.
        ''' </summary>
        Public Function Clone() As Readings
            Return New Readings(Me)
        End Function

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If Me._readingNumber IsNot Nothing Then
                            Me._readingNumber = Nothing
                        End If

                        If Me._sourceCurrent IsNot Nothing Then
                            Me._sourceCurrent = Nothing
                        End If

                        If Me._senseVoltage IsNot Nothing Then
                            Me._senseVoltage = Nothing
                        End If

                        If Me._timeStamp IsNot Nothing Then
                            Me._timeStamp = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " SHARED METHODS "

        ''' <summary>
        ''' Parse reading data into a readings array.
        ''' </summary>
        ''' <param name="baseReading">Specifies the base reading which includes the limits for all reading
        ''' elements.</param>
        ''' <param name="measuredData">The measured data.</param>
        ''' <returns>Readings[][].</returns>
        ''' <exception cref="System.ArgumentNullException">measuredData</exception>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Object is returned")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMulti(ByVal baseReading As Readings, ByVal measuredData As String) As Readings()

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length = 0 Then
                Dim r As Readings() = {}
                Return r
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If

            Dim values As String() = measuredData.Split(","c)
            If values.Length < baseReading.ElementsCount Then
                Dim r As Readings() = {}
                Return r
            End If

            Dim readingsArray(values.Length \ baseReading.ElementsCount - 1) As Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readingsArray.Length * baseReading.ElementsCount - 1 Step baseReading.ElementsCount
                Dim reading As New Readings(baseReading)
                reading.Parse(values, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

#End Region

#Region " PROPERTIES "

        Private _senseVoltage As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the measured voltage <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property SenseVoltage() As isr.Scpi.MeasurandDouble
            Get
                Return Me._senseVoltage
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._senseVoltage = value
            End Set
        End Property

        Private _elements As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Gets the reading elements.
        ''' </summary>
        Public ReadOnly Property Elements() As isr.Scpi.ReadingElements
            Get
                Return Me._elements
            End Get
        End Property

        Private _readingNumber As isr.Scpi.ReadingLong
        ''' <summary>Gets or sets the <see cref="isr.Scpi.Readinglong">reading number</see>.</summary>
        Public Property ReadingNumber() As isr.Scpi.ReadingLong
            Get
                Return Me._readingNumber
            End Get
            Set(ByVal value As isr.Scpi.ReadingLong)
                Me._readingNumber = value
            End Set
        End Property

        Private _sourceCurrent As isr.Scpi.ReadingDouble
        ''' <summary>Gets or sets the measured voltage <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property SourceCurrent() As isr.Scpi.ReadingDouble
            Get
                Return Me._sourceCurrent
            End Get
            Set(ByVal value As isr.Scpi.ReadingDouble)
                Me._sourceCurrent = value
            End Set
        End Property

        Private _timeStamp As isr.Scpi.ReadingDouble
        ''' <summary>Gets or sets the timestamp <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property Timestamp() As isr.Scpi.ReadingDouble
            Get
                Return Me._timeStamp

            End Get
            Set(ByVal value As isr.Scpi.ReadingDouble)
                Me._timeStamp = value
            End Set
        End Property

#End Region

    End Class

End Namespace