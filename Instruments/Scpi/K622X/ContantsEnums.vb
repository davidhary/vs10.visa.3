﻿Namespace K6220

#Region " TYPES "

    ''' <summary>Gets or sets the status byte flags of the measurement event register.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2217:DoNotMarkEnumsWithFlags")>
    <System.Flags()>
    Public Enum MeasurementEvents
        <ComponentModel.Description("None")> None = 0
        ''' <summary>
        '''  Set bit indicates that delta reading exceeds the selected range of the Model 2182A.
        ''' </summary>
        <ComponentModel.Description("Reading Overflow")> ReadingOverflow = 1
        ''' <summary>
        ''' Set bit indicates that the interlock is asserted indicating that the output can be turned on.
        ''' </summary>
        <ComponentModel.Description("Interlock Asserted")> InterlockAsserted = 2
        ''' <summary>
        ''' Set bit indicates that the internal temperature limit has been exceeded. Check that the cooling vents are 
        ''' clear and the heat sink is free of dust and dirt.
        ''' </summary>
        <ComponentModel.Description("Over Temperature")> OverTemperature = 4
        ''' <summary>
        '''  Set bit indicates that the Model 622x is in compliance.
        ''' </summary>
        <ComponentModel.Description("Compliance")> Compliance = 8
        <ComponentModel.Description("Not Used 1")> NotUsed1 = 16
        ''' <summary>
        ''' Set bit indicates that a delta reading has been taken and processed.
        ''' </summary>
        <ComponentModel.Description("Reading Available")> ReadingAvailable = 32
        ''' <summary>
        ''' Set bit indicates that the user-defined reading number has been stored in the buffer 
        ''' (see TRACe:Notify command in Section 6).
        ''' </summary>
        <ComponentModel.Description("Trace Notify Event")> TraceNotify = 64
        ''' <summary>
        ''' Set bit indicates that there are at least two readings stored in the buffer.
        ''' </summary>
        <ComponentModel.Description("Buffer Available")> BufferAvailable = 128
        ''' <summary>
        ''' Set bit indicates that the buffer if half full. Keep in mind that buffer size is set by the 
        ''' programmed number of sweep points or delta measurement points (cycles).
        ''' </summary>
        <ComponentModel.Description("Buffer Half Full")> BufferHalfFull = 256
        ''' <summary>
        ''' Set bit indicates that the buffer is full. Keep in mind that buffer size is set by the 
        ''' programmed number of sweep points or delta measurement points (cycles).
        ''' </summary>
        <ComponentModel.Description("Buffer Full")> BufferFull = 512
        <ComponentModel.Description("Buffer Quarter Full")> BufferQuarterFull = 4096
        <ComponentModel.Description("Buffer Three Quarters Full")> BufferThreeQuartersFull = 8192
        <ComponentModel.Description("All")> All = 16383
    End Enum

    ''' <summary>Enumerates the status byte flags of the operation event register.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2217:DoNotMarkEnumsWithFlags")>
    <System.Flags()>
    Public Enum OperationEvents
        <ComponentModel.Description("None")> None = 0
        <ComponentModel.Description("Calibrating")> Calibrating = 1
        ''' <summary>
        ''' Set bit indicates that a sweep, Delta, Differential Conductance, or a pulse sweep 
        ''' for Pulse Delta is finished
        ''' </summary>
        <ComponentModel.Description("Sweep Done")> SweepDone = 2
        ''' <summary>
        ''' Set bit indicates that a sweep, Delta, Differential Conductance, or a pulse sweep 
        ''' for Pulse Delta has been aborted.
        ''' </summary>
        <ComponentModel.Description("Sweep Aborted")> SweepAborted = 4
        ''' <summary>
        ''' Set bit indicates that a sweep, Delta, Differential Conductance, or a pulse sweep 
        ''' for Pulse Delta is running. Status code: +124 Device sweeping.
        ''' </summary>
        <ComponentModel.Description("Sweeping")> Sweeping = 8
        ''' <summary>
        ''' Set bit indicates that the Wave mode has been started.
        ''' </summary>
        <ComponentModel.Description("Wave Started")> WaveStarted = 16
        ''' <summary>
        ''' Set bit indicates that the Model 622x is in the trigger layer waiting for a trigger event to occur.
        ''' </summary>
        <ComponentModel.Description("Waiting For Trigger")> WaitingForTrigger = 32
        ''' <summary>
        '''  Set bit indicates that the Model 622x is in the arm layer waiting for an arm event to occur.
        ''' </summary>
        <ComponentModel.Description("Waiting For Arm")> WaitingForArm = 64
        ''' <summary>
        ''' Set bit indicates that the Wave mode has been aborted.
        ''' </summary>
        <ComponentModel.Description("Wave Stopped")> WaveStopped = 128
        ''' <summary>
        ''' Arming one of the delta tests will clear (0) this bit. A set bit indicates that 
        ''' the delta test has started and the filter is settled.
        ''' </summary>
        <ComponentModel.Description("Filter Settled")> FilterSettled = 256
        <ComponentModel.Description("Idle")> Idle = 1024
        ''' <summary>
        ''' Set bit indicates that one of the rS-232 errors has occurred:
        ''' </summary>
        <ComponentModel.Description("RS232 Error")> SerialError = 2048
    End Enum

    ''' <summary>Enumerates the status byte flags of the operation transition event register.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2217:DoNotMarkEnumsWithFlags")>
    <System.Flags()>
    Public Enum OperationTransitionEvents
        <ComponentModel.Description("None")> None = 0
        <ComponentModel.Description("Settling")> Settling = 2
        <ComponentModel.Description("Waiting For Trigger")> WaitingForTrigger = 32
        <ComponentModel.Description("Waiting For Arm")> WaitingForArm = 64
        <ComponentModel.Description("Idle ")> Idle = 1024
    End Enum

    ''' <summary>Enumerates the status byte flags of the questionable event register.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2217:DoNotMarkEnumsWithFlags")>
    <System.Flags()>
    Public Enum QuestionableEvents
        <ComponentModel.Description("None")> None = 0
        <ComponentModel.Description("Calibration Event")> CalibrationEvent = 256
        <ComponentModel.Description("Command Warning")> CommandWarning = 16384
        'All = 32767
    End Enum


#End Region

End Namespace