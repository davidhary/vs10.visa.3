Namespace K6220

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class Panel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try

        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me._tabControl = New System.Windows.Forms.TabControl
            Me._readingTabPage = New System.Windows.Forms.TabPage
            Me._readingNumberUpDownLabel = New System.Windows.Forms.Label
            Me._readingNumberUpDown = New System.Windows.Forms.NumericUpDown
            Me._modalityComboBox = New System.Windows.Forms.ComboBox
            Me._readingLabel = New System.Windows.Forms.Label
            Me._abortButton = New System.Windows.Forms.Button
            Me._readButton = New System.Windows.Forms.Button
            Me._InitiateButton = New System.Windows.Forms.Button
            Me._interfaceTabPage = New System.Windows.Forms.TabPage
            Me._resetButton = New System.Windows.Forms.Button
            Me._interfaceClearButton = New System.Windows.Forms.Button
            Me._deltaTabPage = New System.Windows.Forms.TabPage
            Me._averageCountTextBox = New System.Windows.Forms.TextBox
            Me._averageCountTextBoxLabel = New System.Windows.Forms.Label
            Me._integrationPeriodTextBox = New System.Windows.Forms.TextBox
            Me._integrationPeriodTextBoxLabel = New System.Windows.Forms.Label
            Me._readSourceToggle = New System.Windows.Forms.CheckBox
            Me._deltaAutoRangeToggle = New System.Windows.Forms.CheckBox
            Me._deltaHighLevelTextBox = New System.Windows.Forms.TextBox
            Me._deltaDelayTextBox = New System.Windows.Forms.TextBox
            Me._sourceDelayTextBoxLabel = New System.Windows.Forms.Label
            Me._RestoreDeltaSettingsButton = New System.Windows.Forms.Button
            Me._applyDeltaSettingButton = New System.Windows.Forms.Button
            Me._sourceLevelTextBoxLabel = New System.Windows.Forms.Label
            Me._deltaCountTextBox = New System.Windows.Forms.TextBox
            Me._deltaCountTextBoxLabel = New System.Windows.Forms.Label
            Me._deltaVoltageLimitTextBox = New System.Windows.Forms.TextBox
            Me._sourceLimitTextBoxLabel = New System.Windows.Forms.Label
            Me._senseTabPage = New System.Windows.Forms.TabPage
            Me._digitalOutputUpDown = New System.Windows.Forms.NumericUpDown
            Me._digitalOutputUpDownLabel = New System.Windows.Forms.Label
            Me._messagesTabPage = New System.Windows.Forms.TabPage
            Me._messagesBox = New isr.Controls.MessagesBox
            Me._2182BufferTextBox = New System.Windows.Forms.TextBox
            Me._Flash2182BufferButton = New System.Windows.Forms.Button
            Me._tabControl.SuspendLayout()
            Me._readingTabPage.SuspendLayout()
            CType(Me._readingNumberUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            Me._interfaceTabPage.SuspendLayout()
            Me._deltaTabPage.SuspendLayout()
            Me._senseTabPage.SuspendLayout()
            CType(Me._digitalOutputUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            Me._messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            Me.Connector.Searchable = True
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'StatusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'IdentityPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            '_tabControl
            '
            Me._tabControl.Controls.Add(Me._readingTabPage)
            Me._tabControl.Controls.Add(Me._interfaceTabPage)
            Me._tabControl.Controls.Add(Me._deltaTabPage)
            Me._tabControl.Controls.Add(Me._senseTabPage)
            Me._tabControl.Controls.Add(Me._messagesTabPage)
            Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me._tabControl.Enabled = False
            Me._tabControl.ItemSize = New System.Drawing.Size(52, 18)
            Me._tabControl.Location = New System.Drawing.Point(0, 0)
            Me._tabControl.Multiline = True
            Me._tabControl.Name = "_tabControl"
            Me._tabControl.SelectedIndex = 0
            Me._tabControl.Size = New System.Drawing.Size(312, 233)
            Me._tabControl.TabIndex = 15
            '
            '_readingTabPage
            '
            Me._readingTabPage.Controls.Add(Me._readingNumberUpDownLabel)
            Me._readingTabPage.Controls.Add(Me._readingNumberUpDown)
            Me._readingTabPage.Controls.Add(Me._modalityComboBox)
            Me._readingTabPage.Controls.Add(Me._readingLabel)
            Me._readingTabPage.Controls.Add(Me._abortButton)
            Me._readingTabPage.Controls.Add(Me._readButton)
            Me._readingTabPage.Controls.Add(Me._InitiateButton)
            Me._readingTabPage.Location = New System.Drawing.Point(4, 22)
            Me._readingTabPage.Name = "_readingTabPage"
            Me._readingTabPage.Size = New System.Drawing.Size(304, 207)
            Me._readingTabPage.TabIndex = 0
            Me._readingTabPage.Text = "Reading"
            Me._readingTabPage.UseVisualStyleBackColor = True
            '
            '_readingNumberUpDownLabel
            '
            Me._readingNumberUpDownLabel.AutoSize = True
            Me._readingNumberUpDownLabel.Location = New System.Drawing.Point(181, 87)
            Me._readingNumberUpDownLabel.Name = "_readingNumberUpDownLabel"
            Me._readingNumberUpDownLabel.Size = New System.Drawing.Size(60, 13)
            Me._readingNumberUpDownLabel.TabIndex = 4
            Me._readingNumberUpDownLabel.Text = "Reading #:"
            Me._readingNumberUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_readingNumberUpDown
            '
            Me._readingNumberUpDown.Enabled = False
            Me._readingNumberUpDown.Location = New System.Drawing.Point(243, 83)
            Me._readingNumberUpDown.Name = "_readingNumberUpDown"
            Me._readingNumberUpDown.Size = New System.Drawing.Size(50, 20)
            Me._readingNumberUpDown.TabIndex = 5
            '
            '_modalityComboBox
            '
            Me._modalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._modalityComboBox.Location = New System.Drawing.Point(173, 56)
            Me._modalityComboBox.Name = "_modalityComboBox"
            Me._modalityComboBox.Size = New System.Drawing.Size(120, 21)
            Me._modalityComboBox.TabIndex = 3
            '
            '_readingLabel
            '
            Me._readingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me._readingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._readingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me._readingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._readingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me._readingLabel.Location = New System.Drawing.Point(8, 16)
            Me._readingLabel.Name = "_readingLabel"
            Me._readingLabel.Size = New System.Drawing.Size(288, 36)
            Me._readingLabel.TabIndex = 0
            Me._readingLabel.Text = "0.0000000 mV"
            Me._readingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_abortButton
            '
            Me._abortButton.Location = New System.Drawing.Point(243, 172)
            Me._abortButton.Name = "_abortButton"
            Me._abortButton.Size = New System.Drawing.Size(50, 23)
            Me._abortButton.TabIndex = 6
            Me._abortButton.Text = "&Abort"
            Me._abortButton.UseVisualStyleBackColor = True
            '
            '_readButton
            '
            Me._readButton.Location = New System.Drawing.Point(8, 56)
            Me._readButton.Name = "_readButton"
            Me._readButton.Size = New System.Drawing.Size(50, 23)
            Me._readButton.TabIndex = 1
            Me._readButton.Text = "&Read"
            Me._readButton.UseVisualStyleBackColor = True
            '
            '_InitiateButton
            '
            Me._InitiateButton.Location = New System.Drawing.Point(64, 56)
            Me._InitiateButton.Name = "_InitiateButton"
            Me._InitiateButton.Size = New System.Drawing.Size(50, 23)
            Me._InitiateButton.TabIndex = 2
            Me._InitiateButton.Text = "&Initiate"
            Me._InitiateButton.UseVisualStyleBackColor = True
            '
            '_interfaceTabPage
            '
            Me._interfaceTabPage.Controls.Add(Me._resetButton)
            Me._interfaceTabPage.Controls.Add(Me._interfaceClearButton)
            Me._interfaceTabPage.Location = New System.Drawing.Point(4, 22)
            Me._interfaceTabPage.Name = "_interfaceTabPage"
            Me._interfaceTabPage.Size = New System.Drawing.Size(304, 207)
            Me._interfaceTabPage.TabIndex = 2
            Me._interfaceTabPage.Text = "Interface"
            Me._interfaceTabPage.UseVisualStyleBackColor = True
            '
            '_resetButton
            '
            Me._resetButton.Location = New System.Drawing.Point(66, 100)
            Me._resetButton.Name = "_resetButton"
            Me._resetButton.Size = New System.Drawing.Size(156, 23)
            Me._resetButton.TabIndex = 2
            Me._resetButton.Text = "&Reset to Known State"
            Me._resetButton.UseVisualStyleBackColor = True
            '
            '_interfaceClearButton
            '
            Me._interfaceClearButton.Location = New System.Drawing.Point(97, 59)
            Me._interfaceClearButton.Name = "_interfaceClearButton"
            Me._interfaceClearButton.Size = New System.Drawing.Size(108, 23)
            Me._interfaceClearButton.TabIndex = 0
            Me._interfaceClearButton.Text = "Clear &Interface"
            Me._interfaceClearButton.UseVisualStyleBackColor = True
            '
            '_deltaTabPage
            '
            Me._deltaTabPage.Controls.Add(Me._averageCountTextBox)
            Me._deltaTabPage.Controls.Add(Me._averageCountTextBoxLabel)
            Me._deltaTabPage.Controls.Add(Me._integrationPeriodTextBox)
            Me._deltaTabPage.Controls.Add(Me._integrationPeriodTextBoxLabel)
            Me._deltaTabPage.Controls.Add(Me._readSourceToggle)
            Me._deltaTabPage.Controls.Add(Me._deltaAutoRangeToggle)
            Me._deltaTabPage.Controls.Add(Me._deltaHighLevelTextBox)
            Me._deltaTabPage.Controls.Add(Me._deltaDelayTextBox)
            Me._deltaTabPage.Controls.Add(Me._sourceDelayTextBoxLabel)
            Me._deltaTabPage.Controls.Add(Me._RestoreDeltaSettingsButton)
            Me._deltaTabPage.Controls.Add(Me._applyDeltaSettingButton)
            Me._deltaTabPage.Controls.Add(Me._sourceLevelTextBoxLabel)
            Me._deltaTabPage.Controls.Add(Me._deltaCountTextBox)
            Me._deltaTabPage.Controls.Add(Me._deltaCountTextBoxLabel)
            Me._deltaTabPage.Controls.Add(Me._deltaVoltageLimitTextBox)
            Me._deltaTabPage.Controls.Add(Me._sourceLimitTextBoxLabel)
            Me._deltaTabPage.Location = New System.Drawing.Point(4, 22)
            Me._deltaTabPage.Name = "_deltaTabPage"
            Me._deltaTabPage.Size = New System.Drawing.Size(304, 207)
            Me._deltaTabPage.TabIndex = 1
            Me._deltaTabPage.Text = "Delta"
            Me._deltaTabPage.UseVisualStyleBackColor = True
            '
            '_averageCountTextBox
            '
            Me._averageCountTextBox.Location = New System.Drawing.Point(152, 127)
            Me._averageCountTextBox.Name = "_averageCountTextBox"
            Me._averageCountTextBox.Size = New System.Drawing.Size(130, 20)
            Me._averageCountTextBox.TabIndex = 12
            Me._averageCountTextBox.Text = "10"
            '
            '_averageCountTextBoxLabel
            '
            Me._averageCountTextBoxLabel.Location = New System.Drawing.Point(1, 128)
            Me._averageCountTextBoxLabel.Name = "_averageCountTextBoxLabel"
            Me._averageCountTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._averageCountTextBoxLabel.TabIndex = 11
            Me._averageCountTextBoxLabel.Text = "Average Count:"
            Me._averageCountTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_integrationPeriodTextBox
            '
            Me._integrationPeriodTextBox.Location = New System.Drawing.Point(152, 104)
            Me._integrationPeriodTextBox.Name = "_integrationPeriodTextBox"
            Me._integrationPeriodTextBox.Size = New System.Drawing.Size(130, 20)
            Me._integrationPeriodTextBox.TabIndex = 10
            Me._integrationPeriodTextBox.Text = "16"
            '
            '_integrationPeriodTextBoxLabel
            '
            Me._integrationPeriodTextBoxLabel.Location = New System.Drawing.Point(1, 105)
            Me._integrationPeriodTextBoxLabel.Name = "_integrationPeriodTextBoxLabel"
            Me._integrationPeriodTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._integrationPeriodTextBoxLabel.TabIndex = 9
            Me._integrationPeriodTextBoxLabel.Text = "Integration Period [ms] :"
            Me._integrationPeriodTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_readSourceToggle
            '
            Me._readSourceToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
            Me._readSourceToggle.Location = New System.Drawing.Point(76, 157)
            Me._readSourceToggle.Name = "_readSourceToggle"
            Me._readSourceToggle.Size = New System.Drawing.Size(90, 16)
            Me._readSourceToggle.TabIndex = 13
            Me._readSourceToggle.Text = "Read Source"
            Me.TipsToolTip.SetToolTip(Me._readSourceToggle, "Check to have the instrument read the source level.")
            Me._readSourceToggle.UseVisualStyleBackColor = True
            '
            '_deltaAutoRangeToggle
            '
            Me._deltaAutoRangeToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
            Me._deltaAutoRangeToggle.Enabled = False
            Me._deltaAutoRangeToggle.Location = New System.Drawing.Point(81, 179)
            Me._deltaAutoRangeToggle.Name = "_deltaAutoRangeToggle"
            Me._deltaAutoRangeToggle.Size = New System.Drawing.Size(85, 16)
            Me._deltaAutoRangeToggle.TabIndex = 14
            Me._deltaAutoRangeToggle.Text = "Auto Range"
            Me.TipsToolTip.SetToolTip(Me._deltaAutoRangeToggle, "AUTO RANGE MUST BE OFF FOR DELTA")
            Me._deltaAutoRangeToggle.UseVisualStyleBackColor = True
            '
            '_deltaHighLevelTextBox
            '
            Me._deltaHighLevelTextBox.Location = New System.Drawing.Point(152, 12)
            Me._deltaHighLevelTextBox.Name = "_deltaHighLevelTextBox"
            Me._deltaHighLevelTextBox.Size = New System.Drawing.Size(130, 20)
            Me._deltaHighLevelTextBox.TabIndex = 2
            Me._deltaHighLevelTextBox.Text = "0.001"
            '
            '_deltaDelayTextBox
            '
            Me._deltaDelayTextBox.Location = New System.Drawing.Point(152, 81)
            Me._deltaDelayTextBox.Name = "_deltaDelayTextBox"
            Me._deltaDelayTextBox.Size = New System.Drawing.Size(130, 20)
            Me._deltaDelayTextBox.TabIndex = 8
            Me._deltaDelayTextBox.Text = "0.002"
            '
            '_sourceDelayTextBoxLabel
            '
            Me._sourceDelayTextBoxLabel.Location = New System.Drawing.Point(1, 82)
            Me._sourceDelayTextBoxLabel.Name = "_sourceDelayTextBoxLabel"
            Me._sourceDelayTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._sourceDelayTextBoxLabel.TabIndex = 7
            Me._sourceDelayTextBoxLabel.Text = "Delay [s]:"
            Me._sourceDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_RestoreDeltaSettingsButton
            '
            Me._RestoreDeltaSettingsButton.Location = New System.Drawing.Point(13, 167)
            Me._RestoreDeltaSettingsButton.Name = "_RestoreDeltaSettingsButton"
            Me._RestoreDeltaSettingsButton.Size = New System.Drawing.Size(60, 27)
            Me._RestoreDeltaSettingsButton.TabIndex = 15
            Me._RestoreDeltaSettingsButton.Text = "&Restore"
            Me.TipsToolTip.SetToolTip(Me._RestoreDeltaSettingsButton, "Reads back instrument settings")
            Me._RestoreDeltaSettingsButton.UseVisualStyleBackColor = True
            '
            '_applyDeltaSettingButton
            '
            Me._applyDeltaSettingButton.Location = New System.Drawing.Point(231, 168)
            Me._applyDeltaSettingButton.Name = "_applyDeltaSettingButton"
            Me._applyDeltaSettingButton.Size = New System.Drawing.Size(60, 27)
            Me._applyDeltaSettingButton.TabIndex = 0
            Me._applyDeltaSettingButton.Text = "&Apply"
            Me._applyDeltaSettingButton.UseVisualStyleBackColor = True
            '
            '_sourceLevelTextBoxLabel
            '
            Me._sourceLevelTextBoxLabel.Location = New System.Drawing.Point(1, 13)
            Me._sourceLevelTextBoxLabel.Name = "_sourceLevelTextBoxLabel"
            Me._sourceLevelTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._sourceLevelTextBoxLabel.TabIndex = 1
            Me._sourceLevelTextBoxLabel.Text = "Current Level [A]:"
            Me._sourceLevelTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_deltaCountTextBox
            '
            Me._deltaCountTextBox.Location = New System.Drawing.Point(152, 58)
            Me._deltaCountTextBox.Name = "_deltaCountTextBox"
            Me._deltaCountTextBox.Size = New System.Drawing.Size(130, 20)
            Me._deltaCountTextBox.TabIndex = 6
            Me._deltaCountTextBox.Text = "10"
            '
            '_deltaCountTextBoxLabel
            '
            Me._deltaCountTextBoxLabel.Location = New System.Drawing.Point(1, 59)
            Me._deltaCountTextBoxLabel.Name = "_deltaCountTextBoxLabel"
            Me._deltaCountTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._deltaCountTextBoxLabel.TabIndex = 5
            Me._deltaCountTextBoxLabel.Text = "Number of Measurements:"
            Me._deltaCountTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_deltaVoltageLimitTextBox
            '
            Me._deltaVoltageLimitTextBox.Location = New System.Drawing.Point(152, 35)
            Me._deltaVoltageLimitTextBox.Name = "_deltaVoltageLimitTextBox"
            Me._deltaVoltageLimitTextBox.Size = New System.Drawing.Size(130, 20)
            Me._deltaVoltageLimitTextBox.TabIndex = 4
            Me._deltaVoltageLimitTextBox.Text = "5.00"
            '
            '_sourceLimitTextBoxLabel
            '
            Me._sourceLimitTextBoxLabel.Location = New System.Drawing.Point(1, 36)
            Me._sourceLimitTextBoxLabel.Name = "_sourceLimitTextBoxLabel"
            Me._sourceLimitTextBoxLabel.Size = New System.Drawing.Size(148, 18)
            Me._sourceLimitTextBoxLabel.TabIndex = 3
            Me._sourceLimitTextBoxLabel.Text = "Voltage Limit [V]:"
            Me._sourceLimitTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_senseTabPage
            '
            Me._senseTabPage.Controls.Add(Me._Flash2182BufferButton)
            Me._senseTabPage.Controls.Add(Me._2182BufferTextBox)
            Me._senseTabPage.Controls.Add(Me._digitalOutputUpDown)
            Me._senseTabPage.Controls.Add(Me._digitalOutputUpDownLabel)
            Me._senseTabPage.Location = New System.Drawing.Point(4, 22)
            Me._senseTabPage.Name = "_senseTabPage"
            Me._senseTabPage.Size = New System.Drawing.Size(304, 207)
            Me._senseTabPage.TabIndex = 4
            Me._senseTabPage.Text = "Sense"
            Me._senseTabPage.UseVisualStyleBackColor = True
            '
            '_digitalOutputUpDown
            '
            Me._digitalOutputUpDown.Location = New System.Drawing.Point(117, 14)
            Me._digitalOutputUpDown.Maximum = New Decimal(New Integer() {15, 0, 0, 0})
            Me._digitalOutputUpDown.Name = "_digitalOutputUpDown"
            Me._digitalOutputUpDown.Size = New System.Drawing.Size(59, 20)
            Me._digitalOutputUpDown.TabIndex = 15
            Me._digitalOutputUpDown.Value = New Decimal(New Integer() {15, 0, 0, 0})
            '
            '_digitalOutputUpDownLabel
            '
            Me._digitalOutputUpDownLabel.Location = New System.Drawing.Point(15, 15)
            Me._digitalOutputUpDownLabel.Name = "_digitalOutputUpDownLabel"
            Me._digitalOutputUpDownLabel.Size = New System.Drawing.Size(100, 18)
            Me._digitalOutputUpDownLabel.TabIndex = 14
            Me._digitalOutputUpDownLabel.Text = "Digital Output: "
            Me._digitalOutputUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_messagesTabPage
            '
            Me._messagesTabPage.Controls.Add(Me._messagesBox)
            Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me._messagesTabPage.Name = "_messagesTabPage"
            Me._messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me._messagesTabPage.TabIndex = 3
            Me._messagesTabPage.Text = "Messages"
            Me._messagesTabPage.UseVisualStyleBackColor = True
            '
            '_messagesBox
            '
            Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
            Me._messagesBox.Delimiter = "; "
            Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
            Me._messagesBox.Location = New System.Drawing.Point(0, 0)
            Me._messagesBox.Multiline = True
            Me._messagesBox.Name = "_messagesBox"
            Me._messagesBox.PresetCount = 50
            Me._messagesBox.ReadOnly = True
            Me._messagesBox.ResetCount = 100
            Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._messagesBox.Size = New System.Drawing.Size(304, 207)
            Me._messagesBox.TabIndex = 0
            Me._messagesBox.TimeFormat = "HH:mm:ss.f"
            '
            '_2182BufferTextBox
            '
            Me._2182BufferTextBox.Location = New System.Drawing.Point(15, 72)
            Me._2182BufferTextBox.Multiline = True
            Me._2182BufferTextBox.Name = "_2182BufferTextBox"
            Me._2182BufferTextBox.Size = New System.Drawing.Size(270, 96)
            Me._2182BufferTextBox.TabIndex = 16
            '
            '_Flash2182BufferButton
            '
            Me._Flash2182BufferButton.Location = New System.Drawing.Point(15, 43)
            Me._Flash2182BufferButton.Name = "_Flash2182BufferButton"
            Me._Flash2182BufferButton.Size = New System.Drawing.Size(125, 23)
            Me._Flash2182BufferButton.TabIndex = 17
            Me._Flash2182BufferButton.Text = "FLUSH 2182"
            Me._Flash2182BufferButton.UseVisualStyleBackColor = True
            '
            'Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me._tabControl)
            Me.Name = "Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me._tabControl, 0)
            Me._tabControl.ResumeLayout(False)
            Me._readingTabPage.ResumeLayout(False)
            Me._readingTabPage.PerformLayout()
            CType(Me._readingNumberUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            Me._interfaceTabPage.ResumeLayout(False)
            Me._deltaTabPage.ResumeLayout(False)
            Me._deltaTabPage.PerformLayout()
            Me._senseTabPage.ResumeLayout(False)
            Me._senseTabPage.PerformLayout()
            CType(Me._digitalOutputUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            Me._messagesTabPage.ResumeLayout(False)
            Me._messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents _tabControl As System.Windows.Forms.TabControl
        Friend WithEvents _readingTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _modalityComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _readingLabel As System.Windows.Forms.Label
        Friend WithEvents _readButton As System.Windows.Forms.Button
        Friend WithEvents _InitiateButton As System.Windows.Forms.Button
        Friend WithEvents _interfaceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _interfaceClearButton As System.Windows.Forms.Button
        Friend WithEvents _deltaTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _deltaDelayTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _sourceDelayTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _applyDeltaSettingButton As System.Windows.Forms.Button
        Friend WithEvents _sourceLevelTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _deltaHighLevelTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _deltaVoltageLimitTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _sourceLimitTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _senseTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _messagesBox As isr.Controls.MessagesBox
        Friend WithEvents _deltaCountTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _deltaCountTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _averageCountTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _averageCountTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _integrationPeriodTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _integrationPeriodTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _deltaAutoRangeToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _resetButton As System.Windows.Forms.Button
        Friend WithEvents _digitalOutputUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents _digitalOutputUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents _abortButton As System.Windows.Forms.Button
        Friend WithEvents _readSourceToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _readingNumberUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents _readingNumberUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents _RestoreDeltaSettingsButton As System.Windows.Forms.Button
        Friend WithEvents _Flash2182BufferButton As System.Windows.Forms.Button
        Friend WithEvents _2182BufferTextBox As System.Windows.Forms.TextBox

    End Class

End Namespace
