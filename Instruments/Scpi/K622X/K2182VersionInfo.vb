Namespace K2182

    ''' <summary>
    ''' Parses and holds the instrument version information.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class VersionInfo
        Inherits isr.Scpi.VersionInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="identity">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2182,0669977/zzzzzz.</c>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2182A,1152808,C01  /A02  </c>
        ''' </param>
        Public Sub New(ByVal identity As String)
            MyBase.New()
            MyBase.ParseInstrumentId(identity)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _boardRevisions As System.Collections.Specialized.StringDictionary
        ''' <summary>Returns the list of board revisions.</summary>
        Public ReadOnly Property BoardRevisions() As System.Collections.Specialized.StringDictionary
            Get
                Return Me._boardRevisions
            End Get
        End Property

#End Region

#Region " METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument
        '''   <see cref="BoardRevisions">board revisions</see>
        '''   e.g., <c>yyyyy/zzzzz/w</c> for the digital, display, and firmware level, e.g., 
        '''   <c>C01 /A02</c>
        '''   </param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overrides Sub ParseFirmwareRevision(ByVal revision As String)

            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If

            ' get the revision sections
            Dim revisionSections() As String = revision.Split("/"c)

            ' set board revisions collection
            Me._boardRevisions = New System.Collections.Specialized.StringDictionary

            ' Rev: yyyyy/ZZZZZ/w
            If revisionSections.Length > 0 Then
                Me._boardRevisions.Add(InstrumentBoards.Digital.ToString, revisionSections(0).Trim)
                If revisionSections.Length > 1 Then
                    Me._boardRevisions.Add(InstrumentBoards.Display.ToString, revisionSections(1).Trim)
                    If revisionSections.Length > 2 Then
                        Me._boardRevisions.Add(InstrumentBoards.Firmware.ToString, revisionSections(2).Trim)
                    End If
                End If
            End If

        End Sub

#End Region

    End Class

End Namespace
