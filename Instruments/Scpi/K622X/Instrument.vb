Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ControlExtensions
Imports isr.Core.NumericUpDownExtensions
Imports isr.Core.EventHandlerExtensions
Namespace K6220

    ''' <summary>Implements a VISA interface to a Keithley 622X instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class Instrument

        ' based on the isr.Scpi instrument class
        Inherits isr.Visa.Instruments.Controller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)
            MyBase.New(resourceTitle, NationalInstruments.VisaNS.HardwareInterfaceType.Gpib)
            MyBase.UsingDevices = True
        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As Panel, ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        If BufferAvailableEvent IsNot Nothing Then
                            For Each d As [Delegate] In BufferAvailableEvent.GetInvocationList
                                RemoveHandler BufferAvailable, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If BufferFullEvent IsNot Nothing Then
                            For Each d As [Delegate] In BufferFullEvent.GetInvocationList
                                RemoveHandler BufferFull, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If DataAvailableEvent IsNot Nothing Then
                            For Each d As [Delegate] In DataAvailableEvent.GetInvocationList
                                RemoveHandler DataAvailable, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If MeasurementFailedEvent IsNot Nothing Then
                            For Each d As [Delegate] In MeasurementFailedEvent.GetInvocationList
                                RemoveHandler MeasurementFailed, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If ReadingAvailableEvent IsNot Nothing Then
                            For Each d As [Delegate] In ReadingAvailableEvent.GetInvocationList
                                RemoveHandler ReadingAvailable, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If SweepAbortedEvent IsNot Nothing Then
                            For Each d As [Delegate] In SweepAbortedEvent.GetInvocationList
                                RemoveHandler SweepAborted, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        If SweepDoneEvent IsNot Nothing Then
                            For Each d As [Delegate] In SweepDoneEvent.GetInvocationList
                                RemoveHandler SweepDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                            Next
                        End If

                        ' Free managed resources when explicitly called
                        If Me._reading IsNot Nothing Then
                            Me._reading.Dispose()
                            Me._reading = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I DISPLAY "

        Private _gui As Panel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As Panel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " I Resettable "

        ''' <summary>
        ''' Clears the measurement status and event registers.
        ''' </summary>
        Public Overrides Function ClearExecutionState() As Boolean

            Return MyBase.ClearExecutionState() AndAlso Me.RemoteClearExecutionState() AndAlso MyBase.OperationCompleted

        End Function

        ''' <summary>Initialize Resettable values, units, scales.
        ''' This is to be called once upon connecting but before reset and clear..</summary>
        Public Overrides Function InitializeExecutionState() As Boolean

            MyBase.InitializeExecutionState()

            ' set the line frequency.
            Me.PresetLineFrequency()

            ' instantiate the reading elements.
            Me._reading = New Readings(isr.Scpi.ReadingElements.Reading)

            ' add source and sense functions
            Me.addFunctions()

            ' add arm layers
            Me.addLayers()

            ' set reset, clear, and preset values.
            ' Me._armLayer1.AutoDelay.ResetValue = False
            ' Me._armLayer1.Count.ResetValue = 1
            ' Me._armLayer1.Delay.ResetValue = 0
            Me._armLayer1.Bypass.ResetValue = True
            Me._armLayer1.InputLineNumber.ResetValue = 1
            Me._armLayer1.OutputLineNumber.ResetValue = 2
            Me._armLayer1.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me._armLayer1.TimerSeconds.ResetValue = 0.1

            ' Me.Calculate2.CompositeLimitsAutoClearEnabled.ResetValue = True
            ' Me.Calculate2.CompositeLimitsFailureBits.ResetValue = 15
            ' Me.Calculate2.FeedSource.ResetValue = isr.Scpi.FeedSource.Voltage
            ' Me.Calculate2.ImmediateBinning.ResetValue = True
            ' Me.Calculate2.gradingControlMode.ResetValue = True

            Me.Calculate3.ForcedDigitalOutputPatternEnabled.ResetValue = False
            Me.Calculate3.ForcedDigitalOutputPattern.ResetValue = 0

            Me.Format.Elements.ResetValue = isr.Scpi.ReadingElements.Reading Or isr.Scpi.ReadingElements.Timestamp

            ' Me.Output.FrontRouteTerminals.ResetValue = True
            Me.Output.IsOn.ResetValue = False
            ' Me.Output.OffMode.ResetValue = isr.Scpi.OutputOffMode.Normal

            ' Me.Route.FrontRouteTerminals.ResetValue = True

            Me.Sense.AverageCount.ResetValue = 10
            Me.Sense.AverageState.ResetValue = False
            Me.Sense.DeltaAperture.ResetValue = 5
            Me.Sense.DeltaVoltageRange.ResetValue = 120 - 2 * Single.Epsilon
            Me.Sense.DeltaVoltageRange.Minimum = Single.Epsilon
            Me.Sense.DeltaVoltageRange.Maximum = 120 - Single.Epsilon
            Me.Sense.DeltaVoltageAutoRange.ResetValue = True
            Me.Sense.FunctionMode.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC
            ' Me.Sense.FunctionsEnabled.ResetValue = isr.Scpi.SenseFunctionModes.CurrentDC
            ' Me.Sense.FunctionsDisabled.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC Or isr.Scpi.SenseFunctionModes.Resistance
            ' Me.Sense.IsFunctionConcurrent.ResetValue = True

            ' Me.Source.AutoClear.ResetValue = False
            ' Me.Source.AutoDelay.ResetValue = True
            Me.Source.Delay.ResetValue = 1
            ' Me.Source.FunctionMode.ResetValue = isr.Scpi.SourceFunctionMode.Current
            Me.Source.SweepPoints.ResetValue = 11
            ' Me.Source.DeltaArm.ResetValue = False
            Me.Source.DeltaAbortOnCompliance.ResetValue = False
            Me.Source.DeltaHighLevel.ResetValue = 0.001
            Me.Source.DeltaCount.ResetValue = Integer.MaxValue ' represents infinity.
            Me.Source.DeltaDelay.ResetValue = 0.002
            ' Me.Source.MemoryPoints.ResetValue = 1 ' not affected by reset.

            ' Me.SystemSubsystem.AutoZero.ResetValue = True
            Me.SystemSubsystem.BeeperEnabled.ResetValue = True
            ' Me.SystemSubsystem.CableGuard.ResetValue = True
            ' Me.SystemSubsystem.ContactCheckEnabled.ResetValue = False
            ' Me.SystemSubsystem.ContactCheckResistance.ResetValue = 50
            ' Me.SystemSubsystem.RemoteSenseMode.ResetValue = False

            ' Me.Trace.PointsCount.ResetValue = 100
            ' Me.Trace.NotifyCount.ResetValue = 50
            Me.Trace.FeedSource.ResetValue = isr.Scpi.FeedSource.Calculate1
            Me.Trace.NextFeedControl.ResetValue = False

            ' Me.Trigger.AutoDelay.ResetValue = False
            Me.Trigger.Count.ResetValue = 1
            Me.Trigger.Count.PresetValue = Integer.MaxValue ' infinity
            Me.Trigger.Delay.ResetValue = 0
            ' Me.Trigger.Bypass.ResetValue = True
            ' Me.Trigger.InputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.InputLineNumber.ResetValue = 1
            Me.Trigger.OutputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            Me.Trigger.OutputLineNumber.ResetValue = 2
            Me.Trigger.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me.Trigger.TimerSeconds.ResetValue = 0.1

            ' MyBase.Sense.DeltaAperture.Units = New isr.Scpi.FormattedUnits(Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            ' MyBase.Sense.DeltaAperture.DisplayUnits = New isr.Scpi.FormattedUnits(Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)

            MyBase.Sense.DeltaVoltageRange.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Voltage, isr.Scpi.UnitsScale.None)

            Return True
        End Function

        ''' <summary>
        ''' Sets subsystem to its default system preset values.
        ''' </summary>
        Public Overrides Function Preset() As Boolean

            MyBase.Preset()

            ' clear the messages from the error queue
            Me.SystemSubsystem.ClearErrorQueue()

            ' initialize the memory
            ' not supported: isr.Scpi.SystemSubsystem.InitializeMemory(Me)

            ' set device to auto clear
            ' not supported: MyBase.Source.AutoClear(isr.Scpi.ResourceAccessLevels.None) = True

            ' Preset the remote instrument.
            Me.RemotePresetExecutionState()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the isr.Scpi specific set
            MyBase.ResetAndClear()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Function ResetKnownState() As Boolean

            ' The 6221 instrument is left in a limbo state here.  As a result, the
            ' first query returns an empty string.

            MyBase.ResetKnownState()

            ' Reset the remote instrument.
            Me.RemoteResetExecutionState()

            ' Me.Sense.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)
            ' Me.Source.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)

            ' upgrade: set display properties and control values:
            ' TO_DO: Add functions
            ' Me.SourceFunctionMode(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Source.FunctionMode(isr.Scpi.ResourceAccessLevels.Cache)
            ' Me.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.FunctionMode(isr.Scpi.ResourceAccessLevels.Cache)
            Me.Elements(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Format.Elements(isr.Scpi.ResourceAccessLevels.Cache)
            Me.DeltaVoltageAutoRange(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.DeltaVoltageAutoRange(isr.Scpi.ResourceAccessLevels.Cache)

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

#End Region

#Region " DIGITAL I/O "

        ''' <summary>
        ''' Enables or disables the Digital I/O.
        ''' </summary>
        Public Property DigitalPortEnabled(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                Return MyBase.Calculate3.ForcedDigitalOutputPatternEnabled(access)
            End Get
            Set(ByVal value As Boolean)
                If Not Me.UsingDevices Then
                    Return
                End If
                If value <> Me.DigitalPortEnabled(access) Then
                    MyBase.Calculate3.ForcedDigitalOutputPatternEnabled(access) = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the digital I/O.
        ''' </summary>
        Public Property DigitalPortValue(ByVal access As isr.Scpi.ResourceAccessLevels) As Integer
            Get
                Return MyBase.Calculate3.ForcedDigitalOutputPattern(access)
            End Get
            Set(ByVal value As Integer)
                If Not Me.UsingDevices Then
                    Return
                End If
                If value <> Me.DigitalPortValue(access) Then
                    MyBase.Calculate3.ForcedDigitalOutputPattern(access) = value
                    If Me.Visible AndAlso (Me.Gui._digitalOutputUpDown.Value <> value) Then
                        Me.Gui._digitalOutputUpDown.SafeValueSetter(value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Returns true if the interlock is asserted.  That is if the interlock input is
        ''' shorted allowing source measurement to proceed.  Note that contrary to the manual,
        ''' the output bit is set (2) if the interlock is open and clear (0) if the interlock is shorted.
        ''' </summary>
        Public Function InterlockAsserted() As Boolean
            If Not Me.UsingDevices Then
                Return True
            End If
            Dim status As Integer? = MyBase.Status.MeasurementEventCondition(access:=isr.Scpi.ResourceAccessLevels.Device)
            Return status.HasValue AndAlso ((status.Value And MeasurementEvents.InterlockAsserted) = 0)
        End Function

#End Region

#Region " 2182A "

        ''' <summary>
        ''' Returns true if the 2182A instrument is present.
        ''' </summary>
        Public Function IsDeltaPresent() As Boolean
            If Me.UsingDevices Then
                Return MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.Device)
            Else
                Return True
            End If
        End Function

        ''' <summary>
        ''' Gets or sets the condition for processing delta measurements
        ''' before raising events from the event handlers.
        ''' </summary>
        Private _processDeltaMeasurements As Boolean

        Private _isDeltaActive As Boolean
        ''' <summary>
        ''' Gets the status of delta measurements.
        ''' </summary>
        Public ReadOnly Property IsDeltaActive() As Boolean
            Get
                Return Me._isDeltaActive
            End Get
        End Property

        ''' <summary>
        ''' Aborts delta with specific design to terminate 
        ''' measurement and operation events.
        ''' </summary>
        Public Sub AbortDelta()

            Me._processDeltaMeasurements = False
            Me.RequestMeasurementService(MeasurementEvents.None)
            Me.RequestOperationService(OperationEvents.None)
            MyBase.Status.IsOperationCompleted()
            MyBase.Source.DeltaAbort()
            isr.Scpi.SystemSubsystem.SerialFlush(Me)
            Me._isDeltaActive = False

        End Sub

        ''' <summary>
        '''  initiate delta measurement.
        ''' </summary>
        Public Sub InitiateDelta(ByVal notificationPeriod As Integer)

            Try

                'Dim x As Double = Me.Source.DeltaHighLevel(isr.Scpi.ResourceAccessLevels.Device)
                'OnMessageAvailable(Diagnostics.TraceEventType.Information, "BEFORE CLEAR", "Current before CLS = {0}", x)

                ' make sure to clear existing errors.
                Me.ClearActiveState()

                ' force reading back from the device.
                'x = Me.Source.DeltaHighLevel(isr.Scpi.ResourceAccessLevels.Device)
                'OnMessageAvailable(Diagnostics.TraceEventType.Information, "AFTER CLEAR", "Current after CLS = {0}", x)
                Me.Gui.RefreshDisplay()

                ' turn off processing of delta
                Me._processDeltaMeasurements = False
                Me._isDeltaActive = False

                ' clear the measured voltage.
                Me._deltaVoltage = New Double?

                ' clear the buffer.
                MyBase.Trace.ClearBuffer()

                ' set delta to abort on compliance
                Me.Source.DeltaAbortOnCompliance(isr.Scpi.ResourceAccessLevels.None) = True

                MyBase.Trace.NotifyInterval = notificationPeriod

                If notificationPeriod > 0 Then
                    MyBase.Trace.NotifyCount(isr.Scpi.ResourceAccessLevels.None) = notificationPeriod
                End If

                ' in delta the readings and time stamps are reset to zero 
                'Me.Instrument.SystemSubsystem.Execute("RNUM", "RES")
                'Me.Instrument.SystemSubsystem.Execute("TST", "REL:RES")
                ' using delta time stamp is bad news because the instrument prepend "DELTA" in front of each reading.
                ' Me.Instrument.Trace.Execute("TST", "FORM DELT")
                ' Me.Instrument.Trace.Execute("TST", "FORM ABS") ' (default)

                ' clear execution state before enabling events
                If Not Me.ClearExecutionState() Then
                    Throw New BaseException("Failed clearing execution state")
                End If

                ' set the service request
                Me.RequestServiceRequests(isr.Scpi.ServiceRequests.ErrorAvailable Or isr.Scpi.ServiceRequests.SystemEvent)

                ' add measurement events.  This also adds the measurement request to the service request.
                Me.RequestMeasurementService(MeasurementEvents.BufferFull Or
                                             MeasurementEvents.TraceNotify Or
                                             MeasurementEvents.Compliance Or
                                             MeasurementEvents.ReadingOverflow Or
                                             MeasurementEvents.OverTemperature)

                ' add operation events.  This also adds the operation request to the service request.
                Me.RequestOperationService(OperationEvents.SweepAborted Or OperationEvents.SweepDone)

                ' arm delta
                Me.ArmDelta()

                'MyBase.Status.EnabledMeasurementEvents(isr.Scpi.ResourceAccessLevels.None) = MeasurementEvents.All
                'MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.ServiceRequests.All And Not isr.Scpi.ServiceRequests.MessageAvailable

                ' trigger the initiation of the measurement letting the service request do the rest.
                ' TO_DO: temporary to see events before initialize and determine if we should
                ' initialize source after getting the filter settled
                ' Me.Instrument.ClearExecutionState()
                MyBase.Trigger.Initiate()

                ' check if instrument failed.
                If MyBase.HadError Then
                    Dim errorQueue As String = MyBase.SystemSubsystem.ReadLastError()
                    If String.IsNullOrWhiteSpace(errorQueue) Then
                        Throw New BaseException("Failed initiating Delta Measurement due to unknown instrument error")
                    Else
                        Throw New BaseException("Failed initiating Delta Measurement due to instrument error: {0}", errorQueue)
                    End If
                End If

                ' flag processing of delta
                Me._processDeltaMeasurements = True

                Me._isDeltaActive = True

            Catch

                Try
                    ' make sure to abort delta.
                    MyBase.Source.DeltaAbort()
                Finally
                End Try

                Throw

            Finally

            End Try

        End Sub

        ''' <summary>
        ''' Arm delta and return control after operation completed.
        ''' </summary>
        Public Function ArmDelta() As Boolean

            If Not MyBase.Source.DeltaArm(isr.Scpi.ResourceAccessLevels.Device) Then

                ' arm delta
                MyBase.Source.DeltaArm(isr.Scpi.ResourceAccessLevels.None) = True

                Return Me.RemoteQueryOperationCompleted()

            End If

        End Function

        ''' <summary>
        ''' Requests the instrument to place an ASCII �1� in the Output Queue after the last pending 
        ''' operation is completed and then reads the Output.
        ''' </summary>
        ''' <returns>True if receive the expected reply or false if timed out or failed to receive 1.</returns>
        Public Function RemoteQueryOperationCompleted() As Boolean

            If MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.Device) Then

                Me.OnMessageAvailable(TraceEventType.Information, "WAITING FOR 2182 OPERATION TO COMPLETE", "Waiting for 2182 operation to complete.")

                If MyBase.SystemSubsystem.SerialQueryOperationCompleted Then

                    Return True

                ElseIf MyBase.SystemSubsystem.SerialCommTimedOut Then

                    Me.OnMessageAvailable(TraceEventType.Warning, "REMOTE INSTRUMENT (2182A) TIMED OUT", "Remote instrument (2182A) Timed out waiting for operation to complete")
                    Return False

                Else

                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED RECEIVING OPERATION COMPLETED", "Failed receiving operation completed.")
                    Return False

                End If

            Else

                Return True

            End If

        End Function

        ''' <summary>
        ''' Executes a remote query command and waits for operation to complete.
        ''' </summary>
        ''' <param name="nonQueryCommand"></param>
        Public Function RemoteExecute(ByVal nonQueryCommand As String) As Boolean

            If MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.Device) Then

                Me.OnMessageAvailable(TraceEventType.Information, "EXECUTING REMOTE COMMAND", "Executing remote command: '{0}'", nonQueryCommand)

                MyBase.SystemSubsystem.SerialSend(nonQueryCommand)

                Return Me.RemoteQueryOperationCompleted()

            Else

                Return False

            End If

        End Function

        ''' <summary>
        ''' Clears (sets to 0) the bits of the Standard Event Register, Operation Event Register, Error Queue, 
        ''' Measurement Event Register and Questionable Event Register.  
        ''' It also forces the instrument into the operation complete command idle state and operation complete query idle state. 
        ''' </summary>
        Public Function RemoteClearExecutionState() As Boolean

            Return Me.RemoteExecute(Scpi.Syntax.ClearExecutionStateCommand)

        End Function

        ''' <summary>
        ''' Preset remote (2182A) instrument to its known state.
        ''' </summary>
        Public Function RemotePresetExecutionState() As Boolean

            If MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.Device) Then

                Return True

            Else

                Return False

            End If

        End Function

        ''' <summary>
        ''' Reset remote (2182A) instrument to its known state.
        ''' </summary>
        Public Function RemoteResetExecutionState() As Boolean

            Return Me.RemoteExecute(Scpi.Syntax.ClearExecutionStateCommand)

        End Function

        Private _deltaVoltage As Double?
        ''' <summary>
        ''' Gets the measured delta voltage.
        ''' </summary>
        Public ReadOnly Property DeltaVoltage() As Double?
            Get
                Return Me._deltaVoltage
            End Get
        End Property

#End Region

#Region " OVERRIDING PROPERTIES "

        ''' <summary>Gets or sets the sense current integration period in seconds.</summary>
        Public Property Elements(ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.ReadingElements
            Get
                Return MyBase.Format.Elements(access)
            End Get
            Set(ByVal value As isr.Scpi.ReadingElements)

                ' update the reading elements.
                Me._reading = New Readings(value)
                Me._reading.SenseVoltage.ComplianceLimit = Me.CurrentSource.Compliance.Value.Value

                MyBase.Format.Elements(access) = value

            End Set
        End Property

#End Region

#Region " GUI PROPERTIES "

        ''' <summary>
        ''' Gets or set the delta auto range.
        ''' </summary>
        Public Property DeltaVoltageAutoRange(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                Return MyBase.Sense.DeltaVoltageAutoRange(access)
            End Get
            Set(ByVal value As Boolean)
                If value <> Me.DeltaVoltageAutoRange(access) Then
                    MyBase.Sense.DeltaVoltageAutoRange(access) = value
                    If Me.Visible AndAlso (Me.Gui._deltaAutoRangeToggle.Checked <> value) Then
                        Me.Gui._deltaAutoRangeToggle.SafeCheckedSetter(value)
                    End If
                End If
            End Set
        End Property

#End Region

#Region " IDENTITY / VERSION INFO "

        ''' <summary> Has identity. </summary>
        ''' <returns> <c>True</c> if identity was read. </returns>
        Public Function HasIdentity() As Boolean
            Return Me._versionInfo IsNot Nothing AndAlso Not String.IsNullOrEmpty(Me.VersionInfo.Identity) AndAlso
                (Not MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.None) OrElse
                 (Me.K2182VersionInfo IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.K2182VersionInfo.Identity)))
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns the string save the termination character.
        ''' </summary>
        Public Overrides Function ReadIdentity() As String

            If Not Me.HasIdentity Then

                Me._versionInfo = New K6220.VersionInfo(MyBase.ReadIdentity)

                If MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.None) Then
                    Dim k2182id As String = MyBase.SystemSubsystem.SerialQuery(Scpi.Syntax.IdentifyQueryCommand, 200, 2)
                    If MyBase.SystemSubsystem.SerialCommTimedOut Then
                        Me._k2182versionInfo = New K2182.VersionInfo("")
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT READING 2182 IDENTITY", "Timeout reading 2182 identity")
                    Else
                        Me._k2182versionInfo = New K2182.VersionInfo(k2182id)
                    End If
                End If

            End If

            Return Me._versionInfo.Identity

        End Function

        Private _versionInfo As K6220.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public Overloads ReadOnly Property VersionInfo() As K6220.VersionInfo
            Get
                Return Me._versionInfo
            End Get
        End Property

        Private _k2182versionInfo As K2182.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public ReadOnly Property K2182VersionInfo() As K2182.VersionInfo
            Get
                Return Me._k2182versionInfo
            End Get
        End Property

#End Region

#Region " READINGS "

        Private _isBufferAvailable As Boolean
        ''' <summary>
        ''' Gets the condition indicating that the buffer was processed and has data.
        ''' </summary>
        Public ReadOnly Property IsBufferAvailable() As Boolean
            Get
                Return Me._isBufferAvailable
            End Get
        End Property

        ''' <summary>
        ''' Fetches and parses the latest data.
        ''' </summary>
        Public Sub FetchLatest()
            Try
                Dim latestDate As String = MyBase.Sense.FetchLatestData()
                Me.ParseReadingElements(latestDate)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT FETCHING LATEST DATA",
                                          "Timeout occurred fetching latest data from the instrument using '{0}'. VISA status={1}. Details: {2}.",
                                          MyBase.Sense.Controller.TransmitBuffer, Me.LastVisaStatus, ex)
                Else
                    Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED FETCHING LATEST DATA",
                                          "Exception occurred fetching the latest data from the instrument using '{0}'. VISA status={1}. Details: {2}.",
                                          MyBase.Sense.Controller.TransmitBuffer, Me.LastVisaStatus, ex)
                End If
            End Try
        End Sub

        ''' <summary>Parses the latest data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        Public Sub ParseReadingElements()

            ' check if we have a fixed or multi mode data
            ' TO_DO: check for multi- mode data.
            If True Then

                ' parse the latest data
                ParseReadingElements(MyBase.Sense.LatestData())

            Else

                ' fetch the data
                Dim lastFetch As String = MyBase.Sense.FetchData()

                ' parse the results of all data sets.
                Me._readings = K6220.Readings.ParseMulti(Me._reading, lastFetch)

                If Me._readings.Length > 0 Then
                    ' select the final set as the valid readings.
                    Me._reading = Me._readings(Me._readings.Length - 1)
                Else
                    Me._reading.Reset()
                End If

            End If

        End Sub

        ''' <summary>Parses a new set of reading elements.</summary>
        ''' <param name="data">Specifies the measurement text to parse into the new reading.</param>
        Public Sub ParseReadingElements(ByVal data As String)

            ' check if we have units suffixes.
            If (Me._reading.Elements And isr.Scpi.ReadingElements.Units) <> 0 Then
                data = isr.Scpi.Syntax.TrimUnits(data)
            End If

            ' Take a reading and parse the results
            Me._reading.Parse(data)

            If Me.Reading.SenseVoltage.Outcome.HitCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT COMPLIANCE",
                                      "Real Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.SenseVoltage.Outcome.HitRangeCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT RANGE COMPLIANCE",
                                      "Range Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.SenseVoltage.Outcome.HitLevelCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT LEVEL COMPLIANCE",
                                      "Level Compliance.  Instrument sensed an output overflow of the measured value.")


            Else

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT PARSED DATA", "Instruments parsed reading elements.")

            End If


            If Me.Visible Then

                ' update the display.
                Me.Gui.DisplaySelectedReading()

            End If

        End Sub

        ''' <summary>
        ''' Reads the 2182 buffer.
        ''' </summary><returns></returns>
        Public Function Read2182Buffer() As String
            Return isr.Scpi.SystemSubsystem.SerialFlush(Me)
        End Function

        ''' <summary>Take a single measurement and parse the result.</summary>
        ''' <remarks>This read uses Output Auto Clear.</remarks>
        Public Sub Read()

            ' disable service request on measurements and operations.
            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.None
            Me.RequestOperationService(OperationEvents.None)

            ' Fetch the last reading and parse the results
            Me.FetchLatest()

        End Sub

        Private _readings As Readings()
        ''' <summary>
        ''' Returns the readings.
        ''' </summary>
        Public Function Readings() As Readings()
            Return Me._readings
        End Function

        Private _reading As Readings
        ''' <summary>
        ''' Gets or sets the current readings.</summary>
        Public Property Reading() As Readings
            Get
                Return Me._reading
            End Get
            Set(ByVal value As Readings)
                Me._reading = value
            End Set
        End Property

        ''' <summary>
        ''' Read the buffer into a reading array.
        ''' </summary>
        Public Function ReadBuffer() As Boolean

            ' trace getting the data from the sense.
            Me.Trace.FeedSource.ResetValue = isr.Scpi.FeedSource.Sense

            ' set the current compliance limit
            Me._reading.SenseVoltage.ComplianceLimit = Me.CurrentSource.Compliance.Value.Value

            ' set a long data string depending on the number of readings expected.
            ' MyBase.GpibSession.Reader.DefaultStringSize = 50 * 2 * 13

            ' get the readings into an array.
            Me._readings = K6220.Readings.ParseMulti(Me._reading, MyBase.Trace.ReadBuffer())

            ' set the buffer available condition
            Me._isBufferAvailable = Me._readings.Length > 0

            If Me._isBufferAvailable Then
                ' select the final set as the valid readings.
                Me._reading = New Readings(Me._readings(Me._readings.Length - 1))
            Else
                Me._reading.Reset()
            End If

            Return Me._isBufferAvailable

        End Function

#End Region

#Region " REGISTERS "

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Return isr.Scpi.SystemSubsystem.ReadLastError(Me)
            End Get
        End Property

        ''' <summary>
        ''' Sets the registers that will issue a service request.
        ''' </summary>
        Public Sub RequestServiceRequests(ByVal value As isr.Scpi.ServiceRequests)
            MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = value
            MyBase.Status.EnabledStandardEvents(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.StandardEvents.All
        End Sub

        ''' <summary>Sets the measurement service status registers to request a service
        '''   request upon measurement events.</summary>
        Public Sub RequestMeasurementService(ByVal value As MeasurementEvents)

            MyBase.Status.EnabledMeasurementEvents(isr.Scpi.ResourceAccessLevels.None) = value

            Dim requestBits As isr.Scpi.ServiceRequests = MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None)
            If value = MeasurementEvents.None Then
                requestBits = requestBits And Not isr.Scpi.ServiceRequests.MeasurementEvent
            Else
                requestBits = requestBits Or isr.Scpi.ServiceRequests.MeasurementEvent
            End If
            Me.RequestServiceRequests(requestBits)

        End Sub

        ''' <summary>Sets the operation event registers to request a service
        '''   request upon the specified operation events.</summary>
        Public Sub RequestOperationService(ByVal value As OperationEvents)

            MyBase.Status.EnabledOperationEvents(isr.Scpi.ResourceAccessLevels.None) = value

            Dim requestBits As isr.Scpi.ServiceRequests = MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None)
            If value = OperationEvents.None Then
                requestBits = requestBits And Not isr.Scpi.ServiceRequests.OperationEvent
            Else
                requestBits = requestBits Or isr.Scpi.ServiceRequests.OperationEvent
            End If
            Me.RequestServiceRequests(requestBits)

        End Sub

#End Region

#Region " ARM LAYERS "

        ''' <summary>
        ''' Adds the Arm Layers.
        ''' </summary>
        Private Sub addLayers()
            Me._armLayer1 = MyBase.Arm.AddLayer("LAY1")
        End Sub

        Private _armLayer1 As isr.Scpi.ArmLayer
        ''' <summary>
        ''' Gets reference to the ARM layers.
        ''' </summary>
        Public ReadOnly Property ArmLayer1() As isr.Scpi.ArmLayer
            Get
                Return Me._armLayer1
            End Get
        End Property

#End Region

#Region " SCPI FUNCTIONS "

        ''' <summary>
        ''' Adds the source and sense functions.
        ''' </summary>
        Private Sub addFunctions()

            Me._currentSource = MyBase.Source.AddFunction(isr.Scpi.Syntax.Current, isr.Scpi.SourceFunctionMode.Current)

            ' set current defaults
            Me._currentSource.AutoRange.ResetValue = False
            Me._currentSource.Compliance.ResetValue = 10
            Me._currentSource.Compliance.Minimum = 0.1 + Single.Epsilon
            Me._currentSource.Compliance.Maximum = 105 - Single.Epsilon
            Me._currentSource.Level.ResetValue = 0
            Me._currentSource.Range.ResetValue = 0.1

        End Sub

        Private _currentSource As isr.Scpi.ScpiFunction
        Public ReadOnly Property CurrentSource() As isr.Scpi.ScpiFunction
            Get
                Return Me._currentSource
            End Get
        End Property

        ''' <summary>Sets the system line frequency.
        ''' This instrument does not directly support the line frequency command.
        ''' The line frequency is set using the 2182 o to the default 60Hz.
        ''' </summary>
        Public Function PresetLineFrequency() As Integer
            Static lineFrequency As Integer?
            If Not lineFrequency.HasValue Then
                lineFrequency = isr.Scpi.SystemSubsystem.LineFrequencyGetter
                If MyBase.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.Device) Then
                    Dim value As String = MyBase.SystemSubsystem.SerialQuery(":SYST:LFR?", 200, 2)
                    lineFrequency = isr.Core.ParseExtensions.ConvertTo(Of Integer)(value, 60)
                End If
                Me.SystemSubsystem.LineFrequency() = lineFrequency.Value
            End If
        End Function

#End Region

#Region " OPERATION AND MEASUREMENT EVENTS "

        ''' <summary>Raised upon the Buffer Available measurement event.</summary>
        Public Event BufferAvailable As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Buffer Available event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnBufferAvailable(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Verbose, "BUFFER AVAILABLE", "Buffer Available")
            If Me._processDeltaMeasurements Then
                ' Fetch the last reading and parse the results
                Me.FetchLatest()
            End If
            BufferAvailableEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised upon the Buffer Full measurement event.</summary>
        Public Event BufferFull As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Buffer Full event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnBufferFull(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Verbose, "BUFFER FULL", "Buffer Full")
            If Me._processDeltaMeasurements Then

                ' abort delta.
                Me.AbortDelta()

                ' read the buffer.
                If Me.ReadBuffer() Then

                    ' calculate the average
                    Me._deltaVoltage = Me.Calculate2.CalculateBufferAverage()
                    Me.OnMessageAvailable(TraceEventType.Information, "CALCULATED AVERAGE",
                                          "Calculated average: {0}", Me._deltaVoltage.Value)

                End If
            End If
            BufferFullEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised upon the Data Available measurement event.
        ''' This event occurs with setting the Notify Count and Notification Interval.</summary>
        Public Event DataAvailable As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Data Available event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnDataAvailable(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Verbose, "DATA AVAILABLE", "Data Available")
            If Me._processDeltaMeasurements Then

                If Me.Trace.PeriodicNotifyEnabled Then
                    ' set the next notification.
                    Me.Trace.NotifyNext(isr.Scpi.ResourceAccessLevels.None)
                End If

                ' Fetch the last reading and parse the results
                Me.FetchLatest()

            End If
            DataAvailableEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised upon the Filter Settled operation event.</summary>
        Public Event MeasurementFailed As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the Filter Settled event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnMeasurementFailed(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Warning, "MEASUREMENT FAILED", "Measurement Failed")
            If (Me.LastServiceEventArgs.MeasurementEventStatus And MeasurementEvents.ReadingOverflow) <> 0 Then
                Me.OnMessageAvailable(TraceEventType.Warning, "READING OVERFLOW", "Reading overflow")
            End If
            If (Me.LastServiceEventArgs.MeasurementEventStatus And MeasurementEvents.OverTemperature) <> 0 Then
                Me.OnMessageAvailable(TraceEventType.Warning, "OVER TEMPERATURE", "Over temperature")
            End If
            If (Me.LastServiceEventArgs.MeasurementEventStatus And MeasurementEvents.Compliance) <> 0 Then
                Me.OnMessageAvailable(TraceEventType.Warning, "COMPLIANCE", "Compliance")
            End If
            If Me._processDeltaMeasurements Then
                ' abort delta.
                Me.Source.DeltaAbort()
            End If
            MeasurementFailedEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Raised upon the reading available measurement event.</summary>
        Public Event ReadingAvailable As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the reading available event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnReadingAvailable(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Information, "READING AVAILABLE", "Reading Available")
            If Me._processDeltaMeasurements Then
            End If
            ReadingAvailableEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Occurs when sweep is aborted.</summary>
        Public Event SweepAborted As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the <see cref="SweepAborted"></see> event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnSweepAborted(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Warning, "SWEEP ABORTED", "Sweep Aborted")
            If Me._processDeltaMeasurements Then
            End If
            SweepAbortedEvent.SafeInvoke(Me, e)
        End Sub

        ''' <summary>Occurs when sweep is Done.</summary>
        Public Event SweepDone As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the <see cref="SweepDone"></see> event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnSweepDone(ByVal e As System.EventArgs)
            Me.OnMessageAvailable(TraceEventType.Information, "SWEEP DONE", "Sweep Done")
            If Me._processDeltaMeasurements Then
            End If
            SweepDoneEvent.SafeInvoke(Me, e)
        End Sub

#End Region

#Region " SERVICE REQUEST HANDLER "

        ''' <summary>Updates the screen if we have a new measurement and raises the service
        '''   request event. New measurements are parsed including setting the threshold
        '''   levels for testing for level compliance.
        ''' Note that on a two point sweep we get to service request.  The first service request
        ''' Appears before the second reading is on screen but already has the values for both sweeps.
        ''' </summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs) ' System.EventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then

                If (e.MeasurementEventStatus And MeasurementEvents.ReadingAvailable) <> 0 Then
                    ' raise the reading Available Event.
                    Me.OnReadingAvailable(System.EventArgs.Empty)
                End If

                If (e.OperationEventStatus And OperationEvents.SweepAborted) <> 0 Then
                    Me.OnSweepAborted(System.EventArgs.Empty)
                End If

                If (e.OperationEventStatus And OperationEvents.SweepDone) <> 0 Then
                    Me.OnSweepDone(System.EventArgs.Empty)
                End If

                If (e.MeasurementEventStatus And
                    (MeasurementEvents.Compliance Or MeasurementEvents.OverTemperature Or MeasurementEvents.ReadingOverflow)) <> 0 Then
                    Me.OnMeasurementFailed(System.EventArgs.Empty)
                End If

                If (e.MeasurementEventStatus And MeasurementEvents.BufferAvailable) <> 0 Then
                    Me.OnBufferAvailable(System.EventArgs.Empty)
                End If

                If (e.MeasurementEventStatus And MeasurementEvents.BufferFull) <> 0 Then
                    Me.OnBufferFull(System.EventArgs.Empty)
                End If

                If (e.MeasurementEventStatus And MeasurementEvents.TraceNotify) <> 0 Then
                    ' 4071 -- ignore? this is causing occasional visa I/O errors
                    Me.OnDataAvailable(System.EventArgs.Empty)
                End If

            End If

            ' raise the service request event
            MyBase.OnServiceRequest(e)

        End Sub

        ''' <summary>
        ''' If error is reported, abort delta.
        ''' </summary>
        Private Sub Instrument_FailureOccurred(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.FailureOccurred
            Me.Source.DeltaAbort()
        End Sub

#End Region

    End Class

End Namespace
