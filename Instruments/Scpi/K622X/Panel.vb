Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ControlExtensions
Imports isr.Core.ComboBoxExtensions
Namespace K6220

    ''' <summary>Provides a user interface for the Keithley 622X instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    <System.ComponentModel.DisplayName("K6220 Panel"),
    System.ComponentModel.Description("Keithley 6220 Instrument Family - Windows Forms Custom Control"),
    System.Drawing.ToolboxBitmap(GetType(K6220.Panel))>
    Public Class Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New K6220.Instrument(Me, Me.Name)

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                Me._messagesBox.PrependMessage(e.ExtendedMessage)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function


        ''' <summary>Update the display based on the current instrument values.</summary>
        Friend Sub RefreshDisplay()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If

            Me._readSourceToggle.SafeCheckedSetter((Me.Instrument.Elements(isr.Scpi.ResourceAccessLevels.None) And isr.Scpi.ReadingElements.Source) <> 0)
            Me._averageCountTextBox.SafeTextSetter(Me.Instrument.Sense.AverageCount.ToString)
            Me._deltaAutoRangeToggle.SafeCheckedSetter(Me.Instrument.Sense.DeltaVoltageAutoRange.Value.Value)
            Me._deltaHighLevelTextBox.SafeTextSetter(Me.Instrument.Source.DeltaHighLevel.DisplayCaption)
            Me._integrationPeriodTextBox.SafeTextSetter(CStr(Me.Instrument.Sense.DeltaAperture.Value / Me.Instrument.SystemSubsystem.LineFrequency))

            Me._deltaCountTextBox.SafeTextSetter(Me.Instrument.Source.DeltaCount.ToString)
            Me._deltaDelayTextBox.SafeTextSetter(Me.Instrument.Source.DeltaDelay.DisplayCaption)
            Me._deltaVoltageLimitTextBox.SafeTextSetter(Me.Instrument.CurrentSource.Compliance.DisplayCaption)

        End Sub

#End Region

#Region " I Connectible "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me._tabControl.Enabled = value
                If value Then
                    If Me.Instrument.UsingDevices Then
                        populateControls()
                        RefreshDisplay()
                    End If
                End If
            End Set
        End Property

        ''' <summary>List reading elements and enables source functions.
        ''' </summary>
        Private Sub populateControls()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If
            populateModalities()
            If Me.Instrument.Source.DeltaPresent(isr.Scpi.ResourceAccessLevels.None) Then
                If Me.Instrument.K2182VersionInfo Is Nothing Then
                    Me.Instrument.ReadIdentity()
                End If
                If Me.Instrument.K2182VersionInfo IsNot Nothing Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Information, "KEITHLEY 2182 LOCATED", "Keithley 2182 located '{0}'", Me.Instrument.K2182VersionInfo.Identity)
                End If
            Else
                Me.Instrument.OnMessageAvailable(TraceEventType.Warning, "KEITHLEY 2182 NOT CONNECTED", "Keithley 2182 not connected.")
            End If
            Me._integrationPeriodTextBoxLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                   "Integration Period [{0}]", "s")

        End Sub

        Private _modalities As isr.Scpi.ReadingElements
        Private Sub populateModalities()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If

            If Me._modalities <> Me.Instrument.Elements(isr.Scpi.ResourceAccessLevels.Cache) Then
                Dim selectedIndex As Integer = Me._modalityComboBox.SelectedIndex
                Me._modalities = Me.Instrument.Elements(isr.Scpi.ResourceAccessLevels.Cache)
                Me._modalityComboBox.DataSource = Nothing
                Me._modalityComboBox.Items.Clear()
                Me._modalityComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(
                              GetType(isr.Scpi.ReadingElements), Me._modalities And Not isr.Scpi.ReadingElements.Units)
                Me._modalityComboBox.DisplayMember = "Value"
                Me._modalityComboBox.ValueMember = "Key"

                If Me._modalityComboBox.Items.Count > 0 Then
                    Me._modalityComboBox.SelectedIndex = Math.Max(selectedIndex, 0)
                End If

            End If

        End Sub

#End Region

#Region " PROPERTIES "

        Private WithEvents _instrument As K6220.Instrument
        ''' <summary>Gets a reference to the Keithley 6220 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As K6220.Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As K6220.Instrument)
                If value IsNot Nothing Then
                    Me._instrument = value
                    MyBase.ConnectableResource = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE SETTINGS: MODALITY "

        Private _actualModality As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Selects a new modality.
        ''' </summary>
        Friend Function ApplyModality(ByVal value As isr.Scpi.ReadingElements) As isr.Scpi.ReadingElements
            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected AndAlso
                (value <> isr.Scpi.ReadingElements.None) AndAlso (value <> Me._actualModality) Then
                Me._actualModality = value
                Me._modalityComboBox.SafeSelectItem(isr.Core.EnumExtensions.ValueDescriptionPair(value))
            End If
            Return Me._actualModality
        End Function

        ''' <summary>
        ''' Gets or sets the selected modality.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Private ReadOnly Property selectedModality() As isr.Scpi.ReadingElements
            Get
                Return CType(CType(Me._modalityComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, isr.Scpi.ReadingElements)
            End Get
        End Property

#End Region

#Region " BULK SETTINGS "

        ''' <summary>
        ''' Applies the selected source level and limits.
        ''' </summary>
        Private Sub applyDelta(ByVal accessLevel As isr.Scpi.ResourceAccessLevels)

            If Not Me.Instrument.IsConnected Then
                Return
            End If

            Dim valueInteger As Integer
            Dim valueDouble As Double

            ' get the Integration Period
            Me.Instrument.Sense.DeltaAperture(accessLevel) =
                    Me.Instrument.Sense.DeltaAperture.ParseScaledValue(Me._integrationPeriodTextBox.Text) * Me.Instrument.SystemSubsystem.LineFrequency

            If Me._readSourceToggle.Checked Then
                Me.Instrument.Elements(accessLevel) = isr.Scpi.ReadingElements.Reading Or
                                                      isr.Scpi.ReadingElements.Source Or
                                                      isr.Scpi.ReadingElements.Timestamp Or
                                                      isr.Scpi.ReadingElements.ReadingNumber
            Else
                Me.Instrument.Elements(accessLevel) = isr.Scpi.ReadingElements.Reading Or isr.Scpi.ReadingElements.Timestamp
            End If

            ' update modalities display.
            populateModalities()

            ' turn auto range off.
            Me.Instrument.Sense.DeltaVoltageAutoRange(accessLevel) = False
            Me._deltaAutoRangeToggle.Checked = False

            Me.Instrument.Sense.DeltaVoltageRange(accessLevel) =
                  Me.Instrument.Sense.DeltaVoltageRange.ParseScaledValue(Me._deltaVoltageLimitTextBox.Text)

            If Integer.TryParse(Me._averageCountTextBox.Text, Globalization.NumberStyles.Integer,
                                Globalization.CultureInfo.CurrentCulture, valueInteger) Then
                Me.Instrument.Sense.AverageCount(accessLevel) = valueInteger
            End If

            ' default to filter on.
            Me.Instrument.Sense.AverageState(accessLevel) = True

            ' set source to auto range
            Me.Instrument.Source.AutoRange(accessLevel) = True
            If Double.TryParse(Me._deltaHighLevelTextBox.Text, Globalization.NumberStyles.Float,
                               Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                Me.Instrument.Source.DeltaHighLevel(accessLevel) = valueDouble
                ' Me.Instrument.Source.Range(Me.Instrument.CurrentSource, accessLevel) = 2 * valueDouble
            End If

            If Double.TryParse(Me._deltaVoltageLimitTextBox.Text, Globalization.NumberStyles.Float,
                               Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                Me.Instrument.Source.Compliance(Me.Instrument.CurrentSource, accessLevel) = valueDouble
            End If

            If Double.TryParse(Me._deltaDelayTextBox.Text, Globalization.NumberStyles.Float,
                               Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                Me.Instrument.Source.DeltaDelay(accessLevel) = valueDouble
            End If

            If Integer.TryParse(Me._deltaCountTextBox.Text, Globalization.NumberStyles.Integer,
                                Globalization.CultureInfo.CurrentCulture, valueInteger) Then
                Me.Instrument.Source.DeltaCount(accessLevel) = valueInteger
                If valueInteger < Integer.MaxValue - 1 Then
                    Me.Instrument.Trace.PointsCount(accessLevel) = valueInteger
                    Me.Instrument.Trace.DefaultTraceSize = (valueInteger + 1) * (Me.Instrument.Reading.ElementsLength + 1)
                End If

            End If

            ' update with values from the instrument
            Me.Instrument.StoreResourceAccessLevel()
            Me.Instrument.AccessLevel = isr.Scpi.ResourceAccessLevels.Device
            Me.RefreshDisplay()
            Me.Instrument.RestoreResourceAccessLevel()

        End Sub

        ''' <summary>Displays a reading based on the selected reading to display.</summary>
        Public Sub DisplaySelectedReading()

            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.Sense IsNot Nothing AndAlso Me.Instrument.Reading IsNot Nothing Then

                Select Case Me.selectedModality
                    Case isr.Scpi.ReadingElements.Reading
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.SenseVoltage.Reading)
                    Case isr.Scpi.ReadingElements.Timestamp
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Timestamp.Reading)
                    Case isr.Scpi.ReadingElements.Source
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.SourceCurrent.Reading)
                    Case isr.Scpi.ReadingElements.ReadingNumber
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.ReadingNumber.ToDisplayString())
                End Select
            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: INTERFACE "

        Private Sub InterfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _interfaceClearButton.Click
            Me.Instrument.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary>
        ''' Issue RST.
        ''' </summary>
        Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _resetButton.Click

            Me.Instrument.ResetKnownState()

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: "

        Private Sub DigitalOutputUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _digitalOutputUpDown.ValueChanged
            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.IsConnected Then
                Me.Instrument.DigitalPortEnabled(isr.Scpi.ResourceAccessLevels.None) = True
                Me.Instrument.DigitalPortValue(isr.Scpi.ResourceAccessLevels.None) = CType(Me._digitalOutputUpDown.Value, Integer)
                ' Me.Instrument.Sense.
            End If
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub _abortButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _abortButton.Click
            ' Me.Instrument.Source.DeltaAbort()
            Me.Instrument.AbortDelta()
        End Sub

        Private Sub _applyDeltaSettingButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _applyDeltaSettingButton.Click
            applyDelta(isr.Scpi.ResourceAccessLevels.Device)
        End Sub

        ''' <summary>
        ''' Restores delta settings.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="current")>
            <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="count")>
            <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="delay")>
            <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="aperture")>
            <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="comp")>
            <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="autoRange")>
        Private Sub _RestoreDeltaSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RestoreDeltaSettingsButton.Click

            Dim count As Integer = Me.Instrument.Sense.AverageCount(isr.Scpi.ResourceAccessLevels.Device)
            Dim autoRange As Boolean = Me.Instrument.Sense.DeltaVoltageAutoRange(isr.Scpi.ResourceAccessLevels.Device)
            Dim current As Double = Me.Instrument.Source.DeltaHighLevel(isr.Scpi.ResourceAccessLevels.Device)
            Dim aperture As Double = Me.Instrument.Sense.DeltaAperture(isr.Scpi.ResourceAccessLevels.Device)
            count = Me.Instrument.Source.DeltaCount(isr.Scpi.ResourceAccessLevels.Device)
            Dim delay As Double = Me.Instrument.Source.DeltaDelay(isr.Scpi.ResourceAccessLevels.Device)
            Dim comp As Double = Me.Instrument.Source.Compliance(Me.Instrument.CurrentSource, isr.Scpi.ResourceAccessLevels.Device)
            Me.RefreshDisplay()

        End Sub

        Dim initLock As New Object
        ''' <summary>
        ''' Initiates a reading for retrieval by way of the service request event.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitiateButton.Click

            SyncLock initLock

                Try

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                    Me._readingNumberUpDown.Enabled = False

                    Dim notificationInterval As Double = 0.25
                    Dim notificationPeriod As Integer = CInt(60 * notificationInterval / Me.Instrument.Sense.DeltaAperture.Value.Value)

                    ' initiate delta.
                    Me.Instrument.InitiateDelta(notificationPeriod)

                Catch ex As Exception

                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "INITIALIZE FAILED", "Exception occurred initiating measurement.")

                Finally

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                    ' update display modalities if changed.
                    populateModalities()

                End Try

            End SyncLock

        End Sub

        ''' <summary>Selects a new reading to display.</summary>
        Private Sub ModalityComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _modalityComboBox.SelectedIndexChanged

            If Me._modalityComboBox.Enabled AndAlso Me._modalityComboBox.SelectedIndex >= 0 AndAlso Not String.IsNullOrWhiteSpace(Me._modalityComboBox.Text) Then
                Me.DisplaySelectedReading()
            End If

        End Sub

        ''' <summary>Query the instrument for a reading.</summary>
        Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _readButton.Click

            ' update display modalities if changed.
            populateModalities()

            Me.Instrument.FetchLatest()

            Me.DisplaySelectedReading()

        End Sub

        Private Sub _readingNumberUpDown_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _readingNumberUpDown.ValueChanged

            If Me._readingNumberUpDown.Enabled AndAlso Me.Instrument IsNot Nothing AndAlso Me.Instrument.Readings IsNot Nothing Then
                Dim readingIndex As Integer = CInt(Me._readingNumberUpDown.Value) - 1
                If readingIndex >= 0 AndAlso readingIndex < Me.Instrument.Readings.Count Then
                    Me.Instrument.Reading = New Readings(Me.Instrument.Readings(readingIndex))
                    Me.DisplaySelectedReading()
                End If
            End If

        End Sub

#End Region

#Region " INSTRUMENT EVENT HANDLERS "

        Private Sub _instrument_BufferAvailable(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.BufferAvailable
        End Sub

        Private Sub _instrument_BufferFull(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrument.BufferFull

            ' allow display of readings.
            If Me.Instrument.Readings IsNot Nothing Then
                Me._readingNumberUpDown.Minimum = 1
                Me._readingNumberUpDown.Value = 1
                Me._readingNumberUpDown.Maximum = Me.Instrument.Readings.Count
                Me._readingNumberUpDown.Invalidate()
                Me._readingNumberUpDown.Enabled = Me.Instrument.Readings.Count > 0
            End If

            ' update the display.
            Me.DisplaySelectedReading()

        End Sub

#End Region

        Private Sub _Flash2182BufferButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _Flash2182BufferButton.Click
            Me._2182BufferTextBox.Text = ""
            Me._2182BufferTextBox.Text = Me.Instrument.Read2182Buffer()
        End Sub
    End Class

End Namespace
