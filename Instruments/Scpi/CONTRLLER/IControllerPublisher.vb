﻿''' <summary>
''' The contract implemented by controller-publisher to anon observers with synchronization.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/08/2010" by="David" revision="1.2.3691.x">
''' Created
''' </history>
Public Interface IControllerPublisher

  Inherits isr.Core.ISyncPublisher

#Region " CONTROLLER "

  ''' <summary>Gets reference to the controller.</summary>
  ReadOnly Property Controller() As isr.Visa.Instruments.Controller

  ''' <summary>
  ''' Returns true if the controller is connected.
  ''' </summary>
      Function IsConnected() As Boolean

#End Region

End Interface
