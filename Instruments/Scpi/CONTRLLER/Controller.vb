﻿Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Imports System.ComponentModel

''' <summary>
''' Provides a Core. for isr.Scpi (488.2) instruments.
''' </summary>
''' <remarks>Later on we can tease out the non-SCPI parts of this instrument 
'''   to define a Instrument that inherits a Instrument.</remarks>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/07/05" by="David" revision="1.0.2137.x">
''' Create from Instrument User Control
''' </history>
''' <history date="03/25/08" by="David" revision="2.1.3006.x">
''' Use the <see cref="isr.Scpi">SCPI</see> library and
''' <see cref="isr.Scpi.IDevice">controller</see> interface.
''' </history>
Public MustInherit Class Controller
    Inherits DeviceBase
    Implements IDisposable, isr.Scpi.IDevice, isr.Visa.IInstrument, isr.Core.IFailurePublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceTitle">Specifies the instrument name.</param>
    Protected Sub New(ByVal resourceTitle As String, ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType)

        ' instantiate the base class
        MyBase.New(resourceTitle, resourceType)

        Me._terminationCharacter = 10
        Me._UsingDevices = True
        Me._accessLevel = isr.Scpi.ResourceAccessLevels.Device
        Me._messageAvailableBits = isr.Scpi.ServiceRequests.MessageAvailable
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' dispose of 'status' properties
                    Me._identity = String.Empty

                    If FailureOccurredEvent IsNot Nothing Then
                        For Each d As [Delegate] In FailureOccurredEvent.GetInvocationList
                            RemoveHandler FailureOccurred, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If

                    If ServiceRequestEvent IsNot Nothing Then
                        For Each d As [Delegate] In ServiceRequestEvent.GetInvocationList
                            RemoveHandler ServiceRequest, CType(d, Global.System.EventHandler(Of isr.Scpi.BaseServiceEventArgs))
                        Next
                    End If

                    If ServiceRequestEvent IsNot Nothing Then
                        For Each d As [Delegate] In ServiceRequestEvent.GetInvocationList
                            RemoveHandler ServiceRequest, CType(d, Global.System.EventHandler(Of isr.Scpi.BaseServiceEventArgs))
                        Next
                    End If

                    ' terminate the isr.Scpi sub systems
                    If Me._arm IsNot Nothing Then
                        Me._arm.Dispose()
                        Me._arm = Nothing
                    End If
                    If Me._calculate2 IsNot Nothing Then
                        Me._calculate2.Dispose()
                        Me._calculate2 = Nothing
                    End If
                    If Me._calculate3 IsNot Nothing Then
                        Me._calculate3.Dispose()
                        Me._calculate3 = Nothing
                    End If
                    If Me._display IsNot Nothing Then
                        Me._display.Dispose()
                        Me._display = Nothing
                    End If
                    If Me._format IsNot Nothing Then
                        Me._format.Dispose()
                        Me._format = Nothing
                    End If
                    If Me._trace IsNot Nothing Then
                        Me._trace.Dispose()
                        Me._trace = Nothing
                    End If
                    If Me._trigger IsNot Nothing Then
                        Me._trigger.Dispose()
                        Me._trigger = Nothing
                    End If
                    If Me._system IsNot Nothing Then
                        Me._system.Dispose()
                        Me._system = Nothing
                    End If
                    If Me._sense IsNot Nothing Then
                        Me._sense.Dispose()
                        Me._sense = Nothing
                    End If
                    If Me._output IsNot Nothing Then
                        Me._output.Dispose()
                        Me._output = Nothing
                    End If
                    If Me._status IsNot Nothing Then
                        Me._status.Dispose()
                        Me._status = Nothing
                    End If
                    If Me._route IsNot Nothing Then
                        Me._route.Dispose()
                        Me._route = Nothing
                    End If
                    If Me._source IsNot Nothing Then
                        Me._source.Dispose()
                        Me._source = Nothing
                    End If

                    If Me._gpibSession IsNot Nothing Then
                        If Me.IsConnected Then
                            ' remove the service request handler ignoring exceptions.
                            RemoveHandler Me._gpibSession.ServiceRequest, AddressOf OnVisaServiceRequest
                        End If
                        Me._gpibSession.Dispose()
                        Me._gpibSession = Nothing
                    End If

                    If Me._gpibInterface IsNot Nothing Then
                        Me._gpibInterface.Dispose()
                        Me._gpibInterface = Nothing
                    End If

                End If
            End If

            ' Free shared unmanaged resources

        Finally

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " I Resettable "

    Public Overrides Function ClearResource() As Boolean
        Return ResetAndClear()
    End Function

    Private _resettableValues As isr.Scpi.ResettableCollection(Of isr.Scpi.IResettable)
    ''' <summary>
    ''' Gets reference to the Resettable values for this controller.
    ''' </summary>
    Public ReadOnly Property ResettableValues() As isr.Scpi.ResettableCollection(Of isr.Scpi.IResettable)
        Get
            Return Me._resettableValues
        End Get
    End Property

    ''' <summary>Selectively clears the instrument.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overrides Function ClearActiveState() As Boolean

        If Me.UsingDevices AndAlso Me._gpibSession IsNot Nothing Then

            Try
                Me._gpibSession.ClearActiveState()
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING ACTIVE STATE",
                                          "Timeout occurred when clearing active state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                Else
                    Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                          "VISA Exception occurred when clearing active state. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                      "Exception occurred when clearing active state. Details: {0}.", ex)
            End Try
        End If
        Return Me.IsLastVisaOperationSuccess

    End Function

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    ''' <remarks>Also clears all service registers as well as the 
    '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
    Public Overrides Function ClearExecutionState() As Boolean Implements isr.Scpi.IDevice.ClearExecutionState

        If Me._gpibSession IsNot Nothing Then

            ' Clear the device status.  This will set all the
            ' standard subsystem properties.
            Me._gpibSession.ClearExecutionState()

        End If

        ' Clear the event arguments
        Me._lastServiceEventArgs = New isr.Scpi.DeviceServiceEventArgs(Me)

        ' clear the Resettable values.
        Me._resettableValues.ClearExecutionState()

        ' clear the subsystems.
        Me._subsystems.ClearExecutionState()

        Me.OnMessageAvailable(TraceEventType.Verbose, "STATUS CLEARED", "Status Cleared")

        Return Not Me.HadError

    End Function

    ''' <summary>Initialize Resettable values, units, scales.
    ''' This is to be called once upon connecting but before reset and clear..</summary>
    Public Overridable Function InitializeExecutionState() As Boolean Implements isr.Scpi.IDevice.InitializeExecutionState

        Static executed As Boolean

        ' instantiate event arguments
        Me._lastServiceEventArgs = New isr.Scpi.DeviceServiceEventArgs(Me)

        Me._subsystems = New isr.Scpi.ResettableCollection(Of isr.Scpi.Subsystem)

        ' instantiate the isr.Scpi sub systems
        Me._system = New isr.Scpi.SystemSubsystem(Me)
        Me._subsystems.Add(Me._system)
        Me._sense = New isr.Scpi.SenseSubsystem(Me)
        Me._subsystems.Add(Me._sense)
        Me._output = New isr.Scpi.OutputSubsystem(Me)
        Me._subsystems.Add(Me._output)
        Me._route = New isr.Scpi.RouteSubsystem(Me)
        Me._subsystems.Add(Me._route)
        Me._status = New isr.Scpi.StatusSubsystem(Me)
        Me._subsystems.Add(Me._status)
        Me._source = New isr.Scpi.SourceSubsystem(Me)
        Me._subsystems.Add(Me._source)
        Me._trigger = New isr.Scpi.TriggerSubsystem(Me)
        Me._subsystems.Add(Me._trigger)
        Me._calculate3 = New isr.Scpi.Calculate3Subsystem(Me)
        Me._subsystems.Add(Me._calculate3)
        Me._calculate2 = New isr.Scpi.Calculate2Subsystem(Me)
        Me._subsystems.Add(Me._calculate2)
        Me._format = New isr.Scpi.FormatSubsystem(Me)
        Me._subsystems.Add(Me._format)
        Me._trace = New isr.Scpi.TraceSubsystem(Me)
        Me._subsystems.Add(Me._trace)
        Me._display = New isr.Scpi.DisplaySubsystem(Me)
        Me._subsystems.Add(Me._display)
        Me._arm = New isr.Scpi.ArmSubsystem(Me)
        Me._subsystems.Add(Me._arm)

        ' read and interpret the instrument identity allowing  use of the version info to
        ' set specific properties as necessary.
        Me.ReadIdentity()

        Me._resettableValues = New isr.Scpi.ResettableCollection(Of isr.Scpi.IResettable)

        If Not executed Then
            executed = True

            ' clear status and disable service request on all operations.
            Me.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.ServiceRequests.None

            ' register the handler before enabling the event
            AddHandler Me._gpibSession.ServiceRequest, AddressOf OnVisaServiceRequest
            Me._gpibSession.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)

        End If

        Return Not Me.HadError

    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Overrides Function Preset() As Boolean Implements isr.Scpi.IDevice.PresetKnownState

        ' preset the Resettable values.
        Me._resettableValues.PresetKnownState()

        ' preset the subsystems.
        Return Me._subsystems.PresetKnownState()

    End Function

    ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
    '''   (RST), Clear Status (CLS, and clear error queue.</summary>
    ''' <history>
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function ResetAndClear() As Boolean

        Try

            ' clear the device
            Me.ClearActiveState()

            ' reset the device and set default properties
            Me.ResetKnownState()

            ' Clear the device Status and set more defaults
            Me.ClearExecutionState()

            ' preset known state
            Me.Preset()

            Me.OnMessageAvailable(TraceEventType.Verbose, "RESET AND CLEARED", "reset and cleared.")

            Return Not Me.HadError

        Catch ex As Exception

            Me.OnMessageAvailable(TraceEventType.Error, "FAILED RESETTING AND CLEARING", "Failed resetting and clearing. Details: {0}", ex)

        End Try

    End Function

    ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
    '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
    '''   than the minimal timeout set for the session.  Typically, the source meter may
    '''   required a 5000 milliseconds timeout.</summary>
     ''' <remarks>This also sets output to Auto Clear.</remarks>
    Public Overridable Overloads Function ResetAndClear(ByVal timeout As Integer) As Boolean

        If Me._gpibSession Is Nothing Then
            Return True
        End If

        Try
            Me.GpibSession.StoreTimeout(Math.Max(Me._gpibSession.Timeout, timeout))
            Dim outcome As Boolean = ResetAndClear()
            Return outcome
        Catch
            Throw
        Finally
            Me.GpibSession.RestoreTimeout()
        End Try

    End Function

    ''' <summary>Returns the device to its default known state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overrides Function ResetKnownState() As Boolean Implements isr.Scpi.IDevice.ResetKnownState

        If Me._gpibSession Is Nothing Then
            Return False
        End If
        Try
            Me._gpibSession.ResetKnownState()

            ' reset the Resettable values.
            Me._resettableValues.ResetKnownState()

            ' reset the subsystems.
            Return Me._subsystems.ResetKnownState()

        Catch ex As NationalInstruments.VisaNS.VisaException
            If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT RESETTING KNOWN STATE",
                                      "Timeout occurred when resetting known state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
            Else
                Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                      "VISA Exception occurred when resetting known state. Details: {0}.", ex)
            End If
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                  "Exception occurred when resetting known state. Details: {0}.", ex)
        End Try
        Return False

    End Function

#End Region

#Region " I CONNECTABLE RESOURCE "

    ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
    ''' <param name="resourceName">Specifies the instrument resource name.</param>
    ''' <returns>Returns True if success or false if it failed opening the session.</returns>
    ''' <exception cref="OperationOpenException" guarantee="strong"></exception> 
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

        Me.ResourceName = resourceName
        Dim lastAction As String = String.Empty

        Try

            If Me.UsingDevices Then

                ' open a GPIB session to this instrument
                Me._gpibSession = New isr.Visa.GpibSession(resourceName)

                If Me._gpibSession Is Nothing Then

                    Me.Disconnect()

                Else

                    ' initialize execution state.
                    Me.InitializeExecutionState()

                    ' issue a selective device clear and total clear of the instrument.
                    If Me.ResetAndClear() Then

                        Me.OnMessageAvailable(TraceEventType.Information, "SESSION OPENED", "Session opened.")

                        lastAction = "displaying instrument information"
                        Me.OnMessageAvailable(TraceEventType.Verbose, "CONNECTED",
                                              "Connected to {1}{0}Manufacturer name: {2}{0}Manufacturer ID: 0x{3:X}{0}SCPI Version: {4:0.0###}",
                                              Environment.NewLine,
                                              Me.ReadIdentity(),
                                              Me.GpibSession.ResourceManufacturerName,
                                              Me.GpibSession.ResourceManufacturerID,
                                              Me.SystemSubsystem.ScpiRevision(isr.Scpi.ResourceAccessLevels.None))

                        lastAction = "connected"
                        MyBase.Connect(resourceName)

                    End If

                End If

            Else

                lastAction = "connected"
                MyBase.Connect(resourceName)

            End If

        Catch ex As isr.Visa.BaseException

            ' close to meet strong guarantees
            Try
                Me.Disconnect()
            Finally
            End Try

            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CONNECTING", "Exception occurred {0}. Details: {1}", lastAction, ex)

        End Try

        Return Me.IsConnected

    End Function

    ''' <summary>Connects this instance.</summary>
    ''' <returns><c>True</c> if the instance connected.</returns>
    ''' <param name="resourceName">Specifies the name of the resource to which
    ''' to connect.</param>
    ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
    Public Overloads Overrides Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
        Me.ResourceTitle = resourceTitle
        Return Me.Connect(resourceName)
    End Function

    ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
    ''' <param name="address">Specifies the primary address of the instrument.</param>
    ''' <returns>Returns True if success or false if it failed opening the session.</returns>
    ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
    Public Overloads Overrides Function Connect(ByVal address As Integer) As Boolean

        Return Me.Connect(GpibSession.BuildResourceName(address))

    End Function

    ''' <summary>Disconnect the instrument.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overrides Function Disconnect() As Boolean

        If Not Me.IsConnected Then
            Return Not Me.IsConnected
        End If
        Try
            Return MyBase.Disconnect
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", "Exception occurred disconnecting. Details: {0}", ex)
        End Try

    End Function

    ''' <summary>
    ''' True when using actual devices or False for running the
    ''' program without connecting to devices.
    ''' </summary>
    Public Property UsingDevices() As Boolean Implements IInstrument.UsingDevices, isr.Scpi.IDevice.UsingDevices

    ''' <summary>
    ''' Gets or sets the resource name.
    ''' </summary>
    ''' <value><c>ResourceName</c> is a String property.</value>
    Public Overrides Property ResourceName() As String Implements isr.Scpi.IDevice.ResourceName
        Get
            Return MyBase.ResourceName
        End Get
        Set(ByVal value As String)
            MyBase.ResourceName = value
        End Set
    End Property

#End Region

#Region " I FAILURE PUBLISHER "

    ''' <summary>Raised if an error had occurred.</summary>
    Public Event FailureOccurred As EventHandler(Of System.EventArgs) Implements isr.Core.IFailurePublisher.FailureOccurred

    ''' <summary>Raises the error available event.</summary>
    ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
    Protected Overridable Sub OnFailureOccurred(ByVal e As System.EventArgs) Implements isr.Core.IFailurePublisher.OnFailureOccurred
        FailureOccurredEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " RESOURCE ACCESS MANAGEMENT "

    ''' <summary>
    ''' Gets or sets reference to the level of <see cref="isr.scpi.ResourceAccessLevels">resource access.</see>
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AccessLevel() As isr.Scpi.ResourceAccessLevels Implements isr.Scpi.IDevice.AccessLevel

    Private _accessStack As New Generic.Stack(Of isr.Scpi.ResourceAccessLevels)
    ''' <summary>
    ''' Saves the access level.
    ''' </summary>
    Public Sub StoreResourceAccessLevel() Implements isr.Scpi.IDevice.StoreResourceAccessLevel
        Me._accessStack.Push(Me._accessLevel)
    End Sub

    ''' <summary>
    ''' Restores the access level.
    ''' </summary>
    Public Sub RestoreResourceAccessLevel() Implements isr.Scpi.IDevice.RestoreResourceAccessLevel
        If Me._accessStack.Count > 0 Then
            Me._accessLevel = Me._accessStack.Pop
        End If
    End Sub

#End Region

#Region " FLUSH "

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

        Dim timedOut As Boolean = False
        Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

            timedOut = Not Me.AwaitMessageAvailable(timeout, pollDelay)
            If Not timedOut Then

                If reportUnreadData Then

                    Dim flushed As String = Me.ReadLine()
                    If Not String.IsNullOrWhiteSpace(flushed) Then
                        Me.OnMessageAvailable(TraceEventType.Verbose, "Flushed '{0}'", isr.Visa.My.MyLibrary.InsertCommonEscapeSequences(flushed))
                    End If

                Else
                    Me.ReadLine()
                    'Me.Session.DiscardUnreadData()

                End If
            End If

        Loop

        Return Me.IsLastVisaOperationSuccess

    End Function

    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read.
    ''' </summary>
    Public Sub DiscardUnreadData() Implements isr.Scpi.IDevice.DiscardUnreadData
        If Me._gpibSession IsNot Nothing Then
            Me._gpibSession.DiscardUnreadData(16)
        End If
    End Sub

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overridable Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.DiscardUnreadData

        Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

    End Function

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.FlushRead

        Try
            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED FLUSHING READ BUFFERS",
                                  "Exception occurred flushing read buffers. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
        End Try

    End Function

    ''' <summary>
    ''' Flush the write buffers.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _flushWrite()

        Try

            ' flush read buffer.
            Me.GpibSession.FlushWrite()

            If Not Me.IsLastVisaOperationSuccess Then
                Me.OnMessageAvailable(TraceEventType.Warning, "LAST VISA OPERATION FAILED", "Last visa operation failed. VISA status={0}.{1}{2}", Me.LastVisaStatus,
                                      Environment.NewLine, isr.Core.StackTraceParser.UserCallStack(4, 4))
            End If

        Catch ex As Exception

            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED FLUSHING WRITE BUFFERS",
                                  "Exception occurred flushing write buffers. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)

        Finally

        End Try

    End Sub

    ''' <summary>
    ''' Flush the write buffers.
    ''' </summary>
    Public Overridable Sub FlushWrite()
        Me._flushWrite()
    End Sub

#End Region

#Region " QUERY "

    ''' <summary>Queries the GPIB instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean) Implements isr.Scpi.IDevice.QueryBoolean
        Dim queryResult As String = Me.QueryTrimEnd(question)
        Return MessageBasedReader.ParseBoolean(queryResult)
    End Function

    ''' <summary>Queries the GPIB instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryDouble(ByVal question As String) As Nullable(Of Double) Implements isr.Scpi.IDevice.QueryDouble
        Dim queryResult As String = Me.QueryTrimEnd(question)
        Return MessageBasedReader.ParseDouble(queryResult)
    End Function

    ''' <summary>Queries the GPIB instrument and returns an Integer value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInteger(ByVal question As String) As Nullable(Of Integer) Implements isr.Scpi.IDevice.QueryInteger
        Dim queryResult As String = Me.QueryTrimEnd(question)
        Return MessageBasedReader.ParseInteger(queryResult)
    End Function

    ''' <summary>Queries the GPIB instrument and returns an Integer value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInfInteger(ByVal question As String) As Nullable(Of Integer) Implements isr.Scpi.IDevice.QueryInfInteger
        Dim queryResult As String = Me.QueryTrimEnd(question)
        Return MessageBasedReader.ParseInfInteger(queryResult)
    End Function

    ''' <summary>
    ''' Sends the query and reads the reply.
    ''' </summary>
    ''' <param name="query"></param>
    Public Function QueryString(ByVal query As String) As String Implements isr.Scpi.IDevice.QueryString
        Me.WriteLine(query)
        Return Me.ReadLine()
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryTrimEnd(ByVal question As String) As String Implements isr.Scpi.IDevice.QueryTrimEnd, IInstrument.QueryTrimEnd
        Me.WriteLine(question)
        Return Me.ReadLineTrimEnd
    End Function

    ''' <summary>
    ''' Queries the GPIB instrument and returns a string save the termination character.
    ''' </summary>
    ''' <param name="format">Specifies the format statement.</param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    ''' <returns>System.String.</returns>
    ''' <exception cref="System.ArgumentNullException">format</exception>
    Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IInstrument.QueryTrimEnd
        If String.IsNullOrWhiteSpace(format) Then
            Throw New ArgumentNullException("format")
        End If
        Me.WriteLine(format, args)
        Return Me.ReadLineTrimEnd()
    End Function


    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimNewLine(ByVal question As String) As String Implements isr.Scpi.IDevice.QueryTrimNewLine
        Me._receiveBuffer = Me.QueryString(question)
        Return Me._receiveBuffer.TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function

#End Region

#Region " READ "

    ''' <summary>
    ''' Gets or sets the default size used to read strings.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DefaultStringSize() As Integer Implements isr.Scpi.IDevice.DefaultStringSize
        Get
            If Me.GpibSession Is Nothing OrElse Me.GpibSession.Reader Is Nothing Then
                Return 0
            Else
                Return GpibSession.Reader.DefaultStringSize
            End If
        End Get
        Set(ByVal value As Integer)
            If Me.GpibSession IsNot Nothing AndAlso Me.GpibSession.Reader IsNot Nothing Then
                GpibSession.Reader.DefaultStringSize = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the termination character.
    ''' </summary>
    Public Property TerminationCharacter() As Byte Implements isr.Scpi.IDevice.TerminationCharacter

    ''' <summary>Gets or sets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Private _receiveBuffer As String

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property ReceiveBuffer() As String Implements isr.Scpi.IDevice.ReceiveBuffer
        Get
            Return Me._receiveBuffer
        End Get
    End Property

    ''' <summary>Reads the GPIB instrument and returns a Boolean value.</summary>
    Public Function ReadBoolean() As Nullable(Of Boolean) Implements isr.Scpi.IDevice.ReadBoolean
        Me._receiveBuffer = Me.ReadLineTrimEnd()
        Return MessageBasedReader.ParseBoolean(Me._receiveBuffer)
    End Function

    ''' <summary>Reads the GPIB instrument and returns a double value.</summary>
    Public Function ReadDouble() As Nullable(Of Double) Implements isr.Scpi.IDevice.ReadDouble
        Me._receiveBuffer = Me.ReadLineTrimEnd()
        Return MessageBasedReader.ParseDouble(Me._receiveBuffer)
    End Function

    ''' <summary>Reads the GPIB instrument and returns an Integer value.</summary>
    Public Function ReadInteger() As Nullable(Of Integer) Implements isr.Scpi.IDevice.ReadInteger
        Me._receiveBuffer = Me.ReadLineTrimEnd()
        Return MessageBasedReader.ParseInteger(Me._receiveBuffer)
    End Function

    ''' <summary>Reads the GPIB instrument and returns an Integer value.</summary>
    Public Function ReadInfInteger() As Nullable(Of Integer) Implements isr.Scpi.IDevice.ReadInfInteger
        Me._receiveBuffer = Me.ReadLineTrimEnd()
        Return MessageBasedReader.ParseInfInteger(Me._receiveBuffer)
    End Function

    ''' <summary>Reads a string from GPIB instrument and returns the string save the termination character.</summary>
    Public Function ReadLine() As String Implements isr.Scpi.IDevice.ReadLine
        If Me._gpibSession Is Nothing Then
            Return String.Empty
        Else
            Me._receiveBuffer = Me._gpibSession.ReadLine()
            Return Me._receiveBuffer
        End If
    End Function

    ''' <summary>Reads a string from GPIB instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadLineTrimEnd() As String Implements isr.Scpi.IDevice.ReadLineTrimEnd
        If Me._gpibSession Is Nothing Then
            Return String.Empty
        Else
            Me._receiveBuffer = Me._gpibSession.ReadLineTrimEnd()
            Return Me._receiveBuffer
        End If
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    Public Overloads Function ReadLineTrimEnd(ByVal terminationCharacters As Char()) As String Implements isr.Scpi.IDevice.ReadLineTrimEnd
        Me._receiveBuffer = Me.ReadLine()
        Return Me._receiveBuffer.TrimEnd(terminationCharacters)
    End Function

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Public Overloads Function ReadLineTrimEnd(ByVal terminationCharacter As Byte) As String Implements isr.Scpi.IDevice.ReadLineTrimEnd
        Return Me.ReadLineTrimEnd(New Char() {Convert.ToChar(terminationCharacter)})
    End Function

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Public Overloads Function ReadLineTrimNewLine() As String Implements isr.Scpi.IDevice.ReadLineTrimNewLine
        Return Me.ReadLineTrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Public Overloads Function ReadLineTrimLinefeed() As String Implements isr.Scpi.IDevice.ReadLineTrimLinefeed
        Return Me.ReadLineTrimEnd(10)
    End Function

#End Region

#Region " WRITE "

    ''' <summary>Gets or sets the last message that was transmitted to the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Private _transmitBuffer As String

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property TransmitBuffer() As String Implements isr.Scpi.IDevice.TransmitBuffer
        Get
            Return Me._transmitBuffer
        End Get
    End Property

    ''' <summary>Writes to instrument.</summary>
    ''' <param name="value">The string to output.</param>
    Public Sub WriteLine(ByVal value As String) Implements isr.Scpi.IDevice.WriteLine
        If Me._gpibSession IsNot Nothing Then
            Me._transmitBuffer = value
            Me._gpibSession.Write(value)
        End If
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    Public Sub WriteLine(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As isr.Scpi.BooleanDataFormat) Implements isr.Scpi.IDevice.WriteLine
        Me.WriteLine(isr.Scpi.Syntax.BuildCommand(queryCommand, value, boolFormat))
    End Sub

    ''' <summary>
    ''' Outputs a command based on the format string and the specified values.
    ''' </summary>
      Public Sub Writeline(ByVal format As String, ByVal ParamArray values() As Object) Implements isr.Scpi.IDevice.WriteLine
        Me.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, values))
    End Sub

#End Region

#Region " COMMAND BUILDERS: QUERIES "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="queryCommand ">Specifies the command sub header for a command string in the form '{0}?' where 
    ''' the first item is the <paramref name="queryCommand">command</paramref>.
    ''' </param>
    Public Shared Function BuildQuery(ByVal queryCommand As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                            "{0}?", queryCommand)
    End Function

#End Region

#Region " COMMAND BUILDERS "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax
    ''' </summary>
    ''' <param name="queryCommand">Specifies the command sub header for a command string in the form '{0} {1}' where 
    ''' the first item is a <paramref name="queryCommand">command</paramref> and 
    ''' the second item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal queryCommand As String, ByVal value As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                            "{0} {1}", queryCommand, value)
    End Function

#End Region

#Region " PROPERTY GETTERS "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="queryCommand">Specifies the query Command.</param>
    ''' <param name="value">Specifies the current value.</param>
    ''' <param name="access">Specifies the desired access level to the instrument.</param>
    Public Function Getter(ByVal queryCommand As String, ByVal value As String,
                           ByVal access As isr.Scpi.ResourceAccessLevels) As String
        If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(value) Then
            value = Me.QueryTrimEnd(queryCommand)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="queryCommand">Specifies the query Command.</param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Getter(ByVal queryCommand As String,
                           ByVal nullableValue As isr.Scpi.ResettableValue(Of Integer),
                           ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.ResettableValue(Of Integer)
        If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue = Me.QueryInteger(queryCommand)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="queryCommand">Specifies the query Command.</param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Setter(ByVal queryCommand As String, ByVal nullableValue As isr.Scpi.ResettableValue(Of Integer),
                           ByVal value As Integer,
                           ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.ResettableValue(Of Integer)
        If isr.Scpi.Subsystem.IsCacheAccess(access) Then
            Return value
        End If
        If isr.Scpi.Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.WriteLine(Controller.BuildCommand(queryCommand, value))
        End If
        If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
            Dim actual As New isr.Scpi.ResettableValue(Of Integer)
            actual = Me.Getter(queryCommand, actual, access)
            If value <> actual Then
                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Controller.BuildCommand(queryCommand, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If
            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

#End Region

#Region " REGISTERS "

    ''' <summary>
    ''' Returns the error queue and event status register (ESR) report.
    ''' </summary>
    Public MustOverride ReadOnly Property DeviceErrors() As String Implements IInstrument.DeviceErrors

    ''' <summary>Returns True if the last operation reported an error by
    '''   way of a service request.</summary>
    Public ReadOnly Property HadError() As Boolean Implements isr.Scpi.IDevice.HadError
        Get
            Return Me._lastServiceEventArgs.HasError OrElse Me._gpibSession.HasError(isr.Scpi.ServiceRequests.ErrorAvailable)
#If DEBUG Then
            ' In debug mode, make sure to wait for the end of service
            ' request processing to get the error immediately after
            ' the instrument reports it.  In this mode, we expect to have
            ' syntax error that the instrument is unlikely to be happy about.
            ' once in release mode, the incidences are lower and we can wait for
            ' the error rather than slowing operations due to this delay
            Return Me.HadError(10)
#Else
        Windows.Forms.Application.DoEvents()
        Return Me._lastServiceEventArgs.HasError
#End If
        End Get
    End Property

    ''' <summary>Returns True if the last operation reported an error by
    '''   way of a service request.</summary>
    ''' <param name="waitMilliseconds">Time to wait before returning the error.</param>
    Private ReadOnly Property HadError(ByVal waitMilliseconds As Integer) As Boolean
        Get
            ' add a couple of milliseconds wait
            Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, waitMilliseconds))
            Do
                '         Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(Math.Min(10, waitMilliseconds))
            Loop While Me._lastServiceEventArgs.ServicingRequest AndAlso (Date.Now <= endTime)
            Return Me._lastServiceEventArgs.HasError
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition that determines if the device supports OPC.
    ''' </summary>
    ''' <remarks>
    ''' Some instruments provide only partial support of IEEE488.2 not supporting the query for 
    ''' determining completion of operations. For these instruments, another method needs to be 
    ''' used such as polling.
    ''' </remarks>
    Public Property SupportsOperationComplete() As Boolean Implements isr.Scpi.IDevice.SupportsOperationComplete

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    Public Function OperationCompleted() As Boolean
        If Me._gpibSession Is Nothing Then
            Return True
        End If
        Me._lastServiceEventArgs.OperationCompleted = Me._gpibSession.OperationCompleted
        Return Me._lastServiceEventArgs.OperationCompleted
    End Function

    ''' <summary>Gets or sets the last service event arguments
    '''   <see cref="isr.Scpi.BaseServiceEventArgs">status</see></summary>
    ''' <remarks>Also used to hold values read from instrument by way of static methods
    '''   such as *OPC?</remarks>
    Public Property LastServiceEventArgs() As isr.Scpi.BaseServiceEventArgs Implements isr.Scpi.IDevice.LastServiceEventArgs

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public Function SerialPoll() As isr.Scpi.ServiceRequests Implements isr.Scpi.IDevice.SerialPoll
        Return CType(Me.GpibSession.ReadStatusByte, isr.Scpi.ServiceRequests)
    End Function

    ''' <summary>
    ''' Serial polls the device and returns the <see cref="isr.Scpi.ServiceRequests">service requests bits.</see>
    ''' </summary>
    Function ReadStatusByte() As isr.Scpi.ServiceRequests Implements isr.Scpi.IDevice.ReadStatusByte
        Return Me.SerialPoll
    End Function

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    Function ReadStandardEventStatus() As Integer Implements isr.Scpi.IDevice.ReadStandardEventStatus
        Return Me.Status.StandardEventStatus(isr.Scpi.ResourceAccessLevels.Device)
    End Function


#End Region

#Region " MAV "

    ''' <summary>
    ''' Awaits the message available bit.
    ''' </summary>
    ''' <param name="timeout">The timeout.</param>
    ''' <param name="pollDelay">The poll delay.</param>
    ''' <returns>
    ''' <c>True</c> if message available or false if not.
    ''' </returns>
    Public Function AwaitMessageAvailable(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean Implements isr.Scpi.IDevice.AwaitMessageAvailable
        Dim stopWatch As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
        Dim affirmative As Boolean = Me.IsMessageAvailable
        Dim nextPollTime As Long = stopWatch.ElapsedMilliseconds + pollDelay
        Do
            Windows.Forms.Application.DoEvents()
            If stopWatch.ElapsedMilliseconds > nextPollTime Then
                affirmative = Me.IsMessageAvailable
                nextPollTime = stopWatch.ElapsedMilliseconds + pollDelay
            End If
        Loop Until affirmative OrElse stopWatch.ElapsedMilliseconds > timeout
        Return affirmative OrElse Me.IsMessageAvailable
    End Function

    ''' <summary>Reads the device status data byte and returns True
    ''' if the message available bits were set.
    ''' </summary>
    ''' <returns>True if the device has data in the queue
    ''' </returns>
    Public Overridable Function IsMessageAvailable() As Boolean Implements isr.Scpi.IDevice.IsMessageAvailable

        ' check if we have data in the queue
        Return (Me.ReadStatusByte And Me._messageAvailableBits) <> 0

    End Function

    ''' <summary>Gets or sets the serial poll bit value for determining
    ''' if a message is available.  This is required in order to flush the
    ''' read buffer as the GPIB formatted I/O does not seem to work right.
    ''' </summary>
    Public Property MessageAvailableBits() As isr.Scpi.ServiceRequests

#End Region

#Region " IDENTITY AND VERSION INFORMATION "

    ''' <summary>
    ''' Gets or sets the version information.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property VersionInfo() As isr.Scpi.IVersionInfo Implements isr.Scpi.IDevice.VersionInfo

    Private _identity As String
    ''' <summary>
    ''' Queries the instrument and returns the Identity string..
    ''' </summary>
    Public Overridable Function ReadIdentity() As String Implements isr.Scpi.IDevice.ReadIdentity
        If String.IsNullOrWhiteSpace(Me._identity) Then
            If Me._gpibSession Is Nothing Then
                Me._identity = "GPIB Corp., Model 1, 123, A"
            Else
                Me._identity = Me._gpibSession.ReadIdentity
            End If
        End If
        Return Me._identity
    End Function

#End Region

#Region " GPIB VISA INTERFACE METHODS "

    Private _gpibInterface As VisaNS.GpibInterface
    ''' <summary>Gets or sets reference to the GPIB interface for this instrument.</summary>
    Public ReadOnly Property GpibInterface() As VisaNS.GpibInterface
        Get
            If Me._gpibInterface Is Nothing AndAlso Me.UsingDevices Then
                Me._gpibInterface = isr.Visa.GpibInterface.OpenInterface(Me._gpibSession.HardwareInterfaceName & "::INTFC")
            End If
            Return Me._gpibInterface
        End Get
    End Property

    ''' <summary>Issues an interface clear.</summary>
    Public Sub InterfaceClear()

        If GpibInterface IsNot Nothing Then
            GpibInterface.SendInterfaceClear()
        End If

    End Sub

#End Region

#Region " GPIB VISA SESSION "

    Private _gpibSession As isr.Visa.GpibSession
    ''' <summary>Gets or sets reference to the GPIB session.</summary>
    Public ReadOnly Property GpibSession() As isr.Visa.GpibSession
        Get
            Return Me._gpibSession
        End Get
    End Property

    ''' <summary> Waits the on event. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> true if it succeeds, false if timeout. </returns>
    Public Function WaitOnEvent(ByVal timeout As Integer) As Boolean
        If Me._gpibSession IsNot Nothing Then
            Me._gpibSession.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
        End If
    End Function

    ''' <summary> Restores the last timeout from the stack. </summary>
    Public Sub RestoreTimeout() Implements isr.Scpi.IDevice.RestoreTimeout
        Me.GpibSession.RestoreTimeout()
    End Sub

    ''' <summary> Saves the current timeout and sets a new setting timeout. </summary>
    ''' <param name="timeout"> Specifies the new timeout. </param>
    Public Sub StoreTimeout(ByVal timeout As Integer) Implements isr.Scpi.IDevice.StoreTimeout
        Me.GpibSession.StoreTimeout(timeout)
    End Sub

#End Region

#Region " isr.Scpi SUBSYSTEMS "

    Private _subsystems As isr.Scpi.ResettableCollection(Of isr.Scpi.Subsystem)

    Private _arm As isr.Scpi.ArmSubsystem
    ''' <summary>Gets or sets reference to the ARM isr.Scpi subsystem.</summary>
    Public ReadOnly Property Arm() As isr.Scpi.ArmSubsystem
        Get
            Return Me._arm
        End Get
    End Property

    Private _calculate2 As isr.Scpi.Calculate2Subsystem
    ''' <summary>Gets or sets reference to the Calculate 1 isr.Scpi subsystem.</summary>
    Public ReadOnly Property Calculate2() As isr.Scpi.Calculate2Subsystem
        Get
            Return Me._calculate2
        End Get
    End Property

    Private _calculate3 As isr.Scpi.Calculate3Subsystem
    ''' <summary>Gets or sets reference to the Calculate 3 isr.Scpi subsystem.</summary>
    Public ReadOnly Property Calculate3() As isr.Scpi.Calculate3Subsystem
        Get
            Return Me._calculate3
        End Get
    End Property

    Private _display As isr.Scpi.DisplaySubsystem
    ''' <summary>Gets or sets reference to the Display isr.Scpi subsystem.</summary>
    Public ReadOnly Property Display() As isr.Scpi.DisplaySubsystem
        Get
            Return Me._display
        End Get
    End Property

    Private _format As isr.Scpi.FormatSubsystem
    ''' <summary>Gets or sets reference to the Format isr.Scpi subsystem.</summary>
    Public ReadOnly Property Format() As isr.Scpi.FormatSubsystem
        Get
            Return Me._format
        End Get
    End Property

    Private _output As isr.Scpi.OutputSubsystem
    ''' <summary>Gets or sets reference to the Output isr.Scpi subsystem.</summary>
    Public ReadOnly Property Output() As isr.Scpi.OutputSubsystem
        Get
            Return Me._output
        End Get
    End Property

    Private _route As isr.Scpi.RouteSubsystem
    ''' <summary>Gets or sets reference to the Route isr.Scpi subsystem.</summary>
    Public ReadOnly Property Route() As isr.Scpi.RouteSubsystem
        Get
            Return Me._route
        End Get
    End Property

    Private _sense As isr.Scpi.SenseSubsystem
    ''' <summary>Gets or sets reference to the Sense isr.Scpi subsystem.</summary>
    Public ReadOnly Property Sense() As isr.Scpi.SenseSubsystem
        Get
            Return Me._sense
        End Get
    End Property

    Private _source As isr.Scpi.SourceSubsystem
    ''' <summary>Gets or sets reference to the Source isr.Scpi subsystem.</summary>
    Public ReadOnly Property Source() As isr.Scpi.SourceSubsystem
        Get
            Return Me._source
        End Get
    End Property

    Private _status As isr.Scpi.StatusSubsystem
    ''' <summary>Gets or sets reference to the Status isr.Scpi subsystem.</summary>
    Public ReadOnly Property Status() As isr.Scpi.StatusSubsystem
        Get
            Return Me._status
        End Get
    End Property

    Private _system As isr.Scpi.SystemSubsystem
    ''' <summary>Gets or sets reference to the System isr.Scpi subsystem.</summary>
    Public ReadOnly Property SystemSubsystem() As isr.Scpi.SystemSubsystem Implements isr.Scpi.IDevice.SystemSubsystem
        Get
            Return Me._system
        End Get
    End Property

    Private _trace As isr.Scpi.TraceSubsystem
    ''' <summary>Gets or sets reference to the Trace isr.Scpi subsystem.</summary>
    Public ReadOnly Property Trace() As isr.Scpi.TraceSubsystem
        Get
            Return Me._trace
        End Get
    End Property

    Private _trigger As isr.Scpi.TriggerSubsystem
    ''' <summary>Gets or sets reference to the Trigger isr.Scpi subsystem.</summary>
    Public ReadOnly Property Trigger() As isr.Scpi.TriggerSubsystem
        Get
            Return Me._trigger
        End Get
    End Property

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>Raised upon receiving a service request.  This event does not expose the
    '''   internal service request arguments directly.</summary>
    Public Event ServiceRequest As EventHandler(Of isr.Scpi.BaseServiceEventArgs)

    ''' <summary> Raises the service request event. </summary>
    ''' <param name="e"> Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">service
    ''' request event arguments</see>. </param>
    ''' <history date="04/08/11" by="David" revision="3.0.4113.x"> The none event is raised in the CAOT application. 
    '''                                                            Use information trace level to alert on the
    ''' none event. </history>
    Protected Overridable Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs)

        If e Is Nothing Then

        ElseIf e.HasError Then

            Me.OnMessageAvailable(TraceEventType.Error, "ERROR REPORTED", "Instrument reported an error:{0}{1}", Environment.NewLine, e.LastError)

        Else

            Dim eventReported As Boolean
            ' Me.OnMessageAvailable(TraceEventType.verbose,"Operation completed in {0}ms", e.OperationElapsedTime.TotalMilliseconds())
            If (e.ServiceRequestStatus And isr.Scpi.ServiceRequests.MeasurementEvent) <> 0 Then
                eventReported = True
                Me.OnMessageAvailable(TraceEventType.Verbose, "MEASUREMENT EVENT", "Measurement event {0}", e.MeasurementEventStatus)
            End If
            If (e.ServiceRequestStatus And isr.Scpi.ServiceRequests.MessageAvailable) <> 0 Then
                eventReported = True
                Me.OnMessageAvailable(TraceEventType.Verbose, "MESSAGE AVAILABLE", "Message available")
            End If
            If (e.ServiceRequestStatus And isr.Scpi.ServiceRequests.OperationEvent) <> 0 Then
                eventReported = True
                Me.OnMessageAvailable(TraceEventType.Verbose, "OPERATION EVENT", "Operation event {0}", e.OperationEventStatus)
            End If
            If (e.ServiceRequestStatus And isr.Scpi.ServiceRequests.StandardEvent) <> 0 Then
                eventReported = True
                Me.OnMessageAvailable(TraceEventType.Verbose, "STANDARD EVENT", "Standard event {0}", e.StandardEventStatus)
            End If
            If (e.ServiceRequestStatus And isr.Scpi.ServiceRequests.SystemEvent) <> 0 Then
                eventReported = True
                Me.OnMessageAvailable(TraceEventType.Verbose, "SYSTEM EVENT", "System Event")
            End If

            If Not eventReported Then
                Me.OnMessageAvailable(TraceEventType.Information, "UNKNOWN EVENT", "Unknown event {0}", e.ServiceRequestStatus)
            End If

        End If

        ServiceRequestEvent.SafeInvoke(Me, e)
        If e IsNot Nothing Then
            e.ServicingRequest = False
        End If

    End Sub

    ''' <summary>Handles service requests from the instrument.</summary>
    Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

        If e IsNot Nothing AndAlso Not Me._lastServiceEventArgs.ServicingRequest Then
            ' do not initialize the last service event arguments because this is done with Clear Status.
            Me._lastServiceEventArgs.ServicingRequest = True
            Me._lastServiceEventArgs.ProcessServiceRequest()
            Me._lastServiceEventArgs.ReadRegisters()
            Try
                Me.OnServiceRequest(Me._lastServiceEventArgs)
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED PROCESSING VISA SERVICE REQUEST",
                                      "Exception occurred processing visa service request. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                Me.OnFailureOccurred(New System.EventArgs)
            End Try
        End If

    End Sub

#End Region

#Region " VISA STATUS "

    ''' <summary>
    ''' Returns true if the last VISA operation ended successfully.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
        Get
            Return Me.LastVisaStatus = NationalInstruments.VisaNS.VisaStatusCode.Success
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
        Get
            If Me._gpibSession Is Nothing Then
                Return False
            Else
                Return Me._gpibSession.IsLastVisaOperationFailed
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
        Get
            If Me._gpibSession Is Nothing Then
                Return False
            Else
                Return Me._gpibSession.IsLastVisaOperationWarning
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets the last VISA Status.
    ''' </summary>
    Public ReadOnly Property LastVisaStatus() As Integer Implements IInstrument.LastVisaStatus '  NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
        Get
            If Me._gpibSession Is Nothing Then
                Return NationalInstruments.VisaNS.VisaStatusCode.Success
            Else
                Return Me._gpibSession.LastStatus
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    Public ReadOnly Property LastVisaStatusDetails() As String Implements IInstrument.LastVisaStatusDetails
        Get
            Return Me._gpibSession.BuildLastVisaStatusDetails()
        End Get
    End Property

#End Region

#Region " RESOURCE MANAGEMENT "

    ''' <summary>
    ''' Returns true if having a valid primary address.
    ''' </summary>
    ''' <param name="address">The device primary address</param>
    Public Shared Function IsValidPrimaryAddress(ByVal address As Integer) As Boolean
        Return (address >= 1) AndAlso (address <= 31)
    End Function

    ''' <summary>Gets or sets the primary address of the instrument on the GPIB bus.</summary>
    Public ReadOnly Property PrimaryAddress() As Int32
        Get
            If Me._gpibSession Is Nothing Then
                Return 0
            Else
                Return Me._gpibSession.PrimaryAddress
            End If
        End Get
    End Property

    ''' <summary>Gets the board number.</summary>
    Public ReadOnly Property BoardNumber() As Integer
        Get
            If Me._gpibSession Is Nothing Then
                Return 0
            Else
                Return Me._gpibSession.BoardNumber
            End If
        End Get
    End Property

#End Region

End Class

