﻿''' <summary>
''' A base class implementing an <see cref="IControllerPublisher">Controller Publisher</see> with synchronization.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/08/2010" by="David" revision="1.2.3691.x">
''' Created
''' </history>
Public MustInherit Class ControllerPublisherBase

    Inherits isr.Core.SyncPublisherBase
    Implements IControllerPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class for linking to a controller type instrument.
    ''' </summary>
    ''' <param name="controller">Specifies a reference to the instrument interface/</param>
    ''' <param name="synchronizer">Specifies reference to a class implementing the 
    ''' <see cref="System.ComponentModel.ISynchronizeInvoke">synchronizer</see> contract</param>
    Protected Sub New(ByVal controller As isr.Visa.Instruments.Controller, ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)
        MyBase.New(synchronizer)
        Me._controller = controller
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' remove reference to the instrument
                    Me._controller = Nothing

                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' set the sentinel indicating that the class was disposed.
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " CONTROLLER "

    Private WithEvents _controller As isr.Visa.Instruments.Controller

    ''' <summary>Gets reference to the controller.</summary>
    Public ReadOnly Property Controller() As isr.Visa.Instruments.Controller Implements IControllerPublisher.Controller
        Get
            Return Me._controller
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the controller is connected.
    ''' </summary>
    Public Function IsConnected() As Boolean Implements IControllerPublisher.IsConnected
        Return Me._controller IsNot Nothing AndAlso Me._controller.IsConnected
    End Function

#End Region

End Class
