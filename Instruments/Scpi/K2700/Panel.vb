Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ComboBoxExtensions
Imports isr.Core.ControlExtensions
Imports isr.Core.LabelExtensions
Imports isr.Core.TextBoxExtensions
Imports System.ComponentModel

Namespace K2700

    ''' <summary>Provides a user interface for the Keithley 27XX instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    <System.ComponentModel.DisplayName("K2700 Panel"),
      System.ComponentModel.Description("Keithley 2700 Instrument Family - Windows Forms Custom Control"),
      System.Drawing.ToolboxBitmap(GetType(K2700.Panel))>
    Public Class Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New K2700.Instrument(Me, "K2700")

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                Me._messagesBox.PrependMessage(e.ExtendedMessage)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

        ''' <summary>Update the display based on the current instrument values.</summary>
        Private Sub refreshDisplay()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If

            Me._terminalsToggle.SafeCheckedSetter(Not Me.Instrument.SystemSubsystem.FrontSwitched(isr.Scpi.ResourceAccessLevels.Device))
            Me._senseRangeTextBox.SafeTextSetter(Me.Instrument.SenseRange(isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            Me._integrationPeriodTextBox.SafeTextSetter(Me.Instrument.SenseIntegrationPeriodCaption)
            Me._triggerDelayTextBox.SafeTextSetter(Me.Instrument.TriggerDelay(isr.Scpi.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            Me._closedChannelsTextBox.SafeTextSetter(Me.Instrument.Route.ClosedChannels)

        End Sub

#End Region

#Region " I Connectible "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me._tabControl.Enabled = value
                If value Then
                    populateControls()
                    refreshDisplay()
                End If
            End Set
        End Property

        ''' <summary>List reading elements and enables source functions.
        ''' </summary>
        Private Sub populateControls()

            If Me.Instrument Is Nothing OrElse Not Me.Instrument.IsConnected OrElse Not Me.Instrument.UsingDevices Then
                Return
            End If
            Me.populateModalities()
            Me.PopulateSenseFunctionModes()
        End Sub

        Friend Sub PopulateSenseFunctionModes()

            If Me._instrument IsNot Nothing AndAlso Me._instrument.SupportedFunctionModes <> isr.Scpi.SenseFunctionModes.None Then
                Me._senseFunctionComboBox.DataSource = Nothing
                Me._senseFunctionComboBox.Items.Clear()
                Me._senseFunctionComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(
                          GetType(isr.Scpi.SenseFunctionModes), Me._instrument.SupportedFunctionModes)
                Me._senseFunctionComboBox.DisplayMember = "Value"
                Me._senseFunctionComboBox.ValueMember = "Key"
                If Me._senseFunctionComboBox.Items.Count > 0 Then
                    Me._senseFunctionComboBox.SelectedItem = isr.Core.EnumExtensions.Description(isr.Scpi.SenseFunctionModes.VoltageDC)
                End If
            End If

        End Sub

        ''' <summary>Adds new items to the combo box.</summary>
        Friend Sub updateChannelListComboBox()

            If MyBase.Visible Then
                ' check if we are asking for a new channel list
                If Me._channelListComboBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me._channelListComboBox.Text) AndAlso
                    Me._channelListComboBox.FindString(Me._channelListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the channel list
                    Me._channelListComboBox.Items.Add(Me._channelListComboBox.Text)
                End If
            End If

        End Sub

        Private _modalities As isr.Scpi.ReadingElements
        Private Sub populateModalities()


            If Me._modalities <> Me.Instrument.Elements(isr.Scpi.ResourceAccessLevels.Cache) Then
                Dim selectedIndex As Integer = Me._modalityComboBox.SelectedIndex
                Me._modalities = Me.Instrument.Elements(isr.Scpi.ResourceAccessLevels.Cache)
                Me._modalityComboBox.DataSource = Nothing
                Me._modalityComboBox.Items.Clear()
                Me._modalityComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(
                              GetType(isr.Scpi.ReadingElements), Me._modalities And Not isr.Scpi.ReadingElements.Units)
                Me._modalityComboBox.DisplayMember = "Value"
                Me._modalityComboBox.ValueMember = "Key"

                If Me._modalityComboBox.Items.Count > 0 Then
                    Me._modalityComboBox.SelectedIndex = Math.Max(selectedIndex, 0)
                End If

            End If

        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As K2700.Instrument
        ''' <summary>Gets a reference to the Keithley 2700 instrument.</summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Instrument() As K2700.Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As K2700.Instrument)
                If value IsNot Nothing Then
                    Me._instrument = value
                    MyBase.ConnectableResource = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE SETTINGS: MODALITY "

        Private _actualModality As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Selects a new modality.
        ''' </summary>
        Friend Function ApplyModality(ByVal value As isr.Scpi.ReadingElements) As isr.Scpi.ReadingElements
            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected AndAlso
                (value <> isr.Scpi.ReadingElements.None) AndAlso (value <> Me._actualModality) Then
                Me._actualModality = value
                Me._modalityComboBox.SafeSelectItem(isr.Core.EnumExtensions.ValueDescriptionPair(value))
            End If
            Return Me._actualModality
        End Function

        ''' <summary>
        ''' Gets or sets the selected modality.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Private ReadOnly Property selectedModality() As isr.Scpi.ReadingElements
            Get
                Return CType(CType(Me._modalityComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, isr.Scpi.ReadingElements)
            End Get
        End Property

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

        Private _actualFunctionMode As isr.Scpi.SenseFunctionModes
        ''' <summary>
        ''' Return the last Sense function mode.  This is set only after the 
        ''' actual value is applies whereas the <see cref="SelectedFunctionMode">selected mode</see>
        ''' reflects the status of the combo box.
        ''' </summary>
        Friend ReadOnly Property ActualFunctionMode() As isr.Scpi.SenseFunctionModes
            Get
                Return Me._actualFunctionMode
            End Get
        End Property

        ''' <summary>
        ''' Selects a new sense mode.
        ''' </summary>
        Friend Function ApplyFunctionMode(ByVal value As isr.Scpi.SenseFunctionModes) As isr.Scpi.SenseFunctionModes
            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected AndAlso
                ((Me._instrument.SupportedFunctionModes And value) <> 0) AndAlso
                (value <> Me._actualFunctionMode) Then
                Me._instrument.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.None) = value
                Me._senseRangeTextBox.SafeTextSetter(CStr(Me.Instrument.Sense.Range(isr.Scpi.ResourceAccessLevels.Cache)))
                Me._integrationPeriodTextBox.SafeTextSetter(Me.Instrument.Sense.ActiveFunction.IntegrationPeriod.DisplayCaption)
                Me._senseAutoRangeToggle.SafeCheckedSetter(Me.Instrument.SenseAutoRange(isr.Scpi.ResourceAccessLevels.Cache))
                Me._actualFunctionMode = value
                Me._senseFunctionComboBox.SafeSelectItem(value, isr.Core.EnumExtensions.Description(value))
                Me._senseRangeTextBoxLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                             "Range [{0}]:",
                                                             Me.Instrument.Sense.ActiveFunction.Units)
            End If
        End Function

        ''' <summary>
        ''' Gets or sets the selected function mode.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Private ReadOnly Property selectedFunctionMode() As isr.Scpi.SenseFunctionModes
            Get
                Return CType(CType(Me._senseFunctionComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, isr.Scpi.SenseFunctionModes)
            End Get
        End Property

#End Region

#Region " BULK SETTINGS "

        ''' <summary>
        ''' Applies the selected measurements settings.
        ''' </summary>
        Private Sub applySenseSettings()

            If Not Me.Instrument.IsConnected Then
                Return
            End If

            Dim valueDouble As Double

            Me.Instrument.ClearExecutionState()

            Me.Instrument.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.None) = Me.selectedFunctionMode

            ' set the Integration Period
            Me.Instrument.SenseIntegrationPeriod(isr.Scpi.ResourceAccessLevels.None) =
               Me.Instrument.Sense.ActiveFunction.IntegrationPeriod.ParseScaledValue(Me._integrationPeriodTextBox.Text)

            ' set the range
            Me.Instrument.SenseAutoRange(isr.Scpi.ResourceAccessLevels.None) = Me._senseAutoRangeToggle.Checked

            If Not Me._senseAutoRangeToggle.Checked Then
                If Double.TryParse(Me._senseRangeTextBox.Text, Globalization.NumberStyles.Float,
                                  Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                    Me.Instrument.SenseRange(isr.Scpi.ResourceAccessLevels.None) = valueDouble
                End If
            End If

            ' get the delay time
            If Double.TryParse(Me._triggerDelayTextBox.Text, Globalization.NumberStyles.Float,
                              Globalization.CultureInfo.CurrentCulture, valueDouble) Then
                Me.Instrument.TriggerDelay(isr.Scpi.ResourceAccessLevels.None) = valueDouble
            Else
                Me.Instrument.Trigger.AutoDelay(isr.Scpi.ResourceAccessLevels.None) = True
            End If


            ' update with values from the instrument
            Me.Instrument.StoreResourceAccessLevel()
            Me.Instrument.AccessLevel = isr.Scpi.ResourceAccessLevels.Device
            Me.refreshDisplay()
            Me.Instrument.RestoreResourceAccessLevel()

        End Sub

        ''' <summary>Displays a reading based on the selected reading to display.</summary>
        Public Sub DisplaySelectedReading()

            If Me.Instrument IsNot Nothing AndAlso Me.Instrument.Sense IsNot Nothing AndAlso Me.Instrument.Reading IsNot Nothing Then

                Select Case Me.selectedModality
                    Case isr.Scpi.ReadingElements.Reading
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Reading.ToString())
                    Case isr.Scpi.ReadingElements.Timestamp
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Timestamp.ToDisplayString())
                    Case isr.Scpi.ReadingElements.ReadingNumber
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.ReadingNumber.ToDisplayString())
                    Case isr.Scpi.ReadingElements.Channel
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Channel.ToDisplayString())
                    Case isr.Scpi.ReadingElements.Limits
                        Me._readingLabel.SafeTextSetter(Me.Instrument.Reading.Limits.ToDisplayString())
                End Select
            End If

        End Sub

#End Region

#Region " CHANNELS "

        ''' <summary>
        '''  Gets or sets the scan list of closed channels.
        ''' </summary>
        Public Property ClosedChannels() As String
            Get
                Return Me._closedChannelsTextBox.Text
            End Get
            Set(ByVal value As String)
                Me._closedChannelsTextBox.SafeTextSetter(value)
            End Set
        End Property

#End Region

#Region " CONTROL EVENT HANDLERS: CHANNEL "

        Private Sub _closeChannelsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _closeChannelsButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearExecutionState()
            Me.Instrument.CloseMultipleChannels(Me._channelListComboBox.Text)
        End Sub

        Private Sub channelOpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _openChannelsButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearExecutionState()
            Me.Instrument.OpenMultipleChannels(Me._channelListComboBox.Text)
        End Sub

        Private Sub CloseOnlyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _closeOnlyButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearExecutionState()
            Me.Instrument.OpenAll()
            Me.Instrument.CloseMultipleChannels(Me._channelListComboBox.Text)
            ' this works only if a single channel:
            ' isr.Scpi.RouteSubsystem.CloseChannels(Me.Instrument, Me._channelListComboBox.Text)
        End Sub

        Private Sub OpenAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _openAllButton.Click
            Me.Instrument.ClearExecutionState()
            Me.Instrument.OpenAll()
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: INTERFACE "

        Private Sub interfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _interfaceClearButton.Click
            Me.Instrument.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary>
        ''' Issue RST.
        ''' </summary>
        Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _resetButton.Click

            Me.Instrument.ResetKnownState()

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _applySenseSettingsButton.Click
            Me.applySenseSettings()
        End Sub

        ''' <summary>Initiates a reading for retrieval by way of the service request event.</summary>
        Private Sub InitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitiateButton.Click

            ' update display modalities if changed.
            populateModalities()

            ' clear execution state before enabling events
            Me.Instrument.ClearExecutionState()

            ' set the service request
            Me.Instrument.RequestMeasurementService()

            ' trigger the initiation of the measurement letting the service request do the rest.
            Me.Instrument.ClearExecutionState()
            Me.Instrument.Trigger.Initiate()

        End Sub

        ''' <summary>Selects a new reading to display.</summary>
        Private Sub ModalityComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _modalityComboBox.SelectedIndexChanged

            If Me._modalityComboBox.Enabled AndAlso Me._modalityComboBox.SelectedIndex >= 0 AndAlso
                Not String.IsNullOrWhiteSpace(Me._modalityComboBox.Text) Then
                Me.DisplaySelectedReading()
            End If

        End Sub

        ''' <summary>Query the instrument for a reading.</summary>
        Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _readButton.Click

            ' update the terminal display.
            Me._terminalsToggle.SafeCheckedSetter(Not Me.Instrument.SystemSubsystem.FrontSwitched(isr.Scpi.ResourceAccessLevels.Device))

            ' update display modalities if changed.
            populateModalities()

            Me.Instrument.Read()

            Me.DisplaySelectedReading()

        End Sub

        Private Sub _senseFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _senseFunctionComboBox.SelectedIndexChanged

            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected Then
                Me.Instrument.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.None) = Me.selectedFunctionMode
            End If

        End Sub

        Private Sub _senseFunctionComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _senseFunctionComboBox.SelectedValueChanged

            If Me._instrument IsNot Nothing AndAlso Me._instrument.IsConnected Then
                Me.Instrument.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.None) = Me.selectedFunctionMode
            End If

        End Sub

#End Region

        Private Sub _terminalsToggle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _terminalsToggle.Click,
                                                                                                        _terminalsToggle.CheckedChanged
            If Me._terminalsToggle.Checked Then
                Me._terminalsToggle.Text = "REAR"
            Else
                Me._terminalsToggle.Text = "FRONT"
            End If
        End Sub
    End Class

End Namespace
