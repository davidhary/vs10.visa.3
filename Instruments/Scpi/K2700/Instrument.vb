Imports isr.Core
Imports isr.Core.CheckBoxExtensions
Imports isr.Core.ControlExtensions
Namespace K2700

    ''' <summary>Implements a VISA interface to a Keithley 27XX instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Created
    ''' </history>
    Public Class Instrument

        ' based on the isr.Scpi instrument class
        Inherits isr.Visa.Instruments.Controller

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)
            MyBase.New(resourceTitle, NationalInstruments.VisaNS.HardwareInterfaceType.Gpib)
            MyBase.UsingDevices = True
        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As Panel, ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._reading IsNot Nothing Then
                            Me._reading.Dispose()
                            Me._reading = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I DISPLAY "

        Private _gui As Panel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As Panel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " I Resettable "

        ''' <summary>
        ''' Clears the measurement status and event registers.
        ''' </summary>
        Public Overrides Function ClearExecutionState() As Boolean

            MyBase.ClearExecutionState()

            Me._measurementDone = False

            Return Not MyBase.HadError

        End Function

        ''' <summary>Initialize Resettable values, units, scales.
        ''' This is to be called once upon connecting but before reset and clear..</summary>
        Public Overrides Function InitializeExecutionState() As Boolean

            MyBase.InitializeExecutionState()

            ' instantiate the reading elements.
            Me._reading = New Readings(isr.Scpi.ReadingElements.Reading)

            ' add sense functions
            Me.addFunctions()

            ' set reset, clear, and preset values.
            ' Me.Arm.AutoDelay.ResetValue = False
            ' Me.Arm.Count.ResetValue = 1
            ' Me.Arm.Delay.ResetValue = 0
            ' Me.Arm.Bypass.ResetValue = True
            ' Me.Arm.InputLineNumber.ResetValue = 1
            ' Me.Arm.OutputLineNumber.ResetValue = 2
            ' Me.Arm.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            ' Me.Arm.TimerSeconds.ResetValue = 0.1

            ' Me.Calculate2.CompositeLimitsAutoClearEnabled.ResetValue = True
            ' Me.Calculate2.CompositeLimitsFailureBits.ResetValue = 15
            ' Me.Calculate2.FeedSource.ResetValue = isr.Scpi.FeedSource.Voltage
            ' Me.Calculate2.ImmediateBinning.ResetValue = True
            ' Me.Calculate2.gradingControlMode.ResetValue = True

            ' Me.Calculate3.ForcedDigitalOutputPatternEnabled.ResetValue = False
            ' Me.Calculate3.ForcedDigitalOutputPattern.ResetValue = 15

            Me.Format.Elements.ResetValue = isr.Scpi.ReadingElements.Reading Or isr.Scpi.ReadingElements.Units Or
                                            isr.Scpi.ReadingElements.ReadingNumber Or isr.Scpi.ReadingElements.Timestamp

            ' Me.Output.FrontRouteTerminals.ResetValue = True
            ' Me.Output.IsOn.ResetValue = False
            ' Me.Output.OffMode.ResetValue = isr.Scpi.OutputOffMode.Normal

            ' Me.Route.FrontRouteTerminals.ResetValue = True

            ' Me.Sense.AverageCount.ResetValue = 10
            ' Me.Sense.AverageState.ResetValue = False
            ' Me.Sense.DeltaIntegrationPeriod.ResetValue = 5 / MyBase.SystemSubsystem.LineFrequency
            ' Me.Sense.DeltaVoltageRange.ResetValue = 120
            ' Me.Sense.DeltaVoltageAutoRange.ResetValue = True
            Me.Sense.FunctionMode.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC
            ' Me.Sense.FunctionsEnabled.ResetValue = isr.Scpi.SenseFunctionModes.CurrentDC
            ' Me.Sense.FunctionsDisabled.ResetValue = isr.Scpi.SenseFunctionModes.VoltageDC Or isr.Scpi.SenseFunctionModes.Resistance
            ' Me.Sense.IsFunctionConcurrent.ResetValue = True

            ' Me.Source.AutoClear.ResetValue = False
            ' Me.Source.AutoDelay.ResetValue = True
            ' Me.Source.Delay.ResetValue = 0
            ' Me.Source.FunctionMode.ResetValue = isr.Scpi.SourceFunctionMode.Voltage
            ' Me.Source.SweepPoints.ResetValue = 2500
            ' Me.Source.DeltaArm.ResetValue = False
            ' Me.Source.DeltaHighLevel.ResetValue = 0.001
            ' Me.Source.DeltaCount.ResetValue = Integer.MaxValue ' represents infinity.
            ' Me.Source.DeltaDelay.ResetValue = 0.002
            ' Me.Source.MemoryPoints.ResetValue = 1 ' not affected by reset.

            Me.SystemSubsystem.AutoZero.ResetValue = True
            Me.SystemSubsystem.BeeperEnabled.ResetValue = True
            ' Me.SystemSubsystem.CableGuard.ResetValue = True
            ' Me.SystemSubsystem.ContactCheckEnabled.ResetValue = False
            ' Me.SystemSubsystem.ContactCheckResistance.ResetValue = 50
            ' Me.SystemSubsystem.RemoteSenseMode.ResetValue = False

            Me.Trace.PointsCount.ResetValue = 100
            Me.Trace.FeedSource.ResetValue = isr.Scpi.FeedSource.Calculate1
            Me.Trace.NextFeedControl.ResetValue = False

            ' Me.Trigger.AutoDelay.ResetValue = False
            Me.Trigger.Count.ResetValue = 1
            Me.Trigger.Count.PresetValue = Integer.MaxValue ' infinity
            Me.Trigger.Delay.ResetValue = 0
            ' Me.Trigger.Bypass.ResetValue = True
            ' Me.Trigger.InputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            ' Me.Trigger.InputLineNumber.ResetValue = 1
            ' Me.Trigger.OutputEvent.ResetValue = isr.Scpi.TriggerEvent.None
            ' Me.Trigger.OutputLineNumber.ResetValue = 2
            Me.Trigger.Source.ResetValue = isr.Scpi.ArmSource.Immediate
            Me.Trigger.TimerSeconds.ResetValue = 0.1

            Return True

        End Function

        ''' <summary>
        ''' Sets subsystem to its default system preset values.
        ''' </summary>
        Public Overrides Function Preset() As Boolean

            MyBase.Preset()

            ' clear the messages from the error queue
            isr.Scpi.SystemSubsystem.ClearErrorQueue(Me)

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            MyBase.ResetAndClear()

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Function ResetKnownState() As Boolean

            MyBase.ResetKnownState()

            ' Me.Sense.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)
            ' Me.Source.ActivateFunction(isr.Scpi.ResourceAccessLevels.Cache)
            Me.Elements(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Format.Elements(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseAutoRange(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.AutoRange(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseFunctionMode(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.FunctionMode(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseIntegrationPeriod(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.IntegrationPeriod(isr.Scpi.ResourceAccessLevels.Cache)
            Me.SenseRange(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Sense.Range(isr.Scpi.ResourceAccessLevels.Cache)
            Me.TriggerDelay(isr.Scpi.ResourceAccessLevels.Cache) = MyBase.Trigger.Delay(isr.Scpi.ResourceAccessLevels.Cache)

            If Me.Visible Then
                Me.Gui.ClosedChannels = Me.Route.ClosedChannels
            End If

            ' allow operations to complete
            Return MyBase.OperationCompleted

        End Function

#End Region

#Region " OVERRIDING PROPERTIES "

        ''' <summary>Gets or sets the sense current integration period in seconds.</summary>
        Public Property Elements(ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.ReadingElements
            Get
                Return MyBase.Format.Elements(access)
            End Get
            Set(ByVal value As isr.Scpi.ReadingElements)

                ' update the reading elements.
                Me._reading = New Readings(value)

                MyBase.Format.Elements(access) = value

            End Set
        End Property

#End Region

#Region " GUI PROPERTIES "

        ''' <summary>Gets or sets the condition for auto current range.</summary>
        Public Property SenseAutoRange(ByVal access As isr.Scpi.ResourceAccessLevels) As Boolean
            Get
                Return MyBase.Sense.AutoRange(access)
            End Get
            Set(ByVal value As Boolean)
                If MyBase.Sense.ActiveFunction.AutoRange.Value <> value Then
                    MyBase.Sense.AutoRange(access) = value
                    If Me.Visible AndAlso (Me.Gui._senseAutoRangeToggle.Checked <> value) Then
                        Me.Gui._senseAutoRangeToggle.SafeCheckedSetter(value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the <see cref="isr.Scpi.SenseFunctionModes">sense function mode</see>.</summary>
        ''' <remarks>This method is useful with instruments having a single sense function.</remarks>
        Public Property SenseFunctionMode(ByVal access As isr.Scpi.ResourceAccessLevels) As isr.Scpi.SenseFunctionModes
            Get
                Return MyBase.Sense.FunctionMode(access)
            End Get
            Set(ByVal value As isr.Scpi.SenseFunctionModes)
                If MyBase.Sense.ActiveFunction.SenseModality <> value Then
                    MyBase.Sense.ActivateFunction(value, access)
                    If Me.Visible AndAlso (Me.Gui.ActualFunctionMode <> value) Then
                        Me.Gui.ApplyFunctionMode(value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Gets the caption for displaying the sense integration period in MS.
        ''' </summary>
        Public ReadOnly Property SenseIntegrationPeriodCaption() As String
            Get
                Return MyBase.Sense.ActiveFunction.IntegrationPeriod.DisplayCaption
            End Get
        End Property

        ''' <summary>
        ''' Gets or sets the integration period of the current function.
        ''' </summary>
        Public Property SenseIntegrationPeriod(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            Get
                Return MyBase.Sense.IntegrationPeriod(access)
            End Get
            Set(ByVal value As Double)
                If value <> MyBase.Sense.IntegrationPeriod(isr.Scpi.ResourceAccessLevels.Cache) Then
                    MyBase.Sense.IntegrationPeriod(access) = value
                    If Me.Visible Then
                        Me.Gui._integrationPeriodTextBox.SafeTextSetter(Me.SenseIntegrationPeriodCaption)
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the upper range of the active sense function.
        ''' </summary>
        Public Property SenseRange(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            Get
                Return MyBase.Sense.Range(access)
            End Get
            Set(ByVal Value As Double)
                If isr.Scpi.ResettableDouble.AreDifferent(Value, MyBase.Sense.ActiveFunction.Range.Value, Single.Epsilon) Then
                    MyBase.Sense.Range(access) = Value
                    If Me.Visible Then
                        Me.Gui._senseRangeTextBox.SafeTextSetter(CStr(Value))
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the condition for auto current Delay.</summary>
        Public Property TriggerDelay(ByVal access As isr.Scpi.ResourceAccessLevels) As Double
            Get
                Return MyBase.Trigger.Delay(access)
            End Get
            Set(ByVal value As Double)
                If isr.Scpi.ResettableDouble.AreDifferent(value, MyBase.Trigger.Delay(isr.Scpi.ResourceAccessLevels.Cache), Single.Epsilon) Then
                    MyBase.Trigger.Delay(access) = value
                    If Me.Visible Then
                        Me.Gui._triggerDelayTextBox.SafeTextSetter(CStr(value))
                    End If
                End If
            End Set
        End Property

#End Region

#Region " IDENTITY / VERSION INFO "

        ''' <summary>
        ''' Queries the GPIB instrument and returns the string save the termination character.
        ''' </summary>
        Public Overrides Function ReadIdentity() As String

            If Me._versionInfo Is Nothing OrElse String.IsNullOrWhiteSpace(Me._versionInfo.Identity) Then

                Me._versionInfo = New K2700.VersionInfo(MyBase.ReadIdentity)

            End If

            Return Me._versionInfo.Identity

        End Function

        Private _versionInfo As K2700.VersionInfo
        ''' <summary>
        '''  Holds the version information.
        ''' </summary>
        Public Overloads ReadOnly Property VersionInfo() As K2700.VersionInfo
            Get
                Return Me._versionInfo
            End Get
        End Property

#End Region

#Region " CHANNELS "

        ''' <summary>
        ''' Close the channels specified in the scan list.
        ''' </summary>
        ''' <param name="scanList"></param>
        Public Sub CloseMultipleChannels(ByVal scanList As String)
            Me.Route.ClosedMultipleChannels = scanList
            If Me.Visible Then
                Me.Gui.ClosedChannels = Me.Route.ClosedChannels
            End If
        End Sub

        ''' <summary>Opens all channels</summary>
        Public Sub OpenAll()
            Const routeOpenAllCommand As String = ":ROUT:OPEN:ALL"
            isr.Scpi.RouteSubsystem.OpenAll(Me, routeOpenAllCommand)
            If Me.Visible Then
                Me.Gui.ClosedChannels = Me.Route.ClosedChannels
            End If
        End Sub

        ''' <summary>
        ''' Open the channels specified in the scan list.
        ''' </summary>
        ''' <param name="scanList"></param>
        Public Sub OpenMultipleChannels(ByVal scanList As String)
            Me.Route.OpenedMultipleChannels = scanList
            If Me.Visible Then
                Me.Gui.ClosedChannels = Me.Route.ClosedChannels
            End If
        End Sub

#End Region

#Region " READINGS "

        Private _measurementDone As Boolean
        ''' <summary>
        ''' Gets the measurement done state.  With a 2-point sweep we get two service requests both
        ''' with complete data in the buffer.  With this flag we process these only once.
        ''' </summary>
        Public ReadOnly Property MeasurementDone() As Boolean
            Get
                Return Me._measurementDone
            End Get
        End Property

        ''' <summary>
        ''' Fetches and parses the latest data.
        ''' </summary>
        Public Sub FetchLatest()

            ' Fetch the last reading and parse the results
            Me.ParseReadingElements(MyBase.Sense.FetchLatestData())

        End Sub

        ''' <summary>Parses the latest data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        Public Sub ParseReadingElements()

            ' check if we have a fixed or multi mode data 
            ' TO_DO: use the route scan to determine how many elements are received are ignore.
            If True Then

                ' parse the latest data
                ParseReadingElements(MyBase.Sense.LatestData())

            Else

                ' fetch the data
                Dim lastFetch As String = MyBase.Sense.FetchData()

                ' parse the results of all data sets.
                Me._readings = K2700.Readings.ParseMulti(Me._reading, lastFetch)

                If Me._readings.Length > 0 Then
                    ' select the final set as the valid readings.
                    Me._reading = New Readings(Me._readings(Me._readings.Length - 1))
                Else
                    Me._reading.Reset()
                End If

            End If

        End Sub

        ''' <summary>Parses a new set of reading elements.</summary>
        ''' <param name="data">Specifies the measurement text to parse into the new reading.</param>
        Public Sub ParseReadingElements(ByVal data As String)

            ' check if we have units suffixes.
            If (Me._reading.Elements And isr.Scpi.ReadingElements.Units) <> 0 Then
                data = isr.Scpi.Syntax.TrimUnits(data)
            End If

            ' Take a reading and parse the results
            Me._reading.Parse(data)

            If Me.Reading.Reading.Outcome.HitCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT COMPLIANCE",
                                      "Real Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.Reading.Outcome.HitRangeCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT RANGE COMPLIANCE",
                                      "Range Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.Reading.Outcome.HitLevelCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT HIT LEVEL COMPLIANCE",
                                      "Level Compliance.  Instrument sensed an output overflow of the measured value.")


            Else

                Me.OnMessageAvailable(TraceEventType.Information, "INSTRUMENT PARSED DATA", "Instruments parsed reading elements.")

            End If

            If Me.Visible Then

                ' update the display.
                Me.Gui.DisplaySelectedReading()

            End If

        End Sub

        ''' <summary>Take a single measurement and parse the result.</summary>
        ''' <remarks>This read uses Output Auto Clear.</remarks>
        Public Sub Read()

            ' disable service request on measurements
            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.None

            ' clear the status register and prepare for the service request
            MyBase.ClearExecutionState()

            ' Take a reading and parse the results
            Me.ParseReadingElements(MyBase.Sense.Read())

        End Sub

        ''' <summary>
        ''' Gets or sets an array of readings.
        ''' </summary>
        Private _readings As Readings()

        ''' <summary>
        ''' Returns the readings.
        ''' </summary>
        Public Function Readings() As Readings()
            Return Me._readings
        End Function

        Private _reading As Readings
        ''' <summary>
        ''' Returns the readings from the instrument.</summary>
        Public Function Reading() As Readings
            Return Me._reading
        End Function

#End Region

#Region " REGISTERS "

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Return isr.Scpi.SystemSubsystem.ReadLastError(Me)
            End Get
        End Property

        ''' <summary>Sets the measurement service status registers to request a service
        '''   request upon measurement events.</summary>
        Public Sub RequestMeasurementService()

            isr.Scpi.StatusSubsystem.EnabledMeasurementEvents(Me) = MeasurementEvents.All
            MyBase.Status.EnabledServiceRequests(isr.Scpi.ResourceAccessLevels.None) = isr.Scpi.ServiceRequests.All And
                                                                                      (Not isr.Scpi.ServiceRequests.MessageAvailable)
        End Sub

#End Region

#Region " isr.Scpi FUNCTIONS "

        ''' <summary>
        ''' Adds the source and sense functions.
        ''' </summary>
        Private Sub addFunctions()

            MyBase.Sense.SupportsMultiFunctions = False
            Me._supportedFunctionModes = isr.Scpi.SenseFunctionModes.CurrentDC
            Me._current = MyBase.Sense.AddFunction(isr.Scpi.Syntax.CurrentDC, isr.Scpi.SenseFunctionModes.CurrentDC)

            ' set current defaults
            Me._current.AutoRange.ResetValue = True
            Me._current.IntegrationPeriod.ResetValue = 1 / Me._current.LineFrequency
            Me._current.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._current.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)
            Me._current.Range.ResetValue = 3

            Me._supportedFunctionModes = Me._supportedFunctionModes Or isr.Scpi.SenseFunctionModes.VoltageDC
            Me._voltage = MyBase.Sense.AddFunction(isr.Scpi.Syntax.VoltDC, isr.Scpi.SenseFunctionModes.VoltageDC)

            ' set voltage defaults
            Me._voltage.AutoRange.ResetValue = True
            Me._voltage.IntegrationPeriod.ResetValue = 5 / Me._voltage.LineFrequency
            Me._voltage.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._voltage.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)

            Me._voltage.Range.ResetValue = 1000

            Me._supportedFunctionModes = Me._supportedFunctionModes Or isr.Scpi.SenseFunctionModes.Resistance
            Me._resistance = MyBase.Sense.AddFunction(isr.Scpi.Syntax.Resistance, isr.Scpi.SenseFunctionModes.Resistance)

            ' set resistance defaults
            Me._resistance.AutoRange.ResetValue = True
            Me._resistance.IntegrationPeriod.ResetValue = 1 / Me._resistance.LineFrequency
            Me._resistance.IntegrationPeriod.Units = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.None)
            Me._resistance.IntegrationPeriod.DisplayUnits = New isr.Scpi.FormattedUnits(isr.Scpi.PhysicalUnit.Time, isr.Scpi.UnitsScale.Milli)

            Me._resistance.Range.ResetValue = 100000000.0

        End Sub

        Private _current As isr.Scpi.ScpiFunction
        Public ReadOnly Property Current() As isr.Scpi.ScpiFunction
            Get
                Return Me._current
            End Get
        End Property

        Private _resistance As isr.Scpi.ScpiFunction
        Public ReadOnly Property Resistance() As isr.Scpi.ScpiFunction
            Get
                Return Me._resistance
            End Get
        End Property

        Private _voltage As isr.Scpi.ScpiFunction
        Public ReadOnly Property Voltage() As isr.Scpi.ScpiFunction
            Get
                Return Me._voltage
            End Get
        End Property

        Private _supportedFunctionModes As isr.Scpi.SenseFunctionModes = isr.Scpi.SenseFunctionModes.None
        ''' <summary>
        ''' Gets or sets the supported function.  This is a subset of the functions supported by the instrument.
        ''' </summary>
        Public Property SupportedFunctionModes() As isr.Scpi.SenseFunctionModes
            Get
                Return Me._supportedFunctionModes
            End Get
            Set(ByVal value As isr.Scpi.SenseFunctionModes)
                Me._supportedFunctionModes = value
            End Set
        End Property

#End Region

#Region " SERVICE REQUEST HANDLER "

        ''' <summary>Updates the screen if we have a new measurement and raises the service
        '''   request event. New measurements are parsed including setting the threshold
        '''   levels for testing for level compliance.
        ''' Note that on a two point sweep we get to service request.  The first service request
        ''' Appears before the second reading is on screen but already has the values for both sweeps.
        ''' </summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Scpi.BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Scpi.BaseServiceEventArgs)

            If e Is Nothing Then

            ElseIf Not e.HasError Then

                If (e.MeasurementEventStatus And MeasurementEvents.ReadingAvailable) <> 0 Then
                    If Not Me._measurementDone Then
                        'Debug.WriteLine("M: " & e.MeasurementEventStatus & "; S: " & e.ServiceRequestStatus 
                        '  & "; O: " & e.OperationEventStatus & "; Q: " & e.QuestionableEventStatus 
                        '  & "; SRQ: " & e.StandardEventStatus & "; Done: " & e.OperationCompleted)
                        ' parse the results
                        Me.ParseReadingElements()
                        Me._measurementDone = True
                    End If
                End If

            End If

            ' raise the service request event
            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace
