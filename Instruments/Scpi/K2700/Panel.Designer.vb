Namespace K2700

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class Panel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try

        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me._tabControl = New System.Windows.Forms.TabControl
            Me._readingTabPage = New System.Windows.Forms.TabPage
            Me._terminalsToggle = New System.Windows.Forms.CheckBox
            Me._modalityComboBox = New System.Windows.Forms.ComboBox
            Me._readingLabel = New System.Windows.Forms.Label
            Me._readButton = New System.Windows.Forms.Button
            Me._InitiateButton = New System.Windows.Forms.Button
            Me._interfaceTabPage = New System.Windows.Forms.TabPage
            Me._resetButton = New System.Windows.Forms.Button
            Me._interfaceClearButton = New System.Windows.Forms.Button
            Me._channelTabPage = New System.Windows.Forms.TabPage
            Me._closedChannelsTextBoxLabel = New System.Windows.Forms.Label
            Me._closedChannelsTextBox = New System.Windows.Forms.TextBox
            Me._openAllButton = New System.Windows.Forms.Button
            Me._openChannelsButton = New System.Windows.Forms.Button
            Me._closeOnlyButton = New System.Windows.Forms.Button
            Me._closeChannelsButton = New System.Windows.Forms.Button
            Me._channelListComboBox = New System.Windows.Forms.ComboBox
            Me._channelListComboBoxLabel = New System.Windows.Forms.Label
            Me._senseTabPage = New System.Windows.Forms.TabPage
            Me._triggerDelayTextBox = New System.Windows.Forms.TextBox
            Me._triggerDelayTextBoxLabel = New System.Windows.Forms.Label
            Me._senseRangeTextBox = New System.Windows.Forms.TextBox
            Me._senseRangeTextBoxLabel = New System.Windows.Forms.Label
            Me._integrationPeriodTextBox = New System.Windows.Forms.TextBox
            Me._integrationPeriodTextBoxLabel = New System.Windows.Forms.Label
            Me._senseFunctionComboBox = New System.Windows.Forms.ComboBox
            Me._senseFunctionComboBoxLabel = New System.Windows.Forms.Label
            Me._senseAutoRangeToggle = New System.Windows.Forms.CheckBox
            Me._applySenseSettingsButton = New System.Windows.Forms.Button
            Me._messagesTabPage = New System.Windows.Forms.TabPage
            Me._messagesBox = New isr.Controls.MessagesBox
            Me._tabControl.SuspendLayout()
            Me._readingTabPage.SuspendLayout()
            Me._interfaceTabPage.SuspendLayout()
            Me._channelTabPage.SuspendLayout()
            Me._senseTabPage.SuspendLayout()
            Me._messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Clearable = True
            Me.Connector.Connectible = True
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            Me.Connector.Searchable = True
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'StatusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'IdentityPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            '_tabControl
            '
            Me._tabControl.Controls.Add(Me._readingTabPage)
            Me._tabControl.Controls.Add(Me._interfaceTabPage)
            Me._tabControl.Controls.Add(Me._channelTabPage)
            Me._tabControl.Controls.Add(Me._senseTabPage)
            Me._tabControl.Controls.Add(Me._messagesTabPage)
            Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me._tabControl.Enabled = False
            Me._tabControl.ItemSize = New System.Drawing.Size(52, 18)
            Me._tabControl.Location = New System.Drawing.Point(0, 0)
            Me._tabControl.Multiline = True
            Me._tabControl.Name = "_tabControl"
            Me._tabControl.SelectedIndex = 0
            Me._tabControl.Size = New System.Drawing.Size(312, 233)
            Me._tabControl.TabIndex = 15
            '
            '_readingTabPage
            '
            Me._readingTabPage.Controls.Add(Me._terminalsToggle)
            Me._readingTabPage.Controls.Add(Me._modalityComboBox)
            Me._readingTabPage.Controls.Add(Me._readingLabel)
            Me._readingTabPage.Controls.Add(Me._readButton)
            Me._readingTabPage.Controls.Add(Me._InitiateButton)
            Me._readingTabPage.Location = New System.Drawing.Point(4, 22)
            Me._readingTabPage.Name = "_readingTabPage"
            Me._readingTabPage.Size = New System.Drawing.Size(304, 207)
            Me._readingTabPage.TabIndex = 0
            Me._readingTabPage.Text = "Reading"
            Me._readingTabPage.UseVisualStyleBackColor = True
            '
            '_terminalsToggle
            '
            Me._terminalsToggle.Appearance = System.Windows.Forms.Appearance.Button
            Me._terminalsToggle.Enabled = False
            Me._terminalsToggle.Location = New System.Drawing.Point(239, 173)
            Me._terminalsToggle.Name = "_terminalsToggle"
            Me._terminalsToggle.Size = New System.Drawing.Size(57, 24)
            Me._terminalsToggle.TabIndex = 3
            Me._terminalsToggle.Text = "REAR"
            Me._terminalsToggle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me._terminalsToggle.UseVisualStyleBackColor = True
            '
            '_modalityComboBox
            '
            Me._modalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._modalityComboBox.Location = New System.Drawing.Point(158, 56)
            Me._modalityComboBox.Name = "_modalityComboBox"
            Me._modalityComboBox.Size = New System.Drawing.Size(135, 21)
            Me._modalityComboBox.TabIndex = 1
            '
            '_readingLabel
            '
            Me._readingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me._readingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me._readingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me._readingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._readingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me._readingLabel.Location = New System.Drawing.Point(8, 16)
            Me._readingLabel.Name = "_readingLabel"
            Me._readingLabel.Size = New System.Drawing.Size(288, 36)
            Me._readingLabel.TabIndex = 2
            Me._readingLabel.Text = "0.0000000 mV"
            Me._readingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            '_readButton
            '
            Me._readButton.Location = New System.Drawing.Point(8, 56)
            Me._readButton.Name = "_readButton"
            Me._readButton.Size = New System.Drawing.Size(50, 23)
            Me._readButton.TabIndex = 0
            Me._readButton.Text = "&Read"
            Me._readButton.UseVisualStyleBackColor = True
            '
            '_InitiateButton
            '
            Me._InitiateButton.Location = New System.Drawing.Point(64, 56)
            Me._InitiateButton.Name = "_InitiateButton"
            Me._InitiateButton.Size = New System.Drawing.Size(50, 23)
            Me._InitiateButton.TabIndex = 0
            Me._InitiateButton.Text = "&Initiate"
            Me._InitiateButton.UseVisualStyleBackColor = True
            '
            '_interfaceTabPage
            '
            Me._interfaceTabPage.Controls.Add(Me._resetButton)
            Me._interfaceTabPage.Controls.Add(Me._interfaceClearButton)
            Me._interfaceTabPage.Location = New System.Drawing.Point(4, 22)
            Me._interfaceTabPage.Name = "_interfaceTabPage"
            Me._interfaceTabPage.Size = New System.Drawing.Size(304, 207)
            Me._interfaceTabPage.TabIndex = 2
            Me._interfaceTabPage.Text = "Interface"
            Me._interfaceTabPage.UseVisualStyleBackColor = True
            '
            '_resetButton
            '
            Me._resetButton.Location = New System.Drawing.Point(67, 92)
            Me._resetButton.Name = "_resetButton"
            Me._resetButton.Size = New System.Drawing.Size(156, 23)
            Me._resetButton.TabIndex = 1
            Me._resetButton.Text = "&Reset to Known State"
            Me._resetButton.UseVisualStyleBackColor = True
            '
            '_interfaceClearButton
            '
            Me._interfaceClearButton.Location = New System.Drawing.Point(91, 50)
            Me._interfaceClearButton.Name = "_interfaceClearButton"
            Me._interfaceClearButton.Size = New System.Drawing.Size(116, 23)
            Me._interfaceClearButton.TabIndex = 0
            Me._interfaceClearButton.Text = "&Clear Interface"
            Me._interfaceClearButton.UseVisualStyleBackColor = True
            '
            '_channelTabPage
            '
            Me._channelTabPage.Controls.Add(Me._closedChannelsTextBoxLabel)
            Me._channelTabPage.Controls.Add(Me._closedChannelsTextBox)
            Me._channelTabPage.Controls.Add(Me._openAllButton)
            Me._channelTabPage.Controls.Add(Me._openChannelsButton)
            Me._channelTabPage.Controls.Add(Me._closeOnlyButton)
            Me._channelTabPage.Controls.Add(Me._closeChannelsButton)
            Me._channelTabPage.Controls.Add(Me._channelListComboBox)
            Me._channelTabPage.Controls.Add(Me._channelListComboBoxLabel)
            Me._channelTabPage.Location = New System.Drawing.Point(4, 22)
            Me._channelTabPage.Name = "_channelTabPage"
            Me._channelTabPage.Size = New System.Drawing.Size(304, 207)
            Me._channelTabPage.TabIndex = 1
            Me._channelTabPage.Text = "Channel"
            Me._channelTabPage.UseVisualStyleBackColor = True
            '
            '_closedChannelsTextBoxLabel
            '
            Me._closedChannelsTextBoxLabel.Location = New System.Drawing.Point(8, 84)
            Me._closedChannelsTextBoxLabel.Name = "_closedChannelsTextBoxLabel"
            Me._closedChannelsTextBoxLabel.Size = New System.Drawing.Size(97, 16)
            Me._closedChannelsTextBoxLabel.TabIndex = 13
            Me._closedChannelsTextBoxLabel.Text = "Closed Channels:"
            '
            '_closedChannelsTextBox
            '
            Me._closedChannelsTextBox.Location = New System.Drawing.Point(6, 101)
            Me._closedChannelsTextBox.Multiline = True
            Me._closedChannelsTextBox.Name = "_closedChannelsTextBox"
            Me._closedChannelsTextBox.ReadOnly = True
            Me._closedChannelsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._closedChannelsTextBox.Size = New System.Drawing.Size(290, 52)
            Me._closedChannelsTextBox.TabIndex = 12
            '
            '_openAllButton
            '
            Me._openAllButton.Location = New System.Drawing.Point(222, 59)
            Me._openAllButton.Name = "_openAllButton"
            Me._openAllButton.Size = New System.Drawing.Size(77, 23)
            Me._openAllButton.TabIndex = 11
            Me._openAllButton.Text = "Open &All"
            Me._openAllButton.UseVisualStyleBackColor = True
            '
            '_channelOpenButton
            '
            Me._openChannelsButton.Location = New System.Drawing.Point(251, 8)
            Me._openChannelsButton.Name = "_channelOpenButton"
            Me._openChannelsButton.Size = New System.Drawing.Size(48, 23)
            Me._openChannelsButton.TabIndex = 10
            Me._openChannelsButton.Text = "&Open"
            Me._openChannelsButton.UseVisualStyleBackColor = True
            '
            '_closeOnlyButton
            '
            Me._closeOnlyButton.Location = New System.Drawing.Point(94, 59)
            Me._closeOnlyButton.Name = "_closeOnlyButton"
            Me._closeOnlyButton.Size = New System.Drawing.Size(122, 23)
            Me._closeOnlyButton.TabIndex = 9
            Me._closeOnlyButton.Text = "Open All and &Close"
            Me.TipsToolTip.SetToolTip(Me._closeOnlyButton, "Closes the specified channels opening all other channels")
            Me._closeOnlyButton.UseVisualStyleBackColor = True
            '
            '_channelCloseButton
            '
            Me._closeChannelsButton.Location = New System.Drawing.Point(198, 8)
            Me._closeChannelsButton.Name = "_channelCloseButton"
            Me._closeChannelsButton.Size = New System.Drawing.Size(49, 23)
            Me._closeChannelsButton.TabIndex = 9
            Me._closeChannelsButton.Text = "&Close"
            Me._closeChannelsButton.UseVisualStyleBackColor = True
            '
            '_channelListComboBox
            '
            Me._channelListComboBox.Location = New System.Drawing.Point(6, 32)
            Me._channelListComboBox.Name = "_channelListComboBox"
            Me._channelListComboBox.Size = New System.Drawing.Size(293, 21)
            Me._channelListComboBox.TabIndex = 8
            Me._channelListComboBox.Text = "(@ 201,203:205)"
            '
            '_channelListComboBoxLabel
            '
            Me._channelListComboBoxLabel.Location = New System.Drawing.Point(6, 16)
            Me._channelListComboBoxLabel.Name = "_channelListComboBoxLabel"
            Me._channelListComboBoxLabel.Size = New System.Drawing.Size(72, 16)
            Me._channelListComboBoxLabel.TabIndex = 7
            Me._channelListComboBoxLabel.Text = "Channel List:"
            '
            '_senseTabPage
            '
            Me._senseTabPage.Controls.Add(Me._triggerDelayTextBox)
            Me._senseTabPage.Controls.Add(Me._triggerDelayTextBoxLabel)
            Me._senseTabPage.Controls.Add(Me._senseRangeTextBox)
            Me._senseTabPage.Controls.Add(Me._senseRangeTextBoxLabel)
            Me._senseTabPage.Controls.Add(Me._integrationPeriodTextBox)
            Me._senseTabPage.Controls.Add(Me._integrationPeriodTextBoxLabel)
            Me._senseTabPage.Controls.Add(Me._senseFunctionComboBox)
            Me._senseTabPage.Controls.Add(Me._senseFunctionComboBoxLabel)
            Me._senseTabPage.Controls.Add(Me._senseAutoRangeToggle)
            Me._senseTabPage.Controls.Add(Me._applySenseSettingsButton)
            Me._senseTabPage.Location = New System.Drawing.Point(4, 22)
            Me._senseTabPage.Name = "_senseTabPage"
            Me._senseTabPage.Size = New System.Drawing.Size(304, 207)
            Me._senseTabPage.TabIndex = 4
            Me._senseTabPage.Text = "Sense"
            Me._senseTabPage.UseVisualStyleBackColor = True
            '
            '_triggerDelayTextBox
            '
            Me._triggerDelayTextBox.Location = New System.Drawing.Point(135, 87)
            Me._triggerDelayTextBox.Name = "_triggerDelayTextBox"
            Me._triggerDelayTextBox.Size = New System.Drawing.Size(65, 20)
            Me._triggerDelayTextBox.TabIndex = 37
            Me._triggerDelayTextBox.Text = "0.0"
            '
            '_triggerDelayTextBoxLabel
            '
            Me._triggerDelayTextBoxLabel.Location = New System.Drawing.Point(13, 88)
            Me._triggerDelayTextBoxLabel.Name = "_triggerDelayTextBoxLabel"
            Me._triggerDelayTextBoxLabel.Size = New System.Drawing.Size(121, 18)
            Me._triggerDelayTextBoxLabel.TabIndex = 36
            Me._triggerDelayTextBoxLabel.Text = "Trigger Delay [s]:"
            Me._triggerDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_senseRangeTextBox
            '
            Me._senseRangeTextBox.Location = New System.Drawing.Point(135, 62)
            Me._senseRangeTextBox.Name = "_senseRangeTextBox"
            Me._senseRangeTextBox.Size = New System.Drawing.Size(65, 20)
            Me._senseRangeTextBox.TabIndex = 33
            Me._senseRangeTextBox.Text = "1.00"
            '
            '_senseRangeTextBoxLabel
            '
            Me._senseRangeTextBoxLabel.Location = New System.Drawing.Point(13, 63)
            Me._senseRangeTextBoxLabel.Name = "_senseRangeTextBoxLabel"
            Me._senseRangeTextBoxLabel.Size = New System.Drawing.Size(121, 18)
            Me._senseRangeTextBoxLabel.TabIndex = 32
            Me._senseRangeTextBoxLabel.Text = "Range:"
            Me._senseRangeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_integrationPeriodTextBox
            '
            Me._integrationPeriodTextBox.Location = New System.Drawing.Point(135, 36)
            Me._integrationPeriodTextBox.Name = "_integrationPeriodTextBox"
            Me._integrationPeriodTextBox.Size = New System.Drawing.Size(65, 20)
            Me._integrationPeriodTextBox.TabIndex = 31
            Me._integrationPeriodTextBox.Text = "16.7"
            '
            '_integrationPeriodTextBoxLabel
            '
            Me._integrationPeriodTextBoxLabel.Location = New System.Drawing.Point(13, 37)
            Me._integrationPeriodTextBoxLabel.Name = "_integrationPeriodTextBoxLabel"
            Me._integrationPeriodTextBoxLabel.Size = New System.Drawing.Size(121, 18)
            Me._integrationPeriodTextBoxLabel.TabIndex = 30
            Me._integrationPeriodTextBoxLabel.Text = "Integration Period [ms] :"
            Me._integrationPeriodTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_senseFunctionComboBox
            '
            Me._senseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._senseFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
            Me._senseFunctionComboBox.Location = New System.Drawing.Point(66, 9)
            Me._senseFunctionComboBox.Name = "_senseFunctionComboBox"
            Me._senseFunctionComboBox.Size = New System.Drawing.Size(151, 21)
            Me._senseFunctionComboBox.TabIndex = 29
            '
            '_senseFunctionComboBoxLabel
            '
            Me._senseFunctionComboBoxLabel.Location = New System.Drawing.Point(12, 11)
            Me._senseFunctionComboBoxLabel.Name = "_senseFunctionComboBoxLabel"
            Me._senseFunctionComboBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me._senseFunctionComboBoxLabel.TabIndex = 28
            Me._senseFunctionComboBoxLabel.Text = "Function: "
            Me._senseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_senseAutoRangeToggle
            '
            Me._senseAutoRangeToggle.Location = New System.Drawing.Point(205, 64)
            Me._senseAutoRangeToggle.Name = "_senseAutoRangeToggle"
            Me._senseAutoRangeToggle.Size = New System.Drawing.Size(88, 16)
            Me._senseAutoRangeToggle.TabIndex = 22
            Me._senseAutoRangeToggle.Text = "Auto Range"
            Me._senseAutoRangeToggle.UseVisualStyleBackColor = True
            '
            '_applySenseSettingsButton
            '
            Me._applySenseSettingsButton.Location = New System.Drawing.Point(239, 173)
            Me._applySenseSettingsButton.Name = "_applySenseSettingsButton"
            Me._applySenseSettingsButton.Size = New System.Drawing.Size(50, 23)
            Me._applySenseSettingsButton.TabIndex = 13
            Me._applySenseSettingsButton.Text = "&Apply"
            Me._applySenseSettingsButton.UseVisualStyleBackColor = True
            '
            '_messagesTabPage
            '
            Me._messagesTabPage.Controls.Add(Me._messagesBox)
            Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me._messagesTabPage.Name = "_messagesTabPage"
            Me._messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me._messagesTabPage.TabIndex = 3
            Me._messagesTabPage.Text = "Messages"
            Me._messagesTabPage.UseVisualStyleBackColor = True
            '
            '_messagesBox
            '
            Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
            Me._messagesBox.Delimiter = "; "
            Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
            Me._messagesBox.Location = New System.Drawing.Point(0, 0)
            Me._messagesBox.Multiline = True
            Me._messagesBox.Name = "_messagesBox"
            Me._messagesBox.PresetCount = 50
            Me._messagesBox.ReadOnly = True
            Me._messagesBox.ResetCount = 100
            Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me._messagesBox.Size = New System.Drawing.Size(304, 207)
            Me._messagesBox.TabIndex = 0
            Me._messagesBox.TimeFormat = "HH:mm:ss.f"
            '
            'Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me._tabControl)
            Me.Name = "Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me._tabControl, 0)
            Me._tabControl.ResumeLayout(False)
            Me._readingTabPage.ResumeLayout(False)
            Me._interfaceTabPage.ResumeLayout(False)
            Me._channelTabPage.ResumeLayout(False)
            Me._channelTabPage.PerformLayout()
            Me._senseTabPage.ResumeLayout(False)
            Me._senseTabPage.PerformLayout()
            Me._messagesTabPage.ResumeLayout(False)
            Me._messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents _tabControl As System.Windows.Forms.TabControl
        Friend WithEvents _readingTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _terminalsToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _modalityComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _readingLabel As System.Windows.Forms.Label
        Friend WithEvents _readButton As System.Windows.Forms.Button
        Friend WithEvents _InitiateButton As System.Windows.Forms.Button
        Friend WithEvents _interfaceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _interfaceClearButton As System.Windows.Forms.Button
        Friend WithEvents _channelTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _senseTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _applySenseSettingsButton As System.Windows.Forms.Button
        Friend WithEvents _messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents _messagesBox As isr.Controls.MessagesBox
        Friend WithEvents _senseAutoRangeToggle As System.Windows.Forms.CheckBox
        Friend WithEvents _senseFunctionComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _senseFunctionComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _integrationPeriodTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _integrationPeriodTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _senseRangeTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _senseRangeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _triggerDelayTextBox As System.Windows.Forms.TextBox
        Friend WithEvents _triggerDelayTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _resetButton As System.Windows.Forms.Button
        Friend WithEvents _openChannelsButton As System.Windows.Forms.Button
        Friend WithEvents _closeChannelsButton As System.Windows.Forms.Button
        Friend WithEvents _channelListComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents _channelListComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _openAllButton As System.Windows.Forms.Button
        Friend WithEvents _closeOnlyButton As System.Windows.Forms.Button
        Friend WithEvents _closedChannelsTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents _closedChannelsTextBox As System.Windows.Forms.TextBox

    End Class

End Namespace
