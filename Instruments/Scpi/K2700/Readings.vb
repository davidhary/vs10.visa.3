Namespace K2700

    ''' <summary>
    ''' Holds a single set of 27xx instrument reading elements.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x">
    ''' Create based on the 24xx system classes.
    ''' </history>
    Public Class Readings
        Inherits isr.Scpi.Readings(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="elements">Specifies the 
        ''' <see cref="isr.Scpi.ReadingElements">reading elements</see>
        ''' expected in the data strings.
        ''' </param>
        ''' <remarks>Reading elements are added in the order they are returned by
        ''' the instrument so as to automate parsing of these data.</remarks>
        Public Sub New(ByVal elements As isr.Scpi.ReadingElements)

            ' instantiate the base class
            MyBase.New()

            Me._elements = elements
            If (Me._elements And isr.Scpi.ReadingElements.Reading) <> 0 Then
                Me._reading = New isr.Scpi.MeasurandDouble()
                Me._reading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
                Me._reading.SaveCaption.Units = Me._reading.DisplayCaption.Units
                Me._reading.ComplianceLimit = isr.Scpi.Syntax.Infinity
                Me._reading.HighLimit = isr.Scpi.Syntax.Infinity
                Me._reading.LowLimit = isr.Scpi.Syntax.NegativeInfinity
                MyBase.AddReading(Me._reading, 15)
            End If

            If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                Me._timeStamp = New isr.Scpi.ReadingDouble()
                Me._timeStamp.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Time
                Me._timeStamp.SaveCaption.Units = Me._timeStamp.DisplayCaption.Units
                MyBase.AddReading(Me._timeStamp, 7)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.ReadingNumber) <> 0 Then

                Me._readingNumber = New isr.Scpi.ReadingLong()
                Me._readingNumber.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.None
                Me._readingNumber.SaveCaption.Units = Me._readingNumber.DisplayCaption.Units
                MyBase.AddReading(Me._readingNumber, 4)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.Channel) <> 0 Then

                Me._channel = New isr.Scpi.ReadingLong()
                Me._channel.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.None
                Me._channel.SaveCaption.Units = Me._channel.DisplayCaption.Units
                MyBase.AddReading(Me._channel, 3)

            End If

            If (Me._elements And isr.Scpi.ReadingElements.Limits) <> 0 Then

                Me._limits = New isr.Scpi.ReadingLong()
                Me._limits.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.None
                Me._limits.SaveCaption.Units = Me._limits.DisplayCaption.Units
                MyBase.AddReading(Me._limits, 4)

            End If

        End Sub

        ''' <summary>
        ''' Create a copy of the model.
        ''' </summary>
        Public Sub New(ByVal model As Readings)

            ' instantiate the base class
            MyBase.New()

            If model IsNot Nothing Then
                Me._elements = model._elements

                If (Me._elements And isr.Scpi.ReadingElements.Reading) <> 0 Then
                    Me._reading = New isr.Scpi.MeasurandDouble(model._reading)
                    MyBase.AddReading(Me._reading, 15)
                End If

                If (Me._elements And isr.Scpi.ReadingElements.Timestamp) <> 0 Then

                    Me._timeStamp = New isr.Scpi.ReadingDouble(model._timeStamp)
                    MyBase.AddReading(Me._timeStamp, 7)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.ReadingNumber) <> 0 Then

                    Me._readingNumber = New isr.Scpi.ReadingLong(model._readingNumber)
                    MyBase.AddReading(Me._readingNumber, 4)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.Channel) <> 0 Then

                    Me._channel = New isr.Scpi.ReadingLong(model._channel)
                    MyBase.AddReading(Me._channel, 3)

                End If

                If (Me._elements And isr.Scpi.ReadingElements.Limits) <> 0 Then

                    Me._limits = New isr.Scpi.ReadingLong(model._limits)
                    MyBase.AddReading(Me._limits, 4)

                End If
            End If

        End Sub

        ''' <summary>
        ''' Clones this class.
        ''' </summary>
        Public Function Clone() As Readings
            Return New Readings(Me)
        End Function

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If Me._channel IsNot Nothing Then
                            Me._channel = Nothing
                        End If

                        If Me._limits IsNot Nothing Then
                            Me._limits = Nothing
                        End If

                        If Me._readingNumber IsNot Nothing Then
                            Me._readingNumber = Nothing
                        End If

                        If Me._reading IsNot Nothing Then
                            Me._reading = Nothing
                        End If

                        If Me._timeStamp IsNot Nothing Then
                            Me._timeStamp = Nothing
                        End If

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PARSE "

        ''' <summary>
        ''' Parse reading data into a readings array.
        ''' </summary>
        ''' <param name="baseReading">Specifies the base reading which includes the limits for all reading
        ''' elements.</param>
        ''' <param name="readingRecords">The reading records.</param>
        ''' <returns>Readings[][].</returns>
        ''' <exception cref="System.ArgumentNullException">readingRecords</exception>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Object is returned")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMulti(ByVal baseReading As Readings, ByVal readingRecords As String) As Readings()

            If readingRecords Is Nothing Then
                Throw New ArgumentNullException("readingRecords")
            ElseIf readingRecords.Length = 0 Then
                Dim r As Readings() = {}
                Return r
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If

            ' TO_DO: This assumes 5 elements per reading.
            '  Use Me._elements.Set to set the number of elements per reading.
            Dim readings As String() = readingRecords.Split(","c)
            If readings.Length < baseReading.ElementsCount Then
                Dim r As Readings() = {}
                Return r
            End If

            Dim readingsArray(readings.Length \ baseReading.ElementsCount - 1) As Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readings.Length - 1 Step baseReading.ElementsCount
                Dim reading As New Readings(baseReading)
                reading.Parse(readings, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

#End Region

#Region " PROPERTIES "

        Private _elements As isr.Scpi.ReadingElements
        ''' <summary>
        ''' Gets the reading elements.
        ''' </summary>
        Public ReadOnly Property Elements() As isr.Scpi.ReadingElements
            Get
                Return Me._elements
            End Get
        End Property

        Private _channel As isr.Scpi.ReadingLong
        ''' <summary>Gets or sets the <see cref="isr.Scpi.Readinglong">channel number</see>.</summary>
        Public Property Channel() As isr.Scpi.ReadingLong
            Get
                Return Me._channel
            End Get
            Set(ByVal value As isr.Scpi.ReadingLong)
                Me._channel = value
            End Set
        End Property

        Private _limits As isr.Scpi.ReadingLong
        ''' <summary>Gets or sets the <see cref="isr.Scpi.Readinglong">limits</see>.</summary>
        Public Property Limits() As isr.Scpi.ReadingLong
            Get
                Return Me._limits
            End Get
            Set(ByVal value As isr.Scpi.ReadingLong)
                Me._limits = value
            End Set
        End Property

        Private _reading As isr.Scpi.MeasurandDouble
        ''' <summary>Gets or sets the DMM <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property Reading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._reading
            End Get
            Set(ByVal value As isr.Scpi.MeasurandDouble)
                Me._reading = value
            End Set
        End Property

        Private _readingNumber As isr.Scpi.ReadingLong
        ''' <summary>Gets or sets the <see cref="isr.Scpi.Readinglong">reading number</see>.</summary>
        Public Property ReadingNumber() As isr.Scpi.ReadingLong
            Get
                Return Me._readingNumber
            End Get
            Set(ByVal value As isr.Scpi.ReadingLong)
                Me._readingNumber = value
            End Set
        End Property

        Private _timeStamp As isr.Scpi.ReadingDouble
        ''' <summary>Gets or sets the timestamp <see cref="isr.Scpi.ReadingDouble">reading</see>.</summary>
        Public Property Timestamp() As isr.Scpi.ReadingDouble
            Get
                Return Me._timeStamp

            End Get
            Set(ByVal value As isr.Scpi.ReadingDouble)
                Me._timeStamp = value
            End Set
        End Property

#End Region

    End Class

End Namespace