Namespace R2D2

    ''' <summary>Provides a user interface for a Weinschel Controller GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("Weinschel Controller - Windows Forms Custom Control")>
    Public Class W8200InstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New W8200Instrument(Me, "W8200")

            ' set combo
            serviceRequestFlagsComboBox.DataSource = Nothing
            serviceRequestFlagsComboBox.Items.Clear()
            serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Ieee4882.ServiceRequests))

            ' set default value for the device and enable connection:
            Me.Connector.SelectedName = "GPIB0::4::INSTR"

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me.Instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                messagesMessageList.PrependMessage(e.Details)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

#End Region

#Region " PROPERTIES AND METHODS: DISPLAY "

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="isr.Visa.Ieee4882.ServiceRequests">service request flags</see></param>
        Friend Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Visa.Ieee4882.ServiceRequests)
            If Me.Visible Then
                Me.serviceRequestMaskTextBox.Text = serviceRequestMask.ToString()
                Me.enableServiceRequestCheckBox.Enabled = False
                Me.enableServiceRequestCheckBox.Checked = turnOn
                Me.enableServiceRequestCheckBox.Enabled = True
                Me.enableServiceRequestCheckBox.Visible = True
                Me.enableServiceRequestCheckBox.Invalidate()
            End If
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me.mainTabControl.Enabled = value
                If value Then
                    refreshDisplay()
                    ' set combo
                    Me._serviceRequestFlagsComboBox.DataSource = Nothing
                    Me._serviceRequestFlagsComboBox.Items.Clear()
                    Me._serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Scpi.ServiceRequests))
                    If True Then
                    Else
                        Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "CONNECTED",
                                              "Manufacturer name: {1}{0}Manufacturer ID: 0x{2:X}{0}Connected to {3}",
                                              Environment.NewLine,
                                              Me._instrument.GpibSession.ResourceManufacturerName,
                                              Me._instrument.GpibSession.ResourceManufacturerID,
                                              Me._instrument.Id())
                    End If
                End If
            End Set
        End Property

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.Instruments.R2D2.W8200Instrument
        ''' <summary>Gets a reference to the Weinschell 8200 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As isr.Visa.Instruments.R2D2.W8200Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As isr.Visa.Instruments.R2D2.W8200Instrument)
                Me._instrument = value
                MyBase.ConnectableResource = value
            End Set
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

#Region " I/O "

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub InputFixtureOneCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InputFixtureOneCheckBox.CheckedChanged
            Try
                Me._instrument.InputFixtureOne = Me.InputFixtureOneCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting input fixture one. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting input fixture one. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting input fixture one. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub InputFixtureTwoCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InputFixtureTwoCheckBox.CheckedChanged
            Try
                Me._instrument.InputFixtureTwo = Me.InputFixtureTwoCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting input fixture two. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting input fixture two. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting input fixture two. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub fixtureOnePortTwoCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fixtureOnePortTwoCheckBox.CheckedChanged
            Try
                Me._instrument.FixtureOnePortTwoOutput = Me.fixtureOnePortTwoCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting fixture one port 2. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting fixture one port 2. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting fixture one port 2. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub fixtureOnePortThreeCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fixtureOnePortThreeCheckBox.CheckedChanged
            Try
                Me._instrument.FixtureOnePortThreeOutput = Me.fixtureOnePortThreeCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting fixture one port 3. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting fixture one port 3. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting fixture one port 3. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub fixtureTwoPortTwoCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fixtureTwoPortTwoCheckBox.CheckedChanged
            Try
                Me._instrument.FixtureTwoPortTwoOutput = Me.fixtureTwoPortTwoCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting fixture two port 2. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting fixture two port 2. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting fixture two port 2. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub fixtureTwoPortThreeCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fixtureTwoPortThreeCheckBox.CheckedChanged
            Try
                Me._instrument.FixtureTwoPortThreeOutput = Me.fixtureTwoPortThreeCheckBox.Checked
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT",
                                          "Timeout occurred when setting fixture two port 3. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED",
                                          "VISA Exception occurred when setting fixture two port 3. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED",
                                      "Exception occurred when setting fixture two port 3. Details: {0}.", ex)
            End Try
        End Sub

#End Region

#Region " READ / WRITE "

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.QueryTrimEnd(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.ReadString()
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Try
                Me._instrument.GpibSession.Write(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED WRITING",
                                          "Timeout occurred when writing. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED WRITING",
                                          "VISA Exception occurred when writing. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED WRITING",
                                      "Exception occurred when writing. Details: {0}.", ex)
            End Try
        End Sub

#End Region

#Region " SRQ "

        Private Sub enableServiceRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableServiceRequestCheckBox.CheckedChanged
            If MyBase.Visible AndAlso enableServiceRequestCheckBox.Enabled Then
                Me.Instrument.ClearExecutionState()
                Me.Instrument.ToggleServiceRequest(enableServiceRequestCheckBox.Checked, CType(serviceRequestMaskTextBox.Text, isr.Visa.Ieee4882.ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskAddButton.Click

            Dim selectedFlag As isr.Visa.Ieee4882.ServiceRequests =
                CType(serviceRequestFlagsComboBox.SelectedItem, isr.Visa.Ieee4882.ServiceRequests)

            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte Or selectedFlag)
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskRemoveButton.Click

            Dim selectedFlag As isr.Visa.Ieee4882.ServiceRequests =
                CType(serviceRequestFlagsComboBox.SelectedItem, isr.Visa.Ieee4882.ServiceRequests)

            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte And (Not selectedFlag))
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles serviceRequestMaskTextBox.Validating
            Dim data As String = serviceRequestMaskTextBox.Text.Trim

            If String.IsNullOrWhiteSpace(data) Then
                serviceRequestMaskTextBox.Text = "0"
                e.Cancel = True
                Return
            Else

                Dim value As Int32
                If Int32.TryParse(data, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, value) Then
                    If value < 0 Or value > 255 Then
                        serviceRequestMaskTextBox.Text = "0"
                        e.Cancel = True
                        Return
                    End If
                Else
                    serviceRequestMaskTextBox.Text = "0"
                    e.Cancel = True
                End If



            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace
