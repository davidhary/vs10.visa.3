Imports isr.Core.ControlExtensions
Imports isr.Core.CheckBoxExtensions
Namespace R2D2

    ''' <summary>Implements a VISA interface to a generic GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class W8200Instrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.GpibInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(Nothing, resourceTitle)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As W8200InstrumentPanel, ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        ' remove the reference to the Gui
                        Me._gui = Nothing

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PROPERTIES AND METHODS: GUI "

        Private _gui As W8200InstrumentPanel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As W8200InstrumentPanel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " WEINSCHEL MODEL 8200 CONTROLLER METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history date="04/28/2006" by="David" revision="1.0.2309.x">
        ''' Do not wait for OPC - This takes more code as the instrument does not comply with *OPC?.
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' allow operations to complete
                    Dim opc As Int32 = 1 ' MyBase.QueryInt32("OPC?")
                    Return opc = 1 ' True ' MyBase.IsOpc

                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED RESETTING AND CLEARING",
                                              "Timeout occurred resetting and clearing. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING AND CLEARING",
                                              "VISA Exception occurred resetting and clearing. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING AND CLEARING",
                                          "Exception occurred resetting and clearing. Details: {0}.", ex)
                End Try

            End If

            Return False

        End Function

        ''' <summary>
        ''' Defines the device with all its pins.
        ''' </summary>
        ''' <param name="deviceName">Specifies the device name</param>
        ''' <param name="devicePinsValue">Specifies the equivalent value of the device pins.</param>
        Public Sub DefineDevice(ByVal deviceName As String, ByVal devicePinsValue As Integer)

            If String.IsNullOrWhiteSpace(deviceName) OrElse devicePinsValue < 0 Then
                Return
            End If

            For i As Integer = 0 To 16
                Dim pinValue As Integer = Convert.ToInt32(Math.Pow(2, i), Globalization.CultureInfo.CurrentCulture)
                If (devicePinsValue And pinValue) = pinValue Then
                    MyBase.Write(String.Format(Globalization.CultureInfo.CurrentCulture,
                              "DEFINE {0},{1}", deviceName, fixtureOneInputSetting))
                End If
            Next

        End Sub

        ''' <summary>
        ''' Defines the default fixture input and output devices.
        ''' </summary>
        ''' <history>
        ''' 	[David]	4/1/2006	Created
        ''' </history>
        Public Sub DefineDefaultDevices()
            DefineDevice(fixtureInputDevice, 255)
            DefineDevice(fixtureOutputDevice, 255)
        End Sub

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="ServiceRequest">service request flags</see></param>
        Friend Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Visa.Ieee4882.ServiceRequests)
            If MyBase.GpibSession IsNot Nothing Then
                MyBase.GpibSession.ServiceRequestEventEnable() = serviceRequestMask
            End If
            If Me.Visible Then
                Me.Gui.ToggleServiceRequest(turnOn, serviceRequestMask)
            End If
        End Sub

#End Region

#Region " WEINSCHEL MODEL 8200 CONTROLLER PROPERTIES "

#Region " INPUT CONTROL "

        ''' <summary>
        ''' Holds the device name for input switches.
        ''' </summary>
        Private Const fixtureInputDevice As String = "A"

        Private _inputControl As Integer
        ''' <summary>Gets or sets the value of the input control.</summary>
        Public Property InputControl() As Integer
            Get
                Dim value As Nullable(Of Integer) = MyBase.QueryInt32(String.Format(Globalization.CultureInfo.CurrentCulture,
                  "SET? {0}", fixtureInputDevice))
                Me._inputControl = value.Value
                Return Me._inputControl
            End Get
            Set(ByVal Value As Integer)
                MyBase.Write(String.Format(Globalization.CultureInfo.CurrentCulture,
                          "SET {0} {1}", fixtureInputDevice, Value))
                If Value <> Me.InputControl Then
                    Throw New isr.Visa.BaseException("Failed setting the input controller")
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the status of a controller pins.</summary>
        Public ReadOnly Property InputFixtureControl(ByVal mask As Integer) As Boolean
            Get
                Return (Me.InputControl And mask) = mask
            End Get
        End Property

        ''' <summary>Gets or sets the status of a controller pins.</summary>
        ''' <param name="checkBoxControl"></param>
        Public WriteOnly Property InputFixtureControl(ByVal mask As Integer, ByVal checkBoxControl As Windows.Forms.CheckBox) As Boolean
            Set(ByVal Value As Boolean)
                If Value Then
                    Me.InputControl = Me.InputControl Or mask
                Else
                    Me.InputControl = Me.InputControl And (Not mask)
                End If
                If Me.Visible AndAlso checkBoxControl IsNot Nothing Then
                    checkBoxControl.SafeCheckedSetter((Me._inputControl And mask) = mask)
                End If
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pints that control input 1.
        ''' </summary>
        ''' <remarks>The value of hex F or 15 specifies the bottom 4 pins
        ''' </remarks>
        Private Const fixtureOneInputSetting As Int32 = &HF
        ''' <summary>Gets or sets the status of the input to fixture one.</summary>
        Public Property InputFixtureOne() As Boolean
            Get
                Return Me.InputFixtureControl(fixtureOneInputSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.InputFixtureControl(fixtureOneInputSetting, Me.Gui.InputFixtureOneCheckBox) = Value
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pints that control input 2.
        ''' </summary>
        ''' <remarks>The value of F0H or 240 specifies the top 4 pins
        ''' </remarks>
        Private Const fixtureTwoInputSetting As Int32 = &HF0
        ''' <summary>Gets or sets the status of the input to fixture two.
        ''' </summary>
        Public Property InputFixtureTwo() As Boolean
            Get
                Return Me.InputFixtureControl(fixtureTwoInputSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.InputFixtureControl(fixtureTwoInputSetting, Me.Gui.InputFixtureTwoCheckBox) = Value
            End Set
        End Property

#End Region

#Region " OUTPUT CONTROL "

        ''' <summary>
        ''' Holds the device name for output switches.
        ''' </summary>
        Private Const fixtureOutputDevice As String = "B"

        Private _outputControl As Int32
        ''' <summary>Gets or sets the value of the output control.</summary>
        Public Property OutputControl() As Int32
            Get
                Dim value As Nullable(Of Int32) = MyBase.QueryInt32(String.Format(Globalization.CultureInfo.CurrentCulture,
                  "SET? {0}", fixtureOutputDevice))
                Me._outputControl = value.Value
                Return Me._outputControl
            End Get
            Set(ByVal Value As Int32)
                MyBase.Write(String.Format(Globalization.CultureInfo.CurrentCulture,
                          "SET {0} {1}", fixtureOutputDevice, Value))
                If Value <> Me.OutputControl Then
                    Throw New isr.Visa.BaseException("Failed setting the output controller")
                End If
            End Set
        End Property

        ''' <summary>Gets the status of a controller pins.</summary>
        Public ReadOnly Property OutputFixtureControl(ByVal mask As Integer) As Boolean
            Get
                Return (Me.OutputControl And mask) = mask
            End Get
        End Property

        ''' <summary>Gets or sets the status of a controller pins.</summary>
        ''' <param name="checkBoxControl"></param>
        Public WriteOnly Property OutputFixtureControl(ByVal mask As Integer, ByVal checkBoxControl As Windows.Forms.CheckBox) As Boolean
            Set(ByVal Value As Boolean)
                If Value Then
                    Me.OutputControl = Me.OutputControl Or mask
                Else
                    Me.OutputControl = Me.OutputControl And (Not mask)
                End If
                If Me.Visible AndAlso checkBoxControl IsNot Nothing Then
                    checkBoxControl.SafeCheckedSetter((Me._outputControl And mask) = mask)
                End If
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pins that control output two on fixture one.
        ''' </summary>
        ''' <remarks>The value of 1 (was 3 in rev 2307) specifies the first output.
        ''' </remarks>
        ''' <history date="04/28/2006" by="David" revision="1.0.2309.x">
        ''' Use only a single pin for output.
        ''' </history>
        Private Const fixtureOnePortTwoSetting As Int32 = 1

        ''' <summary>Gets or sets the status of the output from fixture one port two.</summary>
        Public Property FixtureOnePortTwoOutput() As Boolean
            Get
                Return Me.OutputFixtureControl(fixtureOnePortTwoSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.OutputFixtureControl(fixtureOnePortTwoSetting, Me.Gui.fixtureOnePortTwoCheckBox) = Value
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pins that control output three on fixture one.
        ''' </summary>
        ''' <remarks>The value of 4 (was 12 in rev 2307) specifies the second output
        ''' </remarks>
        ''' <history date="04/28/2006" by="David" revision="1.0.2309.x">
        ''' Use only a single pin for output.
        ''' </history>
        Private Const fixtureOnePortThreeSetting As Int32 = 4

        ''' <summary>Gets or sets the status of the output from fixture one port three.</summary>
        Public Property FixtureOnePortThreeOutput() As Boolean
            Get
                Return Me.OutputFixtureControl(fixtureOnePortThreeSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.OutputFixtureControl(fixtureOnePortThreeSetting, Me.Gui.fixtureOnePortThreeCheckBox) = Value
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pins that control output two on fixture two.
        ''' </summary>
        ''' <remarks>The value of 16 (was 48 in rev 2307) specifies the third output.
        ''' </remarks>
        ''' <history date="04/28/2006" by="David" revision="1.0.2309.x">
        ''' Use only a single pin for output.
        ''' </history>
        Private Const fixtureTwoPortTwoSetting As Int32 = 16

        ''' <summary>Gets or sets the status of the output from fixture one port two.</summary>
        Public Property FixtureTwoPortTwoOutput() As Boolean
            Get
                Return Me.OutputFixtureControl(fixtureTwoPortTwoSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.OutputFixtureControl(fixtureTwoPortTwoSetting, Me.Gui.fixtureTwoPortTwoCheckBox) = Value
            End Set
        End Property

        ''' <summary>
        ''' Holds the value specifying the pins that control output port three on fixture two.
        ''' </summary>
        ''' <remarks>The value of 64 (was 192 in rev 2307) specifies the fourth output.
        ''' </remarks>
        ''' <history date="04/28/2006" by="David" revision="1.0.2309.x">
        ''' Use only a single pin for output.
        ''' </history>
        Private Const fixtureTwoPortThreeSetting As Int32 = 64

        ''' <summary>Gets or sets the status of the output from fixture two port three.</summary>
        Public Property FixtureTwoPortThreeOutput() As Boolean
            Get
                Return Me.OutputFixtureControl(fixtureTwoPortThreeSetting)
            End Get
            Set(ByVal Value As Boolean)
                Me.OutputFixtureControl(fixtureTwoPortThreeSetting, Me.Gui.fixtureTwoPortThreeCheckBox) = Value
            End Set
        End Property

#End Region

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Visa.R2D2.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Visa.R2D2.ServiceEventArgs) ' System.EventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then
            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class
End Namespace
