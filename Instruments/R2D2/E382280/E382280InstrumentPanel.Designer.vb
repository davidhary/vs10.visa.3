Namespace R2D2

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class E382280InstrumentPanel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.mainTabControl = New System.Windows.Forms.TabControl
            Me.controlTabPage = New System.Windows.Forms.TabPage
            Me.controlPanel = New System.Windows.Forms.Panel
            Me.fixedSupplyGroupBox = New System.Windows.Forms.GroupBox
            Me.fixedSupplyVoltageNumericUpDown = New System.Windows.Forms.NumericUpDown
            Me.fixedSupplyVoltageNumericUpDownLabel = New System.Windows.Forms.Label
            Me.getFixedSupplyVoltageButton = New System.Windows.Forms.Button
            Me.fixedSupplyCurrentNumericUpDown = New System.Windows.Forms.NumericUpDown
            Me.fixedSupplyCurrentNumericUpDownLabel = New System.Windows.Forms.Label
            Me.getFixedSupplyCurrentButton = New System.Windows.Forms.Button
            Me.variableSupplyGroupBox = New System.Windows.Forms.GroupBox
            Me.getVoltageButton = New System.Windows.Forms.Button
            Me.voltageNumericUpDown = New System.Windows.Forms.NumericUpDown
            Me.setVoltageButton = New System.Windows.Forms.Button
            Me.voltageNumericUpDownLabel = New System.Windows.Forms.Label
            Me.setCurrentButton = New System.Windows.Forms.Button
            Me.currentNumericUpDownLabel = New System.Windows.Forms.Label
            Me.currentNumericUpDown = New System.Windows.Forms.NumericUpDown
            Me.getCurrentButton = New System.Windows.Forms.Button
            Me.OperationToggleButton = New System.Windows.Forms.CheckBox
            Me.readWriteTabPage = New System.Windows.Forms.TabPage
            Me.queryButton = New System.Windows.Forms.Button
            Me.readButton = New System.Windows.Forms.Button
            Me.readTextBox = New System.Windows.Forms.TextBox
            Me.readTextBoxLabel = New System.Windows.Forms.Label
            Me.writeButton = New System.Windows.Forms.Button
            Me.writeTextBox = New System.Windows.Forms.TextBox
            Me.writeTextBoxLabel = New System.Windows.Forms.Label
            Me.messagesTabPage = New System.Windows.Forms.TabPage
            Me.messagesMessageList = New isr.Controls.MessagesBox
            Me.mainTabControl.SuspendLayout()
            Me.controlTabPage.SuspendLayout()
            Me.controlPanel.SuspendLayout()
            Me.fixedSupplyGroupBox.SuspendLayout()
            CType(Me.fixedSupplyVoltageNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fixedSupplyCurrentNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.variableSupplyGroupBox.SuspendLayout()
            CType(Me.voltageNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.currentNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.readWriteTabPage.SuspendLayout()
            Me.messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'statusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'idPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            'mainTabControl
            '
            Me.mainTabControl.Controls.Add(Me.controlTabPage)
            Me.mainTabControl.Controls.Add(Me.readWriteTabPage)
            Me.mainTabControl.Controls.Add(Me.messagesTabPage)
            Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.mainTabControl.Enabled = False
            Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
            Me.mainTabControl.Multiline = True
            Me.mainTabControl.Name = "mainTabControl"
            Me.mainTabControl.SelectedIndex = 0
            Me.mainTabControl.Size = New System.Drawing.Size(312, 233)
            Me.mainTabControl.TabIndex = 15
            '
            'controlTabPage
            '
            Me.controlTabPage.Controls.Add(Me.controlPanel)
            Me.controlTabPage.Controls.Add(Me.OperationToggleButton)
            Me.controlTabPage.Location = New System.Drawing.Point(4, 22)
            Me.controlTabPage.Name = "controlTabPage"
            Me.controlTabPage.Size = New System.Drawing.Size(304, 207)
            Me.controlTabPage.TabIndex = 4
            Me.controlTabPage.Text = "Control"
            '
            'controlPanel
            '
            Me.controlPanel.Controls.Add(Me.fixedSupplyGroupBox)
            Me.controlPanel.Controls.Add(Me.variableSupplyGroupBox)
            Me.controlPanel.Enabled = False
            Me.controlPanel.Location = New System.Drawing.Point(0, 0)
            Me.controlPanel.Name = "controlPanel"
            Me.controlPanel.Size = New System.Drawing.Size(304, 174)
            Me.controlPanel.TabIndex = 1
            '
            'fixedSupplyGroupBox
            '
            Me.fixedSupplyGroupBox.Controls.Add(Me.fixedSupplyVoltageNumericUpDown)
            Me.fixedSupplyGroupBox.Controls.Add(Me.fixedSupplyVoltageNumericUpDownLabel)
            Me.fixedSupplyGroupBox.Controls.Add(Me.getFixedSupplyVoltageButton)
            Me.fixedSupplyGroupBox.Controls.Add(Me.fixedSupplyCurrentNumericUpDown)
            Me.fixedSupplyGroupBox.Controls.Add(Me.fixedSupplyCurrentNumericUpDownLabel)
            Me.fixedSupplyGroupBox.Controls.Add(Me.getFixedSupplyCurrentButton)
            Me.fixedSupplyGroupBox.Location = New System.Drawing.Point(8, 90)
            Me.fixedSupplyGroupBox.Name = "fixedSupplyGroupBox"
            Me.fixedSupplyGroupBox.Size = New System.Drawing.Size(288, 76)
            Me.fixedSupplyGroupBox.TabIndex = 17
            Me.fixedSupplyGroupBox.TabStop = False
            Me.fixedSupplyGroupBox.Text = "+5 Volt Supply"
            '
            'fixedSupplyVoltageNumericUpDown
            '
            Me.fixedSupplyVoltageNumericUpDown.DecimalPlaces = 3
            Me.fixedSupplyVoltageNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.fixedSupplyVoltageNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
            Me.fixedSupplyVoltageNumericUpDown.Location = New System.Drawing.Point(104, 19)
            Me.fixedSupplyVoltageNumericUpDown.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
            Me.fixedSupplyVoltageNumericUpDown.Name = "fixedSupplyVoltageNumericUpDown"
            Me.fixedSupplyVoltageNumericUpDown.ReadOnly = True
            Me.fixedSupplyVoltageNumericUpDown.Size = New System.Drawing.Size(64, 21)
            Me.fixedSupplyVoltageNumericUpDown.TabIndex = 14
            Me.fixedSupplyVoltageNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 196608})
            '
            'fixedSupplyVoltageNumericUpDownLabel
            '
            Me.fixedSupplyVoltageNumericUpDownLabel.Location = New System.Drawing.Point(32, 21)
            Me.fixedSupplyVoltageNumericUpDownLabel.Name = "fixedSupplyVoltageNumericUpDownLabel"
            Me.fixedSupplyVoltageNumericUpDownLabel.Size = New System.Drawing.Size(64, 16)
            Me.fixedSupplyVoltageNumericUpDownLabel.TabIndex = 13
            Me.fixedSupplyVoltageNumericUpDownLabel.Text = "Voltage (V): "
            Me.fixedSupplyVoltageNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'getFixedSupplyVoltageButton
            '
            Me.getFixedSupplyVoltageButton.Location = New System.Drawing.Point(168, 18)
            Me.getFixedSupplyVoltageButton.Name = "getFixedSupplyVoltageButton"
            Me.getFixedSupplyVoltageButton.Size = New System.Drawing.Size(40, 23)
            Me.getFixedSupplyVoltageButton.TabIndex = 12
            Me.getFixedSupplyVoltageButton.Text = "Get"
            '
            'fixedSupplyCurrentNumericUpDown
            '
            Me.fixedSupplyCurrentNumericUpDown.DecimalPlaces = 3
            Me.fixedSupplyCurrentNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.fixedSupplyCurrentNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
            Me.fixedSupplyCurrentNumericUpDown.Location = New System.Drawing.Point(104, 47)
            Me.fixedSupplyCurrentNumericUpDown.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
            Me.fixedSupplyCurrentNumericUpDown.Name = "fixedSupplyCurrentNumericUpDown"
            Me.fixedSupplyCurrentNumericUpDown.ReadOnly = True
            Me.fixedSupplyCurrentNumericUpDown.Size = New System.Drawing.Size(64, 21)
            Me.fixedSupplyCurrentNumericUpDown.TabIndex = 11
            Me.fixedSupplyCurrentNumericUpDown.Value = New Decimal(New Integer() {2, 0, 0, 196608})
            '
            'fixedSupplyCurrentNumericUpDownLabel
            '
            Me.fixedSupplyCurrentNumericUpDownLabel.Location = New System.Drawing.Point(32, 49)
            Me.fixedSupplyCurrentNumericUpDownLabel.Name = "fixedSupplyCurrentNumericUpDownLabel"
            Me.fixedSupplyCurrentNumericUpDownLabel.Size = New System.Drawing.Size(64, 16)
            Me.fixedSupplyCurrentNumericUpDownLabel.TabIndex = 10
            Me.fixedSupplyCurrentNumericUpDownLabel.Text = "Current [A]:"
            Me.fixedSupplyCurrentNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'getFixedSupplyCurrentButton
            '
            Me.getFixedSupplyCurrentButton.Location = New System.Drawing.Point(168, 46)
            Me.getFixedSupplyCurrentButton.Name = "getFixedSupplyCurrentButton"
            Me.getFixedSupplyCurrentButton.Size = New System.Drawing.Size(40, 23)
            Me.getFixedSupplyCurrentButton.TabIndex = 9
            Me.getFixedSupplyCurrentButton.Text = "Get"
            '
            'variableSupplyGroupBox
            '
            Me.variableSupplyGroupBox.Controls.Add(Me.getVoltageButton)
            Me.variableSupplyGroupBox.Controls.Add(Me.voltageNumericUpDown)
            Me.variableSupplyGroupBox.Controls.Add(Me.setVoltageButton)
            Me.variableSupplyGroupBox.Controls.Add(Me.voltageNumericUpDownLabel)
            Me.variableSupplyGroupBox.Controls.Add(Me.setCurrentButton)
            Me.variableSupplyGroupBox.Controls.Add(Me.currentNumericUpDownLabel)
            Me.variableSupplyGroupBox.Controls.Add(Me.currentNumericUpDown)
            Me.variableSupplyGroupBox.Controls.Add(Me.getCurrentButton)
            Me.variableSupplyGroupBox.Location = New System.Drawing.Point(8, 8)
            Me.variableSupplyGroupBox.Name = "variableSupplyGroupBox"
            Me.variableSupplyGroupBox.Size = New System.Drawing.Size(288, 76)
            Me.variableSupplyGroupBox.TabIndex = 16
            Me.variableSupplyGroupBox.TabStop = False
            Me.variableSupplyGroupBox.Text = "Variable Supply"
            '
            'getVoltageButton
            '
            Me.getVoltageButton.Location = New System.Drawing.Point(168, 18)
            Me.getVoltageButton.Name = "getVoltageButton"
            Me.getVoltageButton.Size = New System.Drawing.Size(40, 23)
            Me.getVoltageButton.TabIndex = 1
            Me.getVoltageButton.Text = "Get"
            '
            'voltageNumericUpDown
            '
            Me.voltageNumericUpDown.DecimalPlaces = 3
            Me.voltageNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.voltageNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
            Me.voltageNumericUpDown.Location = New System.Drawing.Point(104, 19)
            Me.voltageNumericUpDown.Maximum = New Decimal(New Integer() {40, 0, 0, 0})
            Me.voltageNumericUpDown.Name = "voltageNumericUpDown"
            Me.voltageNumericUpDown.Size = New System.Drawing.Size(64, 21)
            Me.voltageNumericUpDown.TabIndex = 4
            Me.voltageNumericUpDown.Value = New Decimal(New Integer() {1000, 0, 0, 196608})
            '
            'setVoltageButton
            '
            Me.setVoltageButton.Location = New System.Drawing.Point(216, 18)
            Me.setVoltageButton.Name = "setVoltageButton"
            Me.setVoltageButton.Size = New System.Drawing.Size(40, 23)
            Me.setVoltageButton.TabIndex = 0
            Me.setVoltageButton.Text = "Set"
            '
            'voltageNumericUpDownLabel
            '
            Me.voltageNumericUpDownLabel.Location = New System.Drawing.Point(32, 21)
            Me.voltageNumericUpDownLabel.Name = "voltageNumericUpDownLabel"
            Me.voltageNumericUpDownLabel.Size = New System.Drawing.Size(64, 16)
            Me.voltageNumericUpDownLabel.TabIndex = 3
            Me.voltageNumericUpDownLabel.Text = "Voltage [V]:"
            Me.voltageNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'setCurrentButton
            '
            Me.setCurrentButton.Location = New System.Drawing.Point(216, 46)
            Me.setCurrentButton.Name = "setCurrentButton"
            Me.setCurrentButton.Size = New System.Drawing.Size(40, 23)
            Me.setCurrentButton.TabIndex = 5
            Me.setCurrentButton.Text = "Set"
            Me.setCurrentButton.Visible = False
            '
            'currentNumericUpDownLabel
            '
            Me.currentNumericUpDownLabel.Location = New System.Drawing.Point(32, 49)
            Me.currentNumericUpDownLabel.Name = "currentNumericUpDownLabel"
            Me.currentNumericUpDownLabel.Size = New System.Drawing.Size(64, 16)
            Me.currentNumericUpDownLabel.TabIndex = 7
            Me.currentNumericUpDownLabel.Text = "Current [A]:"
            Me.currentNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'currentNumericUpDown
            '
            Me.currentNumericUpDown.DecimalPlaces = 3
            Me.currentNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.currentNumericUpDown.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
            Me.currentNumericUpDown.Location = New System.Drawing.Point(104, 47)
            Me.currentNumericUpDown.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
            Me.currentNumericUpDown.Name = "currentNumericUpDown"
            Me.currentNumericUpDown.ReadOnly = True
            Me.currentNumericUpDown.Size = New System.Drawing.Size(64, 21)
            Me.currentNumericUpDown.TabIndex = 8
            Me.currentNumericUpDown.Value = New Decimal(New Integer() {1007, 0, 0, 196608})
            '
            'getCurrentButton
            '
            Me.getCurrentButton.Location = New System.Drawing.Point(168, 46)
            Me.getCurrentButton.Name = "getCurrentButton"
            Me.getCurrentButton.Size = New System.Drawing.Size(40, 23)
            Me.getCurrentButton.TabIndex = 6
            Me.getCurrentButton.Text = "Get"
            '
            'OperationToggleButton
            '
            Me.OperationToggleButton.Appearance = System.Windows.Forms.Appearance.Button
            Me.OperationToggleButton.Enabled = False
            Me.OperationToggleButton.Location = New System.Drawing.Point(128, 178)
            Me.OperationToggleButton.Name = "OperationToggleButton"
            Me.OperationToggleButton.Size = New System.Drawing.Size(168, 24)
            Me.OperationToggleButton.TabIndex = 0
            Me.OperationToggleButton.Text = "OPERATION:: STANDBY"
            Me.OperationToggleButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'readWriteTabPage
            '
            Me.readWriteTabPage.Controls.Add(Me.queryButton)
            Me.readWriteTabPage.Controls.Add(Me.readButton)
            Me.readWriteTabPage.Controls.Add(Me.readTextBox)
            Me.readWriteTabPage.Controls.Add(Me.readTextBoxLabel)
            Me.readWriteTabPage.Controls.Add(Me.writeButton)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBox)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBoxLabel)
            Me.readWriteTabPage.Location = New System.Drawing.Point(4, 22)
            Me.readWriteTabPage.Name = "readWriteTabPage"
            Me.readWriteTabPage.Size = New System.Drawing.Size(304, 207)
            Me.readWriteTabPage.TabIndex = 1
            Me.readWriteTabPage.Text = "Read/Write"
            '
            'queryButton
            '
            Me.queryButton.Location = New System.Drawing.Point(35, 177)
            Me.queryButton.Name = "queryButton"
            Me.queryButton.Size = New System.Drawing.Size(75, 23)
            Me.queryButton.TabIndex = 11
            Me.queryButton.Text = "&Query"
            '
            'readButton
            '
            Me.readButton.Location = New System.Drawing.Point(195, 177)
            Me.readButton.Name = "readButton"
            Me.readButton.Size = New System.Drawing.Size(75, 23)
            Me.readButton.TabIndex = 10
            Me.readButton.Text = "&Read"
            '
            'readTextBox
            '
            Me.readTextBox.Location = New System.Drawing.Point(12, 83)
            Me.readTextBox.Multiline = True
            Me.readTextBox.Name = "readTextBox"
            Me.readTextBox.Size = New System.Drawing.Size(280, 88)
            Me.readTextBox.TabIndex = 9
            '
            'readTextBoxLabel
            '
            Me.readTextBoxLabel.Location = New System.Drawing.Point(12, 67)
            Me.readTextBoxLabel.Name = "readTextBoxLabel"
            Me.readTextBoxLabel.Size = New System.Drawing.Size(116, 16)
            Me.readTextBoxLabel.TabIndex = 8
            Me.readTextBoxLabel.Text = "Message Received:"
            Me.readTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'writeButton
            '
            Me.writeButton.Location = New System.Drawing.Point(115, 177)
            Me.writeButton.Name = "writeButton"
            Me.writeButton.Size = New System.Drawing.Size(75, 23)
            Me.writeButton.TabIndex = 2
            Me.writeButton.Text = "&Write"
            '
            'writeTextBox
            '
            Me.writeTextBox.Location = New System.Drawing.Point(12, 19)
            Me.writeTextBox.Multiline = True
            Me.writeTextBox.Name = "writeTextBox"
            Me.writeTextBox.Size = New System.Drawing.Size(280, 40)
            Me.writeTextBox.TabIndex = 1
            Me.writeTextBox.Tag = "Enter test to send to the instrument"
            Me.writeTextBox.Text = "*IDN?"
            '
            'writeTextBoxLabel
            '
            Me.writeTextBoxLabel.Location = New System.Drawing.Point(12, 3)
            Me.writeTextBoxLabel.Name = "writeTextBoxLabel"
            Me.writeTextBoxLabel.Size = New System.Drawing.Size(132, 16)
            Me.writeTextBoxLabel.TabIndex = 0
            Me.writeTextBoxLabel.Text = "Message to Send:"
            Me.writeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'messagesTabPage
            '
            Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
            Me.messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me.messagesTabPage.Name = "messagesTabPage"
            Me.messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me.messagesTabPage.TabIndex = 3
            Me.messagesTabPage.Text = "Messages"
            '
            'messagesMessageList
            '
            Me.messagesMessageList.Bullet = "* "
            Me.messagesMessageList.Delimiter = "; "
            Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
            Me.messagesMessageList.Multiline = True
            Me.messagesMessageList.Name = "messagesMessageList"
            Me.messagesMessageList.PresetCount = 50
            Me.messagesMessageList.ReadOnly = True
            Me.messagesMessageList.ResetCount = 100
            Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.messagesMessageList.Size = New System.Drawing.Size(304, 207)
            Me.messagesMessageList.TabIndex = 0
            Me.messagesMessageList.TimeFormat = "HH:mm:ss.f"
            Me.messagesMessageList.UsingBullet = True
            Me.messagesMessageList.UsingSynopsis = False
            Me.messagesMessageList.UsingTimeBullet = True
            Me.messagesMessageList.UsingTraceLevel = False
            '
            'E382280InstrumentPanel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.mainTabControl)
            Me.Name = "E382280InstrumentPanel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me.mainTabControl, 0)
            Me.mainTabControl.ResumeLayout(False)
            Me.controlTabPage.ResumeLayout(False)
            Me.controlPanel.ResumeLayout(False)
            Me.fixedSupplyGroupBox.ResumeLayout(False)
            CType(Me.fixedSupplyVoltageNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fixedSupplyCurrentNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            Me.variableSupplyGroupBox.ResumeLayout(False)
            CType(Me.voltageNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.currentNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            Me.readWriteTabPage.ResumeLayout(False)
            Me.readWriteTabPage.PerformLayout()
            Me.messagesTabPage.ResumeLayout(False)
            Me.messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
        Friend WithEvents controlTabPage As System.Windows.Forms.TabPage
        Friend WithEvents controlPanel As System.Windows.Forms.Panel
        Friend WithEvents fixedSupplyGroupBox As System.Windows.Forms.GroupBox
        Friend WithEvents fixedSupplyVoltageNumericUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents fixedSupplyVoltageNumericUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents getFixedSupplyVoltageButton As System.Windows.Forms.Button
        Friend WithEvents fixedSupplyCurrentNumericUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents fixedSupplyCurrentNumericUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents getFixedSupplyCurrentButton As System.Windows.Forms.Button
        Friend WithEvents variableSupplyGroupBox As System.Windows.Forms.GroupBox
        Friend WithEvents getVoltageButton As System.Windows.Forms.Button
        Friend WithEvents voltageNumericUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents setVoltageButton As System.Windows.Forms.Button
        Friend WithEvents voltageNumericUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents setCurrentButton As System.Windows.Forms.Button
        Friend WithEvents currentNumericUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents currentNumericUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents getCurrentButton As System.Windows.Forms.Button
        Friend WithEvents OperationToggleButton As System.Windows.Forms.CheckBox
        Friend WithEvents readWriteTabPage As System.Windows.Forms.TabPage
        Friend WithEvents queryButton As System.Windows.Forms.Button
        Friend WithEvents readButton As System.Windows.Forms.Button
        Friend WithEvents readTextBox As System.Windows.Forms.TextBox
        Friend WithEvents readTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents writeButton As System.Windows.Forms.Button
        Friend WithEvents writeTextBox As System.Windows.Forms.TextBox
        Friend WithEvents writeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents messagesMessageList As isr.Controls.MessagesBox

    End Class

End Namespace
