Namespace R2D2

    ''' <summary>Provides a user interface for the EXTECH 382280 Power Supply.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("EXTECH 382280 Power Supply - Windows Forms Custom Control")>
    Public Class E382280InstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New E382280Instrument(Me, Me.Name)

            ' set default value for the device and enable connection:
            MyBase.Connector.SelectedName = "ASRL1::INSTR"

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me.Instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                messagesMessageList.PrependMessage(e.Details)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

#End Region

#Region " METHODS "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me.mainTabControl.Enabled = value
                If value Then

                    ' enable the operation control
                    Me.OperationToggleButton.Enabled = False
                    Me.OperationToggleButton.Checked = True
                    Me.OperationToggleButton.Checked = False
                    Me.OperationToggleButton.Invalidate()
                    Me.OperationToggleButton.Enabled = True

                    ' update the current scan list
                    Me.refreshDisplay()

                    If True Then
                    Else
                        Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "CONNECTED",
                                              "Manufacturer name: {1}{0}Manufacturer ID: 0x{2:X}{0}Connected to {3}",
                                              Environment.NewLine,
                                              Me._instrument.SerialSession.ResourceManufacturerName,
                                              Me._instrument.SerialSession.ResourceManufacturerID,
                                              Me._instrument.Id())
                    End If
                Else
                    Me.controlPanel.Enabled = False
                    Me.OperationToggleButton.Enabled = False
                End If
            End Set
        End Property

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.Instruments.R2D2.E382280Instrument
        ''' <summary>Gets a reference to the EXTECH 382280 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As isr.Visa.Instruments.R2D2.E382280Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As isr.Visa.Instruments.R2D2.E382280Instrument)
                Me._instrument = value
                MyBase.ConnectableResource = value
            End Set
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

#Region " READ / WRITE "

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.SerialSession.Query(
                    Me.writeTextBox.Text & Convert.ToChar(Me._instrument.SerialSession.TerminationCharacter))
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.SerialSession.ReadString()
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Try
                Me._instrument.SerialSession.Write(Me.writeTextBox.Text)
                Me._instrument.SerialSession.Write(Convert.ToChar(Me._instrument.SerialSession.TerminationCharacter))
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED WRITING",
                                          "Timeout occurred when writing. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED WRITING",
                                          "VISA Exception occurred when writing. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED WRITING",
                                      "Exception occurred when writing. Details: {0}.", ex)
            End Try
        End Sub

#End Region

        Private Sub OperationToggleButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperationToggleButton.CheckedChanged

            If Me.OperationToggleButton.Enabled Then
                Try
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    Me._instrument.OperationOn = Me.OperationToggleButton.Checked
                    Me.controlPanel.Enabled = Me.OperationToggleButton.Checked
                Catch
                    Throw
                Finally
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                End Try
            End If

            If Me.OperationToggleButton.Checked Then
                Me.OperationToggleButton.Text = "OPERATION: ON"
            Else
                Me.OperationToggleButton.Text = "OPERATION: STANDBY"
            End If

        End Sub

        Private Sub getVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getVoltageButton.Click
            Try
                Me.StatusStrip.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                  "Reading = {0}", Me._instrument.Voltage)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub setVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles setVoltageButton.Click
            Try
                Me._instrument.Voltage = Me.voltageNumericUpDown.Value
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub setCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles setCurrentButton.Click
            Try
                Me._instrument.Current = Me.currentNumericUpDown.Value
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getCurrentButton.Click
            Try
                Me.StatusStrip.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                  "Reading = {0}", Me._instrument.Current)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getFixedSupplyCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getFixedSupplyCurrentButton.Click
            Try
                Me.StatusStrip.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                  "Reading = {0}", Me._instrument.FixedSupplyCurrent)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getFixedSupplyVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getFixedSupplyVoltageButton.Click
            Try
                Me.StatusStrip.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                  "Reading = {0}", Me._instrument.FixedSupplyVoltage)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

#End Region

    End Class

End Namespace
