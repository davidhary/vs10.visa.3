Namespace R2D2

    ''' <summary>Implements a VISA interface for the EXTECH 382280 Power Supply.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class E382280Instrument
        Inherits isr.Visa.R2D2.SerialInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(Nothing, resourceTitle)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As E382280InstrumentPanel, ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)
            Me._gui = gui

            Me._currentReading = New isr.Scpi.MeasurandDouble
            Me._currentReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Current
            Me._currentReading.SaveCaption.Units = Me._currentReading.DisplayCaption.Units
            Me._currentReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
            Me._currentReading.HighLimit = isr.Scpi.Syntax.Infinity
            Me._currentReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity

            Me._voltageReading = New isr.Scpi.MeasurandDouble()
            Me._voltageReading.DisplayCaption.Units.PhysicalUnits = isr.Scpi.PhysicalUnit.Voltage
            Me._voltageReading.SaveCaption.Units = Me._voltageReading.DisplayCaption.Units
            Me._voltageReading.ComplianceLimit = isr.Scpi.Syntax.Infinity
            Me._voltageReading.HighLimit = isr.Scpi.Syntax.Infinity
            Me._voltageReading.LowLimit = isr.Scpi.Syntax.NegativeInfinity

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._currentReading IsNot Nothing Then
                            Me._currentReading = Nothing
                        End If

                        If Me._voltageReading IsNot Nothing Then
                            Me._voltageReading = Nothing
                        End If

                        ' remove the reference to the Gui
                        Me._gui = Nothing

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PROPERTIES AND METHODS: GUI "

        ''' <summary>
        ''' Opens a VISA session for the instruments at the given resource name.
        ''' </summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If MyBase.Connect(resourceName) Then

                ' set termination character to carriage return
                If MyBase.SerialSession IsNot Nothing Then
                    MyBase.SerialSession.TerminationCharacter = 13
                End If

            End If

            Return MyBase.IsConnected

        End Function

        ''' <summary>
        ''' Opens a VISA session for the instruments at the given port.
        ''' </summary>
        ''' <param name="address">Specifies the instrument port number.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Function Connect(ByVal address As Int32) As Boolean

            Return Connect(isr.Visa.My.MyLibrary.BuildSerialResourceName(address))

        End Function

        Private _gui As E382280InstrumentPanel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As E382280InstrumentPanel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " EXTECH 382280 METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            Return MyBase.ResetAndClear()

        End Function

#End Region

#Region " EXTECH 382280 PROPERTIES "

        Private _currentReading As isr.Scpi.MeasurandDouble
        Public ReadOnly Property CurrentReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._currentReading
            End Get
        End Property

        Const usingExternalCurrentMeter As Boolean = True
        Private _lastCurrentValue As String = "0"
        ''' <summary>Gets or sets the supply current
        ''' </summary>
        Public Property Current() As Double
            Get
                Dim outcome As Double = 0
                If usingExternalCurrentMeter Then
                    Using inputBox As isr.WindowsForms.InputBox = New isr.WindowsForms.InputBox
                        inputBox.EnteredValue = Me._lastCurrentValue
                        inputBox.NumberStyle = Globalization.NumberStyles.Number
                        inputBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Enter {0} Supply Current", MyBase.ResourceTitle)
                        If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            Dim newValue As String = inputBox.EnteredValue
                            If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                                Me._lastCurrentValue = newValue
                            End If
                        End If
                    End Using
                Else
                    MyBase.AwaitCommandInterval()
                    Dim question As String = "A?"
                    Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                    outcome = reply.MeasuredValue
                End If
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "A {0}", Value))
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Public Property NewCurrent() As Double
            Get
                If usingExternalCurrentMeter Then
                    Using inputBox As isr.WindowsForms.InputBox = New isr.WindowsForms.InputBox
                        inputBox.EnteredValue = Me._lastCurrentValue
                        inputBox.NumberStyle = Globalization.NumberStyles.Number
                        inputBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Enter {0} Supply Current in {1}", MyBase.ResourceTitle, Me._currentReading.DisplayCaption.Units.LongUnits)
                        If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            Dim newValue As String = inputBox.EnteredValue
                            If Me._currentReading.Parse(newValue) Then
                                Me._lastCurrentValue = newValue
                            End If
                        End If
                    End Using
                Else
                    MyBase.AwaitCommandInterval()
                    Dim question As String = "A?"
                    Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                    Me._currentReading.Reading = reply.MeasuredValueReply
                End If
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(Me._currentReading.Value, Globalization.CultureInfo.CurrentCulture)
                End If
                Return Me._currentReading.Value.Value
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "A {0}", Value))
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Private _lastFixedSupplyCurrent As String = "0"
        ''' <summary>Gets the value of the fixed supply current from the user.
        ''' </summary>
        Public ReadOnly Property FixedSupplyCurrent() As Double
            Get
                Using inputBox As isr.WindowsForms.InputBox = New isr.WindowsForms.InputBox
                    inputBox.EnteredValue = Me._lastFixedSupplyCurrent
                    inputBox.NumberStyle = Globalization.NumberStyles.Number
                    Dim outcome As Double = 0
                    If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim newValue As String = inputBox.EnteredValue
                        If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                            Me._lastFixedSupplyCurrent = newValue
                        End If
                    End If
                    If Me.Visible Then
                        Me.Gui.fixedSupplyCurrentNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                    End If
                    Return outcome
                End Using
            End Get
        End Property

        Private _lastFixedSupplyVoltage As String = "0"
        ''' <summary>Gets the value of the fixed supply voltage from the user.
        ''' </summary>
        Public ReadOnly Property FixedSupplyVoltage() As Double
            Get
                Using inputBox As isr.WindowsForms.InputBox = New isr.WindowsForms.InputBox
                    inputBox.EnteredValue = Me._lastFixedSupplyVoltage
                    inputBox.NumberStyle = Globalization.NumberStyles.Number
                    Dim outcome As Double = 0
                    If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim newValue As String = inputBox.EnteredValue
                        If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                            Me._lastFixedSupplyVoltage = newValue
                        End If
                    End If
                    If Me.Visible Then
                        Me.Gui.fixedSupplyVoltageNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                    End If
                    Return outcome
                End Using
            End Get
        End Property

        Private Const standbyStatus As String = "0V"
        ''' <summary>Gets or sets the instrument operation status.
        ''' </summary>
        Public Property OperationOn() As Boolean
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Return Not reply.Status.Equals(standbyStatus)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.AwaitCommandInterval()
                If Value Then
                    MyBase.WriteTerminate("OPER")
                Else
                    MyBase.WriteTerminate("STBY")
                    MyBase.AwaitCommandInterval(2000)
                End If
                If Me.Visible Then
                    If Me.Gui.OperationToggleButton.Enabled Then
                        Me.Gui.OperationToggleButton.Enabled = False
                        Me.Gui.OperationToggleButton.Checked = Me.OperationOn
                        Me.Gui.OperationToggleButton.Invalidate()
                        Me.Gui.OperationToggleButton.Enabled = True
                    Else
                        Me.Gui.OperationToggleButton.Checked = Me.OperationOn
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the supply voltage
        ''' </summary>
        Public Property Voltage() As Double
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Dim outcome As Double = reply.MeasuredValue
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "V {0}", Value))
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the supply voltage
        ''' </summary>
        Public Property NewVoltage() As Double
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Me._voltageReading.Reading = reply.MeasuredValueReply
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Me._voltageReading.Value, Globalization.CultureInfo.CurrentCulture)
                End If
                Return Me._voltageReading.Value.Value
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "V {0}", Value))
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Private _voltageReading As isr.Scpi.MeasurandDouble
        Public ReadOnly Property VoltageReading() As isr.Scpi.MeasurandDouble
            Get
                Return Me._voltageReading
            End Get
        End Property

#End Region

    End Class
End Namespace

Public Class OutputReply

    Public Sub New(ByVal reply As String)
        MyBase.new()
        Me.ParseReply(reply)
    End Sub

    Private _queryType As String
    Public ReadOnly Property QueryType() As String
        Get
            Return Me._outputElements(0)
        End Get
    End Property

    Private _setValue As Double
    Public ReadOnly Property SetValue() As Double
        Get
            Return Me._setValue
        End Get
    End Property

    Private _measuredValue As Double
    Public ReadOnly Property MeasuredValue() As Double
        Get
            Return Me._measuredValue
        End Get
    End Property

    Private _status As String
    Public ReadOnly Property Status() As String
        Get
            Return Me._outputElements(3)
        End Get
    End Property

    Private _outputElements As String()
    Public ReadOnly Property SetValueReply() As String
        Get
            Return Me._outputElements(1)
        End Get
    End Property

    Public ReadOnly Property MeasuredValueReply() As String
        Get
            Return Me._outputElements(2)
        End Get
    End Property

    Private Function DefaultElements() As String()
        Dim elements As String() = {_queryType, Me._setValue.ToString(Globalization.CultureInfo.CurrentCulture),
                                    Me._measuredValue.ToString(Globalization.CultureInfo.CurrentCulture), Me._status}
        Return elements
    End Function

    ''' <summary>Query voltage or current and return a response array with elements.    ''' </summary>
    ''' <param name="reply"></param>
    Public Sub ParseReply(ByVal reply As String)
        Me._queryType = String.Empty
        Me._setValue = 0
        Me._measuredValue = 0
        Me._status = String.Empty
        Me._outputElements = DefaultElements()
        If Not String.IsNullOrWhiteSpace(reply) Then
            Me._outputElements = reply.Split(" "c)
            If Me._outputElements.Length >= 4 Then
                Me._queryType = Me.QueryType
                Me._status = Me.Status
                If Double.TryParse(Me.SetValueReply, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, Me._setValue) Then
                End If
                If Double.TryParse(Me.MeasuredValueReply, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, Me._measuredValue) Then
                End If
            Else
                Me._outputElements = DefaultElements()
            End If
        End If
    End Sub

End Class