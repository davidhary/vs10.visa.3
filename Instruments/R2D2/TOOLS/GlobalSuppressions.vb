Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"
' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.Visa.Instruments", 
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 

<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.Visa.Instruments.My")> 

#End Region

<Module: SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope:="member", Target:="isr.Visa.Instruments.R2D2.W8200Instrument.#OutputFixtureControl[System.Int32,System.Windows.Forms.CheckBox]")> 
<Module: SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope:="member", Target:="isr.Visa.Instruments.R2D2.W8200Instrument.#InputFixtureControl[System.Int32,System.Windows.Forms.CheckBox]")> 