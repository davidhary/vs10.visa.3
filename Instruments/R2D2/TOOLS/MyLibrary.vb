Namespace My

    ''' <summary>Defines a singleton class to provide project management
    ''' for this project. This class has the Public Shared Not Creatable
    ''' instancing property.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="06/13/2006" by="David" revision="1.15.2355.x">
    ''' Created
    ''' </history> 
    Public NotInheritable Class MyLibrary

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Private Sub New()

            MyBase.New()

        End Sub

        ''' <summary>Terminates the class members, disposes its internal objects,
        ''' and terminates references to external objects.
        ''' </summary>
    Private Shared Sub destroy()

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
    Private Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Private Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        My.MyLibrary.destroy()

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " PROPERTIES: BOARDCAST AND TRACE LEVELS "

        ''' <summary>
        ''' Gets the default broadcast level.
        ''' </summary>
        Public Const DefaultBroadcastLevel As TraceEventType = TraceEventType.Warning

        ''' <summary>
        ''' Gets the default broadcast level.
        ''' </summary>
        Public Const DefaultTraceLevel As TraceEventType = TraceEventType.Warning

        Private Shared _broadcastLevel As TraceEventType = DefaultBroadcastLevel
        ''' <summary>
        ''' Gets or sets the Broadcast level.
        ''' </summary>
        Public Shared Property BroadcastLevel() As TraceEventType
            Get
                Return MyLibrary._broadcastLevel
            End Get
            Set(ByVal value As TraceEventType)
                MyLibrary._broadcastLevel = value
            End Set
        End Property

        ''' <summary>
        ''' Returns true if the broadcast or trace levels are higher than the default level.
        ''' </summary>
            Public Shared Function MustBroadcast() As Boolean
            Return (My.MyLibrary.BroadcastLevel <= My.MyLibrary.DefaultBroadcastLevel) OrElse
                MustTrace()
        End Function

        ''' <summary>
        ''' Returns true if the specified broadcast or trace levels are lower than the library levels.
        ''' </summary>
            Public Shared Function MustBroadcast(ByVal broadcastLevel As TraceEventType, ByVal traceLevel As TraceEventType) As Boolean
            Return (broadcastLevel <= My.MyLibrary.BroadcastLevel) OrElse MustTrace(traceLevel)
        End Function

        ''' <summary>
        ''' Returns true if the broadcast or trace levels are higher than the default level.
        ''' </summary>
            Public Shared Function MustTrace() As Boolean
            Return My.MyLibrary.TraceLevel <= My.MyLibrary.DefaultTraceLevel
        End Function

        ''' <summary>
        ''' Returns true if the specified broadcast or trace levels are lower than the library levels.
        ''' </summary>
            Public Shared Function MustTrace(ByVal traceLevel As TraceEventType) As Boolean
            Return traceLevel <= My.MyLibrary.TraceLevel
        End Function

        ''' <summary>
        ''' Replaces the default file log trace listener with a new one.
        ''' </summary>
    ''' <param name="logWriter"></param>
            Public Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
            Application.Log.TraceSource.Listeners.Remove("FileLog")
            Application.Log.TraceSource.Listeners.Add(logWriter)
        End Sub

        Private Shared _traceLevel As TraceEventType = DefaultTraceLevel
        ''' <summary>
        ''' Gets or sets the trace level.
        ''' </summary>
        Public Shared Property TraceLevel() As TraceEventType
            Get
                Return MyLibrary._traceLevel
            End Get
            Set(ByVal value As TraceEventType)
                MyLibrary._traceLevel = value
                My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), MyLibrary._traceLevel.ToString), SourceLevels)
            End Set
        End Property

#End Region

    End Class

End Namespace
