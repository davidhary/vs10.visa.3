Namespace My

  ''' <summary>
  ''' Handles application level functionality.
  ''' </summary>
  ''' <license>
  ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
  ''' <history date="03/14/06" by="David" revision="1.0.2264.x">
  ''' Created
''' </history> 
  Partial Friend Class MyApplication

#Region " APPLICATION LOG "

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="details">Specifies the message details</param>
    ''' <returns>Message or empty string.</returns>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
      If details IsNot Nothing Then
        My.Application.Log.WriteEntry(details, severity)
        Return details
      End If
      Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="format">Specifies the message format</param>
    ''' <param name="args">Specified the message arguments</param>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
      If format IsNot Nothing Then
        Return WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
      End If
      Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="messages">Message information to log.</param>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
      If messages IsNot Nothing Then
        Return WriteLogEntry(severity, String.Join(",", messages))
      End If
      Return ""
    End Function

    ''' <summary>
    ''' Adds exception details to the error log.  Includes stack and data.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <param name="severity">Specifies the exception severity.</param>
    ''' <param name="additionalInfo">Specifies additional information.</param>
    Private Shared Sub _writeExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, 
        ByVal additionalInfo As String)

      My.Application.Log.WriteException(ex, severity, additionalInfo)
      If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
        Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
        WriteLogEntry(severity, stackTrace)
      End If
      If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
        For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
          My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
        Next
      End If
      If ex.InnerException IsNot Nothing Then
        MyApplication._writeExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
      End If

    End Sub

    ''' <summary>
    ''' Adds exception details to the error log.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <param name="severity">Specifies the exception severity.</param>
    ''' <param name="additionalInfo">Specifies additional information.</param>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, 
        ByVal additionalInfo As String)

      ' write exception details.
      MyApplication._writeExceptionDetails(ex, severity, additionalInfo)

    End Sub

    ''' <summary>
    ''' Adds exception details to the error log.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
      MyApplication.WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
    End Sub

#End Region

#Region " EXCEPTION HANDLING "

    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from GUI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    ''' <param name="additionalInfo">Specifies additional log information.</param>
    ''' <param name="buttons">Specifies the buttons to show on the exception display.</param>
    Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, ByVal buttons As isr.WindowsForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

      Dim result As Windows.Forms.DialogResult
      Try

        ' log the exception in details.
        My.MyApplication.WriteExceptionDetails(ex, TraceEventType.Critical, additionalInfo)

        Dim frm As New isr.WindowsForms.ExceptionDisplay
        result = frm.ShowDialog(ex, buttons)
        My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture, 
            "{0} requested by user.", result), TraceEventType.Verbose)

      Catch displayException As System.Exception

        ' Log but also display the error in a message box
        MyApplication.WriteExceptionDetails(displayException, TraceEventType.Critical, "Exception occurred displaying application exception.")

        Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
        errorMessage.Append("The following error occurred while displaying the application exception:")
        errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
        errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}Click Abort to exit application.  Otherwise, the application will continue.", Environment.NewLine)
        result = Windows.Forms.MessageBox.Show( 
          errorMessage.ToString(), "Application Error", Windows.Forms.MessageBoxButtons.AbortRetryIgnore, Windows.Forms.MessageBoxIcon.Stop, Windows.Forms.MessageBoxDefaultButton.Button2, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

      End Try

      Return result

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Returns a string selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

    ''' <summary>Returns an Integer selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Integer, ByVal falsePart As Integer) As Integer
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

#If False Then
    ''' <summary>Returns a double selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Double, ByVal falsePart As Double) As Double
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function
#End If

    Private Shared _NA As String = "NA"
    Public Shared ReadOnly Property NA() As String
      Get
        Return Me._NA
      End Get
    End Property

#End Region

  End Class
End Namespace

