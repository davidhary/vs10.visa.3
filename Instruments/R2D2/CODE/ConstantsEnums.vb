Namespace R2D2

#Region " EVENT FLAGS "

    ''' <summary>Enumerates the status byte flags of the operation event register.</summary>
    <System.Flags()> Public Enum OperationEvents
        <ComponentModel.Description("None")> None = 0
        <ComponentModel.Description("Settling")> Settling = 2
        <ComponentModel.Description("Waiting For Trigger")> WaitingForTrigger = 32
        <ComponentModel.Description("Waiting For Arm")> WaitingForArm = 64
        <ComponentModel.Description("Idle")> Idle = 1024
    End Enum

    ''' <summary>Enumerates the status byte flags of the operation transition event register.</summary>
    <System.Flags()> Public Enum OperationTransitionEvents
        <ComponentModel.Description("None")> None = 0
        <ComponentModel.Description("Settling")> Settling = 2
        <ComponentModel.Description("Waiting For Trigger")> WaitingForTrigger = 32
        <ComponentModel.Description("Waiting For Arm")> WaitingForArm = 64
        <ComponentModel.Description("Idle ")> Idle = 1024
    End Enum

#End Region

End Namespace
