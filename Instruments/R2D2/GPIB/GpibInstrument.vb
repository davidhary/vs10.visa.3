Namespace R2D2

    ''' <summary>Implements a VISA interface to a generic GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class GpibInstrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.GpibInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(Nothing, resourceTitle)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As GpibInstrumentPanel, ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)
            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        ' remove the reference to the Gui
                        Me._gui = Nothing

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PROPERTIES AND METHODS: GUI "

        Private _gui As GpibInstrumentPanel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As GpibInstrumentPanel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " GENERIC GPIB INSTRUMENT METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                ' allow operations to complete
                Return MyBase.IsOpc

            End If

            Return False

        End Function

#End Region

#Region " GENERIC GPIB INSTRUMENT PROPERTIES "

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="ServiceRequest">service request flags</see></param>
        Friend Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As isr.Visa.Ieee4882.ServiceRequests)
            If MyBase.GpibSession IsNot Nothing Then
                MyBase.GpibSession.ServiceRequestEventEnable() = serviceRequestMask
            End If
            If Me.Visible Then
                Me.Gui.ToggleServiceRequest(turnOn, serviceRequestMask)
            End If
        End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Visa.R2D2.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Visa.R2D2.ServiceEventArgs) '  System.EventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then
                ' check if we have an operation request
                If (e.OperationEventStatus And OperationEvents.Settling) <> 0 Then

                    If Me.Gui.Visible Then
                        ' display the operation duration
                        ' MyBase.DisplayInfoProvider("New message available under the Messages tab")
                    End If
                End If
            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class
End Namespace
