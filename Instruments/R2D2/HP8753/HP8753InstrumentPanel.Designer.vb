Namespace R2D2

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class HP8753InstrumentPanel
        Inherits isr.Controls.ResourcePanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.mainTabControl = New System.Windows.Forms.TabControl
            Me.controlTabPage = New System.Windows.Forms.TabPage
            Me.localButton = New System.Windows.Forms.Button
            Me.ThirdValueLabel = New System.Windows.Forms.Label
            Me.SecondValueLabel = New System.Windows.Forms.Label
            Me.readMarkerButton = New System.Windows.Forms.Button
            Me.markerNumberUpDown = New System.Windows.Forms.NumericUpDown
            Me.markerNumberUpDownLabel = New System.Windows.Forms.Label
            Me.FirstValueLabel = New System.Windows.Forms.Label
            Me.setupTabPage = New System.Windows.Forms.TabPage
            Me.defaultsComboBoxLabel = New System.Windows.Forms.Label
            Me.defaultsComboBox = New System.Windows.Forms.ComboBox
            Me.defaultsButton = New System.Windows.Forms.Button
            Me.scaleComboBoxLabel = New System.Windows.Forms.Label
            Me.scaleComboBox = New System.Windows.Forms.ComboBox
            Me.channelNumeric = New System.Windows.Forms.NumericUpDown
            Me.channelNumericLabel = New System.Windows.Forms.Label
            Me.parameterComboBoxLabel = New System.Windows.Forms.Label
            Me.parameterComboBox = New System.Windows.Forms.ComboBox
            Me.powerNumeric = New System.Windows.Forms.NumericUpDown
            Me.powerNumericLabel = New System.Windows.Forms.Label
            Me.traceStopNumeric = New System.Windows.Forms.NumericUpDown
            Me.traceStopNumericLabel = New System.Windows.Forms.Label
            Me.traceStartNumeric = New System.Windows.Forms.NumericUpDown
            Me.traceStartNumericLabel = New System.Windows.Forms.Label
            Me.markerTwoNumeric = New System.Windows.Forms.NumericUpDown
            Me.markerTwoNumericLabel = New System.Windows.Forms.Label
            Me.applyButton = New System.Windows.Forms.Button
            Me.markerOneNumeric = New System.Windows.Forms.NumericUpDown
            Me.markerOneNumericLabel = New System.Windows.Forms.Label
            Me.readWriteTabPage = New System.Windows.Forms.TabPage
            Me.queryButton = New System.Windows.Forms.Button
            Me.readButton = New System.Windows.Forms.Button
            Me.readTextBox = New System.Windows.Forms.TextBox
            Me.readTextBoxLabel = New System.Windows.Forms.Label
            Me.readWriteStatusTextBox = New System.Windows.Forms.TextBox
            Me.readWriteStatusTextBoxLabel = New System.Windows.Forms.Label
            Me.writeButton = New System.Windows.Forms.Button
            Me.writeTextBox = New System.Windows.Forms.TextBox
            Me.writeTextBoxLabel = New System.Windows.Forms.Label
            Me.messagesTabPage = New System.Windows.Forms.TabPage
            Me.messagesMessageList = New isr.Controls.MessagesBox
            Me.mainTabControl.SuspendLayout()
            Me.controlTabPage.SuspendLayout()
            CType(Me.markerNumberUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.setupTabPage.SuspendLayout()
            CType(Me.channelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.powerNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.traceStopNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.traceStartNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.markerTwoNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.markerOneNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.readWriteTabPage.SuspendLayout()
            Me.messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'Connector
            '
            Me.Connector.Location = New System.Drawing.Point(0, 233)
            '
            'MainStatusBar
            '
            Me.StatusStrip.Location = New System.Drawing.Point(0, 264)
            '
            'statusPanel
            '
            Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
            Me.StatusToolStripStatusLabel.Width = 302
            '
            'idPanel
            '
            Me.IdentityToolStripStatusLabel.Text = ""
            Me.IdentityToolStripStatusLabel.Width = 10
            '
            'mainTabControl
            '
            Me.mainTabControl.Controls.Add(Me.controlTabPage)
            Me.mainTabControl.Controls.Add(Me.setupTabPage)
            Me.mainTabControl.Controls.Add(Me.readWriteTabPage)
            Me.mainTabControl.Controls.Add(Me.messagesTabPage)
            Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.mainTabControl.Enabled = False
            Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
            Me.mainTabControl.Multiline = True
            Me.mainTabControl.Name = "mainTabControl"
            Me.mainTabControl.SelectedIndex = 0
            Me.mainTabControl.Size = New System.Drawing.Size(312, 233)
            Me.mainTabControl.TabIndex = 15
            '
            'controlTabPage
            '
            Me.controlTabPage.Controls.Add(Me.localButton)
            Me.controlTabPage.Controls.Add(Me.ThirdValueLabel)
            Me.controlTabPage.Controls.Add(Me.SecondValueLabel)
            Me.controlTabPage.Controls.Add(Me.readMarkerButton)
            Me.controlTabPage.Controls.Add(Me.markerNumberUpDown)
            Me.controlTabPage.Controls.Add(Me.markerNumberUpDownLabel)
            Me.controlTabPage.Controls.Add(Me.FirstValueLabel)
            Me.controlTabPage.Location = New System.Drawing.Point(4, 22)
            Me.controlTabPage.Name = "controlTabPage"
            Me.controlTabPage.Size = New System.Drawing.Size(304, 207)
            Me.controlTabPage.TabIndex = 5
            Me.controlTabPage.Text = "Control"
            '
            'localButton
            '
            Me.localButton.Location = New System.Drawing.Point(230, 176)
            Me.localButton.Name = "localButton"
            Me.localButton.Size = New System.Drawing.Size(64, 24)
            Me.localButton.TabIndex = 14
            Me.localButton.Text = "GTL"
            '
            'ThirdValueLabel
            '
            Me.ThirdValueLabel.BackColor = System.Drawing.Color.MidnightBlue
            Me.ThirdValueLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.ThirdValueLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ThirdValueLabel.ForeColor = System.Drawing.Color.Aqua
            Me.ThirdValueLabel.Location = New System.Drawing.Point(8, 76)
            Me.ThirdValueLabel.Name = "ThirdValueLabel"
            Me.ThirdValueLabel.Size = New System.Drawing.Size(288, 32)
            Me.ThirdValueLabel.TabIndex = 12
            Me.ThirdValueLabel.Text = "+000.000000000000000E-00"
            Me.ThirdValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.ThirdValueLabel.UseMnemonic = False
            '
            'SecondValueLabel
            '
            Me.SecondValueLabel.BackColor = System.Drawing.Color.MidnightBlue
            Me.SecondValueLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.SecondValueLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.SecondValueLabel.ForeColor = System.Drawing.Color.Aqua
            Me.SecondValueLabel.Location = New System.Drawing.Point(8, 42)
            Me.SecondValueLabel.Name = "SecondValueLabel"
            Me.SecondValueLabel.Size = New System.Drawing.Size(288, 32)
            Me.SecondValueLabel.TabIndex = 11
            Me.SecondValueLabel.Text = "+000.000000000000000E-00"
            Me.SecondValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.SecondValueLabel.UseMnemonic = False
            '
            'readMarkerButton
            '
            Me.readMarkerButton.Location = New System.Drawing.Point(160, 120)
            Me.readMarkerButton.Name = "readMarkerButton"
            Me.readMarkerButton.Size = New System.Drawing.Size(75, 23)
            Me.readMarkerButton.TabIndex = 7
            Me.readMarkerButton.Text = "&Read"
            '
            'markerNumberUpDown
            '
            Me.markerNumberUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.markerNumberUpDown.Location = New System.Drawing.Point(120, 121)
            Me.markerNumberUpDown.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
            Me.markerNumberUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me.markerNumberUpDown.Name = "markerNumberUpDown"
            Me.markerNumberUpDown.Size = New System.Drawing.Size(40, 21)
            Me.markerNumberUpDown.TabIndex = 6
            Me.markerNumberUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
            '
            'markerNumberUpDownLabel
            '
            Me.markerNumberUpDownLabel.Location = New System.Drawing.Point(48, 123)
            Me.markerNumberUpDownLabel.Name = "markerNumberUpDownLabel"
            Me.markerNumberUpDownLabel.Size = New System.Drawing.Size(72, 16)
            Me.markerNumberUpDownLabel.TabIndex = 5
            Me.markerNumberUpDownLabel.Text = "Marker: "
            Me.markerNumberUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'FirstValueLabel
            '
            Me.FirstValueLabel.BackColor = System.Drawing.Color.MidnightBlue
            Me.FirstValueLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.FirstValueLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.FirstValueLabel.ForeColor = System.Drawing.Color.Aqua
            Me.FirstValueLabel.Location = New System.Drawing.Point(8, 8)
            Me.FirstValueLabel.Name = "FirstValueLabel"
            Me.FirstValueLabel.Size = New System.Drawing.Size(288, 32)
            Me.FirstValueLabel.TabIndex = 4
            Me.FirstValueLabel.Text = "+000.000000000000000E-00"
            Me.FirstValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.FirstValueLabel.UseMnemonic = False
            '
            'setupTabPage
            '
            Me.setupTabPage.Controls.Add(Me.defaultsComboBoxLabel)
            Me.setupTabPage.Controls.Add(Me.defaultsComboBox)
            Me.setupTabPage.Controls.Add(Me.defaultsButton)
            Me.setupTabPage.Controls.Add(Me.scaleComboBoxLabel)
            Me.setupTabPage.Controls.Add(Me.scaleComboBox)
            Me.setupTabPage.Controls.Add(Me.channelNumeric)
            Me.setupTabPage.Controls.Add(Me.channelNumericLabel)
            Me.setupTabPage.Controls.Add(Me.parameterComboBoxLabel)
            Me.setupTabPage.Controls.Add(Me.parameterComboBox)
            Me.setupTabPage.Controls.Add(Me.powerNumeric)
            Me.setupTabPage.Controls.Add(Me.powerNumericLabel)
            Me.setupTabPage.Controls.Add(Me.traceStopNumeric)
            Me.setupTabPage.Controls.Add(Me.traceStopNumericLabel)
            Me.setupTabPage.Controls.Add(Me.traceStartNumeric)
            Me.setupTabPage.Controls.Add(Me.traceStartNumericLabel)
            Me.setupTabPage.Controls.Add(Me.markerTwoNumeric)
            Me.setupTabPage.Controls.Add(Me.markerTwoNumericLabel)
            Me.setupTabPage.Controls.Add(Me.applyButton)
            Me.setupTabPage.Controls.Add(Me.markerOneNumeric)
            Me.setupTabPage.Controls.Add(Me.markerOneNumericLabel)
            Me.setupTabPage.Location = New System.Drawing.Point(4, 22)
            Me.setupTabPage.Name = "setupTabPage"
            Me.setupTabPage.Size = New System.Drawing.Size(304, 207)
            Me.setupTabPage.TabIndex = 4
            Me.setupTabPage.Text = "Setup"
            '
            'defaultsComboBoxLabel
            '
            Me.defaultsComboBoxLabel.Location = New System.Drawing.Point(32, 138)
            Me.defaultsComboBoxLabel.Name = "defaultsComboBoxLabel"
            Me.defaultsComboBoxLabel.Size = New System.Drawing.Size(56, 16)
            Me.defaultsComboBoxLabel.TabIndex = 21
            Me.defaultsComboBoxLabel.Text = "Test: "
            Me.defaultsComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'defaultsComboBox
            '
            Me.defaultsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.defaultsComboBox.Items.AddRange(New Object() {"S11", "S22", "S21 Mag", "S21 Phase"})
            Me.defaultsComboBox.Location = New System.Drawing.Point(88, 136)
            Me.defaultsComboBox.Name = "defaultsComboBox"
            Me.defaultsComboBox.Size = New System.Drawing.Size(104, 21)
            Me.defaultsComboBox.TabIndex = 20
            '
            'defaultsButton
            '
            Me.defaultsButton.Location = New System.Drawing.Point(192, 136)
            Me.defaultsButton.Name = "defaultsButton"
            Me.defaultsButton.Size = New System.Drawing.Size(75, 23)
            Me.defaultsButton.TabIndex = 19
            Me.defaultsButton.Text = "Set &Defaults"
            '
            'scaleComboBoxLabel
            '
            Me.scaleComboBoxLabel.Location = New System.Drawing.Point(152, 90)
            Me.scaleComboBoxLabel.Name = "scaleComboBoxLabel"
            Me.scaleComboBoxLabel.Size = New System.Drawing.Size(64, 16)
            Me.scaleComboBoxLabel.TabIndex = 18
            Me.scaleComboBoxLabel.Text = "Scale: "
            Me.scaleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'scaleComboBox
            '
            Me.scaleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.scaleComboBox.Items.AddRange(New Object() {"LOGM", "PHASE"})
            Me.scaleComboBox.Location = New System.Drawing.Point(224, 88)
            Me.scaleComboBox.Name = "scaleComboBox"
            Me.scaleComboBox.Size = New System.Drawing.Size(56, 21)
            Me.scaleComboBox.TabIndex = 17
            '
            'channelNumeric
            '
            Me.channelNumeric.AccessibleRole = System.Windows.Forms.AccessibleRole.Chart
            Me.channelNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.channelNumeric.Location = New System.Drawing.Point(224, 64)
            Me.channelNumeric.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
            Me.channelNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
            Me.channelNumeric.Name = "channelNumeric"
            Me.channelNumeric.Size = New System.Drawing.Size(40, 21)
            Me.channelNumeric.TabIndex = 14
            Me.channelNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
            '
            'channelNumericLabel
            '
            Me.channelNumericLabel.Location = New System.Drawing.Point(160, 66)
            Me.channelNumericLabel.Name = "channelNumericLabel"
            Me.channelNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.channelNumericLabel.TabIndex = 13
            Me.channelNumericLabel.Text = "Channel: "
            Me.channelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'parameterComboBoxLabel
            '
            Me.parameterComboBoxLabel.Location = New System.Drawing.Point(160, 42)
            Me.parameterComboBoxLabel.Name = "parameterComboBoxLabel"
            Me.parameterComboBoxLabel.Size = New System.Drawing.Size(64, 16)
            Me.parameterComboBoxLabel.TabIndex = 12
            Me.parameterComboBoxLabel.Text = "Parameter: "
            Me.parameterComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'parameterComboBox
            '
            Me.parameterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.parameterComboBox.Items.AddRange(New Object() {"S11", "S12", "S21", "S22"})
            Me.parameterComboBox.Location = New System.Drawing.Point(224, 40)
            Me.parameterComboBox.Name = "parameterComboBox"
            Me.parameterComboBox.Size = New System.Drawing.Size(56, 21)
            Me.parameterComboBox.TabIndex = 11
            '
            'powerNumeric
            '
            Me.powerNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.powerNumeric.Location = New System.Drawing.Point(224, 16)
            Me.powerNumeric.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
            Me.powerNumeric.Minimum = New Decimal(New Integer() {40, 0, 0, -2147483648})
            Me.powerNumeric.Name = "powerNumeric"
            Me.powerNumeric.Size = New System.Drawing.Size(56, 21)
            Me.powerNumeric.TabIndex = 10
            Me.powerNumeric.Value = New Decimal(New Integer() {20, 0, 0, -2147483648})
            '
            'powerNumericLabel
            '
            Me.powerNumericLabel.Location = New System.Drawing.Point(160, 18)
            Me.powerNumericLabel.Name = "powerNumericLabel"
            Me.powerNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.powerNumericLabel.TabIndex = 9
            Me.powerNumericLabel.Text = "Power: "
            Me.powerNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'traceStopNumeric
            '
            Me.traceStopNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.traceStopNumeric.Location = New System.Drawing.Point(80, 40)
            Me.traceStopNumeric.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
            Me.traceStopNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
            Me.traceStopNumeric.Name = "traceStopNumeric"
            Me.traceStopNumeric.Size = New System.Drawing.Size(56, 21)
            Me.traceStopNumeric.TabIndex = 8
            Me.traceStopNumeric.Value = New Decimal(New Integer() {1600, 0, 0, 0})
            '
            'traceStopNumericLabel
            '
            Me.traceStopNumericLabel.Location = New System.Drawing.Point(16, 42)
            Me.traceStopNumericLabel.Name = "traceStopNumericLabel"
            Me.traceStopNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.traceStopNumericLabel.TabIndex = 7
            Me.traceStopNumericLabel.Text = "Trace Stop: "
            Me.traceStopNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'traceStartNumeric
            '
            Me.traceStartNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.traceStartNumeric.Location = New System.Drawing.Point(80, 16)
            Me.traceStartNumeric.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
            Me.traceStartNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
            Me.traceStartNumeric.Name = "traceStartNumeric"
            Me.traceStartNumeric.Size = New System.Drawing.Size(56, 21)
            Me.traceStartNumeric.TabIndex = 6
            Me.traceStartNumeric.Value = New Decimal(New Integer() {1200, 0, 0, 0})
            '
            'traceStartNumericLabel
            '
            Me.traceStartNumericLabel.Location = New System.Drawing.Point(16, 18)
            Me.traceStartNumericLabel.Name = "traceStartNumericLabel"
            Me.traceStartNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.traceStartNumericLabel.TabIndex = 5
            Me.traceStartNumericLabel.Text = "Trace Start: "
            Me.traceStartNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'markerTwoNumeric
            '
            Me.markerTwoNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.markerTwoNumeric.Location = New System.Drawing.Point(80, 88)
            Me.markerTwoNumeric.Maximum = New Decimal(New Integer() {1600, 0, 0, 0})
            Me.markerTwoNumeric.Minimum = New Decimal(New Integer() {1200, 0, 0, 0})
            Me.markerTwoNumeric.Name = "markerTwoNumeric"
            Me.markerTwoNumeric.Size = New System.Drawing.Size(56, 21)
            Me.markerTwoNumeric.TabIndex = 4
            Me.markerTwoNumeric.Value = New Decimal(New Integer() {1575, 0, 0, 0})
            '
            'markerTwoNumericLabel
            '
            Me.markerTwoNumericLabel.Location = New System.Drawing.Point(16, 90)
            Me.markerTwoNumericLabel.Name = "markerTwoNumericLabel"
            Me.markerTwoNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.markerTwoNumericLabel.TabIndex = 3
            Me.markerTwoNumericLabel.Text = "Marker 2: "
            Me.markerTwoNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'applyButton
            '
            Me.applyButton.Location = New System.Drawing.Point(224, 178)
            Me.applyButton.Name = "applyButton"
            Me.applyButton.Size = New System.Drawing.Size(75, 23)
            Me.applyButton.TabIndex = 2
            Me.applyButton.Text = "&Apply"
            '
            'markerOneNumeric
            '
            Me.markerOneNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.markerOneNumeric.Location = New System.Drawing.Point(80, 64)
            Me.markerOneNumeric.Maximum = New Decimal(New Integer() {1600, 0, 0, 0})
            Me.markerOneNumeric.Minimum = New Decimal(New Integer() {1200, 0, 0, 0})
            Me.markerOneNumeric.Name = "markerOneNumeric"
            Me.markerOneNumeric.Size = New System.Drawing.Size(56, 21)
            Me.markerOneNumeric.TabIndex = 1
            Me.markerOneNumeric.Value = New Decimal(New Integer() {1227, 0, 0, 0})
            '
            'markerOneNumericLabel
            '
            Me.markerOneNumericLabel.Location = New System.Drawing.Point(16, 66)
            Me.markerOneNumericLabel.Name = "markerOneNumericLabel"
            Me.markerOneNumericLabel.Size = New System.Drawing.Size(64, 16)
            Me.markerOneNumericLabel.TabIndex = 0
            Me.markerOneNumericLabel.Text = "Marker 1: "
            Me.markerOneNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'readWriteTabPage
            '
            Me.readWriteTabPage.Controls.Add(Me.queryButton)
            Me.readWriteTabPage.Controls.Add(Me.readButton)
            Me.readWriteTabPage.Controls.Add(Me.readTextBox)
            Me.readWriteTabPage.Controls.Add(Me.readTextBoxLabel)
            Me.readWriteTabPage.Controls.Add(Me.readWriteStatusTextBox)
            Me.readWriteTabPage.Controls.Add(Me.readWriteStatusTextBoxLabel)
            Me.readWriteTabPage.Controls.Add(Me.writeButton)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBox)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBoxLabel)
            Me.readWriteTabPage.Location = New System.Drawing.Point(4, 22)
            Me.readWriteTabPage.Name = "readWriteTabPage"
            Me.readWriteTabPage.Size = New System.Drawing.Size(304, 207)
            Me.readWriteTabPage.TabIndex = 1
            Me.readWriteTabPage.Text = "Read/Write"
            '
            'queryButton
            '
            Me.queryButton.Location = New System.Drawing.Point(35, 179)
            Me.queryButton.Name = "queryButton"
            Me.queryButton.Size = New System.Drawing.Size(75, 23)
            Me.queryButton.TabIndex = 10
            Me.queryButton.Text = "&Query"
            '
            'readButton
            '
            Me.readButton.Location = New System.Drawing.Point(195, 179)
            Me.readButton.Name = "readButton"
            Me.readButton.Size = New System.Drawing.Size(75, 23)
            Me.readButton.TabIndex = 9
            Me.readButton.Text = "&Read"
            '
            'readTextBox
            '
            Me.readTextBox.Location = New System.Drawing.Point(12, 82)
            Me.readTextBox.Multiline = True
            Me.readTextBox.Name = "readTextBox"
            Me.readTextBox.Size = New System.Drawing.Size(280, 92)
            Me.readTextBox.TabIndex = 8
            '
            'readTextBoxLabel
            '
            Me.readTextBoxLabel.Location = New System.Drawing.Point(12, 66)
            Me.readTextBoxLabel.Name = "readTextBoxLabel"
            Me.readTextBoxLabel.Size = New System.Drawing.Size(148, 16)
            Me.readTextBoxLabel.TabIndex = 7
            Me.readTextBoxLabel.Text = "Message Received:"
            Me.readTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'readWriteStatusTextBox
            '
            Me.readWriteStatusTextBox.Location = New System.Drawing.Point(220, 8)
            Me.readWriteStatusTextBox.Name = "readWriteStatusTextBox"
            Me.readWriteStatusTextBox.Size = New System.Drawing.Size(72, 20)
            Me.readWriteStatusTextBox.TabIndex = 4
            '
            'readWriteStatusTextBoxLabel
            '
            Me.readWriteStatusTextBoxLabel.Location = New System.Drawing.Point(159, 10)
            Me.readWriteStatusTextBoxLabel.Name = "readWriteStatusTextBoxLabel"
            Me.readWriteStatusTextBoxLabel.Size = New System.Drawing.Size(56, 16)
            Me.readWriteStatusTextBoxLabel.TabIndex = 3
            Me.readWriteStatusTextBoxLabel.Text = "Status: "
            Me.readWriteStatusTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'writeButton
            '
            Me.writeButton.Location = New System.Drawing.Point(115, 179)
            Me.writeButton.Name = "writeButton"
            Me.writeButton.Size = New System.Drawing.Size(75, 23)
            Me.writeButton.TabIndex = 2
            Me.writeButton.Text = "&Write"
            '
            'writeTextBox
            '
            Me.writeTextBox.Location = New System.Drawing.Point(12, 32)
            Me.writeTextBox.Multiline = True
            Me.writeTextBox.Name = "writeTextBox"
            Me.writeTextBox.Size = New System.Drawing.Size(280, 32)
            Me.writeTextBox.TabIndex = 1
            Me.writeTextBox.Text = "*IDN?"
            '
            'writeTextBoxLabel
            '
            Me.writeTextBoxLabel.Location = New System.Drawing.Point(12, 16)
            Me.writeTextBoxLabel.Name = "writeTextBoxLabel"
            Me.writeTextBoxLabel.Size = New System.Drawing.Size(100, 16)
            Me.writeTextBoxLabel.TabIndex = 0
            Me.writeTextBoxLabel.Text = "Message To Send: "
            Me.writeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'messagesTabPage
            '
            Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
            Me.messagesTabPage.Location = New System.Drawing.Point(4, 22)
            Me.messagesTabPage.Name = "messagesTabPage"
            Me.messagesTabPage.Size = New System.Drawing.Size(304, 207)
            Me.messagesTabPage.TabIndex = 3
            Me.messagesTabPage.Text = "Messages"
            '
            'messagesMessageList
            '
            Me.messagesMessageList.Bullet = "* "
            Me.messagesMessageList.Delimiter = "; "
            Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
            Me.messagesMessageList.Multiline = True
            Me.messagesMessageList.Name = "messagesMessageList"
            Me.messagesMessageList.PresetCount = 50
            Me.messagesMessageList.ReadOnly = True
            Me.messagesMessageList.ResetCount = 100
            Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.messagesMessageList.Size = New System.Drawing.Size(304, 207)
            Me.messagesMessageList.TabIndex = 0
            Me.messagesMessageList.TimeFormat = "HH:mm:ss.f"
            Me.messagesMessageList.UsingBullet = True
            Me.messagesMessageList.UsingSynopsis = False
            Me.messagesMessageList.UsingTimeBullet = True
            Me.messagesMessageList.UsingTraceLevel = False
            '
            'HP8753InstrumentPanel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.mainTabControl)
            Me.Name = "HP8753InstrumentPanel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.StatusStrip, 0)
            Me.Controls.SetChildIndex(Me.Connector, 0)
            Me.Controls.SetChildIndex(Me.mainTabControl, 0)
            Me.mainTabControl.ResumeLayout(False)
            Me.controlTabPage.ResumeLayout(False)
            CType(Me.markerNumberUpDown, System.ComponentModel.ISupportInitialize).EndInit()
            Me.setupTabPage.ResumeLayout(False)
            CType(Me.channelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.powerNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.traceStopNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.traceStartNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.markerTwoNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.markerOneNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            Me.readWriteTabPage.ResumeLayout(False)
            Me.readWriteTabPage.PerformLayout()
            Me.messagesTabPage.ResumeLayout(False)
            Me.messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
        Friend WithEvents controlTabPage As System.Windows.Forms.TabPage
        Friend WithEvents localButton As System.Windows.Forms.Button
        Friend WithEvents ThirdValueLabel As System.Windows.Forms.Label
        Friend WithEvents SecondValueLabel As System.Windows.Forms.Label
        Friend WithEvents readMarkerButton As System.Windows.Forms.Button
        Friend WithEvents markerNumberUpDown As System.Windows.Forms.NumericUpDown
        Friend WithEvents markerNumberUpDownLabel As System.Windows.Forms.Label
        Friend WithEvents FirstValueLabel As System.Windows.Forms.Label
        Friend WithEvents setupTabPage As System.Windows.Forms.TabPage
        Friend WithEvents defaultsComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents defaultsComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents defaultsButton As System.Windows.Forms.Button
        Friend WithEvents scaleComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents scaleComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents channelNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents channelNumericLabel As System.Windows.Forms.Label
        Friend WithEvents parameterComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents parameterComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents powerNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents powerNumericLabel As System.Windows.Forms.Label
        Friend WithEvents traceStopNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents traceStopNumericLabel As System.Windows.Forms.Label
        Friend WithEvents traceStartNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents traceStartNumericLabel As System.Windows.Forms.Label
        Friend WithEvents markerTwoNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents markerTwoNumericLabel As System.Windows.Forms.Label
        Friend WithEvents applyButton As System.Windows.Forms.Button
        Friend WithEvents markerOneNumeric As System.Windows.Forms.NumericUpDown
        Friend WithEvents markerOneNumericLabel As System.Windows.Forms.Label
        Friend WithEvents readWriteTabPage As System.Windows.Forms.TabPage
        Friend WithEvents queryButton As System.Windows.Forms.Button
        Friend WithEvents readButton As System.Windows.Forms.Button
        Friend WithEvents readTextBox As System.Windows.Forms.TextBox
        Friend WithEvents readTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents readWriteStatusTextBox As System.Windows.Forms.TextBox
        Friend WithEvents readWriteStatusTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents writeButton As System.Windows.Forms.Button
        Friend WithEvents writeTextBox As System.Windows.Forms.TextBox
        Friend WithEvents writeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents messagesMessageList As isr.Controls.MessagesBox

    End Class

End Namespace

