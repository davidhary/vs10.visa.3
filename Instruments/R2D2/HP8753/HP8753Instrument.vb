Namespace R2D2

    ''' <summary>Implements a VISA interface to a generic GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class HP8753Instrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.GpibInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            Me.New(Nothing, resourceTitle)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="gui">Specifies the instrument panel</param>
        ''' <param name="resourceTitle">Specifies the instrument name.</param>
        Friend Sub New(ByVal gui As HP8753InstrumentPanel, ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)

            Me._gui = gui

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        ' remove the reference to the Gui
                        Me._gui = Nothing

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " PROPERTIES AND METHODS: GUI "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception> 
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If MyBase.Connect(resourceName) Then
                ' go to local to allow the operator to load the instrument state.
                ' set a long time out after connecting.
                Me.GoToLocal()
            End If
            Return MyBase.IsConnected

        End Function

        Private _gui As HP8753InstrumentPanel
        ''' <summary>Gets reference to the instrument interface.</summary>
        Public ReadOnly Property Gui() As HP8753InstrumentPanel
            Get
                Return Me._gui
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " HP8753 GPIB INSTRUMENT METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' preset the analyzer and wait
                    Dim opc As Nullable(Of Int32) = MyBase.QueryInt32("OPC?; PRES;")

                    ' allow operations to complete
                    Return opc.HasValue AndAlso opc.Value = 1

                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED RESETTING AND CLEARING",
                                              "Timeout occurred resetting and clearing. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING AND CLEARING",
                                              "VISA Exception occurred resetting and clearing. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING AND CLEARING",
                                          "Exception occurred resetting and clearing. Details: {0}.", ex)
                End Try

            End If

            Return False

        End Function

        ''' <summary>Allows the operator to go to local.
        ''' </summary>
        Public Sub GoToLocal()
            If MyBase.GpibSession IsNot Nothing Then
                Dim defaultTimeout As Int32 = MyBase.GpibSession.Timeout
                MyBase.GpibSession.Timeout = 10000
                MyBase.GpibSession.ControlRen(NationalInstruments.VisaNS.RenMode.AddressAndGtl)
                MyBase.GpibSession.Query("IDN?;")
                MyBase.GpibSession.Timeout = defaultTimeout
            End If
        End Sub

        ''' <summary>Set auto scale.
        ''' </summary>
        Public Sub AutoScale()
            MyBase.Write("AUTO")
        End Sub

        ''' <summary>Reads data at the specified marker.
        ''' </summary>
        ''' <param name="markerNumber">Specifies the marker number between 1 and 4.</param>
        Public Sub ReadMarker(ByVal markerNumber As Int32)

            Me.OnMessageAvailable(TraceEventType.Verbose, "READING MARKER", "Reading marker {0}...", markerNumber)
            ' select the active marker
            Dim marker As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK{0};", markerNumber)
            MyBase.Write(marker)

            ' read the data
            MyBase.GpibSession.Write("OUTPMARK;")
            Threading.Thread.Sleep(300)
            Me.Reading = MyBase.GpibSession.ReadString()
            'Me.Reading = MyBase.GpibSession.QueryTrimEnd("OUTPMARK;")

            ' log the message
            Me.OnMessageAvailable(TraceEventType.Information, "DONE READING MARKER", "{0},{1}", marker, Me.Reading)
            Me.OnMessageAvailable(TraceEventType.Information, "DONE READING MARKER", "Marker {0},{1}", markerNumber, Me.Reading)

        End Sub

        ''' <summary>Starts the requested sequence.
        ''' </summary>
        ''' <param name="sequenceNumber">Specifies the sequence number between 1 and 6.</param>
        Public Sub StartSequence(ByVal sequenceNumber As Int32)

            Me.OnMessageAvailable(TraceEventType.Verbose, "STARTING SEQUENCE", "Starting Sequence {0}", sequenceNumber)

            ' starts the sequence
            MyBase.Write(String.Format(Globalization.CultureInfo.CurrentCulture, "DOSEQ{0};", sequenceNumber))

            Me.OnMessageAvailable(TraceEventType.Verbose, "SEQUENCE STARTED", "Sequence {0} started", sequenceNumber)

        End Sub

        Public Sub ContinueSequence()

            Me.OnMessageAvailable(TraceEventType.Verbose, "CONTINUING SEQUENCE", "Continuing Sequence...")

            ' starts the sequence
            MyBase.Write("CONS;")

            Me.OnMessageAvailable(TraceEventType.Verbose, "SEQUENCE CONTINUED", "Sequence continued")

        End Sub

#End Region

#Region " HP8753C GPIB INSTRUMENT PROPERTIES "

        ''' <summary>Sets channel 1 as the active channel.
        ''' </summary>
        Property ChannelOne() As Boolean
            Get
                Dim message As String = "CHAN1?;"
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) = 1
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    Dim message As String = "OPC?; CHAN1;"
                    MyBase.QueryTrimEnd(message)
                    If Me.Visible Then
                        Me.Gui.channelNumeric.Value = 1
                    End If
                    Me.OnMessageAvailable(TraceEventType.Information, "DONE SELECTING CHANNEL 1", "Channel 1")
                End If
            End Set
        End Property

        ''' <summary>Sets channel 2 as the active channel.
        ''' </summary>
        Property ChannelTwo() As Boolean
            Get
                Dim message As String = "CHAN2?;"
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) = 1
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    Dim message As String = "OPC?; CHAN2;"
                    MyBase.QueryTrimEnd(message)
                    If Me.Visible Then
                        Me.Gui.channelNumeric.Value = 1
                    End If
                    Me.OnMessageAvailable(TraceEventType.Information, "DONE SELECTING CHANNEL 2", "Done selecting channel 2")
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the position of the marker one in MHz.
        ''' </summary>
        Public Property MarkerOnePosition() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK1?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK1{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.markerOneNumeric.Value = Value
                End If
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING MARKER ONE POSITION", "Marker 1, {0}MHz", Value)
            End Set
        End Property

        ''' <summary>Gets or sets the position of the marker two in MHz.
        ''' </summary>
        Public Property MarkerTwoPosition() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK2?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK2{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.markerTwoNumeric.Value = Value
                End If
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING MARKER 2 POSITION", "Marker 2, {0}MHz", Value)
            End Set
        End Property

        Private _measure As String
        ''' <summary>Gets or sets the measurement.  Valid measurements are
        '''   S11, S21, ... 
        ''' </summary>
        Public Property Measure() As String
            Get
                ' TO_DO: GET THE MEASUREMENT STATUS FROM THE INSTRUMENT
                Return Me._measure
            End Get
            Set(ByVal Value As String)
                Me._measure = Value
                MyBase.Write(Value)
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING MEASUREMENT", "Measurement, {0}", Value)
            End Set
        End Property

        ''' <summary>Gets or sets the power level.
        ''' </summary>
        Public Property PowerLevel() As Double
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "POWE?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Double)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "POWE{0}DB;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.powerNumeric.Value = Convert.ToDecimal(Value)
                End If
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING POWER LEVEL", "Power Level, {0}DB", Value)
            End Set
        End Property

        Private _scaleFormat As String
        ''' <summary>Gets or sets the scale format.  Valid formats are
        '''   LOGM, PHAS, ...
        ''' </summary>
        Public Property ScaleFormat() As String
            Get
                Return Me._scaleFormat
            End Get
            Set(ByVal Value As String)
                If Not String.IsNullOrWhiteSpace(Value) Then
                    MyBase.Write(Value.Substring(0, 4))
                    Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING SCALE FORMAT", "Scale, {0}", Value)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the start position of the trace in MHz.
        ''' </summary>
        Public Property TraceStart() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STAR?;")
                Return CInt(Double.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) / 1000000.0)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STAR{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.traceStartNumeric.Value = Value
                End If
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING TRACE START", "Trace Start, {0} MHz", Value)
            End Set
        End Property

        ''' <summary>Gets or sets the start position of the trace in MHz.
        ''' </summary>
        Public Property TraceStop() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STOP?;")
                Return CInt(Double.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) / 1000000.0)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STOP{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.traceStopNumeric.Value = Value
                End If
                Me.OnMessageAvailable(TraceEventType.Information, "DONE SETTING TRACE STOP", "Trace Stop, {0} MHz", Value)
            End Set
        End Property

        Private _firstValue As Double
        ''' <summary>Returns the measured first Value.</summary>
        Public ReadOnly Property FirstValue() As Double
            Get
                Return Me._firstValue
            End Get
        End Property

        Private _reading As String
        ''' <summary>Gets or sets the last reading.
        ''' </summary>
        Public Property Reading() As String
            Get
                Return Me._reading
            End Get
            Set(ByVal Value As String)
                If Not String.IsNullOrWhiteSpace(Value) Then
                    Me._reading = Value
                    If Value.Length > 0 Then
                        Dim values As String() = Value.Split(","c)
                        If values.Length >= 1 Then
                            If Me.Visible Then
                                Me.Gui.FirstValueLabel.Text = values(0)
                            End If
                            If Double.TryParse(values(0), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, Me._firstValue) Then
                            Else
                                Me._firstValue = 0
                            End If
                        End If
                        If values.Length >= 2 Then
                            If Me.Visible Then
                                Me.Gui.SecondValueLabel.Text = values(1)
                            End If
                            If Not Double.TryParse(values(1), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, Me._secondValue) Then
                                Me._secondValue = 0
                            End If
                        End If
                        If values.Length >= 3 Then
                            If Me.Visible Then
                                Me.Gui.ThirdValueLabel.Text = values(2)
                            End If
                            If Not Double.TryParse(values(2), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, Me._thirdValue) Then
                                Me._thirdValue = 0
                            End If
                        End If
                    End If
                    OnMessageAvailable(TraceEventType.Verbose, "READING PARSED", "Reading available: '{0}'", Value)
                End If
            End Set
        End Property

        Private _secondValue As Double
        ''' <summary>Returns the measured second Value.</summary>
        Public ReadOnly Property SecondValue() As Double
            Get
                Return Me._secondValue
            End Get
        End Property

        Private _thirdValue As Double
        ''' <summary>Returns the measured third Value.</summary>
        Public ReadOnly Property ThirdValue() As Double
            Get
                Return Me._thirdValue
            End Get
        End Property

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="isr.Visa.R2D2.ServiceEventArgs">service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As isr.Visa.R2D2.ServiceEventArgs) ' System.EventArgs)

            If e IsNot Nothing AndAlso Not e.HasError Then
            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class
End Namespace
