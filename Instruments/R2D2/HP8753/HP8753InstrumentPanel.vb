Namespace R2D2

    ''' <summary>
    ''' A user interface user control for the HP8753 Network Analyzer GPIB instrument
    ''' class. Inherits <see cref="isr.Controls.ResourcePanel">connectible resource Panel</see>.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("HP8753 Network Analyzer Instrument - Windows Forms Custom Control")>
    Public Class HP8753InstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            ' onInstantiate()

            ' instantiate the reference to the instrument
            Me.Instrument = New HP8753Instrument(Me, "HP8753")

            ' set default values.
            Me.defaultsComboBox.SelectedIndex = 0
            Me.SetDefaults()

            ' set default value for the device and enable connection:
            Me.Connector.SelectedName = "GPIB0::16::INSTR"

        End Sub

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me.Instrument = Nothing
            End If
        End Sub

#End Region

#Region " DISPLAY "

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Overrides Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean
            If e IsNot Nothing Then
                messagesMessageList.PrependMessage(e.Details)
            End If
            Return MyBase.OnMessageAvailable(e)
        End Function

#End Region

#Region " METHODS "

        ''' <summary>Gets or sets the connection status.
        ''' Updates controls.
        ''' </summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overrides Property IsConnected() As Boolean
            Get
                Return MyBase.IsConnected
            End Get
            Set(ByVal value As Boolean)
                MyBase.IsConnected = value
                Me.mainTabControl.Enabled = value
                If value Then
                    refreshDisplay()
                    If True Then
                    Else
                        Me.Instrument.OnMessageAvailable(TraceEventType.Verbose, "CONNECTED",
                                              "Manufacturer name: {1}{0}Manufacturer ID: 0x{2:X}{0}Connected to {3}",
                                              Environment.NewLine,
                                              Me._instrument.GpibSession.ResourceManufacturerName,
                                              Me._instrument.GpibSession.ResourceManufacturerID,
                                              Me._instrument.Id())
                    End If
                Else

                End If
            End Set
        End Property

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.Instruments.R2D2.HP8753Instrument
        ''' <summary>Gets a reference to the HP 8753 instrument.</summary>
        <ComponentModel.DesignerSerializationVisibility(ComponentModel.DesignerSerializationVisibility.Hidden), ComponentModel.Browsable(False)>
        Public Overloads Property Instrument() As isr.Visa.Instruments.R2D2.HP8753Instrument
            Get
                Return Me._instrument
            End Get
            Friend Set(ByVal value As isr.Visa.Instruments.R2D2.HP8753Instrument)
                Me._instrument = value
                MyBase.ConnectableResource = value
            End Set
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub readMarkerButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readMarkerButton.Click
            Try
                Me._instrument.ReadMarker(Convert.ToInt32(Me.markerNumberUpDown.Value, Globalization.CultureInfo.CurrentCulture))
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout reading marker"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub localButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles localButton.Click
            Try
                Me.Instrument.GoToLocal()
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout going to local"
                Else
                    Throw
                End If
            End Try
        End Sub

        Friend Enum TestParameterIndex
            None
            S11
            S22
            S21Magnitude
            S21Phase
        End Enum

        Friend Enum MeasurementParameterIndex
            None
            S11
            S12
            S21
            S22
        End Enum

        Friend Enum ScaleIndex
            None
            LogMagnitude
            Phase
        End Enum

        Private Sub SetDefaults()

            Me.StatusStrip.Text = "Setting defaults..."

            Dim testIndex As TestParameterIndex = CType(Me.defaultsComboBox.SelectedIndex + 1, TestParameterIndex)
            Select Case testIndex
                Case TestParameterIndex.S11
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S11 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S22
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S22 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S21Magnitude
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S21 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S21Phase
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S21 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.Phase - 1
            End Select

            Me.markerOneNumeric.Value = 1227
            Me.markerTwoNumeric.Value = 1575
            Me.traceStartNumeric.Value = 1200
            Me.traceStopNumeric.Value = 1600
            Me.powerNumeric.Value = -20
            Me.channelNumeric.Value = 1

            Me.StatusStrip.Text = "Done setting defaults."

        End Sub

        Private Sub defaultsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles defaultsButton.Click
            SetDefaults()
        End Sub

        Private Sub applyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyButton.Click

            Dim message As String = String.Empty
            Try

                Me.StatusStrip.Text = "Applying..."

                message = "Trace Start"
                Me._instrument.TraceStart = Convert.ToInt32(Me.traceStartNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Trace Stop"
                Me._instrument.TraceStop = Convert.ToInt32(Me.traceStopNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Marker 1 Position"
                Me._instrument.MarkerOnePosition = Convert.ToInt32(Me.markerOneNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Marker 2 Position"
                Me._instrument.MarkerTwoPosition = Convert.ToInt32(Me.markerTwoNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Power Level"
                Me._instrument.PowerLevel = Convert.ToDouble(Me.powerNumeric.Value, Globalization.CultureInfo.CurrentCulture)

                If Me.channelNumeric.Value = 1 Then
                    message = "channel one"
                    Me._instrument.ChannelOne = True
                ElseIf Me.channelNumeric.Value = 2 Then
                    message = "channel two"
                    Me._instrument.ChannelTwo = True
                End If

                message = "measurement"
                Me._instrument.Measure = Me.parameterComboBox.Text

                message = "scale format"
                Me._instrument.ScaleFormat = Me.scaleComboBox.Text

                Me.StatusStrip.Text = "Apply completed."

            Catch ex As NationalInstruments.VisaNS.VisaException

                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.StatusStrip.Text = "Timeout applying " & message
                Else
                    Throw New isr.Visa.BaseException("Failed setting " & message, ex)
                End If

            Catch ex As Exception

                Throw New isr.Visa.BaseException("Failed setting " & message, ex)

            End Try

        End Sub

#Region " READ / WRITE "

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.QueryTrimEnd(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.ReadString()
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED READING",
                                          "Timeout occurred when reading. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED READING",
                                          "VISA Exception occurred when reading. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED READING",
                                      "Exception occurred when reading. Details: {0}.", ex)
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Try
                Me._instrument.GpibSession.Write(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}",
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "TIMEOUT OCCURRED WRITING",
                                          "Timeout occurred when writing. VISA status={0}. Details: {1}.", Me.Instrument.LastVisaStatus, ex)
                Else
                    Me.Instrument.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED WRITING",
                                          "VISA Exception occurred when writing. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.Instrument.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED WRITING",
                                      "Exception occurred when writing. Details: {0}.", ex)
            End Try
        End Sub

#End Region

#End Region

    End Class

End Namespace
