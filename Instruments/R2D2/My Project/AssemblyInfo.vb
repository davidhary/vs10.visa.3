﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("R2D2 VISA Instruments")> 
<Assembly: AssemblyDescription("R2D2 VISA Instruments")> 
<Assembly: AssemblyProduct("Visa.Library.Instruments.R2D2.2013")>
<Assembly: CLSCompliant(True)> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM

<Assembly: Guid("bc16aae2-b5e9-45b2-8418-8cdcc2cfef1d")> 
