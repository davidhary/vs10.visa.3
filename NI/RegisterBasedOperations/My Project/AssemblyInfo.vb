﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("Register Based Operations")> 
<Assembly: AssemblyDescription("Register Based Operations")> 
<Assembly: AssemblyProduct("Visa.RegisterBasedOperations.VS2010")>
<Assembly: CLSCompliant(True)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 


