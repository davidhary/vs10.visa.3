Imports NationalInstruments
Imports NationalInstruments.VisaNS
Imports System.Threading
''' <summary>
''' Manages local VISA resources.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/24/10" by="David" revision="3.0.4010.x">
''' Created
''' </history>
Public Class LocalResourceManager

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()
        MyBase.New()
        Me._resourceFindingFailureSymptom = ""
        Me._cachedResources = New String() {}
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Private Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._cachedResources = Nothing
                    Me._resourceFindingFailureSymptom = ""

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " ENCAPSULATE FIND RESOURCES WITH DIAGNOSTICS "

    Private _cachedResources As String()
    ''' <summary>
    ''' Returns the list of resources located with the last call to
    ''' <see cref="LocateResources">Find Resources</see>.
    ''' </summary>
    Public Function CachedResources() As String()
        Return Me._cachedResources
    End Function

    Private _resourceFindingFailureSymptom As String
    ''' <summary>
    ''' Explains failure finding resources.
    ''' </summary>
    Public ReadOnly Property ResourceFindingFailureSymptom() As String
        Get
            Return Me._resourceFindingFailureSymptom
        End Get
    End Property

    Private _failedFindingResources As Boolean?
    ''' <summary>
    ''' Returns true if an general visa exception occurred finding resources.
    ''' The exception points to failure not due to parsing of the resource name
    ''' as those are trapped and addressed.
    ''' </summary>
    Public ReadOnly Property FailedFindingResources() As Boolean?
        Get
            Return Me._failedFindingResources
        End Get
    End Property

    ''' <summary>
    ''' Finds a specific resource in the cached list of resources.
    ''' </summary>
    ''' <param name="resourceName">Specifies a full resource name matching the search resource type in 
    ''' <see cref="FindResources"></see></param>
    Public Function FindResource(ByVal resourceName As String) As Boolean

        If Me._cachedResources IsNot Nothing AndAlso Me._cachedResources.Length > 0 Then
            Return Me._cachedResources.Contains(resourceName, CType(New System.Collections.CaseInsensitiveComparer(Globalization.CultureInfo.CurrentCulture), 
                                                                 Global.System.Collections.Generic.IEqualityComparer(Of String)))
#If False Then
      For Each name As String In Me._cachedResources
        If String.Equals(name, resourceName, StringComparison.OrdinalIgnoreCase) Then
          Return True
        End If
      Next
#End If
        End If
        Return False

    End Function

    ''' <summary>
    ''' Queries the VISA system to locate the resources associated with the specified interface.
    ''' Uses the <see cref="NationalInstruments.VisaNS.ResourceManager">local resource manager</see>
    ''' to find local resources.
    ''' This is the base method used by all the <see cref="LocateResources">find resources</see> methods.
    ''' </summary>
    ''' <param name="resourceSearchCommand">Specifies a resource name or resource search string.</param>
    ''' <remarks>
    ''' This came about because VISA failed returning resources getting
    ''' <see cref="NationalInstruments.VisaNS.VisaStatusCode.ErrorInvalidSetup">Invalid setup error</see>.
    ''' on a application installation.
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Public Function LocateResources(ByVal resourceSearchCommand As String) As String()

        Dim lastAction As String = ""
        Me._cachedResources = New String() {}
        Me.ResetFindResources()

        Me._failedFindingResources = False
        Dim manager As NationalInstruments.VisaNS.ResourceManager
        Try

            lastAction = "getting an instance of local VISA resource manager"

            ' get the local resource manager
            manager = VisaNS.ResourceManager.GetLocalManager()

            Try

                lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                           "getting resources using the local VISA resource manager search command '{0}'", 
                                           resourceSearchCommand)

                Me._cachedResources = manager.FindResources(resourceSearchCommand)
                If Me._cachedResources Is Nothing Then
                    Me._cachedResources = New String() {}
                    Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                                   "VISA local manager returned an empty resource list for the search command '{0}'", 
                                                                   resourceSearchCommand)
                End If

            Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaNS.VisaStatusCode.ErrorResourceNotFound

                Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                               "The VISA local manager returned a VISA Resource Not Round error for the search command '{0}'", 
                                                               resourceSearchCommand)
                Me._cachedResources = New String() {}

            Catch ex As NationalInstruments.VisaNS.VisaException

                Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                               "A VISA exception occurred {0}. Details: {1}", lastAction, ex)
                Me._failedFindingResources = True

            Catch ex As System.ArgumentException

                ' trap the Run-time exception thrown : System.ArgumentException - Insufficient location 
                ' information or the requested device or resource is not present in the system.  
                ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
                Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                               "The VISA local manager returned an argument exception when using the search command '{0}' possibly because insufficient location information or the requested device or resource is not present in the system", 
                                                               resourceSearchCommand)
                Me._cachedResources = New String() {}

            Catch ex As Exception

                Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                              "A general exception occurred {0}. Details: {1}", lastAction, ex)
                Me._failedFindingResources = True

            End Try

        Catch ex As NationalInstruments.VisaNS.VisaException

            Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                          "A VISA exception occurred {0}. Details: {1}", lastAction, ex)
            Me._failedFindingResources = True

        Catch ex As Exception

            Me._resourceFindingFailureSymptom = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                          "A general exception occurred {0}. Details: {1}", lastAction, ex)
            Me._failedFindingResources = True

        End Try

        Return Me._cachedResources

    End Function

    ''' <summary>
    ''' Resets conditions for finding resources.
    ''' </summary>
    Public Sub ResetFindResources()
        Me._failedFindingResources = New Boolean?
        Me._resourceFindingFailureSymptom = ""
        Me._cachedResources = New String() {}
    End Sub

#End Region

#Region " INTERFACES "

    Private interfaceSearchString As String = "{0}?*INTFC"

    ''' <summary>Returns the names of all local interfaces.</summary>
    ''' <returns>string array</returns>
    Public Function FindInterfaces() As String()
        Return Me.FindInterfaces(String.Empty)
    End Function

    ''' <summary>Returns the name of the local interfaces matching
    ''' the interface base name such as GPIB.</summary>
    ''' <returns>string array</returns>
    Public Function FindInterfaces(ByVal interfaceBaseName As String) As String()
        Return Me.LocateResources( 
               String.Format(Globalization.CultureInfo.CurrentCulture, interfaceSearchString, interfaceBaseName))
    End Function

    ''' <summary>
    ''' Returns true if we have interfaces.
    ''' </summary>
    Public Function HasInterfaces(ByVal interfaceBaseName As String) As Boolean
        Return Me.FindInterfaces(interfaceBaseName).Length > 0 OrElse Me.FailedFindingResources.Value
    End Function

#End Region

#Region " RESOURCES "

    Private serialResourceFormat As String = "ASRL{0}::INSTR"
    ''' <summary>Returns a Serial Port resource name string.</summary>
    ''' <param name="portNumber">A gpib instrument address</param>
    ''' <returns>A VISA resource Gpib instrument name in the form "GPIB0::PrimaryAddress::INSTR".</returns>
    Public Function BuildSerialResourceName(ByVal portNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, serialResourceFormat, portNumber)
    End Function

    Private allSearchString As String = "?*"
    Private resourceSearchString As String = "{0}?*INSTR"
    Private resourceBoardSearchString As String = "{0}{1}?*INSTR"

#If False Then
  ''' <summary>
  ''' This search string while working for VISA COM does not work for managed VISA and is removed.
  ''' Rather, use the board only search string and then look for the required address in 
  ''' the list.
  ''' </summary>
    Private resourceBoardAddressSearchString As String = "{0}{1}?*?{2}INSTR"
#Else
    Private resourceBoardAddressSearchString As String = "{0}{1}::{2}::INSTR"
#End If

    Public Const GpibResourceBaseName As String = "GPIB"
    Public Const SerialResourceBaseName As String = "ASRL"
    Public Const TcpIPResourceBaseName As String = "TCPIP"

    ''' <summary>Returns a resource base name.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    Public Shared Function BuildResourceBaseName(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String
        Select Case resourceType
            Case VisaNS.HardwareInterfaceType.Serial
                Return SerialResourceBaseName
            Case VisaNS.HardwareInterfaceType.Gpib
                Return GpibResourceBaseName
            Case VisaNS.HardwareInterfaceType.Tcpip
                Return TcpIPResourceBaseName
            Case Else
                Return resourceType.ToString
        End Select
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    Public Function FindResources(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String()
        Return Me.FindResources(BuildResourceBaseName(resourceType))
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceBaseName">Specifies the resource base name.</param>
    Public Function FindResources(ByVal resourceBaseName As String) As String()
        Return Me.LocateResources( 
             String.Format(Globalization.CultureInfo.CurrentCulture, resourceSearchString, resourceBaseName))
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    ''' <param name="boardNumber">Specifies the resource board number.</param>
    Public Function FindResources(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType, 
                                       ByVal boardNumber As Integer) As String()
        Return Me.FindResources(BuildResourceBaseName(resourceType), boardNumber)
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceBaseName">Specifies the resource base name.</param>
    ''' <param name="boardNumber">Specifies the resource board number.</param>
    Public Function FindResources(ByVal resourceBaseName As String, 
                                       ByVal boardNumber As Integer) As String()
        Return Me.LocateResources( 
             String.Format(Globalization.CultureInfo.CurrentCulture, resourceBoardSearchString, resourceBaseName, boardNumber))
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceBaseName">Specifies the resource base name.</param>
    ''' <param name="boardNumber">Specifies the resource board number.</param>
    Public Function FindResources(ByVal resourceBaseName As String, 
                                       ByVal boardNumber As Integer, ByVal address As Integer) As String()
        Return Me.LocateResources( 
             String.Format(Globalization.CultureInfo.CurrentCulture, 
                           resourceBoardAddressSearchString, resourceBaseName, boardNumber, address))
    End Function

    ''' <summary>Returns a string array of all local resources.</summary>
    ''' <history date="08/05/2008" by="David" revision="3.0.3139.x">
    ''' Failed when tested at SEMTECH giving VISA error: Unable to start operation because 
    ''' setup is invalid (due to attributes being set to an inconsistent state).  
    ''' VISA error code -1073807302 (0xBFFF003A). ErrorInvalidSetup.
    ''' </history>
    Public Function FindResources() As String()
        Return Me.LocateResources(allSearchString)
    End Function

    ''' <summary>
    ''' Returns true if we have instrument resources.
    ''' </summary>
    Public Function HasResources(ByVal resourceBaseName As String) As Boolean
        Return FindResources(resourceBaseName).Length > 0 OrElse Me.FailedFindingResources.Value
    End Function

    ''' <summary>
    ''' Returns true if we have instrument resources.
    ''' </summary>
    ''' <param name="resourceBaseName"></param>
     Public Function HasResources(ByVal resourceBaseName As String, ByVal boardNumber As Integer) As Boolean
        Return FindResources(resourceBaseName, boardNumber).Length > 0 OrElse Me.FailedFindingResources.Value
    End Function

    ''' <summary>
    ''' Returns true if we have instrument resources.
    ''' </summary>
    ''' <param name="resourceBaseName"></param>
     ''' <param name="address"></param>
    Public Function HasResources(ByVal resourceBaseName As String, ByVal boardNumber As Integer, ByVal address As Integer) As Boolean
        Return FindResources(resourceBaseName, boardNumber, address).Length > 0 OrElse Me.FailedFindingResources.Value
    End Function

#End Region

End Class

