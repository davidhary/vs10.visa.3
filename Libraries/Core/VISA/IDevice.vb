''' <summary>
''' Defines the contract implemented by a VISA instrument.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/2011" by="David" revision="3.0.4038.x">
''' Created
''' </history>
Public Interface IDevice
    Inherits ISimpleDevice

#Region " QUERY "

    ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryTrimEnd(ByVal question As String) As String

    ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
    ''' <param name="format">Specifies the format statement.
    ''' </param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String

#End Region

#Region " READ "

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

    ''' <summary>Reads and returns a string to the termination character and return the string
    ''' not including the testmination character.</summary>
    Function ReadLineTrimEnd() As String

#End Region

#Region " STATUS AND ERRORS "

    ''' <summary>
    ''' Returns the error queue and event status register (ESR) report.
    ''' </summary>
    ReadOnly Property DeviceErrors() As String

    ''' <summary>Reads the error queue from the instrument.
    ''' </summary>
    Function ReadErrorQueue() As String

    ''' <summary>
    ''' Gets the last VISA Status.
    ''' </summary>
    ReadOnly Property LastVisaStatus() As Integer ' NationalInstruments.VisaNS.VisaStatusCode

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    ReadOnly Property LastVisaStatusDetails() As String

    ''' <summary>Returns the last error from the instrument.</summary>
    Function ReadLastError() As String

#End Region

End Interface
