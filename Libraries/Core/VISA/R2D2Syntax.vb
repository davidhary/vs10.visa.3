﻿Namespace Scpi
    ''' <summary>
    ''' General syntax for SCPI instruments.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/21/2011" by="David" revision="1.2.4038.x">
    ''' Created
    ''' </history>
    Public NotInheritable Class Syntax

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Private Sub New()
            MyBase.New()
        End Sub

#End Region

#Region " COMMAND BUILDERS "

        ''' <summary>
        ''' Builds a command for setting a boolean value.
        ''' </summary>
        ''' <param name="boolFormat"></param>
        Public Shared Function BuildBooleanCommand(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
            Dim boolValue As String = String.Empty
            Select Case boolFormat
                Case BooleanDataFormat.None
                    boolValue = String.Empty
                Case BooleanDataFormat.OneZero
                    boolValue = isr.Core.IIf(Of String)(value, "1", "0")
                Case BooleanDataFormat.OnOff
                    boolValue = isr.Core.IIf(Of String)(value, "ON", "OFF")
                Case BooleanDataFormat.TrueFalse
                    boolValue = isr.Core.IIf(Of String)(value, "TRUE", "FALSE")
            End Select
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                "{0} {1}", queryCommand, boolValue)
        End Function

#End Region

#Region " SYNTAX DEFAULTS "

        ''' <summary>
        ''' Gets the Clear Status (CLS) command.
        ''' </summary>
        Public Const ClearExecutionStateCommand As String = "*CLS"

        ''' <summary>
        ''' Gets the Identify query (*IDN?) command.
        ''' </summary>
        Public Const IdentityQueryCommand As String = "*IDN?"

        ''' <summary>
        ''' Gets the operation complete (*OPC) command.
        ''' </summary>
        Public Const OperationCompletedCommand As String = "*OPC"

        ''' <summary>
        ''' Gets the operation complete query (*OPC?) command.
        ''' </summary>
        Public Const OperationCompletedQueryCommand As String = "*OPC?"

        ''' <summary>
        ''' Gets the Wait (*WAI) command.
        ''' </summary>
        Public Const WaitCommand As String = "*WAI"

        ''' <summary>
        ''' Gets the Standard Event Enable (*ESE) command.
        ''' </summary>
        Public Const StandardEventEnableCommand As String = "*ESE {0:D}"

        ''' <summary>
        ''' Gets the Standard Event Enable query (*ESE?) command.
        ''' </summary>
        Public Const StandardEventEnableQueryCommand As String = "*ESE?"

        ''' <summary>
        ''' Gets the Standard Event Enable (*ESR?) command.
        ''' </summary>
        Public Const StandardEventStatusQueryCommand As String = "*ESR?"

        ''' <summary>
        ''' Gets the Service Request Enable Enable (*SRE) command.
        ''' </summary>
        Public Const ServiceRequestEnableCommand As String = "*SRE {0:D}"

        ''' <summary>
        ''' Gets the Service Request Enable query (*SRE?) command.
        ''' </summary>
        Public Const ServiceRequestEnableQueryCommand As String = "*SRE?"

        ''' <summary>
        ''' Gets the Service Request Status query (*STB?) command.
        ''' </summary>
        Public Const ServiceRequestStatusQueryCommand As String = "*STB?"

        ''' <summary>
        ''' Gets the reset to known state (*RST) command.
        ''' </summary>
        Public Const ResetKnownStateCommand As String = "*RST"

        ''' <summary>
        ''' The default error queue query command.
        ''' </summary>
        Public Const ErrorQueueQueryCommand As String = ":STAT:QUE?"

        ''' <summary>
        ''' The default error queue clear command.
        ''' </summary>
        Public Const ClearErrorQueueCommand As String = ":STAT:QUE:CLEAR"

        ''' <summary>
        ''' The default error queue command.
        ''' </summary>
        Public Const ErrorQueryCommand As String = ":SYST:ERR?"

        ''' <summary>
        ''' The default questionable status query command
        ''' </summary>
        Public Const QuestionableStatusQueryCommand As String = ":STAT:QUES:EVEN?"

        ''' <summary>
        ''' The default measurement Status query command.
        ''' </summary>
        Public Const MeasurementStatusQueryCommand As String = ":STAT:MEAS:EVEN?"

        ''' <summary>
        ''' The default operation register status query command.
        ''' </summary>
        Public Const OperationEventStatusQueryCommand As String = ":STAT:OPER:EVEN?"

#End Region

    End Class

End Namespace

''' <summary>
''' Enumerates the format for writing boolean types.
''' </summary>
Public Enum BooleanDataFormat
    <ComponentModel.Description("Not Defined")> None
    <ComponentModel.Description("On or Off")> OnOff
    <ComponentModel.Description("One or Zero")> OneZero
    <ComponentModel.Description("True or False")> TrueFalse
End Enum

