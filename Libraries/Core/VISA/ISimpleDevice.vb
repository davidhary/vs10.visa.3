﻿''' <summary>
''' Defines the contract implemented by a simple VISA device.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/2011" by="David" revision="3.0.4038.x">
''' Created
''' </history>
Public Interface ISimpleDevice
    Inherits IDisposable, isr.Core.IConnectResource, isr.Core.IResettableDevice, isr.Core.IPresettable, isr.Core.IMessagePublisher, isr.Core.IAnonPublisher

#Region " MANAGE "

    ''' <summary>Gets or sets the condition for using actual hardware or false if it is 
    ''' non-hardware emulation mode.</summary>
    Property UsingDevices() As Boolean

#End Region

End Interface
