''' <summary>Handles General VISA exceptions.</summary>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/27/07" by="David" revision="1.0.2917.x">
''' Created
''' </history>
<Serializable()> Public Class VisaException
    Inherits BaseException

    Private Const _defMessage As String = "failed writing or reading a VISA Command."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(VisaException._defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub


    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.
    ''' </summary>
    ''' <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that
    ''' holds the serialized object data about the exception being thrown.</param>
    ''' <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that
    ''' contains contextual information about the source or destination.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, 
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="visaStatus">Specifies the VISA status code.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the report argument.</param>
    Public Sub New(ByVal visaStatus As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args) &
                   ". Visa status: " & VisaManager.BuildVisaStatusDetails(visaStatus))
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="instrument">Specifies the VISA status code.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the report argument.</param>
    Public Shared Sub ThrowException(ByVal instrument As isr.Visa.IDevice, ByVal format As String, ByVal ParamArray args() As Object)
        If instrument IsNot Nothing Then
            Throw New VisaException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0}. VISA '{1}' error Details: {2}",
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  instrument.ResourceName, instrument.LastVisaStatusDetails))
        End If
    End Sub

#End Region

End Class

''' <summary>Handles Instrument exception including device errors retrieved from the instrument.</summary>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/28/09" by="David" revision="3.0.3346.x">
''' Created
''' </history>
<Serializable()> Public Class InstrumentException
    Inherits BaseException

    Private Const _defMessage As String = "failed accessing the instrument."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(InstrumentException._defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub


    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.
    ''' </summary>
    ''' <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that
    ''' holds the serialized object data about the exception being thrown.</param>
    ''' <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that
    ''' contains contextual information about the source or destination.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, 
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="instrument">Specifies the VISA status code.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the report argument.</param>
    Public Shared Sub ThrowException(ByVal instrument As isr.Visa.IDevice, ByVal format As String, ByVal ParamArray args() As Object)
        If instrument IsNot Nothing Then
            Throw New InstrumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                        "{0}. Instrument '{1}' error details: {2}",
                                                        String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                        instrument.ResourceName, instrument.DeviceErrors))
        End If
    End Sub

#End Region

End Class

