﻿Imports NationalInstruments
Imports NationalInstruments.VisaNS
Imports System.Threading
Imports isr.Core.EnumExtensions
''' <summary>
''' General manager for VISA entities and information.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/24/10" by="David" revision="3.0.4010.x">
''' Created
''' </history>
Public NotInheritable Class VisaManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>

    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " VISA STATUS MESSAGE BUILDERS "

    ''' <summary>
    ''' Builds a VISA warning or error message.
    ''' </summary>
    ''' <param name="lastVisaAction"></param>
    ''' <param name="value">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
    ''' from the last operation.</param>
    Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String, 
                                           ByVal value As Integer) As String
        Return BuildVisaStatusDetails(lastVisaAction, CType(value, NationalInstruments.VisaNS.VisaStatusCode))
    End Function

    ''' <summary>Builds a VISA warning or error message.
    ''' </summary>
    ''' <param name="lastVisaAction"></param>
    ''' <param name="value">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
    ''' from the last operation.</param>
    Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String, 
                                           ByVal value As NationalInstruments.VisaNS.VisaStatusCode) As String
        If value = 0 Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} OK.", lastVisaAction)
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                                        "{0} {1}.", lastVisaAction, BuildVisaStatusDetails(value))
        End If
    End Function

    ''' <summary>Builds a VISA warning or error message.
    ''' </summary>
    ''' <param name="value">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
    ''' from the last operation.</param>
    Public Shared Function BuildVisaStatusDetails(ByVal value As Integer) As String

        Return BuildVisaStatusDetails(CType(value, NationalInstruments.VisaNS.VisaStatusCode))

    End Function

    ''' <summary>
    ''' Builds a VISA warning or error message.
    ''' This method does not require additional information from the instrument.
    ''' </summary>
    ''' <param name="value">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
    ''' from the last operation.</param>
    Public Shared Function BuildVisaStatusDetails(ByVal value As NationalInstruments.VisaNS.VisaStatusCode) As String

        If value = 0 Then
            Return "OK"
        Else
            Dim visaMessage As New System.Text.StringBuilder
            If value > 0 Then
                visaMessage.Append("VISA Warning")
            Else
                visaMessage.Append("VISA Error")
            End If
            Dim description As String = value.Description
            If String.Equals(value.ToString, description, StringComparison.CurrentCultureIgnoreCase) Then
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, ": {0}.", value)
            Else
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, " {0}: {1}.", value, description)
            End If
            Return visaMessage.ToString
        End If

    End Function

#End Region

#Region " APPLICATION LOG "

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one.
    ''' </summary>
    ''' <param name="logWriter"></param>
    Public Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
        My.Application.Log.TraceSource.Listeners.Remove("FileLog")
        My.Application.Log.TraceSource.Listeners.Add(logWriter)
    End Sub

    ''' <summary>
    ''' Sets the trace level. Can be called from the constructor as it is not Overridable.
    ''' </summary>
    ''' <param name="value">Specifies the <see cref="TraceEventType">trace level</see></param>
    Public Shared Sub TraceLevelSetter(ByVal value As TraceEventType)
        My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), value.ToString), SourceLevels)
    End Sub

#End Region

End Class

''' <summary>
''' Defines VISA resources.
''' </summary>
Public Enum VisaResourceClass
    <System.ComponentModel.Description("Not Defined")> None = 0
    <System.ComponentModel.Description("Interface")> [Interface]
    <System.ComponentModel.Description("Instrument")> Instrument
End Enum
