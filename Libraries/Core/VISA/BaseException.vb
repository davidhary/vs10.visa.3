''' <summary>Provides the base exception for this library.</summary>
''' <remarks>Inherit this class to define derivative exceptions.</remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/22/04" by="David" revision="1.0.1787.x">
''' Created
''' </history>
<Serializable()> Public Class BaseException
    Inherits System.Exception

    Private Const _defMessage As String = "failed VISA operation."

#Region " CUSTOM CONSTRUCTORS "

    ''' <summary>Constructor with custom data that uses a custom format string.</summary>
    ''' <param name="messageFormat">String expression that specifies the message format.</param>
    ''' <param name="data1">String expression that ,,,.</param>
    Protected Sub New(ByVal messageFormat As String, ByVal data1 As String)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, messageFormat, data1))
    End Sub

    ''' <summary>Constructor with custom data that uses a custom format string.</summary>
    ''' <param name="messageFormat">String expression that specifies the message format.</param>
    ''' <param name="data1">String expression that ,,,.</param>
    ''' <param name="data2">String expression that ,,,.</param>
    Protected Sub New(ByVal messageFormat As String, ByVal data1 As String, ByVal data2 As String)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, messageFormat, data1, data2))
    End Sub

#End Region

#Region " BASE CONSTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(BaseException._defMessage)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor specifying the Message property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor specifying the Message property to be set.</summary>
    ''' <param name="format">Specifies a format string for the message.</param>
    ''' <param name="args">Specifies the arguments for the message.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.
    ''' </summary>
    ''' <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that
    ''' holds the serialized object data about the exception being thrown.</param>
    ''' <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that
    ''' contains contextual information about the source or destination.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, 
        ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        Me._machineName = info.GetString("machineName")
        Me._createdDateTime = info.GetDateTime("createdDateTime")
        Me._appDomainName = info.GetString("appDomainName")
        Me._threadIdentity = info.GetString("threadIdentity")
        Me._windowsIdentity = info.GetString("windowsIdentity")
        Me._OSVersion = info.GetString("OSVersion")
        Me._additionalInformation = CType(info.GetValue("additionalInformation", GetType(System.Collections.Specialized.NameValueCollection)), System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> 
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("machineName", Me._machineName, GetType(String))
        info.AddValue("createdDateTime", Me._createdDateTime)
        info.AddValue("appDomainName", Me._appDomainName, GetType(String))
        info.AddValue("threadIdentity", Me._threadIdentity, GetType(String))
        info.AddValue("windowsIdentity", Me._windowsIdentity, GetType(String))
        info.AddValue("OSVersion", Me._OSVersion, GetType(String))
        info.AddValue("additionalInformation", Me._additionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

#Region " Public Properties "

    Private _additionalInformation As New System.Collections.Specialized.NameValueCollection
    ''' <summary>Collection allowing additional information to be added to the exception.</summary>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return Me._additionalInformation
        End Get
    End Property

    Private _appDomainName As String = String.Empty
    ''' <summary>AppDomain name where the exception occurred.</summary>
    Public ReadOnly Property AppDomainName() As String
        Get
            Return Me._appDomainName
        End Get
    End Property

    Private _createdDateTime As DateTime = DateTime.Now
    ''' <summary>Date and Time the exception was created.</summary>
    Public ReadOnly Property CreatedDateTime() As DateTime
        Get
            Return Me._createdDateTime
        End Get
    End Property

    Private _machineName As String = String.Empty
    ''' <summary>Machine name where the exception occurred.</summary>
    Public ReadOnly Property MachineName() As String
        Get
            Return Me._machineName
        End Get
    End Property

    ''' <summary>OS Version where the exception occurred.</summary>
    Private _OSVersion As String = Environment.OSVersion.ToString()
    Public ReadOnly Property OSVersion() As String
        Get
            Return Me._OSVersion
        End Get
    End Property

    Private _threadIdentity As String = String.Empty
    ''' <summary>Identity of the executing thread on which the exception was created.</summary>
    Public ReadOnly Property ThreadIdentityName() As String
        Get
            Return Me._threadIdentity
        End Get
    End Property

    Private _windowsIdentity As String = String.Empty
    ''' <summary>Windows identity under which the code was running.</summary>
    Public ReadOnly Property WindowsIdentityName() As String
        Get
            Return Me._windowsIdentity
        End Get
    End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

    Private Const unknown As String = "N/A"
    ''' <summary>Function that gathers environment information safely.</summary>
    ''' <history date="01/10/03" by="David" revision="1.0.1105.x">
    ''' 	Create
    ''' </history>
    Private Sub obtainEnvironmentInformation()

        Me._machineName = Environment.MachineName
        If Me._machineName.Length = 0 Then
            Me._machineName = "N/A"
        End If

        Me._threadIdentity = System.Threading.Thread.CurrentPrincipal.Identity.Name
        If Me._threadIdentity.Length = 0 Then
            Me._threadIdentity = "N/A"
        End If

        Me._windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent().Name
        If Me._windowsIdentity.Length = 0 Then
            Me._windowsIdentity = "N/A"
        End If

        Me._appDomainName = AppDomain.CurrentDomain.FriendlyName
        If Me._appDomainName.Length = 0 Then
            Me._appDomainName = "N/A"
        End If
        Me._OSVersion = Environment.OSVersion.ToString
        If Me._OSVersion.Length = 0 Then
            Me._OSVersion = "N/A"
        End If

    End Sub

#End Region

End Class

