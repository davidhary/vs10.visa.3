Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Imports NationalInstruments.VisaNS

Namespace Gpib

    ''' <summary>Provides a Core. for non-SCPI GPIB instruments.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class Device
        Inherits SimpleDevice
        Implements IDisposable, IDevice

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Protected Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If ServiceRequestEvent IsNot Nothing Then
                            For Each d As [Delegate] In ServiceRequestEvent.GetInvocationList
                                RemoveHandler ServiceRequest, CType(d, Global.System.EventHandler(Of ServiceEventArgs))
                            Next
                        End If

                        If Me.Session IsNot Nothing Then
                            If Me.IsConnected Then
                                ' remove the service request handler ignoring exceptions.
                                RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                        End If

                        If Me._gpibInterface IsNot Nothing Then
                            Me._gpibInterface.Dispose()
                            Me._gpibInterface = Nothing
                        End If

                    End If

                    ' remove the reference to the synchronizer
                    ' Me._synchronizer = Nothing

                    ' Free shared unmanaged resources

                Finally

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Device).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean
            If Not MyBase.IsConnected Then
                If MyBase.Connect(resourceName) Then
                    Me.ServiceRequestEventEnable = 0 '  ServiceRequests.None
                    ' register the handler before enabling the event
                    AddHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                    Me.Session.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)
                End If
            End If
            Return Me.IsConnected
        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        Public Overrides Function Disconnect() As Boolean

            If Me.IsConnected Then
                If Me.Session IsNot Nothing Then
                    ' remove the service request handler ignoring exceptions.
                    RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                End If
                MyBase.Disconnect()
            End If
            Return Not Me.IsConnected

        End Function

#End Region

#Region " I RESETTABLE "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            If MyBase.ClearExecutionState Then
                Me._lastServiceEventArgs = New ServiceEventArgs(Me.Session)
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.IsLastVisaOperationFailed
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.IsLastVisaOperationWarning
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IDevice.LastVisaStatus '  NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me.Session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me.Session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IDevice.LastVisaStatusDetails
            Get
                Return Me.Session.BuildLastVisaStatusDetails()
            End Get
        End Property

#End Region

#Region " INSTRUMENT METHODS "

        ''' <summary>
        ''' Returns the error queue and event status register (ESR) report.
        ''' </summary>
        Public ReadOnly Property DeviceErrors() As String Implements IDevice.DeviceErrors
            Get
                Return "NOT IMPLEMENTED YET"
            End Get
        End Property

#End Region

#Region " INSTRUMENT PROPERTIES "

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public ReadOnly Property HadError() As Boolean
            Get
                Return Me._lastServiceEventArgs.HasError OrElse Me.Session.HasError(isr.Visa.IEEE4882.ServiceRequests.ErrorAvailable)
#If DEBUG Then
                ' In debug mode, make sure to wait for the end of service
                ' request processing to get the error immediately after
                ' the instrument reports it.  In this mode, we expect to have
                ' syntax error that the instrument is unlikely to be happy about.
                ' once in release mode, the incidences are lower and we can wait for
                ' the error rather than slowing operations due to this delay
                Return Me.HadError(10)
#Else
        Windows.Forms.Application.DoEvents()
        Return Me._lastServiceEventArgs.HasError
#End If
            End Get
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        ''' <param name="timeout">Time to wait before returning the error.</param>
        Private ReadOnly Property HadError(ByVal timeout As Int32) As Boolean
            Get
                ' add a couple of milliseconds wait
                Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, timeout))
                Do
                    Threading.Thread.Sleep(Math.Min(10, timeout))
                Loop While Me._lastServiceEventArgs.ServicingRequest And (Date.Now <= endTime)
                Return Me._lastServiceEventArgs.HasError
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If id Is Nothing Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            Me._manufacturerName = idItems(0)

            ' model: MODEL 2420
            Me._model = idItems(1)

            ' Serial Number: 0669977
            Me._serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            Me._firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(Me._firmwareRevision)

        End Sub

        ''' <summary>Queries the Gpib instrument and returns a Boolean value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            If Not Me.UsingDevices Then
                Return True
            End If
            Return Me.Session.QueryBoolean(question)
        End Function

        ''' <summary>Queries the Gpib instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.QueryDouble(question)
        End Function

        ''' <summary>Queries the Gpib instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.QueryInt32(question)
        End Function

        ''' <summary>Queries the Gpib instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.QueryInfInt32(question)
        End Function

        ''' <summary>Reads the Gpib instrument and returns a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            If Not Me.UsingDevices Then
                Return True
            End If
            Return Me.Session.ReadBoolean()
        End Function

        ''' <summary>Reads the Gpib instrument and returns a double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.ReadDouble()
        End Function

        ''' <summary>Reads the Gpib instrument and returns an Int32 value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.ReadInt32()
        End Function

        ''' <summary>Reads the Gpib instrument and returns an Int32 value.</summary>
        Public Function ReadInfInt32() As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me.Session.ReadInfInt32()
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & isr.Core.IIf(Of String)(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & isr.Core.IIf(Of String)(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As ServiceEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="ServiceEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As ServiceEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As ServiceEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return Me._manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return Me._model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return Me._serialNumber
            End Get
        End Property

#End Region

#Region " GPIB VISA INTERFACE METHODS "

        Private _gpibInterface As VisaNS.GpibInterface
        ''' <summary>Gets or sets reference to the Gpib interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As VisaNS.GpibInterface
            Get
                If Me._gpibInterface Is Nothing AndAlso Me.UsingDevices Then
                    Me._gpibInterface = Gpib.GpibInterface.OpenInterfaceSession(Me.Session.HardwareInterfaceName & "::INTFC", Me.ConnectTimeout)
                End If
                Return Me._gpibInterface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Sub InterfaceClear()

            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SendInterfaceClear()
            End If

        End Sub

#End Region

#Region " COMMON COMMANDS "

        ''' <summary>
        ''' Gets or sets the instrument identity. Reads the instrument identity the first time it is 
        ''' called. Setting the identity allows emulation.
        ''' Cleared when connected.
        ''' </summary>
        Public Overrides Property Identity() As String
            Get
                Me.ParseInstrumentId(MyBase.Identity)
                Return MyBase.Identity
            End Get
            Set(ByVal value As String)
                MyBase.Identity = value
                Me.ParseInstrumentId(MyBase.Identity)
            End Set
        End Property

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function IsOpc() As Boolean
            If Me.Session IsNot Nothing Then
                Me._lastServiceEventArgs.OperationCompleted = Me.Session.ReadOperationComplete.GetValueOrDefault(0) = 1
                Return Me._lastServiceEventArgs.OperationCompleted
            Else
                Return True
            End If
        End Function

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public ReadOnly Property ServiceRequestEventStatus() As isr.Visa.IEEE4882.ServiceRequests
            Get
                If Me.Session IsNot Nothing Then
                    Return CType(Me.Session.ReadServiceRequestEventStatus(), IEEE4882.ServiceRequests)
                Else
                    Return IEEE4882.ServiceRequests.None
                End If
            End Get
        End Property

        ''' <summary>Programs or reads back the service request event request.</summary>
        ''' <returns>The <see cref="ServiceRequest">mask</see>
        '''    to use for enabling the events.</returns>
        ''' <remarks>Make sure to issue <see cref="ClearExecutionState">status clear</see> before
        '''   setting the service request.</remarks>
        Public Property ServiceRequestEventEnable() As isr.Visa.IEEE4882.ServiceRequests
            Get
                If Me.Session IsNot Nothing Then
                    Return CType(Me.Session.ReadServiceRequestEventEnable(), IEEE4882.ServiceRequests)
                End If
            End Get
            Set(ByVal value As isr.Visa.IEEE4882.ServiceRequests)
                If Me.Session IsNot Nothing Then
                    Me.Session.WriteServiceRequestEventEnable(value)
                End If
            End Set
        End Property

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public ReadOnly Property StandardEventStatus() As isr.Visa.IEEE4882.StandardEvents
            Get
                If Me.Session IsNot Nothing Then
                    Return CType(Me.Session.ReadStandardEventStatus(), IEEE4882.StandardEvents)
                Else
                    Return IEEE4882.StandardEvents.None
                End If
            End Get
        End Property

        ''' <summary>Programs or reads back the standard event request.</summary>
        ''' <returns>The <see cref="isr.Visa.Ieee4882.StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Property StandardEventEnable() As isr.Visa.IEEE4882.StandardEvents
            Get
                If Me.Session IsNot Nothing Then
                    Return CType(Me.Session.ReadStandardEventEnable(), IEEE4882.StandardEvents)
                End If
            End Get
            Set(ByVal value As isr.Visa.IEEE4882.StandardEvents)
                If Me.Session IsNot Nothing Then
                    Me.Session.WriteServiceRequestEventEnable(value)
                End If
            End Set
        End Property

#End Region

#Region " GPIB VISA SESSION METHODS AND PROPERTIES "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until ((Me.LastServiceEventArgs.ServiceRequestStatus And IEEE4882.ServiceRequests.MessageAvailable) =
                          IEEE4882.ServiceRequests.MessageAvailable) OrElse timedOut
                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me.Session.Reader.ReadLine()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Verbose, "FLUSHED", "Discarded unread data '{0}'",
                                                  isr.Core.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(flushed))
                        End If

                    Else

                        Me.Session.DiscardUnreadData()

                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.DiscardUnreadData

            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.FlushRead

            Try

                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION DURING FLUSH READ",
                                      "Exception occurred flushing read buffer. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)

            End Try

        End Function

        ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IDevice.QueryTrimEnd
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return Me.Session.QueryTrimEnd(question)
            End If
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IDevice.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return Me.Session.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Function

        ''' <summary>Reads a string from Gpib instrument and returns the string save the termination character.</summary>
        Public Function ReadLineTrimEnd() As String Implements IDevice.ReadLineTrimEnd
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return Me.Session.ReadLineTrimEnd()
            End If
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If Me.Session IsNot Nothing Then
                Me.Session.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If Me.Session IsNot Nothing Then
                Me.Session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Raised upon receiving a service request.  This event does not expose the
        '''   internal service request arguments directly.</summary>
        Public Event ServiceRequest As EventHandler(Of ServiceEventArgs) ' System.EventArgs)

        ''' <summary>Raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnServiceRequest(ByVal e As ServiceEventArgs) ' System.EventArgs)

            If e Is Nothing Then Return

            If e.HasError Then
                Me.OnMessageAvailable(TraceEventType.Warning, "SERVICE REPORT REPORT",
                                      "Instrument reported an error:{0}{1}.", Environment.NewLine, e.LastError)
            Else
                Dim elapsedMilliseconds As Double = e.OperationElapsedTime.TotalMilliseconds()
                Me.OnMessageAvailable(TraceEventType.Verbose, "SERVICE REPORT REPORT",
                                      "Operation completed in {0}ms.", elapsedMilliseconds)

            End If

            ServiceRequestEvent.SafeInvoke(Me, e)
            e.ServicingRequest = False

        End Sub

        ''' <summary>Handles service requests from the instrument.</summary>
        Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

            If Not Me._lastServiceEventArgs.ServicingRequest Then
                ' do not initialize the last service event arguments because this is done with Clear Status.
                Me._lastServiceEventArgs.ServicingRequest = True
                Me._lastServiceEventArgs.ProcessServiceRequest()
                Me._lastServiceEventArgs.ReadRegisters()
                If Me._lastServiceEventArgs.HasError Then
                    ' disable recurring service requests
                    'Me.ServiceRequestEventEnable = ServiceRequests.None
                Else
                    ' check if we have consecutive requests.
                    If Me._lastServiceEventArgs.RequestCount > 10 Then
                        ' disable recurring service requests
                        Me.ServiceRequestEventEnable = 0 '  ServiceRequests.None
                        Me.OnMessageAvailable(TraceEventType.Verbose, "VISA SERVICE REPORT REPORT",
                                              "Instrument had over 10 consecutive service requests.")
                    End If
                End If
                OnServiceRequest(Me._lastServiceEventArgs)
            End If

        End Sub

#End Region

#Region " ERROR MANAGEMENT "

        Private _errorQueueCache As String
        Public Property ErrorQueueCache() As String
            Get
                Return Me._errorQueueCache
            End Get
            Set(ByVal value As String)
                Me._errorQueueCache = value
            End Set
        End Property

        Public Function ReadErrorQueue() As String Implements IDevice.ReadErrorQueue
            Return Me._errorQueueCache
        End Function

        Private _lastErrorCache As String
        Public Property LastErrorCache() As String
            Get
                Return Me._lastErrorCache
            End Get
            Set(ByVal value As String)
                Me._lastErrorCache = value
            End Set
        End Property

        Public Function ReadLastError() As String Implements IDevice.ReadLastError
            Return Me._lastErrorCache
        End Function

#End Region

#Region " RESOURCE MANAGEMENT "

        ''' <summary>
        ''' Returns true if having a valid primary address.
        ''' </summary>
        ''' <param name="address">The device primary address</param>
        Public Shared Function IsValidPrimaryAddress(ByVal address As Integer) As Boolean
            Return (address >= 1) AndAlso (address <= 31)
        End Function

        ''' <summary>Gets or sets the primary address of the instrument on the Gpib bus.</summary>
        Public ReadOnly Property PrimaryAddress() As Int32
            Get
                If Me.Session Is Nothing Then
                    Return 0
                Else
                    Return Me.Session.PrimaryAddress
                End If
            End Get
        End Property

        ''' <summary>Gets the board number.</summary>
        Public ReadOnly Property BoardNumber() As Integer
            Get
                If Me.Session Is Nothing Then
                    Return 0
                Else
                    Return Me.Session.BoardNumber
                End If
            End Get
        End Property

#End Region

    End Class

End Namespace
