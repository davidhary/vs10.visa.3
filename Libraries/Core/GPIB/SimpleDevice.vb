Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Imports NationalInstruments.VisaNS

Namespace Gpib

    ''' <summary>
    ''' Provides a Core. for non-SCPI GPIB instruments.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class SimpleDevice
        Inherits DeviceBase
        Implements IDisposable, ISimpleDevice

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Protected Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle, HardwareInterfaceType.Gpib)

            ' default to using devices
            Me._UsingDevices = True

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If Me.Session IsNot Nothing Then
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If

                    End If

                    ' remove the reference to the synchronizer
                    ' Free shared unmanaged resources

                Finally

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(SimpleDevice).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Me.ResourceName = resourceName

            Try
                Me.OnMessageAvailable(TraceEventType.Verbose, "PREPARING TO CONNECT", "Preparing to establishing connection to {0}", resourceName)
                Dim connectingEvents As New System.ComponentModel.CancelEventArgs()
                Me.OnConnecting(connectingEvents)
                If connectingEvents.Cancel Then
                    Return Me.IsConnected
                End If
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION PREPARING TO CONNECT", "Exception occurred preparing to connect. Details: {0}", ex)
                Return Me.IsConnected
            End Try

            If Not Me.UsingDevices Then
                MyBase.Connect(resourceName)
                Return Me.IsConnected
            End If

            Try

                ' open a gpib session to this instrument
                Me._session = Gpib.GpibSession.OpenSession(resourceName, Me.ConnectTimeout)

                If Me.Session Is Nothing Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED INSTANTIATING A SESSION", "Failed instantiating a VISA session. Disconnecting.{0}{1}",
                                          Environment.NewLine, isr.Core.StackTraceParser.UserCallStack(4, 4))
                    Me.Disconnect()
                Else
                    Me.Session.StoreTimeout(Me.ConnectTimeout)
                    ' clear status and disable service request on all operations.
                    Me.ClearExecutionState()
                    ' issue a selective device clear and total clear of the instrument.
                    Me.ResetAndClear()
                    MyBase.Connect(resourceName)
                End If

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CONNECTING", "Exception occurred connecting. Details: {0}", ex)

                Try
                    Me.Disconnect()
                Finally
                End Try

            Finally
                If Me.Session IsNot Nothing Then
                    Me.Session.RestoreTimeout()
                End If
            End Try

            Return Me.IsConnected

        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="address">Specifies the primary address of the instrument.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Overrides Function Connect(ByVal address As Integer) As Boolean
            Return Me.Connect(Gpib.GpibSession.BuildResourceName(address))
        End Function

        ''' <summary>Opens a Gpib VISA interface for the specified resource.</summary>
        ''' <param name="boardNumber">Specifies the GPIB resource board name.</param>
        ''' <param name="address">Specifies the GPIB resource address.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overridable Overloads Function Connect(ByVal boardNumber As Integer, ByVal address As Integer) As Boolean
            Return Me.Connect(Gpib.GpibSession.BuildResourceName(boardNumber, address))
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
        Public Overloads Overrides Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
            Me.ResourceTitle = resourceTitle
            Return Me.Connect(resourceName)
        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function Disconnect() As Boolean

            If Me.IsConnected Then
                Try
                    Dim e As New System.ComponentModel.CancelEventArgs
                    Me.OnDisconnecting(e)
                    If Not e.Cancel Then
                        If Me.Session IsNot Nothing Then
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If
                        If Not MyBase.Disconnect Then
                            MyBase.Connect(Me.ResourceName)
                        End If
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", "Exception occurred disconnecting. Details: {0}", ex)
                End Try
            End If
            Return Not Me.IsConnected

        End Function

#End Region

#Region " I RESETTABLE "

        Public Overrides Function ClearResource() As Boolean
            Return ResetAndClear()
        End Function

        ''' <summary>Selectively clears the instrument.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearActiveState() As Boolean

            If Me.Session IsNot Nothing Then
                Try
                    Me.Session.Clear()
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING ACTIVE STATE",
                                              "Timeout occurred when clearing active state. VISA status={0}. Details: {1}.", Me.Session.LastStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                              "VISA Exception occurred when clearing active state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                          "Exception occurred when clearing active state. Details: {0}.", ex)
                End Try
                Return Me.Session.LastStatus = VisaStatusCode.Success
            Else
                Return False
            End If

        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            If Me.Session Is Nothing Then
                Return False
            End If
            Try
                ' Clear the device status.  This will set all the
                ' standard subsystem properties.
                Me.Session.ClearExecutionState()

                ' Clear the event arguments
                Me.OnMessageAvailable(TraceEventType.Verbose, "STATUS CLEARED", "Status Cleared.")
                Return True
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING EXECUTION STATE",
                                          "Timeout occurred when clearing execution state. VISA status={0}. Details: {1}.", Me.Session.LastStatus, ex)
                Else
                    Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                          "VISA Exception occurred when clearing execution state. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                      "Exception occurred when clearing execution state. Details: {0}.", ex)
            End Try
            Return False

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            If Not Me.UsingDevices Then
                Return True
            End If

            Try

                ' clear the device
                Me.ClearActiveState()

                ' reset the device and set default properties
                Me.ResetKnownState()

                ' Clear the device Status and set more defaults
                Me.ClearExecutionState()

                ' preset the event registers to their power on conditions
                ' including the event transition registers.
                ' Scpi.StatusSubsystem.Preset(Me._gpibSession)
                Me.OnMessageAvailable(TraceEventType.Verbose, "RESET AND CLEARED", "Reset and cleared.")

                Return True

            Catch ex As NationalInstruments.VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION", "VISA Exception occurred reset and clear. Details: {0}", ex)

                Return False

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session.  Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If Not Me.UsingDevices Then
                Return True
            End If

            Dim newTimeout As Int32 = Me.Session.Timeout
            Me.Session.Timeout = Math.Max(Me.Session.Timeout, timeout)
            Dim outcome As Boolean = ResetAndClear()
            Me.Session.Timeout = newTimeout
            Return outcome

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ResetKnownState() As Boolean

            If Me.Session IsNot Nothing Then
                Try
                    Me.Session.ResetKnownState()
                    Return True
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT RESETTING KNOWN STATE",
                                              "Timeout occurred when resetting known state. VISA status={0}. Details: {1}.", Me.Session.LastStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                              "VISA Exception occurred when resetting known state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                          "Exception occurred when resetting known state. Details: {0}.", ex)
                End Try
                Return False
            Else
                Return False
            End If

        End Function

#End Region

#Region " I SIMPLE DEVICE "

        Private _session As Gpib.GpibSession
        ''' <summary>Gets or sets reference to the Gpib session.</summary>
        Public ReadOnly Property Session() As Gpib.GpibSession
            Get
                Return Me._session
            End Get
        End Property

        ''' <summary>
        ''' True when using actual devices or False for running the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean Implements ISimpleDevice.UsingDevices

#End Region

#Region " I RESOURCE INFO "

        ''' <summary>
        ''' Gets or sets the instrument identity. Reads the instrument identity the first time it is 
        ''' called. Setting the identity allows emulation.
        ''' Cleared when connected.
        ''' </summary>
        Public Overrides Property Identity() As String
            Get
                If String.IsNullOrWhiteSpace(MyBase.Identity) Then
                    If Me.Session IsNot Nothing Then
                        MyBase.Identity = Me.Session.ReadIdentity()
                    ElseIf Me._session Is Nothing Then
                        MyBase.Identity = ""
                    Else
                        MyBase.Identity = "Salute to SCPI Corp., Model 1, 123, A"
                    End If
                    'Me.ParseInstrumentId(MyBase.Identity)
                End If
                Return MyBase.Identity
            End Get
            Set(ByVal value As String)
                MyBase.Identity = value
                'Me.ParseInstrumentId(MyBase.Identity)
            End Set
        End Property

#End Region

    End Class

End Namespace
