﻿Imports NationalInstruments
Imports isr.Core.EventHandlerExtensions

Namespace Gpib

    ''' <summary>
    ''' Implements a Gpib interface.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/21/2011" by="David" revision="1.2.4038.x">
    ''' Created
    ''' </history>
    Public Class GpibInterfaceDevice
        Implements IDisposable, isr.Core.IConnectableInterface, isr.Core.IAnonPublisher


#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New()

            Me.ResourceTitle = resourceTitle

            ' default 30 seconds reset and clear timeout.
            Me._connectTimeout = 30000

            ' default to using devices
            Me._UsingDevices = True

            Me._resourceManager = New isr.Visa.LocalResourceManager

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        Me._identity = ""
                        Me._resourceName = ""
                        Me._ResourceTitle = ""

                        If MessageAvailableEvent IsNot Nothing Then
                            For Each d As [Delegate] In MessageAvailableEvent.GetInvocationList
                                RemoveHandler MessageAvailable, CType(d, Global.System.EventHandler(Of isr.Core.MessageEventArgs))
                            Next
                        End If

                        If PropertyChangedEvent IsNot Nothing Then
                            For Each d As [Delegate] In PropertyChangedEvent.GetInvocationList
                                RemoveHandler PropertyChanged, CType(d, System.ComponentModel.PropertyChangedEventHandler)
                            Next
                        End If

                        If Me._resourceManager IsNot Nothing Then
                            Me._resourceManager.Dispose()
                            Me._resourceManager = Nothing
                        End If

                        If Me._GpibInterface IsNot Nothing Then
                            Me._GpibInterface.Dispose()
                            Me._GpibInterface = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " I ANON PUBLISHER "

        ''' <summary>
        ''' Occurs when a property value changes.
        ''' </summary>
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Raised to notify the bindable object of a change.
        ''' </summary>
        Public Sub OnPropertyChanged(ByVal name As String) Implements Core.IAnonPublisher.OnPropertyChanged
            If Me.Publishable Then
                PropertyChangedEvent.Invoke(Me, New ComponentModel.PropertyChangedEventArgs(name))
            End If
        End Sub

        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Sub Publish() Implements Core.IAnonPublisher.Publish
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(GpibInterfaceDevice).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub

        Private _Publishable As Boolean
        ''' <summary>
        ''' Gets or sets the publishable status of the publisher.
        ''' When created, the publisher is not publishable.
        ''' This allows changing properties without affecting the observers.
        ''' </summary>
        ''' 
        Public Property Publishable() As Boolean Implements Core.IAnonPublisher.Publishable
            Get
                Return Me._Publishable
            End Get
            Set(ByVal value As Boolean)
                Me._Publishable = value
            End Set
        End Property

#End Region

#Region " I CONNECT "

        Private _connectTimeout As Integer
        ''' <summary>
        ''' Gets or sets the time out for doing a reset and clear on the instrument.
        ''' </summary>
        Public Property ConnectTimeout() As Integer Implements isr.Core.IConnect.Timeout
            Get
                Return Me._connectTimeout
            End Get
            Set(ByVal Value As Integer)
                Me._connectTimeout = Value
            End Set
        End Property

        Public Overridable Overloads Function Connect() As Boolean Implements Core.IConnect.Connect
            Return Me.Connect(Me.ResourceName)
        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Overloads Function Connect(ByVal resourceName As String) As Boolean Implements Core.IConnectableInterface.Connect

            Dim synopsis As String = "Connecting"
            If Me.IsConnected Then
                Return Me.IsConnected
            End If

            Me._identity = ""
            Me.ResourceName = resourceName

            If Not Me.UsingDevices Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "connected")
                Me._isConnected = True

            End If

            Try

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Opening VISA session to {0}", resourceName)

                ' open a gpib session to this instrument
                Me._GpibInterface = GpibInterface.OpenGpibInterface(resourceName, Me.ConnectTimeout)


                Me.OnMessageAvailable(TraceEventType.Information, synopsis, "VISA session opened to {0}", resourceName)

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred while connecting. Disconnecting. Details: {1}.", ex)

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            Finally

            End Try

            If Me.GpibInterface Is Nothing Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Failed instantiating a VISA interface session. Disconnecting.{1}{2}",
                                             Environment.NewLine, isr.Core.StackTraceParser.UserCallStack(4, 4))
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            End If

            Try

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Establishing connection to {0}", resourceName)
                Me._isConnected = True

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred connecting. Disconnecting. Details: {1}", ex)
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            End Try

            If Me.IsConnected Then
                Me.OnConnectionChanged(System.EventArgs.Empty)
            End If
            Return Me.IsConnected

        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="address">Specifies the primary address of the instrument.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overridable Overloads Function Connect(ByVal address As Integer) As Boolean Implements Core.IConnectableInterface.Connect

            Return Me.Connect(GpibSession.BuildResourceName(address))

        End Function

        ''' <summary>Opens a Gpib VISA interface for the specified resource.</summary>
        ''' <param name="boardNumber">Specifies the GPIB resource board name.</param>
        ''' <param name="address">Specifies the GPIB resource address.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overridable Overloads Function Connect(ByVal boardNumber As Integer, ByVal address As Integer) As Boolean

            Return Me.Connect(GpibSession.BuildResourceName(boardNumber, address))

        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
        Public Overridable Overloads Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
            Me.ResourceTitle = resourceTitle
            Return Me.Connect(resourceName)
        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        Public Overridable Function Disconnect() As Boolean Implements Core.IConnect.Disconnect

            Dim synopsis As String = "Disconnecting"
            If Not Me.IsConnected Then
                Return Not Me.IsConnected
            End If

            Try

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "disconnecting {0}", Me.ResourceName)
                Dim e As New System.ComponentModel.CancelEventArgs
                If Not e.Cancel Then

                    If Me.GpibInterface IsNot Nothing Then
                        Me._GpibInterface.Dispose()
                        Me._GpibInterface = Nothing
                    End If

                    Me._isConnected = False

                End If

                If Not Me.IsConnected Then
                    Me.OnConnectionChanged(System.EventArgs.Empty)
                End If
                Return Not Me.IsConnected

            Catch

                Throw

            Finally

            End Try

        End Function

        Private _isConnected As Boolean
        ''' <summary>Gets or sets the connect status flag.</summary>
        ''' <value><c>True</c> if connected.</value>
        Public ReadOnly Property IsConnected() As Boolean Implements Core.IConnectableInterface.IsConnected
            Get
                Return Me._isConnected
            End Get
        End Property

        ''' <summary>Raised to update the connection state.</summary>
        ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
        ''' </param>
        Public Event ConnectionChanged As EventHandler(Of EventArgs) Implements isr.Core.IConnect.ConnectionChanged

        ''' <summary>Raises an event to alert on change of connection.
        ''' </summary>
        Protected Overridable Sub OnConnectionChanged(ByVal e As System.EventArgs) Implements isr.Core.IConnect.OnConnectionChanged
            ConnectionChangedEvent.SafeInvoke(Me)
        End Sub

        Private _resourceName As String = String.Empty
        ''' <summary>Gets or sets the resource name.</summary>
        ''' <value><c>ResourceName</c> is a String property.</value>
        Public Overridable Property ResourceName() As String Implements Core.IConnectableResource.ResourceName
            Get
                Return Me._resourceName
            End Get
            Set(ByVal value As String)
                Me._resourceName = value
                If String.IsNullOrWhiteSpace(Me._ResourceTitle) Then
                    Me._ResourceTitle = value
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the resource human readable name.</summary>
        ''' <value><c>ResourceName</c> is a String property.</value>
        Public Property ResourceTitle() As String Implements isr.Core.IConnectableResource.ResourceTitle

        ''' <summary>
        ''' True when using actual devices or False for running the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean

#End Region

#Region " I MESSAGE PUBLISHER "

        ''' <summary>Occurs when the element or instrument has a message
        ''' </summary>
        Public Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs) Implements isr.Core.IMessagePublisher.MessageAvailable

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="e">Specifies the event arguments.</param>
        Public Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
            If e IsNot Nothing Then
                MessageAvailableEvent.SafeBeginInvoke(Me, e)
                Return e.Details
            Else
                Return ""
            End If
        End Function

        ''' <summary>
        ''' Raises a message.
        ''' </summary>
        ''' <param name="traceLevel">Specifies the event arguments message
        ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
        ''' <param name="synopsis">Specifies the message short synopsis.</param>
        ''' <param name="format">Specifies the format for building the message detains</param>
        ''' <param name="args">Specifies the format arguments.</param>
        Public Overridable Function OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType,
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
            Return Me.OnMessageAvailable(traceLevel, traceLevel, synopsis, format, args)
        End Function

        ''' <summary>
        ''' Raises the Message Available event.
        ''' </summary>
        ''' <param name="broadcastLevel">Specifies the event arguments message
        ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
        ''' <param name="traceLevel">Specifies the event arguments message
        ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
        ''' <param name="synopsis">Specifies the message Synopsis.</param>
        ''' <param name="format">Specifies the message details.</param>
        ''' <param name="args">Arguments to use in the format statement.</param>
        Public Overridable Function OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType,
                                      ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
            Return OnMessageAvailable(New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, Me.ResourceTitle & ":: " & format, args))
        End Function

#End Region

#Region " INTERFACE "

        Private _GpibInterface As isr.Visa.Gpib.GpibInterface
        ''' <summary>Gets or sets reference to the Gpib interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As isr.Visa.Gpib.GpibInterface
            Get
                Return Me._GpibInterface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Overridable Function ClearInterface() As Boolean Implements Core.IConnectableInterface.ClearResource
            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SendInterfaceClear()
            End If
            Return True
        End Function

        ''' <summary>Returns a string array of all local GPIB resources.</summary>
        Public Shared Function LocalResourceNames() As String()
            Using manager As New isr.Visa.LocalResourceManager
                Return manager.FindInterfaces(GpibInterface.InterfaceName)
            End Using
        End Function

        Public Overloads Function ClearResource(ByVal resourceName As String) As Boolean Implements Core.IConnectableInterface.ClearSelectiveResource
            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SelectiveDeviceClear(resourceName)
            End If
        End Function

        Public Overloads Function ClearResources() As Boolean Implements Core.IConnectableInterface.ClearResources
            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.DevicesClear()
            End If
        End Function

#End Region

#Region " RESOURCE MANAGEMENT - INTERFACE "

        Private _resourceManager As isr.Visa.LocalResourceManager
        ''' <summary>
        ''' Returns reference to the <see cref="LocalResourceManager">local resource manager.</see>
        ''' </summary>
        Private ReadOnly Property ResourceManager() As LocalResourceManager
            Get
                Return Me._resourceManager
            End Get
        End Property

        ''' <summary>
        ''' Returns true if we have interfaces or if an error occurred allowing to enter specific interface name.
        ''' </summary>
        Public Function HasInterfaces() As Boolean
            Return (Me.ResourceManager.FindInterfaces(GpibInterface.InterfaceName).Length > 0) OrElse Me.ResourceManager.FailedFindingResources.Value
        End Function

        ''' <summary>Returns a string array of all local Gpib interfaces.</summary>
        Public Function FindInterfaces() As String() Implements Core.IConnectableInterface.FindInterfaces
            If GpibInterface.HasBoards() Then
                Return Me.ResourceManager.FindInterfaces(GpibInterface.InterfaceName)
            Else
                Return New String() {}
            End If
        End Function

        ''' <summary>Returns a string array of all local Gpib interfaces.</summary>
        ''' <param name="timeout">specifies the time out for looking for the first GPIB board.</param>
        Public Function FindInterfaces(ByVal firstBoardNumber As Integer, ByVal lastBoardNumber As Integer,
                                           ByVal timeout As Integer) As String()
            If GpibInterface.HasBoards(firstBoardNumber, lastBoardNumber, timeout) Then
                Return Me.ResourceManager.FindInterfaces(GpibInterface.InterfaceName)
            Else
                Return New String() {}
            End If
        End Function

        ''' <summary>Returns a string array of all local Gpib interfaces.</summary>
        ''' <param name="timeout">specifies the time out for looking for the first GPIB board.</param>
        Public Function FindInterfaces(ByVal timeout As Integer) As String()
            If GpibInterface.HasBoards(timeout) Then
                Return Me.ResourceManager.FindInterfaces(GpibInterface.InterfaceName)
            Else
                Return New String() {}
            End If
        End Function

        Private _identity As String
        ''' <summary>Gets or sets the instrument identity. Reads the instrument identity the first time it is 
        ''' called. Setting the identity allows emulation.</summary>
        Public Overridable Property Identity() As String Implements Core.IConnectableResource.Identity
            Get
                If String.IsNullOrWhiteSpace(Me._identity) Then
                    If Me.UsingDevices AndAlso Me._GpibInterface IsNot Nothing Then
                        Me._identity = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "Manufacturer Name: {0}; Manufacturer ID: 0x{1:X}; Specification Version: {2}; Interface Number: {3}.",
                                                  Me._GpibInterface.ResourceManufacturerName, Me._GpibInterface.ResourceManufacturerID,
                                                  Me._GpibInterface.ResourceSpecificationVersion, Me._GpibInterface.HardwareInterfaceNumber)
                    Else
                        Me._identity = "unknown"
                    End If
                End If
                Return Me._identity
            End Get
            Set(ByVal value As String)
                Me._identity = value
            End Set
        End Property

#End Region

#Region " RESOURCE MANAGEMENT "

        ''' <summary>
        ''' Returns true if an general visa exception occurred finding resources.
        ''' The exception points to failure not due to parsing of the resource name
        ''' as those are trapped and addressed.
        ''' </summary>
        Public ReadOnly Property FailedFindingResources() As Boolean? Implements Core.IConnectableInterface.FailedFindingResources
            Get
                If Me._resourceManager Is Nothing Then
                    Me._resourceManager = New LocalResourceManager
                    Return Not HasInterfaces()
                Else
                    Return Me._resourceManager.FailedFindingResources
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns a string array of the resources for this connectible resource.
        ''' </summary><returns></returns>
        Public Function FindResources() As String() Implements Core.IConnectableInterface.FindResources
            Return Me.ResourceManager.FindResources(LocalResourceManager.BuildResourceBaseName(VisaNS.HardwareInterfaceType.Gpib))
        End Function

        ''' <summary>
        ''' Explains failure finding resources.
        ''' </summary>
        ''' 
        Public ReadOnly Property ResourceFindingFailureSymptom() As String Implements Core.IConnectableInterface.ResourceFindingFailureSymptom
            Get
                Return Me.ResourceManager.ResourceFindingFailureSymptom
            End Get
        End Property

        ''' <summary>
        ''' Returns true if we have resources.
        ''' </summary>
        Public Function HasResources() As Boolean
            Return Me.ResourceManager.HasResources(LocalResourceManager.BuildResourceBaseName(VisaNS.HardwareInterfaceType.Gpib))
        End Function

#End Region

    End Class

End Namespace


