﻿Imports NationalInstruments

Namespace Gpib

    ''' <summary>Extends the VISA GPIB message based session.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history>  
    Public Class GpibSession

        Inherits NationalInstruments.VisaNS.GpibSession
        Implements isr.Core.IServicingRequest

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Initializes a new instance of the <see cref="GpibSession" /> class.
        ''' </summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        ''' <param name="connectTimeout">The connect timeout.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal resourceName As String, ByVal connectTimeout As Integer)

            ' instantiate the base class
            ' Rev 4.1 and 5.0 of VISA did not support this call and could not verify the 
            ' resource.  
            MyBase.New(resourceName, VisaNS.AccessModes.NoLock, connectTimeout, False)
            Me._reader = New MessageBased.SessionReader(Me, MyBase.TerminationCharacter)
            Me._writer = New MessageBased.SessionWriter(Me)
            Me._timeouts = New Collections.Generic.Stack(Of Integer)
            Me._errorAvailableBits = 4
            Me._messageAvailableBits = 16

            Me._identity = ""
            Me._errorQueue = New System.Text.StringBuilder

            Me._clearExecutionStateCommand = New SupportedCommand(Scpi.Syntax.ClearExecutionStateCommand)
            Me._ErrorQueryCommand = New SupportedCommand(Scpi.Syntax.ErrorQueryCommand)
            Me._errorQueueQueryCommand = New SupportedCommand(Scpi.Syntax.ErrorQueueQueryCommand) '  ":STAT:QUE?"
            Me._clearErrorQueueCommand = New SupportedCommand(Scpi.Syntax.ClearErrorQueueCommand) '  ":STAT:QUE:CLEAR"
            Me._standardEventEnableCommand = New SupportedCommand(Scpi.Syntax.StandardEventEnableCommand)
            Me._standardEventEnableQueryCommand = New SupportedCommand(Scpi.Syntax.StandardEventEnableQueryCommand)
            Me._standardEventStatusQueryCommand = New SupportedCommand(Scpi.Syntax.StandardEventStatusQueryCommand)
            Me._identityQueryCommand = New SupportedCommand(Scpi.Syntax.IdentityQueryCommand & "; " & Scpi.Syntax.WaitCommand)
            Me._operationCompletedCommand = New SupportedCommand(Scpi.Syntax.OperationCompletedCommand)
            Me._operationCompletedQueryCommand = New SupportedCommand(Scpi.Syntax.OperationCompletedQueryCommand & "; " & Scpi.Syntax.WaitCommand)
            Me._resetKnownStateCommand = New SupportedCommand(Scpi.Syntax.ResetKnownStateCommand)
            Me._serviceRequestEnableCommand = New SupportedCommand(Scpi.Syntax.ServiceRequestEnableCommand)
            Me._serviceRequestEnableQueryCommand = New SupportedCommand(Scpi.Syntax.ServiceRequestEnableQueryCommand)
            Me._serviceRequestStatusQueryCommand = New SupportedCommand(Scpi.Syntax.ServiceRequestStatusQueryCommand)
            Me._waitCommand = New SupportedCommand(Scpi.Syntax.WaitCommand)

            Me._lastErrorQueryCommand = New SupportedCommand(Scpi.Syntax.ErrorQueryCommand)
            Me._questionableStatusQueryCommand = New SupportedCommand(Scpi.Syntax.QuestionableStatusQueryCommand) ' ":STAT:QUES:EVEN?"
            Me._measurementStatusQueryCommand = New SupportedCommand(Scpi.Syntax.MeasurementStatusQueryCommand) ' ":STAT:MEAS:EVEN?")
            Me._operationEventStatusQueryCommand = New SupportedCommand(Scpi.Syntax.OperationEventStatusQueryCommand) ' ":STAT:OPER:EVEN?")

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Not Me._timeouts Is Nothing Then
                            Me._timeouts = Nothing
                        End If

                        If Me._reader IsNot Nothing Then
                            Me._reader = Nothing
                        End If

                        If Me._writer IsNot Nothing Then
                            Me._writer = Nothing
                        End If

                        Me._errorQueue = Nothing
                        Me._identity = ""

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " RESOURCE NAME MANAGEMENT "

        ''' <summary>
        ''' Specifies the format of a GPIB resource instrument.
        ''' </summary>
        Public Const ResourceFormat As String = "GPIB{0}::{1}::INSTR"

        ''' <summary>Returns a Gpib resource name string.</summary>
        ''' <param name="address">The device primary address</param>
        ''' <returns>A VISA resource Gpib instrument name in the form "GPIB0::PrimaryAddress::INSTR".</returns>
        Public Shared Function BuildResourceName(ByVal address As Int32) As String
            Return BuildResourceName(0, address)
        End Function

        ''' <summary>Returns a Gpib resource name string.</summary>
        ''' <param name="boardNumber">Gpib board number.</param>
        ''' <param name="address">The device primary address</param>
        ''' <returns>A VISA resource Gpib instrument name in the form 
        '''   "GPIBBoardNumber::PrimaryAddress::INSTR".</returns>
        Public Shared Function BuildResourceName(ByVal boardNumber As Int32, ByVal address As Int32) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, GpibSession.ResourceFormat, boardNumber, address)
        End Function

        ''' <summary>Returns a Gpib resource name string.</summary>
        ''' <param name="boardNumber">Gpib board number.</param>
        ''' <param name="address">The device primary address</param>
        ''' <returns>A VISA resource Gpib instrument name in the form 
        '''   "GPIBBoardNumber::PrimaryAddress::INSTR".</returns>
        Public Shared Function BuildGpibResourceName(ByVal boardNumber As String, ByVal address As String) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, GpibSession.ResourceFormat, boardNumber, address)
        End Function

        ''' <summary>Parses the instrument address from the resource name.</summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        Public Shared Function ParsePrimaryAddress(ByVal resourceName As String) As Int32

            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If

            Dim value As String = isr.Core.StringSubstringExtensions.SubstringBetween(resourceName, "::", "::")
            If String.IsNullOrWhiteSpace(value) Then
                Dim numericValue As Integer
                If Int32.TryParse(value, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return 0
                End If
            Else
                Return 0
            End If

        End Function

        ''' <summary>Parses the GPIB instrument address from the GPIB resource name.</summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        Public Shared Function ParseBoardNumber(ByVal resourceName As String) As Int32

            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If

            Dim value As String = isr.Core.StringSubstringExtensions.SubstringBetween(resourceName, "(", ")")
            If String.IsNullOrWhiteSpace(value) Then
                Dim numericValue As Integer
                If Int32.TryParse(value, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return 0
                End If
            Else
                Return 0
            End If

        End Function

        ''' <summary>
        ''' Gets the board number fo the current resource name.
        ''' </summary>
        Public ReadOnly Property BoardNumber() As Integer
            Get
                Return GpibSession.ParseBoardNumber(MyBase.ResourceName)
            End Get
        End Property

#End Region

#Region " IEEE 488.2:  IRESETTABLE "

        ''' <summary>Selectively clears the instrument.</summary>
        Public Function ClearActiveState() As Boolean
            MyBase.Clear()
            Return Not Me.HasError()
        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        Public Function ClearExecutionState() As Boolean
            Return Me.Execute(Me.ClearErrorQueueCommand)
        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Function ResetKnownState() As Boolean
            Return Me.Execute(Me.ResetKnownStateCommand)
        End Function

#End Region

#Region " TRY CONNECT "

        ''' <summary>
        ''' Try to connect to the specified VISA resource.
        ''' </summary>
        ''' <param name="resourceName">A VISA resource Name.</param>
        ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
        Public Shared Function TryConnect(ByVal resourceName As String, ByVal timeout As Integer) As Boolean

            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Dim session As GpibSession = Nothing
            Try

                ' check if we have the specified GPIB resource
                session = New GpibSession(resourceName, timeout)

                If session Is Nothing OrElse session.LastStatus <> VisaNS.VisaStatusCode.Success Then
                    Return False
                Else
                    ' set timeout.
                    session.Timeout = timeout
                    ' verify that we can clear and query the session.
                    session.Clear()
                    session.Query("*OPC?")
                    Return True
                End If

            Catch ex As NationalInstruments.VisaNS.VisaException

                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Return False
                Else
                    Throw New NationalInstruments.VisaNS.VisaException("VISA Exception", ex)
                End If

            Catch

                Throw

            Finally

                If session IsNot Nothing Then
                    session.Dispose()
                    session = Nothing
                End If

            End Try

        End Function

        ''' <summary>
        ''' Try to connect to the specified GPIB instrument.
        ''' </summary>
        ''' <param name="boardNumber">Gpib board number.</param>
        ''' <param name="address">The device primary address</param>
        ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
        Public Shared Function TryConnect(ByVal boardNumber As Integer, ByVal address As Int32, ByVal timeout As Integer) As Boolean
            Return GpibSession.TryConnect(GpibSession.BuildResourceName(boardNumber, address), timeout)
        End Function


#End Region

#Region " DEVICE COMMANDS "

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function QueryOperationCompleted() As Boolean
            Return Me.QueryInt32(OperationCompletedQueryCommand.Value).Value = 1
        End Function

        ''' <summary>
        ''' Allows controlling the instrument manually.
        ''' </summary>
        Public Function GoToLocal() As Boolean
            Me.ControlRen(VisaNS.RenMode.Assert)
            Return Me.LastStatus = VisaNS.VisaStatusCode.Success
        End Function

        Private _identity As String
        ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
        ''' <remarks>
        ''' 2400: KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E
        ''' 2600: Keithley Instruments Inc., Model 2612A, 1214466, 2.1.6
        ''' </remarks>
        Public Function ReadIdentity() As String
            If Me._identityQueryCommand.IsSupported Then
                Me._identity = Me.QueryTrimEnd(Me.IdentifyQueryCommand.Value)
            Else
                Me._identity = "NOT SUPPORTED"
            End If
            Return Me._identity
        End Function

        ''' <summary>Queries the Gpib instrument and returns the string save the termination character.</summary>
        Public Overridable Property Identity() As String
            Get
                If String.IsNullOrWhiteSpace(Me._identity) Then
                    Me.ReadIdentity()
                End If
                Return Me._identity
            End Get
            Set(ByVal Value As String)
                Me._identity = Value
            End Set
        End Property

#End Region

#Region " ERROR MANAGEMENT "

        ''' <summary>Clears the error Queue.</summary>
        Public Overridable Function ClearErrorQueue() As Boolean
            If Me.ClearErrorQueueCommand.IsSupported Then
                Me.WriteLine(Me.ClearErrorQueueCommand.Value)
            End If
            Return Me.LastStatus = VisaNS.VisaStatusCode.Success
        End Function

        ''' <summary>
        ''' Gets a report of the error stored in the cached error queue event status register (ESR) information.
        ''' </summary>
        Public Overridable ReadOnly Property DeviceErrors() As String
            Get
                Dim message As New System.Text.StringBuilder
                If Me._errorQueue.Length > 0 Then
                    If message.Length > 0 Then
                        message.AppendLine()
                    End If
                    message.AppendLine("The device reported the following error(s) from the error queue:")
                    message.Append(Me._errorQueue.ToString)
                End If
                Return message.ToString
            End Get
        End Property

        Private _errorAvailableBits As Integer
        ''' <summary>Gets or sets bits that would be set for detecting if an error
        ''' is available.</summary>
        Public Overridable Property ErrorAvailableBits() As Integer
            Get
                Return Me._errorAvailableBits
            End Get
            Set(ByVal Value As Integer)
                Me._errorAvailableBits = Value
            End Set
        End Property

        Private _hadError As Boolean
        ''' <summary>Returns True if the last operation reported an error by
        ''' way of a service request.
        ''' The error is reported if the cached error indicator is true or a new error is reported by 
        ''' the service request register.</summary>
        Public Property HadError() As Boolean
            Get
                Me._hadError = Me._hadError OrElse Me.HasError()
                Return Me._hadError
            End Get
            Set(ByVal value As Boolean)
                Me._hadError = value
            End Set
        End Property

        ''' <summary>Returns a True is the service request register 
        ''' status byte reports error available.</summary>
        Public Function HasError() As Boolean
            Return Me.HasError(Me.ErrorAvailableBits)
        End Function

        ''' <summary>Returns a True is the service request register 
        ''' status byte reports error available.</summary>
        Public Function HasError(ByVal errorBits As Integer) As Boolean
            Return (Me.ReadStatusByte() And errorBits) <> 0
        End Function

        ''' <summary>
        ''' Check the status byte for error bits and latches the last error sentinel.
        ''' </summary>
        Public Overridable Function IsErrorAvailable() As Boolean
            Me.HadError = False
            Return Me.HadError
        End Function

        Private _errorQueue As System.Text.StringBuilder
        ''' <summary>
        ''' Gets the error queue cache.
        ''' </summary>
        ''' 
        Public ReadOnly Property ErrorQueueCache() As String
            Get
                If Me._errorQueue Is Nothing OrElse Me._errorQueue.Length = 0 Then
                    Return ""
                Else
                    Return Me._errorQueue.ToString
                End If
            End Get
        End Property

        ''' <summary>Gets or sets the last read error queue.
        ''' Setting the error allows emulating or clearing errors when the error queue command
        ''' is not supported or not using devices.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function ReadErrorQueue() As String Implements isr.Core.IServicingRequest.ReadErrorQueue
            Try

                ' clear the queue string
                Me._errorQueue = New System.Text.StringBuilder

                ' loop while we have an error
                Do While (Me.ReadStatusByte And Me.ErrorAvailableBits) <> 0

                    If Me._errorQueue.Length > 0 Then
                        Me._errorQueue.Append(Environment.NewLine)
                    End If

                    ' save the error status
                    Me.HadError = True

                    ' send the command to get the queue
                    Me.WriteLine(ErrorQueueQueryCommand.Value)

                    ' Add the error string
                    Me._errorQueue.AppendFormat("Device error {0}", Me.ReadLineTrimEnd())

                Loop

                ' return the queue
                Return Me._errorQueue.ToString

            Catch ex As Exception

                ' if we have an error, return a note of it
                Return "Exception occurred getting the error queue. Details: " & ex.ToString

            End Try
        End Function

        ''' <summary>Returns the last error from the instrument.</summary>
        Public Function ReadLastError() As String Implements isr.Core.IServicingRequest.ReadLastError
            Return Me.Query(Me.ErrorQueryCommand.Value)
        End Function

        ''' <summary>
        ''' Returns the error count.
        ''' </summary>
        ''' <returns>The error count or -1 is command is not supported.</returns>
        Public Function ReadErrorQueueCount() As Integer
            If Me.ErrorQueueCountQueryCommand.IsSupported Then
                Dim count As Integer? = Me.QueryInt32(Me.ErrorQueueCountQueryCommand.Value)
                Return count.GetValueOrDefault(0)
            Else
                Return -1
            End If
        End Function

#End Region

#Region " I/O "

        ''' <summary>
        ''' Executes the specified command if the command is supported.
        ''' </summary>
        ''' <param name="value">The value.</param><returns></returns>
        Public Function Execute(ByVal value As SupportedCommand) As Boolean
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If
            If value.IsSupported Then
                MyBase.Write(value.Value)
            End If
            Return Me.LastStatus = VisaNS.VisaStatusCode.Success
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.
        ''' </param>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal value As Boolean, ByVal numeric As Boolean) As Boolean
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Me.WriteLine(queryCommand.BuildCommand(value, numeric))
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.
        ''' </param>
        ''' <param name="value">Specifies the command value.</param>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal value As String) As Boolean
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Me.WriteLine(queryCommand.BuildCommand(value))
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.
        ''' </param>
        ''' <param name="value">Specifies the command value.</param>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal value As Integer) As Boolean
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Me.WriteLine(queryCommand.BuildCommand(value))
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.
        ''' </param>
        ''' <param name="value">Specifies the command value.</param>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal value As Double) As Boolean
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Me.WriteLine(queryCommand.BuildCommand(value))
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Reads the operation completion status of the last operation.
        ''' </summary>
        Public Function ReadOperationComplete() As Integer?
            If Me._operationCompletedQueryCommand.IsSupported Then
                Return Me.QueryInteger(Me.OperationCompletedQueryCommand.Value)
            Else
                Return New Integer?(1)
            End If
        End Function

        ''' <summary>
        ''' Reads the a register.
        ''' </summary>
        Public Function ReadRegister(ByVal value As SupportedCommand) As Integer?
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If
            If value.IsSupported Then
                Return Me.QueryInteger(value.Value)
            Else
                Return New Integer?
            End If
        End Function

#End Region

#Region " MAV "

        ''' <summary>Reads the device status data byte and returns True
        ''' if the message available bits were set.
        ''' </summary>
        ''' <returns>True if the device has data in the queue</returns>
        Public Overridable Function IsMessageAvailable() As Boolean
            Return (Me.ReadStatusByte() And Me._messageAvailableBits) <> 0
        End Function

        Private _messageAvailableBits As Integer
        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Public Property MessageAvailableBits() As Integer
            Get
                Return Me._messageAvailableBits
            End Get
            Set(ByVal Value As Integer)
                Me._messageAvailableBits = Value
            End Set
        End Property

#End Region

#Region " OPEN "

        ''' <summary>Returns an open message based session.</summary>
        ''' <returns>An open Gpib VISA session.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function OpenSession(ByVal resourceName As String, ByVal connectTimeout As Integer) As GpibSession
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New GpibSession(resourceName, connectTimeout)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("The resource selected must be a Gpib resource", ex)
            End Try
        End Function

#End Region

#Region " QUERY "

        ''' <summary>Queries the instrument and returns a Boolean value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            Return MessageBased.SessionReader.ParseBoolean(Me.QueryTrimEnd(question))
        End Function

        ''' <summary>Queries the instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            Return MessageBased.SessionReader.ParseDouble(Me.QueryTrimEnd(question))
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            Return MessageBased.SessionReader.ParseInt32(Me.QueryTrimEnd(question))
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.
        ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
            Dim queryResult As String = Me.QueryTrimEnd(question)
            Return MessageBased.SessionReader.ParseInfInt32(queryResult)
        End Function

        ''' <summary>Queries the instrument and returns an Integer value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInteger(ByVal question As String) As Nullable(Of Integer)
            Return MessageBased.SessionReader.ParseInteger(Me.QueryTrimEnd(question))
        End Function

        ''' <summary>Queries the instrument and returns an Integer value.
        ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInteger(ByVal question As String) As Nullable(Of Integer)
            Return MessageBased.SessionReader.ParseInfInteger(Me.QueryTrimEnd(question))
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Shadows Function Query(ByVal question As String) As String
#If DIAGNOSE Then
    Trace.CorrelationManager.StartLogicalOperation("QUERY")
    My.Application.Log.WriteEntry(question, TraceEventType.Verbose)
#End If
            Me._writer.TransmitBuffer = question
            Me._reader.ReceiveBuffer = MyBase.Query(question)
#If DIAGNOSE Then
    My.Application.Log.WriteEntry(Me._reader.ReceiveBuffer, TraceEventType.Verbose)
    Trace.CorrelationManager.StopLogicalOperation()
#End If
            Return Me._reader.ReceiveBuffer
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Overloads Function QueryTrimEnd(ByVal question As String) As String
            Return Me.Query(question).TrimEnd(Convert.ToChar(MyBase.TerminationCharacter))
        End Function

        ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
        ''' <param name="question">The query to use.</param>
        Public Overloads Function QueryTrimNewLine(ByVal question As String) As String
            Return Me.Query(question).TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
        End Function

#End Region

#Region " READ "

        ''' <summary>
        ''' Reads and discards all data from the VISA session until the END indicator is read.
        ''' </summary>
        Public Sub DiscardUnreadData()
            ' this does not work! Me._reader.DiscardUnreadData()
            Do While Me.IsMessageAvailable()
                Me._reader.ReadLine()
            Loop
        End Sub

        Private _reader As MessageBased.SessionReader
        ''' <summary>
        ''' Gets reference to the message based reader.
        ''' </summary>
        Public ReadOnly Property Reader() As MessageBased.SessionReader
            Get
                Return Me._reader
            End Get
        End Property

        ''' <summary>Gets the last message that was received from the instrument.
        ''' Note that not all messages get recorded.
        ''' </summary>
        Public ReadOnly Property ReceiveBuffer() As String
            Get
                Return Me._reader.ReceiveBuffer
            End Get
        End Property

        ''' <summary>Reads the instrument and returns a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            Return MessageBased.SessionReader.ParseBoolean(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads the instrument and returns a double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            Return MessageBased.SessionReader.ParseDouble(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads the instrument and returns an Int32 value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            Return MessageBased.SessionReader.ParseInt32(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads the instrument and returns an Int32 value.
        ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
        Public Function ReadInfInt32() As Nullable(Of Int32)
            Return MessageBased.SessionReader.ParseInfInt32(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads the instrument and returns an Integer value.</summary>
        Public Function ReadInteger() As Nullable(Of Integer)
            Return MessageBased.SessionReader.ParseInteger(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads the instrument and returns an Integer value.
        ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
        Public Function ReadInfInteger() As Nullable(Of Integer)
            Return MessageBased.SessionReader.ParseInfInteger(Me._reader.ReadLineTrimEnd())
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
        Public Shadows Function ReadString() As String
            Return Me._reader.ReadLine()
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
        Public Overloads Function ReadLine() As String
            Return Me._reader.ReadLine()
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
        Public Overloads Function ReadLineTrimEnd() As String Implements isr.Core.IServicingRequest.ReadLineTrimEnd
            Return Me._reader.ReadLineTrimEnd()
        End Function

        ''' <summary>
        ''' Gets or sets the termination character.
        ''' </summary>
        Public Shadows Property TerminationCharacter() As Byte
            Get
                Return MyBase.TerminationCharacter
            End Get
            Set(ByVal value As Byte)
                MyBase.TerminationCharacter = value
                Me._reader.TerminationCharacter = value
            End Set
        End Property

#End Region

#Region " IEEE 488.2:  REGISTERS "

#Region " MEASUREMENT EVENTS "

        Private _MeasurementEventStatus As Integer
        ''' <summary>
        ''' Gets or sets the Measurement event status.
        ''' </summary>
        ''' <value>
        ''' The Measurement event status.
        ''' </value>
        Public Property MeasurementEventStatus() As Integer
            Get
                Return Me._MeasurementEventStatus
            End Get
            Set(ByVal Value As Integer)
                Me._MeasurementEventStatus = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadMeasurementEventStatus() As Integer Implements isr.Core.IServicingRequest.ReadMeasurementEventStatus
            Me.MeasurementEventStatus = Me.ReadRegister(Me.MeasurementStatusQueryCommand).GetValueOrDefault(0)
            Return Me.MeasurementEventStatus
        End Function

#End Region

#Region " OPERATION "

        Private _OperationEventStatus As Integer
        ''' <summary>
        ''' Gets or sets the Operation event status.
        ''' </summary>
        ''' <value>
        ''' The Operation event status.
        ''' </value>
        Public Property OperationEventStatus() As Integer
            Get
                Return Me._OperationEventStatus
            End Get
            Set(ByVal Value As Integer)
                Me._OperationEventStatus = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadOperationEventStatus() As Integer Implements isr.Core.IServicingRequest.ReadOperationEventStatus
            Me.OperationEventStatus = Me.ReadRegister(Me.OperationEventStatusQueryCommand).Value
            Return Me.OperationEventStatus
        End Function

#End Region

#Region " QUESTIONABLE EVENTS "

        Private _QuestionableEventStatus As Integer
        ''' <summary>
        ''' Gets or sets the Questionable event status.
        ''' </summary>
        ''' <value>
        ''' The Questionable event status.
        ''' </value>
        Public Property QuestionableEventStatus() As Integer
            Get
                Return Me._QuestionableEventStatus
            End Get
            Set(ByVal Value As Integer)
                Me._QuestionableEventStatus = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadQuestionableEventStatus() As Integer Implements isr.Core.IServicingRequest.ReadQuestionableEventStatus
            Me.QuestionableEventStatus = Me.ReadRegister(Me.QuestionableStatusQueryCommand).Value
            Return Me.QuestionableEventStatus
        End Function

#End Region

#Region " STANDARD EVENTS "

        ''' <summary>Returns a True is the service request register 
        ''' status byte reports error available.</summary>
        ''' <param name="standardErrorBits">Specifies the standard device register error bits.</param>
        Public Overridable Function HasStandardError(ByVal standardErrorBits As Integer) As Boolean
            Return (Me.ReadStandardEventStatus() And standardErrorBits) <> 0
        End Function

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public Overridable Function ReadStandardEventStatus() As Integer Implements isr.Core.IServicingRequest.ReadStandardEventStatus
            Me.StandardEventStatus = Me.QueryInt32(StandardEventStatusQueryCommand.Value).Value
            Return Me.StandardEventStatus
        End Function

        ''' <summary>Read the standard event request.</summary>
        ''' <returns>The mask to use for enabling the events.</returns>
        Public Overridable Function ReadStandardEventEnable() As Integer
            Me.StandardEventEnable = Me.QueryInt32(StandardEventEnableQueryCommand.Value).Value
            Return Me.StandardEventEnable
        End Function

        Private _standardEventEnable As Integer
        ''' <summary>
        ''' Gets or sets the standard event enable.
        ''' </summary>
        ''' <value>
        ''' The standard event enable.
        ''' </value>
        Public Property StandardEventEnable() As Integer
            Get
                Return Me._standardEventEnable
            End Get
            Set(ByVal Value As Integer)
                Me._standardEventEnable = Value
            End Set
        End Property

        Private _standardEventStatus As Integer
        ''' <summary>
        ''' Gets or sets the standard event status.
        ''' </summary>
        ''' <value>
        ''' The standard event status.
        ''' </value>
        Public Property StandardEventStatus() As Integer
            Get
                Return Me._standardEventStatus
            End Get
            Set(ByVal Value As Integer)
                Me._standardEventStatus = Value
            End Set
        End Property

        ''' <summary>Programs the standard event request.</summary>
        Public Overridable Function WriteStandardEventEnable(ByVal value As Integer) As Boolean
            Me.StandardEventEnable = value
            Me.WriteLine(StandardEventEnableCommand.BuildCommand(value))
            Return Me.IsLastVisaOperationSuccess
        End Function

#End Region

#Region " STATUS REGISTER "

        Private _cachedStatusByte As Integer
        ''' <summary>
        ''' Gets the last read status byte.
        ''' </summary>
        Public Property CachedStatusByte() As Integer
            Get
                Return Me._cachedStatusByte
            End Get
            Set(ByVal value As Integer)
                Me._cachedStatusByte = value
            End Set
        End Property

        ''' <summary>Returns the service request register status byte.</summary>
        Public Shadows Function ReadStatusByte() As Integer Implements isr.Core.IServicingRequest.ReadStatusByte1
            Me._cachedStatusByte = CType(MyBase.ReadStatusByte(), Integer)
            If Me._cachedStatusByte >= 0 Then
                Return Me._cachedStatusByte
            Else
                Return 256 + Me._cachedStatusByte
            End If
        End Function

#End Region

#Region " SERVICE REQUESTS / STATUS REGISTER "

        ''' <summary>Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that wil issue an SRQ
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Overridable Function EnableServiceRequest(ByVal standardEventEnableBits As Integer,
                                                         ByVal serviceRequestEnableBits As Integer) As Boolean

            Dim commandBuilder As New System.Text.StringBuilder
            commandBuilder.Append(ClearExecutionStateCommand)
            commandBuilder.Append("; ")
            commandBuilder.Append(StandardEventEnableCommand.BuildCommand(standardEventEnableBits))
            commandBuilder.Append("; ")
            commandBuilder.Append(ServiceRequestEnableCommand.BuildCommand(serviceRequestEnableBits))

            ' Check if necessary; clear the service request bit
            Me.ReadStatusByte()

            Me.WriteLine(commandBuilder.ToString)
            ' Me._scpi.Device.Send("*cls; *ese " & CStr(CInt(standardEventEnableBits)) & "; *sre " & CStr(CInt(serviceRequestEnableBits)))

            Return Me.LastStatus = VisaNS.VisaStatusCode.Success

        End Function

        ''' <summary>Reads the service request event request.</summary>
        ''' <returns>The mask to use for enabling the events.</returns>
        Public Overridable Function ReadServiceRequestEventEnable() As Integer
            Me.ServiceRequestEventEnable = Me.QueryInt32(ServiceRequestEnableQueryCommand.Value).Value
            Return Me.ServiceRequestEventEnable
        End Function

        Private _ServiceRequestEventEnable As Integer
        ''' <summary>
        ''' Gets or sets the ServiceRequest event enable.
        ''' </summary>
        ''' <value>
        ''' The ServiceRequest event enable.
        ''' </value>
        Public Property ServiceRequestEventEnable() As Integer
            Get
                Return Me._ServiceRequestEventEnable
            End Get
            Set(ByVal Value As Integer)
                Me._ServiceRequestEventEnable = Value
            End Set
        End Property

        Private _ServiceRequestEventStatus As Integer
        ''' <summary>
        ''' Gets or sets the ServiceRequest event status.
        ''' </summary>
        ''' <value>
        ''' The ServiceRequest event status.
        ''' </value>
        Public Property ServiceRequestEventStatus() As Integer
            Get
                Return Me._ServiceRequestEventStatus
            End Get
            Set(ByVal Value As Integer)
                Me._ServiceRequestEventStatus = Value
            End Set
        End Property

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public Function ReadServiceRequestEventStatus() As Integer
            ServiceRequestEventStatus = Me.QueryInt32(ServiceRequestStatusQueryCommand.Value).Value
            Return ServiceRequestEventStatus
        End Function

        ''' <summary>Programs the service request event request.</summary>
        ''' <remarks>When set clears all service registers.</remarks>
        Public Overridable Function WriteServiceRequestEventEnable(ByVal value As Integer) As Boolean
            Me.ServiceRequestEventEnable = value
            Me.WriteLine(ServiceRequestEnableCommand.BuildCommand(value))
            Return Me.IsLastVisaOperationSuccess
        End Function


#End Region

#End Region

#Region " SYNTAX -- SUPPORTED COMMANDS "

#Region " ERROR "

        Private _clearErrorQueueCommand As SupportedCommand
        ''' <summary>Gets or sets the Error Query clear Command for this instrument.</summary>
        Public Property ClearErrorQueueCommand() As SupportedCommand
            Get
                Return Me._clearErrorQueueCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._clearErrorQueueCommand = Value
            End Set
        End Property

        Private _ErrorQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the last error Query Command for this instrument.</summary>
        Public Property ErrorQueryCommand() As SupportedCommand
            Get
                Return Me._ErrorQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._ErrorQueryCommand = Value
            End Set
        End Property

        Private _errorQueueQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the Error Query Command for this instrument.</summary>
        Public Property ErrorQueueQueryCommand() As SupportedCommand
            Get
                Return Me._errorQueueQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._errorQueueQueryCommand = Value
            End Set
        End Property

        Private _errorQueueCountQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the Error Count Query Command for this instrument.</summary>
        Public Property ErrorQueueCountQueryCommand() As SupportedCommand
            Get
                Return Me._errorQueueCountQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._errorQueueCountQueryCommand = Value
            End Set
        End Property

        Private _lastErrorQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Public Property LastErrorQueryCommand() As SupportedCommand
            Get
                Return Me._lastErrorQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._lastErrorQueryCommand = Value
            End Set
        End Property

#End Region

#Region " REGISTERS "

        Private _measurementStatusQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Public Property MeasurementStatusQueryCommand() As SupportedCommand
            Get
                Return Me._measurementStatusQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._measurementStatusQueryCommand = Value
            End Set
        End Property

        Private _operationEventEnableCommand As SupportedCommand
        ''' <summary>Gets or sets the operation event enable command message for this instrument.
        ''' Is supported if the operation event enable command message is not empty.
        ''' </summary>
        Public Property OperationEventEnableCommand() As SupportedCommand
            Get
                Return Me._operationEventEnableCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._operationEventEnableCommand = Value
                If Value IsNot Nothing AndAlso Me._operationEventEnableCommand.IsSupported Then
                    Me.OperationEventEnableQueryCommand = New SupportedCommand(Value.Value & "?")
                Else
                    Me.OperationEventEnableQueryCommand = New SupportedCommand("")
                End If
            End Set
        End Property

        Private _operationEventEnableQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the operation event enable Query Command for this instrument.</summary>
        Public Property OperationEventEnableQueryCommand() As SupportedCommand
            Get
                Return Me._operationEventEnableQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._operationEventEnableQueryCommand = Value
            End Set
        End Property

        Private _operationEventStatusQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the event status register (ESR) Query Command for this instrument.</summary>
        Public Property OperationEventStatusQueryCommand() As SupportedCommand
            Get
                Return Me._operationEventStatusQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._operationEventStatusQueryCommand = Value
            End Set
        End Property

        Private _questionableStatusQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the questionable status Query Command for this instrument.</summary>
        Public Property QuestionableStatusQueryCommand() As SupportedCommand
            Get
                Return Me._questionableStatusQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._questionableStatusQueryCommand = Value
            End Set
        End Property

        Private _serviceRequestEnableCommand As SupportedCommand
        ''' <summary>Gets or sets the service request enable (SRE) command message for this instrument.</summary>
        Public Property ServiceRequestEnableCommand() As SupportedCommand
            Get
                Return Me._serviceRequestEnableCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._serviceRequestEnableCommand = Value
                If Value IsNot Nothing AndAlso Me._serviceRequestEnableCommand.IsSupported Then
                    Me.ServiceRequestEnableQueryCommand = New SupportedCommand(Value.Value & "?")
                Else
                    Me.ServiceRequestEnableQueryCommand = New SupportedCommand("")
                End If
            End Set
        End Property

        Private _serviceRequestEnableQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the service request enable (SRE) Query Command for this instrument.</summary>
        Public Property ServiceRequestEnableQueryCommand() As SupportedCommand
            Get
                Return Me._serviceRequestEnableQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._serviceRequestEnableQueryCommand = Value
            End Set
        End Property

        Private _serviceRequestStatusQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the status byte (STB) Query Command for this instrument.</summary>
        Public Property ServiceRequestStatusQueryCommand() As SupportedCommand
            Get
                Return Me._serviceRequestStatusQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._serviceRequestStatusQueryCommand = Value
            End Set
        End Property

        Private _standardEventEnableCommand As SupportedCommand
        ''' <summary>Gets or sets the event enable (ESE) command message for this instrument.
        ''' Is supported if the event enable (ESE) command message is not empty.
        ''' </summary>
        Public Property StandardEventEnableCommand() As SupportedCommand
            Get
                Return Me._standardEventEnableCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._standardEventEnableCommand = Value
                If Value IsNot Nothing AndAlso Me._standardEventEnableCommand.IsSupported Then
                    Me.StandardEventEnableQueryCommand = New SupportedCommand(Value.Value & "?")
                Else
                    Me.StandardEventEnableQueryCommand = New SupportedCommand("")
                End If
            End Set
        End Property

        Private _standardEventEnableQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the event enable (ESE) Query Command for this instrument.</summary>
        Public Property StandardEventEnableQueryCommand() As SupportedCommand
            Get
                Return Me._standardEventEnableQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._standardEventEnableQueryCommand = Value
            End Set
        End Property

        Private _standardEventStatusQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the event status register (ESR) Query Command for this instrument.</summary>
        Public Property StandardEventStatusQueryCommand() As SupportedCommand
            Get
                Return Me._standardEventStatusQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._standardEventStatusQueryCommand = Value
            End Set
        End Property

#End Region

#Region " SYSTEM "

        Private _clearExecutionStateCommand As SupportedCommand
        ''' <summary>Gets or sets the CLS command message for this instrument.</summary>
        Public Property ClearExecutionStateCommand() As SupportedCommand
            Get
                Return Me._clearExecutionStateCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._clearExecutionStateCommand = Value
            End Set
        End Property

        Private _identityQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the IDN command message for this instrument.</summary>
        Public Property IdentifyQueryCommand() As SupportedCommand
            Get
                Return Me._identityQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._identityQueryCommand = Value
            End Set
        End Property

        Private _operationCompletedQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the OPC Query Command for this instrument.</summary>
        Public Property OperationCompletedQueryCommand() As SupportedCommand
            Get
                Return Me._operationCompletedQueryCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._operationCompletedQueryCommand = Value
            End Set
        End Property

        Private _operationCompletedCommand As SupportedCommand
        ''' <summary>Gets or sets the OPC command message for this instrument.</summary>
        Public Property OperationCompletedCommand() As SupportedCommand
            Get
                Return Me._operationCompletedCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._operationCompletedCommand = Value
                If Value IsNot Nothing AndAlso Me._operationCompletedCommand.IsSupported Then
                    Me.OperationCompletedQueryCommand = New SupportedCommand(Value.Value & "?")
                Else
                    Me.OperationCompletedQueryCommand = New SupportedCommand("")
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the RTS command for this instrument.</summary>
        Private _resetKnownStateCommand As SupportedCommand

        ''' <summary>Gets or sets the RTS command for this instrument.</summary>
        Public Property ResetKnownStateCommand() As SupportedCommand
            Get
                Return Me._resetKnownStateCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._resetKnownStateCommand = Value
            End Set
        End Property

        Private _waitCommand As SupportedCommand
        ''' <summary>Gets or sets the WAI command message for this instrument.</summary>
        Public Property WaitCommand() As SupportedCommand
            Get
                Return Me._waitCommand
            End Get
            Set(ByVal Value As SupportedCommand)
                Me._waitCommand = Value
            End Set
        End Property

#End Region

#End Region

#Region " WRITE "

        ''' <summary>
        ''' Sends all unsent data from the write buffer to the device.
        ''' </summary>
        Public Sub FlushWrite()
            Me._writer.Flush()
        End Sub

        Private _writer As MessageBased.SessionWriter
        ''' <summary>
        ''' Gets reference to the message based writer.
        ''' </summary>
        Public ReadOnly Property Writer() As MessageBased.SessionWriter
            Get
                Return Me._writer
            End Get
        End Property

        ''' <summary>Writes command to the instrument.</summary>
        ''' <param name="value">The string to write.</param>
        Public Shadows Sub Write(ByVal value As String)
            Me.WriteLine(value)
        End Sub

        ''' <summary>Writes command to the instrument.</summary>
        ''' <param name="value">The string to write.</param>
        Public Sub WriteLine(ByVal value As String)
            Me._writer.WriteLine(value)
        End Sub

        ''' <summary>Writes boolean command to the instrument.</summary>
        ''' <param name="queryCommand">The string to write.</param>
        ''' <param name="value">The boolean value to write.</param>
        ''' <param name="boolFormat">The boolean format.</param>
        Public Sub WriteLine(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat)
            Me.WriteLine(Scpi.Syntax.BuildBooleanCommand(queryCommand, value, boolFormat))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="queryCommand">The string to write.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteLineOnOff(ByVal queryCommand As String, ByVal isOn As Boolean)
            Me.WriteLine(Scpi.Syntax.BuildBooleanCommand(queryCommand, isOn, BooleanDataFormat.OnOff))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="queryCommand">The string to write.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteLineOneZero(ByVal queryCommand As String, ByVal one As Boolean)
            Me.WriteLine(Scpi.Syntax.BuildBooleanCommand(queryCommand, one, BooleanDataFormat.OneZero))
        End Sub

        ''' <summary>Gets the last message that was received from the instrument.
        ''' Note that not all messages get recorded.
        ''' </summary>
        Public ReadOnly Property TransmitBuffer() As String
            Get
                Return Me._writer.TransmitBuffer
            End Get
        End Property

#End Region

#Region " TIMEOUT MANAGEMENT "

        ''' <summary>
        ''' Gets or sets the minimum instrument timeout value in milliseconds
        ''' when accessing the device.
        ''' </summary>
        Public Shadows Property Timeout() As Integer
            Get
                Return MyBase.Timeout
            End Get
            Set(ByVal value As Integer)
                MyBase.Timeout = value
            End Set
        End Property

        ''' <summary>Restores the last timeout from the stack.
        ''' </summary>
        Public Sub RestoreTimeout()
            MyBase.Timeout = Me._timeouts.Pop
        End Sub

        ''' <summary>Saves the current timeout and sets a new setting timeout.
        ''' </summary>
        ''' <param name="timeout">Specifies the new timeout</param>
        Public Sub StoreTimeout(ByVal timeout As Integer)
            Me._timeouts.Push(MyBase.Timeout)
            MyBase.Timeout = timeout
        End Sub

        ''' <summary>Gets or sets the reference to the stack of timeouts.
        ''' </summary>
        Private _timeouts As System.Collections.Generic.Stack(Of Integer)

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended with success.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return MyBase.LastStatus = 0
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                Return MyBase.LastStatus < 0
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                Return MyBase.LastStatus > 0
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public Function BuildLastVisaStatusDetails() As String
            Return VisaManager.BuildVisaStatusDetails(CType(MyBase.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
        End Function

#End Region

    End Class

End Namespace

