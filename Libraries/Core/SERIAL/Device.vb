Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Imports NationalInstruments.VisaNS

Namespace Serial
    ''' <summary>Provides a Core. for non-SCPI Serial instruments.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history> 
    Public Class Device
        Inherits SimpleDevice
        Implements IDisposable, IDevice

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Protected Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Device).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I RESETTABLE "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            MyBase.ClearExecutionState()
            Me._lastServiceEventArgs = Nothing
            Return True

        End Function

#End Region

#Region " I RESOURCE INFO "

        ''' <summary>
        ''' Gets or sets the instrument identity. Reads the instrument identity the first time it is 
        ''' called. Setting the identity allows emulation.
        ''' Cleared when connected.
        ''' </summary>
        Public Overrides Property Identity() As String
            Get
                Me.ParseInstrumentId(MyBase.Identity)
                Return MyBase.Identity
            End Get
            Set(ByVal value As String)
                MyBase.Identity = value
                Me.ParseInstrumentId(MyBase.Identity)
            End Set
        End Property

#End Region

#Region " INSTRUMENT PROPERTIES "

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public Shared ReadOnly Property HadError() As Boolean
            Get
                Return False
            End Get
        End Property

        ''' <summary>
        ''' Returns the error queue and event status register (ESR) report.
        ''' </summary>
        Public ReadOnly Property DeviceErrors() As String Implements IDevice.DeviceErrors
            Get
                Return "NOT IMPLEMENTED YET"
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If id Is Nothing Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            Me._manufacturerName = idItems(0)

            ' model: MODEL 2420
            Me._model = idItems(1)

            ' Serial Number: 0669977
            Me._serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            Me._firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(Me._firmwareRevision)

        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & isr.Core.IIf(Of String)(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & isr.Core.IIf(Of String)(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As NationalInstruments.VisaNS.SerialSessionEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="NationalInstruments.VisaNS.SerialSessionEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As NationalInstruments.VisaNS.SerialSessionEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As NationalInstruments.VisaNS.SerialSessionEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return Me._manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return Me._model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return Me._serialNumber
            End Get
        End Property

#End Region

#Region " SERIAL SESSION METHODS AND PROPERTIES "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until ((Me.Session.ReadStatusByte And StatusByteFlags.MessageAvailable) = StatusByteFlags.MessageAvailable) OrElse timedOut

                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me.Session.ReadString()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Verbose, "FLUSHED",
                                                  "Discureded unread data '{0}'.", isr.Core.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(flushed))
                        End If

                    Else

                        Me.Session.ReadString()

                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.DiscardUnreadData

            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.FlushRead

            Try

                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION FLUSH READ",
                                      "Exception occurred flushing read buffer. VISA status={2}. Details: {0}.", Me.LastVisaStatusDetails, ex)

            End Try

        End Function

        Private _lastCommandTime As DateTime = DateTime.Now
        ''' <summary>Gets or sets the time of the last command for delay purposes.
        ''' </summary>
        Public ReadOnly Property LastCommandTime() As DateTime
            Get
                Return Me._lastCommandTime
            End Get
        End Property

        Private _minimumCommandInterval As Int32 = 200
        ''' <summary>Gets or sets the time between commands in milliseconds.
        ''' </summary>
        Public Property MinimumCommandInterval() As Int32
            Get
                Return Me._minimumCommandInterval
            End Get
            Set(ByVal Value As Int32)
                Me._minimumCommandInterval = Value
            End Set
        End Property

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval()
            AwaitCommandInterval(Me._minimumCommandInterval)
        End Sub

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval(ByVal milliseconds As Int32)

            Do While DateTime.Compare(DateTime.Now, Me._lastCommandTime.AddMilliseconds(milliseconds)) < 0
                '        Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(2)
            Loop
            Me._lastCommandTime = DateTime.Now
        End Sub

        ''' <summary>Sends a query and gets back the result.</summary>
        ''' <param name="output">The main command.</param>
        Public Function QueryTerminate(ByVal output As String) As String
            If Me.Session IsNot Nothing Then
                output &= Convert.ToChar(Me.Session.TerminationCharacter)
            End If
            Return Me.QueryTrimNewLine(output)
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimNewLine(ByVal question As String) As String
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBased.Session.QueryTrimNewLine(Me.Session, question)
            End If
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IDevice.QueryTrimEnd
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBased.Session.QueryTrimEnd(Me.Session, question)
            End If
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IDevice.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.Session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBased.Session.QueryTrimEnd(Me.Session, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Function

        ''' <summary>
        ''' Reads and returns a string to the termination character and return the string
        ''' not including the termination character.
        ''' </summary><returns></returns>
        Public Function ReadLineTrimEnd() As String Implements IDevice.ReadLineTrimEnd
            Return MessageBased.Session.ReadStringTrimEnd(Me.Session)
        End Function

        ''' <summary>Writes output and terminate.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub WriteTerminate(ByVal output As String)
            If Me.Session IsNot Nothing Then
                output &= Convert.ToChar(Me.Session.TerminationCharacter)
                Me.Session.Write(output)
            End If
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If Me.Session IsNot Nothing Then
                Me.Session.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If Me.Session IsNot Nothing Then
                Me.Session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.LastStatus < 0
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.LastStatus > 0
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IDevice.LastVisaStatus '  NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me.Session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me.Session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IDevice.LastVisaStatusDetails
            Get
                Return VisaManager.BuildVisaStatusDetails(CType(Me.Session.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
            End Get
        End Property

#End Region

#Region " ERROR MANAGEMENT "

        Private _errorQueueCache As String
        Public Property ErrorQueueCache() As String
            Get
                Return Me._errorQueueCache
            End Get
            Set(ByVal value As String)
                Me._errorQueueCache = value
            End Set
        End Property

        Public Function ReadErrorQueue() As String Implements IDevice.ReadErrorQueue
            Return Me._errorQueueCache
        End Function

        Private _lastErrorCache As String
        Public Property LastErrorCache() As String
            Get
                Return Me._lastErrorCache
            End Get
            Set(ByVal value As String)
                Me._lastErrorCache = value
            End Set
        End Property

        Public Function ReadLastError() As String Implements IDevice.ReadLastError
            Return Me._lastErrorCache
        End Function

#End Region

    End Class

End Namespace

