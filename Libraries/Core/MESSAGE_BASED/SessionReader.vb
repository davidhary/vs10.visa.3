Imports NationalInstruments

Namespace MessageBased

    ''' <summary>Extends the VISA message based reader.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/04/08" by="David" revision="2.0.1841.x">
    ''' Created
    ''' </history> 
    Public Class SessionReader

        Inherits VisaNS.MessageBasedSessionReader

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="session">Specifies a reference to a valid 
        ''' <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        Public Sub New(ByVal session As NationalInstruments.VisaNS.IMessageBasedSession,
                       ByVal terminationCharacter As Byte)

            ' instantiate the base class
            MyBase.New(session)
            Me._terminationCharacter = terminationCharacter
            Me._defaultStringSizes = New System.Collections.Generic.Stack(Of Integer)
        End Sub

#End Region

#Region " PARSERS "

        ''' <summary>
        ''' Returns true if the value matches the true value.
        ''' </summary>
        ''' <param name="value">Value read from the instrument.</param>
        ''' <param name="trueValue">The value expected for a true reply</param>
        Public Shared Function ParseBoolean(ByVal value As String, ByVal trueValue As String) As Nullable(Of Boolean)
            Return Not String.IsNullOrWhiteSpace(value) AndAlso value.StartsWith(trueValue, StringComparison.Ordinal)
        End Function

        ''' <summary>
        ''' Returns a boolean or no value.
        ''' </summary>
        ''' <param name="value">Value read from the instrument.</param>
        Public Shared Function ParseBoolean(ByVal value As String) As Nullable(Of Boolean)
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Boolean)
            ElseIf value.StartsWith("0", StringComparison.Ordinal) Then
                Return False
            ElseIf value.StartsWith("1", StringComparison.Ordinal) Then
                Return True
            ElseIf value.StartsWith("OFF", StringComparison.OrdinalIgnoreCase) Then
                Return False
            ElseIf value.StartsWith("ON", StringComparison.OrdinalIgnoreCase) Then
                Return True
            ElseIf value.StartsWith("TRUE", StringComparison.OrdinalIgnoreCase) Then
                Return True
            ElseIf value.StartsWith("FALSE", StringComparison.OrdinalIgnoreCase) Then
                Return False
            ElseIf value.StartsWith("T", StringComparison.OrdinalIgnoreCase) Then
                Return True
            ElseIf value.StartsWith("F", StringComparison.OrdinalIgnoreCase) Then
                Return False
            Else
                Return New Nullable(Of Boolean)
            End If
        End Function

        ''' <summary>
        ''' Returns a double or no value.
        ''' </summary>
        Public Shared Function ParseDouble(ByVal value As String) As Nullable(Of Double)

            Dim numericValue As Double

            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Double)
            ElseIf Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return New Nullable(Of Double)
            End If

        End Function

        ''' <summary>
        ''' Returns an Int32 or no value.
        ''' </summary>
        Public Shared Function ParseInt32(ByVal value As String) As Nullable(Of Int32)
            Dim numericValue As Int32
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Int32)
            ElseIf Int32.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return New Nullable(Of Int32)
            End If

        End Function

        ''' <summary>
        ''' Returns an Int32 or no value.
        ''' </summary>
        Public Shared Function ParseInfInt32(ByVal value As String) As Nullable(Of Int32)

            Dim numericValue As Int32
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Int32)
            ElseIf Int32.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return Int32.MaxValue
            End If

        End Function

        ''' <summary>
        ''' Returns an Integer or no value.
        ''' </summary>
        Public Shared Function ParseInteger(ByVal value As String) As Nullable(Of Integer)

            Dim numericValue As Integer
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Integer)
            ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return New Nullable(Of Integer)
            End If

        End Function

        ''' <summary>
        ''' Returns an Integer or no value.
        ''' </summary>
        Public Shared Function ParseInfInteger(ByVal value As String) As Nullable(Of Integer)

            Dim numericValue As Integer
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of Integer)
            ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return Integer.MaxValue
            End If

        End Function

#End Region

#Region " READ "

        Private _terminationCharacter As Byte
        ''' <summary>
        ''' Gets or sets the termination character.
        ''' </summary>
        Public Property TerminationCharacter() As Byte
            Get
                Return Me._terminationCharacter
            End Get
            Set(ByVal value As Byte)
                Me._terminationCharacter = value
            End Set
        End Property

        ''' <summary>Gets or sets the last message that was received from the instrument.
        ''' Note that not all messages get recorded.
        ''' </summary>
        Private _receiveBuffer As String

        ''' <summary>Gets the last message that was received from the instrument.
        ''' Note that not all messages get recorded.
        ''' </summary>
        Public Property ReceiveBuffer() As String
            Get
                Return Me._receiveBuffer
            End Get
            Friend Set(ByVal value As String)
                Me._receiveBuffer = value
            End Set
        End Property

        ''' <summary>Reads a string of characters from the instrument until a line feed character is read and returns
        ''' the string.</summary>
        Public Shadows Function ReadLine() As String
#If DIAGNOSE Then
    Trace.CorrelationManager.StartLogicalOperation("READ")
#End If
            Me._receiveBuffer = MyBase.ReadLine()
#If DIAGNOSE Then
    My.Application.Log.WriteEntry(Me._receiveBuffer.TrimEnd(New Char() {Convert.ToChar(TerminationCharacter)}), TraceEventType.Verbose)
    Trace.CorrelationManager.StopLogicalOperation()
#End If
            Return Me._receiveBuffer
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        Public Overloads Function ReadLineTrimEnd(ByVal terminationCharacters As Char()) As String
            Me.ReadLine()
            Return Me._receiveBuffer.TrimEnd(terminationCharacters)
        End Function

        ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
        Public Overloads Function ReadLineTrimEnd(ByVal terminationCharacter As Byte) As String
            Return Me.ReadLineTrimEnd(New Char() {Convert.ToChar(terminationCharacter)})
        End Function

        ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
        Public Overloads Function ReadLineTrimEnd() As String
            Return Me.ReadLineTrimEnd(Me._terminationCharacter)
        End Function

        ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
        Public Overloads Function ReadLineTrimNewLine() As String
            Return Me.ReadLineTrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
        End Function

        ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
        Public Overloads Function ReadLineTrimLinefeed() As String
            Return Me.ReadLineTrimEnd(10)
        End Function

#End Region

#Region " BUFFER SIZE MANAGEMENT "

        ''' <summary>Gets or sets the reference to the stack of timeouts.
        ''' </summary>
        Private _defaultStringSizes As System.Collections.Generic.Stack(Of Integer)

        ''' <summary>
        ''' Restores the previous default string size.
        ''' </summary>
        Public Sub RestoreDefaultStringSize()
            ' MyBase..Timeout = Me._bufferSizes.Pop
            MyBase.DefaultStringSize = Me._defaultStringSizes.Pop
        End Sub

        ''' <summary>
        ''' Stores the current default string size and sets a new size.
        ''' </summary>
        ''' <param name="newSize">Specifies the new string size</param>
        Public Sub StoreDefaultStringSize(ByVal newSize As Integer)
            Me._defaultStringSizes.Push(MyBase.DefaultStringSize)
            MyBase.DefaultStringSize = newSize
        End Sub

#End Region

    End Class

End Namespace
