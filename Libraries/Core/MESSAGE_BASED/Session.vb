Imports NationalInstruments

Namespace MessageBased

    ''' <summary>
    ''' Extends the VISA message based session.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history> 
    Public Class Session

        Inherits VisaNS.MessageBasedSession

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal resourceName As String)

            ' instantiate the base class
            MyBase.New(resourceName)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up managed and unmanaged resources.</summary>
        ''' <param name="disposing">If true, the method has been called directly or 
        '''   indirectly by a user's code. Managed and unmanaged resources can be disposed.
        '''   If disposing equals false, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects. Only unmanaged resources can be disposed.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " OPEN "

        ''' <summary>Returns an open message based session.  Message-based sessions access VISA 
        '''   resources in an interface-independent manner. Such sessions can communicate with an 
        '''   instrument over a GPIB interface or serial interface.</summary>
        ''' <returns>An open message-based VISA session.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function OpenSession(ByVal resourceName As String) As Session
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New Session(resourceName)
                '      Return CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), MessageBasedSession)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("Resource selected must be a message-based session", ex)
            End Try
        End Function

#End Region

#Region " QUERY "

        ''' <summary>Queries the instrument and returns a Boolean value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="question">The query to use.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function QueryBoolean(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Boolean)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim queryResult As String = visaSession.Query(question)
            Return MessageBased.SessionReader.ParseBoolean(queryResult)
        End Function

        ''' <summary>Queries the instrument and returns a Boolean value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            Return Session.QueryBoolean(Me, question)
        End Function

        ''' <summary>Queries the instrument and returns a double value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="question">The query to use.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function QueryDouble(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Double)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim queryResult As String = Session.QueryTrimEnd(visaSession, question)
            Return SessionReader.ParseDouble(queryResult)
        End Function

        ''' <summary>Queries the instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            Return Session.QueryDouble(Me, question)
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="question">The query to use.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function QueryInt32(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Int32)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim queryResult As String = Session.QueryTrimEnd(visaSession, question)
            Return SessionReader.ParseInt32(queryResult)
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            Return Session.QueryInt32(Me, question)
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="question">The query to use.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function QueryTrimEnd(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal question As String) As String
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return visaSession.Query(question).TrimEnd(Convert.ToChar(visaSession.TerminationCharacter))
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Overloads Function QueryTrimEnd(ByVal question As String) As String
            Return Session.QueryTrimEnd(Me, question)
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="question">The query to use.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function QueryTrimNewLine(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal question As String) As String
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return visaSession.Query(question).TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Overloads Function QueryTrimNewline(ByVal question As String) As String
            Return Session.QueryTrimNewLine(Me, question)
        End Function

#End Region

#Region " READ "

        ''' <summary>Queries the instrument and returns a Boolean value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadBoolean(ByVal visaSession As VisaNS.IMessageBasedSession) As Nullable(Of Boolean)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim readResult As String = Session.ReadStringTrimEnd(visaSession)
            Return SessionReader.ParseBoolean(readResult)
        End Function

        ''' <summary>Reads a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            Return Session.ReadBoolean(Me)
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadDouble(ByVal visaSession As VisaNS.IMessageBasedSession) As Nullable(Of Double)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim readResult As String = Session.ReadStringTrimEnd(visaSession)
            Return SessionReader.ParseDouble(readResult)
        End Function

        ''' <summary>Reads a Double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            Return Session.ReadDouble(Me)
        End Function

        ''' <summary>Queries the instrument and returns an Int32 value.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadInt32(ByVal visaSession As VisaNS.IMessageBasedSession) As Nullable(Of Int32)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim readResult As String = Session.ReadStringTrimEnd(visaSession)
            Return SessionReader.ParseInt32(readResult)
        End Function

        ''' <summary>Reads an integer value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            Return Session.ReadInt32(Me)
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function ReadStringTrimEnd(ByVal visaSession As VisaNS.IMessageBasedSession) As String
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return visaSession.ReadString().TrimEnd(Convert.ToChar(visaSession.TerminationCharacter))
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination new line
        '''   characters.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function ReadStringNewLine(ByVal visaSession As VisaNS.IMessageBasedSession) As String
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return visaSession.ReadString().TrimEnd(Convert.ToChar(13)).TrimEnd(Convert.ToChar(10))
        End Function

        ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
        Public Overloads Function ReadString() As String
            Return Session.ReadStringTrimEnd(Me)
        End Function

#End Region

#Region " WRITE "

        ''' <summary>Writes to the instrument.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="output">The main command.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Sub Write(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal output As String)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            visaSession.Write(output)
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub WriteOnOff(ByVal visaSession As VisaNS.IMessageBasedSession, ByVal output As String, ByVal isOn As Boolean)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Session.Write(visaSession, output & isr.Core.IIf(Of String)(isOn, " ON", " OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub WriteOneZero(ByVal visaSession As VisaNS.IMessageBasedSession,
            ByVal output As String, ByVal one As Boolean)
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Session.Write(visaSession, output & isr.Core.IIf(Of String)(one, " 1", " 0"))
        End Sub

#End Region

#Region " WRITE "

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Session.WriteOnOff(Me, output, isOn)
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Session.WriteOneZero(Me, output, one)
        End Sub

#End Region

    End Class

End Namespace