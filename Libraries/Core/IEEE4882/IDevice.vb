﻿Namespace IEEE4882
    ''' <summary>
    ''' Defines the contract implemented by VISA instruments for IEEE488.2 interfaces.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/21/2011" by="David" revision="3.0.4038.x">
    ''' Created
    ''' </history>
    Public Interface IDevice
        Inherits isr.Visa.IDevice

#Region " REGISTERS  "

#Region " MEASUREMENTS "

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Function ReadMeasurementEventStatus() As Integer

#End Region

#Region " OPERATION "

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Function ReadOperationEventStatus() As Integer

#End Region

#Region " QUESTIONABLE "

        ''' <summary>
        ''' Reads the questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Function ReadQuestionableEventStatus() As Integer

#End Region

#Region " STANDARD EVENTS "

        ''' <summary>
        ''' Gets or sets the standard event status.
        ''' </summary>
        ''' <value>
        ''' The standard event status.
        ''' </value>
        Property StandardEventStatus() As isr.Visa.IEEE4882.StandardEvents

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Function ReadStandardEventStatus() As isr.Visa.IEEE4882.StandardEvents

#End Region

#End Region

    End Interface

End Namespace
