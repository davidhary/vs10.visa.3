Imports NationalInstruments

Namespace IEEE4882

    ''' <summary>Extends the message based writer  to support IEEE488.2.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/04/08" by="David" revision="2.0.1841.x">
    ''' Created
    ''' </history> 
    Public Class SessionWriter

        Inherits MessageBased.SessionWriter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="session">Specifies a reference to a valid 
        ''' <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        Public Sub New(ByVal session As NationalInstruments.VisaNS.IMessageBasedSession)

            ' instantiate the base class
            MyBase.New(session)

        End Sub

#End Region

#Region " IRESETTABLE "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <param name="writer">A reference to an open
        '''   <see cref="isr.Visa.MessageBased.SessionWriter">writer</see>.</param>
        Public Shared Function ClearExecutionState(ByVal writer As MessageBased.SessionWriter) As Boolean

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If

            ' Clear the device status.  This will set all the
            ' standard subsystem properties.
            writer.WriteLine(Scpi.Syntax.ClearExecutionStateCommand)

            Return True

        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        Public Function ClearExecutionState() As Boolean

            ' Clear the device status.  This will set all the standard subsystem properties.
            Me.WriteLine(Scpi.Syntax.ClearExecutionStateCommand)
            Return True

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Function ResetKnownState() As Boolean
            Me.WriteLine(Scpi.Syntax.ResetKnownStateCommand)
            Return True
        End Function

#End Region

    End Class

End Namespace