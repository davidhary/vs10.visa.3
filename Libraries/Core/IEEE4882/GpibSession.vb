Imports NationalInstruments
Imports isr.Core.EnumExtensions

Namespace IEEE4882

    ''' <summary>Extends the GPIB message based session to supporting IEEE4882.2.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class GpibSession

        Inherits Gpib.GpibSession
        Implements isr.Core.IServicingRequest

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Initializes a new instance of the <see cref="GpibSession" /> class.
        ''' </summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        ''' <param name="connectTimeout">The connect timeout.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal resourceName As String, ByVal connectTimeout As Integer)

            ' instantiate the base class
            ' Rev 4.1 and 5.0 of VISA did not support this call and could not verify the 
            ' resource.  
            MyBase.New(resourceName, connectTimeout)

            Me.ErrorAvailableBits = ServiceRequests.ErrorAvailable
            Me.MessageAvailableBits = ServiceRequests.MessageAvailable

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " OPEN "

        ''' <summary>Returns an open message based session.</summary>
        ''' <returns>An open Gpib VISA session.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function OpenSession(ByVal resourceName As String, ByVal connectTimeout As Integer) As GpibSession
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New GpibSession(resourceName, connectTimeout)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("Resource selected must be a Gpib session", ex)
            End Try
        End Function

#End Region

#Region " IEEE 488.2:  ERROR MANAGEMENT "

        ''' <summary>Returns a detailed report of the event status register (ESR) byte.
        ''' </summary>
        ''' <param name="esrValue">Specifies the value that was read from the status register.</param>
        Public Shared Function BuildEsrReport(ByVal esrValue As isr.Visa.IEEE4882.StandardEvents, ByVal delimiter As String) As String

            If String.IsNullOrWhiteSpace(delimiter) Then
                delimiter = "; "
            End If

            Dim message As New System.Text.StringBuilder

            For Each value As isr.Visa.IEEE4882.StandardEvents In [Enum].GetValues(GetType(isr.Visa.IEEE4882.StandardEvents))
                If value <> IEEE4882.StandardEvents.None AndAlso value <> IEEE4882.StandardEvents.All AndAlso (value And esrValue) <> 0 Then
                    If message.Length > 0 Then
                        message.Append(delimiter)
                    End If
                    message.Append(value.Description)
                End If
            Next

            If message.Length > 0 Then
                message.Append(".")
                message.Insert(0, "The device standard status register reported: " & Environment.NewLine)
            End If
            Return message.ToString

        End Function

        ''' <summary>
        ''' Gets a report of the error stored in the cached error queue event status register (ESR) information.
        ''' </summary>
        Public Overrides ReadOnly Property DeviceErrors() As String
            Get
                Dim message As New System.Text.StringBuilder
                message.AppendLine(MyBase.DeviceErrors)
                Dim report As String
                report = GpibSession.BuildEsrReport(Me.ReadStandardEventStatus, "; ")
                If Not String.IsNullOrWhiteSpace(report) Then
                    If message.Length > 0 Then
                        message.AppendLine()
                    End If
                    message.Append(report)
                End If
                Return message.ToString
            End Get
        End Property

        ''' <summary>Gets or sets bits that would be set for detecting if an error
        ''' is available.</summary>
        Public Shadows Property ErrorAvailableBits() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return CType(MyBase.ErrorAvailableBits, isr.Visa.IEEE4882.ServiceRequests)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                MyBase.ErrorAvailableBits = Value
            End Set
        End Property

        ''' <summary>
        ''' Returns true if the device had an error include standard query or execution errors.
        ''' Clears the error queue and status by reading the error queue.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if error occurred.</returns>
        Public Overridable Function HasDeviceError(ByVal flushReadFirst As Boolean) As Boolean

            If Not Me.IsErrorAvailable AndAlso (Me.CachedStatusByte And Visa.IEEE4882.ServiceRequests.StandardEvent) = 0 Then
                Return False
            End If

            If flushReadFirst Then
                Me.DiscardUnreadData()
            End If

            '  In order to keep compatibility, we clear the error here by reading the error status.
            Me.ReadErrorQueue()

            ' check for query and other errors reported by the standard event register
            Dim standardErrorBits As isr.Visa.IEEE4882.StandardEvents = isr.Visa.IEEE4882.StandardEvents.CommandError Or
                isr.Visa.IEEE4882.StandardEvents.DeviceDependentError Or
                isr.Visa.IEEE4882.StandardEvents.ExecutionError Or isr.Visa.IEEE4882.StandardEvents.QueryError

            Return Me.HadError OrElse Me.HasStandardError(standardErrorBits)

        End Function

#End Region

#Region " IEEE 488.2:  MAV "

        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Public Shadows Property MessageAvailableBits() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return CType(MyBase.MessageAvailableBits, isr.Visa.IEEE4882.ServiceRequests)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                MyBase.MessageAvailableBits = Value
            End Set
        End Property

#End Region

#Region " IEEE 488.2:  REGISTERS "

#Region " SERVICE REQUESTS / STATUS REGISTER "

        ''' <summary>
        ''' Gets the last read status byte.
        ''' </summary>
        Public Overloads Property CachedStatusByte() As ServiceRequests
            Get
                Return CType(MyBase.CachedStatusByte, ServiceRequests)
            End Get
            Set(ByVal value As ServiceRequests)
                MyBase.CachedStatusByte = value
            End Set
        End Property

        ''' <summary>Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that wil issue an SRQ
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Overloads Function EnableServiceRequest(ByVal standardEventEnableBits As isr.Visa.IEEE4882.StandardEvents,
                                             ByVal serviceRequestEnableBits As isr.Visa.IEEE4882.ServiceRequests) As Boolean
            Return MyBase.EnableServiceRequest(standardEventEnableBits, serviceRequestEnableBits)
        End Function

        ''' <summary>Returns the service request register status byte.</summary>
        Public Shadows Function ReadStatusByte() As ServiceRequests
            Return CType(MyBase.ReadStatusByte(), ServiceRequests)
        End Function

        ''' <summary>Reads the service request event request.</summary>
        ''' <returns>The <see cref="ServiceRequests">mask</see>
        '''    to use for enabling the events.</returns>
        Public Shadows Function ReadServiceRequestEventEnable() As ServiceRequests
            Return CType(MyBase.ReadServiceRequestEventEnable(), ServiceRequests)
        End Function

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public Shadows Function ReadServiceRequestEventStatus() As ServiceRequests
            Return CType(MyBase.ReadServiceRequestEventStatus(), ServiceRequests)
        End Function

        ''' <summary>
        ''' Gets or sets the ServiceRequest event enable.
        ''' </summary>
        ''' <value>
        ''' The ServiceRequest event enable.
        ''' </value>
        Public Overloads Property ServiceRequestEventEnable() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return CType(MyBase.ServiceRequestEventEnable, isr.Visa.IEEE4882.ServiceRequests)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                MyBase.ServiceRequestEventEnable = Value
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the ServiceRequest event Status.
        ''' </summary>
        ''' <value>
        ''' The ServiceRequest event Status.
        ''' </value>
        Public Overloads Property ServiceRequestEventStatus() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return CType(MyBase.ServiceRequestEventStatus, isr.Visa.IEEE4882.ServiceRequests)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                MyBase.ServiceRequestEventStatus = Value
            End Set
        End Property

        ''' <summary>Programs the service request event request.</summary>
        ''' <remarks>When set clears all service registers.</remarks>
        Public Overloads Function WriteServiceRequestEventEnable(ByVal value As ServiceRequests) As Boolean
            MyBase.WriteServiceRequestEventEnable(value)
            Return Me.IsLastVisaOperationSuccess
        End Function

#End Region

#Region " STANDARD EVENTS "

        ''' <summary>Returns a True is the service request register 
        ''' status byte reports error available.</summary>
        ''' <param name="standardErrorBits">Specifies the standard device register error bits.</param>
        Public Overloads Function HasStandardError(ByVal standardErrorBits As StandardEvents) As Boolean
            Return (Me.ReadStandardEventStatus() And standardErrorBits) <> 0
        End Function

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public Overloads Function ReadStandardEventStatus() As StandardEvents
            Return CType(MyBase.ReadStandardEventStatus(), StandardEvents)
        End Function

        ''' <summary>Read the standard event request.</summary>
        ''' <returns>The <see cref=" StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Overloads Function ReadStandardEventEnable() As StandardEvents
            Return CType(MyBase.ReadStandardEventEnable(), StandardEvents)
        End Function

        ''' <summary>
        ''' Gets or sets the standard event enable.
        ''' </summary>
        ''' <value>
        ''' The standard event enable.
        ''' </value>
        Public Overloads Property StandardEventEnable() As isr.Visa.IEEE4882.StandardEvents
            Get
                Return CType(MyBase.StandardEventEnable, isr.Visa.IEEE4882.StandardEvents)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.StandardEvents)
                MyBase.StandardEventEnable = Value
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the standard event Status.
        ''' </summary>
        ''' <value>
        ''' The standard event Status.
        ''' </value>
        Public Overloads Property StandardEventStatus() As isr.Visa.IEEE4882.StandardEvents
            Get
                Return CType(MyBase.StandardEventStatus, isr.Visa.IEEE4882.StandardEvents)
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.StandardEvents)
                MyBase.StandardEventStatus = Value
            End Set
        End Property

        ''' <summary>Programs the standard event request.</summary>
        Public Overloads Function WriteStandardEventEnable(ByVal value As StandardEvents) As Boolean
            MyBase.WriteStandardEventEnable(value)
            Return Me.IsLastVisaOperationSuccess
        End Function

#End Region

#End Region

    End Class

#Region " IEEE 488.2 EVENT FLAGS "

    ''' <summary>Gets or sets the status byte bits of the service request register.</summary>
    ''' <remarks>Extends the VISA 
    '''   <see cref="NationalInstruments.VisaNS.StatusByteFlags.EventStatusRegister">event status flags</see>.
    ''' Because the VISA status returns a signed byte, we need to use Int16.
    ''' Enumerates the Status Byte Register Bits.
    ''' Use STB? or status.request_event to read this register.
    ''' Use *SRE or status.request_enable to enable these services.
    ''' This attribute is used to read the status byte, which is returned as a
    ''' numeric value. The binary equivalent of the returned value indicates which
    ''' register bits are set.
    ''' </remarks>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    <System.Flags()> Public Enum ServiceRequests
        <ComponentModel.Description("None")> None = 0
        ''' <summary>
        ''' Bit B0, Measurement Summary Bit (MSB). Set summary bit indicates
        ''' that an enabled measurement event has occurred.
        ''' </summary>
        <ComponentModel.Description("Measurement Event (MSB)")> MeasurementEvent = &H1
        ''' <summary>
        ''' Bit B1, System Summary Bit (SSB). Set summary bit indicates
        ''' that an enabled system event has occurred.
        ''' </summary>
        <ComponentModel.Description("System Event (SSB)")> SystemEvent = &H2
        ''' <summary>
        ''' Bit B2, Error Available (EAV). Set summary bit indicates that
        ''' an error or status message is present in the Error Queue.
        ''' </summary>
        <ComponentModel.Description("Error Available (EAV)")> ErrorAvailable = &H4
        ''' <summary>
        ''' Bit B3, Questionable Summary Bit (QSB). Set summary bit indicates
        ''' that an enabled questionable event has occurred.
        ''' </summary>
        <ComponentModel.Description("Questionable Event (QSB)")> QuestionableEvent = &H8
        ''' <summary>
        ''' Bit B4 (16), Message Available (MAV). Set summary bit indicates that
        ''' a response message is present in the Output Queue.
        ''' </summary>
        <ComponentModel.Description("Message Available (MAV)")> MessageAvailable = &H10
        ''' <summary>Bit B5, Event Summary Bit (ESB). Set summary bit indicates 
        ''' that an enabled standard event has occurred.
        ''' </summary>
        <ComponentModel.Description("Standard Event (ESB)")> StandardEvent = &H20 ' (32) ESB
        ''' <summary>
        ''' Bit B6 (64), Request Service (RQS)/Master Summary Status (MSS).
        ''' Set bit indicates that an enabled summary bit of the Status Byte Register
        ''' is set. Depending on how it is used, Bit B6 of the Status Byte Register
        ''' is either the Request for Service (RQS) bit or the Master Summary Status
        ''' (MSS) bit: When using the GPIB serial poll sequence of the unit to obtain
        ''' the status byte (serial poll byte), B6 is the RQS bit. When using
        ''' status.condition or the *STB? common command to read the status byte,
        ''' B6 is the MSS bit.
        ''' </summary>
        <ComponentModel.Description("Request Service (RQS)/Master Summary Status (MSS)")> RequestingService = &H40
        ''' <summary>
        ''' Bit B7 (128), Operation Summary (OSB). Set summary bit indicates that
        ''' an enabled operation event has occurred.
        ''' </summary>
        <ComponentModel.Description("Operation Event (OSB)")> OperationEvent = &H80
        ''' <summary>
        ''' Includes all bits.
        ''' </summary>
        <ComponentModel.Description("All")> All = &HFF ' 255
        ''' <summary>
        ''' Unknown value due to, for example, error trying to get value from the
        ''' instrument.
        ''' </summary>
        <ComponentModel.Description("Unknown")> Unknown = &H100
    End Enum

    ''' <summary>
    ''' Enumerates the status byte flags of the standard event register.
    ''' </summary>
    ''' <remarks>
    ''' Enumerates the Standard Event Status Register Bits.
    ''' Read this information using ESR? or status.standard.event.
    ''' Use *ESE or status.standard.enable or event status enable 
    ''' to enable this register.
    ''' These values are used when reading or writing to the
    ''' standard event registers. Reading a status register returns a value.
    ''' The binary equivalent of the returned value indicates which register bits
    ''' are set. The least significant bit of the binary number is bit 0, and
    ''' the most significant bit is bit 15. For example, assume value 9 is
    ''' returned for the enable register. The binary equivalent is
    ''' 0000000000001001. This value indicates that bit 0 (OPC) and bit 3 (DDE)
    ''' are set.
    ''' </remarks>
    <System.Flags()> Public Enum StandardEvents
        <ComponentModel.Description("None")> None = 0
        ''' <summary>
        ''' Bit B0, Operation Complete (OPC). Set bit indicates that all
        ''' pending selected device operations are completed and the unit is ready to
        ''' accept new commands. The bit is set in response to an *OPC command.
        ''' The ICL function opc() can be used in place of the *OPC command.
        ''' </summary>
        <ComponentModel.Description("Operation Complete (OPC)")> OperationComplete = 1
        ''' <summary>
        ''' Bit B1, Request Control (RQC). Set bit indicates that....
        ''' </summary>
        <ComponentModel.Description("Request Control (RQC)")> RequestControl = &H2
        ''' <summary>
        ''' Bit B2, Query Error (QYE). Set bit indicates that you attempted
        ''' to read data from an empty Output Queue.
        ''' </summary>
        <ComponentModel.Description("Query Error (QYE)")> QueryError = &H4
        ''' <summary>
        ''' Bit B3, Device-Dependent Error (DDE). Set bit indicates that an
        ''' instrument operation did not execute properly due to some internal
        ''' condition.
        ''' </summary>
        <ComponentModel.Description("Device Dependent Error (DDE)")> DeviceDependentError = &H8
        ''' <summary>
        ''' Bit B4 (16), Execution Error (EXE). Set bit indicates that the unit
        ''' detected an error while trying to execute a command. 
        ''' This is used by Quatech to report No Contact.
        ''' </summary>
        <ComponentModel.Description("Execution Error (EXE)")> ExecutionError = &H10
        ''' <summary>
        ''' Bit B5 (32), Command Error (CME). Set bit indicates that a
        ''' command error has occurred. Command errors include:<p>
        ''' IEEE-488.2 syntax error � unit received a message that does not follow
        ''' the defined syntax of the IEEE-488.2 standard.  </p><p>
        ''' Semantic error � unit received a command that was misspelled or received
        ''' an optional IEEE-488.2 command that is not implemented.  </p><p>
        ''' The instrument received a Group Execute Trigger (GET) inside a program
        ''' message.  </p>
        ''' </summary>
        <ComponentModel.Description("Command Error (CME)")> CommandError = &H20
        ''' <summary>
        ''' Bit B6 (64), User Request (URQ). Set bit indicates that the LOCAL
        ''' key on the SourceMeter front panel was pressed.
        ''' </summary>
        <ComponentModel.Description("User Request (URQ)")> UserRequest = &H40
        ''' <summary>
        ''' Bit B7 (128), Power ON (PON). Set bit indicates that the instrument
        ''' has been turned off and turned back on since the last time this register
        ''' has been read.
        ''' </summary>
        <ComponentModel.Description("Power Toggled (PON)")> PowerToggled = &H80
        ''' <summary>
        ''' Unknown value due to, for example, error trying to get value from the
        ''' instrument.
        ''' </summary>
        <ComponentModel.Description("Unknown")> Unknown = &H100
        ''' <summary>Includes all bits.
        ''' </summary>
        <ComponentModel.Description("All")> All = &HFF ' 255
    End Enum

#End Region

End Namespace

