Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Imports NationalInstruments.VisaNS

Namespace IEEE4882

    ''' <summary>Provides a Core. for General GPIB instruments.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>  
    Public Class Device
        Inherits SimpleDevice
        Implements IDisposable, IDevice

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle)

            ' default 30 seconds reset and clear timeout.
            MyBase.ConnectTimeout = 30000

            ' instantiate event arguments
            Me._lastServiceEventArgs = New ServiceEventArgs(Me.Session)

            Me.IsDelegateErrorHandling = False

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                        If HandleErrorEvent IsNot Nothing Then
                            For Each d As [Delegate] In HandleErrorEvent.GetInvocationList
                                RemoveHandler HandleError, CType(d, Global.System.EventHandler(Of System.EventArgs))
                            Next
                        End If

                        If ServiceRequestEvent IsNot Nothing Then
                            For Each d As [Delegate] In ServiceRequestEvent.GetInvocationList
                                RemoveHandler ServiceRequest, CType(d, Global.System.EventHandler(Of ServiceEventArgs))
                            Next
                        End If

                        If Me.Session IsNot Nothing Then
                            If Me._requestRegistered Then
                                Me._requestRegistered = False
                                ' remove the service request handler
                                RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                        End If

                        If Me._interface IsNot Nothing Then
                            Me._interface.Dispose()
                            Me._interface = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                Finally

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Device).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If Me.IsConnected Then
                Return Me.IsConnected
            End If

            Try
                If MyBase.Connect(resourceName) Then

                    ' try reading the status byte to check if the instrument exists.
                    Me.OnMessageAvailable(TraceEventType.Verbose, "READING STATUS BYTE", "Reading {0} instrument status register.", resourceName)
                    Me.ReadDeviceStatusByte()

                Else

                    Me.OnMessageAvailable(TraceEventType.Verbose, "CONNECTING CANCELED", "Connection canceled.")
                    If Me.Session IsNot Nothing Then
                        If Me._requestRegistered Then
                            Me._requestRegistered = False
                            ' remove the service request handler
                            RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                        End If
                    End If

                End If

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CONNECTING", "Exception occurred connecting. Disconnecting. Details: {0}.", ex)
                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            Finally

            End Try

            Return Me.IsConnected

        End Function

        ''' <summary>Opens a Gpib VISA interface for the specified resource.</summary>
        ''' <param name="boardNumber">Specifies the GPIB resource board name.</param>
        ''' <param name="address">Specifies the GPIB resource address.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overridable Overloads Function Connect(ByVal boardNumber As Integer, ByVal address As Integer) As Boolean

            Return Me.Connect(GpibSession.BuildResourceName(boardNumber, address))

        End Function

        ''' <summary>Returns true if the instrument has a valid bus and primary address.
        ''' Before connecting, a valid bus must be established.
        ''' </summary>
        Public ReadOnly Property Connectable(ByVal resourceName As String) As Boolean
            Get
                If Me.IsConnected Then
                    Return False
                Else
                    Return Device.IsValidPrimaryAddress(GpibSession.ParsePrimaryAddress(resourceName))
                End If
            End Get
        End Property

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function Disconnect() As Boolean

            If Me.IsConnected Then
                Try
                    If Me.Session IsNot Nothing Then
                        If Me._requestRegistered Then
                            Me._requestRegistered = False
                            ' remove the service request handler
                            RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                        End If
                    End If
                    If Not MyBase.Disconnect Then
                        MyBase.Connect(Me.ResourceName)
                    End If

                Catch ex As Exception

                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", "Exception occurred disconnecting. Details: {0}", ex)

                Finally

                End Try
            End If
            Return Not Me.IsConnected

        End Function

#Region " CONNECTION EVENT HANDLERS "

        ''' <summary>
        ''' Implements full reset clear that was removed from the instrument connect event to 
        ''' be implement by each of the inheriting entities.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overridable Sub OnConnectedResetClear()

            Dim synopsis As String = "Connect Reset and Clear"
            Try

                Me.StoreTimeout(Me.ConnectTimeout)

                ' allow connection time to materialize
                Threading.Thread.Sleep(100)

                ' step through connection sequence allowing to proceed in DIAGNOSE mode

                ' flush read and write buffers
                Try
                    Me.StoreTimeout(1000)
                    Me._discardUnreadData(3, 100, True)
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Exception occurred flushing read buffer. Details: {1}. Ignored.", ex)
                Finally
                    Me.RestoreTimeout()
                End Try
                Me._flushWrite()

#If False Then
        ' 8/11/2008 old order
        ' clear execution state
        Me.ClearExecutionState()

        ' register the handler before enabling the event
        Me.RegisterServiceRequestHandler(Ieee4882.StandardEvents.UserRequest, Ieee4882.ServiceRequests.All)

        ' issue a selective device clear and total clear of the instrument.
        Me.ResetAndClear()

#Else
                ' 8/11/2008 new order. Clear execution test is part of reset and clear.

                ' clear execution state
                ' Me.ClearExecutionState()

                ' issue a selective device clear and total clear of the instrument.
                Me.ResetAndClear()

                ' register the handler before enabling the event
                Me.RegisterServiceRequestHandler(IEEE4882.StandardEvents.UserRequest, IEEE4882.ServiceRequests.All)

#End If

                ' restore session timeout 
                Me.RestoreTimeout()

            Catch

                Throw

            Finally


            End Try

        End Sub

#End Region

#End Region

#Region " I RESETTABLE "

        Private _resetKnownStateTimeout As Int32 = 10000
        ''' <summary>
        ''' Gets or sets the time out for resetting to known state.
        ''' </summary>
        Public Property ResetKnownStateTimeout() As Integer
            Get
                Return Me._resetKnownStateTimeout
            End Get
            Set(ByVal Value As Integer)
                Me._resetKnownStateTimeout = Value
            End Set
        End Property

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                If MyBase.ClearExecutionState Then

                    ' Clear the event arguments
                    Me._lastServiceEventArgs = New ServiceEventArgs(Me.Session)

                    ' clear the last read status byte
                    Me.ClearDeviceStatusByte()

                    ' clear the error queue for compatibility with how the source meters (2400) work.
                    Me.ClearErrorQueue()

                End If

            End If

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session. Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If Not Me.UsingDevices OrElse Me.Session Is Nothing Then
                Return True
            End If

            Me.StoreTimeout(Math.Max(Me.Session.Timeout, timeout))
            Dim outcome As Boolean = MyBase.ResetAndClear()
            Me.RestoreTimeout()
            Return outcome

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overrides Function ResetKnownState() As Boolean

            ' Minimum timeout for instrument reset
            Return Me.ResetKnownState(Me._resetKnownStateTimeout)

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Overloads Function ResetKnownState(ByVal timeout As Integer) As Boolean

            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then

                Try

                    ' set minimum timeout
                    Me.StoreTimeout(timeout)

                    MyBase.ResetKnownState()

                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT RESETTING KNOWN STATE",
                                              "Timeout occurred when resetting known state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                              "VISA Exception occurred when resetting known state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                          "Exception occurred when resetting known state. Details: {0}.", ex)
                Finally

                    ' restore timeout.
                    Me.RestoreTimeout()

                End Try

            End If
            Return True

        End Function

#End Region

#Region " ERROR MANAGEMENT "

        ''' <summary>Clears the error Queue.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Function ClearErrorQueue() As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Try
                    If Not Me.Session.ClearErrorQueue() Then
                        Me.OnMessageAvailable(TraceEventType.Error, "FAILED CLEARING ERROR QUEUE", "Failed clearing error queue. VISA status={0}.{1}{2}",
                                              Me.LastVisaStatus, Environment.NewLine,
                                              isr.Core.StackTraceParser.UserCallStack(4, 10))
                    End If
                    Return Me.IsLastVisaOperationSuccess
                Catch ex As Exception
                    Throw
                End Try
            End If
        End Function

        Public ReadOnly Property DeviceErrors() As String Implements IDevice.DeviceErrors
            Get
                If Me.Session Is Nothing Then
                    Return ""
                Else
                    Return Me.Session.DeviceErrors
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the error count.
        ''' </summary>
        ''' <returns>The error count or -1 is command is not supported.</returns>
        Public Function ReadErrorQueueCount() As Integer
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadErrorQueueCount
            Else
                Return 0
            End If
        End Function

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public ReadOnly Property HadError(ByVal flushReadFirst As Boolean) As Boolean
            Get
                If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                    ' new since 3346
                    Me._lastServiceEventArgs.HasError = Me._lastServiceEventArgs.HasError
                    Me._lastServiceEventArgs.HasError = Me.Session.HasDeviceError(flushReadFirst)
                Else
                    Return Me._lastServiceEventArgs.HasError
                End If
            End Get
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        ''' <param name="timeout">Time to wait before returning the error.</param>
        Private ReadOnly Property HadError(ByVal timeout As Integer, ByVal flushReadFirst As Boolean) As Boolean
            Get
                ' add a couple of milliseconds wait
                Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, timeout))
                Do
                    Threading.Thread.Sleep(Math.Min(10, timeout))
                Loop While Me._lastServiceEventArgs.ServicingRequest And (Date.Now <= endTime)
                Return Me.HadError(flushReadFirst) '  Me._lastServiceEventArgs.HasError
            End Get
        End Property

        Public Function ReadLastError() As String Implements IDevice.ReadLastError
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadLastError
            Else
                Return ""
            End If
        End Function

        Public Function ReadErrorQueue() As String Implements IDevice.ReadErrorQueue
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadErrorQueue
            Else
                Return ""
            End If
        End Function


#End Region

#Region " EVENTS AND HANDLERS "

        ''' <summary>Occurs when the status module needs to have
        ''' the calling routine read the error queue allowing for different
        ''' ways by which to read the error queue.
        ''' </summary>
        ''' <param name="e">Specifies reference to the 
        ''' <see cref="EventArgs">event arguments</see>.
        ''' </param>
        Public Event HandleError As EventHandler(Of EventArgs)

        ''' <summary>Raises the Handle Error event.</summary>
        Protected Overridable Sub OnHandleError()
            HandleErrorEvent.SafeInvoke(Me)
        End Sub

#End Region

#Region " IEEE488.2 COMMANDS "

        ''' <summary>
        ''' Asserts a hardware trigger. The device is addressed to listen, and then
        ''' the GPIB GET command is sent.
        ''' </summary>
        ''' <returns>Returns a long or invalid.
        ''' </returns>
        Public Overridable Function AssertTrigger() As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.AssertTrigger()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Allows controlling the instrument manually.
        ''' </summary>
        Public Function GoToLocal() As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.ControlRen(VisaNS.RenMode.Deassert)
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Gets or sets (for emulation) the completion status of the last operation.
        ''' </summary>
        Public Property OperationCompleted() As Boolean
            Get
                If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                    Me._lastServiceEventArgs.OperationCompleted = Me.Session.ReadOperationComplete.GetValueOrDefault(0) = 1
                Else
                    Me._lastServiceEventArgs.OperationCompleted = True
                End If
                Return Me._lastServiceEventArgs.OperationCompleted
            End Get
            Set(ByVal value As Boolean)
                Me._lastServiceEventArgs.OperationCompleted = value
            End Set
        End Property

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function ReadOperationComplete() As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadOperationComplete.GetValueOrDefault(0) = 1
            Else
                Return Me._lastServiceEventArgs.OperationCompleted
            End If
        End Function

        ''' <summary>
        ''' Waits until some event.
        ''' </summary>
        Public Function WaitOnEvent(ByVal timeout As Int32) As NationalInstruments.VisaNS.MessageBasedSessionEventArgs
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            Else
                Return CType(NationalInstruments.VisaNS.MessageBasedSessionEventArgs.Empty, NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            End If
        End Function

#End Region

#Region " IEEE488.2 REGISTER COMMANDS "

#Region " DEVICE STATUS BYTE / SERIAL POLL "

        ''' <summary>
        ''' Clears the last read device status byte.
        ''' </summary>
        Public Sub ClearDeviceStatusByte()
            Me.DeviceStatusByte = 0
        End Sub

        Private _DeviceStatusByte As isr.Visa.IEEE4882.ServiceRequests
        ''' <summary>
        ''' <summary>
        ''' Reads or sets the serial poll value.
        ''' Setting this value is allowed for emulation when using devices.
        ''' </summary>
        ''' </summary>
        Public Property DeviceStatusByte() As isr.Visa.IEEE4882.ServiceRequests
            Get
                If Me.Session Is Nothing Then
                    Return Me._DeviceStatusByte
                Else
                    Return Me.Session.CachedStatusByte
                End If
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                If Me.Session IsNot Nothing Then
                    Me.Session.CachedStatusByte = Value
                End If
                Me._DeviceStatusByte = Value
            End Set
        End Property

        ''' <summary>Returns a serial poll HEX caption.
        ''' </summary>
        Public Function DeviceStatusByteCaption() As String
            Return "0x" & CInt(Me.DeviceStatusByte).ToString("X", Globalization.CultureInfo.CurrentCulture).PadLeft(2, "0"c)
        End Function

        ''' <summary>
        ''' Reads the device status byte.
        ''' </summary>
        Public Function ReadDeviceStatusByte() As isr.Visa.IEEE4882.ServiceRequests
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.DeviceStatusByte = CType(Me.Session.ReadStatusByte(), isr.Visa.IEEE4882.ServiceRequests)
            End If
            Return Me.DeviceStatusByte
        End Function

#End Region

#Region " ESE "

        Private _standardEventEnable As Integer
        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadStandardEventEnable() As isr.Visa.IEEE4882.StandardEvents
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.ReadStandardEventEnable()
                Return Me.Session.StandardEventEnable()
            Else
                Return CType(Me._standardEventEnable, StandardEvents)
            End If
        End Function

        ''' <summary>
        ''' Writes to the standard event enable register.
        ''' </summary>
        Public Sub WriteStandardEventEnable(ByVal value As isr.Visa.IEEE4882.StandardEvents)
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.WriteStandardEventEnable(value)
            End If
            Me._standardEventEnable = value
        End Sub

#End Region

#Region " ESR "

        ''' <summary>Returns a detailed report of the event status register (ESR) byte.
        ''' </summary>
        ''' <param name="esrValue">Specifies the value that was read from the status register.</param>
        Public Shared Function BuildEsrReport(ByVal esrValue As isr.Visa.IEEE4882.StandardEvents, ByVal delimiter As String) As String

            If String.IsNullOrWhiteSpace(delimiter) Then
                delimiter = "; "
            End If

            Dim message As New System.Text.StringBuilder

            For Each value As isr.Visa.IEEE4882.StandardEvents In [Enum].GetValues(GetType(isr.Visa.IEEE4882.ServiceRequests))
                If value <> IEEE4882.StandardEvents.None AndAlso value <> IEEE4882.StandardEvents.All AndAlso ((value And esrValue) <> 0) Then
                    If message.Length > 0 Then
                        message.Append(delimiter)
                    End If
                    message.Append(isr.Core.EnumExtensions.Description(value))
                End If
            Next

            If message.Length > 0 Then
                message.Append(".")
                message.Insert(0, "The device standard status register reported: " & Environment.NewLine)
            End If
            Return message.ToString

        End Function

        Private _StandardEventStatus As Integer
        ''' <summary>Reads or sets the service request event status (ESR) data byte.
        ''' For instruments not supporting event status register (ESR) this can be set as the
        ''' default value returned when reading ESR.  ESR can be set also if not using devices.
        ''' </summary>
        Public Property StandardEventStatus() As isr.Visa.IEEE4882.StandardEvents Implements IDevice.StandardEventStatus
            Get
                If Me.Session IsNot Nothing Then
                    Return Me.Session.StandardEventStatus
                Else
                    Return CType(Me._StandardEventStatus, StandardEvents)
                End If
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.StandardEvents)
                If Me.Session IsNot Nothing Then
                    Me.Session.StandardEventStatus = Value
                End If
                Me._StandardEventStatus = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Public Function ReadStandardEventStatus() As isr.Visa.IEEE4882.StandardEvents Implements IDevice.ReadStandardEventStatus
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.ReadStandardEventStatus()
                Return Me.StandardEventStatus()
            Else
                Return CType(Me._StandardEventStatus, StandardEvents)
            End If
        End Function

#Region " REPLACED WITH STANDARD EVENT STATUS "
#If False Then
    ''' <summary>Gets or sets the last event status register (ESR) data byte.</summary>
    Private _eventStatusRegisterByte As Nullable(Of Int32)

    ''' <summary>Reads or sets the event status register (ESR) data byte.
    ''' For instruments not supporting event status register (ESR) this can be set as the
    ''' default value returned when reading ESR.  ESR can be set also if not using devices.
    ''' </summary>
    ''' <param name="access">Determines access to the device for read, write, and verify.</param>
    Public Property EventStatusRegisterByte(byval access As AccessControl) As Int32
      Get
        If Me.UsingDevices AndAlso me.session IsNot Nothing andalso Me._esrQueryCommand.IsSupported Then
          Me._eventStatusRegisterByte = Me.Getter(access, Me._esrQueryCommand, Me._eventStatusRegisterByte)
        End If
        Return Me._eventStatusRegisterByte.Value
      End Get
      Set(ByVal Value As Int32)
        If Not (Me.UsingDevices AndAlso Me._esrQueryCommand.IsSupported) Then
          Me._eventStatusRegisterByte = Value
        End If
      End Set
    End Property

    ''' <summary>
    ''' Reads the event status register.
    ''' </summary>
    Public Function ReadEventStatusRegister() As Int32
      return Me.EventStatusRegisterByte(accesscontrol.read)
    End Function

#End If
#End Region

#End Region

#Region " MAV "

        ''' <summary>Reads the device status data byte and returns True
        ''' if the message available bits were set.
        ''' </summary>
        ''' <returns>True if the device has data in the queue
        ''' </returns>
        Public Overridable Function IsMessageAvailable() As Boolean
            ' check if we have data in the queue
            Return (Me.Session.ReadStatusByte() And Me._messageAvailableBits) <> 0
        End Function

        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Private _messageAvailableBits As isr.Visa.IEEE4882.ServiceRequests = ServiceRequests.MessageAvailable

        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Public Property MessageAvailableBits() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return Me._messageAvailableBits
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                Me._messageAvailableBits = Value
            End Set
        End Property

#End Region

#Region " SRE: SERVICE REQUEST ENABLE "

        Private _ServiceRequestEventEnable As Integer
        ''' <summary>Reads or write the service request event Enable (SRE) data byte.
        ''' For instruments not supporting event status register (SRE) this can be set as the
        ''' default value returned when reading SRE.  SRE can be set also if not using devices.
        ''' The binary equivalent of the returned value indicates which register bits
        ''' Assigning a value to this attribute enables one or more status events for
        ''' service request. When an enabled status event occurs, bit B6 of the status
        ''' byte sets to generate an SRQ (service request). The service request enable
        ''' register uses most of the same summary events as the status byte.
        ''' Bit B6 (MSS) is not used by the enable register.
        ''' </summary>
        Public Property ServiceRequestEventEnable() As isr.Visa.IEEE4882.ServiceRequests
            Get
                If Me.Session IsNot Nothing Then
                    Return Me.Session.ServiceRequestEventEnable
                Else
                    Return CType(Me._ServiceRequestEventEnable, ServiceRequests)
                End If
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                If Me.Session IsNot Nothing Then
                    Me.Session.ServiceRequestEventEnable = Value
                End If
                Me._ServiceRequestEventEnable = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadServiceRequestEventEnable() As isr.Visa.IEEE4882.ServiceRequests
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.ReadServiceRequestEventEnable()
            End If
            Return Me.ServiceRequestEventEnable
        End Function

        ''' <summary>
        ''' Writes to the event register.
        ''' </summary>
        Public Function WriteServiceRequestEventEnable(ByVal value As isr.Visa.IEEE4882.ServiceRequests) As isr.Visa.IEEE4882.ServiceRequests
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.WriteServiceRequestEventEnable(value)
            End If
            Me._ServiceRequestEventEnable = value
        End Function

#End Region

#Region " SRQ "

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeoutSeconds">
        ''' Specifies how many seconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <param name="pollDelay">Specifies milliseconds between serial polls.</param>
        ''' <history>
        ''' 07/02/99  David Hary  1.3.9  Add Status Byte Register argument
        ''' 12/15/00  David Hary  1.09.12  Add long time out option.
        ''' 11/05/01  David Hary  1.11.10  Use new GPIB classes
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.IEEE4882.ServiceRequests,
                                 ByVal timeoutSeconds As Single, ByVal pollDelay As Integer) As Boolean

            Dim endTime As Date

            ' check if time out is negative
            If timeoutSeconds >= 10 * Single.Epsilon Then
                endTime = DateTime.Now.AddSeconds(timeoutSeconds)
            Else
                ' if negative, set to 'infinite' value.
                endTime = DateTime.Now.AddDays(1)
            End If

            ' Clear the SRQ flag
            Me.ReadDeviceStatusByte()

            ' Loop until SQR or Time out
            Do

                ' await for some time
                Threading.Thread.Sleep(pollDelay)

            Loop Until Me.CachedServiceRequest(statusByteBits) OrElse (DateTime.Now.CompareTo(endTime) > 0)

            ' Set the time out flag to true if the SRQ flag was not set
            Me._timedOut = Not Me.IsReceivedSrq(statusByteBits)

            Return Not Me._timedOut AndAlso Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeoutSeconds">
        ''' Specifies how many seconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <history>
        ''' 07/02/99  David Hary  1.3.9  Add Status Byte Register argument
        ''' 12/15/00  David Hary  1.09.12  Add long time out option.
        ''' 11/05/01  David Hary  1.11.10  Use new GPIB classes
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.IEEE4882.ServiceRequests, ByVal timeoutSeconds As Single) As Boolean

            Return Me.AwaitServiceRequest(statusByteBits, timeoutSeconds, 1)

        End Function

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeout">
        ''' Specifies how many milliseconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <param name="pollDelay">Specifies milliseconds between serial polls.</param>
        ''' <history>
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.IEEE4882.ServiceRequests,
                                 ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

            Return Me.AwaitServiceRequest(statusByteBits, 0.001! * timeout, pollDelay)

        End Function

        ''' <summary>Serial polls if not already received SRQ.
        ''' This function sets the service request tag (blnSRQ)
        ''' property of this device.  If the SQR is set (i.e.,
        ''' SRQ serial poll bit was set), the function returns
        ''' true and sets the SRQ tag to true.
        ''' Thus, you must make sure that the SRQ tag property
        ''' is cleared (set to False) before inquiring about the
        ''' SRQ bit again.
        ''' </summary>
        ''' <returns>True if service requested.
        ''' </returns>
        ''' <param name="statusBits">Specifies specifies which bits to look for
        ''' in the status byte register.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Function CachedServiceRequest(ByVal statusBits As isr.Visa.IEEE4882.ServiceRequests) As Boolean

            ' return true if already received the service request.  Otherwise, read and return true if now got it.
            Return Me.IsReceivedSrq(statusBits) OrElse ((Me.ReadDeviceStatusByte() And statusBits) <> 0)

        End Function

        ''' <summary>Requests the instrument to set the OPC
        ''' bit of the status byte register after all operations
        ''' are complete and issue an SRQ.
        ''' Use this method to request an OPC bit set after
        ''' all operations are complete.
        ''' The standard byte register must be cleared before
        ''' issuing this command.
        ''' You must enable the standard event register OPC byte
        ''' before  issuing this command.
        ''' </summary>
        ''' <history>
        ''' </history>
        Public Function EnableOpcSrqDeprecated() As Boolean

            Debug.Assert(Not Debugger.IsAttached, "DEPRECATED - USE IssueOperationCompleteRequest()")
            ' update: Check if necessary. clear the service request bit
            ' Me._Scpi.GPIBDevice.SerialPoll And &HFF
            ' this is done only when enabling service request.
            ' Me.ReadDeviceStatusByte()

            ' Issue service request
            Return Me.IssueOperationCompleteRequest()

        End Function

        ''' <summary>Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that wil issue an SRQ
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' 3.0.3478: Renamed from EnableSrq.
        ''' </history>
        Public Function EnableServiceRequestLong(ByVal standardEventEnableBits As isr.Visa.IEEE4882.StandardEvents,
                                  ByVal serviceRequestEnableBits As isr.Visa.IEEE4882.ServiceRequests) As Boolean

            ' clear the event status.
            Me.ClearExecutionState()

            ' Check if necessary; clear the service request bit
            Me.ReadDeviceStatusByte()

            ' build the request message.
            Me.WriteStandardEventEnable(standardEventEnableBits)
            Me.WriteServiceRequestEventEnable(serviceRequestEnableBits)

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that wil issue an SRQ
        ''' Uses a single line command to accomplish the entire command so as to save time.
        ''' Also issues *OPC.
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' 3.0.3478: Renamed from EnableSrq.
        ''' </history>
        Public Function EnableServiceRequestComplete(ByVal standardEventEnableBits As isr.Visa.IEEE4882.StandardEvents,
                                  ByVal serviceRequestEnableBits As isr.Visa.IEEE4882.ServiceRequests) As Boolean

            ' clear internal elements.
            Me.ClearDeviceStatusByte()

            ' create the command string.
            Dim commandLine As New System.Text.StringBuilder
            commandLine.AppendFormat("{0}{1}", Me.Session.ClearExecutionStateCommand.Value, Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me.Session.StandardEventEnableCommand.BuildCommand(CInt(standardEventEnableBits)), Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me.Session.ServiceRequestEnableCommand.BuildCommand(CInt(serviceRequestEnableBits)), Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me.Session.OperationCompletedCommand.Value, Environment.NewLine)

            Me.WriteLine(commandLine.ToString)

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Returns true if service request bit is on.  Does not
        ''' change the cached value of the service request condition.
        ''' </summary>
        Public Function IsRequestingService() As Boolean
            Return (Me.ReadDeviceStatusByte() And isr.Visa.IEEE4882.ServiceRequests.RequestingService) <> 0
        End Function

        ''' <summary>
        ''' Gets or sets the cached service request status.
        ''' True if service request was received.
        ''' </summary>
        ''' <param name="statusBits">Specifies specifies which bits to look for
        ''' in the status byte register.
        ''' </param>
        Public Property IsReceivedSrq(ByVal statusBits As isr.Visa.IEEE4882.ServiceRequests) As Boolean
            Get
                If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                    Return (Me.DeviceStatusByte And statusBits) <> 0 OrElse (Me.ReadDeviceStatusByte() And statusBits) <> 0
                Else
                    Return (statusBits And IEEE4882.ServiceRequests.Unknown) <> 0
                End If
            End Get
            Set(ByVal Value As Boolean)
                ' use for simulation.
                If Value Then
                    Me.DeviceStatusByte = Me.DeviceStatusByte Or statusBits
                Else
                    Me.DeviceStatusByte = Me.DeviceStatusByte And Not statusBits
                End If
            End Set
        End Property

        ''' <summary>Issues an OPC command, e.g., *OPC, requesting
        ''' the instrument to turn on the OPC bit upon completion.
        ''' </summary>
        Public Function IssueOperationCompleteRequest() As Boolean

            If Me.UsingDevices AndAlso Me.Session IsNot Nothing AndAlso Me.Session.OperationCompletedCommand.IsSupported Then
                ' Output OPC for waiting for the operation completion.
                Return Me.WriteLine(Me.Session.OperationCompletedCommand.Value)
            Else
                Return True
            End If

        End Function

        Dim _requestRegistered As Boolean
        ''' <summary>
        ''' Returns true if the service request event handler was registered.
        ''' </summary>
        Public ReadOnly Property ServiceRequestHandlerRegistered() As Boolean
            Get
                Return Me._requestRegistered
            End Get
        End Property

        ''' <summary>
        ''' enables the service request register as well as the request event handling.
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Sub RegisterServiceRequestHandler(ByVal standardEventEnableBits As isr.Visa.IEEE4882.StandardEvents,
                                             ByVal serviceRequestEnableBits As isr.Visa.IEEE4882.ServiceRequests)

            If Me._requestRegistered Then
                Me._requestRegistered = False
                ' remove the service request handler
                RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
            End If

            Me._requestRegistered = True

            ' register the handler after enabling the event to make sure things were enabled.
            AddHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest

            Try

                ' this is required for raising the events.
                Me.Session.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)

            Catch ex As Exception

                If Me.Session IsNot Nothing Then
                    If Me._requestRegistered Then
                        Me._requestRegistered = False
                        ' remove the service request handler
                        RemoveHandler Me.Session.ServiceRequest, AddressOf OnVisaServiceRequest
                    End If
                End If

                Throw

            End Try

            Try


                Me.WriteStandardEventEnable(standardEventEnableBits)

            Catch ex As Exception

                Throw

            End Try


            If serviceRequestEnableBits <> Me.ReadServiceRequestEventEnable() Then

                Try
                    ' todo:  Check if also enables the SRQ.
                    Me.WriteServiceRequestEventEnable(serviceRequestEnableBits)

                Catch ex As Exception

                    Throw

                End Try

            End If

        End Sub

#End Region

#Region " STB "

        Private _serviceRequestEventStatus As Nullable(Of isr.Visa.IEEE4882.ServiceRequests)
        ''' <summary>Reads or sets the service request event status (STB) data byte.
        ''' For instruments not supporting event status register (STB) this can be set as the
        ''' default value returned when reading STB.  STB can be set also if not using devices.
        ''' </summary>
        Public Property ServiceRequestEventStatus() As isr.Visa.IEEE4882.ServiceRequests
            Get
                Return Me._serviceRequestEventStatus.Value
            End Get
            Set(ByVal Value As isr.Visa.IEEE4882.ServiceRequests)
                Me._serviceRequestEventStatus = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Public Function ReadServiceRequestEventStatus() As isr.Visa.IEEE4882.ServiceRequests
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing AndAlso Me.Session.ServiceRequestStatusQueryCommand.IsSupported Then
                Me.ServiceRequestEventStatus = CType(Me.Session.ReadRegister(Me.Session.ServiceRequestStatusQueryCommand).GetValueOrDefault(0), isr.Visa.IEEE4882.ServiceRequests)
            End If
            Return Me.ServiceRequestEventStatus()
        End Function

#End Region

#Region " MEASUREMENT EVENTS "

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadMeasurementEventStatus() As Integer Implements IDevice.ReadMeasurementEventStatus
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadMeasurementEventStatus()
            Else
                Return 0
            End If
        End Function

#End Region

#Region " OPERATION EVENTS "

        Private _operationEventEnable As Nullable(Of Integer)
        ''' <summary>Reads or write the operation event request enable data byte.
        ''' For instruments not supporting operation event status register this can be set as the
        ''' default value returned when reading operation status event. Operation status event can 
        ''' be set also if not using devices.
        ''' </summary>
        Public Overridable Property OperationEventEnable() As Integer
            Get
                Return Me._operationEventEnable.Value
            End Get
            Set(ByVal Value As Integer)
                Me._operationEventEnable = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadOperationEventEnable() As Integer
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing AndAlso Me.Session.OperationEventEnableQueryCommand.IsSupported Then
                Me._operationEventEnable = Me.Session.ReadRegister(Me.Session.OperationEventEnableQueryCommand).GetValueOrDefault(&H10000) ' Bit 16 (zero based) = unknown
            End If
            Return Me.OperationEventEnable()
        End Function

        ''' <summary>
        ''' Writes to the operation event enable register.
        ''' </summary>
        Public Sub WriteOperationEventEnable(ByVal value As Integer)
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing AndAlso Me.Session.OperationEventEnableCommand.IsSupported Then
                If Me.Session.Setter(Me.Session.OperationEventEnableCommand, value) Then
                    Me._operationEventEnable = value
                Else
                    Me._operationEventEnable = &H10000 ' Bit 16 (zero based) = unknown
                End If
            Else
                Me._operationEventEnable = value
            End If
        End Sub

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadOperationEventStatus() As Integer Implements IDevice.ReadOperationEventStatus
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Dim value As Nullable(Of Integer)
                value = Me.Session.ReadOperationEventStatus()
                If value.HasValue Then
                    Return value.Value
                Else
                    Return 0
                End If
            Else
                Return 0
            End If
        End Function

#End Region

#Region " QUESTIONABLE EVENTS "

        ''' <summary>
        ''' Reads the questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadQuestionableEventStatus() As Integer Implements IDevice.ReadQuestionableEventStatus
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Dim value As Nullable(Of Integer) = Me.Session.ReadQuestionableEventStatus()
                If value.HasValue Then
                    Return value.Value
                Else
                    Return 0
                End If
            Else
                Return 0
            End If
        End Function

#End Region

#End Region

#Region " INTERFACE "

        Private _interface As isr.Visa.Gpib.GpibInterface
        ''' <summary>Gets or sets reference to the Gpib interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As isr.Visa.Gpib.GpibInterface
            Get
                If Me._interface Is Nothing AndAlso Me.UsingDevices Then
                    Me._interface = New isr.Visa.Gpib.GpibInterface(Me.BoardNumber, Me.ConnectTimeout)
                    Me._interface.PrimaryAddress = Me.Session.PrimaryAddress
                End If
                Return Me._interface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Overridable Sub ClearInterface()
            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SendInterfaceClear()
            End If
        End Sub

        ''' <summary>Returns a string array of all local GPIB resources.</summary>
        Public Shared Function LocalResourceNames() As String()
            Using manager As New isr.Visa.LocalResourceManager
                Return manager.FindResources(VisaNS.HardwareInterfaceType.Gpib)
            End Using
        End Function

#End Region

#Region " FLUSH "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim synopsis As String = "Discard unread data"

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until Me.IsMessageAvailable OrElse timedOut
                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me.ReadLine()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Flushed '{0}'", isr.Core.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(flushed))
                        End If

                    Else
                        Me.ReadLine()
                        'Me.Session.DiscardUnreadData()

                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.DiscardUnreadData

            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IDevice.FlushRead

            Dim synopsis As String = "Flush Read"
            Try

                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis,
                                      "Exception occurred flushing read buffer. VISA status={1}. Details: {2}.",
                                      Me.LastVisaStatus, ex)

            End Try

        End Function

        ''' <summary>
        ''' Flush the write buffers.
        ''' </summary>
        Private Sub _flushWrite()

            Try

                ' flush read buffer.
                Me.OnMessageAvailable(TraceEventType.Verbose, "FLUSHING WRITE BUFFERS", "Flushing write buffers")
                Me.Session.FlushWrite()

                ' Me.WriteLine("")

                If Not Me.IsLastVisaOperationSuccess Then
                    Me.OnMessageAvailable(TraceEventType.Verbose, "FAILED FLUSHING WRITE BUFFERS", "Failed during flush write. VISA status={0}{1}{2}",
                                                 Me.LastVisaStatus, Environment.NewLine,
                                                 isr.Core.StackTraceParser.UserCallStack(4, 10))
                End If

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED FLUSHING WRITE BUFFER", "Exception occurred flushing write buffer. VISA status={1}. Details: {2}.",
                                     Me.LastVisaStatus, ex)

            Finally

            End Try

        End Sub

        ''' <summary>
        ''' Flush the write buffers.
        ''' </summary>
        Public Overridable Sub FlushWrite()
            Me._flushWrite()
        End Sub

#End Region

#Region " QUERY "

        ''' <summary>
        ''' Queries the Gpib instrument and returns a Boolean value.
        ''' </summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            If String.IsNullOrWhiteSpace(question) Then
                Throw New ArgumentNullException("question")
            End If
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Boolean) = Me.Session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Boolean") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Boolean")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Boolean query '{0}'", question) Then
                    InstrumentException.ThrowException(Me, "failed sending Boolean query '{0}'", question)
                End If
            End If
        End Function

        ''' <summary>
        ''' Sends a query command and returns the Boolean outcome.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <remarks>
        ''' </remarks>
        Public Function QueryBoolean(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Boolean)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Boolean) = Me.Session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Boolean") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Boolean")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Boolean query: " & format, args) Then
                    InstrumentException.ThrowException(Me, "failed sending Boolean query: " & format, args)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Double) = Me.Session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query double") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query double")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending double query '{0}'", question) Then
                    InstrumentException.ThrowException(Me, "failed sending double query '{0}'", question)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a double value.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryDouble(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Double)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Double) = Me.Session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query double") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query double")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending double query: " & format, args) Then
                    InstrumentException.ThrowException(Me, "failed sending double query: " & format, args)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Int32) = Me.Session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Int32")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Int32 query '{0}'", question) Then
                    InstrumentException.ThrowException(Me, "failed sending Int32 query '{0}'", question)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a int32 value.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryInt32(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Int32)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Int32) = Me.Session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Int32")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Int32 query: " & format, args) Then
                    InstrumentException.ThrowException(Me, "failed sending Int32 query: " & format, args)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Int32) = Me.Session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Inf-Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Inf-Int32")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Inf-Int32 query '{0}'", question) Then
                    InstrumentException.ThrowException(Me, "failed sending Inf-Int32 query '{0}'", question)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a int32 value.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryInfInt32(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Int32)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Int32) = Me.Session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Inf-Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing query Inf-Int32")
                    End If
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Inf-Int32 query: " & format, args) Then
                    InstrumentException.ThrowException(Me, "failed sending Inf-Int32 query: " & format, args)
                End If
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IDevice.QueryTrimEnd
            If Me.WriteQueryLine(question) Then
                Return Me.ReadLineTrimEnd()
            Else
                Return String.Empty
            End If
#If False Then
      If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
        Return me.session.QueryTrimEnd(question)
      Else
        Return String.Empty
      End If
#End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IDevice.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Return Me.ReadLineTrimEnd()
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
        ''' <param name="format">Specifies the format statement.
        ''' </param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Function QueryLine(ByVal format As String, ByVal ParamArray args() As Object) As String
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Return Me.ReadLine()
            Else
                Return String.Empty
            End If
        End Function

#End Region

#Region " READ "

        ''' <summary>Reads the Gpib instrument and returns a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            Dim value As Nullable(Of Boolean) = True
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                value = Me.Session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Boolean") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing Boolean")
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>Reads the Gpib instrument and returns a double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            Dim value As Nullable(Of Double) = 0
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                value = Me.Session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Double") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing Double")
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>Reads the Gpib instrument and returns an Int32 value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            Dim value As Nullable(Of Int32) = 0
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                value = Me.Session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing Int32")
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>Reads the Gpib instrument and returns an Int32 value.</summary>
        Public Function ReadInfInt32() As Nullable(Of Int32)
            Dim value As Nullable(Of Int32) = 0
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                value = Me.Session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Inf-Int32") Then
                        InstrumentException.ThrowException(Me, "failed reading or parsing Inf-Int32")
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>Reads and returns a string to the termination character and return the string
        ''' including the termination character.</summary>
        Public Function ReadLine() As String
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadLine()
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>Reads and returns a string to the termination character and return the string
        ''' not including the termination character.</summary>
        Public Function ReadLineTrimEnd() As String Implements IDevice.ReadLineTrimEnd
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Return Me.Session.ReadLineTrimEnd()
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until data is no longer available.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        Public Function ReadLines(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal trimEnd As Boolean) As String

            Return ReadLines(pollDelay, timeout, False, trimEnd)

        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until data is no longer available
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="trimSpaces">Specifies a directive to trim leading and trailing spaces from each line.
        ''' This also trims the end character.</param>
        ''' <param name="trimEnd">Specifies a directive to trim the end character from each line.</param>
        Public Function ReadLines(ByVal pollDelay As Integer, ByVal timeout As Integer,
                          ByVal trimSpaces As Boolean, ByVal trimEnd As Boolean) As String

            Try

                If Not Me.UsingDevices OrElse Me.Session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder

                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)
                Dim timedOut As Boolean = False
                Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                    ' allow message available time to materialize
                    Dim hasData As Boolean = Me.IsMessageAvailable
                    Do Until hasData OrElse timedOut
                        timedOut = DateTime.Now > endTime
                        Dim t1 As DateTime = DateTime.Now.AddMilliseconds(pollDelay)
                        Do Until DateTime.Now > t1
                            Global.System.Windows.Forms.Application.DoEvents()
                            Threading.Thread.Sleep(2)
                            Global.System.Windows.Forms.Application.DoEvents()
                        Loop
                        hasData = Me.IsMessageAvailable
                    Loop

                    If hasData Then
                        timedOut = False
                        endTime = DateTime.Now.AddMilliseconds(timeout)
                        If trimSpaces Then
                            listBuilder.AppendLine(Me.Session.ReadLine().Trim())
                        ElseIf trimEnd Then
                            listBuilder.AppendLine(Me.Session.ReadLineTrimEnd())
                        Else
                            listBuilder.AppendLine(Me.Session.ReadLine())
                        End If
                    End If

                Loop

                Return listBuilder.ToString

            Catch
                Throw
            Finally
            End Try
        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until timeout.
        ''' </summary>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="trimSpaces">Specifies a directive to trim leading and trailing spaces from each line</param>
        ''' <param name="expectedLength">Specifies the amount of data expected without trimming.</param>
        Public Function ReadLines(ByVal timeout As Integer, ByVal trimSpaces As Boolean, ByVal expectedLength As Integer) As String

            Try

                If Not Me.UsingDevices OrElse Me.Session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder

                Me.StoreTimeout(timeout)

                Dim timedOut As Boolean = False
                Dim currentLength As Integer = 0
                Do While currentLength < expectedLength AndAlso Me.IsLastVisaOperationSuccess AndAlso Not timedOut

                    Try

                        Me.Session.ReadLine()
                        expectedLength += Me.Session.ReceiveBuffer.Length
                        If trimSpaces Then
                            listBuilder.AppendLine(Me.Session.ReceiveBuffer.Trim)
                        Else
                            listBuilder.AppendLine(Me.Session.ReceiveBuffer)
                        End If

                    Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaStatusCode.ErrorTimeout

                        timedOut = True

                    End Try

                Loop

                Return listBuilder.ToString

            Catch

                Throw

            Finally

                Me.RestoreTimeout()

            End Try

        End Function

        ''' <summary>
        ''' Reads a potentially long string from the instrument until reading an end of line.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        Public Function ReadString(ByVal pollDelay As Integer, ByVal timeout As Integer) As String

            Try

                If Not Me.UsingDevices OrElse Me.Session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder
                Dim endDetected As Boolean = False
                Dim timedOut As Boolean = False
                Do Until endDetected OrElse timedOut OrElse Me.IsLastVisaOperationFailed

                    timedOut = False
                    Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                    ' allow message available time to materialize
                    Do Until Me.IsMessageAvailable OrElse timedOut
                        timedOut = DateTime.Now > endTime
                        Threading.Thread.Sleep(pollDelay)
                    Loop

                    If Not timedOut Then
                        Dim value As String = Me.Session.Reader.ReadString()
                        listBuilder.Append(value)
                        endDetected = value.EndsWith(Convert.ToChar(Me.Session.Reader.TerminationCharacter), StringComparison.OrdinalIgnoreCase)
                    End If

                Loop

                Return listBuilder.ToString

            Catch
                Throw
            Finally

            End Try
        End Function


#End Region

#Region " INSTRUMENT ERROR MANAGEMENT "

        Private _lastOperationOkay As Boolean
        ''' <summary>
        ''' Gets the status of the last operation.
        ''' </summary>
        Public Property LastOperationOkay() As Boolean
            Get
                Return Me._lastOperationOkay
            End Get
            Protected Set(ByVal value As Boolean)
                Me._lastOperationOkay = value
            End Set
        End Property

        Private _isDelegateErrorHandling As Boolean
        ''' <summary>Gets or sets the condition for telling the device to raise the
        ''' <see cref="HandleError">handle error event</see> to delegate error
        ''' handling to the calling instrument.</summary>
        Public Property IsDelegateErrorHandling() As Boolean
            Get
                Return Me._isDelegateErrorHandling
            End Get
            Set(ByVal Value As Boolean)
                Me._isDelegateErrorHandling = Value
            End Set
        End Property

        ''' <summary>
        ''' Reports if a visa or device error occurred as set the <see cref="LastOperationOkay">success condition</see>
        ''' Can only be used after receiving a full reply from the instrument.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if success</returns>
        Public Function ReportVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal synopsis As String,
                                              ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "VISA errors occurred {0}. VISA '{1}' error details: {2}{3}{4}",
                                                     String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                     Me.ResourceName,
                                                     Me.LastVisaStatusDetails, Environment.NewLine,
                                                     isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            ElseIf Me.HandleInstrumentErrorIfError(flushReadFirst) Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Instrument encountered errors {0}. Instrument '{1}' error details: {2}{3}{4}",
                                                     String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                     Me.ResourceName,
                                                     Me.Session.DeviceErrors, Environment.NewLine,
                                                     isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Reports if a visa error occurred as set the <see cref="LastOperationOkay">success condition</see>
        ''' Can be used with queries.
        ''' </summary>
        ''' <returns>True if success</returns>
        Public Function ReportVisaOperationOkay(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "VISA '{0}' error details: {1}{2}{3}",
                                                     String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                     Me.LastVisaStatusDetails, Environment.NewLine,
                                                     isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Handles instrument error.  For compatibility
        ''' Clears the error queue and status by reading the error queue.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if error occurred.</returns>
        Public Overridable Function HandleInstrumentError(ByVal flushReadFirst As Boolean) As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False

            If Me._isDelegateErrorHandling Then

                Me._lastServiceEventArgs.HasError = True
                Me.OnHandleError()

            Else

                If flushReadFirst Then
                    Me.FlushRead(10, 200, False)
                End If

                '  In order to keep compatibility, we clear the error here by reading the error status.
                Me.Session.ReadErrorQueue()

                ' read the standard event register
                Me.ReadStandardEventStatus()

                If Not Me._lastServiceEventArgs.HasError Then

                    Dim errorBits As isr.Visa.IEEE4882.StandardEvents = isr.Visa.IEEE4882.StandardEvents.CommandError Or
                        isr.Visa.IEEE4882.StandardEvents.DeviceDependentError Or
                        isr.Visa.IEEE4882.StandardEvents.ExecutionError Or
                        isr.Visa.IEEE4882.StandardEvents.QueryError
                    Me._lastServiceEventArgs.HasError = (Me.StandardEventStatus And errorBits) <> 0

                End If

            End If

            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Check the status byte for error bits and latches the
        ''' <see cref="ServiceEventArgs.HasError">last service request error sentinel.</see>
        ''' </summary>
        Public Overridable Function IsErrorAvailable() As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False
            Me._lastServiceEventArgs.HasError = (Me.ReadDeviceStatusByte And Me.Session.ErrorAvailableBits) <> 0
            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Checks the status register for an error.  For compatibility
        ''' Clears the error status by reading the error queue.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if error occurred.</returns>
        Public Overridable Function HandleInstrumentErrorIfError(ByVal flushReadFirst As Boolean) As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False

            If (Me.ReadDeviceStatusByte And Me.Session.ErrorAvailableBits) <> 0 Then

                Me._lastServiceEventArgs.HasError = Me.HandleInstrumentError(flushReadFirst)

            End If

            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Throws a <see cref="VisaException">VISA exception</see>
        ''' Sets the <see cref="LastOperationOkay">success condition</see>
        ''' Can be used with queries.
        ''' </summary>
        ''' <returns>True if success</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")>
        Public Function RaiseVisaException(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.LastOperationOkay = False
                VisaException.ThrowException(Me, format, args)
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Throws an <see cref="InstrumentException">instrument exceptions</see>.
        ''' Set the <see cref="LastOperationOkay">success condition</see>
        ''' Flushes the read buffer if an error occurred.
        ''' </summary>
        ''' <returns>True if success</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")>
        Public Function RaiseVisaOrDeviceException(ByVal format As String, ByVal ParamArray args() As Object) As Boolean

            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Return Me.RaiseVisaException(format, args)
            ElseIf Me.IsErrorAvailable Then
                ' if an error occurred, flush the read buffers to make sure we can get the error and report it.
                ' Me.FlushRead(2, 200, False)
                If Me.HandleInstrumentError(True) Then
                    Me.LastOperationOkay = False
                    InstrumentException.ThrowException(Me, format, args)
                End If
            End If
            Return Me.LastOperationOkay
        End Function

#End Region

#Region " RESOURCE MANAGEMENT "

        ''' <summary>
        ''' Returns true if having a valid primary address.
        ''' </summary>
        ''' <param name="address">The device primary address</param>
        Public Shared Function IsValidPrimaryAddress(ByVal address As Integer) As Boolean
            Return (address >= 1) AndAlso (address <= 31)
        End Function

        ''' <summary>Gets or sets the primary address of the instrument on the Gpib bus.</summary>
        Public ReadOnly Property PrimaryAddress() As Int32
            Get
                If Me.Session IsNot Nothing Then
                    Return Me.Session.PrimaryAddress
                Else
                    Return 0
                End If
            End Get
        End Property

        ''' <summary>Gets the board number.</summary>
        Public ReadOnly Property BoardNumber() As Integer
            Get
                If Me.Session IsNot Nothing Then
                    Return Me.Session.BoardNumber
                Else
                    Return 0
                End If
            End Get
        End Property

#End Region

#Region " SERVICE REQUEST HANDLERS "

        Private _lastServiceEventArgs As ServiceEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="isr.Visa.ServiceEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As ServiceEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As ServiceEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        ''' <summary>Raised upon receiving a service request.  This event does not expose the
        '''   internal service request arguments directly.</summary>
        Public Event ServiceRequest As EventHandler(Of ServiceEventArgs) ' System.EventArgs)

        ''' <summary>Raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnServiceRequest(ByVal e As ServiceEventArgs) ' System.EventArgs)

            If e Is Nothing Then Return
            Dim synopsis As String = "On Service Request"
            If e.HasError Then
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Instrument reported an error on service request:{0}{1}",
                                             Environment.NewLine, e.LastError)
            Else

                Dim elapsedMilliseconds As Double = e.OperationElapsedTime.TotalMilliseconds()
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Service Request Event", "Operation completed in {0}ms", elapsedMilliseconds)
            End If

            ServiceRequestEvent.SafeInvoke(Me, e)
            e.ServicingRequest = False

        End Sub

        ''' <summary>Handles service requests from the instrument.</summary>
        Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

            If Not Me._lastServiceEventArgs.ServicingRequest Then
                ' do not initialize the last service event arguments because this is done with Clear Status.
                Me._lastServiceEventArgs.ServicingRequest = True
                Me._lastServiceEventArgs.ProcessServiceRequest()
                Me._lastServiceEventArgs.ReadRegisters()
                If Me._lastServiceEventArgs.HasError Then
                    ' disable recurring service requests
                    'Me.ServiceRequestEventEnable = ServiceRequests.None
                Else
                    ' check if we have consecutive requests.
                    If Me._lastServiceEventArgs.RequestCount > 10 Then
                        ' disable recurring service requests
                        Me.ServiceRequestEventEnable() = IEEE4882.ServiceRequests.None
                        Dim synopsis As String = "Service Request Queue Limit"
                        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Service Request Queue Limit", "Instrument had over 10 consecutive service requests")
                    End If
                End If
                OnServiceRequest(Me._lastServiceEventArgs)
            End If

        End Sub

#End Region

#Region " SESSION MANAGEMENT "

        ''' <summary>Restores the last timeout from the stack.
        ''' </summary>
        Public Sub RestoreTimeout()
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.RestoreTimeout()
            End If
        End Sub

        ''' <summary>Saves the current timeout and sets a new setting timeout.
        ''' </summary>
        ''' <param name="timeout">Specifies the new timeout</param>
        Public Sub StoreTimeout(ByVal timeout As Integer)
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.StoreTimeout(timeout)
            End If
        End Sub

        ''' <summary>
        ''' Gets the device timeout in milliseconds.
        ''' Use store and restore timeout to change timeouts.
        ''' </summary>
        Public ReadOnly Property Timeout() As Integer
            Get
                If Me.Session IsNot Nothing Then
                    Return Me.Session.Timeout
                Else
                    Return 0
                End If
            End Get
        End Property

        Private _timedOut As Boolean
        ''' <summary>Gets or sets the condition for any operation timed out.
        ''' </summary>
        Public ReadOnly Property TimedOut() As Boolean
            Get
                Return Me._timedOut
            End Get
        End Property

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.IsLastVisaOperationFailed
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me.Session Is Nothing Then
                    Return False
                Else
                    Return Me.Session.IsLastVisaOperationWarning
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IDevice.LastVisaStatus ' NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me.Session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me.Session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IDevice.LastVisaStatusDetails
            Get
                If Me.Session Is Nothing Then
                    Return ""
                Else
                    Return Me.Session.BuildLastVisaStatusDetails()
                End If
            End Get
        End Property

#End Region

#Region " WRITE "

        ''' <summary>Sends a command line to the instrument.</summary>
        ''' <param name="format">Specifies the format statement</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Overridable Function WriteLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Return Me.WriteLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        End Function

        ''' <summary>Sends a command line to the instrument.</summary>
        ''' <param name="value">The command.</param>
        Public Overridable Function WriteLine(ByVal value As String) As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.WriteLine(value)
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="queryCommand">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Overridable Function WriteOnOff(ByVal queryCommand As String, ByVal isOn As Boolean) As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.WriteLine(queryCommand)
                Me.Session.WriteLineOnOff(queryCommand, isOn)
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="queryCommand">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Overridable Function WriteOneZero(ByVal queryCommand As String, ByVal one As Boolean) As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.WriteLineOneZero(queryCommand, one)
                Me.WriteLine(queryCommand)
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Sends a query command line to the instrument.
        ''' A returned value is expected from the instrument after issuing this command.</summary>
        ''' <param name="value">The command.</param>
        Public Overridable Function WriteQueryLine(ByVal value As String) As Boolean
            If Me.UsingDevices AndAlso Me.Session IsNot Nothing Then
                Me.Session.WriteLine(value)
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Sends a query command line to the instrument.
        ''' A returned value is expected from the instrument after issuing this command.</summary>
        ''' <param name="format">Specifies the format statement</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Overridable Function WriteQueryLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Return Me.WriteQueryLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        End Function

#End Region

    End Class

End Namespace

#Region " UNUSED "
#If False Then
#Region " SYNCHRONIZER "

    ''' <summary>
    ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
    ''' to use for marshaling events.
    ''' </summary>
    Private _synchronizer As System.ComponentModel.ISynchronizeInvoke

    ''' <summary>
    ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
    ''' to use for marshaling events.
    ''' </summary>
    Public Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke
      Get
        If Me.IsDisposed Then
          Throw New ObjectDisposedException("isr.Visa.Ieee4882.Instrument")
        End If
        Return Me._synchronizer
      End Get
      Set(ByVal value As System.ComponentModel.ISynchronizeInvoke)
        If Me.IsDisposed Then
          Throw New ObjectDisposedException("isr.Visa.Ieee4882.Instrument")
        End If
        Me._synchronizer = value
      End Set
    End Property

#End Region

#End If
#End Region

#Region " UNUSED "
#If False Then
#Region " MESSAGE AVAILABLE "

    ''' <summary>Occurs when the element or instrument has a message
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs)

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="e">Specifies the event arguments.</param>
    Public Sub OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs)
      Me._lastMessage = e.Details
      MessageAvailableEvent.SafeBeginInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Raises a message.
    ''' </summary>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message short synopsis.</param>
    ''' <param name="format">Specifies the format for building the message detains</param>
    ''' <param name="args">Specifies the format arguments.</param>
    Public Overridable Sub OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType, 
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)

      Me.OnMessageAvailable(New isr.Core.MessageEventArgs(traceLevel, synopsis, format, args))

    End Sub

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="broadcastLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message Synopsis.</param>
    ''' <param name="format">Specifies the message details.</param>
    ''' <param name="args">Arguments to use in the format statement.</param>
    Public Overridable Sub OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, 
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)

      OnMessageAvailable(New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, format, args))

    End Sub

    Private _lastMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <System.ComponentModel.Browsable(False)> Public ReadOnly Property LastMessage() As String
      Get
        Return Me._lastMessage
      End Get
    End Property

    ''' <summary>Sets the status caption in case we have a display.</summary>
    ''' <param name="caption">Specifies the caption.</param>
    Public Overridable Sub UpdateStatusCaption(ByVal caption As String)
    End Sub

#End Region
#End If
#End Region

#Region " UNUSED "
#If False Then
    ''' <summary>
    ''' Occurs when the instrument is connected.
    ''' </summary>
    Public Event Connected(ByVal sender As Object, ByVal e As System.EventArgs) Implements isr.Core.IConnectResource.Connected

    ''' <summary>Raises the connected event.</summary>
    ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
    Protected Overridable Sub OnConnected(ByVal e As System.EventArgs) Implements isr.Core.IConnectResource.OnConnected
      ConnectedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when the instrument is disconnected.
    ''' </summary>
    Public Event Disconnected(ByVal sender As Object, ByVal e As System.EventArgs) Implements isr.Core.IConnectResource.Disconnected

    ''' <summary>Raises the disconnected event.</summary>
    ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
    Protected Overridable Sub OnDisconnected(ByVal e As System.EventArgs) Implements isr.Core.IConnectResource.OnDisconnected
      DisconnectedEvent.SafeInvoke(Me, e)
    End Sub
#End If
#End Region
