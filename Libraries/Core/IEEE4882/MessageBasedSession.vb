Imports NationalInstruments

Namespace IEEE4882

    ''' <summary>
    ''' Extends the message based session to support IEEE488.2.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history> 
    Public Class MessageBasedSession

        Inherits MessageBased.Session

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal resourceName As String)

            ' instantiate the base class
            MyBase.New(resourceName)

        End Sub

        ''' <summary>Cleans up managed and unmanaged resources.</summary>
        ''' <param name="disposing">If true, the method has been called directly or 
        '''   indirectly by a user's code. Managed and unmanaged resources can be disposed.
        '''   If disposing equals false, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects. Only unmanaged resources can be disposed.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " IRESETTABLE "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ClearExecutionState(ByVal visaSession As VisaNS.IMessageBasedSession) As Boolean

            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            ' Clear the device status.  This will set all the
            ' standard subsystem properties.
            MessageBased.Session.Write(visaSession, Scpi.Syntax.ClearExecutionStateCommand)

            Return Not MessageBasedSession.HasError(visaSession)

        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        Public Function ClearExecutionState() As Boolean

            Return MessageBasedSession.ClearExecutionState(Me)

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ResetKnownState(ByVal visaSession As VisaNS.IMessageBasedSession) As Boolean

            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            MessageBased.Session.Write(visaSession, Scpi.Syntax.ResetKnownStateCommand)
            Return Not MessageBasedSession.HasError(visaSession)

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Function ResetKnownState() As Boolean

            Return MessageBasedSession.ResetKnownState(Me)

        End Function

#End Region

#Region " IEEE488.2 COMMANDS "

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function OperationCompleted(ByVal visaSession As VisaNS.IMessageBasedSession) As Boolean
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return MessageBased.Session.QueryInt32(visaSession, Scpi.Syntax.OperationCompletedQueryCommand).Value = 1
        End Function

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function OperationCompleted() As Boolean
            Return MessageBasedSession.OperationCompleted(Me)
        End Function

        ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadIdentity(ByVal visaSession As VisaNS.IMessageBasedSession) As String
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return MessageBased.Session.QueryTrimEnd(visaSession, Scpi.Syntax.IdentityQueryCommand)
        End Function

        ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
        Public Function ReadIdentity() As String
            Return MessageBasedSession.ReadIdentity(Me)
        End Function

#End Region

#Region " OPEN "

        ''' <summary>Returns an open message based session.  Message-based sessions access VISA 
        '''   resources in an interface-independent manner. Such sessions can communicate with an 
        '''   instrument over a GPIB interface or serial interface.</summary>
        ''' <returns>An open message-based VISA session.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function OpenSession(ByVal resourceName As String) As MessageBasedSession
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New MessageBasedSession(resourceName)
                '      Return CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), MessageBasedSession)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("Resource selected must be a message-based session", ex)
            End Try
        End Function

#End Region

#Region " READ REGISTERS "

        ''' <summary>Returns True if the service request register 
        '''   status byte reports error available.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function HasError(ByVal visaSession As VisaNS.IMessageBasedSession) As Boolean
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Return (MessageBasedSession.ReadStatusByte(visaSession) And IEEE4882.ServiceRequests.ErrorAvailable) <> 0
        End Function

        ''' <summary>Returns True if the service request register 
        '''   status byte reports error available.</summary>
        Public Function HasError() As Boolean
            Return MessageBasedSession.HasError(Me)
        End Function

        ''' <summary>Returns the service request register status byte.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overloads Shared Function ReadStatusByte(ByVal visaSession As VisaNS.IMessageBasedSession) As IEEE4882.ServiceRequests
            If visaSession Is Nothing Then
                Throw New ArgumentNullException("visaSession")
            End If
            Dim status As Int32 = CType(visaSession.ReadStatusByte(), Int32)
            If status >= 0 Then
                Return CType(status, IEEE4882.ServiceRequests)
            Else
                Return CType(256 + status, IEEE4882.ServiceRequests)
            End If
        End Function

        ''' <summary>Reads the service request event status from the instrument.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared ReadOnly Property ServiceRequestEventStatus(ByVal visaSession As VisaNS.IMessageBasedSession) As IEEE4882.ServiceRequests
            Get
                If visaSession Is Nothing Then
                    Throw New ArgumentNullException("visaSession")
                End If
                Return CType(MessageBased.Session.QueryInt32(visaSession, Scpi.Syntax.ServiceRequestStatusQueryCommand), IEEE4882.ServiceRequests)
            End Get
        End Property

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public ReadOnly Property ServiceRequestEventStatus() As IEEE4882.ServiceRequests
            Get
                Return MessageBasedSession.ServiceRequestEventStatus(Me)
            End Get
        End Property

        ''' <summary>Programs or reads back the service request event request.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <returns>The <see cref="IEEE4882.ServiceRequests">mask</see>
        '''    to use for enabling the events.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>When set clears all service registers .</remarks>
        Public Shared Property ServiceRequestEventEnable(ByVal visaSession As VisaNS.IMessageBasedSession) As IEEE4882.ServiceRequests
            Get
                If visaSession Is Nothing Then
                    Throw New ArgumentNullException("visaSession")
                End If
                Return CType(MessageBased.Session.QueryInt32(visaSession, Scpi.Syntax.ServiceRequestEnableQueryCommand), IEEE4882.ServiceRequests)
            End Get
            Set(ByVal value As IEEE4882.ServiceRequests)
                MessageBased.Session.Write(visaSession, String.Format(Globalization.CultureInfo.CurrentCulture, Scpi.Syntax.ServiceRequestEnableCommand, value))
            End Set
        End Property

        ''' <summary>Programs or reads back the service request event request.</summary>
        ''' <returns>The <see cref="IEEE4882.ServiceRequests">mask</see>
        '''    to use for enabling the events.</returns>
        ''' <remarks>When set clears all service registers.</remarks>
        Public Property ServiceRequestEventEnable() As IEEE4882.ServiceRequests
            Get
                Return MessageBasedSession.ServiceRequestEventEnable(Me)
            End Get
            Set(ByVal value As IEEE4882.ServiceRequests)
                MessageBasedSession.ServiceRequestEventEnable(Me) = value
            End Set
        End Property

        ''' <summary>Reads the standard event status from the instrument.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared ReadOnly Property StandardEventStatus(ByVal visaSession As VisaNS.IMessageBasedSession) As IEEE4882.StandardEvents
            Get
                If visaSession Is Nothing Then
                    Throw New ArgumentNullException("visaSession")
                End If
                Return CType(MessageBased.Session.QueryInt32(visaSession, Scpi.Syntax.StandardEventStatusQueryCommand), IEEE4882.StandardEvents)
            End Get
        End Property

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public ReadOnly Property StandardEventStatus() As IEEE4882.StandardEvents
            Get
                Return MessageBasedSession.StandardEventStatus(Me)
            End Get
        End Property

        ''' <summary>Programs or reads back the standard event request.</summary>
        ''' <param name="visaSession">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <returns>The <see cref="isr.Visa.IEEE4882.StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Shared Property StandardEventEnable(ByVal visaSession As VisaNS.IMessageBasedSession) As IEEE4882.StandardEvents
            Get
                If visaSession Is Nothing Then
                    Throw New ArgumentNullException("visaSession")
                End If
                Return CType(MessageBased.Session.QueryInt32(visaSession, Scpi.Syntax.StandardEventEnableQueryCommand), IEEE4882.StandardEvents)
            End Get
            Set(ByVal value As IEEE4882.StandardEvents)
                MessageBased.Session.Write(visaSession, String.Format(Globalization.CultureInfo.CurrentCulture, Scpi.Syntax.StandardEventEnableCommand, value))
            End Set
        End Property

        ''' <summary>Programs or reads back the standard event request.</summary>
        ''' <returns>The <see cref="isr.Visa.IEEE4882.StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Property StandardEventEnable() As IEEE4882.StandardEvents
            Get
                Return MessageBasedSession.StandardEventEnable(Me)
            End Get
            Set(ByVal value As IEEE4882.StandardEvents)
                MessageBasedSession.StandardEventEnable(Me) = value
            End Set
        End Property

#End Region

    End Class

End Namespace