''' <summary>Implements a GPIB interface with a user interface.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/07/08" by="David" revision="1.0.2926.x">
''' Created
''' </history>
Public Class GpibInterface

    Inherits isr.Visa.GpibInterface

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName">Specifies the resource name of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the resource name.</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        MyBase.New(resourceName)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="gpibBoardNumber">Specifies the board number of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the board number.</remarks>
    Public Sub New(ByVal gpibBoardNumber As Int32)

        ' instantiate the base class
        MyBase.New(gpibBoardNumber)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Returns all instruments to some default state.</summary>
    Public Overloads Overrides Sub DevicesClear()

        MyBase.DevicesClear()

        If Me._gui IsNot Nothing Then
            Me._gui.DisplayMessage("All Devices Cleared")
        End If

    End Sub

    Private _gui As InterfacePanel
    ''' <summary>Gets or sets reference to the instrument interface.</summary>
    Public Property Gui() As InterfacePanel
        Get
            Return Me._gui
        End Get
        Set(ByVal Value As InterfacePanel)
            Me._gui = Value
        End Set
    End Property

    ''' <summary>Issues an interface clear.</summary>
    Public Overloads Overrides Function InterfaceClear() As NationalInstruments.VisaNS.VisaStatusCode
        Try
            If Me._gui IsNot Nothing Then
                Dim visaStatus As NationalInstruments.VisaNS.VisaStatusCode = MyBase.SendInterfaceClear()
                If visaStatus = NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me._gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "{0} Interface cleared", MyBase.ResourceName))
                Else
                    Me._gui.DisplayMessage(isr.Visa.My.MyLibrary.BuildVisaStatusDetails(visaStatus))
                    Me._gui.DisplayMessage("Failed interface clear")
                End If
            End If
        Catch ex As NationalInstruments.VisaNS.VisaException
            If Me._gui IsNot Nothing Then
                Me._gui.DisplayMessage(ex.Message)
                Me._gui.DisplayMessage("Failed interface clear")
            End If

        End Try
    End Function

    ''' <summary>Clear the specified device.</summary>
    ''' <param name="primaryAddress">Device address on the GPIB bus.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Overrides Sub SelectiveDeviceClear(ByVal primaryAddress As Integer)

        MyBase.SelectiveDeviceClear(primaryAddress)
        '    gpib.Write(String.Format(Globalization.CultureInfo.CurrentCulture, "UNT UNL LISTEN {0} SDC UNT UNL", gpibAddress))

        If Me._gui IsNot Nothing Then
            Me._gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Cleared device at {0}", primaryAddress))
        End If

    End Sub

#End Region

End Class
