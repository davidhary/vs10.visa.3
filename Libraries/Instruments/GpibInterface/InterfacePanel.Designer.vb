<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class InterfacePanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then
                onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="statusPanel")>
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._tabControl = New System.Windows.Forms.TabControl
        Me._resourcesTabPage = New System.Windows.Forms.TabPage
        Me._resourceChooser = New isr.Controls.SelectorConnector
        Me._interfaceLabel = New System.Windows.Forms.Label
        Me._interfaceChooser = New isr.Controls.SelectorConnector
        Me._resourceChooserLabel = New System.Windows.Forms.Label
        Me._clearSelectedResourceButton = New System.Windows.Forms.Button
        Me._clearAllResourcesButton = New System.Windows.Forms.Button
        Me._messagesTabPage = New System.Windows.Forms.TabPage
        Me._messagesBox = New isr.Controls.MessagesBox
        Me._statusBar = New System.Windows.Forms.StatusBar
        Me._statusPanel = New System.Windows.Forms.StatusBarPanel
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._tabControl.SuspendLayout()
        Me._resourcesTabPage.SuspendLayout()
        Me._messagesTabPage.SuspendLayout()
        CType(Me._statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_tabControl
        '
        Me._tabControl.Controls.Add(Me._resourcesTabPage)
        Me._tabControl.Controls.Add(Me._messagesTabPage)
        Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._tabControl.Location = New System.Drawing.Point(0, 0)
        Me._tabControl.Name = "_tabControl"
        Me._tabControl.SelectedIndex = 0
        Me._tabControl.Size = New System.Drawing.Size(312, 242)
        Me._tabControl.TabIndex = 12
        '
        '_resourcesTabPage
        '
        Me._resourcesTabPage.Controls.Add(Me._resourceChooser)
        Me._resourcesTabPage.Controls.Add(Me._interfaceLabel)
        Me._resourcesTabPage.Controls.Add(Me._interfaceChooser)
        Me._resourcesTabPage.Controls.Add(Me._resourceChooserLabel)
        Me._resourcesTabPage.Controls.Add(Me._clearSelectedResourceButton)
        Me._resourcesTabPage.Controls.Add(Me._clearAllResourcesButton)
        Me._resourcesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._resourcesTabPage.Name = "_resourcesTabPage"
        Me._resourcesTabPage.Size = New System.Drawing.Size(304, 216)
        Me._resourcesTabPage.TabIndex = 0
        Me._resourcesTabPage.Text = "Resources"
        Me._resourcesTabPage.UseVisualStyleBackColor = True
        '
        '_resourceChooser
        '
        Me._resourceChooser.BackColor = System.Drawing.Color.Transparent
        Me._resourceChooser.Clearable = False
        Me._resourceChooser.Connectible = False
        Me._resourceChooser.Enabled = False
        Me._resourceChooser.Location = New System.Drawing.Point(19, 88)
        Me._resourceChooser.Name = "_resourceChooser"
        Me._resourceChooser.Size = New System.Drawing.Size(268, 25)
        Me._resourceChooser.TabIndex = 12
        '
        '_interfaceLabel
        '
        Me._interfaceLabel.AutoSize = True
        Me._interfaceLabel.Location = New System.Drawing.Point(19, 16)
        Me._interfaceLabel.Name = "_interfaceLabel"
        Me._interfaceLabel.Size = New System.Drawing.Size(60, 13)
        Me._interfaceLabel.TabIndex = 11
        Me._interfaceLabel.Text = "Interfaces: "
        Me._interfaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_interfaceChooser
        '
        Me._interfaceChooser.BackColor = System.Drawing.Color.Transparent
        Me._interfaceChooser.Location = New System.Drawing.Point(19, 32)
        Me._interfaceChooser.Name = "_interfaceChooser"
        Me._interfaceChooser.Size = New System.Drawing.Size(268, 26)
        Me._interfaceChooser.TabIndex = 10
        '
        '_resourceChooserLabel
        '
        Me._resourceChooserLabel.AutoSize = True
        Me._resourceChooserLabel.Location = New System.Drawing.Point(19, 72)
        Me._resourceChooserLabel.Name = "_resourceChooserLabel"
        Me._resourceChooserLabel.Size = New System.Drawing.Size(230, 13)
        Me._resourceChooserLabel.TabIndex = 6
        Me._resourceChooserLabel.Text = "Resources Attached to the Selected Interface: "
        Me._resourceChooserLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_clearSelectedResourceButton
        '
        Me._clearSelectedResourceButton.Enabled = False
        Me._clearSelectedResourceButton.Location = New System.Drawing.Point(19, 128)
        Me._clearSelectedResourceButton.Name = "_clearSelectedResourceButton"
        Me._clearSelectedResourceButton.Size = New System.Drawing.Size(268, 24)
        Me._clearSelectedResourceButton.TabIndex = 8
        Me._clearSelectedResourceButton.Text = "Clear Selected Resource"
        '
        '_clearAllResourcesButton
        '
        Me._clearAllResourcesButton.Enabled = False
        Me._clearAllResourcesButton.Location = New System.Drawing.Point(19, 168)
        Me._clearAllResourcesButton.Name = "_clearAllResourcesButton"
        Me._clearAllResourcesButton.Size = New System.Drawing.Size(268, 24)
        Me._clearAllResourcesButton.TabIndex = 9
        Me._clearAllResourcesButton.Text = "Clear All Resources"
        '
        '_messagesTabPage
        '
        Me._messagesTabPage.Controls.Add(Me._messagesBox)
        Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._messagesTabPage.Name = "_messagesTabPage"
        Me._messagesTabPage.Size = New System.Drawing.Size(304, 216)
        Me._messagesTabPage.TabIndex = 1
        Me._messagesTabPage.Text = "Messages"
        '
        '_messagesBox
        '
        Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._messagesBox.Delimiter = "; "
        Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._messagesBox.Location = New System.Drawing.Point(0, 0)
        Me._messagesBox.Multiline = True
        Me._messagesBox.Name = "_messagesBox"
        Me._messagesBox.PresetCount = 50
        Me._messagesBox.ReadOnly = True
        Me._messagesBox.ResetCount = 100
        Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._messagesBox.Size = New System.Drawing.Size(304, 216)
        Me._messagesBox.TabIndex = 0
        Me._messagesBox.TimeFormat = "HH:mm:ss.f"
        '
        '_statusBar
        '
        Me._statusBar.Location = New System.Drawing.Point(0, 242)
        Me._statusBar.Name = "_statusBar"
        Me._statusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me._statusPanel})
        Me._statusBar.ShowPanels = True
        Me._statusBar.Size = New System.Drawing.Size(312, 22)
        Me._statusBar.SizingGrip = False
        Me._statusBar.TabIndex = 13
        Me._statusBar.Text = "StatusBar1"
        '
        '_statusPanel
        '
        Me._statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me._statusPanel.Name = "_statusPanel"
        Me._statusPanel.Width = 312
        '
        'GpibInterfacePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._tabControl)
        Me.Controls.Add(Me._statusBar)
        Me.Name = "GpibInterfacePanel"
        Me.Size = New System.Drawing.Size(312, 264)
        Me._tabControl.ResumeLayout(False)
        Me._resourcesTabPage.ResumeLayout(False)
        Me._resourcesTabPage.PerformLayout()
        Me._messagesTabPage.ResumeLayout(False)
        Me._messagesTabPage.PerformLayout()
        CType(Me._statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _tabControl As System.Windows.Forms.TabControl
    Private WithEvents _resourcesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _resourceChooser As isr.Controls.SelectorConnector
    Private WithEvents _interfaceLabel As System.Windows.Forms.Label
    Private WithEvents _interfaceChooser As isr.Controls.SelectorConnector
    Private WithEvents _resourceChooserLabel As System.Windows.Forms.Label
    Private WithEvents _clearSelectedResourceButton As System.Windows.Forms.Button
    Private WithEvents _clearAllResourcesButton As System.Windows.Forms.Button
    Private WithEvents _messagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _messagesBox As isr.Controls.MessagesBox
    Private WithEvents _statusBar As System.Windows.Forms.StatusBar
    Private WithEvents _statusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents _toolTip As System.Windows.Forms.ToolTip

End Class
