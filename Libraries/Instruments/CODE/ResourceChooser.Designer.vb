<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class ResourceChooser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._nameSelector = New isr.Controls.SelectorConnector
        Me._cancelButton = New System.Windows.Forms.Button
        Me._acceptButton = New System.Windows.Forms.Button
        Me._nameSelectorLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        '_nameSelector
        '
        Me._nameSelector.BackColor = System.Drawing.Color.Transparent
        Me._nameSelector.Clearable = False
        Me._nameSelector.Connectible = False
        Me._nameSelector.IsConnected = False
        Me._nameSelector.Location = New System.Drawing.Point(2, 31)
        Me._nameSelector.Name = "_nameSelector"
        Me._nameSelector.SelectedName = ""
        Me._nameSelector.Size = New System.Drawing.Size(308, 28)
        Me._nameSelector.TabIndex = 28
        '
        '_cancelButton
        '
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._cancelButton.Location = New System.Drawing.Point(56, 62)
        Me._cancelButton.Name = "_cancelButton"
        Me._cancelButton.Size = New System.Drawing.Size(87, 29)
        Me._cancelButton.TabIndex = 27
        Me._cancelButton.Text = "&Cancel"
        '
        '_acceptButton
        '
        Me._acceptButton.Enabled = False
        Me._acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._acceptButton.Location = New System.Drawing.Point(159, 62)
        Me._acceptButton.Name = "_acceptButton"
        Me._acceptButton.Size = New System.Drawing.Size(87, 29)
        Me._acceptButton.TabIndex = 26
        Me._acceptButton.Text = "&OK"
        '
        '_nameSelectorLabel
        '
        Me._nameSelectorLabel.BackColor = System.Drawing.SystemColors.Control
        Me._nameSelectorLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._nameSelectorLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._nameSelectorLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._nameSelectorLabel.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._nameSelectorLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._nameSelectorLabel.Location = New System.Drawing.Point(0, 0)
        Me._nameSelectorLabel.Name = "_nameSelectorLabel"
        Me._nameSelectorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._nameSelectorLabel.Size = New System.Drawing.Size(314, 30)
        Me._nameSelectorLabel.TabIndex = 25
        Me._nameSelectorLabel.Text = "Click the search button and then select a resource  from the drop down list"
        Me._nameSelectorLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ResourceChooser
        '
        Me.AcceptButton = Me._acceptButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._cancelButton
        Me.ClientSize = New System.Drawing.Size(314, 96)
        Me.Controls.Add(Me._nameSelector)
        Me.Controls.Add(Me._cancelButton)
        Me.Controls.Add(Me._acceptButton)
        Me.Controls.Add(Me._nameSelectorLabel)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "ResourceChooser"
        Me.Text = "Select a board"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _nameSelector As isr.Controls.SelectorConnector
    Private WithEvents _cancelButton As System.Windows.Forms.Button
    Private WithEvents _acceptButton As System.Windows.Forms.Button
    Private WithEvents _nameSelectorLabel As System.Windows.Forms.Label
End Class
