''' <summary>Selects a resource.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/08/06" by="David" revision="1.0.2229.x">
''' Created
''' </history>
''' <history date="09/05/2009" by="David" revision="x.xx.3535.x">
''' Make non-singleton.
''' </history>
''' <history date="01/24/2011" by="David" revision="x.xx.4041.x">
''' Uses <see cref="isr.Core.IConnectableResource">interfaces</see>.
''' </history>
Public Class ResourceChooser

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()

    End Sub

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()
        If Me._connectableResource IsNot Nothing Then
            Me._nameSelector.DisplayNames(Me._connectableResource.FindResources())
        Else
            Me._nameSelector.DisplayNames(New String() {})
        End If
    End Sub

#End Region

#Region " PROPERTIES "

    Private _connectableResource As isr.Core.IConnectableResource
    ''' <summary>
    ''' Gets or sets reference to the VISA instrument implementing
    ''' <see cref="isr.Core.IConnectResource">connectible</see> and <see cref="isr.Core.IResettable">resettable</see>
    ''' interfaces.
    ''' </summary>
    Protected Property ConnectableResource() As isr.Core.IConnectableResource
        Get
            Return Me._connectableResource
        End Get
        Set(ByVal value As isr.Core.IConnectableResource)
            Me._connectableResource = value
            If Me._connectableResource IsNot Nothing Then
                Me._nameSelector.DisplayNames(Me._connectableResource.FindResources())
            Else
                Me._nameSelector.DisplayNames(New String() {})
            End If
        End Set
    End Property

    ''' <summary>Returns the selected Resource name or gets the
    '''   last resource from the caller.</summary>
    Public Property SelectedResourceName() As String
        Get
            Return Me._nameSelector.SelectedName
        End Get
        Set(ByVal Value As String)
            Me._nameSelector.SelectedName = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the caption just above the name selector.
    ''' </summary>
    Public Property SelectorCaption() As String
        Get
            Return Me._nameSelectorLabel.Text
        End Get
        Set(ByVal value As String)
            Me._nameSelectorLabel.Text = value
        End Set
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    Private Sub _acceptButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _acceptButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub _cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _cancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Initialize and set the user interface
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _nameSelector_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _nameSelector.DoubleClick
        If String.IsNullOrWhiteSpace(Me.SelectedResourceName) Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    ''' <summary>
    ''' Update the names.
    ''' </summary>
    Private Sub _nameSelector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _nameSelector.FindNames
        If Me._connectableResource IsNot Nothing Then
            Me._nameSelector.DisplayNames(Me._connectableResource.FindResources())
        Else
            Me._nameSelector.DisplayNames(New String() {})
        End If
    End Sub

    Private Sub _nameSelector_NameSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _nameSelector.Selected
        Me._acceptButton.Enabled = Me._nameSelector.SelectedName.Length > 0
    End Sub

#End Region

End Class