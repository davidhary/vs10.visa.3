Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments

Namespace R2D2

    ''' <summary>Provides a Core. for non-SCPI Serial instruments.</summary>
    ''' <license>
    ''' (c) 2096 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history> 
    Public Class SerialInstrument
        Inherits DeviceBase
        Implements IDisposable, IInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Protected Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle, HardwareInterfaceType.Serial)
            Me._UsingDevices = True

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        ' dispose of 'status' properties
                        Me._Id = String.Empty

                        If Me._session IsNot Nothing Then
                            If Me.IsConnected Then
                                ' remove the service request handler ignoring exceptions.
                            End If
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources
                End If

            Finally

                MyBase.Dispose(disposing)

            End Try
        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(SerialInstrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I Resettable "

        Public Overrides Function ClearResource() As Boolean
            Return ResetAndClear()
        End Function

        ''' <summary>Selectively clears the instrument.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearActiveState() As Boolean

            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Try
                    Me._session.Clear()
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING ACTIVE STATE",
                                              "Timeout occurred when clearing active state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                              "VISA Exception occurred when clearing active state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                          "Exception occurred when clearing active state. Details: {0}.", ex)
                End Try
                Return Me._session.LastStatus = VisaStatusCode.Success
            Else
                Return True
            End If

        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            If Me._session IsNot Nothing Then

                Try
                    ' Clear the device status.  This will set all the standard subsystem properties.
                    Me._session.Clear()
                    Me.OnMessageAvailable(TraceEventType.Verbose, "CLEARED EXECUTION STATE", "Cleared execution state.")
                    Return True
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING EXECUTION STATE",
                                              "Timeout occurred when clearing execution state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                              "VISA Exception occurred when clearing execution state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                          "Exception occurred when clearing execution state. Details: {0}.", ex)
                End Try
                Return False
            Else
                ' Clear the event arguments
                Me._lastServiceEventArgs = Nothing

                ' clear the error and information annunciators
                Me.OnMessageAvailable(TraceEventType.Verbose, "STATUS CLEARED", "Status Cleared")
                Return True
            End If


        End Function

#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>
        ''' Gets the connection status.
        ''' The device is connected if it has a valid VISA session.
        ''' </summary>
        ''' <value><c>Connected</c> is a <see cref="Boolean" /> property that is True if the
        ''' connected (open).</value>
        ''' <history date="02/07/06" by="David" revision="1.00.2228.x">
        ''' Rename
        '''   </history>
        Public Overrides ReadOnly Property IsConnected As Boolean
            Get
                Return Me._session IsNot Nothing
            End Get
        End Property

        Public Overloads Overrides Function Connect(ByVal address As Integer) As Boolean
            ' TO_DO: build resource name.
            Return Me.Connect("ASRL" & CStr(address))
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
        Public Overloads Overrides Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
            Me.ResourceTitle = resourceTitle
            Return Me.Connect(resourceName)
        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Me.ResourceName = resourceName
            Dim lastAction As String = String.Empty

            Try

                If Me.UsingDevices Then

                    ' open a session to this instrument
                    Me._session = New NationalInstruments.VisaNS.SerialSession(resourceName)

                    If Me._session Is Nothing Then

                        Me.Disconnect()

                    Else

                        ' clear status and disable service request on all operations.
                        Me.ClearExecutionState()

                        ' issue a selective device clear and total clear of the instrument.
                        Me.ResetAndClear()

                        Me.OnMessageAvailable(TraceEventType.Information, "OPENED", "Opened.")

                        lastAction = "connected"
                        MyBase.Connect(resourceName)

                    End If

                Else

                    lastAction = "connected"
                    MyBase.Connect(resourceName)

                End If

            Catch ex As Exception

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CONNECTING", "Exception occurred {0}. Details: {1}", lastAction, ex)

            End Try

            Return Me.IsConnected

        End Function

        ''' <summary>Disconnects this instance.</summary>
        ''' <returns><c>True</c> if the instance disconnected.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function Disconnect() As Boolean

            Try
                If Me._session IsNot Nothing Then
                    Me._session.Dispose()
                    Me._session = Nothing
                End If
                Return MyBase.Disconnect()
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", "Exception occurred disconnecting. Details: {0}", ex)
                Try
                    If Me._session IsNot Nothing Then
                        Me._session.Dispose()
                        Me._session = Nothing
                    End If
                Catch
                End Try
            End Try
            Return Not Me.IsConnected

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            Try

                Dim affirmative As Boolean = True

                ' MUST CLEAR IN THIS ORDER:

                ' clear the device
                affirmative = affirmative AndAlso Me.ClearActiveState()

                ' reset the device and set default properties
                affirmative = affirmative AndAlso Me.ResetKnownState()

                ' Clear the device Status and set more defaults
                affirmative = affirmative AndAlso Me.ClearExecutionState()

                Me.OnMessageAvailable(TraceEventType.Information, "RESET AND CLEARED", "Reset and cleared.")

                Return True

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING AND CLEARING", "Exception occurred resetting and clearing. Details: {0}", ex)

                Return False

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session.  Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If Me._session Is Nothing Then
                Return True
            End If

            Dim newTimeout As Int32 = Me._session.Timeout
            Me._session.Timeout = Math.Max(Me._session.Timeout, timeout)
            Dim outcome As Boolean = ResetAndClear()
            Me._session.Timeout = newTimeout
            Return outcome

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overrides Function ResetKnownState() As Boolean
            Return True
        End Function


        ''' <summary>
        ''' True when using actual devices or False for running the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean Implements IInstrument.UsingDevices

#End Region

#Region " INSTRUMENT PROPERTIES "

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public Shared ReadOnly Property HadError() As Boolean
            Get
                Return False
            End Get
        End Property

        ''' <summary>
        ''' Returns the error queue and event status register (ESR) report.
        ''' </summary>
        Public ReadOnly Property DeviceErrors() As String Implements IInstrument.DeviceErrors
            Get
                Return "NOT IMPLEMENTED YET"
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>
        ''' Parses the instrument firmware revision.
        ''' </summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        ''' <exception cref="System.ArgumentNullException">revision</exception>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If id Is Nothing Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            Me._manufacturerName = idItems(0)

            ' model: MODEL 2420
            Me._model = idItems(1)

            ' Serial Number: 0669977
            Me._serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            Me._firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(Me._firmwareRevision)

        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & My.MyLibrary.ImmediateIf(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & My.MyLibrary.ImmediateIf(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As NationalInstruments.VisaNS.SerialSessionEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="NationalInstruments.VisaNS.SerialSessionEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As NationalInstruments.VisaNS.SerialSessionEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As NationalInstruments.VisaNS.SerialSessionEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return Me._manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return Me._model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return Me._serialNumber
            End Get
        End Property

#End Region

#Region " SERIAL VISA INTERFACE METHODS "

        ''' <summary>Issues an interface clear.</summary>
        Public Shared Sub InterfaceClear()

        End Sub

#End Region

#Region " COMMON COMMANDS "

        Private _Id As String
        ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
        Public ReadOnly Property Id() As String
            Get
                If String.IsNullOrWhiteSpace(Me._Id) Then
                    If Me._session Is Nothing Then
                        Me._Id = "Serial Corp., Model 1, 1901, A"
                    Else
                        Me._Id = Me._session.Query(isr.Visa.Ieee4882.Syntax.IdentifyQueryCommand)
                    End If
                    Me.ParseInstrumentId(Me._Id)
                End If
                Return Me._Id
            End Get
        End Property

#End Region

#Region " SERIAL SESSION METHODS AND PROPERTIES "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until ((Me._session.ReadStatusByte And StatusByteFlags.MessageAvailable) = StatusByteFlags.MessageAvailable) OrElse timedOut

                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me._session.ReadString()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Information, "Flushed '{0}'", My.MyLibrary.InsertCommonEscapeSequences(flushed))
                        End If

                    Else

                        Me._session.ReadString()

                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.DiscardUnreadData

            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.FlushRead

            Try
                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED FLUSHING READ BUFFER", "Exception occurred flushing read buffer. VISA status={0}. Details: {11}.", Me.LastVisaStatus, ex)
            End Try

        End Function

        Private _lastCommandTime As DateTime = DateTime.Now
        ''' <summary>Gets or sets the time of the last command for delay purposes.
        ''' </summary>
        Public ReadOnly Property LastCommandTime() As DateTime
            Get
                Return Me._lastCommandTime
            End Get
        End Property

        Private _minimumCommandInterval As Int32 = 200
        ''' <summary>Gets or sets the time between commands in milliseconds.
        ''' </summary>
        Public Property MinimumCommandInterval() As Int32
            Get
                Return Me._minimumCommandInterval
            End Get
            Set(ByVal Value As Int32)
                Me._minimumCommandInterval = Value
            End Set
        End Property

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval()
            AwaitCommandInterval(Me._minimumCommandInterval)
        End Sub

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval(ByVal milliseconds As Int32)

            Do While DateTime.Compare(DateTime.Now, Me._lastCommandTime.AddMilliseconds(milliseconds)) < 0
                '        Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(2)
            Loop
            Me._lastCommandTime = DateTime.Now
        End Sub

        Private _session As NationalInstruments.VisaNS.SerialSession
        ''' <summary>Gets or sets reference to the Serial session.</summary>
        Public ReadOnly Property SerialSession() As NationalInstruments.VisaNS.SerialSession
            Get
                Return Me._session
            End Get
        End Property

        ''' <summary>Sends a query and gets back the result.</summary>
        ''' <param name="output">The main command.</param>
        Public Function QueryTerminate(ByVal output As String) As String
            If Me._session IsNot Nothing Then
                output &= Convert.ToChar(Me._session.TerminationCharacter)
            End If
            Return Me.QueryTrimNewLine(output)
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimNewLine(ByVal question As String) As String
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBasedSession.QueryTrimNewLine(Me._session, question)
            End If
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IInstrument.QueryTrimEnd
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBasedSession.QueryTrimEnd(Me._session, question)
            End If
        End Function

        ''' <summary>
        ''' Queries the instrument and returns a string save the termination character.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IInstrument.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return MessageBasedSession.QueryTrimEnd(Me._session, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Function

        ''' <summary>Writes output and terminate.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub WriteTerminate(ByVal output As String)
            If Me._session IsNot Nothing Then
                output &= Convert.ToChar(Me._session.TerminationCharacter)
                Me._session.Write(output)
            End If
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If Me._session IsNot Nothing Then
                Me._session.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If Me._session IsNot Nothing Then
                Me._session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.LastStatus < 0
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.LastStatus > 0
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IInstrument.LastVisaStatus '  NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me._session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me._session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IInstrument.LastVisaStatusDetails
            Get
                Return My.MyLibrary.BuildVisaStatusDetails(CType(Me._session.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
            End Get
        End Property

#End Region

    End Class

End Namespace

