Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Namespace R2D2

    ''' <summary>Provides a Core. for non-SCPI GPIB instruments.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class GpibInstrument
        Inherits DeviceBase
        Implements IDisposable, IInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle, HardwareInterfaceType.Gpib)

            ' default to using devices
            Me._UsingDevices = True

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        ' dispose of 'status' properties
                        Me._Id = String.Empty

                        If ServiceRequestEvent IsNot Nothing Then
                            For Each d As [Delegate] In ServiceRequestEvent.GetInvocationList
                                RemoveHandler ServiceRequest, CType(d, Global.System.EventHandler(Of ServiceEventArgs))
                            Next
                        End If

                        If Me._session IsNot Nothing Then
                            If Me.IsConnected Then
                                ' remove the service request handler ignoring exceptions.
                                RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If

                        If Me._gpibInterface IsNot Nothing Then
                            Me._gpibInterface.Dispose()
                            Me._gpibInterface = Nothing
                        End If

                    End If

                    ' remove the reference to the synchronizer
                    ' Me._synchronizer = Nothing

                    ' Free shared unmanaged resources
                End If

            Finally

                ' set the sentinel indicating that the class was disposed.
                MyBase.Dispose(disposing)

            End Try
        End Sub

#End Region

#Region " I ANON PUBLISHER "

        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(GpibInstrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>
        ''' Gets the connection status.
        ''' The device is connected if it has a valid VISA session.
        ''' </summary>
        ''' <value><c>Connected</c> is a <see cref="Boolean" /> property that is True if the
        ''' connected (open).</value>
        ''' <history date="02/07/06" by="David" revision="1.00.2228.x">
        ''' Rename
        '''   </history>
        Public Overrides ReadOnly Property IsConnected As Boolean
            Get
                Return Me._session IsNot Nothing
            End Get
        End Property

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception> 
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Dim lastAction As String = String.Empty
            Me.ResourceName = resourceName

            Try

                If Me.UsingDevices Then

                    lastAction = "opening VISA session"

                    ' open a GPIB session to this instrument
                    Me._session = New isr.Visa.GpibSession(resourceName)

                    If Me._session Is Nothing Then

                        Me.Disconnect()

                    Else

                        lastAction = "setting long timeout"
                        Dim preConnectTimeout As Int32 = Me._session.Timeout
                        Me._session.Timeout = Me.ConnectTimeout

                        lastAction = "clearing status"

                        ' clear status and disable service request on all operations.
                        Me.ClearExecutionState()
                        Me.ServiceRequestEventEnable = 0 '  ServiceRequests.None

                        lastAction = "registering event handler"

                        ' register the handler before enabling the event
                        AddHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                        Me._session.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)

                        lastAction = "reset clear"

                        ' issue a selective device clear and total clear of the instrument.
                        Me.ResetAndClear()

                        Me.OnMessageAvailable(TraceEventType.Information, "CONNECTED", "connected.")

                        ' restore session timeout 
                        Me._session.Timeout = preConnectTimeout

                        lastAction = "connected"
                        MyBase.Connect(resourceName)

                    End If

                Else

                    lastAction = "connected"
                    MyBase.Connect(resourceName)

                End If

            Catch ex As Exception

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                ' throw an exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CONNECTING", "Exception occurred {0}. Details: {1}", lastAction, ex)

            End Try

            Return Me.IsConnected

        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="address">Specifies the primary address of the instrument.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overloads Overrides Function Connect(ByVal address As Integer) As Boolean
            Return Me.Connect(GpibSession.BuildResourceName(address))
        End Function

        ''' <summary>Opens a GPIB VISA interface for the specified resource.</summary>
        ''' <param name="boardNumber">Specifies the GPIB resource board name.</param>
        ''' <param name="address">Specifies the GPIB resource address.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overridable Overloads Function Connect(ByVal boardNumber As Integer, ByVal address As Integer) As Boolean
            Return Me.Connect(GpibSession.BuildResourceName(boardNumber, address))
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
        Public Overloads Overrides Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
            Me.ResourceTitle = resourceTitle
            Return Me.Connect(resourceName)
        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function Disconnect() As Boolean

            Try
                If Me._session IsNot Nothing Then
                    Me._session.Dispose()
                    Me._session = Nothing
                End If
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", "Exception occurred disconnecting. Details: {0}", ex)
            End Try
            Return MyBase.Disconnect()

        End Function

        ''' <summary>
        ''' True when using actual devices or False for running the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean Implements IInstrument.UsingDevices

#End Region

#Region " I Resettable "

        Public Overrides Function ClearResource() As Boolean
            Return ResetAndClear()
        End Function

        ''' <summary>Selectively clears the instrument.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearActiveState() As Boolean

            If Me._session IsNot Nothing Then
                Try
                    Me._session.ClearActiveState()
                    Return True
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING ACTIVE STATE",
                                              "Timeout occurred when clearing active state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                              "VISA Exception occurred when clearing active state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                          "Exception occurred when clearing active state. Details: {0}.", ex)
                End Try
                Return False
            Else
                Return False
            End If

        End Function


        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            If Me._session Is Nothing Then
                Return False
            End If
            Try
                ' Clear the device status.  This will set all the
                ' standard subsystem properties.
                Me._session.ClearExecutionState()

                ' Clear the event arguments
                Me._lastServiceEventArgs = New ServiceEventArgs(Me._session)

                Me.OnMessageAvailable(TraceEventType.Verbose, "CLEARED EXECUTION STATE", "Cleared execution state.")
                Return True
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING EXECUTION STATE",
                                          "Timeout occurred when clearing execution state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                Else
                    Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                          "VISA Exception occurred when clearing execution state. Details: {0}.", ex)
                End If
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                      "Exception occurred when clearing execution state. Details: {0}.", ex)
            End Try
            Return False

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <history>
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            If Not Me.UsingDevices Then
                Return True
            End If

            Try

                ' clear the device
                Me.ClearActiveState()

                ' reset the device and set default properties
                Me.ResetKnownState()

                ' Clear the device Status and set more defaults
                Me.ClearExecutionState()

                ' preset the event registers to their power on conditions
                ' including the event transition registers.
                ' Scpi.StatusSubsystem.Preset(Me._GpibSession)

                Me.OnMessageAvailable(TraceEventType.Information, "RESET AND CLEARED", "Reset and cleared.")

                Return True

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING AND CLEARING",
                                      "Exception occurred resetting and clearing. Details: {0}", ex)

                Return False

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session.  Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If Not Me.UsingDevices Then
                Return True
            End If

            Dim newTimeout As Int32 = Me._session.Timeout
            Me._session.Timeout = Math.Max(Me._session.Timeout, timeout)
            Dim outcome As Boolean = ResetAndClear()
            Me._session.Timeout = newTimeout
            Return outcome

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ResetKnownState() As Boolean

            If Me._session IsNot Nothing Then
                Try
                    Me._session.ResetKnownState()
                    Return True
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT RESETTING KNOWN STATE",
                                              "Timeout occurred when resetting known state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                              "VISA Exception occurred when resetting known state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                          "Exception occurred when resetting known state. Details: {0}.", ex)
                End Try
                Return False
            Else
                Return False
            End If

        End Function

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.IsLastVisaOperationFailed
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.IsLastVisaOperationWarning
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IInstrument.LastVisaStatus '  NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me._session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me._session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IInstrument.LastVisaStatusDetails
            Get
                Return Me._session.BuildLastVisaStatusDetails()
            End Get
        End Property

#End Region

#Region " INSTRUMENT METHODS "

        ''' <summary>
        ''' Returns the error queue and event status register (ESR) report.
        ''' </summary>
        Public ReadOnly Property DeviceErrors() As String Implements IInstrument.DeviceErrors
            Get
                Return "NOT IMPLEMENTED YET"
            End Get
        End Property

#End Region

#Region " INSTRUMENT PROPERTIES "

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public ReadOnly Property HadError() As Boolean
            Get
                Return Me._lastServiceEventArgs.HasError OrElse Me._session.HasError(isr.Visa.Ieee4882.ServiceRequests.ErrorAvailable)
#If DEBUG Then
                ' In debug mode, make sure to wait for the end of service
                ' request processing to get the error immediately after
                ' the instrument reports it.  In this mode, we expect to have
                ' syntax error that the instrument is unlikely to be happy about.
                ' once in release mode, the incidences are lower and we can wait for
                ' the error rather than slowing operations due to this delay
                Return Me.HadError(10)
#Else
        Windows.Forms.Application.DoEvents()
        Return Me._lastServiceEventArgs.HasError
#End If
            End Get
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        ''' <param name="timeout">Time to wait before returning the error.</param>
        Private ReadOnly Property HadError(ByVal timeout As Int32) As Boolean
            Get
                ' add a couple of milliseconds wait
                Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, timeout))
                Do
                    Threading.Thread.Sleep(Math.Min(10, timeout))
                Loop While Me._lastServiceEventArgs.ServicingRequest And (Date.Now <= endTime)
                Return Me._lastServiceEventArgs.HasError
            End Get
        End Property

#End Region

#Region " RESOURCE MANAGEMENT "

        ''' <summary>
        ''' Returns true if having a valid primary address.
        ''' </summary>
        ''' <param name="address">The device primary address</param>
        Public Shared Function IsValidPrimaryAddress(ByVal address As Integer) As Boolean
            Return (address >= 1) AndAlso (address <= 31)
        End Function

        ''' <summary>Gets or sets the primary address of the instrument on the GPIB bus.</summary>
        Public ReadOnly Property PrimaryAddress() As Int32
            Get
                If Me._session Is Nothing Then
                    Return 0
                Else
                    Return Me._session.PrimaryAddress
                End If
            End Get
        End Property

        ''' <summary>Gets the board number.</summary>
        Public ReadOnly Property BoardNumber() As Integer
            Get
                If Me._session Is Nothing Then
                    Return 0
                Else
                    Return Me._session.BoardNumber
                End If
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>
        ''' Parses the instrument firmware revision.
        ''' </summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        ''' <exception cref="System.ArgumentNullException">revision</exception>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If id Is Nothing Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            Me._manufacturerName = idItems(0)

            ' model: MODEL 2420
            Me._model = idItems(1)

            ' Serial Number: 0669977
            Me._serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            Me._firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(Me._firmwareRevision)

        End Sub

        ''' <summary>Queries the GPIB instrument and returns a Boolean value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            If Not Me.UsingDevices Then
                Return True
            End If
            Return Me._session.QueryBoolean(question)
        End Function

        ''' <summary>Queries the GPIB instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.QueryDouble(question)
        End Function

        ''' <summary>Queries the GPIB instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.QueryInt32(question)
        End Function

        ''' <summary>Queries the GPIB instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.QueryInfInt32(question)
        End Function

        ''' <summary>Reads the GPIB instrument and returns a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            If Not Me.UsingDevices Then
                Return True
            End If
            Return Me._session.ReadBoolean()
        End Function

        ''' <summary>Reads the GPIB instrument and returns a double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.ReadDouble()
        End Function

        ''' <summary>Reads the GPIB instrument and returns an Int32 value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.ReadInt32()
        End Function

        ''' <summary>Reads the GPIB instrument and returns an Int32 value.</summary>
        Public Function ReadInfInt32() As Nullable(Of Int32)
            If Not Me.UsingDevices Then
                Return 0
            End If
            Return Me._session.ReadInfInt32()
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & My.MyLibrary.ImmediateIf(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & My.MyLibrary.ImmediateIf(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As ServiceEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="isr.Visa.R2D2.ServiceEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As ServiceEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As ServiceEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return Me._manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return Me._model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return Me._serialNumber
            End Get
        End Property

#End Region

#Region " GPIB VISA INTERFACE METHODS "

        Private _gpibInterface As VisaNS.GpibInterface
        ''' <summary>Gets or sets reference to the GPIB interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As VisaNS.GpibInterface
            Get
                If Me._gpibInterface Is Nothing AndAlso Me.UsingDevices Then
                    Me._gpibInterface = isr.Visa.GpibInterface.OpenInterface(Me._session.HardwareInterfaceName & "::INTFC")
                End If
                Return Me._gpibInterface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Sub InterfaceClear()

            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SendInterfaceClear()
            End If

        End Sub

#End Region

#Region " COMMON COMMANDS "

        Private _Id As String
        ''' <summary>Queries the GPIB instrument and returns the string save the termination character.</summary>
        Public ReadOnly Property Id() As String
            Get
                If String.IsNullOrWhiteSpace(Me._Id) Then
                    If Me._session Is Nothing Then
                        Me._Id = "GPIB Corp., Model 1, 123, A"
                    Else
                        Me._Id = Me._session.ReadIdentity
                    End If
                    Me.ParseInstrumentId(Me._Id)
                End If
                Return Me._Id
            End Get
        End Property

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function IsOpc() As Boolean
            If Me._session Is Nothing Then
                Return True
            End If
            Me._lastServiceEventArgs.OperationCompleted = Me._session.OperationCompleted
            Return Me._lastServiceEventArgs.OperationCompleted
        End Function

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public ReadOnly Property ServiceRequestEventStatus() As isr.Visa.Ieee4882.ServiceRequests
            Get
                If Me._session Is Nothing Then
                    Return Ieee4882.ServiceRequests.None
                Else
                    Return Me._session.ServiceRequestEventStatus()
                End If
            End Get
        End Property

        ''' <summary>Programs or reads back the service request event request.</summary>
        ''' <returns>The <see cref="ServiceRequest">mask</see>
        '''    to use for enabling the events.</returns>
        ''' <remarks>Make sure to issue <see cref="ClearExecutionState">status clear</see> before
        '''   setting the service request.</remarks>
        Public Property ServiceRequestEventEnable() As isr.Visa.Ieee4882.ServiceRequests
            Get
                If Me._session IsNot Nothing Then
                    Return Me._session.ServiceRequestEventEnable()
                End If
            End Get
            Set(ByVal value As isr.Visa.Ieee4882.ServiceRequests)
                If Me._session IsNot Nothing Then
                    Me._session.ServiceRequestEventEnable() = value
                End If
            End Set
        End Property

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public ReadOnly Property StandardEventStatus() As isr.Visa.Ieee4882.StandardEvents
            Get
                If Me._session Is Nothing Then
                    Return Ieee4882.StandardEvents.None
                Else
                    Return Me._session.StandardEventStatus()
                End If
            End Get
        End Property

        ''' <summary>Programs or reads back the standard event request.</summary>
        ''' <returns>The <see cref="isr.Visa.Ieee4882.StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Property StandardEventEnable() As isr.Visa.Ieee4882.StandardEvents
            Get
                If Me._session IsNot Nothing Then
                    Return Me._session.StandardEventEnable()
                End If
            End Get
            Set(ByVal value As isr.Visa.Ieee4882.StandardEvents)
                If Me._session IsNot Nothing Then
                    Me._session.StandardEventEnable() = value
                End If
            End Set
        End Property

#End Region

#Region " GPIB VISA SESSION METHODS AND PROPERTIES "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until ((Me.LastServiceEventArgs.ServiceRequestStatus And Ieee4882.ServiceRequests.MessageAvailable) =
                          Ieee4882.ServiceRequests.MessageAvailable) OrElse timedOut
                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me._session.Reader.ReadLine()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Information, "Flushed '{0}'", My.MyLibrary.InsertCommonEscapeSequences(flushed))
                        End If

                    Else

                        Me._session.DiscardUnreadData(16)

                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        Public Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.DiscardUnreadData
            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)
        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.FlushRead

            Try
                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)
            Catch ex As Exception
                Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED FLUSHING READ BUFFER", "Exception occurred flushing read buffer. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
            End Try

        End Function

        Private _session As isr.Visa.GpibSession
        ''' <summary>Gets or sets reference to the GPIB session.</summary>
        Public ReadOnly Property GpibSession() As isr.Visa.GpibSession
            Get
                Return Me._session
            End Get
        End Property

        ''' <summary>Queries the GPIB instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IInstrument.QueryTrimEnd
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return Me._session.QueryTrimEnd(question)
            End If
        End Function

        ''' <summary>
        ''' Queries the instrument and returns a string save the termination character.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IInstrument.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return Me._session.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Function

        ''' <summary>Reads a string from GPIB instrument and returns the string save the termination character.</summary>
        Public Function ReadStringTrimEnd() As String
            If Me._session Is Nothing Then
                Return String.Empty
            Else
                Return Me._session.ReadLineTrimEnd()
            End If
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If Me._session IsNot Nothing Then
                Me._session.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If Me._session IsNot Nothing Then
                Me._session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Raised upon receiving a service request.  This event does not expose the
        '''   internal service request arguments directly.</summary>
        Public Event ServiceRequest As EventHandler(Of ServiceEventArgs)

        ''' <summary>Raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnServiceRequest(ByVal e As ServiceEventArgs)

            If e Is Nothing Then
            ElseIf e.HasError Then

                Me.OnMessageAvailable(TraceEventType.Warning, "INSTRUMENT ERROR AVAILABLE",
                                      "Instrument reported an error:{0}{1}", Environment.NewLine, e.LastError)

            Else

                Dim elapsedMilliseconds As Double = e.OperationElapsedTime.TotalMilliseconds()
                Me.OnMessageAvailable(TraceEventType.Information, "OPERATION COMPLETED", "Operation completed in {0}ms", elapsedMilliseconds)

            End If

            Dim evt As EventHandler(Of ServiceEventArgs) = Me.ServiceRequestEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, e)
            If e IsNot Nothing Then
                e.ServicingRequest = False
            End If

        End Sub

        ''' <summary>Handles service requests from the instrument.</summary>
        Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

            If Not Me._lastServiceEventArgs.ServicingRequest Then
                ' do not initialize the last service event arguments because this is done with Clear Status.
                Me._lastServiceEventArgs.ServicingRequest = True
                Me._lastServiceEventArgs.ProcessServiceRequest()
                Me._lastServiceEventArgs.ReadRegisters()
                If Me._lastServiceEventArgs.HasError Then
                    ' disable recurring service requests
                    'Me.ServiceRequestEventEnable = ServiceRequests.None
                Else
                    ' check if we have consecutive requests.
                    If Me._lastServiceEventArgs.RequestCount > 10 Then
                        ' disable recurring service requests
                        Me.ServiceRequestEventEnable = 0
                        Me.OnMessageAvailable(TraceEventType.Information, "CONSECUTIVE SERVICE REQUESTS HIT LIMIT",
                                              "Instrument had over 10 consecutive service requests")
                    End If
                End If
                OnServiceRequest(Me._lastServiceEventArgs)
            End If

        End Sub

#End Region

    End Class

End Namespace
