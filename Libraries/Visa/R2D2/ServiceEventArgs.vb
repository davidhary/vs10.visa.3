Namespace R2D2

    ''' <summary>Provides event arguments for instrument events.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class ServiceEventArgs
        Inherits isr.Visa.BaseServiceEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New(ByVal session As GpibSession)

            ' instantiate the base class
            MyBase.New()
            Me._session = session
        End Sub

        ''' <summary>Constructs this class with a status message.</summary>
        ''' <param name="eventMessage">Specifies the event message.</param>
        ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
        Public Sub New(ByVal eventMessage As String, ByVal session As GpibSession)

            ' instantiate the base class
            MyBase.New(eventMessage)
            Me._session = session

        End Sub

#End Region

#Region " CONTROLLER "

        Private _session As GpibSession

#End Region

#Region " READ REGISTERS AND MESSAGES "

        ''' <summary>
        ''' Reads a message from the instrument.
        ''' </summary>
        Protected Overrides Function ReadLineTrimEnd() As String
            Return Me._session.ReadLineTrimEnd
        End Function

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadMeasurementEventStatus() As Integer
            Return Me._session.QueryInteger("STAT:MEAS:EVEN?").Value
        End Function

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadOperationEventStatus() As Integer
            Return Me._session.QueryInteger("STAT:OPER:EVEN?").Value
        End Function

        ''' <summary>
        ''' Reads the Questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadQuestionableEventStatus() As Integer
            Return Me._session.QueryInteger("STAT:QUES:EVEN?").Value
        End Function

        ''' <summary>
        ''' Reads the standard event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadStandardEventStatus() As Integer
            Return Me._session.StandardEventStatus()
        End Function

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Protected Overrides Function ReadStatusByte() As Integer
            Return Me._session.ReadStatusByte
        End Function

        ''' <summary>
        ''' Reads the last error.
        ''' </summary>
        ''' <returns>System.String.</returns>
        Protected Overrides Function ReadLastError() As String
            Return Me._session.QueryTrimEnd(":SYST:ERR?")
        End Function

        ''' <summary>
        ''' Fetches the error queue.
        ''' </summary>
        ''' <returns>System.String.</returns>
        Protected Overrides Function FetchErrorQueue() As String
            Dim messageBuilder As New System.Text.StringBuilder
            Do
                ProcessServiceRequestRegister()
                If Me.HasError Then
                    If messageBuilder.Length > 0 Then
                        messageBuilder.Append(Environment.NewLine)
                    End If
                    messageBuilder.Append(Me._session.Query(":STAT:QUE?"))
                End If
            Loop Until Not Me.HasError
            Return messageBuilder.ToString
        End Function

#End Region

    End Class

End Namespace
