Imports NationalInstruments

''' <summary>Implements a Basic interface for VISA GPIB Interface.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/08/06" by="David" revision="1.0.2229.x">
''' Created
''' </history>
Public Class GpibInterface

    ' based on the connect base inheritable class
    Inherits NationalInstruments.VisaNS.GpibInterface

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="GpibSession" /> class.
    ''' </summary>
    ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
    ''' <param name="connectTimeout">The connect timeout.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal resourceName As String, ByVal connectTimeout As Integer)

        ' instantiate the base class
        MyBase.New(resourceName, VisaNS.AccessModes.NoLock, connectTimeout, False)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName">Specifies the resource name of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the resource name.</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        MyBase.New(resourceName, VisaNS.AccessModes.NoLock, 2000, False)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="gpibBoardNumber">Specifies the board number of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the board number.</remarks>
    Public Sub New(ByVal gpibBoardNumber As Int32)

        ' instantiate the base class
        Me.New(GpibInterface.BuildInterfaceResourceName(gpibBoardNumber))

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " RESOURCE MANAGEMENT "

    ''' <summary>
    ''' Specifies the standard VISA name for a GPIB interface.
    ''' </summary>
    ''' <remarks>
    ''' This is the same as the non-static 
    ''' <see cref="Session.HardwareInterfaceName">GPIB session hardware interface name.</see>
    ''' </remarks>
    Public Const InterfaceName As String = "GPIB"

    ''' <summary>
    ''' Specifies the standard VISA format for a GPIB interface.
    ''' </summary>
    Public Const InterfaceFormat As String = "GPIB{0}::INTFC"

    ''' <summary>Returns a GPIB Interface name.</summary>
    ''' <param name="boardNumber">A GPIB instrument address.</param>
    ''' <returns>A VISA resource interface name in the form "[GPIB][BoardNumber]::INTFC"
    ''' e.g., "GPIB0::INTFC"
    ''' </returns>
    Public Shared Function BuildInterfaceResourceName(ByVal boardNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, GpibInterface.InterfaceFormat, boardNumber)
    End Function

    ''' <summary>
    ''' Returns true if we have interfaces for boards 0 through 3.
    ''' </summary>
    Public Shared Function HasBoards() As Boolean
        Return HasBoards(0, 3, 10000)
    End Function

    ''' <summary>
    ''' Returns true if we have interfaces or if an error occurred allowing to enter specific interface name.
    ''' </summary>
    Public Shared Function HasInterfaces() As Boolean
        Return (My.MyLibrary.LocalInterfaceNames(GpibInterface.InterfaceName).Length > 0) OrElse My.MyLibrary.FailedFindLocalResources.Value
    End Function

    ''' <summary>
    ''' Returns true if we have interfaces for boards 0 through 3.
    ''' </summary>
    ''' <param name="timeout">Specifies timeout for looking for the interface.</param>
    Public Shared Function HasBoards(ByVal timeout As Integer) As Boolean
        Return HasBoards(0, 3, timeout)
    End Function

    ''' <summary>
    ''' Returns true if we have interfaces for the specified board range.
    ''' </summary>
    ''' <param name="firstBoardNumber">First board number</param>
    ''' <param name="lastBoardNumber">Last board number</param>
    ''' <param name="timeout">Specifies timeout for looking for the interface.</param>
    Public Shared Function HasBoards(ByVal firstBoardNumber As Integer, ByVal lastBoardNumber As Integer, ByVal timeout As Integer) As Boolean
        For boardNumber As Integer = firstBoardNumber To lastBoardNumber
            If GpibInterface.TryConnect(boardNumber, timeout) Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Returns true if able to connect to the specified VISA interface resource.
    ''' </summary>
    ''' <param name="interfaceResourceName">A VISA interface resource Name.</param>
    Public Shared Function HasInterface(ByVal interfaceResourceName As String) As Boolean
        Return TryConnect(interfaceResourceName, 10000)
    End Function

    ''' <summary>Returns a string array of all local GPIB interfaces.</summary>
    Public Shared Function LocalInterfaceNames() As String()
        If GpibInterface.HasBoards() Then
            Return My.MyLibrary.LocalInterfaceNames(GpibInterface.InterfaceName)
        Else
            Return New String() {}
        End If
    End Function

    ''' <summary>Returns a string array of all local GPIB interfaces.</summary>
    ''' <param name="timeout">specifies the time out for looking for the first GPIB board.</param>
    Public Shared Function LocalInterfaceNames(ByVal firstBoardNumber As Integer, ByVal lastBoardNumber As Integer, 
                                               ByVal timeout As Integer) As String()
        If GpibInterface.HasBoards(firstBoardNumber, lastBoardNumber, timeout) Then
            Return My.MyLibrary.LocalInterfaceNames(GpibInterface.InterfaceName)
        Else
            Return New String() {}
        End If
    End Function

    ''' <summary>Returns a string array of all local GPIB interfaces.</summary>
    ''' <param name="timeout">specifies the time out for looking for the first GPIB board.</param>
    Public Shared Function LocalInterfaceNames(ByVal timeout As Integer) As String()
        If GpibInterface.HasBoards(timeout) Then
            Return My.MyLibrary.LocalInterfaceNames(GpibInterface.InterfaceName)
        Else
            Return New String() {}
        End If
    End Function

    ''' <summary>Returns an open message based session.</summary>
    ''' <returns>An open GPIB VISA Interface session.</returns>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function OpenGpibInterface(ByVal resourceName As String, ByVal connectTimeout As Integer) As GpibInterface
        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Try
            Return New GpibInterface(resourceName, connectTimeout)
        Catch ex As InvalidCastException
            Throw New InvalidCastException("Resource selected must be a GPIB Interface resource", ex)
        End Try
    End Function

    ''' <summary>
    ''' Returns an open GPIB interface.
    ''' </summary>
    ''' <param name="resourceName">Name of the resource.</param>
    ''' <returns>An open GPIB interface.</returns>
    ''' <exception cref="System.ArgumentNullException">resourceName</exception>
    ''' <exception cref="System.InvalidCastException">Resource selected must be a GPIB Interface</exception>
    Public Shared Function OpenInterface(ByVal resourceName As String) As VisaNS.GpibInterface
        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Try
            Return CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), VisaNS.GpibInterface)
        Catch ex As InvalidCastException
            Throw New InvalidCastException("Resource selected must be a GPIB Interface", ex)
        End Try
    End Function

    ''' <summary>
    ''' Returns the resource type.
    ''' </summary>
     Public Shared Function ParseResourceInterfaceType(ByVal resourceName As String) As NationalInstruments.VisaNS.HardwareInterfaceType
        Dim interfaceType As NationalInstruments.VisaNS.HardwareInterfaceType
        Dim interfaceNumber As Short
        Dim resourceClass As String = String.Empty
        NationalInstruments.VisaNS.ResourceManager.GetLocalManager.ParseResource(resourceName, interfaceType, interfaceNumber, resourceClass)
        Return interfaceType
    End Function

    ''' <summary>
    ''' Returns the <see cref="VisaResourceClass">resource class</see>
    ''' </summary>
    Public Shared Function ParseResourceClass(ByVal value As String) As VisaResourceClass
        Select Case value
            Case "INSTR"
                Return VisaResourceClass.Instrument
            Case "INTFC"
                Return VisaResourceClass.Interface
            Case Else
                Return VisaResourceClass.None
        End Select
    End Function

    ''' <summary>
    ''' Returns the <see cref="VisaResourceClass">resource class</see> from the full resource name.
    ''' </summary>
     Public Shared Function ParseResourceResourceClass(ByVal resourceName As String) As VisaResourceClass
        Dim interfaceType As NationalInstruments.VisaNS.HardwareInterfaceType
        Dim interfaceNumber As Short
        Dim resourceClass As String = String.Empty
        NationalInstruments.VisaNS.ResourceManager.GetLocalManager.ParseResource(resourceName, interfaceType, interfaceNumber, resourceClass)
        Return ParseResourceClass(resourceClass)
    End Function

    ''' <summary>
    ''' Try to connect to the specified GPIB board.
    ''' </summary>
     ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
    Public Shared Function TryConnect(ByVal boardNumber As Integer, ByVal timeout As Integer) As Boolean

        Return GpibInterface.TryConnect(isr.Visa.GpibInterface.BuildInterfaceResourceName(boardNumber), timeout)

    End Function

    ''' <summary>
    ''' Try to connect to the specified VISA interface resource.
    ''' </summary>
    ''' <param name="interfaceResourceName">A VISA interface resource Name.</param>
    ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
    ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
    ''' <exception cref="System.ArgumentNullException">interfaceResourceName</exception>
    ''' <history date="10/10/2011" by="David" revision="3.0.4300.x">
    ''' Uses <see cref="GpibInterface.IsSystemController">system controller sentinel</see>
    ''' instead of SRQ state to determine existence of the interface.
    '''   </history>
    Public Shared Function TryConnect(ByVal interfaceResourceName As String, ByVal timeout As Integer) As Boolean

        If interfaceResourceName Is Nothing Then
            Throw New ArgumentNullException("interfaceResourceName")
        End If
        Dim session As NationalInstruments.VisaNS.Session = Nothing
        Try

            ' check if we have interfaces 
            session = NationalInstruments.VisaNS.ResourceManager.GetLocalManager.Open( 
                interfaceResourceName, VisaNS.AccessModes.NoLock, timeout)

            If session Is Nothing OrElse session.LastStatus <> VisaStatusCode.Success Then
                Return False
            Else
                ' verify that we have a valid controller.
                Return CType(session, NationalInstruments.VisaNS.GpibInterface).IsSystemController
            End If

        Catch ex As NationalInstruments.VisaNS.VisaException

            Return False

        Catch

            Throw

        Finally

            If session IsNot Nothing Then
                session.Dispose()
                session = Nothing
            End If

        End Try

    End Function

#End Region

#Region " REGISTERS "

    ''' <summary>
    ''' Gets the status register of the specified device.
    ''' </summary>
    ''' <param name="address">Specifies the primary address of the device</param>
    Public Overloads ReadOnly Property DeviceStatusByte(ByVal address As Integer) As Byte
        Get
            MyBase.PrimaryAddress = CShort(address)
            Return MyBase.DeviceStatusByte
        End Get
    End Property

#End Region

#Region " CLEAR "

    ''' <summary>Returns all instruments to some default state.</summary>
    Public Overridable Sub DevicesClear()

        ' Transmit the DCL command to the interface.
        Dim commands(4) As Byte
        Dim gpibCommand As GpibCommandCode
        gpibCommand = GpibCommandCode.Untalk
        commands(0) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(1) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.DeviceClear
        commands(2) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Untalk
        commands(3) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(4) = Convert.ToByte(gpibCommand)
        MyBase.SendCommand(commands)

    End Sub

    ''' <summary>Issues an interface clear.</summary>
    ''' <returns>Returns <see cref="VisaStatusCode">Visa status code</see>.
    ''' </returns>
    ''' <remarks>Works around the following NI VISA Interface Clear issues:
    ''' <para>
    ''' NI VISA (4.1) times out issuing interface clear if no resources exist on the bus.  
    ''' This method traps the timeout and returns a 
    ''' <see cref="VisaStatusCode.ErrorTimeout">timeout status</see>. 
    ''' </para>
    ''' <para>
    ''' Also, NI VISA incorrectly returns a status of success.  This cannot be fixed
    ''' because the <see cref="LastStatus">status</see> property is read only.
    ''' Thus returning the relevant status helps.
    ''' </para>
    ''' </remarks>
    Public Shadows Function SendInterfaceClear() As NationalInstruments.VisaNS.VisaStatusCode
        Try
            MyBase.SendInterfaceClear()
            Return MyBase.LastStatus
        Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaStatusCode.ErrorTimeout
            ' trap timeout error.  Apparently, VISA is trying to send commands to the devices
            ' even if none are present.
            '  un-time exception thrown : NationalInstruments.VisaNS.VisaException - Timeout expired before operation completed.  VISA error code -1073807339 (0xBFFF0015), ErrorTimeout
            Return VisaStatusCode.ErrorTimeout
        Catch
            Throw
        End Try
    End Function

    ''' <summary>Issues an interface clear.
    ''' </summary>
    Public Overridable Function InterfaceClear() As NationalInstruments.VisaNS.VisaStatusCode
        Return Me.SendInterfaceClear()
    End Function

    ''' <summary>Clear the specified device.</summary>
     Public Overridable Sub SelectiveDeviceClear(ByVal primaryAddress As Int32)

        ' Transmit the SDC command to the interface.
        '    Visa.GpibInterface.Write(GPIB, String.Format(Globalization.CultureInfo.CurrentCulture, "UNT UNL LISTEN {0} SDC UNT UNL", primaryAddress))
        Dim commands(5) As Byte
        Dim gpibCommand As GpibCommandCode
        gpibCommand = GpibCommandCode.Untalk
        commands(0) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(1) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.ListenAddressGroup
        commands(2) = Convert.ToByte(gpibCommand) Or Convert.ToByte(primaryAddress)
        gpibCommand = GpibCommandCode.SelectiveDeviceClear
        commands(3) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Untalk
        commands(4) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(5) = Convert.ToByte(gpibCommand)
        MyBase.SendCommand(commands)
        '    gpib.Write(String.Format(Globalization.CultureInfo.CurrentCulture, "UNT UNL LISTEN {0} SDC UNT UNL", primaryAddress))

    End Sub

    ''' <summary>Clear the specified device.</summary>
     Public Overridable Sub SelectiveDeviceClear(ByVal resourceName As String)

        Dim primaryAddress As Int32 = GpibSession.ParsePrimaryAddress(resourceName)
        Me.SelectiveDeviceClear(primaryAddress)

    End Sub

    ''' <summary>Clear the device specified in the interface 
    ''' <see cref="GpibInterface.PrimaryAddress">primary address</see>.</summary>
    Public Overridable Sub SelectiveDeviceClear()

        Me.SelectiveDeviceClear(MyBase.PrimaryAddress)

    End Sub

#End Region

End Class

