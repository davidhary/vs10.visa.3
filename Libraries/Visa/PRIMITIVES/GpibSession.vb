Imports NationalInstruments

''' <summary>Extends the VISA GPIB message based session.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class GpibSession

    Inherits NationalInstruments.VisaNS.GpibSession

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        ' Rev 4.1 and 5.0 of VISA did not support this call and could not verify the 
        ' resource.  
        MyBase.New(resourceName, VisaNS.AccessModes.NoLock, 2000, False)
        Me._reader = New isr.Visa.MessageBasedReader(Me, MyBase.TerminationCharacter)
        Me._writer = New isr.Visa.MessageBasedWriter(Me)
        Me._timeouts = New Collections.Generic.Stack(Of Integer)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
    
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Not Me._timeouts Is Nothing Then
                        Me._timeouts = Nothing
                    End If

                    If Me._reader IsNot Nothing Then
                        Me._reader = Nothing
                    End If

                    If Me._writer IsNot Nothing Then
                        Me._writer = Nothing
                    End If

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " RESOURCE NAME MANAGEMENT "

    ''' <summary>
    ''' Specifies the format of a GPIB resource instrument.
    ''' </summary>
    Public Const ResourceFormat As String = "GPIB{0}::{1}::INSTR"

    ''' <summary>Returns a GPIB resource name string.</summary>
    ''' <param name="address">The device primary address</param>
    ''' <returns>A VISA resource GPIB instrument name in the form "GPIB0::PrimaryAddress::INSTR".</returns>
    Public Shared Function BuildResourceName(ByVal address As Int32) As String
        Return BuildResourceName(0, address)
    End Function

    ''' <summary>Returns a GPIB resource name string.</summary>
    ''' <param name="boardNumber">GPIB board number.</param>
    ''' <param name="address">The device primary address</param>
    ''' <returns>A VISA resource GPIB instrument name in the form 
    '''   "[GPIB][BoardNumber]::PrimaryAddress::INSTR".</returns>
    Public Shared Function BuildResourceName(ByVal boardNumber As Int32, ByVal address As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, GpibSession.ResourceFormat, boardNumber, address)
    End Function

    ''' <summary>Returns a GPIB resource name string.</summary>
    ''' <param name="boardNumber">GPIB board number.</param>
    ''' <param name="address">The device primary address</param>
    ''' <returns>A VISA resource GPIB instrument name in the form 
    '''   "[GPIB][BoardNumber]::PrimaryAddress::INSTR".</returns>
    Public Shared Function BuildGpibResourceName(ByVal boardNumber As String, ByVal address As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, GpibSession.ResourceFormat, boardNumber, address)
    End Function

    ''' <summary>
    ''' Parses the instrument address from the resource name.
    ''' </summary>
    ''' <param name="resourceName">Name of the resource.</param>
    ''' <returns>Int32.</returns>
    ''' <exception cref="System.ArgumentNullException">resourceName</exception>
    Public Shared Function ParsePrimaryAddress(ByVal resourceName As String) As Int32

        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If

        Dim value As String = My.MyLibrary.SubstringBetween(resourceName, "::", "::")
        If String.IsNullOrWhiteSpace(value) Then
            Dim numericValue As Integer
            If Int32.TryParse(value, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return 0
            End If
        Else
            Return 0
        End If

    End Function

    ''' <summary>
    ''' Parses the GPIB instrument address from the GPIB resource name.
    ''' </summary>
    ''' <param name="resourceName">Name of the resource.</param>
    ''' <returns>Int32.</returns>
    ''' <exception cref="System.ArgumentNullException">resourceName</exception>
    Public Shared Function ParseBoardNumber(ByVal resourceName As String) As Int32

        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If

        Dim value As String = My.MyLibrary.SubstringBetween(resourceName, "(", ")")
        If String.IsNullOrWhiteSpace(value) Then
            Dim numericValue As Integer
            If Int32.TryParse(value, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return 0
            End If
        Else
            Return 0
        End If

    End Function

    ''' <summary>
    ''' Gets the board number for the current resource name.
    ''' </summary>
    Public ReadOnly Property BoardNumber() As Integer
        Get
            Return GpibSession.ParseBoardNumber(MyBase.ResourceName)
        End Get
    End Property
#End Region

#Region " TRY CONNECT "

    ''' <summary>
    ''' Try to connect to the specified VISA resource.
    ''' </summary>
    ''' <param name="resourceName">A VISA resource Name.</param>
    ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
    ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
    ''' <exception cref="System.ArgumentNullException">resourceName</exception>
    Public Shared Function TryConnect(ByVal resourceName As String, ByVal timeout As Integer) As Boolean

        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Dim session As isr.Visa.GpibSession = Nothing
        Try

            ' check if we have the specified GPIB resource
            session = New isr.Visa.GpibSession(resourceName)

            If session Is Nothing OrElse session.LastStatus <> VisaStatusCode.Success Then
                Return False
            Else
                ' set timeout.
                session.Timeout = timeout
                ' verify that we can clear and query the session.
                session.Clear()
                session.Query("*OPC?")
                Return True
            End If

        Catch ex As NationalInstruments.VisaNS.VisaException

            Return False

        Catch

            Throw

        Finally

            If session IsNot Nothing Then
                session.Dispose()
                session = Nothing
            End If

        End Try

    End Function

    ''' <summary>
    ''' Try to connect to the specified GPIB instrument.
    ''' </summary>
    ''' <param name="boardNumber">GPIB board number.</param>
    ''' <param name="address">The device primary address</param>
    ''' <param name="timeout">The maximum time in millisecond to wait for opening the VISA session.</param>
    Public Shared Function TryConnect(ByVal boardNumber As Integer, ByVal address As Int32, ByVal timeout As Integer) As Boolean
        Return GpibSession.TryConnect(isr.Visa.GpibSession.BuildResourceName(boardNumber, address), timeout)
    End Function


#End Region

#Region " I Resettable "

    ''' <summary>Selectively clears the instrument.</summary>
    Public Function ClearActiveState() As Boolean

        ' clear the device
        MyBase.Clear()
        Return Not Me.HasError(isr.Visa.Ieee4882.ServiceRequests.ErrorAvailable)

    End Function

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    Public Function ClearExecutionState() As Boolean

        Return Me._writer.ClearExecutionState()

    End Function

    ''' <summary>Returns the device to its default known state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    Public Function ResetKnownState() As Boolean

        Return Me._writer.ResetKnownState()

        ' Set default values if any

    End Function

#End Region

#Region " IEEE488.2 COMMANDS "

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    Public Function OperationCompleted() As Boolean
        Return Me.QueryInt32(isr.Visa.Ieee4882.Syntax.OperationCompletedQueryCommand).GetValueOrDefault(0) = 1
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    Public Function ReadIdentity() As String
        Return Me.QueryTrimEnd(isr.Visa.Ieee4882.Syntax.IdentifyQueryCommand)
    End Function

#End Region

#Region " IEEE488.2 REGISTER COMMANDS "

    ''' <summary>Returns a True is the service request register 
    '''   status byte reports error available.</summary>
    Public Function HasError(ByVal errorBits As Integer) As Boolean
        Return (Me.ReadStatusByte() And errorBits) <> 0
    End Function

    ''' <summary>Returns the service request register status byte.</summary>
    Public Overloads Function ReadStatusByte() As Integer
        Dim status As Int32 = CType(MyBase.ReadStatusByte(), Integer)
        If status >= 0 Then
            Return status
        Else
            Return 256 + status
        End If
    End Function

    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <returns>The <see cref="Ieee4882.ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <remarks>When set clears all service registers.</remarks>
    Public Property ServiceRequestEventEnable() As Ieee4882.ServiceRequests
        Get
            Return CType(Me.QueryInt32(isr.Visa.Ieee4882.Syntax.ServiceRequestEnableQueryCommand), Ieee4882.ServiceRequests)
        End Get
        Set(ByVal value As Ieee4882.ServiceRequests)
            Me.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, 
                                            isr.Visa.Ieee4882.Syntax.ServiceRequestEnableCommand, value))
        End Set
    End Property

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public ReadOnly Property ServiceRequestEventStatus() As Ieee4882.ServiceRequests
        Get
            Return CType(Me.QueryInt32(isr.Visa.Ieee4882.Syntax.ServiceRequestStatusQueryCommand), Ieee4882.ServiceRequests)
        End Get
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    Public ReadOnly Property StandardEventStatus() As Ieee4882.StandardEvents
        Get
            Return CType(Me.QueryInt32(isr.Visa.Ieee4882.Syntax.StandardEventStatusQueryCommand), Ieee4882.StandardEvents)
        End Get
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Ieee4882.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Property StandardEventEnable() As Ieee4882.StandardEvents
        Get
            Return CType(Me.QueryInt32(isr.Visa.Ieee4882.Syntax.StandardEventEnableQueryCommand), Ieee4882.StandardEvents)
        End Get
        Set(ByVal value As Ieee4882.StandardEvents)
            Me.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, isr.Visa.Ieee4882.Syntax.StandardEventEnableCommand, value))
        End Set
    End Property

#End Region

#Region " QUERY "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
        Return MessageBasedReader.ParseBoolean(Me.QueryTrimEnd(question))
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
        Return MessageBasedReader.ParseDouble(Me.QueryTrimEnd(question))
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
        Return MessageBasedReader.ParseInt32(Me.QueryTrimEnd(question))
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
        Dim queryResult As String = Me.QueryTrimEnd(question)
        Return MessageBasedReader.ParseInfInt32(queryResult)
    End Function

    ''' <summary>Queries the instrument and returns an Integer value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInteger(ByVal question As String) As Nullable(Of Integer)
        Return MessageBasedReader.ParseInteger(Me.QueryTrimEnd(question))
    End Function

    ''' <summary>Queries the instrument and returns an Integer value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInfInteger(ByVal question As String) As Nullable(Of Integer)
        Return MessageBasedReader.ParseInfInteger(Me.QueryTrimEnd(question))
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Shadows Function Query(ByVal question As String) As String
#If DIAGNOSE Then
    Trace.CorrelationManager.StartLogicalOperation("QUERY")
    My.Application.Log.WriteEntry(question, TraceEventType.Verbose)
#End If
        Me._writer.TransmitBuffer = question
        Me._reader.ReceiveBuffer = MyBase.Query(question)
#If DIAGNOSE Then
    My.Application.Log.WriteEntry(Me._reader.ReceiveBuffer, TraceEventType.Verbose)
    Trace.CorrelationManager.StopLogicalOperation()
#End If
        Return Me._reader.ReceiveBuffer
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimEnd(ByVal question As String) As String
        Return Me.Query(question).TrimEnd(Convert.ToChar(MyBase.TerminationCharacter))
    End Function

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimNewLine(ByVal question As String) As String
        Return Me.Query(question).TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function

#End Region

#Region " READ "

    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read.
    ''' </summary>
    ''' <remarks>
    ''' This does not work on the 6221 if the only data is an End Line as the instrument does not have the message available flag set.
    ''' TO_DO: Check  Me._reader.DiscardUnreadData() in visa version 5. It did not work in 3.
    ''' </remarks>
    Public Sub DiscardUnreadData(ByVal messageAvailableBits As Integer)
        Do While (Me.ReadStatusByte() And messageAvailableBits) <> 0
            Me._reader.ReadLine()
        Loop
        ' Me._reader.ReadLine()
        ' this does not work! Me._reader.DiscardUnreadData()
    End Sub

    Private _reader As isr.Visa.MessageBasedReader
    ''' <summary>
    ''' Gets reference to the message based reader.
    ''' </summary>
    Public ReadOnly Property Reader() As isr.Visa.MessageBasedReader
        Get
            Return Me._reader
        End Get
    End Property

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property ReceiveBuffer() As String
        Get
            Return Me._reader.ReceiveBuffer
        End Get
    End Property

    ''' <summary>Reads the instrument and returns a Boolean value.</summary>
    Public Function ReadBoolean() As Nullable(Of Boolean)
        Return MessageBasedReader.ParseBoolean(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns a double value.</summary>
    Public Function ReadDouble() As Nullable(Of Double)
        Return MessageBasedReader.ParseDouble(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Int32 value.</summary>
    Public Function ReadInt32() As Nullable(Of Int32)
        Return MessageBasedReader.ParseInt32(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Int32 value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    Public Function ReadInfInt32() As Nullable(Of Int32)
        Return MessageBasedReader.ParseInfInt32(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Integer value.</summary>
    Public Function ReadInteger() As Nullable(Of Integer)
        Return MessageBasedReader.ParseInteger(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Integer value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    Public Function ReadInfInteger() As Nullable(Of Integer)
        Return MessageBasedReader.ParseInfInteger(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Shadows Function ReadString() As String
        Return Me._reader.ReadLine()
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadLine() As String
        Return Me._reader.ReadLine()
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadLineTrimEnd() As String
        Return Me._reader.ReadLineTrimEnd()
    End Function

    ''' <summary>
    ''' Gets or sets the termination character.
    ''' </summary>
    Public Shadows Property TerminationCharacter() As Byte
        Get
            Return MyBase.TerminationCharacter
        End Get
        Set(ByVal value As Byte)
            MyBase.TerminationCharacter = value
            Me._reader.TerminationCharacter = value
        End Set
    End Property

#End Region

#Region " WRITE "

    ''' <summary>
    ''' Sends all unsent data from the write buffer to the device.
    ''' </summary>
    Public Sub FlushWrite()
        Me._writer.Flush()
    End Sub

    Private _writer As isr.Visa.MessageBasedWriter
    ''' <summary>
    ''' Gets reference to the message based writer.
    ''' </summary>
    Public ReadOnly Property Writer() As isr.Visa.MessageBasedWriter
        Get
            Return Me._writer
        End Get
    End Property

    ''' <summary>Writes command to the instrument.</summary>
    ''' <param name="value">The string to write.</param>
    Public Shadows Sub Write(ByVal value As String)
        Me.WriteLine(value)
    End Sub

    ''' <summary>Writes command to the instrument.</summary>
    ''' <param name="value">The string to write.</param>
    Public Sub WriteLine(ByVal value As String)
        Me._writer.WriteLine(value)
    End Sub

    ''' <summary>Writes Boolean command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="value">The Boolean value to write.</param>
    ''' <param name="boolFormat">The Boolean format.</param>
    Public Sub WriteLine(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, value, boolFormat))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    Public Sub WriteLineOnOff(ByVal queryCommand As String, ByVal isOn As Boolean)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, isOn, BooleanDataFormat.OnOff))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    Public Sub WriteLineOneZero(ByVal queryCommand As String, ByVal one As Boolean)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, one, BooleanDataFormat.OneZero))
    End Sub

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property TransmitBuffer() As String
        Get
            Return Me._writer.TransmitBuffer
        End Get
    End Property

#End Region

#Region " TIMEOUT MANAGEMENT "

    ''' <summary>
    ''' Gets or sets the minimum instrument timeout value in milliseconds
    ''' when accessing the device.
    ''' </summary>
    Public Shadows Property Timeout() As Integer
        Get
            Return MyBase.Timeout
        End Get
        Set(ByVal value As Integer)
            MyBase.Timeout = value
        End Set
    End Property

    ''' <summary>Restores the last timeout from the stack.
    ''' </summary>
    Public Sub RestoreTimeout()
        MyBase.Timeout = Me._timeouts.Pop
    End Sub

    ''' <summary>Saves the current timeout and sets a new setting timeout.
    ''' </summary>
    ''' <param name="timeout">Specifies the new timeout</param>
    Public Sub StoreTimeout(ByVal timeout As Integer)
        Me._timeouts.Push(MyBase.Timeout)
        MyBase.Timeout = timeout
    End Sub

    ''' <summary>Gets or sets the reference to the stack of timeouts.
    ''' </summary>
    Private _timeouts As System.Collections.Generic.Stack(Of Integer)

#End Region

#Region " VISA STATUS "

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
        Get
            Return MyBase.LastStatus < 0
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
        Get
            Return MyBase.LastStatus > 0
        End Get
    End Property

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    Public Function BuildLastVisaStatusDetails() As String
        Return My.MyLibrary.BuildVisaStatusDetails(CType(MyBase.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
    End Function

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    Public Function BuildLastVisaStatusDetails(ByVal lastVisaAction As String) As String
        If String.IsNullOrWhiteSpace(lastVisaAction) Then
            Return Me.BuildLastVisaStatusDetails()
        Else
            Return My.MyLibrary.BuildVisaStatusDetails(lastVisaAction, CType(MyBase.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
        End If
    End Function

#End Region

End Class

