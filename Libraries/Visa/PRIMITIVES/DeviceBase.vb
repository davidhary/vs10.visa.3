﻿Imports System.ComponentModel
Imports isr.Core.EventHandlerExtensions
''' <summary>
''' A base device implementing the fundamental interfaces of a device
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/2011" by="David" revision="1.2.4038.x">
''' Created
''' </history>
Public MustInherit Class DeviceBase

    Implements IDisposable, isr.Core.IConnectResource, isr.Core.IResettableDevice, isr.Core.IPresettable, isr.Core.IMessagePublisher, isr.Core.IAnonPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="ResourceTitle">Specifies the instrument name.</param>
    Protected Sub New(ByVal resourceTitle As String, ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType)

        ' instantiate the base class
        MyBase.New()
        Me._resourceTitle = resourceTitle
        Me._connectTimeout = 30000
        Me._resourceType = resourceType
        Me._resourceCaption = resourceTitle

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
    
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._identity = ""

                    If Me.ConnectionChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ConnectionChangedEvent.GetInvocationList
                            RemoveHandler Me.ConnectionChanged, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.ConnectingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ConnectingEvent.GetInvocationList
                            RemoveHandler Me.Connecting, CType(d, Global.System.EventHandler(Of System.ComponentModel.CancelEventArgs))
                        Next
                    End If

                    If Me.DisconnectingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DisconnectingEvent.GetInvocationList
                            RemoveHandler Me.Disconnecting, CType(d, Global.System.EventHandler(Of System.ComponentModel.CancelEventArgs))
                        Next
                    End If

                    If Me.PropertyChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.PropertyChangedEvent.GetInvocationList
                            RemoveHandler Me.PropertyChanged, CType(d, Global.System.ComponentModel.PropertyChangedEventHandler)
                        Next
                    End If

                    If Me.MessageAvailableEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                            RemoveHandler Me.MessageAvailable, CType(d, Global.System.EventHandler(Of isr.Core.MessageEventArgs))
                        Next
                    End If

                End If
            End If

            ' Free shared unmanaged resources

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " I ANON PUBLISHER "

    ''' <summary>
    ''' Occurs when a property value changes.
    ''' </summary>
    Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Raised to notify the bindable object of a change.
    ''' </summary>
     Public Sub OnPropertyChanged(ByVal name As String) Implements Core.IAnonPublisher.OnPropertyChanged
        If Me.Publishable Then
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Publishes all values by raising the changed events.
    ''' </summary>
    Public MustOverride Sub Publish() Implements Core.IAnonPublisher.Publish

    ''' <summary>
    ''' Gets or sets the publishable status of the publisher.
    ''' When created, the publisher is not publishable.
    ''' This allows changing properties without affecting the observers.
    ''' </summary>
    Public Property Publishable() As Boolean Implements Core.IAnonPublisher.Publishable

#End Region

#Region " I CONNECT "


    ''' <summary> Connects this instance. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Private Function _connect() As Boolean
        Me._isConnected = True
        Me._identity = ""
        Me.OnMessageAvailable(TraceEventType.Verbose, "CONNECTION ESTABLISHED", "Connection established.")
        Me.OnConnectionChanged(System.EventArgs.Empty)
        Return Me.IsConnected
    End Function

    ''' <summary>
    ''' Connects this instance using an existing resource name.
    ''' </summary>
    ''' <returns><c>True</c> if connected, <c>False</c> otherwise</returns>
    Public Overridable Overloads Function Connect() As Boolean Implements Core.IConnect.Connect
        Return Me._connect()
    End Function

    ''' <summary>
    ''' Connects this instance.
    ''' </summary>
    ''' <param name="resourceName">Specifies the name of the resource to which to connect.</param>
    ''' <param name="resourceTitle">Specifies the human readable name of the resource.</param>
    ''' <returns><c>True</c> if the instance connected.</returns>
    Public Overridable Overloads Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean Implements Core.IConnectResource.Connect
        Return Me._connect()
    End Function

    ''' <summary>
    ''' Connects and returns true if connected.  Use for connecting to interfaces
    ''' requiring an address
    ''' </summary>
    ''' <param name="address">Specifies the instrument address</param>
    ''' <returns><c>True</c> if connected, <c>False</c> otherwise</returns>
    Public Overridable Overloads Function Connect(ByVal address As Integer) As Boolean Implements Core.IConnect.Connect
        Return Me._connect()
    End Function

    ''' <summary>
    ''' Connects and returns true if connected.  Use for connecting to interfaces
    ''' requiring a resource name.
    ''' </summary>
    ''' <param name="resourceName">Specifies the instrument resource name</param>
    ''' <returns><c>True</c> if connected, <c>False</c> otherwise</returns>
    ''' <history date="09/02/2009" by="David" revision="3.0.3532.x">
    ''' Allows connecting using a string-based address.
    '''   </history>
    Public Overridable Overloads Function Connect(ByVal resourceName As String) As Boolean Implements Core.IConnect.Connect
        Return Me._connect()
    End Function

    ''' <summary>
    ''' Gets or sets the time out for doing a reset and clear on the instrument.
    ''' </summary>
    Public Property ConnectTimeout() As Integer Implements isr.Core.IConnect.Timeout

    ''' <summary>
    ''' Disconnects and returns true if disconnected.
    ''' </summary>
    ''' <returns><c>True</c> if the instance disconnected.</returns>
    Public Overridable Function Disconnect() As Boolean Implements Core.IConnect.Disconnect
        Me._isConnected = False
        Me.OnMessageAvailable(TraceEventType.Verbose, "DISCONNECTED", "Disconnected.")
        Me.OnConnectionChanged(System.EventArgs.Empty)
        Return Not Me.IsConnected
    End Function

    Private _isConnected As Boolean
    ''' <summary>
    ''' Gets or sets the connection status.
    ''' </summary>
    ''' <value><c>Connected</c> is a <see cref="Boolean" /> property that is True if
    ''' connected (open).</value>
    ''' <remakrs>
    ''' Changed 6/14/13 to allow the private connect and disconnect to set this value.
    ''' </remakrs>
    ''' <history date="02/07/06" by="David" revision="1.00.2228.x">
    ''' Rename
    ''' </history>
    Public Overridable ReadOnly Property IsConnected() As Boolean Implements Core.IConnect.IsConnected
        Get
            Return Me._isConnected
        End Get
    End Property

#End Region

#Region " I CONNECT EVENTS "

    Public Event ConnectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Implements Core.IConnect.ConnectionChanged

    ''' <summary>Raises an event to alert on change of connection.
    ''' </summary>
    Protected Overridable Sub OnConnectionChanged(ByVal e As System.EventArgs) Implements isr.Core.IConnect.OnConnectionChanged
        If Me.IsConnected Then
            Me._resourceCaption = Me.ResourceTitle & "." & ResourceName
        Else
            Me._resourceCaption = Me.ResourceTitle
        End If
        Dim evt As EventHandler(Of EventArgs) = Me.ConnectionChangedEvent
        If evt IsNot Nothing Then evt.SafeInvoke(Me, e)

    End Sub

    ''' <summary>
    ''' Occurs before the instrument is connected but after obtaining a valid session.
    ''' </summary>
    Public Event Connecting As EventHandler(Of System.ComponentModel.CancelEventArgs) Implements isr.Core.IConnectResource.Connecting

    ''' <summary>Raises the connecting event.</summary>
    ''' <param name="e">Passes reference to the 
    ''' <see cref="System.ComponentModel.CancelEventArgs">cancel event arguments</see>.</param>
    Protected Overridable Sub OnConnecting(ByVal e As System.ComponentModel.CancelEventArgs) Implements isr.Core.IConnectResource.OnConnecting
        Dim evt As EventHandler(Of System.ComponentModel.CancelEventArgs) = Me.ConnectingEvent
        If evt IsNot Nothing Then evt.SafeInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when the instrument is disconnected.
    ''' </summary>
    Public Event Disconnecting(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Implements isr.Core.IConnectResource.Disconnecting

    ''' <summary>Raises the disconnected event.</summary>
    ''' <param name="e">Passes reference to the <see cref="System.ComponentModel.CancelEventArgs">event arguments</see>.</param>
    Protected Overridable Sub OnDisconnecting(ByVal e As System.ComponentModel.CancelEventArgs) Implements isr.Core.IConnectResource.OnDisconnecting
        Dim evt As EventHandler(Of System.ComponentModel.CancelEventArgs) = Me.DisconnectingEvent
        If evt IsNot Nothing Then evt.SafeInvoke(Me, e)
    End Sub

#End Region

#Region " I CONNECTABLE RESOURCE "

    Private _resourceType As NationalInstruments.VisaNS.HardwareInterfaceType
    ''' <summary>
    ''' Returns true if an general visa exception occurred finding resources.
    ''' The exception points to failure not due to parsing of the resource name
    ''' as those are trapped and addressed.
    ''' </summary>
    ''' 
    Public ReadOnly Property FailedFindingResources() As Boolean? Implements Core.IConnectableResource.FailedFindingResources
        Get
            Return My.MyLibrary.FailedFindLocalResources
        End Get
    End Property

    ''' <summary>
    ''' Returns a string array of the resources for this connectible resource.
    ''' </summary><returns></returns>
    Public Function FindResources() As String() Implements Core.IConnectableResource.FindResources
        Return My.MyLibrary.LocalResourceNames(isr.Visa.My.MyLibrary.ResourceBaseName(Me._resourceType))
    End Function

    Private _identity As String
    ''' <summary>
    ''' Gets or sets the instrument identity. Reads the instrument identity the first time it is 
    ''' called. Setting the identity allows emulation.
    ''' Cleared when connected.
    ''' </summary>
    Public Overridable Property Identity() As String Implements Core.IConnectableResource.Identity
        Get
            Return Me._identity
        End Get
        Set(ByVal value As String)
            Me._identity = value
        End Set
    End Property

    ''' <summary>
    ''' Explains failure finding resources.
    ''' </summary>
    ''' 
    Public ReadOnly Property ResourceFindingFailureSymptom() As String Implements Core.IConnectableResource.ResourceFindingFailureSymptom
        Get
            Return My.MyLibrary.FindLocalResourcesSymptom
        End Get
    End Property

    ''' <summary>
    ''' Returns true if we have resources.
    ''' </summary>
    Public Function HasResources() As Boolean
        Return My.MyLibrary.HasResources(My.MyLibrary.ResourceBaseName(Me._resourceType))
    End Function

    Private _resourceName As String = String.Empty
    ''' <summary>Gets or sets the resource name.</summary>
    ''' <value><c>ResourceName</c> is a String property.</value>
    Public Overridable Property ResourceName() As String Implements Core.IConnectableResource.ResourceName
        Get
            Return Me._resourceName
        End Get
        Set(ByVal value As String)
            Me._resourceName = value
            If String.IsNullOrWhiteSpace(Me._resourceTitle) Then
                Me._resourceTitle = value
            End If
        End Set
    End Property

    Private _resourceTitle As String = String.Empty
    ''' <summary>Gets or sets the resource name.</summary>
    ''' <value><c>ResourceName</c> is a String property.</value>
    Public Overridable Property ResourceTitle() As String Implements Core.IConnectableResource.ResourceTitle
        Get
            Return Me._resourceTitle
        End Get
        Set(ByVal value As String)
            Me._resourceTitle = value
        End Set
    End Property

#End Region

#Region " I Presettable "

    Public Overridable Function Preset() As Boolean Implements Core.IPresettable.Preset
    End Function

#End Region

#Region " I Resettable "

    Public MustOverride Function ClearResource() As Boolean Implements Core.IConnectableResource.ClearResource

    Public MustOverride Function ClearActiveState() As Boolean Implements Core.IResettableDevice.ClearActiveState

    Public MustOverride Function ClearExecutionState() As Boolean Implements Core.IResettableDevice.ClearExecutionState

    Public MustOverride Function ResetAndClear() As Boolean Implements Core.IResettableDevice.ResetAndClear

    Public MustOverride Function ResetKnownState() As Boolean Implements Core.IResettableDevice.ResetKnownState

#End Region

#Region " I MESSAGE PUBLISHER "

    ''' <summary>
    ''' Gets or sets the caption for prefixing messages
    ''' </summary>
    Private _resourceCaption As String

    ''' <summary>Occurs when the element or instrument has a message
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs) Implements isr.Core.IMessagePublisher.MessageAvailable

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="e">Specifies the event arguments.</param>
    Public Overridable Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
        Dim evt As EventHandler(Of isr.Core.MessageEventArgs) = Me.MessageAvailableEvent
        If evt IsNot Nothing Then evt.SafeBeginInvoke(Me, e)
        If e IsNot Nothing Then
            Return e.Details
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Raises a message.
    ''' </summary>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message short synopsis.</param>
    ''' <param name="format">Specifies the format for building the message detains</param>
    ''' <param name="args">Specifies the format arguments.</param>
    Public Function OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType,
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
        Return Me.OnMessageAvailable(traceLevel, traceLevel, synopsis, format, args)
    End Function

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="broadcastLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message Synopsis.</param>
    ''' <param name="format">Specifies the message details.</param>
    ''' <param name="args">Arguments to use in the format statement.</param>
    Public Overridable Function OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType,
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String Implements isr.Core.IMessagePublisher.OnMessageAvailable
        Return OnMessageAvailable(New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, Me._resourceCaption & ":: " & format, args))
    End Function

#End Region

End Class
