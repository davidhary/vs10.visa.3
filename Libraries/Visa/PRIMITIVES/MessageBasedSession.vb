Imports NationalInstruments

''' <summary>Extends the VISA message based session.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class MessageBasedSession

    Inherits VisaNS.MessageBasedSession

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName ">Specifies the name of the VISA resource.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        MyBase.New(resourceName)
        Me._reader = New isr.Visa.MessageBasedReader(Me, MyBase.TerminationCharacter)
        Me._writer = New isr.Visa.MessageBasedWriter(Me)
        Me._timeouts = New Collections.Generic.Stack(Of Integer)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up managed and unmanaged resources.</summary>
    ''' <param name="disposing">If true, the method has been called directly or 
    '''   indirectly by a user's code. Managed and unmanaged resources can be disposed.
    '''   If disposing equals false, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects. Only unmanaged resources can be disposed.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Not Me._timeouts Is Nothing Then
                        Me._timeouts = Nothing
                    End If

                    If Me._reader IsNot Nothing Then
                        Me._reader = Nothing
                    End If

                    If Me._writer IsNot Nothing Then
                        Me._writer = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

            MyBase.Dispose(disposing)

        End Try
    End Sub

#End Region

#Region " I Resettable "

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ClearExecutionState(ByVal session As VisaNS.IMessageBasedSession) As Boolean

        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        ' Clear the device status.  This will set all the
        ' standard subsystem properties.
        MessageBasedSession.Write(session, isr.Visa.Ieee4882.Syntax.ClearExecutionStateCommand)

        Return Not MessageBasedSession.HasError(session)

    End Function

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    Public Function ClearExecutionState() As Boolean

        Return MessageBasedSession.ClearExecutionState(Me)

    End Function

    ''' <summary>Returns the device to its default known state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ResetKnownState(ByVal session As VisaNS.IMessageBasedSession) As Boolean

        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, isr.Visa.Ieee4882.Syntax.ResetKnownStateCommand)
        Return Not MessageBasedSession.HasError(session)

    End Function

    ''' <summary>Returns the device to its default known state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    Public Function ResetKnownState() As Boolean

        Return MessageBasedSession.ResetKnownState(Me)

    End Function

#End Region

#Region " IEEE488.2 COMMANDS "

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function OperationCompleted(ByVal session As VisaNS.IMessageBasedSession) As Boolean
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return MessageBasedSession.QueryInt32(session, isr.Visa.Ieee4882.Syntax.OperationCompletedQueryCommand).Value = 1
    End Function

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    Public Function OperationCompleted() As Boolean
        Return MessageBasedSession.OperationCompleted(Me)
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadIdentity(ByVal session As VisaNS.IMessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return MessageBasedSession.QueryTrimEnd(session, isr.Visa.Ieee4882.Syntax.IdentifyQueryCommand)
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    Public Function ReadIdentity() As String
        Return MessageBasedSession.ReadIdentity(Me)
    End Function

#End Region

#Region " READ REGISTERS "

    ''' <summary>Returns True if the service request register 
    '''   status byte reports error available.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function HasError(ByVal session As VisaNS.IMessageBasedSession) As Boolean
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return (MessageBasedSession.ReadStatusByte(session) And Ieee4882.ServiceRequests.ErrorAvailable) <> 0
    End Function

    ''' <summary>Returns True if the service request register 
    '''   status byte reports error available.</summary>
    Public Function HasError() As Boolean
        Return MessageBasedSession.HasError(Me)
    End Function

    ''' <summary>Returns the service request register status byte.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStatusByte(ByVal session As VisaNS.IMessageBasedSession) As isr.Visa.Ieee4882.ServiceRequests
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim status As Int32 = CType(session.ReadStatusByte(), Int32)
        If status >= 0 Then
            Return CType(status, isr.Visa.Ieee4882.ServiceRequests)
        Else
            Return CType(256 + status, isr.Visa.Ieee4882.ServiceRequests)
        End If
    End Function

    ''' <summary>Reads the service request event status from the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property ServiceRequestEventStatus(ByVal session As VisaNS.IMessageBasedSession) As Ieee4882.ServiceRequests
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, isr.Visa.Ieee4882.Syntax.ServiceRequestStatusQueryCommand), Ieee4882.ServiceRequests)
        End Get
    End Property

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public ReadOnly Property ServiceRequestEventStatus() As Ieee4882.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventStatus(Me)
        End Get
    End Property

    ''' <summary>
    ''' Programs or reads back the service request event request.
    ''' </summary>
    ''' <value>The service request event enable.</value>
    ''' <exception cref="System.ArgumentNullException">session</exception>
    ''' <exception cref="ArgumentNullException"></exception>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    '''   <returns>The <see cref="isr.Visa.Ieee4882.ServiceRequests">mask</see>
    ''' to use for enabling the events.</returns>
    ''' <remarks>When set clears all service registers .</remarks>
    Public Shared Property ServiceRequestEventEnable(ByVal session As VisaNS.IMessageBasedSession) As Ieee4882.ServiceRequests
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, isr.Visa.Ieee4882.Syntax.ServiceRequestEnableQueryCommand), Ieee4882.ServiceRequests)
        End Get
        Set(ByVal value As Ieee4882.ServiceRequests)
            MessageBasedSession.Write(session, String.Format(Globalization.CultureInfo.CurrentCulture, isr.Visa.Ieee4882.Syntax.ServiceRequestEnableCommand, value))
        End Set
    End Property

    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Ieee4882.ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <remarks>When set clears all service registers.</remarks>
    Public Property ServiceRequestEventEnable() As Ieee4882.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventEnable(Me)
        End Get
        Set(ByVal value As Ieee4882.ServiceRequests)
            MessageBasedSession.ServiceRequestEventEnable(Me) = value
        End Set
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property StandardEventStatus(ByVal session As VisaNS.IMessageBasedSession) As Ieee4882.StandardEvents
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, isr.Visa.Ieee4882.Syntax.StandardEventStatusQueryCommand), Ieee4882.StandardEvents)
        End Get
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    Public ReadOnly Property StandardEventStatus() As Ieee4882.StandardEvents
        Get
            Return MessageBasedSession.StandardEventStatus(Me)
        End Get
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    ''' <returns>The <see cref="isr.Visa.Ieee4882.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Shared Property StandardEventEnable(ByVal session As VisaNS.IMessageBasedSession) As Ieee4882.StandardEvents
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, isr.Visa.Ieee4882.Syntax.StandardEventEnableQueryCommand), Ieee4882.StandardEvents)
        End Get
        Set(ByVal value As Ieee4882.StandardEvents)
            MessageBasedSession.Write(session, String.Format(Globalization.CultureInfo.CurrentCulture, isr.Visa.Ieee4882.Syntax.StandardEventEnableCommand, value))
        End Set
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Ieee4882.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Property StandardEventEnable() As Ieee4882.StandardEvents
        Get
            Return MessageBasedSession.StandardEventEnable(Me)
        End Get
        Set(ByVal value As Ieee4882.StandardEvents)
            MessageBasedSession.StandardEventEnable(Me) = value
        End Set
    End Property

#End Region

#Region " QUERY "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryBoolean(ByVal session As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim queryResult As String = session.Query(question)
        Return MessageBasedReader.ParseBoolean(queryResult)
    End Function

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
        Return MessageBasedSession.QueryBoolean(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryDouble(ByVal session As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Double)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim queryResult As String = MessageBasedSession.QueryTrimEnd(session, question)
        Return MessageBasedReader.ParseDouble(queryResult)
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
        Return MessageBasedSession.QueryDouble(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryInt32(ByVal session As VisaNS.IMessageBasedSession, ByVal question As String) As Nullable(Of Int32)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim queryResult As String = MessageBasedSession.QueryTrimEnd(session, question)
        Return MessageBasedReader.ParseInt32(queryResult)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
        Return MessageBasedSession.QueryInt32(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function QueryTrimEnd(ByVal session As VisaNS.IMessageBasedSession, ByVal question As String) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.Query(question).TrimEnd(Convert.ToChar(session.TerminationCharacter))
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimEnd(ByVal question As String) As String
        Return MessageBasedSession.QueryTrimEnd(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function QueryTrimNewLine(ByVal session As VisaNS.IMessageBasedSession, ByVal question As String) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.Query(question).TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimNewline(ByVal question As String) As String
        Return MessageBasedSession.QueryTrimNewLine(Me, question)
    End Function

#End Region

#Region " READ "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadBoolean(ByVal session As VisaNS.IMessageBasedSession) As Nullable(Of Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim readResult As String = MessageBasedSession.ReadStringTrimEnd(session)
        Return MessageBasedReader.ParseBoolean(readResult)
    End Function

    ''' <summary>Reads a Boolean value.</summary>
    Public Function ReadBoolean1() As Nullable(Of Boolean)
        Return MessageBasedSession.ReadBoolean(Me)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadDouble(ByVal session As VisaNS.IMessageBasedSession) As Nullable(Of Double)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim readResult As String = MessageBasedSession.ReadStringTrimEnd(session)
        Return MessageBasedReader.ParseDouble(readResult)
    End Function

    ''' <summary>Reads a Double value.</summary>
    Public Function ReadDouble1() As Nullable(Of Double)
        Return MessageBasedSession.ReadDouble(Me)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadInt32(ByVal session As VisaNS.IMessageBasedSession) As Nullable(Of Int32)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim readResult As String = MessageBasedSession.ReadStringTrimEnd(session)
        Return MessageBasedReader.ParseInt32(readResult)
    End Function

    ''' <summary>Reads an integer value.</summary>
    Public Function ReadInt321() As Nullable(Of Int32)
        Return MessageBasedSession.ReadInt32(Me)
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStringTrimEnd(ByVal session As VisaNS.IMessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.ReadString().TrimEnd(Convert.ToChar(session.TerminationCharacter))
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination new line
    '''   characters.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStringNewLine(ByVal session As VisaNS.IMessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.ReadString().TrimEnd(Convert.ToChar(13)).TrimEnd(Convert.ToChar(10))
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadString1() As String
        Return MessageBasedSession.ReadStringTrimEnd(Me)
    End Function

#End Region

#Region " WRITE "

    ''' <summary>Writes to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Shadows Sub Write(ByVal session As VisaNS.IMessageBasedSession, ByVal output As String)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        session.Write(output)
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub WriteOnOff(ByVal session As VisaNS.IMessageBasedSession, ByVal output As String, ByVal isOn As Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, output & My.MyLibrary.ImmediateIf(isOn, " ON", " OFF"))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.IMessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub WriteOneZero(ByVal session As VisaNS.IMessageBasedSession, 
        ByVal output As String, ByVal one As Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, output & My.MyLibrary.ImmediateIf(one, " 1", " 0"))
    End Sub

#End Region

#Region " WRITE "

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
        MessageBasedSession.WriteOnOff(Me, output, isOn)
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
        MessageBasedSession.WriteOneZero(Me, output, one)
    End Sub

#End Region

#Region " READ "

    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read.
    ''' </summary>
    ''' <remarks>
    ''' This does not work on the 6221 if the only data is an End Line as the instrument does not have the message available flag set.
    ''' TO_DO: Check  Me._reader.DiscardUnreadData() in visa version 5. It did not work in 3.
    ''' </remarks>
    Public Sub DiscardUnreadData(ByVal messageAvailableBits As Integer)
        Do While (Me.ReadStatusByte() And messageAvailableBits) <> 0
            Me._reader.ReadLine()
        Loop
        ' Me._reader.ReadLine()
        ' this does not work! Me._reader.DiscardUnreadData()
    End Sub

    Private _reader As isr.Visa.MessageBasedReader
    ''' <summary>
    ''' Gets reference to the message based reader.
    ''' </summary>
    Public ReadOnly Property Reader() As isr.Visa.MessageBasedReader
        Get
            Return Me._reader
        End Get
    End Property

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property ReceiveBuffer() As String
        Get
            Return Me._reader.ReceiveBuffer
        End Get
    End Property

    ''' <summary>Reads the instrument and returns a Boolean value.</summary>
    Public Function ReadBoolean() As Nullable(Of Boolean)
        Return MessageBasedReader.ParseBoolean(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns a double value.</summary>
    Public Function ReadDouble() As Nullable(Of Double)
        Return MessageBasedReader.ParseDouble(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Int32 value.</summary>
    Public Function ReadInt32() As Nullable(Of Int32)
        Return MessageBasedReader.ParseInt32(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Int32 value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    Public Function ReadInfInt32() As Nullable(Of Int32)
        Return MessageBasedReader.ParseInfInt32(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Integer value.</summary>
    Public Function ReadInteger() As Nullable(Of Integer)
        Return MessageBasedReader.ParseInteger(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads the instrument and returns an Integer value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    Public Function ReadInfInteger() As Nullable(Of Integer)
        Return MessageBasedReader.ParseInfInteger(Me._reader.ReadLineTrimEnd())
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Shadows Function ReadString() As String
        Return Me._reader.ReadLine()
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadLine() As String
        Return Me._reader.ReadLine()
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadLineTrimEnd() As String
        Return Me._reader.ReadLineTrimEnd()
    End Function

    ''' <summary>
    ''' Gets or sets the termination character.
    ''' </summary>
    Public Shadows Property TerminationCharacter() As Byte
        Get
            Return MyBase.TerminationCharacter
        End Get
        Set(ByVal value As Byte)
            MyBase.TerminationCharacter = value
            Me._reader.TerminationCharacter = value
        End Set
    End Property

#End Region

#Region " WRITE "

    ''' <summary>
    ''' Sends all unsent data from the write buffer to the device.
    ''' </summary>
    Public Sub FlushWrite()
        Me._writer.Flush()
    End Sub

    Private _writer As isr.Visa.MessageBasedWriter
    ''' <summary>
    ''' Gets reference to the message based writer.
    ''' </summary>
    Public ReadOnly Property Writer() As isr.Visa.MessageBasedWriter
        Get
            Return Me._writer
        End Get
    End Property

    ''' <summary>Writes command to the instrument.</summary>
    ''' <param name="value">The string to write.</param>
    Public Shadows Sub Write(ByVal value As String)
        Me.WriteLine(value)
    End Sub

    ''' <summary>Writes command to the instrument.</summary>
    ''' <param name="value">The string to write.</param>
    Public Sub WriteLine(ByVal value As String)
        Me._writer.WriteLine(value)
    End Sub

    ''' <summary>Writes Boolean command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="value">The Boolean value to write.</param>
    ''' <param name="boolFormat">The Boolean format.</param>
    Public Sub WriteLine(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, value, boolFormat))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    Public Sub WriteLineOnOff(ByVal queryCommand As String, ByVal isOn As Boolean)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, isOn, BooleanDataFormat.OnOff))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="queryCommand">The string to write.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    Public Sub WriteLineOneZero(ByVal queryCommand As String, ByVal one As Boolean)
        Me.WriteLine(Ieee4882.Syntax.BuildBooleanCommand(queryCommand, one, BooleanDataFormat.OneZero))
    End Sub

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public ReadOnly Property TransmitBuffer() As String
        Get
            Return Me._writer.TransmitBuffer
        End Get
    End Property

#End Region

#Region " TIMEOUT MANAGEMENT "

    ''' <summary>
    ''' Gets or sets the minimum instrument timeout value in milliseconds
    ''' when accessing the device.
    ''' </summary>
    Public Shadows Property Timeout() As Integer
        Get
            Return MyBase.Timeout
        End Get
        Set(ByVal value As Integer)
            MyBase.Timeout = value
        End Set
    End Property

    ''' <summary>Restores the last timeout from the stack.
    ''' </summary>
    Public Sub RestoreTimeout()
        MyBase.Timeout = Me._timeouts.Pop
    End Sub

    ''' <summary>Saves the current timeout and sets a new setting timeout.
    ''' </summary>
    ''' <param name="timeout">Specifies the new timeout</param>
    Public Sub StoreTimeout(ByVal timeout As Integer)
        Me._timeouts.Push(MyBase.Timeout)
        MyBase.Timeout = timeout
    End Sub

    ''' <summary>Gets or sets the reference to the stack of timeouts.
    ''' </summary>
    Private _timeouts As System.Collections.Generic.Stack(Of Integer)

#End Region

#Region " VISA STATUS "

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
        Get
            Return MyBase.LastStatus < 0
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the last VISA operation ended with a warning.
    ''' </summary>
    Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
        Get
            Return MyBase.LastStatus > 0
        End Get
    End Property

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    Public Function BuildLastVisaStatusDetails() As String
        Return My.MyLibrary.BuildVisaStatusDetails(CType(MyBase.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
    End Function

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    Public Function BuildLastVisaStatusDetails(ByVal lastVisaAction As String) As String
        If String.IsNullOrWhiteSpace(lastVisaAction) Then
            Return Me.BuildLastVisaStatusDetails()
        Else
            Return My.MyLibrary.BuildVisaStatusDetails(lastVisaAction, CType(MyBase.LastStatus, NationalInstruments.VisaNS.VisaStatusCode))
        End If
    End Function

#End Region

End Class

