''' <summary>
''' Defines a supported command.  
''' A command is supported if its value is not empty or nothing.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/27/2007" by="David" revision="1.0.2917.x">
''' Created
''' </history>
Public Class SupportedCommand

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    ''' <param name="value">Specify the command value</param>
    Public Sub New(ByVal value As String)
        MyBase.New()
        Me._isSupported = Not String.IsNullOrWhiteSpace(value)
        Me._value = String.Empty
        If Me._isSupported Then
            Me._value = value
        End If
    End Sub

    Private _isSupported As Boolean
    ''' <summary>
    ''' Gets true if the command is supported.
    ''' </summary>
    Public ReadOnly Property IsSupported() As Boolean
        Get
            Return Me._isSupported
        End Get
    End Property

    Private _value As String
    ''' <summary>
    ''' Gets the command string.
    ''' </summary>
    Public ReadOnly Property Value() As String
        Get
            Return Me._value
        End Get
    End Property

    ''' <summary>
    ''' Returns the built command.
    ''' </summary>
     Public Function BuildCommand(ByVal ParamArray args() As Object) As String
        If Me._isSupported Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, Me._value, args)
        Else
            Return String.Empty
        End If
    End Function

End Class
