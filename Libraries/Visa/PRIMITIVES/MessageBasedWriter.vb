Imports NationalInstruments

''' <summary>Extends the VISA message based writer.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/04/08" by="David" revision="2.0.1841.x">
''' Created
''' </history>
Public Class MessageBasedWriter

    Inherits VisaNS.MessageBasedSessionWriter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="session">Specifies a reference to a valid 
    ''' <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    Public Sub New(ByVal session As NationalInstruments.VisaNS.IMessageBasedSession)

        ' instantiate the base class
        MyBase.New(session)
        Me._session = session

    End Sub

#End Region

#Region " I Resettable "

    ''' <summary>
    ''' Clears the error Queue and resets all event registers to zero by issuing the
    ''' IEEE488.2 common command.
    ''' </summary>
    ''' <param name="writer">A reference to an open
    ''' <see cref="MessageBasedWriter">writer</see>.</param>
    ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
    ''' <exception cref="System.ArgumentNullException">writer</exception>
    Public Shared Function ClearExecutionState(ByVal writer As MessageBasedWriter) As Boolean

        If writer Is Nothing Then
            Throw New ArgumentNullException("writer")
        End If

        ' Clear the device status.  This will set all the
        ' standard subsystem properties.
        writer.WriteLine(isr.Visa.Ieee4882.Syntax.ClearExecutionStateCommand)

        Return True

    End Function

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    Public Function ClearExecutionState() As Boolean

        ' Clear the device status.  This will set all the standard subsystem properties.
        Me.WriteLine(isr.Visa.Ieee4882.Syntax.ClearExecutionStateCommand)
        Return True

    End Function

    ''' <summary>Returns the device to its default known state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    Public Function ResetKnownState() As Boolean
        Me.WriteLine(isr.Visa.Ieee4882.Syntax.ResetKnownStateCommand)
        Return True
    End Function

#End Region

#Region " WRITE "

    Private _session As NationalInstruments.VisaNS.IMessageBasedSession
    ''' <summary>
    '''   ''' Gets or sets reference to the session underlying the message based writer.
    ''' </summary>
    Public ReadOnly Property Session() As NationalInstruments.VisaNS.IMessageBasedSession
        Get
            Return Me._session
        End Get
    End Property

    ''' <summary>Gets or sets the last message that was transmitted to the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Private _transmitBuffer As String

    ''' <summary>Gets or sets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    Public Property TransmitBuffer() As String
        Get
            Return Me._transmitBuffer
        End Get
        Friend Set(ByVal value As String)
            Me._transmitBuffer = value
        End Set
    End Property

    ''' <summary>Writes command to the instrument.</summary>
    ''' <param name="value">The string to write.</param>
    Public Overloads Sub WriteLine(ByVal value As String)
#If DIAGNOSE Then
    Trace.CorrelationManager.StartLogicalOperation("WRITE")
#End If

        If String.IsNullOrWhiteSpace(value) Then
            Me._transmitBuffer = ""

#If DIAGNOSE Then
      My.Application.Log.WriteEntry(Me._transmitBuffer, TraceEventType.Verbose)
#End If
            MyBase.WriteLine()

        Else
            Me._transmitBuffer = value

#If DIAGNOSE Then
      My.Application.Log.WriteEntry(Me._transmitBuffer, TraceEventType.Verbose)
#End If

            MyBase.WriteLine(value)
        End If

#If DIAGNOSE Then
    Trace.CorrelationManager.StopLogicalOperation()
#End If

    End Sub

#End Region

End Class
