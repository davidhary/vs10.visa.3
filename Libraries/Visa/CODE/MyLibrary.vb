Imports NationalInstruments
Imports System.Threading

Namespace My

    Public Module MyLibraryProperty
        ''' <summary>
        ''' Returns a singleton instance of the <see cref="Library">library manager</see>.
        ''' </summary>
        ''' <license>
        ''' (c) 2004 Integrated Scientific Resources, Inc.<para>
        ''' Licensed under The MIT License. </para><para>
        ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
        ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
        ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        ''' </para> </license>
        ''' <history date="11/22/04" by="David" revision="1.0.1787.x">
        ''' Created
        ''' </history>
        Public ReadOnly Property [Library]() As My.MyLibrary
            Get
                Return Global.isr.Visa.My.MyLibrary.[Get]
            End Get
        End Property
    End Module

    ''' <summary>Defines a singleton class to provide project management
    ''' for this project. This class has the Public Shared Not Creatable
    ''' instancing property.
    ''' </summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/22/04" by="David" revision="1.0.1787.x">
    ''' Created
    ''' </history>  
    Public NotInheritable Class MyLibrary

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Private Sub New()

            MyBase.New()
            _Epsilon = Single.Epsilon * 100
            _findLocalResourcesSymptom = ""
        End Sub

        ''' <summary>
        ''' The locking object to enforce thread safety when creating the singleton instance.
        ''' </summary>
        Private Shared ReadOnly syncLocker As New Object

        ''' <summary>
        ''' The shared instance.
        ''' </summary>
        Private Shared instance As MyLibrary

        ''' <summary>
        ''' Instantiates the class.
        ''' </summary>
        ''' <returns>
        ''' A new or existing instance of the class.
        ''' </returns>
        ''' <remarks>
        ''' Use this property to instantiate a single instance of this class.
        ''' This class uses lazy instantiation, meaning the instance isn't 
        ''' created until the first time it's retrieved.
        ''' </remarks>
        Public Shared Function [Get]() As MyLibrary
            If Not MyLibrary.Instantiated Then
                SyncLock MyLibrary.syncLocker
                    MyLibrary.instance = New MyLibrary
                End SyncLock
            End If
            Return MyLibrary.instance
        End Function

        ''' <summary>
        ''' Gets True if the singleton instance was instantiated.
        ''' </summary>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock MyLibrary.syncLocker
                    Return Not (MyLibrary.instance Is Nothing OrElse MyLibrary.instance.IsDisposed)
                End SyncLock
            End Get
        End Property


        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Public Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Private Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " BROADCAST AND TRACING "

        ''' <summary>
        ''' Gets or sets the Broadcast level.
        ''' </summary>
        Public Property BroadcastLevel() As TraceEventType

        ''' <summary>
        ''' Replaces the default file log trace listener with a new one.
        ''' </summary>
        ''' <param name="logWriter"></param>
        Public Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
            Application.Log.TraceSource.Listeners.Remove("FileLog")
            Application.Log.TraceSource.Listeners.Add(logWriter)
        End Sub

        Private _traceLevel As TraceEventType
        ''' <summary>
        ''' Gets or sets the trace level.
        ''' </summary>
        Public Property TraceLevel() As TraceEventType
            Get
                Return Me._traceLevel
            End Get
            Set(ByVal value As TraceEventType)
                traceLevelSetter(value)
            End Set
        End Property

        ''' <summary>
        ''' Sets the trace level. Can be called from the constructor as it is not Overridable.
        ''' </summary>
        ''' <param name="value">Specifies the <see cref="TraceEventType">trace level</see></param>
        Private Sub traceLevelSetter(ByVal value As TraceEventType)
            Me._traceLevel = value
            My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), Me._traceLevel.ToString), SourceLevels)
        End Sub

#End Region

#Region " ACCESS MANAGEMENT "

        ''' <summary>
        ''' Returns true if cache only access level is requested.
        ''' </summary>
        Public Shared Function IsCacheAccess(ByVal access As ResourceAccessLevels) As Boolean
            Return (access And ResourceAccessLevels.Cache) <> 0
        End Function

        ''' <summary>
        ''' Returns true if device access level is requested.
        ''' </summary>
        Public Shared Function IsDeviceAccess(ByVal access As ResourceAccessLevels) As Boolean
            Return (access And ResourceAccessLevels.Device) <> 0
        End Function

        ''' <summary>
        ''' Returns true if device verification access level is requested.
        ''' </summary>
        Public Shared Function IsVerifyAccess(ByVal access As ResourceAccessLevels) As Boolean
            Return (access And ResourceAccessLevels.Verify) <> 0
        End Function

#End Region

#Region " COMPARE "

        ''' <summary>Returns true if the absolute difference exceeds the Epsilon.</summary>
        Public Function AreDifferent(ByVal value1 As Double, ByVal value2 As Double) As Boolean
            Return Math.Abs(value1 - value2) > Epsilon
        End Function

        ''' <summary>Returns true if the absolute difference exceeds the Epsilon.</summary>
        Public Function AreDifferent(ByVal value1 As Double, ByVal value2 As Nullable(Of Double)) As Boolean
            Return Not value2.HasValue OrElse Math.Abs(value1 - value2.Value) > Epsilon
        End Function

        ''' <summary>Returns true if the absolute difference exceeds the Epsilon.</summary>
        Public Function AreDifferent(ByVal value1 As Nullable(Of Double), ByVal value2 As Nullable(Of Double)) As Boolean
            Return Not value1.HasValue OrElse Not value2.HasValue OrElse (Math.Abs(value1.Value - value2.Value) > Epsilon)
        End Function

        ''' <summary>Returns true if the absolute difference exceeds the Epsilon.</summary>
        Public Function AreDifferent(ByVal value1 As Nullable(Of Double), ByVal value2 As Double) As Boolean
            Return Not value1.HasValue OrElse (Math.Abs(value1.Value - value2) > Epsilon)
        End Function

        ''' <summary>Gets or sets the Epsilon for comparing values.</summary>
        Public Property Epsilon() As Double

#End Region

#Region " CONSTANTS "

        Private Shared _NA As String = "NA"
        Public Shared ReadOnly Property NA() As String
            Get
                Return MyLibrary._NA
            End Get
        End Property

#End Region

#Region " DELIMITED SCPI "

        Private Const startScpiDelimiter As Char = "("c
        Private Const endScpiDelimiter As Char = ")"c
        Private Const embeddedScpiFormat As String = startScpiDelimiter & "{0}" & endScpiDelimiter

        ''' <summary>
        ''' Get a delimited SCPI command.  This is used to help parse SCPI enumerated values that include
        ''' delimited SCPI command strings in their descriptions.
        ''' </summary>
        Public Shared Function DelimitedScpiCommand(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then Return ""
            Return String.Format(Globalization.CultureInfo.CurrentCulture, embeddedScpiFormat, value)
        End Function

        ''' <summary>
        ''' Extracts a SCPI string from the description of the relevant enumerated value.
        ''' </summary>
        ''' <param name="startDelimiter"></param>
        ''' <param name="endDelimiter"></param>
        Public Shared Function ExtractBetween(ByVal value As String, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
            If String.IsNullOrWhiteSpace(value) Then Return ""
            Dim startingIndex As Integer = value.IndexOf(startDelimiter)
            Dim endingIndex As Integer = value.LastIndexOf(endDelimiter)
            If startingIndex > 0 AndAlso endingIndex > startingIndex Then
                Return value.Substring(startingIndex + 1, endingIndex - startingIndex - 1)
            Else
                Return value
            End If
        End Function

        ''' <summary>
        ''' Extracts a SCPI string from the description of the relevant enumerated value.
        ''' </summary>
        Public Shared Function ExtractScpi(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then Return ""
            Return ExtractBetween(value, startScpiDelimiter, endScpiDelimiter)
        End Function

#End Region

#Region " IMMEDIATE IF "

        ''' <summary>Returns a string selected value</summary>
        ''' <param name="condition">The condition for selecting the true or false parts.</param>
        ''' <param name="truePart">The part selected if the condition is True.</param>
        ''' <param name="falsePart">The part selected if the condition is false.</param>
        Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

        ''' <summary>Returns an Int32 selected value</summary>
        ''' <param name="condition">The condition for selecting the true or false parts.</param>
        ''' <param name="truePart">The part selected if the condition is True.</param>
        ''' <param name="falsePart">The part selected if the condition is false.</param>
        Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Int32, ByVal falsePart As Int32) As Int32
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

#End Region

#Region " TEXT HELPERS "

        ''' <summary>
        ''' Adds text to string builder starting with a new line.
        ''' </summary>
        Public Shared Sub AddLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If builder Is Nothing Then
                    builder = New System.Text.StringBuilder
                End If
                If builder.Length > 0 Then
                    builder.Append(Environment.NewLine)
                End If
                builder.Append(value)
            End If
        End Sub

        ''' <summary>
        ''' Adds text to string builder starting with a new line.
        ''' </summary>
        Public Shared Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If builder Is Nothing Then
                    builder = New System.Text.StringBuilder
                End If
                If builder.Length > 0 Then
                    builder.Append(",")
                End If
                builder.Append(value)
            End If
        End Sub

        ''' <summary>
        ''' Gets the <see cref="ComponentModel.DescriptionAttribute" /> of an <see cref="[Enum]" /> type value.
        ''' </summary>
        ''' <param name="value">The <see cref="[Enum]" /> type value.</param>
        ''' <returns>A string containing the text of the <see cref="ComponentModel.DescriptionAttribute" />.</returns>
        ''' <exception cref="System.ArgumentNullException">value</exception>
        Public Shared Function GetDescription(ByVal value As [Enum]) As String

            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If

            Dim description As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(description)
            Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())

            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                description = attributes(0).Description
            End If
            Return description

        End Function

        ''' <summary>Returns the substring after the last occurrence of the specified string</summary>
        ''' <param name="source">The string to substring.</param>
        ''' <param name="startDelimiter">The start delimiter to search for.</param>
        ''' <param name="endDelimiter">The end delimiter to search for.</param>
        Public Shared Function SubstringBetween(ByVal source As String, ByVal startDelimiter As String, ByVal endDelimiter As String) As String

            If String.IsNullOrWhiteSpace(source) Then
                Return String.Empty
            End If

            If String.IsNullOrWhiteSpace(startDelimiter) Then
                Return String.Empty
            End If

            Dim startLocation As Int32 = source.LastIndexOf(startDelimiter, StringComparison.OrdinalIgnoreCase) + startDelimiter.Length
            Dim endLocation As Int32 = source.LastIndexOf(endDelimiter, StringComparison.OrdinalIgnoreCase)

            If startLocation >= 0 AndAlso startLocation < endLocation Then
                Return source.Substring(startLocation, endLocation - startLocation)
            Else
                Return String.Empty
            End If

        End Function

#End Region

#Region " ENCAPSULATE FIND RESOURCES WITH DIAGNOSTICS "

        Private Shared _lastLocatedResources As String()
        ''' <summary>
        ''' Returns the list of resources located with the last call to
        ''' <see cref="FindResources">Find Resources</see>.
        ''' </summary>
        Public Shared Function LastLocatedResources() As String()
            Return MyLibrary._lastLocatedResources
        End Function

        Private Shared _locatedLocalResources As Boolean?
        ''' <summary>
        ''' Gets the last indication if resources exist on the system.
        ''' This would be set to null if failed (error) locating resources.
        ''' </summary>
        Public Shared ReadOnly Property LocatedLocalResources() As Boolean?
            Get
                Return MyLibrary._locatedLocalResources
            End Get
        End Property

        Private Shared _findLocalResourcesSymptom As String
        ''' <summary>
        ''' Explains failure finding resources
        ''' </summary>
        Public Shared ReadOnly Property FindLocalResourcesSymptom() As String
            Get
                Return MyLibrary._findLocalResourcesSymptom
            End Get
        End Property

        Private Shared _failedFindLocalResources As Boolean?
        ''' <summary>
        ''' Returns true if the library attempted and had an exception finding local resources.
        ''' </summary>
        Public Shared ReadOnly Property FailedFindLocalResources() As Boolean?
            Get
                Return MyLibrary._failedFindLocalResources
            End Get
        End Property

        ''' <summary>
        ''' Finds a specific resource in the already located resource strings.
        ''' </summary>
        ''' <param name="resourceName">Specifies a full resource name matching the search resource type in 
        ''' <see cref="FindResources"></see></param>
        Public Shared Function FindResource(ByVal resourceName As String) As Boolean

            If MyLibrary._lastLocatedResources IsNot Nothing AndAlso MyLibrary._lastLocatedResources.Length > 0 Then
                For Each name As String In MyLibrary._lastLocatedResources
                    If String.Equals(name, resourceName, StringComparison.OrdinalIgnoreCase) Then
                        Return True
                    End If
                Next
            End If
            Return False

        End Function

        ''' <summary>
        ''' Encapsulates the <see cref="NationalInstruments.VisaNS.ResourceManager">resource manager</see>
        ''' method for finding resources.
        ''' </summary>
        ''' <param name="resourceSearchCommand">Specifies a resource name or resource search string.</param>
        ''' <remarks>
        ''' This came about because VISA failed returning resources getting
        ''' <see cref="VisaStatusCode.ErrorInvalidSetup">Invalid setup error</see>.
        ''' on a Semtech installation.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Shared Function FindResources(ByVal resourceSearchCommand As String) As String()

            Dim lastAction As String = ""
            MyLibrary._lastLocatedResources = New String() {}
            My.MyLibrary.ResetFindResources()

            MyLibrary._failedFindLocalResources = True
            Dim manager As NationalInstruments.VisaNS.ResourceManager
            Try

                lastAction = "Getting instance of local resource manager"

                ' get the local resource manager
                manager = VisaNS.ResourceManager.GetLocalManager()

                Try

                    lastAction = String.Format(Globalization.CultureInfo.CurrentCulture,
                                               "getting resources using the local VISA resource manager search command '{0}'",
                                               resourceSearchCommand)

                    MyLibrary._lastLocatedResources = manager.FindResources(resourceSearchCommand)
                    MyLibrary._locatedLocalResources = MyLibrary._lastLocatedResources IsNot Nothing AndAlso MyLibrary._lastLocatedResources.Length > 0
                    If Not MyLibrary._locatedLocalResources Then
                        MyLibrary._lastLocatedResources = New String() {}
                        MyLibrary._findLocalResourcesSymptom = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  "VISA local manager returned an empty resource list for the search command '{0}'", resourceSearchCommand)
                    End If
                    MyLibrary._failedFindLocalResources = False

                Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaStatusCode.ErrorResourceNotFound

                    MyLibrary._findLocalResourcesSymptom = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                               "The VISA local manager returned a VISA Resource Not Round error for the search command '{0}'",
                                                                resourceSearchCommand)
                    MyLibrary._locatedLocalResources = False
                    MyLibrary._lastLocatedResources = New String() {}
                    MyLibrary._failedFindLocalResources = False

                Catch ex As NationalInstruments.VisaNS.VisaException

                    MyLibrary._findLocalResourcesSymptom = "VISA exception occurred " & lastAction & ": " & ex.Message

                Catch ex As System.ArgumentException

                    ' trap the Run-time exception thrown : System.ArgumentException - Insufficient location 
                    ' information or the requested device or resource is not present in the system.  
                    ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
                    MyLibrary._findLocalResourcesSymptom = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                               "The VISA local manager returned an argument exception when using the search command '{0}' possibly because insufficient location information or the requested device or resource is not present in the system",
                                                                resourceSearchCommand)
                    MyLibrary._locatedLocalResources = False
                    MyLibrary._lastLocatedResources = New String() {}
                    MyLibrary._failedFindLocalResources = False

                Catch ex As Exception

                    MyLibrary._findLocalResourcesSymptom = "General exception occurred " & lastAction & ": " & ex.Message

                End Try

            Catch ex As NationalInstruments.VisaNS.VisaException

                MyLibrary._findLocalResourcesSymptom = "VISA exception occurred " & lastAction & ": " & ex.Message

            Catch ex As Exception

                MyLibrary._findLocalResourcesSymptom = "General exception occurred " & lastAction & ": " & ex.Message

            End Try

            Return MyLibrary._lastLocatedResources

        End Function

        ''' <summary>
        ''' Resets conditions for finding resources.
        ''' </summary>
        Public Shared Sub ResetFindResources()
            MyLibrary._locatedLocalResources = New Boolean?
            MyLibrary._failedFindLocalResources = New Boolean?
            MyLibrary._findLocalResourcesSymptom = ""
            MyLibrary._lastLocatedResources = New String() {}
        End Sub

#End Region

#Region " INTERFACES "

        Private Shared interfaceSearchString As String = "{0}?*INTFC"

        ''' <summary>Returns a string array of all local interfaces.</summary>
        Public Shared Function LocalInterfaceNames() As String()
            Return My.MyLibrary.LocalInterfaceNames(String.Empty)
        End Function

        ''' <summary>Returns a string array of all local interfaces matching
        '''   the interface base name such as GPIB.</summary>
        Public Shared Function LocalInterfaceNames(ByVal interfaceBaseName As String) As String()
            Return My.MyLibrary.FindResources(
                   String.Format(Globalization.CultureInfo.CurrentCulture, interfaceSearchString, interfaceBaseName))
        End Function

        ''' <summary>
        ''' Returns true if we have interfaces.
        ''' </summary>
        Public Shared Function HasInterfaces(ByVal interfaceBaseName As String) As Boolean
            Return My.MyLibrary.LocalInterfaceNames(interfaceBaseName).Length > 0 OrElse My.MyLibrary.FailedFindLocalResources.Value
        End Function

#End Region

#Region " RESOURCES "

        Private Shared serialResourceFormat As String = "ASRL{0}::INSTR"
        ''' <summary>Returns a Serial Port resource name string.</summary>
        ''' <param name="portNumber">A GPIB instrument address</param>
        ''' <returns>A VISA resource GPIB instrument name in the form "GPIB0::PrimaryAddress::INSTR".</returns>
        Public Shared Function BuildSerialResourceName(ByVal portNumber As Int32) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, serialResourceFormat, portNumber)
        End Function

        Private Shared allSearchString As String = "?*"
        Private Shared resourceSearchString As String = "{0}?*INSTR"
        Private Shared resourceBoardSearchString As String = "{0}{1}?*INSTR"

        ''' <summary>
        ''' This search string "{0}{1}?*?{2}INSTR"
        ''' while working for VISA COM does not work for managed VISA and is removed.
        ''' Rather, use the board only search string and then look for the required address in 
        ''' the list.
        ''' </summary>
        Private Shared resourceBoardAddressSearchString As String = "{0}{1}::{2}::INSTR"

        Public Const GpibResourceBaseName As String = "GPIB"
        Public Const SerialResourceBaseName As String = "ASRL"
        Public Const TcpIPResourceBaseName As String = "TCPIP"

        ''' <summary>Returns a resource base name.</summary>
        ''' <param name="resourceType">Specifies the 
        '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
        Public Shared Function ResourceBaseName(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String
            Select Case resourceType
                Case VisaNS.HardwareInterfaceType.Serial
                    Return SerialResourceBaseName
                Case VisaNS.HardwareInterfaceType.Gpib
                    Return GpibResourceBaseName
                Case VisaNS.HardwareInterfaceType.Tcpip
                    Return TcpIPResourceBaseName
                Case Else
                    Return resourceType.ToString
            End Select
        End Function

        ''' <summary>Returns a string array of all local resources matching
        '''   the resource base name such as GPIB.</summary>
        ''' <param name="resourceType">Specifies the 
        '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
        Public Shared Function LocalResourceNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String()
            Return My.MyLibrary.LocalResourceNames(ResourceBaseName(resourceType))
        End Function

        ''' <summary>Returns a string array of all local resources matching
        '''   the resource base name such as GPIB.</summary>
        ''' <param name="resourceBaseName">Specifies the resource base name.</param>
        Public Shared Function LocalResourceNames(ByVal resourceBaseName As String) As String()
            Return My.MyLibrary.FindResources(
                 String.Format(Globalization.CultureInfo.CurrentCulture, resourceSearchString, resourceBaseName))
        End Function

        ''' <summary>Returns a string array of all local resources matching
        '''   the resource base name such as GPIB.</summary>
        ''' <param name="resourceType">Specifies the 
        '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
        ''' <param name="boardNumber">Specifies the resource board number.</param>
        Public Shared Function LocalResourceNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType,
            ByVal boardNumber As Int32) As String()
            Return My.MyLibrary.LocalResourceNames(ResourceBaseName(resourceType), boardNumber)
        End Function

        ''' <summary>Returns a string array of all local resources matching
        '''   the resource base name such as GPIB.</summary>
        ''' <param name="resourceBaseName">Specifies the resource base name.</param>
        ''' <param name="boardNumber">Specifies the resource board number.</param>
        Public Shared Function LocalResourceNames(ByVal resourceBaseName As String,
            ByVal boardNumber As Int32) As String()
            Return My.MyLibrary.FindResources(
                 String.Format(Globalization.CultureInfo.CurrentCulture, resourceBoardSearchString, resourceBaseName, boardNumber))
        End Function

        ''' <summary>Returns a string array of all local resources matching
        '''   the resource base name such as GPIB.</summary>
        ''' <param name="resourceBaseName">Specifies the resource base name.</param>
        ''' <param name="boardNumber">Specifies the resource board number.</param>
        Public Shared Function LocalResourceNames(ByVal resourceBaseName As String,
            ByVal boardNumber As Int32, ByVal address As Integer) As String()
            Return My.MyLibrary.FindResources(
                 String.Format(Globalization.CultureInfo.CurrentCulture,
                               resourceBoardAddressSearchString, resourceBaseName, boardNumber, address))
        End Function

        ''' <summary>Returns a string array of all local resources.</summary>
        ''' <history date="08/05/2008" by="David" revision="3.0.3139.x">
        ''' Failed when tested at Semtech giving VISA error: Unable to start operation because 
        ''' setup is invalid (due to attributes being set to an inconsistent state).  
        ''' VISA error code -1073807302 (0xBFFF003A). ErrorInvalidSetup.
        ''' </history>
        Public Shared Function LocalResourceNames() As String()
            Return My.MyLibrary.FindResources(allSearchString)
        End Function

        ''' <summary>
        ''' Returns true if we have instrument resources.
        ''' </summary>
        Public Shared Function HasResources(ByVal resourceBaseName As String) As Boolean
            Return LocalResourceNames(resourceBaseName).Length > 0 OrElse My.MyLibrary.FailedFindLocalResources.Value
        End Function

        ''' <summary>
        ''' Returns true if we have instrument resources.
        ''' </summary>
        ''' <param name="resourceBaseName"></param>
        Public Shared Function HasResources(ByVal resourceBaseName As String, ByVal boardNumber As Integer) As Boolean
            Return LocalResourceNames(resourceBaseName, boardNumber).Length > 0 OrElse My.MyLibrary.FailedFindLocalResources.Value
        End Function

        ''' <summary>
        ''' Returns true if we have instrument resources.
        ''' </summary>
        ''' <param name="resourceBaseName"></param>
        ''' <param name="address"></param>
        Public Shared Function HasResources(ByVal resourceBaseName As String, ByVal boardNumber As Integer, ByVal address As Integer) As Boolean
            Return LocalResourceNames(resourceBaseName, boardNumber, address).Length > 0 OrElse My.MyLibrary.FailedFindLocalResources.Value
        End Function

#End Region

#Region " SESSIONS "

        ''' <summary>
        ''' Returns an open message based session.
        ''' </summary>
        ''' <param name="resourceName">Name of the resource.</param>
        ''' <returns>An open GPIB VISA session.</returns>
        ''' <exception cref="System.ArgumentNullException">resourceName</exception>
        ''' <exception cref="System.InvalidCastException">Resource selected must be a GPIB session</exception>
        Public Shared Function OpenGpibSession(ByVal resourceName As String) As isr.Visa.GpibSession
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New isr.Visa.GpibSession(resourceName)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("Resource selected must be a GPIB session", ex)
            End Try
        End Function

        ''' <summary>
        ''' Returns an open message based session.  Message-based sessions access VISA
        ''' resources in an interface-independent manner. Such sessions can communicate with an
        ''' instrument over a GPIB interface or serial interface.
        ''' </summary>
        ''' <param name="resourceName">Name of the resource.</param>
        ''' <returns>An open message-based VISA session.</returns>
        ''' <exception cref="System.ArgumentNullException">resourceName</exception>
        ''' <exception cref="System.InvalidCastException">Resource selected must be a message-based session</exception>
        Public Shared Function OpenMessageBasedSession(ByVal resourceName As String) As MessageBasedSession
            If resourceName Is Nothing Then
                Throw New ArgumentNullException("resourceName")
            End If
            Try
                Return New isr.Visa.MessageBasedSession(resourceName)
            Catch ex As InvalidCastException
                Throw New InvalidCastException("Resource selected must be a message-based session", ex)
            End Try
        End Function

#End Region

#Region " ESCAPE SEQUENCES "

        ''' <summary>Replace common escape sequences such as '\n' or '\r\ with the 
        '''   corresponding environment characters.</summary>
        ''' <param name="value">The string which characters are replaced.</param>
        Public Shared Function ReplaceCommonEscapeSequences(ByVal value As String) As String
            If value Is Nothing Then
                Return String.Empty
            Else
                Return value.Replace("\n", Convert.ToChar(10)).Replace("\r", Convert.ToChar(13))
            End If
        End Function

        ''' <summary>Replaces environment characters with common escape sequences such as 
        '''   '\n', for new line, or '\r\ for carriage return.</summary>
        ''' <param name="value">The string which characters are replaced.</param>
        Public Shared Function InsertCommonEscapeSequences(ByVal value As String) As String
            If value Is Nothing Then
                Return String.Empty
            Else
                Return value.Replace(Convert.ToChar(10), "\n").Replace(Convert.ToChar(13), "\r")
            End If
        End Function

#End Region

#Region " VISA STATUS MESSAGE BUILDERS "

        ''' <summary>Builds a VISA warning or error message.
        ''' </summary>
        ''' <param name="lastVisaAction"></param>
        ''' <param name="visaStatusCode">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
        ''' from the last operation.</param>
        Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String,
                                              ByVal visaStatusCode As Integer) As String
            Return BuildVisaStatusDetails(lastVisaAction, CType(visaStatusCode, NationalInstruments.VisaNS.VisaStatusCode))
        End Function

        ''' <summary>Builds a VISA warning or error message.
        ''' </summary>
        ''' <param name="lastVisaAction"></param>
        ''' <param name="visaStatusCode">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
        ''' from the last operation.</param>
        Public Shared Function BuildVisaStatusDetails(ByVal lastVisaAction As String,
                                              ByVal visaStatusCode As NationalInstruments.VisaNS.VisaStatusCode) As String

            Dim visaMessage As String = String.Empty
            If visaStatusCode = 0 Then

                visaMessage = "{0} OK."
                visaMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                            visaMessage, lastVisaAction)

            Else

                visaMessage = "{0} {1}."
                visaMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                            visaMessage, lastVisaAction, MyLibrary.BuildVisaStatusDetails(visaStatusCode))

            End If

            Return visaMessage

        End Function

        ''' <summary>Builds a VISA warning or error message.
        ''' </summary>
        ''' <param name="visaStatusCode">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
        ''' from the last operation.</param>
        Public Shared Function BuildVisaStatusDetails(ByVal visaStatusCode As Integer) As String

            Return BuildVisaStatusDetails(CType(visaStatusCode, NationalInstruments.VisaNS.VisaStatusCode))

        End Function


        ''' <summary>Builds a VISA warning or error message.
        ''' </summary>
        ''' <param name="visaStatusCode">The <see cref="NationalInstruments.VisaNS.VisaStatusCode">state code</see>
        ''' from the last operation.</param>
        Public Shared Function BuildVisaStatusDetails(ByVal visaStatusCode As NationalInstruments.VisaNS.VisaStatusCode) As String

            Dim visaMessage As String = String.Empty
            If visaStatusCode = 0 Then

            Else

                If visaStatusCode > 0 Then

                    visaMessage = "VISA Warning"

                Else

                    visaMessage = "VISA Error"

                End If

                If String.Equals(visaStatusCode.ToString, My.MyLibrary.GetDescription(visaStatusCode), StringComparison.CurrentCultureIgnoreCase) Then
                    visaMessage &= ": {0}."
                    visaMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                visaMessage, visaStatusCode)
                Else
                    visaMessage &= " {0}: {1}."
                    visaMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                visaMessage, visaStatusCode,
                                                 My.MyLibrary.GetDescription(visaStatusCode))
                End If

            End If

            Return visaMessage

        End Function

#End Region

    End Class

End Namespace

