#Region " TYPES "

''' <summary>
''' Determines how to access the device
''' </summary>
<Flags()> Public Enum ResourceAccessLevels
    <System.ComponentModel.Description("Access Device only if New Value")> None = 0
    <System.ComponentModel.Description("Access Device")> Device = 1
    <System.ComponentModel.Description("Access Device and Read Back and Verify")> Verify = 2
    <System.ComponentModel.Description("Access Cached Value")> Cache = 4
End Enum

''' <summary>
''' Enumerates the format for writing Boolean types.
''' </summary>
Public Enum BooleanDataFormat
    <ComponentModel.Description("Not Defined")> None
    <ComponentModel.Description("On or Off")> OnOff
    <ComponentModel.Description("One or Zero")> OneZero
    <ComponentModel.Description("True or False")> TrueFalse
End Enum

''' <summary>
''' Defines VISA resources.
''' </summary>
Public Enum VisaResourceClass
    <System.ComponentModel.Description("Not Defined")> None = 0
    <System.ComponentModel.Description("Interface")> [Interface]
    <System.ComponentModel.Description("Instrument")> Instrument
End Enum

''' <summary>
''' Enumerates the GPIB Commands.
''' </summary>
Public Enum GpibCommandCode
    <ComponentModel.Description("Not Defined")> None = 0
    <ComponentModel.Description("Go To Local (GTL)")> GoToLocal = &H1
    <ComponentModel.Description("Selective Device Clear (SDC)")> SelectiveDeviceClear = &H4
    <ComponentModel.Description("Group Execute Trigger (GET)")> GroupExecuteTrigger = &H8
    <ComponentModel.Description("Local Lockout (LLO)")> LocalLockout = &H11
    <ComponentModel.Description("Device Clear (DCL)")> DeviceClear = &H14
    <ComponentModel.Description("Serial Poll Enable (SPE)")> SerialPollEnable = &H18
    <ComponentModel.Description("Serial Poll Disable (SPD)")> SerialPollDisable = &H19
    <ComponentModel.Description("Listen Address Group (LAG)")> ListenAddressGroup = &H20
    <ComponentModel.Description("Talk Address Group (TAG)")> TalkAddressGroup = &H40
    <ComponentModel.Description("Secondary Command Group (SCG)")> SecondaryCommandGroup = &H60
    <ComponentModel.Description("Unlisten (UNL)")> Unlisten = &H3F
    <ComponentModel.Description("Untalk (UNT)")> Untalk = &H5F
End Enum

#End Region

