''' <summary>
''' Provides an interface for a VISA instrument.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/24/2008" by="David" revision="2.0.3003.x">
''' Created
''' </history>
Public Interface IInstrument

    Inherits isr.Core.IConnectResource, isr.Core.IResettableDevice, isr.Core.IPresettable, isr.Core.IMessagePublisher, isr.Core.IAnonPublisher

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
    ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

    ''' <summary>
    ''' Gets the last VISA Status.
    ''' </summary>
    ReadOnly Property LastVisaStatus() As Integer ' NationalInstruments.VisaNS.VisaStatusCode

    ''' <summary>
    ''' Returns the last VISA message status if any.
    ''' </summary>
    ReadOnly Property LastVisaStatusDetails() As String

    ''' <summary>
    ''' Returns the error queue and event status register (ESR) report.
    ''' </summary>
    ReadOnly Property DeviceErrors() As String

    ''' <summary>Queries the GPIB instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryTrimEnd(ByVal question As String) As String

    ''' <summary>Queries the GPIB instrument and returns a string save the termination character.</summary>
    ''' <param name="format">Specifies the format statement.
    ''' </param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary>Gets or sets the condition for using actual hardware or false if it is 
    ''' non-hardware emulation mode.</summary>
    Property UsingDevices() As Boolean

End Interface
