﻿''' <summary>
''' Defines the interface for handling limited device status exposed to the 
''' test manager.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/05/2008" by="David" revision="3.0.3351.x">
''' Created
''' </history>
Public Interface IMessageBasedStatus

    ''' <summary>
    ''' Returns an ESR Status record for the instrument.
    ''' </summary>
    Function BuildEsrStatus() As String

    ''' <summary>
    ''' Sets SBR bits to detect end of operation.
    ''' </summary>
    Sub DetectEndOperation()

    ''' <summary>
    ''' Sets the SBR bits for detecting SRQ to wait till instrument is ready for trigger
    ''' </summary>
    Sub DetectReady()

    ''' <summary>
    ''' Sets the SBR bits for detecting SRQ.
    ''' </summary>
    Sub DetectServiceRequest()

    ''' <summary>
    ''' Fetch the error queue.
    ''' </summary>
    Function DequeueErrorQueue() As String

    ''' <summary>
    ''' Enables detection of completion.
    ''' Enables all events on all but the control and standard events registers.
    ''' Requests the instrument to set the OPC bit of the status byte register after all operations are complete and issue an SRQ.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to request an OPC bit set after all operations are complete. 
    ''' The standard byte register must be cleared before issuing this command. 
    ''' You must enable the standard event register OPC byte before sending the 
    ''' operation complete service request command.
    ''' </remarks>
    Function EnableWaitComplete() As Boolean

    ''' <summary>
    ''' Returns the last event status value.
    ''' </summary>
    Property LastEventStatusValue() As Integer

    ''' <summary>
    ''' Returns true any of the events requested using the
    ''' <see cref="DetectEndOperation">end operations</see>
    ''' <see cref="DetectReady">ready</see> or <see cref="DetectServiceRequest">service request</see>
    ''' were detected. Namely, the corresponding bits in the service request register were set.
    ''' </summary>
    Function MonitoredStatusDetected() As Boolean

    ''' <summary>
    ''' Reads the event status register.
    ''' </summary>
    Function ReadEventStatus() As Integer

    ''' <summary>
    ''' Reports on device errors.
    ''' </summary>
    ''' <param name="synopsis">Specifies the synopsis for the current context.</param>
    ''' <param name="format">Specifies the message format/</param>
    ''' <param name="args">Specifies the message arguments.</param>
    Function ReportDeviceOperationOkay(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean

End Interface
