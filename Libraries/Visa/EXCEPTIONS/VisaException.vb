﻿''' <summary>Handles General VISA exceptions.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/27/07" by="David" revision="1.0.2917.x">
''' Created
''' </history>
<Serializable()> Public Class VisaException
    Inherits BaseException

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private Const defaultMessage As String = "failed writing or reading a VISA Command."
    ''' <summary>
    ''' Initializes a new instance of the <see cref="VisaException"/> class.
    ''' </summary>
    Public Sub New()
        MyBase.New(VisaException.defaultMessage)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub


    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.
    ''' </summary>
    ''' <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that
    ''' holds the serialized object data about the exception being thrown.</param>
    ''' <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that
    ''' contains contextual information about the source or destination.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="VisaException"/> class.
    ''' </summary>
    ''' <param name="visaStatus">Specifies the VISA status code.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the report argument.</param>
    Public Sub New(ByVal visaStatus As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args) &
                   ". Visa status: " & My.MyLibrary.BuildVisaStatusDetails(visaStatus))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="format">Specifies a format string for the message.</param>
    ''' <param name="args">Specifies the arguments for the message.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

#End Region

End Class

