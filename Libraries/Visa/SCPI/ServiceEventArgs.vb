Namespace Scpi

    ''' <summary>Provides event arguments for instrument events.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class ServiceEventArgs
        Inherits isr.Visa.BaseServiceEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Constructs this class.
        ''' </summary>
        ''' <param name="instrument">Reference to the <see cref="Instrument">controller.</see></param>
        Public Sub New(ByVal instrument As Instrument)

            ' instantiate the base class
            MyBase.New()
            Me._instrument = instrument

        End Sub

        ''' <summary>Constructs this class with a status message.</summary>
        ''' <param name="eventMessage">Specifies the event message.</param>
        ''' <param name="instrument">Reference to the <see cref="Instrument">controller.</see></param>
        ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
        Public Sub New(ByVal eventMessage As String, ByVal instrument As Instrument)

            ' instantiate the base class
            MyBase.New(eventMessage)
            Me._instrument = instrument

        End Sub

#End Region

#Region " CONTROLLER "

        ''' <summary>
        ''' Reference to the instrument controller.
        ''' </summary>
        Private _instrument As Instrument

#End Region

#Region " READ REGISTERS AND MESSAGES "

        ''' <summary>
        ''' Reads a message from the instrument.
        ''' </summary>
        Protected Overrides Function ReadLineTrimEnd() As String
            Return Me._instrument.ReadLineTrimEnd
        End Function

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadMeasurementEventStatus() As Integer
            Return Me._instrument.ReadMeasurementEventStatus(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadOperationEventStatus() As Integer
            Return Me._instrument.ReadOperationEventStatus(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Reads the Questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadQuestionableEventStatus() As Integer
            Return Me._instrument.ReadQuestionableEventStatus(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Reads the standard event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Protected Overrides Function ReadStandardEventStatus() As Integer
            Return Me._instrument.ReadStandardEventStatus
        End Function

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Protected Overrides Function ReadStatusByte() As Integer
            Return Me._instrument.ReadStandardEventStatus
        End Function

        ''' <summary>Returns the last error from the instrument.</summary>
        Protected Overrides Function ReadLastError() As String
            Return Me._instrument.ReadLastError
        End Function

        ''' <summary>Reads the error queue from the instrument.
        ''' </summary>
        Protected Overrides Function FetchErrorQueue() As String
            Return Me._instrument.FetchErrorQueue
        End Function

#End Region

    End Class

End Namespace
