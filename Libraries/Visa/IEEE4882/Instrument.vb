Imports isr.Core.EventHandlerExtensions
Imports NationalInstruments
Namespace Ieee4882

    ''' <summary>Provides a Core. for General GPIB instruments.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/20/2006" by="David" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    ''' <history date="11/22/2011" by="David" revision="1.0.4343.x">
    ''' Change are session events are enabled. See RegisterServiceRequestHandler.
    ''' </history>
    Public Class Instrument
        Inherits DeviceBase
        Implements IDisposable, IInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="ResourceTitle">Specifies the instrument name.</param>
        Public Sub New(ByVal resourceTitle As String)

            ' instantiate the base class
            MyBase.New(resourceTitle, HardwareInterfaceType.Gpib)

            ' instantiate event arguments
            Me._lastServiceEventArgs = New ServiceEventArgs(Me)

            ' clear the error queue.
            Me._errorQueue = New System.Text.StringBuilder

            ' Set the default values
            Me.IsTrimTerminator = True
            Me.ErrorAvailableBits = isr.Visa.Ieee4882.ServiceRequests.ErrorAvailable
            Me.MessageAvailableBits = isr.Visa.Ieee4882.ServiceRequests.MessageAvailable
            Me.IsCheckBufferOnReceive = True
            Me.IsCheckBufferOnSend = True
            Me.IsCheckOpcAfterSend = False
            Me.IsDelegateErrorHandling = False

            ' set default CLS and RST message
            Me.ClearExecutionStateCommand = isr.Visa.Ieee4882.Syntax.ClearExecutionStateCommand
            Me.ErrorQueueQueryCommand = ":STAT:QUE?"
            Me.ClearErrorQueueCommand = ":STAT:QUE:CLEAR"
            Me.StandardEventEnableCommand = isr.Visa.Ieee4882.Syntax.StandardEventEnableCommand
            Me.StandardEventEnableQueryCommand = isr.Visa.Ieee4882.Syntax.StandardEventEnableQueryCommand
            Me.StandardEventStatusQueryCommand = isr.Visa.Ieee4882.Syntax.StandardEventStatusQueryCommand
            Me.IdentifyQueryCommand = isr.Visa.Ieee4882.Syntax.IdentifyQueryCommand & " " & isr.Visa.Ieee4882.Syntax.WaitCommand
            Me.OperationCompletedCommand = isr.Visa.Ieee4882.Syntax.OperationCompletedCommand
            Me.OperationCompletedQueryCommand = isr.Visa.Ieee4882.Syntax.OperationCompletedQueryCommand & " " & isr.Visa.Ieee4882.Syntax.WaitCommand
            Me.ResetKnownStateCommand = isr.Visa.Ieee4882.Syntax.ResetKnownStateCommand
            Me.ServiceRequestEnableCommand = isr.Visa.Ieee4882.Syntax.ServiceRequestEnableCommand
            Me.ServiceRequestEnableQueryCommand = isr.Visa.Ieee4882.Syntax.ServiceRequestEnableQueryCommand
            Me.ServiceRequestStatusQueryCommand = isr.Visa.Ieee4882.Syntax.ServiceRequestStatusQueryCommand
            Me.WaitCommand = isr.Visa.Ieee4882.Syntax.WaitCommand

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        ' dispose of 'status' properties

                        If Me.CommandReceivedEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.CommandReceivedEvent.GetInvocationList
                                RemoveHandler Me.CommandReceived, CType(d, Global.System.EventHandler(Of System.EventArgs))
                            Next
                        End If

                        If Me.CommandSentEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.CommandSentEvent.GetInvocationList
                                RemoveHandler Me.CommandSent, CType(d, Global.System.EventHandler(Of System.EventArgs))
                            Next
                        End If

                        If Me.HandleErrorEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.HandleErrorEvent.GetInvocationList
                                RemoveHandler Me.HandleError, CType(d, Global.System.EventHandler(Of System.EventArgs))
                            Next
                        End If

                        If Me.ServiceRequestEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.ServiceRequestEvent.GetInvocationList
                                RemoveHandler Me.ServiceRequest, CType(d, Global.System.EventHandler(Of ServiceEventArgs))
                            Next
                        End If

                        If Me._session IsNot Nothing Then
                            If Me._requestRegistered Then
                                Me._requestRegistered = False
                                ' remove the service request handler
                                RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If

                        If Me._interface IsNot Nothing Then
                            Me._interface.Dispose()
                            Me._interface = Nothing
                        End If

                        If Me._errorQueue IsNot Nothing Then
                            Me._errorQueue = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " I ANON PUBLISHER "
        ''' <summary>
        ''' Publishes all values by raising the changed events.
        ''' </summary>
        Public Overrides Sub Publish()
            If Publishable Then
                For Each p As Reflection.PropertyInfo In GetType(Instrument).GetProperties(Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
                    OnPropertyChanged(p.Name)
                Next
            End If
        End Sub
#End Region

#Region " I CONNECTABLE RESOURCE "

        ''' <summary>
        ''' Gets the connection status.
        ''' The device is connected if it has a valid VISA session.
        ''' </summary>
        ''' <value><c>Connected</c> is a <see cref="Boolean" /> property that is True if the
        ''' connected (open).</value>
        ''' <history date="02/07/06" by="David" revision="1.00.2228.x">
        ''' Rename
        '''   </history>
        Public Overrides ReadOnly Property IsConnected As Boolean
            Get
                Return Me._session IsNot Nothing
            End Get
        End Property

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception> 
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Dim synopsis As String = "Connecting"
            If Me.IsConnected Then
                Return Me.IsConnected
            End If

            ' save the resource name.
            Me.ResourceName = resourceName

            If Not Me.UsingDevices Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "connected")
                MyBase.Connect(resourceName)

            End If

            Try

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Opening VISA session to {0}", resourceName)

                ' open a GPIB session to this instrument
                Me._session = New isr.Visa.GpibSession(resourceName)

                ' verify we have a termination character.
                If Me._session.TerminationCharacterEnabled Then
                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Auto termination character enabled for {0}", Me.ResourceName)
                End If
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Termination character set to ASCII {0}", CInt(Me._session.TerminationCharacter))

                Me.OnMessageAvailable(TraceEventType.Information, synopsis, "VISA session opened to {0}", resourceName)

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred connecting. Disconnecting. Details: {0}.", ex)

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            Finally

            End Try

            If Me._session Is Nothing Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "failed instantiating a VISA session. Disconnecting.{0}{1}",
                                      Environment.NewLine, isr.Core.StackTraceParser.UserCallStack(4, 4))
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            End If

            Try

                ' try reading the status byte to check if the instrument exists.
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Reading {0} instrument status register.", resourceName)
                Me.ReadDeviceStatusByte()

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Establishing connection to {0}", resourceName)
                Dim connectingEvents As New System.ComponentModel.CancelEventArgs()
                Me.OnConnecting(connectingEvents)
                If Not connectingEvents.Cancel Then
                    MyBase.Connect(resourceName)
                End If

            Catch ex As Exception

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred connecting. Disconnecting. Details: {0}", ex)
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            End Try

            Try

                If Not Me.IsConnected Then

                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Connection canceled.")
                    If Me._session IsNot Nothing Then
                        If Me._requestRegistered Then
                            Me._requestRegistered = False
                            ' remove the service request handler
                            RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                        End If
                        Me._session.Dispose()
                        Me._session = Nothing
                    End If

                End If

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred connecting, Disconnecting. Details: {0}.", ex)
                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            Catch ex As isr.Visa.BaseException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred connecting. Disconnecting. Details: {0}.", ex)
                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                Return False

            Finally

            End Try

            Return Me.IsConnected

        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="address">Specifies the primary address of the instrument.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Overrides Function Connect(ByVal address As Integer) As Boolean
            Return Me.Connect(GpibSession.BuildResourceName(address))
        End Function

        ''' <summary>Opens a GPIB VISA interface for the specified resource.</summary>
        ''' <param name="boardNumber">Specifies the GPIB resource board name.</param>
        ''' <param name="address">Specifies the GPIB resource address.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overridable Overloads Function Connect(ByVal boardNumber As Integer, ByVal address As Integer) As Boolean
            Return Me.Connect(GpibSession.BuildResourceName(boardNumber, address))
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <param name="resourceTitle">Specifies the title of the resource in human readable form.</param>
        Public Overloads Overrides Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean
            Me.ResourceTitle = resourceTitle
            Return Me.Connect(resourceName)
        End Function

        ''' <summary>Returns true if the instrument has a valid bus and primary address.
        ''' Before connecting, a valid bus must be established.
        ''' </summary>
        Public ReadOnly Property Connectable(ByVal resourceName As String) As Boolean
            Get
                If Me.IsConnected Then
                    Return False
                Else
                    Return Instrument.IsValidPrimaryAddress(GpibSession.ParsePrimaryAddress(resourceName))
                End If
            End Get
        End Property

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function Disconnect() As Boolean

            Dim synopsis As String = "Disconnecting"
            If Not Me.IsConnected Then
                Return Not Me.IsConnected
            End If

            Dim e As New System.ComponentModel.CancelEventArgs
            Try

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "disconnecting {0}", Me.ResourceName)

                Me.OnDisconnecting(e)

            Catch ex As Exception
                ' throw an exception
                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred disconnecting. Details: {0}", ex)
            Finally
                Try
                    If Not e.Cancel Then
                        If Me._session IsNot Nothing Then
                            If Me._requestRegistered Then
                                Me._requestRegistered = False
                                ' remove the service request handler
                                RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                            Me._session.Dispose()
                            Me._session = Nothing
                        End If
                        MyBase.Disconnect()
                    End If
                Catch
                End Try
            End Try
            Return Not Me.IsConnected

        End Function

        ''' <summary>
        ''' True when using actual devices or False for running the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean Implements IInstrument.UsingDevices

#Region " CONNECTION EVENT HANDLERS "

        ''' <summary>
        ''' Implements full reset clear that was removed from the instrument connect event to 
        ''' be implement by each of the inheriting entities.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overridable Sub OnConnectedResetClear()

            Dim synopsis As String = "Connect Reset and Clear"
            Try

#If DIAGNOSE Then
        Trace.CorrelationManager.StartLogicalOperation("CONNECT")
#End If

                Me.StoreTimeout(Me.ConnectTimeout)

                ' allow connection time to materialize
                Threading.Thread.Sleep(100)

                ' step through connection sequence allowing to proceed in DIAGNOSE mode

                ' verify we have a termination character.
#If DIAGNOSE Then
        If Me._session.TerminationCharacterEnabled Then
          Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Auto termination character enabled")
        End If
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Termination character set to {0}", CInt(Me._session.TerminationCharacter))
#End If

                ' flush read and write buffers
                Try
                    Me.StoreTimeout(1000)
                    Me._discardUnreadData(3, 100, True)
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Exception occurred flushing read buffer. Details: {1}. Ignored.", Me.ResourceName, ex)
                Finally
                    Me.RestoreTimeout()
                End Try
                Me._flushWrite()

                ' 8/11/2008 new order. Clear execution test is part of reset and clear.

                ' clear execution state
                ' Me.ClearExecutionState()

                ' issue a selective device clear and total clear of the instrument.
                Me.ResetAndClear()

                ' register the handler before enabling the event
                Me.RegisterServiceRequestHandler(Ieee4882.StandardEvents.UserRequest, Ieee4882.ServiceRequests.All)

                ' restore session timeout 
                Me.RestoreTimeout()

            Catch ex As Exception

                ' throw an exception
                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred. Details: {0}", ex)

            Finally

#If DIAGNOSE Then
        Trace.CorrelationManager.StopLogicalOperation()
#End If

            End Try

        End Sub

#End Region

#End Region

#Region " I Resettable "

        Public Overrides Function ClearResource() As Boolean
            Return Me.ResetAndClear
        End Function

        Private _resetKnownStateTimeout As Int32 = 10000
        ''' <summary>
        ''' Gets or sets the time out for resetting to known state.
        ''' </summary>
        Public Property ResetKnownStateTimeout() As Integer
            Get
                Return Me._resetKnownStateTimeout
            End Get
            Set(ByVal Value As Integer)
                Me._resetKnownStateTimeout = Value
            End Set
        End Property

        ''' <summary>Selectively clears the instrument.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearActiveState() As Boolean

            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Try
                    Me._session.ClearActiveState()
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING ACTIVE STATE",
                                              "Timeout occurred when clearing active state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                              "VISA Exception occurred when clearing active state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING ACTIVE STATE",
                                          "Exception occurred when clearing active state. Details: {0}.", ex)
                End Try
            End If
            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overrides Function ClearExecutionState() As Boolean

            Dim synopsis As String = "Clearing Execution State"
            If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._clearExecutionStateCommand.IsSupported Then
                Try
                    Me.WriteLine(Me._clearExecutionStateCommand.Value)

                    Me.OnMessageAvailable(TraceEventType.Verbose, "CLEARED EXECUTION STATE", "Cleared execution state.")
                Catch ex As NationalInstruments.VisaNS.VisaException
                    If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                        Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT CLEARING EXECUTION STATE",
                                              "Timeout occurred when clearing execution state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                    Else
                        Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                              "VISA Exception occurred when clearing execution state. Details: {0}.", ex)
                    End If
                Catch ex As Exception
                    Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING EXECUTION STATE",
                                          "Exception occurred when clearing execution state. Details: {0}.", ex)
                End Try

                If Not Me.IsLastVisaOperationSuccess Then
                    Me.OnMessageAvailable(TraceEventType.Warning, synopsis, "failed clearing execution state. VISA status={0}.{1}{2}",
                                          Me.LastVisaStatus, Environment.NewLine,
                                          isr.Core.StackTraceParser.UserCallStack(4, 10))
                End If

                ' Clear the event arguments
                Me._lastServiceEventArgs = New ServiceEventArgs(Me)

                ' clear the last read status byte
                Me.ClearDeviceStatusByte()

                ' clear the error queue for compatibility with how the source meters (2400) work.
                Me.ClearErrorQueue()

            End If

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>Resets and clears the device. 
        ''' Issues device clear (e.g., SDC), reset to known state (e.g., RST), 
        ''' and Clear Execution State (e.g., CLS and clear error queue). Typically, the source meter may
        ''' required a 5000 milliseconds timeout.
        ''' </summary>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            Dim synopsis As String = "Reset and Clear"
            If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                Return True
            End If

            Try

#If DIAGNOSE Then
        Trace.CorrelationManager.StartLogicalOperation("RESET")
#End If

                Dim affirmative As Boolean = True

                ' MUST CLEAR IN THIS ORDER:

                ' clear the device
                affirmative = affirmative AndAlso Me.ClearActiveState()

                ' reset the device and set default properties
                affirmative = affirmative AndAlso Me.ResetKnownState()

                ' Clear the device Status and set more defaults
                affirmative = affirmative AndAlso Me.ClearExecutionState()

                ' preset the event registers to their power on conditions
                ' including the event transition registers.
                ' Scpi.StatusSubsystem.Preset()
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Reset and cleared.")

                Return affirmative

            Catch ex As NationalInstruments.VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred resetting and clearing. Details: {0}.", ex)

                Return False

            Finally

#If DIAGNOSE Then
        Trace.CorrelationManager.StopLogicalOperation()
#End If

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session. Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                Return True
            End If

            Me.StoreTimeout(Math.Max(Me._session.Timeout, timeout))
            Dim outcome As Boolean = Me.ResetAndClear()
            Me.RestoreTimeout()
            Return outcome

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overrides Function ResetKnownState() As Boolean

            ' Minimum timeout for instrument reset
            Return Me.ResetKnownState(Me._resetKnownStateTimeout)

        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overloads Function ResetKnownState(ByVal timeout As Integer) As Boolean

            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then

                If Me._resetKnownStateCommand.IsSupported Then

                    Try

                        ' set minimum timeout
                        Me.StoreTimeout(timeout)

                        ' flush buffers
                        Me.Session.Reader.DiscardUnreadData()

                        Me.WriteLine(Me._resetKnownStateCommand.Value)

                        ' update any properties that the instrument sets to their defaults when issuing RST.
                        Return Me.IsLastVisaOperationSuccess

                    Catch ex As NationalInstruments.VisaNS.VisaException
                        If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                            Me.OnMessageAvailable(TraceEventType.Error, "TIMEOUT RESETTING KNOWN STATE",
                                                  "Timeout occurred when resetting known state. VISA status={0}. Details: {1}.", Me.LastVisaStatus, ex)
                        Else
                            Me.OnMessageAvailable(TraceEventType.Error, "VISA EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                                  "VISA Exception occurred when resetting known state. Details: {0}.", ex)
                        End If
                    Catch ex As Exception
                        Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED RESETTING KNOWN STATE",
                                              "Exception occurred when resetting known state. Details: {0}.", ex)
                    Finally

                        ' restore timeout.
                        Me.RestoreTimeout()

                    End Try

                End If

            End If
            Return False

        End Function

#End Region

#Region " GETTERS AND SETTERS "

#Region " VALUE GETTERS "

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <returns>Nullable{Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function FetchInt32(ByVal queryCommand As SupportedCommand) As Nullable(Of Int32)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If queryCommand.IsSupported Then
                Return Me.QueryInt32(queryCommand.Value)
            End If
        End Function

#End Region

#Region " VALUE SETTERS "

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="value">Specifies the command value.</param>
        ''' <returns>Int32.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal value As Int32) As Int32
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Me.WriteLine(queryCommand.BuildCommand(value))
            Return value
        End Function

        ''' <summary>
        ''' Verifies an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="value">Specifies the command value.</param>
        ''' <returns>Int32.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        ''' <exception cref="ReadException"></exception>
        Public Function Verify(ByVal queryCommand As SupportedCommand, ByVal value As Int32) As Int32
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            Dim actual As Nullable(Of Int32) = Me.FetchInt32(queryCommand)
            If actual.HasValue Then
                If value <> actual Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value), value, actual.Value)
                End If
            Else
                Throw New ReadException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    "Failed reading from VISA resource {0}", Me.ResourceName))
            End If
            Return value
        End Function

#End Region

#Region " PROPERTY GETTERS "

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="value">Specifies the command value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function Getter(ByVal queryCommand As SupportedCommand, ByVal value As String, ByVal access As ResourceAccessLevels) As String
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If My.MyLibrary.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(value) Then
                value = Me.QueryTrimEnd(queryCommand.Value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>Nullable{System.Boolean}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function Getter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Boolean),
                                ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If My.MyLibrary.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
                nullableValue = Me.QueryBoolean(queryCommand.Value)
            End If
            Return nullableValue
        End Function

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>Nullable{Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function Getter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Int32),
                                ByVal access As ResourceAccessLevels) As Nullable(Of Int32)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If My.MyLibrary.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
                nullableValue = Me.QueryInt32(queryCommand.Value)
            End If
            Return nullableValue
        End Function

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>Nullable{System.Double}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function Getter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Double),
                               ByVal access As ResourceAccessLevels) As Nullable(Of Double)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If My.MyLibrary.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
                nullableValue = Me.QueryDouble(queryCommand.Value)
            End If
            Return nullableValue
        End Function

        ''' <summary>
        ''' Gets a value from the instrument using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>Nullable{System.Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Function GetterInfinity(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Integer),
                                        ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If My.MyLibrary.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
                nullableValue = Me.QueryInfInt32(queryCommand.Value)
            End If
            Return nullableValue
        End Function

#End Region

#Region " PROPERTY SETTERS "

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="value">The value.</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="System.ArgumentNullException">value</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As String,
                               ByVal value As String, ByVal access As ResourceAccessLevels) As String
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If String.IsNullOrWhiteSpace(value) Then
                Throw New ArgumentNullException("value")
            End If
            If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                (My.MyLibrary.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(nullableValue) OrElse
                 (value <> nullableValue)) Then
                Me.WriteLine(queryCommand.BuildCommand(value))
            End If
            If My.MyLibrary.IsVerifyAccess(access) Then
                Dim actual As String = String.Empty
                actual = Me.Getter(queryCommand, actual, access)
                If Not String.Equals(value, actual, StringComparison.OrdinalIgnoreCase) Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value), value, actual)
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="value">if set to <c>True</c> [value].</param>
        ''' <param name="numeric">if set to <c>True</c> [numeric].</param>
        ''' <param name="access">Specifies the desired access level to the instrument.</param>
        ''' <returns>Nullable{System.Boolean}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Boolean),
                               ByVal value As Boolean, ByVal numeric As Boolean, ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                   (My.MyLibrary.IsDeviceAccess(access) OrElse nullableValue Is Nothing OrElse
                    Not nullableValue.HasValue OrElse (value <> nullableValue)) Then
                Me.WriteLine(queryCommand.BuildCommand(value, numeric))
            End If
            If My.MyLibrary.IsVerifyAccess(access) Then
                Dim actual As Nullable(Of Boolean) = Me.Getter(queryCommand, actual, access)
                If value <> actual.Value Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value, numeric), value, actual.Value)
                End If
            End If
            Return New Nullable(Of Boolean)(value)
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="value">The value.</param>
        ''' <param name="access">The access.</param>
        ''' <returns>Nullable{System.Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Integer),
                               ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                  (My.MyLibrary.IsDeviceAccess(access) OrElse nullableValue Is Nothing OrElse
                   Not nullableValue.HasValue OrElse (value <> nullableValue)) Then
                Me.WriteLine(queryCommand.BuildCommand(value))
            End If
            If My.MyLibrary.IsVerifyAccess(access) Then
                Dim actual As Nullable(Of Integer) = Me.Getter(queryCommand, actual, access)
                If value <> actual Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value), value, actual.Value)
                End If
            End If
            Return New Nullable(Of Integer)(value)
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="value">The value.</param>
        ''' <param name="access">The access.</param>
        ''' <returns>Nullable{System.Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        Public Function SetterInfinity(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Integer),
                                        ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                   (My.MyLibrary.IsDeviceAccess(access) OrElse nullableValue Is Nothing OrElse
                    Not nullableValue.HasValue OrElse (value <> nullableValue)) Then
                Me.WriteLine(queryCommand.BuildCommand(value))
            End If
            If My.MyLibrary.IsVerifyAccess(access) Then
                Dim actual As Nullable(Of Integer) = Me.GetterInfinity(queryCommand, actual, access)
                If value <> actual Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value), value, actual.Value)
                End If
            End If
            Return New Nullable(Of Integer)(value)
        End Function

        ''' <summary>
        ''' Sets an instrument value using the <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see>.</param>
        ''' <param name="nullableValue">Specifies the command value.</param>
        ''' <param name="value">The value.</param>
        ''' <param name="access">The access.</param>
        ''' <returns>Nullable{System.Double}.</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="isr.Visa.VerificationException"></exception>
        Public Function Setter(ByVal queryCommand As SupportedCommand, ByVal nullableValue As Nullable(Of Double),
                                ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                  (My.MyLibrary.IsDeviceAccess(access) OrElse nullableValue Is Nothing OrElse
                   Not nullableValue.HasValue OrElse My.Library.AreDifferent(value, nullableValue)) Then
                Me.WriteLine(queryCommand.BuildCommand(value))
            End If
            If My.MyLibrary.IsVerifyAccess(access) Then
                Dim actual As Nullable(Of Double) = Me.Getter(queryCommand, actual, access)
                If My.Library.AreDifferent(value, actual.Value) Then
                    Throw New isr.Visa.VerificationException(
                            queryCommand.BuildCommand(value), value, actual.Value)
                End If
            End If
            Return New Nullable(Of Double)(value)
        End Function

#End Region

#End Region

#Region " LAST OPERATION STATUS MANAGEMENT "

        Private _lastOperationOkay As Boolean
        ''' <summary>
        ''' Gets the status of the last operation.
        ''' </summary>
        Public Property LastOperationOkay() As Boolean
            Get
                Return Me._lastOperationOkay
            End Get
            Protected Set(ByVal value As Boolean)
                Me._lastOperationOkay = value
            End Set
        End Property

#End Region

#Region " ERROR MANAGEMENT "

        ''' <summary>Clears the error Queue.</summary>
        Public Overridable Function ClearErrorQueue() As Boolean

            Dim synopsis As String = "Clear Error Queue"
            If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._clearErrorQueueCommand.IsSupported Then

#If DIAGNOSE Then
        Trace.CorrelationManager.StartLogicalOperation("Clear error queue")
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Clearing {0} error queue", Me.ResourceName)
#End If

                Try

                    Me.WriteLine(Me._clearErrorQueueCommand.Value)

                Catch ex As Exception

#If DIAGNOSE Then
          Me.OnMessageAvailable(TraceEventType.Error,synopsis, "Exception occurred {0}. VISA status={2}. Details: {1}", 
                                     Me._lastMessage, ex, Me.LastVisaStatus)
#Else
                    Throw
#End If

                End Try

                If Not Me.IsLastVisaOperationSuccess Then
                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Failed clearing error queue. VISA status={0}.{1}{2}",
                                          Me.LastVisaStatus, Environment.NewLine,
                                          isr.Core.StackTraceParser.UserCallStack(4, 10))
                End If

#If DIAGNOSE Then
        Trace.CorrelationManager.StopLogicalOperation()
#End If

            End If
            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Returns the error queue and event status register (ESR) report.
        ''' </summary>
        Public ReadOnly Property DeviceErrors() As String Implements IInstrument.DeviceErrors
            Get
                Dim message As New System.Text.StringBuilder
                If Me._errorQueue.Length > 0 Then
                    If message.Length > 0 Then
                        message.AppendLine()
                    End If
                    message.AppendLine("The device reported the following error(s) from the error queue:")
                    message.Append(Me._errorQueue.ToString)
                End If

                Dim report As String
                If Me._standardEventStatusQueryCommand.IsSupported AndAlso Me._standardEventStatus.HasValue Then
                    report = Instrument.BuildEsrReport(Me._standardEventStatus.Value, "; ")
                    If Not String.IsNullOrWhiteSpace(report) Then
                        If message.Length > 0 Then
                            message.AppendLine()
                        End If
                        message.Append(report)
                    End If
                End If
                Return message.ToString
            End Get
        End Property

        ''' <summary>Gets or sets bits that would be set for detecting if an error
        ''' is available.</summary>
        Private _errorAvailableBits As isr.Visa.Ieee4882.ServiceRequests

        ''' <summary>Gets or sets bits that would be set for detecting if an error
        ''' is available.</summary>
        Public Property ErrorAvailableBits() As isr.Visa.Ieee4882.ServiceRequests
            Get
                Return Me._errorAvailableBits
            End Get
            Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
                Me._errorAvailableBits = Value
            End Set
        End Property

        ''' <summary>
        ''' Gets the error count on a remote node.
        ''' </summary>
        Public Function ErrorQueueCount(ByVal access As ResourceAccessLevels) As Integer

            If (My.MyLibrary.IsDeviceAccess(access) OrElse Me._errorQueue Is Nothing) AndAlso
                Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso
                Me._errorQueueCountQueryCommand.IsSupported Then

                Dim count As Integer? = Me.QueryInt32(Me._errorQueueCountQueryCommand.Value)
                If Me.ReportVisaDeviceOperationOkay(True, "Fetch error queue count", "Failed fetching error queue count") Then
                    Return count.Value
                Else
                    Return 0
                End If

            Else
                Return 0
            End If

        End Function


        ''' <summary>Gets or sets the error status.</summary>
        Private _errorQueue As System.Text.StringBuilder

        ''' <summary>Gets or sets the last read error queue.
        ''' Setting the error allows emulating or clearing errors when the error queue command
        ''' is not supported or not using devices.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Property ErrorQueue(ByVal access As ResourceAccessLevels) As String
            Get
                Try

                    If (My.MyLibrary.IsDeviceAccess(access) OrElse Me._errorQueue Is Nothing) AndAlso
                        Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._errorQueueQueryCommand.IsSupported Then

                        ' clear the queue string
                        Me._errorQueue = New System.Text.StringBuilder

                        ' loop while we have an error
                        Do While (Me.DeviceStatusByte(ResourceAccessLevels.Device) And Me.ErrorAvailableBits) <> 0

                            If Me._errorQueue.Length > 0 Then
                                Me._errorQueue.Append(Environment.NewLine)
                            End If

                            ' turn on the error state.
                            Me._lastServiceEventArgs.HasError = True

                            ' send the command to get the queue
                            Me.WriteQueryLine(Me._errorQueueQueryCommand.Value)

                            ' Add the error string
                            Me._errorQueue.AppendFormat("Device error {0}", Me.ReadLineTrimEnd())

                        Loop

                    Else
                        If Me._errorQueue Is Nothing Then
                            Me._errorQueue = New System.Text.StringBuilder
                        End If
                    End If

                    ' return the queue
                    Return Me._errorQueue.ToString

                Catch ex As Exception

                    ' if we have an error, return a note of it
                    Return "Exception occurred getting the error queue. Details: " & ex.ToString

                End Try
            End Get
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
            Set(ByVal Value As String)
                If Not (Me.UsingDevices AndAlso Me._errorQueueQueryCommand.IsSupported) Then
                    Me._errorQueue = New System.Text.StringBuilder
                    If String.IsNullOrWhiteSpace(Value) Then
                        ' turn off the error state.
                        Me._lastServiceEventArgs.HasError = False
                    Else
                        ' turn on the error state.
                        Me._lastServiceEventArgs.HasError = True
                        Me._errorQueue.Append(Value)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public ReadOnly Property HadError() As Boolean
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                    ' new since 3346
                    Me._lastServiceEventArgs.HasError = Me._lastServiceEventArgs.HasError OrElse
                                                     (Me.DeviceStatusByte(ResourceAccessLevels.Device) And Me._errorAvailableBits) <> 0
                    Return Me._lastServiceEventArgs.HasError
                    Return Me._lastServiceEventArgs.HasError OrElse (Me.DeviceStatusByte(ResourceAccessLevels.Device) And Me._errorAvailableBits) <> 0
                Else
                    Return Me._lastServiceEventArgs.HasError
                End If
#If DEBUG Then
                ' In debug mode, make sure to wait for the end of service
                ' request processing to get the error immediately after
                ' the instrument reports it.  In this mode, we expect to have
                ' syntax error that the instrument is unlikely to be happy about.
                ' once in release mode, the incidences are lower and we can wait for
                ' the error rather than slowing operations due to this delay
                Return Me.HadError(10)
#End If
            End Get
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        ''' <param name="timeout">Time to wait before returning the error.</param>
        Private ReadOnly Property HadError(ByVal timeout As Integer) As Boolean
            Get
                ' add a couple of milliseconds wait
                Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, timeout))
                Do
                    Threading.Thread.Sleep(Math.Min(10, timeout))
                Loop While Me._lastServiceEventArgs.ServicingRequest And (Date.Now <= endTime)
                Return Me.HadError '  Me._lastServiceEventArgs.HasError
            End Get
        End Property

        ''' <summary>Gets or sets the condition for telling the device to raise the
        ''' <see cref="HandleError">handle error event</see> to delegate error
        ''' handling to the calling instrument.</summary>
        Private _isDelegateErrorHandling As Boolean

        ''' <summary>Gets or sets the condition for telling the device to raise the
        ''' <see cref="HandleError">handle error event</see> to delegate error
        ''' handling to the calling instrument.</summary>
        Public Property IsDelegateErrorHandling() As Boolean
            Get
                Return Me._isDelegateErrorHandling
            End Get
            Set(ByVal Value As Boolean)
                Me._isDelegateErrorHandling = Value
            End Set
        End Property

        ''' <summary>Reads the error queue from the instrument.
        ''' </summary>
        Public Function FetchErrorQueue() As String
            Return Me.ErrorQueue(ResourceAccessLevels.Device)
        End Function

        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Private _lastErrorQueryCommand As SupportedCommand = New SupportedCommand(":SYST:ERR?")

        ''' <summary>Gets or sets the last error Query Command for this instrument.</summary>
        Public Property LastErrorQueryCommand() As String
            Get
                Return Me._lastErrorQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._lastErrorQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Returns the last error from the instrument.</summary>
        Public Function ReadLastError() As String
            Return Me.Getter(Me._lastErrorQueryCommand, String.Empty, ResourceAccessLevels.Device)
        End Function

#End Region

#Region " EVENTS AND HANDLERS "

        ''' <summary>Raised whenever a reply is received from the instrument.</summary>
        ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
        ''' </param>
        Public Event CommandReceived As EventHandler(Of EventArgs)

        ''' <summary>Raises the command received event.
        ''' </summary>
        Protected Overridable Sub OnCommandReceived()
            ' prepend message if verbose
            ' it is too much to send a message from this level
            ' Me.PrependVerboseMessage("{0} received '{1}'", Me.ResourceName, Me._session.ReceiveBuffer)
            Dim evt As EventHandler(Of EventArgs) = Me.CommandReceivedEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, System.EventArgs.Empty)
        End Sub

        ''' <summary>Raised whenever a command is sent to the instrument.</summary>
        ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
        ''' </param>
        Public Event CommandSent As EventHandler(Of EventArgs)

        ''' <summary>Raises the command sent event.
        ''' </summary>
        Protected Overridable Sub OnCommandSent()
            ' prepend message if verbose
            ' it is too much to send a message from this level
            ' Me.PrependVerboseMessage("{0} sent '{1}'", Me.ResourceName, Me._session.TransmitBuffer)
            Dim evt As EventHandler(Of EventArgs) = Me.CommandSentEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, System.EventArgs.Empty)
        End Sub

        ''' <summary>Occurs when the status module needs to have
        ''' the calling routine read the error queue allowing for different
        ''' ways by which to read the error queue.
        ''' </summary>
        ''' <param name="e">Specifies reference to the 
        ''' <see cref="EventArgs">event arguments</see>.
        ''' </param>
        Public Event HandleError As EventHandler(Of EventArgs)

        ''' <summary>Raises the Handle Error event.</summary>
        Protected Overridable Sub OnHandleError()
            Dim evt As EventHandler(Of EventArgs) = Me.HandleErrorEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, System.EventArgs.Empty)
        End Sub

#End Region

#Region " IEEE488.2 COMMANDS "

        ''' <summary>
        ''' Asserts a hardware trigger. The device is addressed to listen, and then
        ''' the GPIB GET command is sent.
        ''' </summary>
        ''' <returns>Returns a long or invalid.
        ''' </returns>
        Public Overridable Function AssertTrigger() As Boolean
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
#If DIAGNOSE Then
        Dim synopsis As String = "Assert Trigger"
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Asserting trigger to {0}", Me.ResourceName)
#End If
                Me._session.AssertTrigger()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Allows controlling the instrument manually.
        ''' </summary>
        Public Function GoToLocal() As Boolean

            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
#If DIAGNOSE Then
        Dim synopsis As String = "GO TO LOCAL"
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "{0} is going to local", Me.ResourceName)
#End If
                Me._session.ControlRen(RenMode.Deassert)
            End If
            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Gets or sets the instrument identity. Reads the instrument identity the first time it is 
        ''' called. Setting the identity allows emulation.
        ''' Cleared when connected.
        ''' </summary>
        Public Overrides Property Identity() As String
            Get
                If String.IsNullOrWhiteSpace(MyBase.Identity) Then
                    If Me.Session IsNot Nothing Then
                        MyBase.Identity = Me.Session.ReadIdentity()
                    ElseIf Me._session Is Nothing Then
                        MyBase.Identity = ""
                    Else
                        MyBase.Identity = "Salute to SCPI Corp., Model 1, 123, A"
                    End If
                    'Me.ParseInstrumentId(MyBase.Identity)
                End If
                Return MyBase.Identity
            End Get
            Set(ByVal value As String)
                MyBase.Identity = value
                'Me.ParseInstrumentId(MyBase.Identity)
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets (for emulation) the completion status of the last operation.
        ''' </summary>
        Public Property OperationCompleted(ByVal access As ResourceAccessLevels) As Boolean
            Get
                If My.MyLibrary.IsDeviceAccess(access) AndAlso Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._operationCompletedQueryCommand.IsSupported Then

                    ' too much: Me.PrependVerboseMessage("Requesting OPC from {0}", Me.ResourceName)

                    ' Read OPC from the device.
                    Dim opcValue As Nullable(Of Integer) = Me.QueryInt32(Me._operationCompletedQueryCommand.Value)
                    If opcValue.HasValue Then
                        Me._lastServiceEventArgs.OperationCompleted = opcValue.Value = 1
                    Else
                        ThrowSessionCommandException(Me._session, Me._operationCompletedQueryCommand)
                    End If

                Else

                    Me._lastServiceEventArgs.OperationCompleted = True

                End If

                Return Me._lastServiceEventArgs.OperationCompleted

            End Get
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
            Set(ByVal value As Boolean)
                If Not (Me.UsingDevices AndAlso Me._operationCompletedQueryCommand.IsSupported) Then
                    Me._lastServiceEventArgs.OperationCompleted = value
                End If
            End Set
        End Property

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function QueryOpc() As Boolean
            Return Me.OperationCompleted(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Waits until some event.
        ''' </summary>
        Public Function WaitOnEvent(ByVal timeout As Int32) As NationalInstruments.VisaNS.MessageBasedSessionEventArgs
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Return Me._session.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            Else
                Return CType(NationalInstruments.VisaNS.MessageBasedSessionEventArgs.Empty, NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            End If
        End Function

#End Region

#Region " EXCEPTIONS "

        Private Const _sessionCommandMessage As String = "failed writing to or reading from {0} using the command {1}."
        Private Const _sessionMessage As String = "failed writing to or reading from {0}."
        ''' <summary>
        ''' Throws an exception specifying the <paramref name="session">session</paramref> and
        ''' <paramref name="queryCommand">command</paramref>.
        ''' </summary>
        ''' <param name="session">Specifies the <see cref="NationalInstruments.VisaNS.MessageBasedSession">visa session</see></param>
        ''' <param name="queryCommand">Specifies the <see cref="SupportedCommand">command</see> that was sent to the instrument</param>
        ''' <exception cref="System.ArgumentNullException">session;Session not specified when attempting to throw the exception</exception>
        ''' <exception cref="IOException"></exception>
        Public Shared Sub ThrowSessionCommandException(ByVal session As NationalInstruments.VisaNS.Session, ByVal queryCommand As SupportedCommand)
            If session Is Nothing Then
                Throw New ArgumentNullException("session", "Session not specified when attempting to throw the exception")
            End If
            If queryCommand Is Nothing Then
                Throw New IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    Instrument._sessionMessage, session.ResourceName))
            Else
                Throw New IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    Instrument._sessionCommandMessage, session.ResourceName,
                                                    queryCommand.Value))
            End If

        End Sub

        ''' <summary>
        ''' Throws session command specifying the <paramref name="session">session</paramref>.
        ''' </summary>
        ''' <param name="session">Specifies the <see cref="NationalInstruments.VisaNS.MessageBasedSession">visa session</see></param>
        ''' <exception cref="System.ArgumentNullException">session;Session not specified when attempting to throw the exception</exception>
        ''' <exception cref="IOException"></exception>
        Public Shared Sub ThrowSessionException(ByVal session As NationalInstruments.VisaNS.Session)
            If session Is Nothing Then
                Throw New ArgumentNullException("session", "Session not specified when attempting to throw the exception")
            End If
            Throw New IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                Instrument._sessionMessage, session.ResourceName))
        End Sub


#End Region

#Region " IEEE488.2 REGISTER COMMANDS "

#Region " DEVICE STATUS BYTE / SERIAL POLL "

        ''' <summary>
        ''' Clears the last read device status byte.
        ''' </summary>
        Public Sub ClearDeviceStatusByte()
            Me._deviceStatusByte = New Nullable(Of isr.Visa.Ieee4882.ServiceRequests)
        End Sub

        ''' <summary>Gets or sets the last serial poll value.
        ''' </summary>
        Private _deviceStatusByte As Nullable(Of isr.Visa.Ieee4882.ServiceRequests)

        ''' <summary>
        ''' <summary>
        ''' Reads or sets the serial poll value.
        ''' Setting this value is allowed for emulation when using devices.
        ''' </summary>
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Property DeviceStatusByte(ByVal access As ResourceAccessLevels) As isr.Visa.Ieee4882.ServiceRequests
            Get
                If (My.MyLibrary.IsDeviceAccess(access) OrElse Not Me._deviceStatusByte.HasValue) AndAlso Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                    ' too much.  This is constantly monitored: Me.PrependVerboseMessage("Reading device status byte from {0}", Me.ResourceName)
                    Me._deviceStatusByte = CType(Me._session.ReadStatusByte(), isr.Visa.Ieee4882.ServiceRequests)
                End If
                If Not Me._deviceStatusByte.HasValue Then
                    Me._deviceStatusByte = Ieee4882.ServiceRequests.Unknown
                    ' Exceptions should not be raised from properties.
                    ' Throw New ReadException(String.Format(Globalization.CultureInfo.CurrentCulture, 
                    '                           "Failed reading device status byte from VISA resource {0}.", Me.ResourceName))
                End If
                Return Me._deviceStatusByte.Value
            End Get
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
            Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
                If Not Me.UsingDevices Then
                    Me._deviceStatusByte = Value
                End If
            End Set
        End Property

        ''' <summary>Returns a serial poll HEX caption.
        ''' </summary>
        Public Function DeviceStatusByteCaption() As String
            Return "0x" & CInt(Me._deviceStatusByte.Value).ToString("X", Globalization.CultureInfo.CurrentCulture).PadLeft(2, "0"c)
        End Function

        ''' <summary>
        ''' Reads the device status byte.
        ''' </summary>
        Public Function ReadDeviceStatusByte() As isr.Visa.Ieee4882.ServiceRequests
            Return DeviceStatusByte(ResourceAccessLevels.Device)
        End Function
#End Region

#Region " ESE "

        ''' <summary>Gets or sets the last standard event request enable (ESE) data byte.</summary>
        Private _standardEventEnable As Nullable(Of isr.Visa.Ieee4882.StandardEvents)

        ''' <summary>Reads or write the standard event request enable (ESE) data byte.
        ''' For instruments not supporting event status register (ESE) this can be set as the
        ''' default value returned when reading ESE.  ESE can be set also if not using devices.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Property StandardEventEnable(ByVal access As ResourceAccessLevels) As isr.Visa.Ieee4882.StandardEvents
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._standardEventEnableQueryCommand.IsSupported Then
                    Dim value As Nullable(Of Int32) = Me.Getter(Me._standardEventEnableQueryCommand, Me._standardEventEnable, access)
                    If value.HasValue Then
                        Me._standardEventEnable = CType(value.Value, isr.Visa.Ieee4882.StandardEvents)
                    Else
                        Me._standardEventEnable = isr.Visa.Ieee4882.StandardEvents.Unknown
                    End If
                End If
                Return Me._standardEventEnable.Value
            End Get
            Set(ByVal Value As isr.Visa.Ieee4882.StandardEvents)
                If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                       (Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._standardEventEnableCommand.IsSupported) Then
                    Dim eseValue As Nullable(Of Int32) = Me.Setter(Me._standardEventEnableCommand, Me._standardEventEnable, Value, access)
#If DIAGNOSE Then
          Dim synopsis As String = "Enable Standard Events"
          Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} standard events {1}", Me.ResourceName, CInt(Value))
#End If
                    If eseValue.HasValue Then
                        Me._standardEventEnable = CType(eseValue.Value, isr.Visa.Ieee4882.StandardEvents)
                    Else
                        Me._standardEventEnable = isr.Visa.Ieee4882.StandardEvents.Unknown
                    End If
                Else
                    ' set for emulation
                    Me._standardEventEnable = Value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadStandardEventEnable() As isr.Visa.Ieee4882.StandardEvents
            Return Me.StandardEventEnable(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Writes to the standard event enable register.
        ''' </summary>
        Public Sub WriteStandardEventEnable(ByVal value As isr.Visa.Ieee4882.StandardEvents)
            Me.StandardEventEnable(ResourceAccessLevels.Device) = value
        End Sub

#End Region

#Region " ESR "

        ''' <summary>Returns a detailed report of the event status register (ESR) byte.
        ''' </summary>
        ''' <param name="esrValue">Specifies the value that was read from the status register.</param>
        Public Shared Function BuildEsrReport(ByVal esrValue As isr.Visa.Ieee4882.StandardEvents, ByVal delimiter As String) As String

            If String.IsNullOrWhiteSpace(delimiter) Then
                delimiter = "; "
            End If

            Dim message As New System.Text.StringBuilder

            For Each value As isr.Visa.Ieee4882.StandardEvents In [Enum].GetValues(GetType(isr.Visa.Ieee4882.ServiceRequests))
                If value <> Ieee4882.StandardEvents.None AndAlso value <> Ieee4882.StandardEvents.All AndAlso ((value And esrValue) <> 0) Then
                    If message.Length > 0 Then
                        message.Append(delimiter)
                    End If
                    message.Append(My.MyLibrary.GetDescription(value))
                End If
            Next

            If message.Length > 0 Then
                message.Append(".")
                message.Insert(0, "The device standard status register reported: " & Environment.NewLine)
            End If
            Return message.ToString

        End Function

        ''' <summary>Gets or sets the last service request event status (ESR) data byte.</summary>
        Private _standardEventStatus As Nullable(Of isr.Visa.Ieee4882.StandardEvents)

        ''' <summary>Reads or sets the service request event status (ESR) data byte.
        ''' For instruments not supporting event status register (ESR) this can be set as the
        ''' default value returned when reading ESR.  ESR can be set also if not using devices.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Property StandardEventStatus(ByVal access As ResourceAccessLevels) As isr.Visa.Ieee4882.StandardEvents
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._standardEventStatusQueryCommand.IsSupported Then
                    Me._standardEventStatus = CType(Me.Getter(Me._standardEventStatusQueryCommand, Me._standardEventStatus, access).Value, isr.Visa.Ieee4882.StandardEvents)
                End If
                Return Me._standardEventStatus.Value
            End Get
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
            Set(ByVal Value As isr.Visa.Ieee4882.StandardEvents)
                If Not (Me.UsingDevices AndAlso Me._standardEventStatusQueryCommand.IsSupported) Then
                    Me._standardEventStatus = Value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Public Function ReadStandardEventStatus() As isr.Visa.Ieee4882.StandardEvents
            Return Me.StandardEventStatus(ResourceAccessLevels.Device)
        End Function

#End Region

#Region " MAV "

        ''' <summary>Reads the device status data byte and returns True
        ''' if the massage available bits were set.
        ''' </summary>
        ''' <returns>True if the device has data in the queue
        ''' </returns>
        Public Overridable Function IsMessageAvailable() As Boolean

            ' check if we have data in the queue
            Return (Me._session.ReadStatusByte() And Me._messageAvailableBits) <> 0

        End Function

        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Private _messageAvailableBits As isr.Visa.Ieee4882.ServiceRequests = ServiceRequests.MessageAvailable

        ''' <summary>Gets or sets the serial poll bit value for determining
        ''' if a message is available.  This is required in order to flush the
        ''' read buffer as the GPIB formatted I/O does not seem to work right.
        ''' </summary>
        Public Property MessageAvailableBits() As isr.Visa.Ieee4882.ServiceRequests
            Get
                Return Me._messageAvailableBits
            End Get
            Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
                Me._messageAvailableBits = Value
            End Set
        End Property

#End Region

#Region " SRE: SERVICE REQUEST ENABLE "

        ''' <summary>Gets or sets the service request enable (SRE) command message for this instrument.</summary>
        Private _serviceRequestEnableCommand As SupportedCommand

        ''' <summary>Gets or sets the service request enable (SRE) command message for this instrument.</summary>
        Public Property ServiceRequestEnableCommand() As String
            Get
                Return Me._serviceRequestEnableCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._serviceRequestEnableCommand = New SupportedCommand(Value)
                If Me._serviceRequestEnableCommand.IsSupported Then
                    Me.ServiceRequestEnableQueryCommand = Value & "?"
                Else
                    Me.ServiceRequestEnableQueryCommand = String.Empty
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Private _serviceRequestEnableQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the service request enable (SRE) Query Command for this instrument.</summary>
        Public Property ServiceRequestEnableQueryCommand() As String
            Get
                Return Me._serviceRequestEnableQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._serviceRequestEnableQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the last service request event Enable (SRE) data byte.</summary>
        Private _serviceRequestEventEnable As Nullable(Of isr.Visa.Ieee4882.ServiceRequests)

        ''' <summary>Reads or write the service request event Enable (SRE) data byte.
        ''' For instruments not supporting event status register (SRE) this can be set as the
        ''' default value returned when reading SRE.  SRE can be set also if not using devices.
        ''' The binary equivalent of the returned value indicates which register bits
        ''' Assigning a value to this attribute enables one or more status events for
        ''' service request. When an enabled status event occurs, bit B6 of the status
        ''' byte sets to generate an SRQ (service request). The service request enable
        ''' register uses most of the same summary events as the status byte.
        ''' Bit B6 (MSS) is not used by the enable register.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Property ServiceRequestEventEnable(ByVal access As ResourceAccessLevels) As isr.Visa.Ieee4882.ServiceRequests
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._serviceRequestEnableQueryCommand.IsSupported Then
                    Dim value As Nullable(Of Int32) = Me.Getter(Me._serviceRequestEnableQueryCommand, Me._serviceRequestEventEnable, access)
                    If value.HasValue Then
                        Me._serviceRequestEventEnable = CType(value.Value, isr.Visa.Ieee4882.ServiceRequests)
                    Else
                        Me._serviceRequestEventEnable = isr.Visa.Ieee4882.ServiceRequests.Unknown
                    End If
                End If
                Return Me._serviceRequestEventEnable.Value
            End Get
            Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
                If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                    (Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._serviceRequestEnableCommand.IsSupported) Then
#If DIAGNOSE Then
          Dim synopsis As String = "Enable Service Request event"
          Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} service request events {1}", Me.ResourceName, CInt(Value))
#End If
                    Dim sreValue As Nullable(Of Int32) = Me.Setter(Me._serviceRequestEnableCommand, Me._serviceRequestEventEnable, Value, access)
                    If sreValue.HasValue Then
                        Me._serviceRequestEventEnable = CType(sreValue.Value, isr.Visa.Ieee4882.ServiceRequests)
                    Else
                        Me._serviceRequestEventEnable = isr.Visa.Ieee4882.ServiceRequests.Unknown
                    End If
                Else
                    ' set for emulation
                    Me._serviceRequestEventEnable = Value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadServiceRequestEventEnable() As isr.Visa.Ieee4882.ServiceRequests
            Return Me.ServiceRequestEventEnable(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Writes to the event register.
        ''' </summary>
        Public Function WriteServiceRequestEventEnable(ByVal value As isr.Visa.Ieee4882.ServiceRequests) As isr.Visa.Ieee4882.ServiceRequests
            Me.ServiceRequestEventEnable(ResourceAccessLevels.Device) = value
        End Function

#End Region

#Region " SRQ "

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeoutSeconds">
        ''' Specifies how many seconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <param name="pollDelay">Specifies milliseconds between serial polls.</param>
        ''' <history>
        ''' 07/02/99  David Hary  1.3.9  Add Status Byte Register argument
        ''' 12/15/00  David Hary  1.09.12  Add long time out option.
        ''' 11/05/01  David Hary  1.11.10  Use new GPIB classes
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.Ieee4882.ServiceRequests,
                                 ByVal timeoutSeconds As Single, ByVal pollDelay As Integer) As Boolean

            Dim endTime As Date

            ' check if time out is negative
            If timeoutSeconds >= 10 * Single.Epsilon Then
                endTime = DateTime.Now.AddSeconds(timeoutSeconds)
            Else
                ' if negative, set to 'infinite' value.
                endTime = DateTime.Now.AddDays(1)
            End If

            ' Clear the SRQ flag
            Me.ReadDeviceStatusByte()

            ' Loop until SRQ or Time out
            Do

                ' await for some time
                Threading.Thread.Sleep(pollDelay)

            Loop Until Me.CachedServiceRequest(statusByteBits) OrElse (DateTime.Now.CompareTo(endTime) > 0)

            ' Set the time out flag to true if the SRQ flag was not set
            Me._timedOut = Not Me.IsReceivedSrq(statusByteBits)

            Return Not Me._timedOut AndAlso Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeoutSeconds">
        ''' Specifies how many seconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <history>
        ''' 07/02/99  David Hary  1.3.9  Add Status Byte Register argument
        ''' 12/15/00  David Hary  1.09.12  Add long time out option.
        ''' 11/05/01  David Hary  1.11.10  Use new GPIB classes
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.Ieee4882.ServiceRequests, ByVal timeoutSeconds As Single) As Boolean

            Return Me.AwaitServiceRequest(statusByteBits, timeoutSeconds, 1)

        End Function

        ''' <summary>Awaits for service request.
        '''   Use this method to wait for an operation to complete
        '''   on this device.  Set the time out value to negative
        '''   value for infinite time out.
        ''' </summary>
        ''' <param name="statusByteBits">Specifies the status byte which to check.
        ''' </param>
        ''' <param name="timeout">
        ''' Specifies how many milliseconds to wait for the service request before exiting this function.  Set to zero
        ''' for an infinite (100 seconds) timeout.
        ''' </param>
        ''' <param name="pollDelay">Specifies milliseconds between serial polls.</param>
        ''' <history>
        ''' 3.0.3478: Renamed from AwaitSrq
        ''' </history>
        Public Function AwaitServiceRequest(ByVal statusByteBits As isr.Visa.Ieee4882.ServiceRequests,
                                 ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

            Return Me.AwaitServiceRequest(statusByteBits, 0.001! * timeout, pollDelay)

        End Function

        ''' <summary>Serial polls if not already received SRQ.
        ''' This function sets the service request tag (IsReceivedSrq)
        ''' property of this device.  If the SRQ is set (i.e.,
        ''' SRQ serial poll bit was set), the function returns
        ''' true and sets the SRQ tag to true.
        ''' Thus, you must make sure that the SRQ tag property
        ''' is cleared (set to False) before inquiring about the
        ''' SRQ bit again.
        ''' </summary>
        ''' <returns>True if service requested.
        ''' </returns>
        ''' <param name="statusBits">Specifies which bits to look for
        ''' in the status byte register.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Function CachedServiceRequest(ByVal statusBits As isr.Visa.Ieee4882.ServiceRequests) As Boolean

            ' return true if already received the service request.  Otherwise, read and return true if now got it.
            Return Me.IsReceivedSrq(statusBits) OrElse ((Me.DeviceStatusByte(ResourceAccessLevels.Device) And statusBits) <> 0)

        End Function

        ''' <summary>Requests the instrument to set the OPC
        ''' bit of the status byte register after all operations
        ''' are complete and issue an SRQ.
        ''' Use this method to request an OPC bit set after
        ''' all operations are complete.
        ''' The standard byte register must be cleared before
        ''' issuing this command.
        ''' You must enable the standard event register OPC byte
        ''' before  issuing this command.
        ''' </summary>
        ''' <history>
        ''' </history>
        Public Function EnableOpcSrqDeprecated() As Boolean

            Debug.Assert(Not Debugger.IsAttached, "DEPRECATED - USE IssueOperationCompleteRequest()")
            ' update: Check if necessary. clear the service request bit
            ' Me._Scpi.GPIBdDevice.SerialPoll And &hFF
            ' this is done only when enabling service request.
            ' Me.ReadDeviceStatusByte()

            ' Issue service request
            Return Me.IssueOperationCompleteRequest()

        End Function

        ''' <summary>Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that will issue an SRQ
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' 3.0.3478: Renamed from EnableSrq.
        ''' </history>
        Public Function EnableServiceRequestLong(ByVal standardEventEnableBits As isr.Visa.Ieee4882.StandardEvents,
                                  ByVal serviceRequestEnableBits As isr.Visa.Ieee4882.ServiceRequests) As Boolean

            ' clear the event status.
            Me.ClearExecutionState()

            ' Check if necessary; clear the service request bit
            Me.ReadDeviceStatusByte()

            ' build the request message.
            Me.WriteStandardEventEnable(standardEventEnableBits)
            Me.WriteServiceRequestEventEnable(serviceRequestEnableBits)

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Sets the device to issue an SRQ upon any of the SCPI events.
        ''' Uses *ESE to select (mask) the events that will issue SRQ
        ''' and  *SRE to select (mask) the event registers to be
        ''' included in the bits that will issue an SRQ
        ''' Uses a single line command to accomplish the entire command so as to save time.
        ''' Also issues *OPC.
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        ''' <history>
        ''' 3.0.3478: Renamed from EnableSrq.
        ''' </history>
        Public Function EnableServiceRequestComplete(ByVal standardEventEnableBits As isr.Visa.Ieee4882.StandardEvents,
                                  ByVal serviceRequestEnableBits As isr.Visa.Ieee4882.ServiceRequests) As Boolean

            ' clear internal elements.
            Me.ClearDeviceStatusByte()

            ' create the command string.
            Dim commandLine As New System.Text.StringBuilder
            commandLine.AppendFormat("{0}{1}", Me._clearExecutionStateCommand.Value, Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me._standardEventEnableCommand.BuildCommand(CInt(standardEventEnableBits)), Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me._serviceRequestEnableCommand.BuildCommand(CInt(serviceRequestEnableBits)), Environment.NewLine)
            commandLine.AppendFormat("{0}{1}", Me._operationCompletedCommand.Value, Environment.NewLine)

            Me.WriteLine(commandLine.ToString)

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Returns true if service request bit is on.  Does not
        ''' change the cached value of the service request condition.
        ''' </summary>
        Public Function IsRequestingService() As Boolean
            Return (Me.DeviceStatusByte(ResourceAccessLevels.Device) And isr.Visa.Ieee4882.ServiceRequests.RequestingService) <> 0
        End Function

        ''' <summary>
        ''' Gets or sets the cached service request status.
        ''' True if service request was received.
        ''' </summary>
        ''' <param name="statusBits">Specifies which bits to look for
        ''' in the status byte register.
        ''' </param>
        Public Property IsReceivedSrq(ByVal statusBits As isr.Visa.Ieee4882.ServiceRequests) As Boolean
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                    Return (Me._deviceStatusByte.GetValueOrDefault(0) And statusBits) <> 0 OrElse (Me.DeviceStatusByte(ResourceAccessLevels.None) And statusBits) <> 0
                Else
                    Return (statusBits And Ieee4882.ServiceRequests.Unknown) <> 0
                End If
            End Get
            Set(ByVal Value As Boolean)
                ' use for simulation.
                If Value Then
                    If Me._deviceStatusByte.HasValue Then
                        Me._deviceStatusByte = Me._deviceStatusByte.Value Or statusBits
                    Else
                        Me._deviceStatusByte = statusBits
                    End If
                Else
                    If Me._deviceStatusByte.HasValue Then
                        Me._deviceStatusByte = Me._deviceStatusByte.Value And Not statusBits
                    Else
                        Me._deviceStatusByte = Ieee4882.ServiceRequests.None
                    End If
                End If
            End Set
        End Property

        ''' <summary>Issues an OPC command, e.g., *OPC, requesting
        ''' the instrument to turn on the OPC bit upon completion.
        ''' </summary>
        Public Function IssueOperationCompleteRequest() As Boolean

            If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._operationCompletedCommand.IsSupported Then
                ' Output OPC for waiting for the operation completion.
                Return Me.WriteLine(Me._operationCompletedCommand.Value)
            Else
                Return True
            End If

        End Function

        Dim _requestRegistered As Boolean
        ''' <summary>
        ''' Returns true if the service request event handler was registered.
        ''' </summary>
        Public ReadOnly Property ServiceRequestHandlerRegistered() As Boolean
            Get
                Return Me._requestRegistered
            End Get
        End Property

        ''' <summary>
        ''' enables the service request register as well as the request event handling.
        ''' </summary>
        ''' <param name="standardEventEnableBits">Specifies standard events will issue an SRQ.
        ''' </param>
        ''' <param name="serviceRequestEnableBits">Specifies which status registers will issue an SRQ.
        ''' </param>
        Public Sub RegisterServiceRequestHandler(ByVal standardEventEnableBits As isr.Visa.Ieee4882.StandardEvents,
                                             ByVal serviceRequestEnableBits As isr.Visa.Ieee4882.ServiceRequests)

            If Me._requestRegistered Then
                Me._requestRegistered = False
                ' remove the service request handler
                RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
            End If

            Me._requestRegistered = True

            ' register the handler after enabling the event to make sure things were enabled.
            AddHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest

            Try

#If DIAGNOSE Then
        Trace.CorrelationManager.StartLogicalOperation("REGISTER")
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} VISA session event handler mechanism", Me.ResourceName)
#End If

                ' this is required for raising the events.
                Me._session.EnableEvent(MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)
                ' Me._session.EnableEvent(MessageBasedSessionEventType.AllEnabledEvents, VisaNS.EventMechanism.Handler)

            Catch ex As Exception

                If Me._session IsNot Nothing Then
                    If Me._requestRegistered Then
                        Me._requestRegistered = False
                        ' remove the service request handler
                        RemoveHandler Me._session.ServiceRequest, AddressOf OnVisaServiceRequest
                    End If
                End If

#If DIAGNOSE Then
        Me.OnMessageAvailable(TraceEventType.Error,synopsis, "Exception occurred {0}. VISA status={2}. Details: {1}.", 
                                   Me._lastMessage, ex, Me.LastVisaStatus)
#Else
                Throw
#End If

            End Try

            Try

#If DIAGNOSE Then
        Trace.CorrelationManager.StartLogicalOperation("REGISTER")
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} standard events {1}", 
                                 Me.ResourceName, CInt(standardEventEnableBits))
#End If

                Me.WriteStandardEventEnable(standardEventEnableBits)

            Catch ex As Exception

#If DIAGNOSE Then
        Me.OnMessageAvailable(TraceEventType.Error,synopsis, "Exception occurred {0}. VISA status={2}. Details: {1}.", 
                                   Me._lastMessage, ex, Me.LastVisaStatus)
#Else
                Throw
#End If

            End Try


            If serviceRequestEnableBits <> Me.ReadServiceRequestEventEnable() Then

                Try

#If DIAGNOSE Then
          Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} service requests {1}", 
                                   Me.ResourceName, CInt(serviceRequestEnableBits))
#End If
                    ' TO_DO:  Check if also enables the SRQ.
                    Me.WriteServiceRequestEventEnable(serviceRequestEnableBits)

                Catch ex As Exception

#If DIAGNOSE Then
        Me.OnMessageAvailable(TraceEventType.Error,synopsis, "Exception occurred {0}. VISA status={2}. Details: {1}.", 
                                   Me._lastMessage, ex, Me.LastVisaStatus)
#Else
                    Throw
#End If

                End Try

            End If

#If DIAGNOSE Then
      Trace.CorrelationManager.StopLogicalOperation()
#End If

        End Sub

#End Region

#Region " STB "

        ''' <summary>Gets or sets the last service request event status (STB) data byte.</summary>
        Private _serviceRequestEventStatus As Nullable(Of isr.Visa.Ieee4882.ServiceRequests)

        ''' <summary>Reads or sets the service request event status (STB) data byte.
        ''' For instruments not supporting event status register (STB) this can be set as the
        ''' default value returned when reading STB.  STB can be set also if not using devices.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Property ServiceRequestEventStatus(ByVal access As ResourceAccessLevels) As isr.Visa.Ieee4882.ServiceRequests
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._serviceRequestStatusQueryCommand.IsSupported Then
                    Me._serviceRequestEventStatus = CType(Me.Getter(Me._serviceRequestStatusQueryCommand, Me._serviceRequestEventStatus, access).Value, isr.Visa.Ieee4882.ServiceRequests)
                End If
                Return Me._serviceRequestEventStatus.Value
            End Get
            <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
            Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
                If Not (Me.UsingDevices AndAlso Me._serviceRequestStatusQueryCommand.IsSupported) Then
                    Me._serviceRequestEventStatus = Value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Reads the event status register.
        ''' </summary>
        Public Function ReadServiceRequestEventStatus() As isr.Visa.Ieee4882.ServiceRequests
            Return Me.ServiceRequestEventStatus(ResourceAccessLevels.Device)
        End Function

#End Region

#Region " MEASUREMENT EVENTS "

        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Private _measurementStatusQueryCommand As SupportedCommand = New SupportedCommand(":STAT:MEAS:EVEN?")

        ''' <summary>Gets or sets the measurement status Query Command for this instrument.</summary>
        Public Property MeasurementStatusQueryCommand() As String
            Get
                Return Me._measurementStatusQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._measurementStatusQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>
        ''' Reads the measurement events register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadMeasurementEventStatus(ByVal access As ResourceAccessLevels) As Integer
            Dim value As Nullable(Of Integer)
            value = Me.Getter(Me._measurementStatusQueryCommand, value, access)
            If value.HasValue Then
                Return value.Value
            Else
                Return 0
            End If
        End Function

#End Region

#Region " OPERATION EVENTS "

        ''' <summary>Gets or sets the operation event enable command message for this instrument.
        ''' Is supported if the operation event enable command message is not empty.
        ''' </summary>
        Private _operationEventEnableCommand As SupportedCommand

        ''' <summary>Gets or sets the event enable (ESE) command message for this instrument.</summary>
        Public Property OperationEventEnableCommand() As String
            Get
                Return Me._operationEventEnableCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._operationEventEnableCommand = New SupportedCommand(Value)
                If Me._operationEventEnableCommand.IsSupported Then
                    Me.OperationEventEnableQueryCommand = Value & "?"
                Else
                    Me.OperationEventEnableQueryCommand = String.Empty
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the operation event enable query message for this instrument.</summary>
        Private _operationEventEnableQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the operation event enable Query Command for this instrument.</summary>
        Public Property OperationEventEnableQueryCommand() As String
            Get
                Return Me._operationEventEnableQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._operationEventEnableQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the operation event status register query message for this instrument.</summary>
        Private _operationEventStatusQueryCommand As SupportedCommand = New SupportedCommand(":STAT:OPER:EVEN?")

        ''' <summary>Gets or sets the event status register (ESR) Query Command for this instrument.</summary>
        Public Property OperationEventStatusQueryCommand() As String
            Get
                Return Me._operationEventStatusQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._operationEventStatusQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the last operation event request enable data byte.</summary>
        Private _operationEventEnable As Nullable(Of Integer)

        ''' <summary>Reads or write the operation event request enable data byte.
        ''' For instruments not supporting operation event status register this can be set as the
        ''' default value returned when reading operation status event. Operation status event can 
        ''' be set also if not using devices.
        ''' </summary>
        ''' <param name="access">Determines access to the device for read, write, and verify.</param>
        Public Overridable Property OperationEventEnable(ByVal access As ResourceAccessLevels) As Integer
            Get
                If Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._operationEventEnableQueryCommand.IsSupported Then
                    Dim value As Nullable(Of Int32) = Me.Getter(Me._operationEventEnableQueryCommand, Me._operationEventEnable, access)
                    If value.HasValue Then
                        Me._operationEventEnable = CType(value.Value, Integer)
                    Else
                        Me._operationEventEnable = &H10000 ' Bit 16 (zero based) = unknown
                    End If
                End If
                Return Me._operationEventEnable.Value
            End Get
            Set(ByVal Value As Integer)
                If Not My.MyLibrary.IsCacheAccess(access) AndAlso
                    (Me.UsingDevices AndAlso Me._session IsNot Nothing AndAlso Me._operationEventEnableCommand.IsSupported) Then
                    Dim eseValue As Nullable(Of Int32) = Me.Setter(Me._operationEventEnableCommand, Me._operationEventEnable, Value, access)
#If DIAGNOSE Then
          Dim synopsis As String = "Enable Operation Events"
          Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Enabling {0} operation events {1}", Me.ResourceName, CInt(Value))
#End If
                    If eseValue.HasValue Then
                        Me._operationEventEnable = CType(eseValue.Value, Integer)
                    Else
                        Me._operationEventEnable = &H10000 ' Bit 16 (zero based) = unknown
                    End If
                Else
                    ' set for emulation
                    Me._operationEventEnable = Value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Reads the event Enable register.
        ''' </summary>
        Public Function ReadOperationEventEnable() As Integer
            Return Me.OperationEventEnable(ResourceAccessLevels.Device)
        End Function

        ''' <summary>
        ''' Writes to the operation event enable register.
        ''' </summary>
        Public Sub WriteOperationEventEnable(ByVal value As Integer)
            Me.OperationEventEnable(ResourceAccessLevels.Device) = value
        End Sub

        ''' <summary>
        ''' Reads the operation event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadOperationEventStatus(ByVal access As ResourceAccessLevels) As Integer
            Dim value As Nullable(Of Integer)
            value = Me.Getter(Me._operationEventStatusQueryCommand, value, access)
            If value.HasValue Then
                Return value.Value
            Else
                Return 0
            End If
        End Function

#End Region

#Region " QUESTIONABLE EVENTS "

        ''' <summary>Gets or sets the service request enable (SRE) query message for this instrument.</summary>
        Private _questionableStatusQueryCommand As SupportedCommand = New SupportedCommand(":STAT:QUES:EVEN?")

        ''' <summary>Gets or sets the questionable status Query Command for this instrument.</summary>
        Public Property QuestionableStatusQueryCommand() As String
            Get
                Return Me._questionableStatusQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._questionableStatusQueryCommand = New SupportedCommand(Value)
            End Set
        End Property


        ''' <summary>
        ''' Reads the questionable event register status.
        ''' </summary>
        ''' <remarks>Override to get the event status.</remarks>
        Public Function ReadQuestionableEventStatus(ByVal access As ResourceAccessLevels) As Integer
            Dim value As Nullable(Of Integer)
            value = Me.Getter(Me._questionableStatusQueryCommand, value, access)
            If value.HasValue Then
                Return value.Value
            Else
                Return 0
            End If
        End Function

#End Region

#End Region

#Region " INTERFACE "

        Private _interface As isr.Visa.GpibInterface
        ''' <summary>Gets or sets reference to the GPIB interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As isr.Visa.GpibInterface
            Get
                If Me._interface Is Nothing AndAlso Me.UsingDevices Then
                    Me._interface = New isr.Visa.GpibInterface(Me.BoardNumber)
                    Me._interface.PrimaryAddress = Me._session.PrimaryAddress
                End If
                Return Me._interface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Overridable Sub ClearInterface()
            If Me.GpibInterface IsNot Nothing Then
                Me.GpibInterface.SendInterfaceClear()
            End If
        End Sub

        ''' <summary>Returns a string array of all local GPIB resources.</summary>
        Public Shared Function LocalResourceNames() As String()
            Return My.MyLibrary.LocalResourceNames(HardwareInterfaceType.Gpib)
        End Function

#End Region

#Region " FLUSH "

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _discardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

            Dim synopsis As String = "Discard unread data"

            Dim timedOut As Boolean = False
            Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                timedOut = False
                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                ' allow message available time to materialize
                Do Until Me.IsMessageAvailable OrElse timedOut
                    timedOut = DateTime.Now > endTime
                    Threading.Thread.Sleep(pollDelay)
                Loop

                If Not timedOut Then

                    If reportUnreadData Then

                        Dim flushed As String = Me.ReadLine()
                        If Not String.IsNullOrWhiteSpace(flushed) Then
                            Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Flushed '{0}'", My.MyLibrary.InsertCommonEscapeSequences(flushed))
                        End If

                    Else
                        Me.ReadLine()
                        'Me.Session.DiscardUnreadData()
                    End If
                End If

            Loop

            Return Me.IsLastVisaOperationSuccess

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Overridable Function DiscardUnreadData(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.DiscardUnreadData

            Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

        End Function

        ''' <summary>
        ''' Flushes the read buffers.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
        ''' message available construct.</param>
        Public Overridable Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean Implements IInstrument.FlushRead

            Dim synopsis As String = "Flush Read"
            Try

                Return Me._discardUnreadData(pollDelay, timeout, reportUnreadData)

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred flushing read buffer. VISA status={1}. Details: {2}.",
                                      Me.LastVisaStatus, ex)

            End Try

        End Function

        ''' <summary>
        ''' Flush the write buffers.
        ''' </summary>
        Private Sub _flushWrite()

            Dim synopsis As String = "Flush Write"
            Try

                ' flush read buffer.
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Flushing write buffers")
                Me.Session.FlushWrite()

                ' Me.WriteLine("")

                If Not Me.IsLastVisaOperationSuccess Then
                    Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Failed flush write. VISA status={0}{1}{2}",
                                          Me.LastVisaStatus, Environment.NewLine,
                                          isr.Core.StackTraceParser.UserCallStack(4, 10))
                End If

            Catch ex As VisaNS.VisaException

                Me.OnMessageAvailable(TraceEventType.Error, synopsis, "Exception occurred flushing write buffer. VISA status={0}. Details: {1}.",
                                      Me.LastVisaStatus, ex)

            Finally

            End Try

        End Sub

        ''' <summary>
        ''' Flush the write buffers.
        ''' </summary>
        Public Overridable Sub FlushWrite()
            Me._flushWrite()
        End Sub

#End Region

#Region " QUERY "

        ''' <summary>
        ''' Queries the GPIB instrument and returns a Boolean value.
        ''' </summary>
        ''' <param name="question">The query to use.</param>
        ''' <returns>Nullable{System.Boolean}.</returns>
        ''' <exception cref="System.ArgumentNullException">question</exception>
        ''' <exception cref="DeviceException">Instrument '{0}' failed reading or parsing Boolean query '{1}'. Instrument errors: {2}{3}</exception>
        Public Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)
            If String.IsNullOrWhiteSpace(question) Then
                Throw New ArgumentNullException("question")
            End If
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Boolean) = Me._session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Boolean") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Boolean query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName, question, Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Boolean query '{0}'", question) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Boolean query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName, question, Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>
        ''' Sends a query command and returns the Boolean outcome.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>Nullable{System.Boolean}.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        ''' <exception cref="DeviceException">Instrument '{0}' failed reading or parsing Boolean query '{1}'. Instrument errors: {2}{3}</exception>
        Public Function QueryBoolean(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Boolean)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Boolean) = Me._session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Boolean") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Boolean query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Boolean query: " & format, args) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Boolean query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>Queries the GPIB instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Nullable(Of Double)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Double) = Me._session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query double") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Double query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      question,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending double query '{0}'", question) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Double query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  question,
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns a double value.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>Nullable{System.Double}.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        ''' <exception cref="DeviceException">Instrument '{0}' failed reading or parsing Double query '{1}'. Instrument errors: {2}{3}</exception>
        Public Function QueryDouble(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Double)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Double) = Me._session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query double") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Double query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending double query: " & format, args) Then
                    Throw New DeviceException("Instrument '{0}' failed sending parsing Double query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>Queries the GPIB instrument and returns a int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Nullable(Of Int32)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Int32) = Me._session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Int32") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Integer query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      question,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Int32 query '{0}'", question) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Integer query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  question,
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns a int32 value.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>Nullable{Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        ''' <exception cref="DeviceException">Instrument '{0}' failed reading or parsing Integer query '{1}'. Instrument errors: {2}{3}</exception>
        Public Function QueryInt32(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Int32)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Int32) = Me._session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Int32") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Integer query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Int32 query: " & format, args) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Integer query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>Queries the GPIB instrument and returns a int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInfInt32(ByVal question As String) As Nullable(Of Int32)
            If Me.WriteQueryLine(question) Then
                Dim value As Nullable(Of Int32) = Me._session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Inf-Int32") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Inf-Integer query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      question,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Inf-Int32 query '{0}'", question) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Inf-Integer query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  question,
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns a int32 value.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>Nullable{Int32}.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        ''' <exception cref="DeviceException">Instrument '{0}' failed reading or parsing Inf-Integer query '{1}'. Instrument errors: {2}{3}</exception>
        Public Function QueryInfInt32(ByVal format As String, ByVal ParamArray args() As Object) As Nullable(Of Int32)
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Dim value As Nullable(Of Int32) = Me._session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing query Inf-Int32") Then
                        Throw New DeviceException("Instrument '{0}' failed reading or parsing Inf-Integer query '{1}'. Instrument errors: {2}{3}",
                                                      Me.ResourceName,
                                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
                Return value
            Else
                If Me.RaiseVisaOrDeviceException("failed sending Inf-Int32 query: " & format, args) Then
                    Throw New DeviceException("Instrument '{0}' failed sending Inf-Integer query '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
        End Function

        ''' <summary>Queries the GPIB instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String Implements IInstrument.QueryTrimEnd
            If Me.WriteQueryLine(question) Then
                Return Me.ReadLineTrimEnd()
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns a string save the termination character.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IInstrument.QueryTrimEnd
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Return Me.ReadLineTrimEnd()
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>
        ''' Queries the GPIB instrument and returns a string save the termination character.
        ''' </summary>
        ''' <param name="format">Specifies the format statement.</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        ''' <returns>System.String.</returns>
        ''' <exception cref="System.ArgumentNullException">format</exception>
        Public Function QueryLine(ByVal format As String, ByVal ParamArray args() As Object) As String
            If String.IsNullOrWhiteSpace(format) Then
                Throw New ArgumentNullException("format")
            End If
            If Me.WriteQueryLine(format, args) Then
                Return Me.ReadLine()
            Else
                Return String.Empty
            End If
        End Function

#End Region

#Region " READ "

        ''' <summary>Reads the GPIB instrument and returns a Boolean value.</summary>
        Public Function ReadBoolean() As Nullable(Of Boolean)
            Dim value As Nullable(Of Boolean) = True
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                value = Me._session.ReadBoolean()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Boolean") Then
                        Throw New DeviceException("Instrument '{0}' reading or parsing Boolean. Instrument errors: {1}{2}",
                                                      Me.ResourceName,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
            Return value
        End Function

        ''' <summary>Reads the GPIB instrument and returns a double value.</summary>
        Public Function ReadDouble() As Nullable(Of Double)
            Dim value As Nullable(Of Double) = 0
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                value = Me._session.ReadDouble()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Double") Then
                        Throw New DeviceException("Instrument '{0}' reading or parsing Double. Instrument errors: {1}{2}",
                                                      Me.ResourceName,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
            Return value
        End Function

        ''' <summary>Reads the GPIB instrument and returns an Int32 value.</summary>
        Public Function ReadInt32() As Nullable(Of Int32)
            Dim value As Nullable(Of Int32) = 0
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                value = Me._session.ReadInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Int32") Then
                        Throw New DeviceException("Instrument '{0}' reading or parsing Integer. Instrument errors: {1}{2}",
                                                      Me.ResourceName,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
            Return value
        End Function

        ''' <summary>Reads the GPIB instrument and returns an Int32 value.</summary>
        Public Function ReadInfInt32() As Nullable(Of Int32)
            Dim value As Nullable(Of Int32) = 0
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                value = Me._session.ReadInfInt32()
                If Not value.HasValue Then
                    If Me.RaiseVisaOrDeviceException("failed reading or parsing Inf-Int32") Then
                        Throw New DeviceException("Instrument '{0}' reading or parsing Inf-Integer. Instrument errors: {1}{2}",
                                                      Me.ResourceName,
                                                      Environment.NewLine, Me.DeviceErrors)
                    End If
                End If
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
            Return value
        End Function

        ''' <summary>Reads and returns a string to the termination character and return the string
        ''' including the termination character.</summary>
        Public Function ReadLine() As String
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                ReadLine = Me._session.ReadLine()
            Else
                ReadLine = String.Empty
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
        End Function

        ''' <summary>Reads and returns a string to the termination character and return the string
        ''' not including the termination character.</summary>
        Public Function ReadLineTrimEnd() As String
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                ReadLineTrimEnd = Me._session.ReadLineTrimEnd()
            Else
                ReadLineTrimEnd = String.Empty
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandReceived()
            End If
        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until data is no longer available.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        Public Function ReadLines(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal trimEnd As Boolean) As String

            Return ReadLines(pollDelay, timeout, False, trimEnd)

        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until data is no longer available
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="trimSpaces">Specifies a directive to trim leading and trailing spaces from each line.
        ''' This also trims the end character.</param>
        ''' <param name="trimEnd">Specifies a directive to trim the end character from each line.</param>
        Public Function ReadLines(ByVal pollDelay As Integer, ByVal timeout As Integer,
                          ByVal trimSpaces As Boolean, ByVal trimEnd As Boolean) As String

            Try

                If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder

                Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)
                Dim timedOut As Boolean = False
                Do While (Not timedOut) AndAlso Me.IsLastVisaOperationSuccess

                    ' allow message available time to materialize
                    Dim hasData As Boolean = Me.IsMessageAvailable
                    Do Until hasData OrElse timedOut
                        timedOut = DateTime.Now > endTime
                        Dim t1 As DateTime = DateTime.Now.AddMilliseconds(pollDelay)
                        Do Until DateTime.Now > t1
                            Windows.Forms.Application.DoEvents()
                            Threading.Thread.Sleep(2)
                            Windows.Forms.Application.DoEvents()
                        Loop
                        hasData = Me.IsMessageAvailable
                    Loop

                    If hasData Then
                        timedOut = False
                        endTime = DateTime.Now.AddMilliseconds(timeout)
                        If trimSpaces Then
                            listBuilder.AppendLine(Me._session.ReadLine().Trim())
                        ElseIf trimEnd Then
                            listBuilder.AppendLine(Me._session.ReadLineTrimEnd())
                        Else
                            listBuilder.AppendLine(Me._session.ReadLine())
                        End If
                    End If

                Loop

                Return listBuilder.ToString

            Catch
                Throw
            Finally
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Reads multiple lines from the instrument until timeout.
        ''' </summary>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        ''' <param name="trimSpaces">Specifies a directive to trim leading and trailing spaces from each line</param>
        ''' <param name="expectedLength">Specifies the amount of data expected without trimming.</param>
        Public Function ReadLines(ByVal timeout As Integer, ByVal trimSpaces As Boolean, ByVal expectedLength As Integer) As String

            Try

                If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder

                Me.StoreTimeout(timeout)

                Dim timedOut As Boolean = False
                Dim currentLength As Integer = 0
                Do While currentLength < expectedLength AndAlso Me.IsLastVisaOperationSuccess AndAlso Not timedOut

                    Try

                        Me._session.ReadLine()
                        expectedLength += Me._session.ReceiveBuffer.Length
                        If trimSpaces Then
                            listBuilder.AppendLine(Me._session.ReceiveBuffer.Trim)
                        Else
                            listBuilder.AppendLine(Me._session.ReceiveBuffer)
                        End If

                    Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaStatusCode.ErrorTimeout

                        timedOut = True

                    End Try

                Loop

                Return listBuilder.ToString

            Catch

                Throw

            Finally

                Me.RestoreTimeout()

                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
            End Try

        End Function

        ''' <summary>
        ''' Reads a potentially long string from the instrument until reading an end of line.
        ''' </summary>
        ''' <param name="pollDelay">Time in milliseconds to wait between service requests.</param>
        ''' <param name="timeout">Specifies the time in millisecond to wait for message available</param>
        Public Function ReadString(ByVal pollDelay As Integer, ByVal timeout As Integer) As String

            Try

                If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                    Return ""
                End If

                Dim listBuilder As New System.Text.StringBuilder
                Dim endDetected As Boolean = False
                Dim timedOut As Boolean = False
                Do Until endDetected OrElse timedOut OrElse Me.IsLastVisaOperationFailed

                    timedOut = False
                    Dim endTime As Date = DateTime.Now.AddMilliseconds(timeout)

                    ' allow message available time to materialize
                    Do Until Me.IsMessageAvailable OrElse timedOut
                        timedOut = DateTime.Now > endTime
                        Threading.Thread.Sleep(pollDelay)
                    Loop

                    If Not timedOut Then
                        Dim value As String = Me._session.Reader.ReadString()
                        listBuilder.Append(value)
                        endDetected = value.EndsWith(Convert.ToChar(Me._session.Reader.TerminationCharacter), StringComparison.OrdinalIgnoreCase)
                    End If

                Loop

                Return listBuilder.ToString

            Catch
                Throw
            Finally
                If Not Me.IsLastVisaOperationFailed Then
                    OnCommandReceived()
                End If
            End Try
        End Function


#End Region

#Region " INSTRUMENT ERROR MANAGEMENT "

        ''' <summary>
        ''' Reports if a visa or device error occurred as set the <see cref="LastOperationOkay">success condition</see>
        ''' Can only be used after receiving a full reply from the instrument.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if success</returns>
        Public Function ReportVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal synopsis As String,
                                              ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "VISA errors occurred {0}. VISA '{1}' error details: {2}{3}{4}",
                                                     String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                     Me.ResourceName,
                                                     Me.LastVisaStatusDetails, Environment.NewLine,
                                                     isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            ElseIf Me.HandleInstrumentErrorIfError(flushReadFirst) Then

                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Instrument encountered errors {0}. Instrument '{1}' error details: {2}{3}{4}",
                                                     String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                     Me.ResourceName,
                                                     Me.DeviceErrors, Environment.NewLine,
                                                     isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Reports if a visa error occurred as set the <see cref="LastOperationOkay">success condition</see>
        ''' Can be used with queries.
        ''' </summary>
        ''' <returns>True if success</returns>
        Public Function ReportVisaOperationOkay(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "{0}. VISA '{1}' error details: {2}{3}{4}",
                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                      Me.ResourceName,
                                      Me.LastVisaStatusDetails, Environment.NewLine,
                                      isr.Core.StackTraceParser.UserCallStack(4, 10))
                Me.LastOperationOkay = False
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Handles instrument error.  For compatibility
        ''' Clears the error queue and status by reading the error queue.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if error occurred.</returns>
        Public Overridable Function HandleInstrumentError(ByVal flushReadFirst As Boolean) As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False

            If Me._isDelegateErrorHandling Then

                Me._lastServiceEventArgs.HasError = True
                Me.OnHandleError()

            Else

                If flushReadFirst Then
                    Me.FlushRead(10, 200, False)
                End If

                '  In order to keep compatibility, we clear the error here by reading the error status.
                Me.FetchErrorQueue()

                ' read the standard event register
                Me.ReadStandardEventStatus()

                If Not Me._lastServiceEventArgs.HasError Then

                    Dim errorBits As isr.Visa.Ieee4882.StandardEvents = isr.Visa.Ieee4882.StandardEvents.CommandError Or
                                                                        isr.Visa.Ieee4882.StandardEvents.DeviceDependentError Or
                                                                        isr.Visa.Ieee4882.StandardEvents.ExecutionError Or
                                                                        isr.Visa.Ieee4882.StandardEvents.QueryError
                    Me._lastServiceEventArgs.HasError = (Me._standardEventStatus.Value And errorBits) <> 0

                End If

            End If

            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Check the status byte for error bits and caches the
        ''' <see cref="ServiceEventArgs.HasError">last service request error sentinel.</see>
        ''' </summary>
        Public Overridable Function IsErrorAvailable() As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False
            Me._lastServiceEventArgs.HasError = (Me.ReadDeviceStatusByte And Me._errorAvailableBits) <> 0
            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Checks the status register for an error.  For compatibility
        ''' Clears the error status by reading the error queue.
        ''' </summary>
        ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
        ''' <returns>True if error occurred.</returns>
        Public Overridable Function HandleInstrumentErrorIfError(ByVal flushReadFirst As Boolean) As Boolean

            ' clear the cached error
            Me._lastServiceEventArgs.HasError = False

            If (Me.ReadDeviceStatusByte And Me._errorAvailableBits) <> 0 Then

                Me._lastServiceEventArgs.HasError = Me.HandleInstrumentError(flushReadFirst)

            End If

            Return Me._lastServiceEventArgs.HasError

        End Function

        ''' <summary>
        ''' Throws a <see cref="VisaException">VISA exception</see>
        ''' Sets the <see cref="LastOperationOkay">success condition</see>
        ''' Can be used with queries.
        ''' </summary>
        ''' <returns>True if success</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")>
        Public Function RaiseVisaException(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Me.LastOperationOkay = False
                Throw New VisaException("Visa '{0}' '{1}'. Visa errors: {2}{3}",
                                        Me.ResourceName,
                                        String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                        Environment.NewLine, Me.DeviceErrors)
            End If
            Return Me.LastOperationOkay
        End Function

        ''' <summary>
        ''' Throws an <see cref="DeviceException">instrument exceptions</see>.
        ''' Set the <see cref="LastOperationOkay">success condition</see>
        ''' Flushes the read buffer if an error occurred.
        ''' </summary>
        ''' <returns>True if success</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")>
        Public Function RaiseVisaOrDeviceException(ByVal format As String, ByVal ParamArray args() As Object) As Boolean

            Me.LastOperationOkay = True
            If Me.IsLastVisaOperationFailed Then
                Return Me.RaiseVisaException(format, args)
            ElseIf Me.IsErrorAvailable Then
                ' if an error occurred, flush the read buffers to make sure we can get the error and report it.
                ' Me.FlushRead(2, 200, False)
                If Me.HandleInstrumentError(True) Then
                    Me.LastOperationOkay = False
                    Throw New DeviceException("Instrument '{0}' '{1}'. Instrument errors: {2}{3}",
                                                  Me.ResourceName,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                  Environment.NewLine, Me.DeviceErrors)
                End If
            End If
            Return Me.LastOperationOkay
        End Function

#End Region

#Region " READ EXTENDED "

        ''' <summary>Gets or sets the condition to control
        ''' trimming the terminator on read.</summary>
        Private _isTrimTerminator As Boolean

        ''' <summary>Gets or sets the condition to control
        ''' trimming the terminator on read.</summary>
        Public Property IsTrimTerminator() As Boolean
            Get
                Return Me._isTrimTerminator
            End Get
            Set(ByVal Value As Boolean)
                Me._isTrimTerminator = Value
            End Set
        End Property

        ''' <summary>Gets or sets the condition to control
        ''' the checking of message available on read.</summary>
        Private _isCheckBufferOnReceive As Boolean

        ''' <summary>Gets or sets the condition to control
        ''' the checking of message available on read.</summary>
        Public Property IsCheckBufferOnReceive() As Boolean
            Get
                Return Me._isCheckBufferOnReceive
            End Get
            Set(ByVal Value As Boolean)
                Me._isCheckBufferOnReceive = Value
            End Set
        End Property

        ''' <summary>
        ''' Reads a string from the instrument.
        ''' Checks for errors.
        ''' </summary>
        ''' <returns>Returns received string.
        ''' </returns>
        Public Function ReadStringExtended() As String

            If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                Return String.Empty
            End If

            Dim hasDataToRead As Boolean
            Dim errorMessage As New System.Text.StringBuilder
            Dim errorReport As String

            ' check if the instrument has reported an error.
            If Me.HandleInstrumentErrorIfError(False) Then

                ' now report the error to the calling module
                errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                             "VISA resource {0} had an error before receiving", Me.ResourceName))
                errorReport = Me.DeviceErrors
                If Not String.IsNullOrWhiteSpace(errorReport) Then
                    errorMessage.Append(":")
                    errorMessage.Append(Environment.NewLine)
                    errorMessage.Append(errorReport)
                End If
                errorMessage.Append(".")
                Throw New ReadException(errorMessage.ToString)

            Else ' if the instrument did not report an error

                ' check if we have data to get
                If Me.IsCheckBufferOnReceive Then
                    hasDataToRead = Me.IsMessageAvailable
                Else
                    ' instruments such as the CVA do report message available.
                    hasDataToRead = True
                End If

                If hasDataToRead Then

                    ' if we have data in the queue then
                    ' get the data from the queue
                    If Me.IsTrimTerminator Then
                        ReadStringExtended = Me.ReadLineTrimEnd()
                    Else
                        ReadStringExtended = Me.ReadLine()
                    End If

                    ' check again if the instrument reported an error.
                    If Me.HandleInstrumentErrorIfError(False) Then

                        ' now report the error to the calling module
                        errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Failed reading from VISA resource {0}", Me.ResourceName))

                        errorReport = Me.DeviceErrors
                        If Not String.IsNullOrWhiteSpace(errorReport) Then
                            errorMessage.Append(":")
                            errorMessage.Append(Environment.NewLine)
                            errorMessage.Append(errorReport)
                        End If
                        errorMessage.Append(".")
                        Throw New ReadException(errorMessage.ToString)

                    End If

                Else

                    ' raise the error to the calling module.
                    errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                      "Failed receiving from VISA resource {0} at primary address {1} because there was nothing to read.",
                                                      Me.ResourceName, Me.PrimaryAddress))
                    Throw New ReadException(errorMessage.ToString)

                End If

            End If

        End Function

#End Region

#Region " RESOURCE MANAGEMENT "

        ''' <summary>
        ''' Returns true if having a valid primary address.
        ''' </summary>
        ''' <param name="address">The device primary address</param>
        Public Shared Function IsValidPrimaryAddress(ByVal address As Integer) As Boolean
            Return (address >= 1) AndAlso (address <= 31)
        End Function

        ''' <summary>Gets or sets the primary address of the instrument on the GPIB bus.</summary>
        Public ReadOnly Property PrimaryAddress() As Int32
            Get
                If Me._session Is Nothing Then
                    Return 0
                Else
                    Return Me._session.PrimaryAddress
                End If
            End Get
        End Property

        ''' <summary>Gets the board number.</summary>
        Public ReadOnly Property BoardNumber() As Integer
            Get
                If Me._session Is Nothing Then
                    Return 0
                Else
                    Return Me._session.BoardNumber
                End If
            End Get
        End Property

#End Region

#Region " SERVICE REQUEST HANDLERS "

        Private _lastServiceEventArgs As ServiceEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="isr.Visa.Ieee4882.ServiceEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As ServiceEventArgs
            Get
                Return Me._lastServiceEventArgs
            End Get
            Set(ByVal value As ServiceEventArgs)
                Me._lastServiceEventArgs = value
            End Set
        End Property

        ''' <summary>Raised upon receiving a service request.  This event does not expose the
        '''   internal service request arguments directly.</summary>
        Public Event ServiceRequest As EventHandler(Of ServiceEventArgs)

        ''' <summary>Raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnServiceRequest(ByVal e As ServiceEventArgs)

            If e Is Nothing Then
            ElseIf e.HasError Then

                Me.OnMessageAvailable(TraceEventType.Verbose, "INSTRUMENT ERROR AVAILABLE",
                                      "Instrument reported an error on service request:{0}{1}", Environment.NewLine, e.LastError)

            Else

                Dim elapsedMilliseconds As Double = e.OperationElapsedTime.TotalMilliseconds()
                Me.OnMessageAvailable(TraceEventType.Verbose, "SERVICE REQUEST EVENT", "Operation completed in {0}ms", elapsedMilliseconds)

            End If

            Dim evt As EventHandler(Of ServiceEventArgs) = Me.ServiceRequestEvent
            If evt IsNot Nothing Then evt.SafeInvoke(Me, e)
            If e IsNot Nothing Then
                e.ServicingRequest = False
            End If

        End Sub

        ''' <summary>Handles service requests from the instrument.</summary>
        Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

            If Not Me._lastServiceEventArgs.ServicingRequest Then
                ' do not initialize the last service event arguments because this is done with Clear Status.
                Me._lastServiceEventArgs.ServicingRequest = True
                Me._lastServiceEventArgs.ProcessServiceRequest()
                Me._lastServiceEventArgs.ReadRegisters()
                If Me._lastServiceEventArgs.HasError Then
                    ' disable recurring service requests
                    'Me.ServiceRequestEventEnable = ServiceRequests.None
                Else
                    ' check if we have consecutive requests.
                    If Me._lastServiceEventArgs.RequestCount > 10 Then
                        ' disable recurring service requests
                        Me.ServiceRequestEventEnable(ResourceAccessLevels.Device) = Ieee4882.ServiceRequests.None
                        Me.OnMessageAvailable(TraceEventType.Verbose, "SERVICE REQUEST QUEUE HIT ITS LIMIT", "Instrument had over 10 consecutive service requests")
                    End If
                End If
                OnServiceRequest(Me._lastServiceEventArgs)
            End If

        End Sub

#End Region

#Region " SESSION MANAGEMENT "

        ''' <summary>Restores the last timeout from the stack.
        ''' </summary>
        Public Sub RestoreTimeout()
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Me._session.RestoreTimeout()
#If DIAGNOSE Then
        Dim synopsis As String = "Restore Timeout"
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Restore Timeout", "Restored timeout to {0}", Me._session.Timeout)
#End If
            End If
        End Sub

        Private _session As isr.Visa.GpibSession
        ''' <summary>Gets or sets reference to the GPIB session.</summary>
        Public ReadOnly Property Session() As isr.Visa.GpibSession
            Get
                Return Me._session
            End Get
        End Property

        ''' <summary>Saves the current timeout and sets a new setting timeout.
        ''' </summary>
        ''' <param name="timeout">Specifies the new timeout</param>
        Public Sub StoreTimeout(ByVal timeout As Integer)
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
#If DIAGNOSE Then
        Dim synopsis As String = "Store Timeout"
        Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "Store Timeout", "Setting timeout to {0}", timeout)
#End If
                Me._session.StoreTimeout(timeout)
            End If
        End Sub

        ''' <summary>
        ''' Gets the device timeout in milliseconds.
        ''' Use store and restore timeout to change timeouts.
        ''' </summary>
        Public ReadOnly Property Timeout() As Integer
            Get
                Return Me._session.Timeout
            End Get
        End Property

        ''' <summary>Gets or sets the condition telling if the instrument
        ''' timed out on waiting for SRQ.</summary>
        Private _timedOut As Boolean

        ''' <summary>Gets or sets the condition for any operation timed out.
        ''' </summary>
        Public ReadOnly Property TimedOut() As Boolean
            Get
                Return Me._timedOut
            End Get
        End Property

#End Region

#Region " SUPPORTED COMMANDS "

        ''' <summary>Gets or sets the CLS command message for this instrument.
        ''' Is supported if the CLS command message is not empty.
        ''' </summary>
        Private _clearExecutionStateCommand As SupportedCommand

        ''' <summary>Gets or sets the CLS command message for this instrument.</summary>
        Public Property ClearExecutionStateCommand() As String
            Get
                Return Me._clearExecutionStateCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._clearExecutionStateCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the error queue clear message for this instrument.
        ''' </summary>
        Private _clearErrorQueueCommand As SupportedCommand

        ''' <summary>Gets or sets the Error Query clear Command for this instrument.</summary>
        Public Property ClearErrorQueueCommand() As String
            Get
                Return Me._clearErrorQueueCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._clearErrorQueueCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the error queue Count query message for this instrument.</summary>
        Private _errorQueueCountQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the Error Count Query Command for this instrument.</summary>
        Public Property ErrorQueueCountQueryCommand() As String
            Get
                Return Me._errorQueueCountQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._errorQueueCountQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the error queue query message for this instrument.</summary>
        Private _errorQueueQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the Error Query Command for this instrument.</summary>
        Public Property ErrorQueueQueryCommand() As String
            Get
                Return Me._errorQueueQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._errorQueueQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the event enable (ESE) command message for this instrument.
        ''' Is supported if the event enable (ESE) command message is not empty.
        ''' </summary>
        Private _standardEventEnableCommand As SupportedCommand

        ''' <summary>Gets or sets the event enable (ESE) command message for this instrument.</summary>
        Public Property StandardEventEnableCommand() As String
            Get
                Return Me._standardEventEnableCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._standardEventEnableCommand = New SupportedCommand(Value)
                If Me._standardEventEnableCommand.IsSupported Then
                    Me.StandardEventEnableQueryCommand = Value & "?"
                Else
                    Me.StandardEventEnableQueryCommand = String.Empty
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the event enable (ESE) query message for this instrument.</summary>
        Private _standardEventEnableQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the event enable (ESE) Query Command for this instrument.</summary>
        Public Property StandardEventEnableQueryCommand() As String
            Get
                Return Me._standardEventEnableQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._standardEventEnableQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the event status register (ESR) query message for this instrument.</summary>
        Private _standardEventStatusQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the event status register (ESR) Query Command for this instrument.</summary>
        Public Property StandardEventStatusQueryCommand() As String
            Get
                Return Me._standardEventStatusQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._standardEventStatusQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        Private _identifyQueryCommand As SupportedCommand
        ''' <summary>Gets or sets the IDN command message for this instrument.</summary>
        Public Property IdentifyQueryCommand() As String
            Get
                Return Me._identifyQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._identifyQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the OPC command message for this instrument.</summary>
        Private _operationCompletedCommand As SupportedCommand

        ''' <summary>Gets or sets the OPC command message for this instrument.</summary>
        Public Property OperationCompletedCommand() As String
            Get
                Return Me._operationCompletedCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._operationCompletedCommand = New SupportedCommand(Value)
                If Me._operationCompletedCommand.IsSupported Then
                    Me.OperationCompletedQueryCommand = Value & "?"
                Else
                    Me.OperationCompletedQueryCommand = String.Empty
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the OPC Query Command for this instrument.</summary>
        Private _operationCompletedQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the OPC Query Command for this instrument.</summary>
        Public Property OperationCompletedQueryCommand() As String
            Get
                Return Me._operationCompletedQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._operationCompletedQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the RTS command for this instrument.</summary>
        Private _resetKnownStateCommand As SupportedCommand

        ''' <summary>Gets or sets the RTS command for this instrument.</summary>
        Public Property ResetKnownStateCommand() As String
            Get
                Return Me._resetKnownStateCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._resetKnownStateCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the status byte (STB) query message for this instrument.</summary>
        Private _serviceRequestStatusQueryCommand As SupportedCommand

        ''' <summary>Gets or sets the status byte (STB) Query Command for this instrument.</summary>
        Public Property ServiceRequestStatusQueryCommand() As String
            Get
                Return Me._serviceRequestStatusQueryCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._serviceRequestStatusQueryCommand = New SupportedCommand(Value)
            End Set
        End Property

        ''' <summary>Gets or sets the WAI command message for this instrument.</summary>
        Private _waitCommand As SupportedCommand

        ''' <summary>Gets or sets the WAI command message for this instrument.</summary>
        Public Property WaitCommand() As String
            Get
                Return Me._waitCommand.Value
            End Get
            Set(ByVal Value As String)
                Me._waitCommand = New SupportedCommand(Value)
            End Set
        End Property

#End Region

#Region " VISA STATUS "

        ''' <summary>
        ''' Returns true if the last VISA operation ended successfully.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
            Get
                Return Me.LastVisaStatus = VisaStatusCode.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.IsLastVisaOperationFailed
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the last VISA operation ended with a warning.
        ''' </summary>
        Public ReadOnly Property IsLastVisaOperationWarning() As Boolean
            Get
                If Me._session Is Nothing Then
                    Return False
                Else
                    Return Me._session.IsLastVisaOperationWarning
                End If
            End Get
        End Property

        ''' <summary>
        ''' Gets the last VISA Status.
        ''' </summary>
        Public ReadOnly Property LastVisaStatus() As Integer Implements IInstrument.LastVisaStatus ' NationalInstruments.VisaNS.VisaStatusCode Implements IInstrument.LastVisaStatus
            Get
                If Me._session Is Nothing Then
                    Return VisaStatusCode.Success
                Else
                    Return Me._session.LastStatus
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns the last VISA message status if any.
        ''' </summary>
        Public ReadOnly Property LastVisaStatusDetails() As String Implements IInstrument.LastVisaStatusDetails
            Get
                Return Me._session.BuildLastVisaStatusDetails()
            End Get
        End Property

#End Region

#Region " WRITE "

        ''' <summary>Sends a command line to the instrument.</summary>
        ''' <param name="format">Specifies the format statement</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Overridable Function WriteLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Return Me.WriteLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        End Function

        ''' <summary>Sends a command line to the instrument.</summary>
        ''' <param name="value">The command.</param>
        Public Overridable Function WriteLine(ByVal value As String) As Boolean
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Me._session.WriteLine(value)
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandSent()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Writes On or Off command to the instrument.
        ''' </summary>
        ''' <param name="queryCommand">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Overridable Function WriteOnOff(ByVal queryCommand As String, ByVal isOn As Boolean) As Boolean

            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            queryCommand = Ieee4882.Syntax.BuildBooleanCommand(queryCommand, isOn, BooleanDataFormat.OnOff)
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Me.WriteLine(queryCommand)
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandSent()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>
        ''' Writes On or Off command to the instrument.
        ''' </summary>
        ''' <param name="queryCommand">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        Public Overridable Function WriteOneZero(ByVal queryCommand As String, ByVal one As Boolean) As Boolean

            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            queryCommand = Ieee4882.Syntax.BuildBooleanCommand(queryCommand, one, BooleanDataFormat.OneZero)
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Me.WriteLine(queryCommand)
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandSent()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Sends a query command line to the instrument.
        ''' A returned value is expected from the instrument after issuing this command.</summary>
        ''' <param name="value">The command.</param>
        Public Overridable Function WriteQueryLine(ByVal value As String) As Boolean
            If Me.UsingDevices AndAlso Me._session IsNot Nothing Then
                Me._session.WriteLine(value)
            End If
            If Not Me.IsLastVisaOperationFailed Then
                OnCommandSent()
            End If
            Return Me.IsLastVisaOperationSuccess
        End Function

        ''' <summary>Sends a query command line to the instrument.
        ''' A returned value is expected from the instrument after issuing this command.</summary>
        ''' <param name="format">Specifies the format statement</param>
        ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
        Public Overridable Function WriteQueryLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Return Me.WriteQueryLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        End Function

#End Region

#Region " WRITE EXTENDED "

        ''' <summary>Gets or sets the condition to control
        ''' the checking output buffer before sending.</summary>
        Private _isCheckBufferOnSend As Boolean

        ''' <summary>Gets or sets the condition to control
        ''' the checking output buffer before sending.</summary>
        Public Property IsCheckBufferOnSend() As Boolean
            Get
                Return Me._isCheckBufferOnSend
            End Get
            Set(ByVal Value As Boolean)
                Me._isCheckBufferOnSend = Value
            End Set
        End Property

        ''' <summary>Gets or sets the condition to control
        ''' OPC on send.</summary>
        Private _isCheckOpcAfterSend As Boolean

        ''' <summary>Gets or sets the condition to control
        ''' OPC on send.</summary>
        Public Property IsCheckOpcAfterSend() As Boolean
            Get
                Return Me._isCheckOpcAfterSend
            End Get
            Set(ByVal Value As Boolean)
                Me._isCheckOpcAfterSend = Value
            End Set
        End Property

        ''' <summary>
        ''' Sends a command to the instrument.
        ''' Reports errors if the instrument is not ready to receive a command.
        ''' </summary>
        ''' <param name="queryCommand">Specifies the data to send.</param>
        ''' <exception cref="System.ArgumentNullException">queryCommand</exception>
        ''' <exception cref="WriteException"></exception>
        Public Sub WriteExtended(ByVal queryCommand As String)

            If queryCommand Is Nothing Then
                Throw New ArgumentNullException("queryCommand")
            End If
            If Not Me.UsingDevices OrElse Me._session Is Nothing Then
                Return
            End If

            Dim hasDataToRead As Boolean
            If String.IsNullOrWhiteSpace(queryCommand) Then
                ' if no data to send, exit
                Return
            End If

            Dim errorMessage As New System.Text.StringBuilder
            Dim errorReport As String

            ' check if the instrument reported an error
            If Me.HandleInstrumentErrorIfError(False) Then

                ' if the instrument reported an error then
                ' now report the error to the calling module
                errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                             "VISA resource {0} had an error before sending", Me.ResourceName))
                errorReport = Me.DeviceErrors
                If Not String.IsNullOrWhiteSpace(errorReport) Then
                    errorMessage.Append(":")
                    errorMessage.Append(Environment.NewLine)
                    errorMessage.Append(errorReport)
                End If
                errorMessage.Append(".")
                Throw New WriteException(errorMessage.ToString)

            Else ' if the instrument did not report an error,

                If Me.IsCheckBufferOnSend Then

                    ' check if we have data to get
                    hasDataToRead = Me.IsMessageAvailable

                Else

                    ' devices such as the CVA cannot check errorMessage available
                    hasDataToRead = False

                End If

                If hasDataToRead Then

                    ' if we have data in the queue, we would get
                    ' a query interrupted error.  We must therefore
                    ' report an error and clear the queue.

                    ' clear the data queue
                    ' clearDataQueue

                    ' now report the error to the calling module
                    errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                 "Failed sending because VISA resource {0} had data in the its read buffer.", Me.ResourceName))
                    Throw New WriteException(errorMessage.ToString)

                Else ' if we do not have data in the queue, we can

                    Dim isQuery As Boolean = Me.IsCheckOpcAfterSend AndAlso queryCommand.Contains("?")

                    ' now send the data to the instrument
                    Try
                        If isQuery Then
                            Me.WriteQueryLine(queryCommand)
                        Else
                            Me.WriteLine(queryCommand)
                        End If
                    Catch ex As Exception
                        ' raise the error to the calling module.
                        errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Exception occurred writing to Visa resource {0}. Details: {1}", Me.ResourceName, ex))
                        Throw New WriteException(errorMessage.ToString, ex)
                    End Try

                    ' check if we are to do error check on OPC
                    If isQuery Then

                        ' if we are to perform error check on OPC then read the OPC bit
                        Me.QueryOpc()

                        If Me.HandleInstrumentErrorIfError(False) Then

                            ' now report the error to the calling module
                            errorMessage.Append(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Failed sending '{0}' to VISA resource {1}",
                                                              queryCommand, Me.ResourceName))

                            errorReport = Me.DeviceErrors
                            If Not String.IsNullOrWhiteSpace(errorReport) Then
                                errorMessage.Append(":")
                                errorMessage.Append(Environment.NewLine)
                                errorMessage.Append(errorReport)
                            End If
                            errorMessage.Append(".")
                            Throw New WriteException(errorMessage.ToString)

                        End If ' 'isError

                    End If ' IsVerifyOpc

                End If

            End If

        End Sub

#End Region

    End Class

End Namespace
