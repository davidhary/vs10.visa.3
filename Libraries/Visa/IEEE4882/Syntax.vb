Namespace Ieee4882

    ''' <summary>Defines a SCPI Base Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/05" by="David" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public NotInheritable Class Syntax

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Private Sub New()
            ' instantiate the base class
            MyBase.New()
        End Sub

#End Region

#Region " IEEE 488.2 STANDARD COMMANDS "

        ''' <summary>
        ''' Gets the Clear Status (CLS) command.
        ''' </summary>
        Public Const ClearExecutionStateCommand As String = "*CLS"

        ''' <summary>
        ''' Gets the Identify query (*IDN?) command.
        ''' </summary>
        Public Const IdentifyQueryCommand As String = "*IDN?"

        ''' <summary>
        ''' Gets the operation complete (*OPC) command.
        ''' </summary>
        Public Const OperationCompletedCommand As String = "*OPC"

        ''' <summary>
        ''' Gets the operation complete query (*OPC?) command.
        ''' </summary>
        Public Const OperationCompletedQueryCommand As String = "*OPC?"

        ''' <summary>
        ''' Gets the Wait (*WAI) command.
        ''' </summary>
        Public Const WaitCommand As String = "*WAI"

        ''' <summary>
        ''' Gets the Standard Event Enable (*ESE) command.
        ''' </summary>
        Public Const StandardEventEnableCommand As String = "*ESE {0:D}"

        ''' <summary>
        ''' Gets the Standard Event Enable query (*ESE?) command.
        ''' </summary>
        Public Const StandardEventEnableQueryCommand As String = "*ESE?"

        ''' <summary>
        ''' Gets the Standard Event Enable (*ESR?) command.
        ''' </summary>
        Public Const StandardEventStatusQueryCommand As String = "*ESR?"

        ''' <summary>
        ''' Gets the Service Request Enable (*SRE) command.
        ''' </summary>
        Public Const ServiceRequestEnableCommand As String = "*SRE {0:D}"

        ''' <summary>
        ''' Gets the Service Request Enable query (*SRE?) command.
        ''' </summary>
        Public Const ServiceRequestEnableQueryCommand As String = "*SRE?"

        ''' <summary>
        ''' Gets the Service Request Status query (*STB?) command.
        ''' </summary>
        Public Const ServiceRequestStatusQueryCommand As String = "*STB?"

        ''' <summary>
        ''' Gets the reset to known state (*RST) command.
        ''' </summary>
        Public Const ResetKnownStateCommand As String = "*RST"

#End Region

#Region " COMMAND BUILDERS "

        ''' <summary>
        ''' Builds a command for setting a Boolean value.
        ''' </summary>
        ''' <param name="boolFormat"></param>
        Public Shared Function BuildBooleanCommand(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
            Dim boolValue As String = String.Empty
            Select Case boolFormat
                Case BooleanDataFormat.None
                    boolValue = String.Empty
                Case BooleanDataFormat.OneZero
                    boolValue = My.MyLibrary.ImmediateIf(value, "1", "0")
                Case BooleanDataFormat.OnOff
                    boolValue = My.MyLibrary.ImmediateIf(value, "ON", "OFF")
                Case BooleanDataFormat.TrueFalse
                    boolValue = My.MyLibrary.ImmediateIf(value, "TRUE", "FALSE")
            End Select
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                "{0} {1}", queryCommand, boolValue)
        End Function

#End Region

    End Class

End Namespace
