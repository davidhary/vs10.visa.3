﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class NetworkAnalyzerTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called
                'onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If


            End If

            ' Free shared unmanaged resources
            'onDisposeUnmanagedResources()

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.gpibInterfaceTabPage = New System.Windows.Forms.TabPage
        Me.GpibInterfacePanel1 = New isr.Controls.InterfacePanel
        Me.gpibInstrumentTabPage = New System.Windows.Forms.TabPage
        Me.GenericGpibInstrumentPanel1 = New isr.Visa.Instruments.R2D2.GpibInstrumentPanel
        Me.GenericSerialInstrumentPanel1 = New isr.Visa.Instruments.R2D2.GenericSerialInstrumentPanel
        Me.mainStatusBar = New System.Windows.Forms.StatusBar
        Me.statusPanel = New System.Windows.Forms.StatusBarPanel
        Me.serialInstrumentTabPage = New System.Windows.Forms.TabPage
        Me.HP8753InstrumentPanel1 = New isr.Visa.Instruments.R2D2.HP8753InstrumentPanel
        Me.W8200InstrumentPanel1 = New isr.Visa.Instruments.R2D2.W8200InstrumentPanel
        Me.w8200TabPage = New System.Windows.Forms.TabPage
        Me.e382280TabPage = New System.Windows.Forms.TabPage
        Me.E382280InstrumentPanel1 = New isr.Visa.Instruments.R2D2.E382280InstrumentPanel
        Me.mainTabControl = New System.Windows.Forms.TabControl
        Me.hp8753TabPage = New System.Windows.Forms.TabPage
        Me.gpibInterfaceTabPage.SuspendLayout()
        Me.gpibInstrumentTabPage.SuspendLayout()
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.serialInstrumentTabPage.SuspendLayout()
        Me.w8200TabPage.SuspendLayout()
        Me.e382280TabPage.SuspendLayout()
        Me.mainTabControl.SuspendLayout()
        Me.hp8753TabPage.SuspendLayout()
        Me.SuspendLayout()
        '
        'GpibInterfaceTabPage
        '
        Me.gpibInterfaceTabPage.Controls.Add(Me.GpibInterfacePanel1)
        Me.gpibInterfaceTabPage.Location = New System.Drawing.Point(4, 22)
        Me.gpibInterfaceTabPage.Name = "GpibInterfaceTabPage"
        Me.gpibInterfaceTabPage.Size = New System.Drawing.Size(488, 460)
        Me.gpibInterfaceTabPage.TabIndex = 0
        Me.gpibInterfaceTabPage.Text = "Interface"
        Me.gpibInterfaceTabPage.ToolTipText = "Selects GPIB Interface"
        '
        'GpibInterfacePanel1
        '
        Me.GpibInterfacePanel1.Location = New System.Drawing.Point(88, 87)
        Me.GpibInterfacePanel1.Name = "GpibInterfacePanel1"
        Me.GpibInterfacePanel1.Size = New System.Drawing.Size(312, 264)
        Me.GpibInterfacePanel1.TabIndex = 0
        '
        'GpibInstrumentTabPage
        '
        Me.gpibInstrumentTabPage.Controls.Add(Me.GenericGpibInstrumentPanel1)
        Me.gpibInstrumentTabPage.Location = New System.Drawing.Point(4, 22)
        Me.gpibInstrumentTabPage.Name = "GpibInstrumentTabPage"
        Me.gpibInstrumentTabPage.Size = New System.Drawing.Size(488, 438)
        Me.gpibInstrumentTabPage.TabIndex = 1
        Me.gpibInstrumentTabPage.Text = "GPIB"
        Me.gpibInstrumentTabPage.ToolTipText = "Talk to a generic GPIB instrument"
        '
        'GenericGpibInstrumentPanel1
        '
        Me.GenericGpibInstrumentPanel1.IsVisible = True
        Me.GenericGpibInstrumentPanel1.Location = New System.Drawing.Point(88, 75)
        Me.GenericGpibInstrumentPanel1.Name = "GenericGpibInstrumentPanel1"
        Me.GenericGpibInstrumentPanel1.Size = New System.Drawing.Size(312, 288)
        Me.GenericGpibInstrumentPanel1.TabIndex = 0
        '
        'GenericSerialInstrumentPanel1
        '
        Me.GenericSerialInstrumentPanel1.IsVisible = True
        Me.GenericSerialInstrumentPanel1.Location = New System.Drawing.Point(88, 75)
        Me.GenericSerialInstrumentPanel1.Name = "GenericSerialInstrumentPanel1"
        Me.GenericSerialInstrumentPanel1.Size = New System.Drawing.Size(312, 288)
        Me.GenericSerialInstrumentPanel1.TabIndex = 0
        '
        'mainStatusBar
        '
        Me.mainStatusBar.Location = New System.Drawing.Point(0, 464)
        Me.mainStatusBar.Name = "mainStatusBar"
        Me.mainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusPanel})
        Me.mainStatusBar.ShowPanels = True
        Me.mainStatusBar.Size = New System.Drawing.Size(496, 22)
        Me.mainStatusBar.TabIndex = 2
        Me.mainStatusBar.Text = "StatusBar1"
        '
        'statusPanel
        '
        Me.statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanel.Name = "statusPanel"
        Me.statusPanel.Text = "<>"
        Me.statusPanel.Width = 479
        '
        'serialInstrumentTabPage
        '
        Me.serialInstrumentTabPage.Controls.Add(Me.GenericSerialInstrumentPanel1)
        Me.serialInstrumentTabPage.Location = New System.Drawing.Point(4, 22)
        Me.serialInstrumentTabPage.Name = "serialInstrumentTabPage"
        Me.serialInstrumentTabPage.Size = New System.Drawing.Size(488, 438)
        Me.serialInstrumentTabPage.TabIndex = 2
        Me.serialInstrumentTabPage.Text = "RS232"
        Me.serialInstrumentTabPage.ToolTipText = "Talk to a Keithley 7000 Instrument"
        '
        'HP8753InstrumentPanel1
        '
        Me.HP8753InstrumentPanel1.IsVisible = True
        Me.HP8753InstrumentPanel1.Location = New System.Drawing.Point(88, 75)
        Me.HP8753InstrumentPanel1.Name = "HP8753InstrumentPanel1"
        Me.HP8753InstrumentPanel1.Size = New System.Drawing.Size(312, 288)
        Me.HP8753InstrumentPanel1.TabIndex = 3
        '
        'W8200InstrumentPanel1
        '
        Me.W8200InstrumentPanel1.IsVisible = True
        Me.W8200InstrumentPanel1.Location = New System.Drawing.Point(88, 75)
        Me.W8200InstrumentPanel1.Name = "W8200InstrumentPanel1"
        Me.W8200InstrumentPanel1.Size = New System.Drawing.Size(312, 288)
        Me.W8200InstrumentPanel1.TabIndex = 0
        '
        'w8200TabPage
        '
        Me.w8200TabPage.Controls.Add(Me.W8200InstrumentPanel1)
        Me.w8200TabPage.Location = New System.Drawing.Point(4, 22)
        Me.w8200TabPage.Name = "w8200TabPage"
        Me.w8200TabPage.Size = New System.Drawing.Size(488, 438)
        Me.w8200TabPage.TabIndex = 4
        Me.w8200TabPage.Text = "W8200"
        Me.w8200TabPage.ToolTipText = "Talk to Weinschel 8200 Controller"
        '
        'e382280TabPage
        '
        Me.e382280TabPage.Controls.Add(Me.E382280InstrumentPanel1)
        Me.e382280TabPage.Location = New System.Drawing.Point(4, 22)
        Me.e382280TabPage.Name = "e382280TabPage"
        Me.e382280TabPage.Size = New System.Drawing.Size(488, 438)
        Me.e382280TabPage.TabIndex = 5
        Me.e382280TabPage.Text = "EXTECH 382280"
        Me.e382280TabPage.ToolTipText = "Talk to EXTECH 382280 Power Supply"
        '
        'E382280InstrumentPanel1
        '
        Me.E382280InstrumentPanel1.IsVisible = True
        Me.E382280InstrumentPanel1.Location = New System.Drawing.Point(88, 75)
        Me.E382280InstrumentPanel1.Name = "E382280InstrumentPanel1"
        Me.E382280InstrumentPanel1.Size = New System.Drawing.Size(312, 288)
        Me.E382280InstrumentPanel1.TabIndex = 0
        '
        'mainTabControl
        '
        Me.mainTabControl.Controls.Add(Me.gpibInterfaceTabPage)
        Me.mainTabControl.Controls.Add(Me.gpibInstrumentTabPage)
        Me.mainTabControl.Controls.Add(Me.serialInstrumentTabPage)
        Me.mainTabControl.Controls.Add(Me.hp8753TabPage)
        Me.mainTabControl.Controls.Add(Me.w8200TabPage)
        Me.mainTabControl.Controls.Add(Me.e382280TabPage)
        Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
        Me.mainTabControl.Name = "mainTabControl"
        Me.mainTabControl.SelectedIndex = 0
        Me.mainTabControl.Size = New System.Drawing.Size(496, 486)
        Me.mainTabControl.TabIndex = 3
        '
        'hp8753TabPage
        '
        Me.hp8753TabPage.Controls.Add(Me.HP8753InstrumentPanel1)
        Me.hp8753TabPage.Location = New System.Drawing.Point(4, 22)
        Me.hp8753TabPage.Name = "hp8753TabPage"
        Me.hp8753TabPage.Size = New System.Drawing.Size(488, 438)
        Me.hp8753TabPage.TabIndex = 3
        Me.hp8753TabPage.Text = " HP8753"
        Me.hp8753TabPage.ToolTipText = "Talk to HP8753 Network Analyzer"
        '
        'NetworkAnalyzerTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(496, 486)
        Me.Controls.Add(Me.mainStatusBar)
        Me.Controls.Add(Me.mainTabControl)
        Me.Name = "NetworkAnalyzerTestPanel"
        Me.Text = "Network Analyzer Test Panel"
        Me.gpibInterfaceTabPage.ResumeLayout(False)
        Me.gpibInstrumentTabPage.ResumeLayout(False)
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.serialInstrumentTabPage.ResumeLayout(False)
        Me.w8200TabPage.ResumeLayout(False)
        Me.e382280TabPage.ResumeLayout(False)
        Me.mainTabControl.ResumeLayout(False)
        Me.hp8753TabPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents gpibInterfaceTabPage As System.Windows.Forms.TabPage
    Private WithEvents GpibInterfacePanel1 As isr.Controls.InterfacePanel
    Private WithEvents gpibInstrumentTabPage As System.Windows.Forms.TabPage
    Private WithEvents GenericGpibInstrumentPanel1 As isr.Visa.Instruments.R2D2.GpibInstrumentPanel
    Private WithEvents GenericSerialInstrumentPanel1 As isr.Visa.Instruments.R2D2.GenericSerialInstrumentPanel
    Private WithEvents mainStatusBar As System.Windows.Forms.StatusBar
    Private WithEvents statusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents serialInstrumentTabPage As System.Windows.Forms.TabPage
    Private WithEvents HP8753InstrumentPanel1 As isr.Visa.Instruments.R2D2.HP8753InstrumentPanel
    Private WithEvents W8200InstrumentPanel1 As isr.Visa.Instruments.R2D2.W8200InstrumentPanel
    Private WithEvents w8200TabPage As System.Windows.Forms.TabPage
    Private WithEvents e382280TabPage As System.Windows.Forms.TabPage
    Private WithEvents E382280InstrumentPanel1 As isr.Visa.Instruments.R2D2.E382280InstrumentPanel
    Private WithEvents mainTabControl As System.Windows.Forms.TabControl
    Private WithEvents hp8753TabPage As System.Windows.Forms.TabPage
End Class
