﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class SandiaTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.K7000Panel1 = New isr.Visa.Instruments.K7000.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.gpibInterfaceTabPage = New System.Windows.Forms.TabPage
        Me.GpibInterface1 = New isr.Controls.InterfacePanel
        Me.k2400TabPage = New System.Windows.Forms.TabPage
        Me.K2400Panel1 = New isr.Visa.Instruments.K2400.Panel
        Me.k7000TabPage = New System.Windows.Forms.TabPage
        Me.MainStatusBar = New System.Windows.Forms.StatusBar
        Me.statusStatusBarPanel = New System.Windows.Forms.StatusBarPanel
        Me.TabControl1.SuspendLayout()
        Me.gpibInterfaceTabPage.SuspendLayout()
        Me.k2400TabPage.SuspendLayout()
        Me.k7000TabPage.SuspendLayout()
        CType(Me.statusStatusBarPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'K7000Panel1
        '
        Me.K7000Panel1.BackColor = System.Drawing.Color.Transparent
        Me.K7000Panel1.IsVisible = True
        Me.K7000Panel1.Location = New System.Drawing.Point(48, 34)
        Me.K7000Panel1.Name = "K7000Panel1"
        Me.K7000Panel1.Size = New System.Drawing.Size(312, 288)
        Me.K7000Panel1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.gpibInterfaceTabPage)
        Me.TabControl1.Controls.Add(Me.k2400TabPage)
        Me.TabControl1.Controls.Add(Me.k7000TabPage)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(416, 382)
        Me.TabControl1.TabIndex = 2
        '
        'GpibInterfaceTabPage
        '
        Me.gpibInterfaceTabPage.Controls.Add(Me.GpibInterface1)
        Me.gpibInterfaceTabPage.Location = New System.Drawing.Point(4, 22)
        Me.gpibInterfaceTabPage.Name = "GpibInterfaceTabPage"
        Me.gpibInterfaceTabPage.Size = New System.Drawing.Size(408, 356)
        Me.gpibInterfaceTabPage.TabIndex = 2
        Me.gpibInterfaceTabPage.Text = "GPIB Interface"
        Me.gpibInterfaceTabPage.UseVisualStyleBackColor = True
        '
        'GpibInterface1
        '
        Me.GpibInterface1.BackColor = System.Drawing.Color.Transparent
        Me.GpibInterface1.Location = New System.Drawing.Point(48, 32)
        Me.GpibInterface1.Name = "GpibInterface1"
        Me.GpibInterface1.Size = New System.Drawing.Size(312, 272)
        Me.GpibInterface1.TabIndex = 0
        '
        'k2400TabPage
        '
        Me.k2400TabPage.Controls.Add(Me.K2400Panel1)
        Me.k2400TabPage.Location = New System.Drawing.Point(4, 22)
        Me.k2400TabPage.Name = "k2400TabPage"
        Me.k2400TabPage.Size = New System.Drawing.Size(408, 356)
        Me.k2400TabPage.TabIndex = 0
        Me.k2400TabPage.Text = "Keithley 2400"
        Me.k2400TabPage.UseVisualStyleBackColor = True
        '
        'K2400Panel1
        '
        Me.K2400Panel1.BackColor = System.Drawing.Color.Transparent
        Me.K2400Panel1.IsVisible = True
        Me.K2400Panel1.Location = New System.Drawing.Point(48, 34)
        Me.K2400Panel1.Name = "K2400Panel1"
        Me.K2400Panel1.Size = New System.Drawing.Size(312, 288)
        Me.K2400Panel1.TabIndex = 0
        '
        'k7000TabPage
        '
        Me.k7000TabPage.Controls.Add(Me.K7000Panel1)
        Me.k7000TabPage.Location = New System.Drawing.Point(4, 22)
        Me.k7000TabPage.Name = "k7000TabPage"
        Me.k7000TabPage.Size = New System.Drawing.Size(408, 356)
        Me.k7000TabPage.TabIndex = 1
        Me.k7000TabPage.Text = "Keithley 7000"
        Me.k7000TabPage.UseVisualStyleBackColor = True
        '
        'MainStatusBar
        '
        Me.MainStatusBar.Location = New System.Drawing.Point(0, 382)
        Me.MainStatusBar.Name = "MainStatusBar"
        Me.MainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusStatusBarPanel})
        Me.MainStatusBar.ShowPanels = True
        Me.MainStatusBar.Size = New System.Drawing.Size(416, 24)
        Me.MainStatusBar.TabIndex = 3
        Me.MainStatusBar.Text = "StatusBar1"
        '
        'statusStatusBarPanel
        '
        Me.statusStatusBarPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusStatusBarPanel.Name = "statusStatusBarPanel"
        Me.statusStatusBarPanel.Text = "<status>"
        Me.statusStatusBarPanel.ToolTipText = "Display the current interface status"
        Me.statusStatusBarPanel.Width = 399
        '
        'SandiaTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(416, 406)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MainStatusBar)
        Me.Name = "SandiaTestPanel"
        Me.Text = "Source/Measure Switching Panel"
        Me.TabControl1.ResumeLayout(False)
        Me.gpibInterfaceTabPage.ResumeLayout(False)
        Me.k2400TabPage.ResumeLayout(False)
        Me.k7000TabPage.ResumeLayout(False)
        CType(Me.statusStatusBarPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents K7000Panel1 As isr.Visa.Instruments.K7000.Panel
    Private WithEvents TabControl1 As System.Windows.Forms.TabControl
    Private WithEvents gpibInterfaceTabPage As System.Windows.Forms.TabPage
    Private WithEvents GpibInterface1 As isr.Controls.InterfacePanel
    Private WithEvents k2400TabPage As System.Windows.Forms.TabPage
    Private WithEvents K2400Panel1 As isr.Visa.Instruments.K2400.Panel
    Private WithEvents k7000TabPage As System.Windows.Forms.TabPage
    Private WithEvents MainStatusBar As System.Windows.Forms.StatusBar
    Private WithEvents statusStatusBarPanel As System.Windows.Forms.StatusBarPanel
End Class
