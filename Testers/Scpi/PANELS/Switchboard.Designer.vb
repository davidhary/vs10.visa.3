﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._openTestPanelButton = New System.Windows.Forms.Button
        Me._testPanelsComboBox = New System.Windows.Forms.ComboBox
        Me._openAboutButton = New System.Windows.Forms.Button
        Me._helpProvider = New System.Windows.Forms.HelpProvider
        Me._exitButton = New System.Windows.Forms.Button
        Me._cancelButton = New System.Windows.Forms.Button
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._messageTextBox = New isr.Controls.MessagesBox
        Me._messagesTextBoxLabel = New System.Windows.Forms.Label
        Me._tipsErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._tipsErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_openTestPanelButton
        '
        Me._openTestPanelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._helpProvider.SetHelpString(Me._openTestPanelButton, "Click to test")
        Me._openTestPanelButton.Location = New System.Drawing.Point(240, 226)
        Me._openTestPanelButton.Name = "_openTestPanelButton"
        Me._helpProvider.SetShowHelp(Me._openTestPanelButton, True)
        Me._openTestPanelButton.Size = New System.Drawing.Size(60, 23)
        Me._openTestPanelButton.TabIndex = 14
        Me._openTestPanelButton.Text = "&Open"
        Me._toolTip.SetToolTip(Me._openTestPanelButton, "Click to open the selected test panel.")
        '
        '_testPanelsComboBox
        '
        Me._testPanelsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._testPanelsComboBox.Items.AddRange(New Object() {"Simple Read and Write"})
        Me._testPanelsComboBox.Location = New System.Drawing.Point(10, 226)
        Me._testPanelsComboBox.Name = "_testPanelsComboBox"
        Me._testPanelsComboBox.Size = New System.Drawing.Size(224, 21)
        Me._testPanelsComboBox.TabIndex = 13
        Me._toolTip.SetToolTip(Me._testPanelsComboBox, "Select a test panel from the list.")
        '
        '_openAboutButton
        '
        Me._openAboutButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._helpProvider.SetHelpString(Me._openAboutButton, "Click to test")
        Me._openAboutButton.Location = New System.Drawing.Point(125, 258)
        Me._openAboutButton.Name = "_openAboutButton"
        Me._helpProvider.SetShowHelp(Me._openAboutButton, True)
        Me._openAboutButton.Size = New System.Drawing.Size(60, 23)
        Me._openAboutButton.TabIndex = 12
        Me._openAboutButton.Text = "&About"
        Me._toolTip.SetToolTip(Me._openAboutButton, "Click to test")
        '
        '_exitButton
        '
        Me._exitButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._exitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._helpProvider.SetHelpString(Me._exitButton, "Click pr press Enter to exit")
        Me._exitButton.Location = New System.Drawing.Point(240, 258)
        Me._exitButton.Name = "_exitButton"
        Me._helpProvider.SetShowHelp(Me._exitButton, True)
        Me._exitButton.Size = New System.Drawing.Size(60, 23)
        Me._exitButton.TabIndex = 7
        Me._exitButton.Text = "E&xit"
        Me._toolTip.SetToolTip(Me._exitButton, "Click pr press Enter to exit")
        '
        '_cancelButton
        '
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._helpProvider.SetHelpString(Me._cancelButton, "Click or press Esc to exit")
        Me._cancelButton.Location = New System.Drawing.Point(10, 258)
        Me._cancelButton.Name = "_cancelButton"
        Me._helpProvider.SetShowHelp(Me._cancelButton, True)
        Me._cancelButton.Size = New System.Drawing.Size(60, 23)
        Me._cancelButton.TabIndex = 10
        Me._cancelButton.Text = "&Cancel"
        Me._toolTip.SetToolTip(Me._cancelButton, "Click or press Esc to exit")
        '
        '_messageTextBox
        '
        Me._messageTextBox.Delimiter = "; "
        Me._messageTextBox.Location = New System.Drawing.Point(12, 19)
        Me._messageTextBox.Multiline = True
        Me._messageTextBox.Name = "_messageTextBox"
        Me._messageTextBox.PresetCount = 50
        Me._messageTextBox.ReadOnly = True
        Me._messageTextBox.ResetCount = 100
        Me._messageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._messageTextBox.Size = New System.Drawing.Size(288, 197)
        Me._messageTextBox.TabIndex = 11
        Me._messageTextBox.TimeFormat = "HH:mm:ss.f"
        '
        '_messagesTextBoxLabel
        '
        Me._messagesTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._messagesTextBoxLabel.Location = New System.Drawing.Point(12, 2)
        Me._messagesTextBoxLabel.Name = "_messagesTextBoxLabel"
        Me._messagesTextBoxLabel.Size = New System.Drawing.Size(67, 14)
        Me._messagesTextBoxLabel.TabIndex = 8
        Me._messagesTextBoxLabel.Text = "Messages: "
        Me._messagesTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_tipsErrorProvider
        '
        Me._tipsErrorProvider.ContainerControl = Me
        Me._tipsErrorProvider.Icon = CType(resources.GetObject("_tipsErrorProvider.Icon"), System.Drawing.Icon)
        '
        '_errorProvider
        '
        Me._errorProvider.ContainerControl = Me
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 286)
        Me.Controls.Add(Me._openTestPanelButton)
        Me.Controls.Add(Me._testPanelsComboBox)
        Me.Controls.Add(Me._openAboutButton)
        Me.Controls.Add(Me._exitButton)
        Me.Controls.Add(Me._messageTextBox)
        Me.Controls.Add(Me._messagesTextBoxLabel)
        Me.Controls.Add(Me._cancelButton)
        Me.Name = "Switchboard"
        Me.Text = "Switchboard"
        CType(Me._tipsErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _openTestPanelButton As System.Windows.Forms.Button
    Private WithEvents _helpProvider As System.Windows.Forms.HelpProvider
    Private WithEvents _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _testPanelsComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _openAboutButton As System.Windows.Forms.Button
    Private WithEvents _exitButton As System.Windows.Forms.Button
    Private WithEvents _cancelButton As System.Windows.Forms.Button
    Private WithEvents _messageTextBox As isr.Controls.MessagesBox
    Private WithEvents _messagesTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _tipsErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _errorProvider As System.Windows.Forms.ErrorProvider
End Class
