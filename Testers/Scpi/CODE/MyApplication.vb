Namespace My

    Partial Friend Class MyApplication

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Destroys objects for this project.
        ''' </summary>
    Friend Shared Sub Destroy()
            ' terminate the reference to instances created by this class
        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
    Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        MyApplication.Destroy()

                    End If

                End If


                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " CONSTANTS "

        Private Shared _unknownValue As String = "N/A"
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", 
          Justification:="Future use.")> 
        Public Shared ReadOnly Property UnknownValue() As String
            Get
                Return MyApplication._unknownValue
            End Get
        End Property

        Private Shared _unknownFileNameValue As String = "NA"
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", 
          Justification:="Future use.")> 
        Public Shared ReadOnly Property UnknownFileNameValue() As String
            Get
                Return MyApplication._unknownFileNameValue
            End Get
        End Property

#End Region

#Region " METHODS: MESSAGE MANAGERS "

        ''' <summary>Builds a message for the message log.
        ''' </summary>
    ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Function BuildMessage(ByVal message As String) As String

            Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                               "{0:HH:mm:ss.fff} {1}", DateTime.Now, message)

        End Function

        ''' <summary>Appends a message to the message log.
        '''   This must be defined within the application calling the
        '''   methods as the TextBox is seen as private shared if called from an
        '''   external application.
        ''' </summary>
    ''' <param name="messageTextBox">Specifies a reference to the message box which to use.</param>
        ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Sub AppendMessage(ByVal messageTextBox As System.Windows.Forms.TextBox, ByVal message As String)

            messageTextBox.SelectionStart = messageTextBox.Text.Length
            messageTextBox.SelectionLength = 0
            If messageTextBox.SelectionStart = 0 Then
                messageTextBox.SelectedText = My.MyApplication.BuildMessage(message)
            Else
                messageTextBox.SelectedText = Environment.NewLine & My.MyApplication.BuildMessage(message)
            End If
            messageTextBox.SelectionStart = messageTextBox.Text.Length

        End Sub

        ''' <summary>Appends a message to the message log.
        '''   This must be defined within the application calling the
        '''   methods as the TextBox is seen as private if called from an
        '''   external application.
        ''' </summary>
    ''' <param name="messageTextBox">Specifies a reference to the message box which to use.</param>
        ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Sub PrependMessage(ByVal messageTextBox As System.Windows.Forms.TextBox, ByVal message As String)

            messageTextBox.SelectionStart = 0
            messageTextBox.SelectionLength = 0
            messageTextBox.SelectedText = My.MyApplication.BuildMessage(message) & Environment.NewLine
            messageTextBox.SelectionLength = 0

        End Sub

#End Region

    End Class

End Namespace

