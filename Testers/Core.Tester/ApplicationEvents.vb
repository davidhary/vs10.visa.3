Namespace My

    Partial Friend Class MyApplication

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Destroys objects for this project.
        ''' </summary>
        Friend Shared Sub Destroy()

            ' terminate the reference to instances created by this class
            If Not MyApplication._splashScreen Is Nothing Then
                MyApplication._splashScreen = Nothing
            End If

            If Err.Number <> 0 Then
                ' prepend error source to allow calling application to pin point the
                ' source of the error.
                Err.Source = My.Application.Info.Title & ".My.MyApplication.Destroy, " & Err.Source
            End If

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                        MyApplication.Destroy()

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Creates objects for this project.
        ''' </summary>
        ''' <returns>True if ok.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Friend Shared Function CreateObjects(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Dim lastAction As String = String.Empty
            Dim className As String = String.Empty
            Dim passed As Boolean

            Try

                ' show status
                lastAction = "Application is initializing..."
                MyApplication.SplashScreenMessage = lastAction

                lastAction = "Parsing the command line..."
                MyApplication.SplashScreenMessage = lastAction
                MyApplication.ParseCommandLine(String.Join(" ", commandLineArgs.ToArray))

                passed = True

                ' return true
                Return passed

            Catch ex As Exception

                ' set error description
                If Not String.IsNullOrWhiteSpace(className) Then

                    Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                            "Failed {0}. Class name='{1}'", lastAction, className)
                    Return ProcessException(ex, message, WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

                Else

                    Throw

                End If

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                If Not passed Then
                    Try
                        MyApplication.Destroy()
                    Finally
                    End Try
                End If

            End Try

        End Function

        ''' <summary>Parses the command line adding the default options.
        ''' </summary>
        ''' <param name="commandLine">Specifies the command line.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Shared Sub ParseCommandLine(ByVal commandLine As String)

            ' save the command line
            ' Dim parser As New isr.Support.CommandLineParser(commandLine)

        End Sub

#End Region

#Region " SPLASH "

        ''' <summary>Gets or sets reference to the
        ''' <see cref=" isr.WindowsForms.SplashScreen">splash screen class</see>.</summary>
        Private Shared _splashScreen As isr.WindowsForms.SplashScreen

        ''' <summary>Displays a splash screen message instantiating and displaying
        '''   the splash screen for the first message.
        ''' </summary>
        Public Shared WriteOnly Property SplashScreenMessage() As String
            Set(ByVal Value As String)

                If MyApplication._splashScreen Is Nothing Then

                    If My.Application.SplashScreen Is Nothing Then
                        My.Application.SplashScreen = New isr.WindowsForms.SplashScreen
                        My.Application.ShowSplashScreen()
                    End If
                    MyApplication._splashScreen = CType(My.Application.SplashScreen, isr.WindowsForms.SplashScreen)
                    If Not MyApplication._splashScreen.Visible Then
                        MyApplication._splashScreen.Show()
                        MyApplication._splashScreen.Refresh()
                    End If
                    MyApplication._splashScreen.TopmostSetter(Not My.MyApplication.InDesignMode)

                    ' update the licensee name if necessary.
                    MyApplication._splashScreen.LicenseeName = "Integrated Scientific Resources, Inc."

                ElseIf Not MyApplication._splashScreen.Visible Then

                    MyApplication._splashScreen.Show()
                    MyApplication._splashScreen.Refresh()

                End If

                ' display the message
                MyApplication._splashScreen.Status = Value

            End Set
        End Property

#End Region

#Region " APPLICATION LOG "

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="details">Specifies the message details</param>
        ''' <returns>Message or empty string.</returns>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
            If details IsNot Nothing Then
                My.Application.Log.WriteEntry(details, severity)
                Return details
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="format">Specifies the message format</param>
        ''' <param name="args">Specified the message arguments</param>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Return MyApplication.WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="messages">Message information to log.</param>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
            If messages IsNot Nothing Then
                Return MyApplication.WriteLogEntry(severity, String.Join(",", messages))
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds exception details to the error log.  Includes stack and data.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        ''' <param name="severity">Specifies the exception severity.</param>
        ''' <param name="additionalInfo">Specifies additional information.</param>
        Private Shared Sub _writeExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
        ByVal additionalInfo As String)

            My.Application.Log.WriteException(ex, severity, additionalInfo)
            If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
                Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                MyApplication.WriteLogEntry(severity, stackTrace)
            End If
            If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                    My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
                Next
            End If
            If ex.InnerException IsNot Nothing Then
                MyApplication._writeExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
            End If

        End Sub

        ''' <summary>
        ''' Adds exception details to the error log.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        ''' <param name="severity">Specifies the exception severity.</param>
        ''' <param name="additionalInfo">Specifies additional information.</param>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
        ByVal additionalInfo As String)

            ' write exception details.
            MyApplication._writeExceptionDetails(ex, severity, additionalInfo)

        End Sub

        ''' <summary>
        ''' Adds exception details to the error log.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
            MyApplication.WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
        End Sub

#End Region

#Region " APPLICATION LEVEL METHODS AND PROPERTIES "

        Private Shared _currentProcessName As String
        ''' <summary> Gets the current process name. </summary>
        ''' <value> The name of the current process. </value>
        Public Shared ReadOnly Property CurrentProcessName() As String
            Get
                If String.IsNullOrWhiteSpace(MyApplication._currentProcessName) Then
                    _currentProcessName = Process.GetCurrentProcess().ProcessName.ToUpperInvariant
                End If
                Return _currentProcessName
            End Get
        End Property

        ''' <summary> Gets the condition for running within the IDE, i.e., in Design Mode. </summary>
        ''' <remarks> Use this property to check if the application is running from the IDE. </remarks>
        ''' <value> <c>InDesignMode</c> is a <see cref="Boolean"/> property that can be read from (read
        ''' only). </value>
        Public Shared ReadOnly Property InDesignMode() As Boolean
            Get
                Return Debugger.IsAttached
            End Get
        End Property

        ''' <summary>
        ''' Process any unhandled exceptions that occur in the application. 
        ''' Call this method from GUI entry points in the application, such as button 
        ''' click events, when an unhandled exception occurs.  
        ''' This could also handle the Application.ThreadException event, however 
        ''' the VS2005 debugger breaks before the event Application.ThreadException 
        ''' is called.
        ''' </summary>
        ''' <param name="ex">Specifies the unhandled exception.</param>
        Public Shared Function ProcessException(ByVal ex As Exception) As Windows.Forms.DialogResult

            Return My.MyApplication.ProcessException(ex, String.Empty, isr.WindowsForms.ExceptionDisplayButtons.Continue)

        End Function

        ''' <summary>
        ''' Process any unhandled exceptions that occur in the application. 
        ''' Call this method from GUI entry points in the application, such as button 
        ''' click events, when an unhandled exception occurs.  
        ''' This could also handle the Application.ThreadException event, however 
        ''' the VS2005 debugger breaks before the event Application.ThreadException 
        ''' is called.
        ''' </summary>
        ''' <param name="ex">Specifies the unhandled exception.</param>
        ''' <param name="additionalInfo">Includes additional information.</param>
        Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String) As Windows.Forms.DialogResult

            Return My.MyApplication.ProcessException(ex, additionalInfo, isr.WindowsForms.ExceptionDisplayButtons.Continue)

        End Function

        ''' <summary>
        ''' Process any unhandled exceptions that occur in the application. 
        ''' Call this method from GUI entry points in the application, such as button 
        ''' click events, when an unhandled exception occurs.  
        ''' This could also handle the Application.ThreadException event, however 
        ''' the VS2005 debugger breaks before the event Application.ThreadException 
        ''' is called.
        ''' </summary>
        ''' <param name="ex">Specifies the unhandled exception.</param>
        ''' <param name="additionalInfo">Includes additional information.</param>
        ''' <param name="buttons">Specifies the buttons on the exception display.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, ByVal buttons As isr.WindowsForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

            Dim result As Windows.Forms.DialogResult
            Try

                ' log the exception
                My.MyApplication.WriteExceptionDetails(ex, TraceEventType.Critical, additionalInfo)

                Dim frm As New isr.WindowsForms.ExceptionDisplay
                result = frm.ShowDialog(ex, buttons)
                My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture,
                    "{0} requested by user.", result), TraceEventType.Verbose)
                Return result

            Catch displayException As System.Exception

                ' Log but also display the error in a message box
                My.MyApplication.WriteExceptionDetails(displayException, TraceEventType.Critical, "Exception occurred displaying application exception.")

                Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
                errorMessage.Append("The following error occurred while displaying the application exception:")
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}Click Abort to exit application.  Otherwise, the application will continue.", Environment.NewLine)
                result = MessageBox.Show(
                  errorMessage.ToString(), "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop)

            Finally

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        Private Shared _usingDevices As Boolean
        ''' <summary>
        ''' Gets or sets the condition for using devices.
        ''' </summary>
        ''' <remarks>
        ''' When true, the application connects to actual devices.  Set to false in case
        ''' testing the application in the absence of devices.
        ''' </remarks>
        Public Shared Property UsingDevices() As Boolean
            Get
                Return MyApplication._usingDevices
            End Get
            Set(ByVal value As Boolean)
                MyApplication._usingDevices = value
            End Set
        End Property


#End Region

#Region " MODULE LEVEL METHODS AND PROPERTIES "

        ''' <summary>
        ''' Parses the command line.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function parseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Try

                ' process the project-specific command line
                MyApplication._usingDevices = True
                For Each commandLineArg As String In commandLineArgs
                    If commandLineArg.StartsWith("/hw:", StringComparison.OrdinalIgnoreCase) Then
                        Dim value As String = commandLineArg.Substring(4)
                        MyApplication._usingDevices = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                    End If
                Next

                Return True

            Catch exn As System.Exception

                Dim message As String = "Exception occurred processing command line arguments. Program execution will continue after this message."
                Return ProcessException(exn, message, WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

            End Try

        End Function

        ''' <summary>Instantiates objects.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function instantiateObjects(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Try

                Return My.MyApplication.CreateObjects(commandLineArgs)

            Catch ex As System.Exception

                Return My.MyApplication.ProcessException(ex, "Exception occurred instantiating objects.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

            Finally

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        ''' <summary>Reads settings.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function readSettings() As Boolean

            Try

                ' read the project-specific settings

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Return True

            Catch ex As System.Exception

                Return My.MyApplication.ProcessException(ex, "Exception occurred reading settings.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

            End Try

        End Function

#End Region

#Region " APPLICATION EVENTS "

        ''' <summary>Occurs when the network connection is connected or disconnected.
        ''' </summary>
        Private Sub MyApplication_NetworkAvailabilityChanged(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs) Handles Me.NetworkAvailabilityChanged

        End Sub

        ''' <summary>Raised after all application forms are closed.  
        ''' This event is not raised if the application terminates abnormally.
        ''' </summary>
        ''' <remarks>
        ''' The event saves user settings for all related libraries.
        ''' </remarks>
        Private Sub MyApplication_Shutdown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shutdown

            If My.Application.SaveMySettingsOnExit Then

                ' save settings on all related libraries. This is required to resist any new settings changed by the library.
                ' Visa.My.MyLibrary.SaveSettings()
                WriteLogEntry(TraceEventType.Information, "Saving gpib device address = {0}", My.Settings.GpibDeviceAddress)

                ' For some reason the event handling set in the Settings class dos not really work.
                My.Settings.Save()

            End If

            ' do some garbage collection
            System.GC.Collect()

        End Sub

        ''' <summary>Occurs when the application starts, before the startup form is created.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup

            ' Turn on the screen hourglass
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
            My.Application.DoEvents()

            ' set reference to the splash screen.
            'My.Application.SplashScreen = New  isr.WindowsForms.SplashScreen

            ' display the splash screen.
            My.Application.ShowSplashScreen()

            Dim info As New System.IO.FileInfo(My.Application.Log.DefaultFileLogWriter.FullLogFileName)
            If String.IsNullOrWhiteSpace(info.FullName) OrElse info.Length < 2 Then

                Trace.CorrelationManager.StartLogicalOperation(My.Application.Info.AssemblyName)
                My.MyApplication.WriteLogEntry(TraceEventType.Critical, "{0} version {1} {2} {3}", My.Application.Info.ProductName, My.Application.Info.Version.ToString(4), Date.Now.ToShortDateString(), Date.Now.ToLongTimeString)
                Trace.CorrelationManager.StopLogicalOperation()
            End If

            Try

                If Me.ParseCommandLine(e.CommandLine) AndAlso Me.instantiateObjects(e.CommandLine) AndAlso Me.readSettings Then

                    ' show status
                    My.MyApplication.SplashScreenMessage = "Loading Test Panel..."

                    ' show status
                    My.MyApplication.SplashScreenMessage = "Unloading Splash Screen..."

                    If My.MyApplication.InDesignMode Then
                        My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "Design mode")
                    Else
                        My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "Runtime mode")
                    End If

                Else

                    My.MyApplication.WriteLogEntry(TraceEventType.Error, "failed starting")
                    ' exit with an error code
                    e.Cancel = True
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()

                End If

            Catch

                Throw

            Finally

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        ''' <summary>Occurs when launching a single-instance application and the application is already active. 
        ''' </summary>
        Private Sub MyApplication_StartupNextInstance(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs) Handles Me.StartupNextInstance

            My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "starting next instant")

        End Sub

        ''' <summary>Raised if the application encounters an unhandled exception.
        ''' </summary>
        ''' <remarks>Use this method to trap unhandled exceptions.  The Application.UnhandledException 
        '''   event fires whenever an unhandled exception is thrown on the current thread.  Use this
        '''   global exception handler to protect all forms from any unhandled errors. The handler
        '''   may ignore the error, log it to a file, display a message box that asks the end user 
        '''   whether she wants to abort the application, send an e-mail to the tech support group, 
        '''   and any other action you deem desirable.
        ''' </remarks>
        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException

            If My.MyApplication.ProcessException(e.Exception, "Unhandled Exception.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) =
                Windows.Forms.DialogResult.Abort Then
                ' exit with an error code
                Environment.Exit(-1)
                e.ExitApplication = True
            End If

        End Sub

#End Region

#Region " ON EVENTS  "

        ''' <summary>
        ''' Replaces the default trace listener with the modified listener.
        ''' Updates the minimum splash screen display time.
        ''' </summary>
        Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            ' replace the default trace listener with the modified listener.
            MyBase.Log.TraceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultTraceListenerName)
            MyBase.Log.TraceSource.Listeners.Add(New DefaultFileLogTraceListener(Debugger.IsAttached, True))

            isr.Visa.VisaManager.ReplaceDefaultTraceListener(My.Application.Log.DefaultFileLogWriter)
            isr.Visa.VisaManager.TraceLevelSetter(My.Settings.TraceLevel)

            ' Set the display time to value from the settings class.
            Me.MinimumSplashScreenDisplayTime = My.Settings.MinimumSplashScreenDisplayMilliseconds

            Return MyBase.OnInitialize(commandLineArgs)

        End Function

#End Region

    End Class

End Namespace

