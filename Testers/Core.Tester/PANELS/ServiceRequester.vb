﻿Imports NationalInstruments.VisaNS
''' <summary>
''' Illustrates how to use the service request event and the service request status byte to determine 
''' when generated data is ready and how to read it. Includes code for form Switchboard.</summary>
''' <history date="12/25/10" by="David" revision="3.0.4010.x">
''' Uses isr.Visa.Core
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' National Instrument example.
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class ServiceRequester

#Region " CONSTRUCTORS AND DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        toolTip.SetToolTip(enableSRQButton, "Enable the instrument's SRQ event on MAV by sending the following command (varies by instrument)")
        toolTip.SetToolTip(deviceConfigureButton, "The resource name of the device is set and the control attempts to connect to the device")
        toolTip.SetToolTip(writeButton, "Send string to device")
        toolTip.SetToolTip(resetButton, "Causes the control to release its handle to the device")
        writeTextBox.Enabled = False
        commandTextBox.Enabled = False

    End Sub

#End Region

    Private gpibSession As GpibSession
    Private lastResourceString As String

    ' When the Configure button is pressed, the resource name of the
    ' device is set and the control attempts to connect to the device
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub deviceConfigureButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deviceConfigureButton.Click
        If resourceNameTextBox.Enabled = True Then
            If lastResourceString <> Nothing Then
                resourceNameTextBox.Text = lastResourceString
            End If
            lastResourceString = resourceNameTextBox.Text

            Try

                '  gpibSession = CType(ResourceManager.GetLocalManager().Open(resourceNameTextBox.Text), gpibSession)
                gpibSession = Visa.IEEE4882.GpibSession.OpenSession(lastResourceString, 3000)

                'For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object 
                'marshals callbacks across threads appropriately.
                ' DH: 3339 Setting true prevents display.
                If Me._syncCallBacksCheckBox.Checked Then
                    ' note that setting to false also breaks display updates.
                    gpibSession.SynchronizeCallbacks = True
                End If

                resourceNameTextBox.Enabled = False
                commandTextBox.Enabled = True
                writeTextBox.Enabled = False

            Catch ex As InvalidCastException
                MessageBox.Show("Resource selected must be a GPIB session")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Else
            MessageBox.Show("You have to reset the currently open resource first.")
        End If
    End Sub

    ' The Enable SRQ button writes the string that tells the instrument to
    ' enable the SRQ bit
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub enableSRQButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableSRQButton.Click
        If commandTextBox.Enabled = True Then
            Try
                ' you have to register the handler before you enable event  
                AddHandler gpibSession.ServiceRequest, AddressOf OnServiceRequest
                gpibSession.EnableEvent(MessageBasedSessionEventType.ServiceRequest, EventMechanism.Handler)
                WriteToSession(commandTextBox.Text)
                commandTextBox.Enabled = False
                writeTextBox.Enabled = True
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            End Try
        Else
            If resourceNameTextBox.Enabled = True Then
                MessageBox.Show("You have to first open a resource.")
            Else
                MessageBox.Show("Enter a command to send to the resource.")
            End If
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub resetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetButton.Click
        If resourceNameTextBox.Enabled = False Then
            gpibSession.Dispose()
            resourceNameTextBox.Enabled = True
            writeTextBox.Enabled = False
            commandTextBox.Enabled = False
        Else
            MessageBox.Show("There is no open resource currently.")
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
        If writeTextBox.Enabled = True Then
            WriteToSession(writeTextBox.Text)
        Else
            If resourceNameTextBox.Enabled = True Then
                MessageBox.Show("You have to first open a resource.")
            Else
                MessageBox.Show("You have to first send a command to enable the SRQ event on MAV.")
            End If
        End If
    End Sub

    Private Sub clearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
        readTextBox.Clear()
    End Sub

    Private Shared Function ReplaceCommonEscapeSequences(ByVal s As String) As String
        If (s <> Nothing) Then
            Return s.Replace("\n", Convert.ToChar(10)).Replace("\r", Convert.ToChar(13))
        Else
            Return Nothing
        End If
    End Function

    Private Shared Function InsertCommonEscapeSequences(ByVal s As String) As String
        If (s <> Nothing) Then
            Return s.Replace(Convert.ToChar(10), "\n").Replace(Convert.ToChar(13), "\r")
        Else
            Return Nothing
        End If
    End Function

    Private timer As Diagnostics.Stopwatch
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub WriteToSession(ByVal txtWrite As String)
        Try
            Dim textToWrite As String = ReplaceCommonEscapeSequences(txtWrite)
            timer = Diagnostics.Stopwatch.StartNew
            gpibSession.Write(textToWrite)
        Catch exp As Exception
            MessageBox.Show(exp.Message)
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")> 
    Private Sub OnServiceRequest(ByVal sender As Object, ByVal e As MessageBasedSessionEventArgs)
        Dim gs As GpibSession = CType(sender, GpibSession)
        If gs IsNot Nothing Then
            Try
                Dim sb As StatusByteFlags = CType(gs.ReadStatusByte(), StatusByteFlags)
                If (sb And StatusByteFlags.MessageAvailable) <> 0 Then
                    Dim textRead As String = gs.ReadString()
                    If timer IsNot Nothing Then
                        Me._timingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
                    End If
                    readTextBox.Text = InsertCommonEscapeSequences(textRead)
                Else
                    MessageBox.Show("MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible.")
                End If
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            End Try
        Else
            MessageBox.Show("Sender is not GPIB session")
        End If
    End Sub

End Class