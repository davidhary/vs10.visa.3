﻿''' <summary>
''' Selects a VISA resource.
''' </summary>
''' <history date="12/25/10" by="David" revision="3.0.4010.x">
''' Uses isr.Visa.Core
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class ResourceSelector

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            Dim resourceManager As New Visa.LocalResourceManager()
            Dim resources() As String = resourceManager.FindResources()
            If resources IsNot Nothing AndAlso resources.Count > 0 Then
                For Each s As String In resources
                    availableResources.Items.Add(s)
                Next
            ElseIf resourceManager.FailedFindingResources Then
                availableResources.Items.Add("VISA Manager failed fetching the list of local resources. It reported: " & resourceManager.ResourceFindingFailureSymptom & ". Try entering Resource String per the sample formats in the Resource String edit box and select OK.")
                availableResources.Items.Add("GPIB[board]::number[::INSTR]")
                availableResources.Items.Add("GPIB[board]::INTFC")
            Else
                availableResources.Items.Add(resourceManager.ResourceFindingFailureSymptom)
            End If
            availableResources.Items.Add("TCPIP[board]::host address[::LAN device name][::INSTR]")
            availableResources.Items.Add("TCPIP[board]::host address::port::SOCKET")

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

        Catch ex As Exception

            If My.MyApplication.ProcessException(ex, "", WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub availableResources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles availableResources.DoubleClick
        Dim selectedString As String = CType(availableResources.SelectedItem, String)
        ResourceName = selectedString
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub availableResources_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles availableResources.SelectedIndexChanged
        Dim selectedString As String = CType(availableResources.SelectedItem, String)
        ResourceName = selectedString
    End Sub

    Public Property ResourceName() As String
        Get
            Return visaResourceName.Text
        End Get
        Set(ByVal Value As String)
            visaResourceName.Text = Value
        End Set
    End Property

End Class