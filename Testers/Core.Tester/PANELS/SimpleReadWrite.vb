﻿''' <summary>
''' Illustrates using simple read write commands.
''' </summary>
''' <history date="12/25/10" by="David" revision="3.0.4010.x">
''' Uses isr.Visa.Core
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SimpleReadWrite

    Private _session As Visa.MessageBased.Session
    ''' <summary>
    ''' Selects a resource and open a message based session with this resource.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Private Sub openSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openSessionButton.Click

        Try

            clear()
            Dim selector As New ResourceSelector()
            If selector.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                Me.statusPanel.Text = "Opening Session."
                Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Me._session = Visa.MessageBased.Session.OpenSession(selector.ResourceName)
                If Me._session IsNot Nothing Then
                    UpdateControlsState(True)
                    Me.statusPanel.Text = "Session Open."
                Else
                    Me.statusPanel.Text = "Failed Opening Session."
                    Me.readText.Text = "Failed opening message based VISA session"
                End If

            End If

        Catch ex As Exception

            Me.statusPanel.Text = "Exception occurred opening session."
            Me.readText.Text = ex.ToString

        Finally

            Windows.Forms.Cursor.Current = Cursors.Default

        End Try
        Return

    End Sub

    ''' <summary>
    ''' Closed an open session.
    ''' </summary>
    Private Sub closeSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles closeSessionButton.Click
        Me.statusPanel.Text = "Closing Session."
        UpdateControlsState(False)
        Me._session.Dispose()
        Me.statusPanel.Text = "Session close."
    End Sub

    ''' <summary>
    ''' Queries (write and then reads) the instrument.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Private Sub query_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me.statusPanel.Text = "Querying."
            Dim message As String = writeText.Text
            My.Application.Log.WriteEntry("Querying: " & message, TraceEventType.Verbose)
            Dim textToWrite As String = isr.Core.StringEscapeSequencesExtensions.ReplaceCommonEscapeSequences(message)
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Dim responseString As String = Me._session.Query(textToWrite)
            Dim lastStatus As NationalInstruments.VisaNS.VisaStatusCode = Me._session.LastStatus
            If lastStatus <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                My.Application.Log.WriteEntry("Failure. Last Visa Status: " & lastStatus.ToString, TraceEventType.Warning)
            End If
            Me._timingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            message = isr.Core.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(responseString)
            My.Application.Log.WriteEntry("Received: " & message, TraceEventType.Verbose)
            readText.Text = message
            If Me._session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
                Me.readStatusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", Me._session.ReadStatusByte)
                Me.writeStatusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", Me._session.ReadStatusByte)
            Else
                Me.readStatusPanel.Text = String.Empty
                Me.writeStatusPanel.Text = String.Empty
            End If
            Me.statusPanel.Text = "Done Querying."
        Catch ex As Exception
            Me.statusPanel.Text = "Error Querying."
            Me.readText.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Writes a message to the instrument.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub write_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me.statusPanel.Text = "Writing."
            Dim message As String = writeText.Text
            My.Application.Log.WriteEntry("Writing: " & message)
            Dim textToWrite As String = isr.Core.StringEscapeSequencesExtensions.ReplaceCommonEscapeSequences(message)
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Me._session.Write(textToWrite)
            Me._timingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            If Me._session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
                Me.writeStatusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", Me._session.ReadStatusByte)
            Else
                Me.writeStatusPanel.Text = String.Empty
            End If
            Me.statusPanel.Text = "Done Writing."
        Catch ex As Exception
            Me.statusPanel.Text = "Error Writing."
            Me.readText.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Reads a message from the instrument.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub read_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me.statusPanel.Text = "Reading."
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Dim responseString As String = Me._session.ReadString()
            Me._timingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            Dim Message As String = isr.Core.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(responseString)
            My.Application.Log.WriteEntry("Received: " & Message)
            readText.Text = Message
            If Me._session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
                Me.readStatusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", Me._session.ReadStatusByte)
            Else
                Me.readStatusPanel.Text = String.Empty
            End If
            Me.statusPanel.Text = "Done Reading."
        Catch ex As Exception
            Me.statusPanel.Text = "Error Reading."
            Me.readText.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Clears the text boxes. 
    ''' </summary>
    Private Sub clear()
        readText.Text = String.Empty
        Me.readStatusPanel.Text = String.Empty
        Me.writeStatusPanel.Text = String.Empty
        Me.statusPanel.Text = String.Empty
    End Sub

    Private Sub clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
        clear()
    End Sub

    ''' <summary>
    ''' Updates the controls state.
    ''' </summary>
    ''' <param name="sessionOpen"></param>
    Private Sub UpdateControlsState(ByVal sessionOpen As Boolean)
        openSessionButton.Enabled = Not sessionOpen
        closeSessionButton.Enabled = sessionOpen
        queryButton.Enabled = sessionOpen
        writeButton.Enabled = sessionOpen
        readButton.Enabled = sessionOpen
        writeText.Enabled = sessionOpen
        clearButton.Enabled = sessionOpen
        If sessionOpen Then
            readText.Text = String.Empty
            writeText.Focus()
        End If
    End Sub

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

        Catch ex As Exception

            If My.MyApplication.ProcessException(ex, "", WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

End Class