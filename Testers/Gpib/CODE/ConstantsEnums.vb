
#Region " TYPES "

''' <summary>
''' Enumerates the color of the state shapes.
''' </summary>
Public Enum StateShapeColor
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Not Active")> Inactive = &HC0C0C0
    <System.ComponentModel.Description("Active")> Active = &H80FF80
    <System.ComponentModel.Description("Opened")> Opened = &HC0C0C0
    <System.ComponentModel.Description("Closed")> Closed = &H80FF80
End Enum

#End Region

