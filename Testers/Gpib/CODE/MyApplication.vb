Namespace My

    ''' <summary>
    ''' A singleton class to provide project management for this application.
    ''' </summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/09/2007" by="David" revision="1.15.2355.x">
    ''' Create from the K24xx User Control.
    ''' </history>
    Partial Friend Class MyApplication

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Destroys objects for this project.
        ''' </summary>
    Friend Shared Sub Destroy()

            ' terminate the reference to instances created by this class
            If Not MyApplication._splashScreen Is Nothing Then
                MyApplication._splashScreen = Nothing
            End If

            If Err.Number <> 0 Then
                ' prepend error source to allow calling application to pin point the
                ' source of the error.
                Err.Source = My.Application.Info.Title & ".My.MyApplication.Destroy, " & Err.Source
            End If

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method Overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
    Protected Property IsDisposed() As Boolean
            Get
                Return Me._isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                        MyApplication.Destroy()

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            Try
                ' Do not re-create Dispose clean-up code here.
                ' Calling Dispose(false) is optimal for readability and maintainability.
                Dispose(False)
            Finally
                ' The compiler automatically adds a call to the base class finalizer 
                ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
                MyBase.Finalize()
            End Try
        End Sub

#End Region

#Region " CONSTANTS "

        ''' <summary>
        ''' Gets the help base for the entire application.
        ''' </summary>
    Private Shared _applicationHelpBase As Integer = 0
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
          Justification:="Future use.")>
        Public Shared ReadOnly Property ApplicationHelpBase() As Integer
            Get
                Return MyApplication._applicationHelpBase
            End Get
        End Property

        ''' <summary>
        ''' Gets the error base for the entire application.
        ''' </summary>
    Private Shared _applicationErrorBase As Integer = ApplicationHelpBase + Microsoft.VisualBasic.vbObjectError + &H200
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
          Justification:="Future use.")>
        Public Shared ReadOnly Property ApplicationErrorBase() As Integer
            Get
                Return MyApplication._applicationErrorBase
            End Get
        End Property

        Private Shared _unknownValue As String = "N/A"
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
          Justification:="Future use.")>
        Public Shared ReadOnly Property UnknownValue() As String
            Get
                Return MyApplication._unknownValue
            End Get
        End Property

        Private Shared _unknownFileNameValue As String = "NA"
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
          Justification:="Future use.")>
        Public Shared ReadOnly Property UnknownFileNameValue() As String
            Get
                Return MyApplication._unknownFileNameValue
            End Get
        End Property

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the
        ''' <see cref=" isr.WindowsForms.SplashScreen">splash screen class</see>.</summary>
        Private Shared _splashScreen As isr.WindowsForms.SplashScreen

        ''' <summary>Displays a splash screen message instantiating and displaying
        '''   the splash screen for the first message.
        ''' </summary>
    Public Shared WriteOnly Property SplashScreenMessage() As String
            Set(ByVal Value As String)

                Dim propertySetter As isr.Core.ThreadSafePropertySetter
                If MyApplication._splashScreen Is Nothing Then

                    If My.Application.SplashScreen Is Nothing Then
                        My.Application.SplashScreen = New SplashScreen1
                        My.Application.ShowSplashScreen()
                    End If
                    MyApplication._splashScreen = CType(My.Application.SplashScreen, isr.WindowsForms.SplashScreen)
                    propertySetter = New isr.Core.ThreadSafePropertySetter(My.Application.SplashScreen)
                    If Not MyApplication._splashScreen.Visible Then
                        MyApplication._splashScreen.Show()
                        MyApplication._splashScreen.Refresh()
                    End If
                    propertySetter.SetCtrlProperty(MyApplication._splashScreen, "TopMost", Not My.MyApplication.InDesignMode)

                    ' update the licensee name if necessary.
                    propertySetter.SetCtrlProperty(MyApplication._splashScreen, "LicenseeName", "Integrated Scientific Resources, Inc.")

                ElseIf Not MyApplication._splashScreen.Visible Then

                    MyApplication._splashScreen.Show()
                    MyApplication._splashScreen.Refresh()

                End If

                ' display the message
                propertySetter = New isr.Core.ThreadSafePropertySetter(My.Application.SplashScreen)
                propertySetter.SetCtrlProperty(MyApplication._splashScreen, "Status", Value)

            End Set
        End Property

#End Region

#Region " METHODS "

        ''' <summary>Creates objects for this project.
        ''' </summary>
    ''' <returns>True if ok.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
        Friend Shared Function CreateObjects(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Dim lastAction As String = String.Empty
            Dim className As String = String.Empty
            Dim passed As Boolean

            Try

                ' show status
                lastAction = "Application is initializing..."
                MyApplication.SplashScreenMessage = lastAction

                lastAction = "Parsing the command line..."
                MyApplication.SplashScreenMessage = lastAction
                MyApplication.parseCommandLine(String.Join(" ", commandLineArgs.ToArray))

                passed = True

                ' return true
                Return passed

            Catch ex As Exception

                ' set error description
                If Not String.IsNullOrWhiteSpace(className) Then

                    Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                            "Failed {0}. Class name='{1}'", lastAction, className)
                    Return ProcessException(ex, message, WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

                Else

                    Throw

                End If

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                If Not passed Then
                    Try
                        MyApplication.Destroy()
                    Finally
                    End Try
                End If

            End Try

        End Function

        ''' <summary>Parses the command line adding the default options.
        ''' </summary>
    ''' <param name="commandLine">Specifies the command line.
        ''' </param>
        ''' <history>
        ''' </history>
        Public Shared Sub ParseCommandLine(ByVal commandLine As String)

            ' save the command line
            ' Dim parser As New isr.Support.CommandLineParser(commandLine)

        End Sub

#End Region

#Region " METHODS: MESSAGE MANAGERS "

        ''' <summary>Builds a message for the message log.
        ''' </summary>
    ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Function BuildMessage(ByVal message As String) As String

            Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                               "{0:HH:mm:ss.fff} {1}{2}", DateTime.Now, message, Environment.NewLine)

        End Function

        ''' <summary>Appends a message to the message log.
        '''   This must be defined within the application calling the
        '''   methods as the TextBox is seen as private shared if called from an
        '''   external application.
        ''' </summary>
    ''' <param name="textBox">Specifies a reference to the message box which to use.</param>
        ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Sub AppendMessage(ByVal textBox As System.Windows.Forms.TextBox, ByVal message As String)

            textBox.SelectionStart = textBox.Text.Length
            textBox.SelectionLength = 0
            If textBox.SelectionStart = 0 Then
                textBox.SelectedText = My.MyApplication.BuildMessage(message)
            Else
                textBox.SelectedText = Environment.NewLine & My.MyApplication.BuildMessage(message)
            End If
            textBox.SelectionStart = textBox.Text.Length

        End Sub

        ''' <summary>Appends a message to the message log.
        '''   This must be defined within the application calling the
        '''   methods as the TextBox is seen as private if called from an
        '''   external application.
        ''' </summary>
    ''' <param name="textBox">Specifies a reference to the message box which to use.</param>
        ''' <param name="message">Specifies the message to append.</param>
        Friend Shared Sub PrependMessage(ByVal textBox As System.Windows.Forms.TextBox, ByVal message As String)

            textBox.SelectionStart = 0
            textBox.SelectionLength = 0
            textBox.SelectedText = My.MyApplication.BuildMessage(message) & Environment.NewLine
            textBox.SelectionLength = 0

        End Sub

#End Region

    End Class

End Namespace

