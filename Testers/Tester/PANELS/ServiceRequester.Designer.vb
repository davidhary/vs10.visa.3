﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ServiceRequester
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.readingGroupBox = New System.Windows.Forms.GroupBox
        Me.clearButton = New System.Windows.Forms.Button
        Me.readTextBox = New System.Windows.Forms.TextBox
        Me.selectResourceLabel = New System.Windows.Forms.Label
        Me._EnableSrqButton = New System.Windows.Forms.Button
        Me.writeTextBox = New System.Windows.Forms.TextBox
        Me.commandTextBox = New System.Windows.Forms.TextBox
        Me.commandLabel = New System.Windows.Forms.Label
        Me.resetButton = New System.Windows.Forms.Button
        Me.writeButton = New System.Windows.Forms.Button
        Me.writingGroupBox = New System.Windows.Forms.GroupBox
        Me.configuringGroupBox = New System.Windows.Forms.GroupBox
        Me._syncCallBacksCheckBox = New System.Windows.Forms.CheckBox
        Me.deviceConfigureButton = New System.Windows.Forms.Button
        Me.resourceNameLabel = New System.Windows.Forms.Label
        Me.resourceNameTextBox = New System.Windows.Forms.TextBox
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._timingUnitsLabel = New System.Windows.Forms.Label
        Me._timingTextBox = New System.Windows.Forms.TextBox
        Me.readingGroupBox.SuspendLayout()
        Me.writingGroupBox.SuspendLayout()
        Me.configuringGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'readingGroupBox
        '
        Me.readingGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                            System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readingGroupBox.Controls.Add(Me._timingUnitsLabel)
        Me.readingGroupBox.Controls.Add(Me._timingTextBox)
        Me.readingGroupBox.Controls.Add(Me.clearButton)
        Me.readingGroupBox.Controls.Add(Me.readTextBox)
        Me.readingGroupBox.Location = New System.Drawing.Point(8, 314)
        Me.readingGroupBox.Name = "readingGroupBox"
        Me.readingGroupBox.Size = New System.Drawing.Size(272, 120)
        Me.readingGroupBox.TabIndex = 8
        Me.readingGroupBox.TabStop = False
        Me.readingGroupBox.Text = "Reading"
        '
        'clearButton
        '
        Me.clearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.clearButton.Location = New System.Drawing.Point(8, 88)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(104, 24)
        Me.clearButton.TabIndex = 1
        Me.clearButton.Text = "Clear"
        '
        'readTextBox
        '
        Me.readTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                        System.Windows.Forms.AnchorStyles.Left) Or
                                    System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readTextBox.Location = New System.Drawing.Point(8, 24)
        Me.readTextBox.Multiline = True
        Me.readTextBox.Name = "readTextBox"
        Me.readTextBox.ReadOnly = True
        Me.readTextBox.Size = New System.Drawing.Size(256, 56)
        Me.readTextBox.TabIndex = 0
        Me.readTextBox.Text = "If not working, an SDC or *RST or *CLS are required line in the GPIB Test Panel. Run the GPIB Test panel and try again."
        '
        'selectResourceLabel
        '
        Me.selectResourceLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.selectResourceLabel.Location = New System.Drawing.Point(8, 10)
        Me.selectResourceLabel.Name = "selectResourceLabel"
        Me.selectResourceLabel.Size = New System.Drawing.Size(272, 56)
        Me.selectResourceLabel.TabIndex = 5
        Me.selectResourceLabel.Text = "Select the Resource Name associated with your device and press the" &
            " Configure Device button. Then enter the command string that enables SRQ and click the Enable SRQ button."
        '
        '_EnableSrqButton
        '
        Me._EnableSrqButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._EnableSrqButton.Location = New System.Drawing.Point(160, 134)
        Me._EnableSrqButton.Name = "_EnableSrqButton"
        Me._EnableSrqButton.Size = New System.Drawing.Size(104, 24)
        Me._EnableSrqButton.TabIndex = 7
        Me._EnableSrqButton.Text = "Enable SRQ"
        '
        'writeTextBox
        '
        Me.writeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeTextBox.Location = New System.Drawing.Point(8, 24)
        Me.writeTextBox.Name = "writeTextBox"
        Me.writeTextBox.Size = New System.Drawing.Size(152, 20)
        Me.writeTextBox.TabIndex = 0
        Me.writeTextBox.Text = "*IDN?\n"
        '
        'commandTextBox
        '
        Me.commandTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                          System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandTextBox.Location = New System.Drawing.Point(8, 136)
        Me.commandTextBox.Name = "commandTextBox"
        Me.commandTextBox.Size = New System.Drawing.Size(152, 20)
        Me.commandTextBox.TabIndex = 6
        Me.commandTextBox.Text = "*SRE 16"
        '
        'commandLabel
        '
        Me.commandLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandLabel.Location = New System.Drawing.Point(8, 96)
        Me.commandLabel.Name = "commandLabel"
        Me.commandLabel.Size = New System.Drawing.Size(256, 32)
        Me.commandLabel.TabIndex = 5
        Me.commandLabel.Text = "Type the command to enable the instrument's SRQ event on MAV:"
        '
        'resetButton
        '
        Me.resetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resetButton.Location = New System.Drawing.Point(160, 64)
        Me.resetButton.Name = "resetButton"
        Me.resetButton.Size = New System.Drawing.Size(104, 24)
        Me.resetButton.TabIndex = 4
        Me.resetButton.Text = "Reset"
        '
        'writeButton
        '
        Me.writeButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeButton.Location = New System.Drawing.Point(160, 23)
        Me.writeButton.Name = "writeButton"
        Me.writeButton.Size = New System.Drawing.Size(104, 23)
        Me.writeButton.TabIndex = 1
        Me.writeButton.Text = "Write"
        '
        'writingGroupBox
        '
        Me.writingGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                           System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writingGroupBox.Controls.Add(Me.writeButton)
        Me.writingGroupBox.Controls.Add(Me.writeTextBox)
        Me.writingGroupBox.Location = New System.Drawing.Point(8, 242)
        Me.writingGroupBox.Name = "writingGroupBox"
        Me.writingGroupBox.Size = New System.Drawing.Size(272, 56)
        Me.writingGroupBox.TabIndex = 7
        Me.writingGroupBox.TabStop = False
        Me.writingGroupBox.Text = "Writing"
        '
        'configuringGroupBox
        '
        Me.configuringGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.configuringGroupBox.Controls.Add(Me._syncCallBacksCheckBox)
        Me.configuringGroupBox.Controls.Add(Me._EnableSrqButton)
        Me.configuringGroupBox.Controls.Add(Me.commandTextBox)
        Me.configuringGroupBox.Controls.Add(Me.commandLabel)
        Me.configuringGroupBox.Controls.Add(Me.resetButton)
        Me.configuringGroupBox.Controls.Add(Me.deviceConfigureButton)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameLabel)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameTextBox)
        Me.configuringGroupBox.Location = New System.Drawing.Point(8, 66)
        Me.configuringGroupBox.Name = "configuringGroupBox"
        Me.configuringGroupBox.Size = New System.Drawing.Size(272, 168)
        Me.configuringGroupBox.TabIndex = 6
        Me.configuringGroupBox.TabStop = False
        Me.configuringGroupBox.Text = "Configuring"
        '
        '_syncCallBacksCheckBox
        '
        Me._syncCallBacksCheckBox.AutoSize = True
        Me._syncCallBacksCheckBox.Location = New System.Drawing.Point(158, 12)
        Me._syncCallBacksCheckBox.Name = "_syncCallBacksCheckBox"
        Me._syncCallBacksCheckBox.Size = New System.Drawing.Size(103, 17)
        Me._syncCallBacksCheckBox.TabIndex = 8
        Me._syncCallBacksCheckBox.Text = "Sync Call Backs"
        Me.toolTip.SetToolTip(Me._syncCallBacksCheckBox, "Defaults to true with a new session.")
        Me._syncCallBacksCheckBox.UseVisualStyleBackColor = True
        '
        'deviceConfigureButton
        '
        Me.deviceConfigureButton.Location = New System.Drawing.Point(8, 64)
        Me.deviceConfigureButton.Name = "deviceConfigureButton"
        Me.deviceConfigureButton.Size = New System.Drawing.Size(104, 24)
        Me.deviceConfigureButton.TabIndex = 3
        Me.deviceConfigureButton.Text = "Configure Device"
        '
        'resourceNameLabel
        '
        Me.resourceNameLabel.Location = New System.Drawing.Point(8, 16)
        Me.resourceNameLabel.Name = "resourceNameLabel"
        Me.resourceNameLabel.Size = New System.Drawing.Size(112, 16)
        Me.resourceNameLabel.TabIndex = 2
        Me.resourceNameLabel.Text = "Resource Name:"
        '
        'resourceNameTextBox
        '
        Me.resourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceNameTextBox.Location = New System.Drawing.Point(8, 32)
        Me.resourceNameTextBox.Name = "resourceNameTextBox"
        Me.resourceNameTextBox.Size = New System.Drawing.Size(256, 20)
        Me.resourceNameTextBox.TabIndex = 1
        Me.resourceNameTextBox.Text = "GPIB0::26::INSTR"
        '
        '_timingUnitsLabel
        '
        Me._timingUnitsLabel.AutoSize = True
        Me._timingUnitsLabel.Location = New System.Drawing.Point(241, 93)
        Me._timingUnitsLabel.Name = "_timingUnitsLabel"
        Me._timingUnitsLabel.Size = New System.Drawing.Size(20, 13)
        Me._timingUnitsLabel.TabIndex = 25
        Me._timingUnitsLabel.Text = "ms"
        '
        '_timingTextBox
        '
        Me._timingTextBox.Location = New System.Drawing.Point(174, 89)
        Me._timingTextBox.Name = "_timingTextBox"
        Me._timingTextBox.Size = New System.Drawing.Size(63, 20)
        Me._timingTextBox.TabIndex = 24
        '
        'ServiceRequester
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(288, 448)
        Me.Controls.Add(Me.readingGroupBox)
        Me.Controls.Add(Me.selectResourceLabel)
        Me.Controls.Add(Me.writingGroupBox)
        Me.Controls.Add(Me.configuringGroupBox)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(296, 482)
        Me.Name = "ServiceRequester"
        Me.Text = "Service Request Tester"
        Me.readingGroupBox.ResumeLayout(False)
        Me.readingGroupBox.PerformLayout()
        Me.writingGroupBox.ResumeLayout(False)
        Me.writingGroupBox.PerformLayout()
        Me.configuringGroupBox.ResumeLayout(False)
        Me.configuringGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents readingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents clearButton As System.Windows.Forms.Button
    Private WithEvents readTextBox As System.Windows.Forms.TextBox
    Private WithEvents selectResourceLabel As System.Windows.Forms.Label
    Private WithEvents _EnableSrqButton As System.Windows.Forms.Button
    Private WithEvents writeTextBox As System.Windows.Forms.TextBox
    Private WithEvents commandTextBox As System.Windows.Forms.TextBox
    Private WithEvents commandLabel As System.Windows.Forms.Label
    Private WithEvents resetButton As System.Windows.Forms.Button
    Private WithEvents writeButton As System.Windows.Forms.Button
    Private WithEvents writingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents configuringGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents deviceConfigureButton As System.Windows.Forms.Button
    Private WithEvents resourceNameLabel As System.Windows.Forms.Label
    Private WithEvents resourceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _syncCallBacksCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _timingUnitsLabel As System.Windows.Forms.Label
    Private WithEvents _timingTextBox As System.Windows.Forms.TextBox
End Class
