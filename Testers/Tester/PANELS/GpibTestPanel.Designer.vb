<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpibTestPanel
#Region "Windows Form Designer generated code "
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then

                ' Free managed resources when explicitly called
                Me.onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources
            'onDisposeUnManagedResources()

        Finally

            ' Invoke the base class dispose method

            MyBase.Dispose(disposing)

        End Try
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents commandsComboLabel As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents _sendButton As System.Windows.Forms.Button
    Public WithEvents _receiveButton As System.Windows.Forms.Button
    Public WithEvents _readStatusRegisterButton As System.Windows.Forms.Button
    Public WithEvents commandsCombo As System.Windows.Forms.ComboBox
    Public WithEvents sendComboCommandButton As System.Windows.Forms.Button
    Public WithEvents _GpibTabPage As System.Windows.Forms.TabPage
    Public WithEvents messagesTextBox As System.Windows.Forms.TextBox
    Public WithEvents _messagesTabPage As System.Windows.Forms.TabPage
    Public WithEvents mainTabs As System.Windows.Forms.TabControl
    Public WithEvents _statusStatusBarPanel As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents _statusBar As System.Windows.Forms.StatusStrip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GpibTestPanel))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me._sendButton = New System.Windows.Forms.Button
        Me._receiveButton = New System.Windows.Forms.Button
        Me._readStatusRegisterButton = New System.Windows.Forms.Button
        Me.sendComboCommandButton = New System.Windows.Forms.Button
        Me._clearInterfaceButton = New System.Windows.Forms.Button
        Me.GpibAddressTextBox = New System.Windows.Forms.TextBox
        Me._connectToggle = New System.Windows.Forms.CheckBox
        Me._clearDeviceButton = New System.Windows.Forms.Button
        Me._pollRadioButton = New System.Windows.Forms.RadioButton
        Me._serviceRequestReceiveOptionRadioButton = New System.Windows.Forms.RadioButton
        Me.mainTabs = New System.Windows.Forms.TabControl
        Me._connectTabPage = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me._settingsGroupBox = New System.Windows.Forms.GroupBox
        Me.DisconnectCommandsTextBox = New System.Windows.Forms.TextBox
        Me.sendDisconnectCommandsCheckBox = New System.Windows.Forms.CheckBox
        Me._connectGroupBox = New System.Windows.Forms.GroupBox
        Me._PrimaryAddressLabel = New System.Windows.Forms.Label
        Me._GpibTabPage = New System.Windows.Forms.TabPage
        Me._sendReceiveLayout = New System.Windows.Forms.TableLayoutPanel
        Me._sendReceiveControlPanel = New System.Windows.Forms.Panel
        Me._receiveOptionsGroupBox = New System.Windows.Forms.GroupBox
        Me._pollIntervalUnitsLabel = New System.Windows.Forms.Label
        Me._pollIntervalNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me._readManualRadioButton = New System.Windows.Forms.RadioButton
        Me.commandsCombo = New System.Windows.Forms.ComboBox
        Me.commandsComboLabel = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me._sendReceiveSplitContainer = New System.Windows.Forms.SplitContainer
        Me._sendGroupBox = New System.Windows.Forms.GroupBox
        Me.inputTextBox = New System.Windows.Forms.TextBox
        Me._receiveGroupBox = New System.Windows.Forms.GroupBox
        Me.outputTextBox = New System.Windows.Forms.TextBox
        Me._messagesTabPage = New System.Windows.Forms.TabPage
        Me.messagesTextBox = New System.Windows.Forms.TextBox
        Me._statusBar = New System.Windows.Forms.StatusStrip
        Me._statusStatusBarPanel = New System.Windows.Forms.ToolStripStatusLabel
        Me._statusRegisterLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.mainTabs.SuspendLayout()
        Me._connectTabPage.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me._settingsGroupBox.SuspendLayout()
        Me._connectGroupBox.SuspendLayout()
        Me._GpibTabPage.SuspendLayout()
        Me._sendReceiveLayout.SuspendLayout()
        Me._sendReceiveControlPanel.SuspendLayout()
        Me._receiveOptionsGroupBox.SuspendLayout()
        CType(Me._pollIntervalNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._sendReceiveSplitContainer.Panel1.SuspendLayout()
        Me._sendReceiveSplitContainer.Panel2.SuspendLayout()
        Me._sendReceiveSplitContainer.SuspendLayout()
        Me._sendGroupBox.SuspendLayout()
        Me._receiveGroupBox.SuspendLayout()
        Me._messagesTabPage.SuspendLayout()
        Me._statusBar.SuspendLayout()
        Me.SuspendLayout()
        '
        '_sendButton
        '
        Me._sendButton.BackColor = System.Drawing.SystemColors.Control
        Me._sendButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._sendButton.Enabled = False
        Me._sendButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._sendButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._sendButton.Location = New System.Drawing.Point(13, 10)
        Me._sendButton.Name = "_sendButton"
        Me._sendButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._sendButton.Size = New System.Drawing.Size(145, 29)
        Me._sendButton.TabIndex = 11
        Me._sendButton.Text = "&SEND TEXT"
        Me.ToolTip1.SetToolTip(Me._sendButton, "Sends")
        Me._sendButton.UseVisualStyleBackColor = True
        '
        '_receiveButton
        '
        Me._receiveButton.BackColor = System.Drawing.SystemColors.Control
        Me._receiveButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._receiveButton.Enabled = False
        Me._receiveButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._receiveButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._receiveButton.Location = New System.Drawing.Point(164, 10)
        Me._receiveButton.Name = "_receiveButton"
        Me._receiveButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._receiveButton.Size = New System.Drawing.Size(125, 29)
        Me._receiveButton.TabIndex = 14
        Me._receiveButton.Text = "&RECEIVE"
        Me.ToolTip1.SetToolTip(Me._receiveButton, "Receives data.  Enabled after reading SRQ.")
        Me._receiveButton.UseVisualStyleBackColor = True
        '
        '_readStatusRegisterButton
        '
        Me._readStatusRegisterButton.BackColor = System.Drawing.SystemColors.Control
        Me._readStatusRegisterButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._readStatusRegisterButton.Enabled = False
        Me._readStatusRegisterButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._readStatusRegisterButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._readStatusRegisterButton.Location = New System.Drawing.Point(299, 10)
        Me._readStatusRegisterButton.Name = "_readStatusRegisterButton"
        Me._readStatusRegisterButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._readStatusRegisterButton.Size = New System.Drawing.Size(190, 29)
        Me._readStatusRegisterButton.TabIndex = 16
        Me._readStatusRegisterButton.Text = "READ SR&Q (enable receive)"
        Me.ToolTip1.SetToolTip(Me._readStatusRegisterButton, "Reads SRQ and enables receive")
        Me._readStatusRegisterButton.UseVisualStyleBackColor = True
        '
        'sendComboCommandButton
        '
        Me.sendComboCommandButton.BackColor = System.Drawing.SystemColors.Control
        Me.sendComboCommandButton.Cursor = System.Windows.Forms.Cursors.Default
        Me.sendComboCommandButton.Enabled = False
        Me.sendComboCommandButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sendComboCommandButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.sendComboCommandButton.Location = New System.Drawing.Point(245, 66)
        Me.sendComboCommandButton.Name = "sendComboCommandButton"
        Me.sendComboCommandButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.sendComboCommandButton.Size = New System.Drawing.Size(145, 29)
        Me.sendComboCommandButton.TabIndex = 18
        Me.sendComboCommandButton.Text = "&SEND COMMAND"
        Me.ToolTip1.SetToolTip(Me.sendComboCommandButton, "Sends a single command")
        Me.sendComboCommandButton.UseVisualStyleBackColor = True
        '
        '_clearInterfaceButton
        '
        Me._clearInterfaceButton.BackColor = System.Drawing.SystemColors.Control
        Me._clearInterfaceButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._clearInterfaceButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._clearInterfaceButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._clearInterfaceButton.Location = New System.Drawing.Point(18, 36)
        Me._clearInterfaceButton.Name = "_clearInterfaceButton"
        Me._clearInterfaceButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._clearInterfaceButton.Size = New System.Drawing.Size(145, 29)
        Me._clearInterfaceButton.TabIndex = 21
        Me._clearInterfaceButton.Text = "CLEAR &INTERFACE"
        Me.ToolTip1.SetToolTip(Me._clearInterfaceButton, "Clears the GPIB interface")
        Me._clearInterfaceButton.UseVisualStyleBackColor = True
        '
        'GpibAddressTextBox
        '
        Me.GpibAddressTextBox.AcceptsReturn = True
        Me.GpibAddressTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.GpibAddressTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.GpibAddressTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GpibAddressTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GpibAddressTextBox.Location = New System.Drawing.Point(399, 40)
        Me.GpibAddressTextBox.MaxLength = 0
        Me.GpibAddressTextBox.Name = "GpibAddressTextBox"
        Me.GpibAddressTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GpibAddressTextBox.Size = New System.Drawing.Size(37, 20)
        Me.GpibAddressTextBox.TabIndex = 22
        Me.ToolTip1.SetToolTip(Me.GpibAddressTextBox, "Primary address of GPIB Instrument")
        '
        '_connectToggle
        '
        Me._connectToggle.Appearance = System.Windows.Forms.Appearance.Button
        Me._connectToggle.BackColor = System.Drawing.SystemColors.Control
        Me._connectToggle.Cursor = System.Windows.Forms.Cursors.Default
        Me._connectToggle.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._connectToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._connectToggle.Location = New System.Drawing.Point(166, 36)
        Me._connectToggle.Name = "_connectToggle"
        Me._connectToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._connectToggle.Size = New System.Drawing.Size(101, 29)
        Me._connectToggle.TabIndex = 23
        Me._connectToggle.Text = "&CONNECT"
        Me._connectToggle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me._connectToggle, "Connects the instrument")
        Me._connectToggle.UseVisualStyleBackColor = True
        '
        '_clearDeviceButton
        '
        Me._clearDeviceButton.BackColor = System.Drawing.SystemColors.Control
        Me._clearDeviceButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._clearDeviceButton.Enabled = False
        Me._clearDeviceButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._clearDeviceButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._clearDeviceButton.Location = New System.Drawing.Point(443, 36)
        Me._clearDeviceButton.Name = "_clearDeviceButton"
        Me._clearDeviceButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._clearDeviceButton.Size = New System.Drawing.Size(89, 29)
        Me._clearDeviceButton.TabIndex = 25
        Me._clearDeviceButton.Text = "SDC"
        Me.ToolTip1.SetToolTip(Me._clearDeviceButton, "Clears the device.")
        Me._clearDeviceButton.UseVisualStyleBackColor = True
        '
        '_pollRadioButton
        '
        Me._pollRadioButton.AutoSize = True
        Me._pollRadioButton.Location = New System.Drawing.Point(12, 41)
        Me._pollRadioButton.Name = "_pollRadioButton"
        Me._pollRadioButton.Size = New System.Drawing.Size(51, 18)
        Me._pollRadioButton.TabIndex = 1
        Me._pollRadioButton.Text = "POLL"
        Me.ToolTip1.SetToolTip(Me._pollRadioButton, "Uses a timer to poll the service request register and receive a buffer whenever the message available bit is set.")
        Me._pollRadioButton.UseVisualStyleBackColor = True
        '
        '_serviceRequestReceiveOptionRadioButton
        '
        Me._serviceRequestReceiveOptionRadioButton.AutoSize = True
        Me._serviceRequestReceiveOptionRadioButton.Location = New System.Drawing.Point(12, 62)
        Me._serviceRequestReceiveOptionRadioButton.Name = "_serviceRequestReceiveOptionRadioButton"
        Me._serviceRequestReceiveOptionRadioButton.Size = New System.Drawing.Size(58, 18)
        Me._serviceRequestReceiveOptionRadioButton.TabIndex = 2
        Me._serviceRequestReceiveOptionRadioButton.Text = "EVENT"
        Me.ToolTip1.SetToolTip(Me._serviceRequestReceiveOptionRadioButton, "Use the service request event on measurement available to receive the buffer.")
        Me._serviceRequestReceiveOptionRadioButton.UseVisualStyleBackColor = True
        '
        'mainTabs
        '
        Me.mainTabs.Controls.Add(Me._connectTabPage)
        Me.mainTabs.Controls.Add(Me._GpibTabPage)
        Me.mainTabs.Controls.Add(Me._messagesTabPage)
        Me.mainTabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainTabs.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mainTabs.ItemSize = New System.Drawing.Size(42, 18)
        Me.mainTabs.Location = New System.Drawing.Point(0, 0)
        Me.mainTabs.Name = "mainTabs"
        Me.mainTabs.SelectedIndex = 0
        Me.mainTabs.Size = New System.Drawing.Size(750, 439)
        Me.mainTabs.TabIndex = 1
        '
        '_connectTabPage
        '
        Me._connectTabPage.Controls.Add(Me.TableLayoutPanel1)
        Me._connectTabPage.Location = New System.Drawing.Point(4, 22)
        Me._connectTabPage.Name = "_connectTabPage"
        Me._connectTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._connectTabPage.Size = New System.Drawing.Size(742, 413)
        Me._connectTabPage.TabIndex = 3
        Me._connectTabPage.Text = "CONNECT"
        Me._connectTabPage.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me._settingsGroupBox, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me._connectGroupBox, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(736, 407)
        Me.TableLayoutPanel1.TabIndex = 26
        '
        '_settingsGroupBox
        '
        Me._settingsGroupBox.Controls.Add(Me.DisconnectCommandsTextBox)
        Me._settingsGroupBox.Controls.Add(Me.sendDisconnectCommandsCheckBox)
        Me._settingsGroupBox.Location = New System.Drawing.Point(91, 188)
        Me._settingsGroupBox.Name = "_settingsGroupBox"
        Me._settingsGroupBox.Size = New System.Drawing.Size(554, 129)
        Me._settingsGroupBox.TabIndex = 29
        Me._settingsGroupBox.TabStop = False
        Me._settingsGroupBox.Text = "SETTINGS: "
        '
        'DisconnectCommandsTextBox
        '
        Me.DisconnectCommandsTextBox.AcceptsReturn = True
        Me.DisconnectCommandsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.DisconnectCommandsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DisconnectCommandsTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DisconnectCommandsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.DisconnectCommandsTextBox.Location = New System.Drawing.Point(18, 45)
        Me.DisconnectCommandsTextBox.MaxLength = 0
        Me.DisconnectCommandsTextBox.Multiline = True
        Me.DisconnectCommandsTextBox.Name = "DisconnectCommandsTextBox"
        Me.DisconnectCommandsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DisconnectCommandsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DisconnectCommandsTextBox.Size = New System.Drawing.Size(188, 72)
        Me.DisconnectCommandsTextBox.TabIndex = 28
        Me.DisconnectCommandsTextBox.Text = "local node prompts = 0"
        '
        'sendDisconnectCommandsCheckBox
        '
        Me.sendDisconnectCommandsCheckBox.AutoSize = True
        Me.sendDisconnectCommandsCheckBox.BackColor = System.Drawing.Color.Transparent
        Me.sendDisconnectCommandsCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.sendDisconnectCommandsCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sendDisconnectCommandsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.sendDisconnectCommandsCheckBox.Location = New System.Drawing.Point(18, 23)
        Me.sendDisconnectCommandsCheckBox.Name = "sendDisconnectCommandsCheckBox"
        Me.sendDisconnectCommandsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.sendDisconnectCommandsCheckBox.Size = New System.Drawing.Size(184, 18)
        Me.sendDisconnectCommandsCheckBox.TabIndex = 27
        Me.sendDisconnectCommandsCheckBox.Text = "Send Commands On Disconnect:"
        Me.sendDisconnectCommandsCheckBox.UseVisualStyleBackColor = False
        '
        '_connectGroupBox
        '
        Me._connectGroupBox.Controls.Add(Me._clearInterfaceButton)
        Me._connectGroupBox.Controls.Add(Me._PrimaryAddressLabel)
        Me._connectGroupBox.Controls.Add(Me._clearDeviceButton)
        Me._connectGroupBox.Controls.Add(Me.GpibAddressTextBox)
        Me._connectGroupBox.Controls.Add(Me._connectToggle)
        Me._connectGroupBox.Location = New System.Drawing.Point(91, 90)
        Me._connectGroupBox.Name = "_connectGroupBox"
        Me._connectGroupBox.Size = New System.Drawing.Size(554, 92)
        Me._connectGroupBox.TabIndex = 27
        Me._connectGroupBox.TabStop = False
        Me._connectGroupBox.Text = "CONNECT:"
        '
        '_PrimaryAddressLabel
        '
        Me._PrimaryAddressLabel.BackColor = System.Drawing.Color.Transparent
        Me._PrimaryAddressLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PrimaryAddressLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PrimaryAddressLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PrimaryAddressLabel.Location = New System.Drawing.Point(280, 40)
        Me._PrimaryAddressLabel.Name = "_PrimaryAddressLabel"
        Me._PrimaryAddressLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PrimaryAddressLabel.Size = New System.Drawing.Size(116, 21)
        Me._PrimaryAddressLabel.TabIndex = 24
        Me._PrimaryAddressLabel.Text = "Primary Address:"
        Me._PrimaryAddressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_GpibTabPage
        '
        Me._GpibTabPage.Controls.Add(Me._sendReceiveLayout)
        Me._GpibTabPage.Location = New System.Drawing.Point(4, 22)
        Me._GpibTabPage.Name = "_GpibTabPage"
        Me._GpibTabPage.Size = New System.Drawing.Size(742, 413)
        Me._GpibTabPage.TabIndex = 0
        Me._GpibTabPage.Text = "SEND/RECEIVE"
        Me._GpibTabPage.UseVisualStyleBackColor = True
        '
        '_sendReceiveLayout
        '
        Me._sendReceiveLayout.ColumnCount = 3
        Me._sendReceiveLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._sendReceiveLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._sendReceiveLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._sendReceiveLayout.Controls.Add(Me._sendReceiveControlPanel, 1, 2)
        Me._sendReceiveLayout.Controls.Add(Me._sendReceiveSplitContainer, 1, 1)
        Me._sendReceiveLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._sendReceiveLayout.Location = New System.Drawing.Point(0, 0)
        Me._sendReceiveLayout.Name = "_sendReceiveLayout"
        Me._sendReceiveLayout.RowCount = 4
        Me._sendReceiveLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4.0!))
        Me._sendReceiveLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._sendReceiveLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._sendReceiveLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4.0!))
        Me._sendReceiveLayout.Size = New System.Drawing.Size(742, 413)
        Me._sendReceiveLayout.TabIndex = 27
        '
        '_sendReceiveControlPanel
        '
        Me._sendReceiveControlPanel.Controls.Add(Me._sendButton)
        Me._sendReceiveControlPanel.Controls.Add(Me.sendComboCommandButton)
        Me._sendReceiveControlPanel.Controls.Add(Me._receiveOptionsGroupBox)
        Me._sendReceiveControlPanel.Controls.Add(Me.commandsCombo)
        Me._sendReceiveControlPanel.Controls.Add(Me.commandsComboLabel)
        Me._sendReceiveControlPanel.Controls.Add(Me._readStatusRegisterButton)
        Me._sendReceiveControlPanel.Controls.Add(Me.Label1)
        Me._sendReceiveControlPanel.Controls.Add(Me._receiveButton)
        Me._sendReceiveControlPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._sendReceiveControlPanel.Enabled = False
        Me._sendReceiveControlPanel.Location = New System.Drawing.Point(13, 302)
        Me._sendReceiveControlPanel.Name = "_sendReceiveControlPanel"
        Me._sendReceiveControlPanel.Size = New System.Drawing.Size(716, 104)
        Me._sendReceiveControlPanel.TabIndex = 28
        '
        '_receiveOptionsGroupBox
        '
        Me._receiveOptionsGroupBox.Controls.Add(Me._pollIntervalUnitsLabel)
        Me._receiveOptionsGroupBox.Controls.Add(Me._pollIntervalNumericUpDown)
        Me._receiveOptionsGroupBox.Controls.Add(Me._serviceRequestReceiveOptionRadioButton)
        Me._receiveOptionsGroupBox.Controls.Add(Me._pollRadioButton)
        Me._receiveOptionsGroupBox.Controls.Add(Me._readManualRadioButton)
        Me._receiveOptionsGroupBox.Location = New System.Drawing.Point(537, 14)
        Me._receiveOptionsGroupBox.Name = "_receiveOptionsGroupBox"
        Me._receiveOptionsGroupBox.Size = New System.Drawing.Size(166, 87)
        Me._receiveOptionsGroupBox.TabIndex = 25
        Me._receiveOptionsGroupBox.TabStop = False
        Me._receiveOptionsGroupBox.Text = "RECEIVE OPTIONS:"
        '
        '_pollIntervalUnitsLabel
        '
        Me._pollIntervalUnitsLabel.AutoSize = True
        Me._pollIntervalUnitsLabel.Location = New System.Drawing.Point(137, 43)
        Me._pollIntervalUnitsLabel.Name = "_pollIntervalUnitsLabel"
        Me._pollIntervalUnitsLabel.Size = New System.Drawing.Size(21, 14)
        Me._pollIntervalUnitsLabel.TabIndex = 4
        Me._pollIntervalUnitsLabel.Text = "ms"
        '
        '_pollIntervalNumericUpDown
        '
        Me._pollIntervalNumericUpDown.Location = New System.Drawing.Point(90, 40)
        Me._pollIntervalNumericUpDown.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me._pollIntervalNumericUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._pollIntervalNumericUpDown.Name = "_pollIntervalNumericUpDown"
        Me._pollIntervalNumericUpDown.Size = New System.Drawing.Size(43, 20)
        Me._pollIntervalNumericUpDown.TabIndex = 3
        Me._pollIntervalNumericUpDown.Value = New Decimal(New Integer() {500, 0, 0, 0})
        '
        '_readManualRadioButton
        '
        Me._readManualRadioButton.AutoSize = True
        Me._readManualRadioButton.Checked = True
        Me._readManualRadioButton.Location = New System.Drawing.Point(12, 20)
        Me._readManualRadioButton.Name = "_readManualRadioButton"
        Me._readManualRadioButton.Size = New System.Drawing.Size(69, 18)
        Me._readManualRadioButton.TabIndex = 0
        Me._readManualRadioButton.TabStop = True
        Me._readManualRadioButton.Text = "MANUAL"
        Me._readManualRadioButton.UseVisualStyleBackColor = True
        '
        'commandsCombo
        '
        Me.commandsCombo.BackColor = System.Drawing.SystemColors.Window
        Me.commandsCombo.Cursor = System.Windows.Forms.Cursors.Default
        Me.commandsCombo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.commandsCombo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.commandsCombo.Items.AddRange(New Object() {"print( Me._VERSION )", "localnode.prompts = 1", "localnode.errorqueue.clear()", "localnode.reset()", "*RST", "*CLS", "*IDN?"})
        Me.commandsCombo.Location = New System.Drawing.Point(13, 70)
        Me.commandsCombo.Name = "commandsCombo"
        Me.commandsCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.commandsCombo.Size = New System.Drawing.Size(229, 24)
        Me.commandsCombo.TabIndex = 17
        Me.commandsCombo.Text = "Combo1"
        '
        'commandsComboLabel
        '
        Me.commandsComboLabel.BackColor = System.Drawing.Color.Transparent
        Me.commandsComboLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.commandsComboLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.commandsComboLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.commandsComboLabel.Location = New System.Drawing.Point(13, 54)
        Me.commandsComboLabel.Name = "commandsComboLabel"
        Me.commandsComboLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.commandsComboLabel.Size = New System.Drawing.Size(137, 13)
        Me.commandsComboLabel.TabIndex = 19
        Me.commandsComboLabel.Text = "SINGLE COMMANDS: "
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(397, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(149, 49)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Receiving the prompt after a print() or *IDN? causes error 420.  "
        '
        '_sendReceiveSplitContainer
        '
        Me._sendReceiveSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._sendReceiveSplitContainer.Location = New System.Drawing.Point(13, 7)
        Me._sendReceiveSplitContainer.Name = "_sendReceiveSplitContainer"
        '
        '_sendReceiveSplitContainer.Panel1
        '
        Me._sendReceiveSplitContainer.Panel1.Controls.Add(Me._sendGroupBox)
        '
        '_sendReceiveSplitContainer.Panel2
        '
        Me._sendReceiveSplitContainer.Panel2.Controls.Add(Me._receiveGroupBox)
        Me._sendReceiveSplitContainer.Size = New System.Drawing.Size(716, 289)
        Me._sendReceiveSplitContainer.SplitterDistance = 238
        Me._sendReceiveSplitContainer.TabIndex = 26
        '
        '_sendGroupBox
        '
        Me._sendGroupBox.Controls.Add(Me.inputTextBox)
        Me._sendGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._sendGroupBox.Location = New System.Drawing.Point(0, 0)
        Me._sendGroupBox.Name = "_sendGroupBox"
        Me._sendGroupBox.Size = New System.Drawing.Size(238, 289)
        Me._sendGroupBox.TabIndex = 27
        Me._sendGroupBox.TabStop = False
        Me._sendGroupBox.Text = "SENT TO INSTRUMENT"
        '
        'inputTextBox
        '
        Me.inputTextBox.AcceptsReturn = True
        Me.inputTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.inputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.inputTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.inputTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inputTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.inputTextBox.Location = New System.Drawing.Point(3, 16)
        Me.inputTextBox.MaxLength = 0
        Me.inputTextBox.Multiline = True
        Me.inputTextBox.Name = "inputTextBox"
        Me.inputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.inputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.inputTextBox.Size = New System.Drawing.Size(232, 270)
        Me.inputTextBox.TabIndex = 11
        '
        '_receiveGroupBox
        '
        Me._receiveGroupBox.Controls.Add(Me.outputTextBox)
        Me._receiveGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._receiveGroupBox.Location = New System.Drawing.Point(0, 0)
        Me._receiveGroupBox.Name = "_receiveGroupBox"
        Me._receiveGroupBox.Size = New System.Drawing.Size(474, 289)
        Me._receiveGroupBox.TabIndex = 0
        Me._receiveGroupBox.TabStop = False
        Me._receiveGroupBox.Text = "RECEIVED FROM INSTRUMENT: "
        '
        'outputTextBox
        '
        Me.outputTextBox.AcceptsReturn = True
        Me.outputTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.outputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.outputTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.outputTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.outputTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.outputTextBox.Location = New System.Drawing.Point(3, 16)
        Me.outputTextBox.MaxLength = 0
        Me.outputTextBox.Multiline = True
        Me.outputTextBox.Name = "outputTextBox"
        Me.outputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.outputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.outputTextBox.Size = New System.Drawing.Size(468, 270)
        Me.outputTextBox.TabIndex = 14
        '
        '_messagesTabPage
        '
        Me._messagesTabPage.Controls.Add(Me.messagesTextBox)
        Me._messagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._messagesTabPage.Name = "_messagesTabPage"
        Me._messagesTabPage.Size = New System.Drawing.Size(742, 413)
        Me._messagesTabPage.TabIndex = 2
        Me._messagesTabPage.Text = "MESSAGES"
        Me._messagesTabPage.UseVisualStyleBackColor = True
        '
        'messagesTextBox
        '
        Me.messagesTextBox.AcceptsReturn = True
        Me.messagesTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.messagesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.messagesTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.messagesTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.messagesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.messagesTextBox.Location = New System.Drawing.Point(0, 0)
        Me.messagesTextBox.MaxLength = 0
        Me.messagesTextBox.Multiline = True
        Me.messagesTextBox.Name = "messagesTextBox"
        Me.messagesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.messagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.messagesTextBox.Size = New System.Drawing.Size(742, 413)
        Me.messagesTextBox.TabIndex = 15
        '
        '_statusBar
        '
        Me._statusBar.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._statusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._statusStatusBarPanel, Me._statusRegisterLabel})
        Me._statusBar.Location = New System.Drawing.Point(0, 439)
        Me._statusBar.Name = "_statusBar"
        Me._statusBar.Size = New System.Drawing.Size(750, 25)
        Me._statusBar.TabIndex = 0
        Me._statusBar.Text = "Ready"
        '
        '_statusStatusBarPanel
        '
        Me._statusStatusBarPanel.AutoSize = False
        Me._statusStatusBarPanel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                                       System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                                   System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._statusStatusBarPanel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._statusStatusBarPanel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._statusStatusBarPanel.Margin = New System.Windows.Forms.Padding(0)
        Me._statusStatusBarPanel.Name = "_statusStatusBarPanel"
        Me._statusStatusBarPanel.Size = New System.Drawing.Size(690, 25)
        Me._statusStatusBarPanel.Spring = True
        Me._statusStatusBarPanel.Text = "Ready"
        Me._statusStatusBarPanel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_statusRegisterLabel
        '
        Me._statusRegisterLabel.Name = "_statusRegisterLabel"
        Me._statusRegisterLabel.Size = New System.Drawing.Size(45, 20)
        Me._statusRegisterLabel.Text = "0x00"
        Me._statusRegisterLabel.ToolTipText = "Status Register value"
        '
        'GpibTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(750, 464)
        Me.Controls.Add(Me.mainTabs)
        Me.Controls.Add(Me._statusBar)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(297, 150)
        Me.MaximizeBox = False
        Me.Name = "GpibTestPanel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Mini VISA Tester"
        Me.mainTabs.ResumeLayout(False)
        Me._connectTabPage.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me._settingsGroupBox.ResumeLayout(False)
        Me._settingsGroupBox.PerformLayout()
        Me._connectGroupBox.ResumeLayout(False)
        Me._connectGroupBox.PerformLayout()
        Me._GpibTabPage.ResumeLayout(False)
        Me._sendReceiveLayout.ResumeLayout(False)
        Me._sendReceiveControlPanel.ResumeLayout(False)
        Me._receiveOptionsGroupBox.ResumeLayout(False)
        Me._receiveOptionsGroupBox.PerformLayout()
        CType(Me._pollIntervalNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me._sendReceiveSplitContainer.Panel1.ResumeLayout(False)
        Me._sendReceiveSplitContainer.Panel2.ResumeLayout(False)
        Me._sendReceiveSplitContainer.ResumeLayout(False)
        Me._sendGroupBox.ResumeLayout(False)
        Me._sendGroupBox.PerformLayout()
        Me._receiveGroupBox.ResumeLayout(False)
        Me._receiveGroupBox.PerformLayout()
        Me._messagesTabPage.ResumeLayout(False)
        Me._messagesTabPage.PerformLayout()
        Me._statusBar.ResumeLayout(False)
        Me._statusBar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()
    End Sub
    Private WithEvents _receiveOptionsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _connectTabPage As System.Windows.Forms.TabPage
    Private WithEvents _settingsGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents DisconnectCommandsTextBox As System.Windows.Forms.TextBox
    Public WithEvents sendDisconnectCommandsCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _connectGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents _clearInterfaceButton As System.Windows.Forms.Button
    Public WithEvents _PrimaryAddressLabel As System.Windows.Forms.Label
    Public WithEvents _clearDeviceButton As System.Windows.Forms.Button
    Public WithEvents GpibAddressTextBox As System.Windows.Forms.TextBox
    Public WithEvents _connectToggle As System.Windows.Forms.CheckBox
    Private WithEvents _readManualRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _serviceRequestReceiveOptionRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _pollRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _statusRegisterLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _sendReceiveSplitContainer As System.Windows.Forms.SplitContainer
    Private WithEvents _sendGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents inputTextBox As System.Windows.Forms.TextBox
    Private WithEvents _receiveGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _sendReceiveLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _sendReceiveControlPanel As System.Windows.Forms.Panel
    Public WithEvents outputTextBox As System.Windows.Forms.TextBox
    Private WithEvents _pollIntervalUnitsLabel As System.Windows.Forms.Label
    Private WithEvents _pollIntervalNumericUpDown As System.Windows.Forms.NumericUpDown
#End Region

End Class