﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SimpleReadWriteTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ReadStatusPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="WriteStatusPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="StatusPanel")>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.clearButton = New System.Windows.Forms.Button
        Me.mainStatusBar = New System.Windows.Forms.StatusBar
        Me.statusPanel = New System.Windows.Forms.StatusBarPanel
        Me.readStatusPanel = New System.Windows.Forms.StatusBarPanel
        Me.writeStatusPanel = New System.Windows.Forms.StatusBarPanel
        Me.readText = New System.Windows.Forms.TextBox
        Me.writeText = New System.Windows.Forms.TextBox
        Me.stringToReadLabel = New System.Windows.Forms.Label
        Me.stringToWriteLabel = New System.Windows.Forms.Label
        Me.closeSessionButton = New System.Windows.Forms.Button
        Me.openSessionButton = New System.Windows.Forms.Button
        Me.tipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.readButton = New System.Windows.Forms.Button
        Me.writeButton = New System.Windows.Forms.Button
        Me.queryButton = New System.Windows.Forms.Button
        Me._timingTextBox = New System.Windows.Forms.TextBox
        Me._timingUnitsLabel = New System.Windows.Forms.Label
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.readStatusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.writeStatusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'clearButton
        '
        Me.clearButton.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.clearButton.Enabled = False
        Me.clearButton.Location = New System.Drawing.Point(0, 240)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(287, 24)
        Me.clearButton.TabIndex = 18
        Me.clearButton.Text = "Clear"
        '
        'mainStatusBar
        '
        Me.mainStatusBar.Location = New System.Drawing.Point(0, 264)
        Me.mainStatusBar.Name = "mainStatusBar"
        Me.mainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusPanel, Me.readStatusPanel, Me.writeStatusPanel})
        Me.mainStatusBar.ShowPanels = True
        Me.mainStatusBar.Size = New System.Drawing.Size(287, 22)
        Me.mainStatusBar.SizingGrip = False
        Me.mainStatusBar.TabIndex = 21
        Me.mainStatusBar.Text = "StatusBar1"
        '
        'statusPanel
        '
        Me.statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanel.Name = "StatusPanel"
        Me.statusPanel.Text = "<>"
        Me.statusPanel.ToolTipText = "Status"
        Me.statusPanel.Width = 210
        '
        'readStatusPanel
        '
        Me.readStatusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.readStatusPanel.Name = "ReadStatusPanel"
        Me.readStatusPanel.Text = "<>"
        Me.readStatusPanel.ToolTipText = "Read status byte"
        Me.readStatusPanel.Width = 27
        '
        'writeStatusPanel
        '
        Me.writeStatusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.writeStatusPanel.MinWidth = 50
        Me.writeStatusPanel.Name = "WriteStatusPanel"
        Me.writeStatusPanel.Text = "<>"
        Me.writeStatusPanel.ToolTipText = "Write status byte"
        Me.writeStatusPanel.Width = 50
        '
        'readText
        '
        Me.readText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                     System.Windows.Forms.AnchorStyles.Left) Or
                                 System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readText.Location = New System.Drawing.Point(5, 134)
        Me.readText.Multiline = True
        Me.readText.Name = "readText"
        Me.readText.ReadOnly = True
        Me.readText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.readText.Size = New System.Drawing.Size(275, 104)
        Me.readText.TabIndex = 17
        Me.readText.TabStop = False
        '
        'writeText
        '
        Me.writeText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                     System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeText.Enabled = False
        Me.writeText.Location = New System.Drawing.Point(5, 52)
        Me.writeText.Name = "writeText"
        Me.writeText.Size = New System.Drawing.Size(275, 20)
        Me.writeText.TabIndex = 13
        Me.writeText.Text = "*IDN?\n"
        '
        'stringToReadLabel
        '
        Me.stringToReadLabel.Location = New System.Drawing.Point(5, 115)
        Me.stringToReadLabel.Name = "stringToReadLabel"
        Me.stringToReadLabel.Size = New System.Drawing.Size(101, 15)
        Me.stringToReadLabel.TabIndex = 20
        Me.stringToReadLabel.Text = "Message Received:"
        '
        'stringToWriteLabel
        '
        Me.stringToWriteLabel.Location = New System.Drawing.Point(5, 32)
        Me.stringToWriteLabel.Name = "stringToWriteLabel"
        Me.stringToWriteLabel.Size = New System.Drawing.Size(107, 14)
        Me.stringToWriteLabel.TabIndex = 19
        Me.stringToWriteLabel.Text = "Message to Send:"
        '
        'closeSessionButton
        '
        Me.closeSessionButton.Enabled = False
        Me.closeSessionButton.Location = New System.Drawing.Point(97, 3)
        Me.closeSessionButton.Name = "closeSessionButton"
        Me.closeSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.closeSessionButton.TabIndex = 12
        Me.closeSessionButton.Text = "Close Session"
        '
        'openSessionButton
        '
        Me.openSessionButton.Location = New System.Drawing.Point(5, 3)
        Me.openSessionButton.Name = "openSessionButton"
        Me.openSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.openSessionButton.TabIndex = 11
        Me.openSessionButton.Text = "Open Session"
        '
        'readButton
        '
        Me.readButton.Enabled = False
        Me.readButton.Location = New System.Drawing.Point(153, 81)
        Me.readButton.Name = "readButton"
        Me.readButton.Size = New System.Drawing.Size(74, 23)
        Me.readButton.TabIndex = 16
        Me.readButton.Text = "Read"
        '
        'writeButton
        '
        Me.writeButton.Enabled = False
        Me.writeButton.Location = New System.Drawing.Point(79, 81)
        Me.writeButton.Name = "writeButton"
        Me.writeButton.Size = New System.Drawing.Size(74, 23)
        Me.writeButton.TabIndex = 15
        Me.writeButton.Text = "Write"
        '
        'queryButton
        '
        Me.queryButton.Enabled = False
        Me.queryButton.Location = New System.Drawing.Point(5, 81)
        Me.queryButton.Name = "queryButton"
        Me.queryButton.Size = New System.Drawing.Size(74, 23)
        Me.queryButton.TabIndex = 14
        Me.queryButton.Text = "Query"
        '
        '_timingTextBox
        '
        Me._timingTextBox.Location = New System.Drawing.Point(192, 112)
        Me._timingTextBox.Name = "_timingTextBox"
        Me._timingTextBox.Size = New System.Drawing.Size(63, 20)
        Me._timingTextBox.TabIndex = 22
        '
        '_timingUnitsLabel
        '
        Me._timingUnitsLabel.AutoSize = True
        Me._timingUnitsLabel.Location = New System.Drawing.Point(259, 116)
        Me._timingUnitsLabel.Name = "_timingUnitsLabel"
        Me._timingUnitsLabel.Size = New System.Drawing.Size(20, 13)
        Me._timingUnitsLabel.TabIndex = 23
        Me._timingUnitsLabel.Text = "ms"
        '
        'SimpleReadWriteTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(287, 286)
        Me.Controls.Add(Me._timingUnitsLabel)
        Me.Controls.Add(Me._timingTextBox)
        Me.Controls.Add(Me.clearButton)
        Me.Controls.Add(Me.mainStatusBar)
        Me.Controls.Add(Me.readText)
        Me.Controls.Add(Me.writeText)
        Me.Controls.Add(Me.stringToReadLabel)
        Me.Controls.Add(Me.stringToWriteLabel)
        Me.Controls.Add(Me.closeSessionButton)
        Me.Controls.Add(Me.openSessionButton)
        Me.Controls.Add(Me.readButton)
        Me.Controls.Add(Me.writeButton)
        Me.Controls.Add(Me.queryButton)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(295, 316)
        Me.Name = "SimpleReadWriteTestPanel"
        Me.Text = "Simple Read/Write"
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.readStatusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.writeStatusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()
    End Sub
    Private WithEvents clearButton As System.Windows.Forms.Button
    Private WithEvents mainStatusBar As System.Windows.Forms.StatusBar
    Private WithEvents statusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents readStatusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents writeStatusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents readText As System.Windows.Forms.TextBox
    Private WithEvents writeText As System.Windows.Forms.TextBox
    Private WithEvents stringToReadLabel As System.Windows.Forms.Label
    Private WithEvents stringToWriteLabel As System.Windows.Forms.Label
    Private WithEvents closeSessionButton As System.Windows.Forms.Button
    Private WithEvents openSessionButton As System.Windows.Forms.Button
    Private WithEvents tipsToolTip As System.Windows.Forms.ToolTip
    Private WithEvents readButton As System.Windows.Forms.Button
    Private WithEvents writeButton As System.Windows.Forms.Button
    Private WithEvents queryButton As System.Windows.Forms.Button
    Private WithEvents _timingTextBox As System.Windows.Forms.TextBox
    Private WithEvents _timingUnitsLabel As System.Windows.Forms.Label
End Class
