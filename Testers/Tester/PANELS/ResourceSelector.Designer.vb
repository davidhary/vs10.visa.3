﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ResourceSelector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ResourceStringLabel = New System.Windows.Forms.Label
        Me.AvailableResourcesLabel = New System.Windows.Forms.Label
        Me.visaResourceName = New System.Windows.Forms.TextBox
        Me.cancel = New System.Windows.Forms.Button
        Me.ok = New System.Windows.Forms.Button
        Me.availableResources = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        '
        'ResourceStringLabel
        '
        Me.ResourceStringLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ResourceStringLabel.Location = New System.Drawing.Point(5, 143)
        Me.ResourceStringLabel.Name = "ResourceStringLabel"
        Me.ResourceStringLabel.Size = New System.Drawing.Size(279, 13)
        Me.ResourceStringLabel.TabIndex = 12
        Me.ResourceStringLabel.Text = "Resource String:"
        '
        'AvailableResourcesLabel
        '
        Me.AvailableResourcesLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                                   System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AvailableResourcesLabel.Location = New System.Drawing.Point(5, 7)
        Me.AvailableResourcesLabel.Name = "AvailableResourcesLabel"
        Me.AvailableResourcesLabel.Size = New System.Drawing.Size(279, 12)
        Me.AvailableResourcesLabel.TabIndex = 11
        Me.AvailableResourcesLabel.Text = "Available Resources:"
        '
        'visaResourceName
        '
        Me.visaResourceName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                            System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.visaResourceName.Location = New System.Drawing.Point(5, 159)
        Me.visaResourceName.Name = "visaResourceName"
        Me.visaResourceName.Size = New System.Drawing.Size(282, 20)
        Me.visaResourceName.TabIndex = 10
        Me.visaResourceName.Text = "GPIB0::2::INSTR"
        '
        'cancel
        '
        Me.cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancel.Location = New System.Drawing.Point(82, 189)
        Me.cancel.Name = "cancel"
        Me.cancel.Size = New System.Drawing.Size(77, 25)
        Me.cancel.TabIndex = 9
        Me.cancel.Text = "Cancel"
        '
        'ok
        '
        Me.ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ok.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ok.Location = New System.Drawing.Point(5, 189)
        Me.ok.Name = "ok"
        Me.ok.Size = New System.Drawing.Size(77, 25)
        Me.ok.TabIndex = 8
        Me.ok.Text = "OK"
        '
        'availableResources
        '
        Me.availableResources.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                               System.Windows.Forms.AnchorStyles.Left) Or
                                           System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.availableResources.Location = New System.Drawing.Point(5, 20)
        Me.availableResources.Name = "availableResources"
        Me.availableResources.Size = New System.Drawing.Size(282, 108)
        Me.availableResources.TabIndex = 7
        '
        'ResourceSelector
        '
        Me.AcceptButton = Me.ok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cancel
        Me.ClientSize = New System.Drawing.Size(292, 220)
        Me.Controls.Add(Me.ResourceStringLabel)
        Me.Controls.Add(Me.AvailableResourcesLabel)
        Me.Controls.Add(Me.visaResourceName)
        Me.Controls.Add(Me.cancel)
        Me.Controls.Add(Me.ok)
        Me.Controls.Add(Me.availableResources)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(177, 247)
        Me.Name = "ResourceSelector"
        Me.Text = "Select Resource"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents ResourceStringLabel As System.Windows.Forms.Label
    Private WithEvents AvailableResourcesLabel As System.Windows.Forms.Label
    Private WithEvents visaResourceName As System.Windows.Forms.TextBox
    Private WithEvents cancel As System.Windows.Forms.Button
    Private WithEvents ok As System.Windows.Forms.Button
    Private WithEvents availableResources As System.Windows.Forms.ListBox
End Class
