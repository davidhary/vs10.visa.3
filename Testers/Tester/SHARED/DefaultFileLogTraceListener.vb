﻿Imports Microsoft.VisualBasic.Logging
Imports System.Globalization
Imports System.Security.Permissions
''' <summary>
''' Replaces the standard event log.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class DefaultFileLogTraceListener

    Inherits Microsoft.VisualBasic.Logging.FileLogTraceListener

    ''' <summary>
    ''' Gets the default listener name.
    ''' </summary>
    Public Const DefaultTraceListenerName As String = "FileLog"

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Diagnostics.TraceListener"></see> class using the specified name as the listener.
    ''' Defaults to using the current user application data folder.
    ''' </summary>
    ''' <param name="designMode">True if running in design mode</param>
    Public Sub New(ByVal designMode As Boolean)
        Me.New(DefaultFileLogTraceListener.DefaultTraceListenerName,
               DefaultFileLogTraceListener.DefaultTraceListenerFolderPath(designMode, False))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Diagnostics.TraceListener"></see> class using the specified name as the listener.
    ''' </summary>
    ''' <param name="designMode">True if running in design mode</param>
    ''' <param name="allUsers">True if using all users application data folder; false for using the current user application data folder.</param>
    Public Sub New(ByVal designMode As Boolean, ByVal allUsers As Boolean)
        Me.New(DefaultFileLogTraceListener.DefaultTraceListenerName,
               DefaultFileLogTraceListener.DefaultTraceListenerFolderPath(designMode, allUsers))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Diagnostics.TraceListener"></see> class using the specified name as the listener.
    ''' </summary>
    ''' <param name="name">The name of the <see cref="T:System.Diagnostics.TraceListener"></see>.</param>
    ''' <param name="folderName">Name of the folder.</param>
    Public Sub New(ByVal name As String, ByVal folderName As String)
        MyBase.New(name)

        Me._timeFormat = "HH:mm:ss:fff"

        ' set the default properties of the log file name 
        Me.LogFileCreationSchedule = LogFileCreationScheduleOption.Daily
        Me.Location = LogFileLocation.Custom
        Me.Delimiter = ", "
        Me.CustomLocation = folderName
        Me.IncludeHostName = False
        Me.AutoFlush = True
        Me.TraceOutputOptions = TraceOptions.DateTime Or TraceOptions.LogicalOperationStack

        ' note that the trace event cache time format is not set correctly to -8 hours offset.
        ' Rather, it gives us a -4 hours offset.

    End Sub

    Private _usingUniversalTime As Boolean
    ''' <summary>
    ''' Gets or sets the condition for using universal time.  When using universal time, the
    ''' format is automatically modified to add a 'T' suffix if not set.
    ''' Defaults to True.
    ''' </summary>
    ''' <value><c>True</c> if [using universal time]; otherwise, <c>False</c>.</value>
    Public Property UsingUniversalTime() As Boolean
        Get
            Return Me._usingUniversalTime
        End Get
        Set(ByVal value As Boolean)
            Me._usingUniversalTime = value
            If value AndAlso Not Me._timeFormat.EndsWith("T", StringComparison.OrdinalIgnoreCase) Then
                Me.TimeFormat &= "T"
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the time format.
    ''' </summary>
    ''' <value>The time format.</value>
    Public Property TimeFormat() As String

    ''' <summary>
    ''' Writes trace information, a message and event information to the output file or stream.
    ''' </summary>
    ''' <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that contains the current process ID, thread ID, and stack trace information.</param>
    ''' <param name="source">A name of the trace source that invoked this method.</param>
    ''' <param name="eventType">One of the <see cref="T:System.Diagnostics.TraceEventType"></see> enumeration values.</param>
    ''' <param name="id">A numeric identifier for the event.</param>
    ''' <param name="message">A message to write.</param>
    ''' <filterPriority>1</filterPriority>
    '''   <history>
    ''' Use higher resolution time.
    ''' Remote Date assuming using one file per day.
    ''' Move fixed information up front.
    ''' Include source with host.
    '''   </history>
    '''   <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" /></PermissionSet>
    <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)>
    Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType,
                                    ByVal id As Integer, ByVal message As String)

        If eventCache Is Nothing OrElse message Is Nothing OrElse String.IsNullOrWhiteSpace(message) Then Return

        If (Me.Filter Is Nothing OrElse
            Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing)) Then

            If message.Contains(Environment.NewLine) Then
                For Each line As String In message.Split(Environment.NewLine.ToCharArray)
                    TraceEvent(eventCache, source, eventType, id, line.Trim)
                Next
                Return
            End If
            Dim builder As New System.Text.StringBuilder

            builder.Append(eventType.ToString.Substring(0, 2) & Me.Delimiter)

            builder.Append((id.ToString(CultureInfo.InvariantCulture) & Me.Delimiter))

            ' builder.Append(([Enum].GetName(GetType(TraceEventType), eventType) & Me.Delimiter))
            If Me.IncludeHostName Then
                builder.Append(Me.HostName & "." & source & Me.Delimiter)
            End If

            If ((Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime) Then
                Dim logTime As DateTime = eventCache.DateTime
                If Not Me._usingUniversalTime Then
                    logTime = logTime.Add(DateTimeOffset.Now.Offset)
                End If
                builder.Append(logTime.ToString(Me._timeFormat, CultureInfo.InvariantCulture) & Me.Delimiter)
                ' builder.Append((Me.Delimiter & eventCache.DateTime.ToString("u", CultureInfo.InvariantCulture)))
            End If

            If ((Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId) Then
                builder.Append(eventCache.ProcessId.ToString(CultureInfo.InvariantCulture) & Me.Delimiter)
            End If
            If ((Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId) Then
                builder.Append(eventCache.ThreadId & Me.Delimiter)
            End If
            If ((Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp) Then
                builder.Append(eventCache.Timestamp.ToString(CultureInfo.InvariantCulture) & Me.Delimiter)
            End If

            builder.Append(message)

            If ((Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack) Then
                builder.Append((Me.Delimiter & StackToString(eventCache.LogicalOperationStack)))
            End If

            If ((Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack) Then
                builder.Append((Me.Delimiter & eventCache.Callstack))
            End If

            Me.WriteLine(builder.ToString)

        End If
    End Sub

    Private _HostName As String
    ''' <summary>
    ''' Gets the name of the host.
    ''' </summary>
    ''' <value>The name of the host.</value>
    Private ReadOnly Property HostName() As String
        Get
            If (Me._HostName = "") Then
                Me._HostName = Environment.MachineName
            End If
            Return Me._HostName
        End Get
    End Property

    ''' <summary>
    ''' Stacks to string.
    ''' </summary>
    ''' <param name="stack">The stack.</param>
    ''' <returns>System.String.</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Private Shared Function StackToString(ByVal stack As Stack) As String
        Dim enumerator As IEnumerator = Nothing
        Dim length As Integer = ", ".Length
        Dim builder As New System.Text.StringBuilder
        Try
            enumerator = stack.GetEnumerator
            Do While enumerator.MoveNext
                builder.Append((enumerator.Current.ToString & ", "))
            Loop
        Finally
            If TypeOf enumerator Is IDisposable Then
                TryCast(enumerator, IDisposable).Dispose()
            End If
        End Try
        builder.Replace("""", """""")
        If (builder.Length >= length) Then
            builder.Remove((builder.Length - length), length)
        End If
        Return ("""" & builder.ToString & """")
    End Function

    ''' <summary>
    ''' Returns the default folder name for tracing.
    ''' </summary>
    ''' <param name="designMode">if set to <c>True</c> [design mode].</param>
    ''' <param name="allUsers">if set to <c>True</c> uses the program application data folder for all users;
    ''' otherwise, uses the current user application data folder.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function DefaultTraceListenerFolderPath(ByVal designMode As Boolean, ByVal allUsers As Boolean) As String
        Const defaultFolderTitle As String = "logs"
        Dim path As String = ""
        If designMode OrElse (Environment.OSVersion.Version.Major <= 5) Then
            path = My.Application.Info.DirectoryPath
        ElseIf allUsers Then
            path = My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData
        Else
            path = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData
        End If
        If designMode Then
            Return System.IO.Path.Combine(path, "..\" & defaultFolderTitle)
        Else
            Return System.IO.Path.Combine(path, defaultFolderTitle)
        End If

    End Function

#Region " FILE SIZE "

    ''' <summary>
    ''' Returns the file size, -2 if path not specified or -1 if file not found.
    ''' </summary>
    ''' <param name="path">The path.</param>
    ''' <returns>System.Int64.</returns>
    Public Shared Function FileSize(ByVal path As String) As Long
        If String.IsNullOrWhiteSpace(path) Then Return 0
        Dim info As System.IO.FileInfo = New System.IO.FileInfo(path)
        Return FileSize(info)
    End Function

    ''' <summary>
    ''' Returns the file size, -2 if path not specified or -1 if file not found.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>System.Int64.</returns>
    Public Shared Function FileSize(ByVal value As System.IO.FileInfo) As Long

        If value Is Nothing Then
            Return -2
        ElseIf value.Exists Then
            Return value.Length
        ElseIf String.IsNullOrWhiteSpace(value.Name) Then
            Return -2
        Else
            Return -1
        End If

    End Function

    ''' <summary>
    ''' Returns true if log file exists; otherwise, false.
    ''' </summary>
    ''' <returns><c>True</c> if okay, <c>False</c> otherwise</returns>
    Public Shared Function LogFileExists() As Boolean
        Return FileSize(My.Application.Log.DefaultFileLogWriter.FullLogFileName) > 2
    End Function

#End Region

End Class

#Region " ORIGINAL WRITE "

#If False Then
  ''' <summary>
  ''' Writes trace information, a message and event information to the output file or stream.
  ''' </summary>
  ''' <param name="source">A name of the trace source that invoked this method. </param>
  ''' <param name="message">A message to write.</param>
  ''' <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that contains the current process ID, thread ID, and stack trace information.</param>
  ''' <param name="eventType">One of the <see cref="T:System.Diagnostics.TraceEventType"></see> enumeration values.</param>
  ''' <param name="id">A numeric identifier for the event.</param>
  ''' <filterPriority>1</filterPriority>
  ''' <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" /></PermissionSet>
  <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)> 
  Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal message As String)
    If ((Me.Filter Is Nothing) OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing)) Then
      Dim builder As New StringBuilder
      builder.Append((source & Me.Delimiter))
      builder.Append(([Enum].GetName(GetType(TraceEventType), eventType) & Me.Delimiter))
      builder.Append((id.ToString(CultureInfo.InvariantCulture) & Me.Delimiter))
      builder.Append(message)
      If ((Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack) Then
        builder.Append((Me.Delimiter & eventCache.Callstack))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack) Then
        builder.Append((Me.Delimiter & StackToString(eventCache.LogicalOperationStack)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime) Then
        builder.Append((Me.Delimiter & eventCache.DateTime.ToString("o", CultureInfo.InvariantCulture)))
        ' builder.Append((Me.Delimiter & eventCache.DateTime.ToString("u", CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId) Then
        builder.Append((Me.Delimiter & eventCache.ProcessId.ToString(CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId) Then
        builder.Append((Me.Delimiter & eventCache.ThreadId))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp) Then
        builder.Append((Me.Delimiter & eventCache.Timestamp.ToString(CultureInfo.InvariantCulture)))
      End If
      If Me.IncludeHostName Then
        builder.Append((Me.Delimiter & Me.HostName))
      End If
      Me.WriteLine(builder.ToString)
    End If
  End Sub
#End If
#End Region

