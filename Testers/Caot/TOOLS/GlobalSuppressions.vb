Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"
' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.Visa.Testers", 
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 

#End Region

<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Visa.Testers.SimpleReadWriteTestPanel.#write_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Visa.Testers.SimpleReadWriteTestPanel.#query_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Visa.Testers.SimpleReadWriteTestPanel.#read_Click(System.Object,System.EventArgs)")> 

