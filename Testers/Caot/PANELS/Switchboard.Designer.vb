<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._applicationsListBoxLabel = New System.Windows.Forms.Label
        Me._openButton = New System.Windows.Forms.Button
        Me._exitButton = New System.Windows.Forms.Button
        Me._applicationsListBox = New System.Windows.Forms.ListBox
        Me._tooltip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'applicationsListBoxLabel
        '
        Me._applicationsListBoxLabel.Location = New System.Drawing.Point(13, 9)
        Me._applicationsListBoxLabel.Name = "applicationsListBoxLabel"
        Me._applicationsListBoxLabel.Size = New System.Drawing.Size(296, 16)
        Me._applicationsListBoxLabel.TabIndex = 15
        Me._applicationsListBoxLabel.Text = "Select an item from the list and click Open"
        '
        'openButton
        '
        Me._openButton.Location = New System.Drawing.Point(355, 29)
        Me._openButton.Name = "openButton"
        Me._openButton.Size = New System.Drawing.Size(75, 23)
        Me._openButton.TabIndex = 13
        Me._openButton.Text = "&Open..."
        '
        'exitButton
        '
        Me._exitButton.Location = New System.Drawing.Point(355, 126)
        Me._exitButton.Name = "exitButton"
        Me._exitButton.Size = New System.Drawing.Size(75, 23)
        Me._exitButton.TabIndex = 12
        Me._exitButton.Text = "E&xit"
        '
        'applicationsListBox
        '
        Me._applicationsListBox.Location = New System.Drawing.Point(11, 29)
        Me._applicationsListBox.Name = "applicationsListBox"
        Me._applicationsListBox.Size = New System.Drawing.Size(328, 121)
        Me._applicationsListBox.TabIndex = 14
        Me._tooltip.SetToolTip(Me._applicationsListBox, "Select an application to test")
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 166)
        Me.Controls.Add(Me._applicationsListBoxLabel)
        Me.Controls.Add(Me._openButton)
        Me.Controls.Add(Me._exitButton)
        Me.Controls.Add(Me._applicationsListBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Switchboard"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _applicationsListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _openButton As System.Windows.Forms.Button
    Private WithEvents _exitButton As System.Windows.Forms.Button
    Private WithEvents _applicationsListBox As System.Windows.Forms.ListBox
    Private WithEvents _tooltip As System.Windows.Forms.ToolTip
End Class
