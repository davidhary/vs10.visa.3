﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class CaotTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CaotTestPanel))
        Me._k7000Panel = New isr.Visa.Instruments.K7000.Panel
        Me._tabControl = New System.Windows.Forms.TabControl
        Me._GpibInterfaceTabPage = New System.Windows.Forms.TabPage
        Me._GpibInterfacePanel = New isr.Controls.InterfacePanel
        Me._k2400TabPage = New System.Windows.Forms.TabPage
        Me._k2400Panel = New isr.Visa.Instruments.K2400.Panel
        Me._k7000TabPage = New System.Windows.Forms.TabPage
        Me._k2700TabPage = New System.Windows.Forms.TabPage
        Me._k2700Panel = New isr.Visa.Instruments.K2700.Panel
        Me._k6220TabPage = New System.Windows.Forms.TabPage
        Me._k6220Panel = New isr.Visa.Instruments.K6220.Panel
        Me._bhk500TabPage = New System.Windows.Forms.TabPage
        Me._bhk500Panel = New isr.Visa.Instruments.K500.Panel
        Me._statusBar = New System.Windows.Forms.StatusBar
        Me._statusPanel = New System.Windows.Forms.StatusBarPanel
        Me._tabControl.SuspendLayout()
        Me._GpibInterfaceTabPage.SuspendLayout()
        Me._k2400TabPage.SuspendLayout()
        Me._k7000TabPage.SuspendLayout()
        Me._k2700TabPage.SuspendLayout()
        Me._k6220TabPage.SuspendLayout()
        Me._bhk500TabPage.SuspendLayout()
        CType(Me._statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_k7000Panel
        '
        Me._k7000Panel.BackColor = System.Drawing.Color.Transparent
        Me._k7000Panel.Location = New System.Drawing.Point(48, 34)
        Me._k7000Panel.Name = "_k7000Panel"
        Me._k7000Panel.Size = New System.Drawing.Size(312, 288)
        Me._k7000Panel.TabIndex = 0
        '
        '_tabControl
        '
        Me._tabControl.Controls.Add(Me._GpibInterfaceTabPage)
        Me._tabControl.Controls.Add(Me._k2400TabPage)
        Me._tabControl.Controls.Add(Me._k7000TabPage)
        Me._tabControl.Controls.Add(Me._k2700TabPage)
        Me._tabControl.Controls.Add(Me._k6220TabPage)
        Me._tabControl.Controls.Add(Me._bhk500TabPage)
        Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._tabControl.Location = New System.Drawing.Point(0, 0)
        Me._tabControl.Name = "_tabControl"
        Me._tabControl.SelectedIndex = 0
        Me._tabControl.Size = New System.Drawing.Size(416, 382)
        Me._tabControl.TabIndex = 2
        '
        '_GpibInterfaceTabPage
        '
        Me._GpibInterfaceTabPage.Controls.Add(Me._GpibInterfacePanel)
        Me._GpibInterfaceTabPage.Location = New System.Drawing.Point(4, 22)
        Me._GpibInterfaceTabPage.Name = "_GpibInterfaceTabPage"
        Me._GpibInterfaceTabPage.Size = New System.Drawing.Size(408, 356)
        Me._GpibInterfaceTabPage.TabIndex = 2
        Me._GpibInterfaceTabPage.Text = "GPIB Interface"
        Me._GpibInterfaceTabPage.UseVisualStyleBackColor = True
        '
        '_GpibInterfacePanel
        '
        Me._GpibInterfacePanel.BackColor = System.Drawing.Color.Transparent
        Me._GpibInterfacePanel.Location = New System.Drawing.Point(48, 32)
        Me._GpibInterfacePanel.Name = "_GpibInterfacePanel"
        Me._GpibInterfacePanel.Size = New System.Drawing.Size(312, 272)
        Me._GpibInterfacePanel.TabIndex = 0
        '
        '_k2400TabPage
        '
        Me._k2400TabPage.Controls.Add(Me._k2400Panel)
        Me._k2400TabPage.Location = New System.Drawing.Point(4, 22)
        Me._k2400TabPage.Name = "_k2400TabPage"
        Me._k2400TabPage.Size = New System.Drawing.Size(408, 356)
        Me._k2400TabPage.TabIndex = 0
        Me._k2400TabPage.Text = "2400"
        Me._k2400TabPage.UseVisualStyleBackColor = True
        '
        '_k2400Panel
        '
        Me._k2400Panel.BackColor = System.Drawing.Color.Transparent
        Me._k2400Panel.Location = New System.Drawing.Point(48, 34)
        Me._k2400Panel.Name = "_k2400Panel"
        Me._k2400Panel.Size = New System.Drawing.Size(312, 288)
        Me._k2400Panel.TabIndex = 0
        '
        '_k7000TabPage
        '
        Me._k7000TabPage.BackColor = System.Drawing.Color.Transparent
        Me._k7000TabPage.Controls.Add(Me._k7000Panel)
        Me._k7000TabPage.Location = New System.Drawing.Point(4, 22)
        Me._k7000TabPage.Name = "_k7000TabPage"
        Me._k7000TabPage.Size = New System.Drawing.Size(408, 356)
        Me._k7000TabPage.TabIndex = 1
        Me._k7000TabPage.Text = "7000"
        Me._k7000TabPage.UseVisualStyleBackColor = True
        '
        '_k2700TabPage
        '
        Me._k2700TabPage.Controls.Add(Me._k2700Panel)
        Me._k2700TabPage.Location = New System.Drawing.Point(4, 22)
        Me._k2700TabPage.Name = "_k2700TabPage"
        Me._k2700TabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._k2700TabPage.Size = New System.Drawing.Size(408, 356)
        Me._k2700TabPage.TabIndex = 3
        Me._k2700TabPage.Text = "2700"
        Me._k2700TabPage.UseVisualStyleBackColor = True
        '
        '_k2700Panel
        '
        Me._k2700Panel.BackColor = System.Drawing.Color.Transparent
        Me._k2700Panel.ClosedChannels = ""
        Me._k2700Panel.Location = New System.Drawing.Point(48, 34)
        Me._k2700Panel.Name = "_k2700Panel"
        Me._k2700Panel.Size = New System.Drawing.Size(312, 288)
        Me._k2700Panel.TabIndex = 0
        '
        '_k6220TabPage
        '
        Me._k6220TabPage.Controls.Add(Me._k6220Panel)
        Me._k6220TabPage.Location = New System.Drawing.Point(4, 22)
        Me._k6220TabPage.Name = "_k6220TabPage"
        Me._k6220TabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._k6220TabPage.Size = New System.Drawing.Size(408, 356)
        Me._k6220TabPage.TabIndex = 4
        Me._k6220TabPage.Text = "6221/2182"
        Me._k6220TabPage.UseVisualStyleBackColor = True
        '
        '_k6220Panel
        '
        Me._k6220Panel.BackColor = System.Drawing.Color.Transparent
        Me._k6220Panel.Location = New System.Drawing.Point(48, 34)
        Me._k6220Panel.Name = "_k6220Panel"
        Me._k6220Panel.Size = New System.Drawing.Size(317, 292)
        Me._k6220Panel.TabIndex = 0
        '
        '_bhk500TabPage
        '
        Me._bhk500TabPage.Controls.Add(Me._bhk500Panel)
        Me._bhk500TabPage.Location = New System.Drawing.Point(4, 22)
        Me._bhk500TabPage.Name = "_bhk500TabPage"
        Me._bhk500TabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._bhk500TabPage.Size = New System.Drawing.Size(408, 356)
        Me._bhk500TabPage.TabIndex = 5
        Me._bhk500TabPage.Text = "BKH500"
        Me._bhk500TabPage.UseVisualStyleBackColor = True
        '
        '_bhk500Panel
        '
        Me._bhk500Panel.BackColor = System.Drawing.Color.Transparent
        Me._bhk500Panel.Location = New System.Drawing.Point(45, 32)
        Me._bhk500Panel.Name = "_bhk500Panel"
        Me._bhk500Panel.Size = New System.Drawing.Size(312, 288)
        Me._bhk500Panel.TabIndex = 0
        '
        '_statusBar
        '
        Me._statusBar.Location = New System.Drawing.Point(0, 382)
        Me._statusBar.Name = "_statusBar"
        Me._statusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me._statusPanel})
        Me._statusBar.ShowPanels = True
        Me._statusBar.Size = New System.Drawing.Size(416, 24)
        Me._statusBar.TabIndex = 3
        Me._statusBar.Text = "StatusBar1"
        '
        '_statusPanel
        '
        Me._statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me._statusPanel.Name = "_StatusPanel"
        Me._statusPanel.Text = "Ready"
        Me._statusPanel.ToolTipText = "Display the current interface status"
        Me._statusPanel.Width = 399
        '
        'CaotTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(416, 406)
        Me.Controls.Add(Me._tabControl)
        Me.Controls.Add(Me._statusBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = False
        Me.Name = "CaotTestPanel"
        Me.Text = "Test Panel for CAOT Instruments"
        Me._tabControl.ResumeLayout(False)
        Me._GpibInterfaceTabPage.ResumeLayout(False)
        Me._k2400TabPage.ResumeLayout(False)
        Me._k7000TabPage.ResumeLayout(False)
        Me._k2700TabPage.ResumeLayout(False)
        Me._k6220TabPage.ResumeLayout(False)
        Me._bhk500TabPage.ResumeLayout(False)
        CType(Me._statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _k7000Panel As isr.Visa.Instruments.K7000.Panel
    Private WithEvents _tabControl As System.Windows.Forms.TabControl
    Private WithEvents _GpibInterfaceTabPage As System.Windows.Forms.TabPage
    Private WithEvents _GpibInterfacePanel As isr.Controls.InterfacePanel
    Private WithEvents _k2400TabPage As System.Windows.Forms.TabPage
    Private WithEvents _k2400Panel As isr.Visa.Instruments.K2400.Panel
    Private WithEvents _k7000TabPage As System.Windows.Forms.TabPage
    Private WithEvents _statusBar As System.Windows.Forms.StatusBar
    Private WithEvents _statusPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents _k2700TabPage As System.Windows.Forms.TabPage
    Private WithEvents _k6220TabPage As System.Windows.Forms.TabPage
    Private WithEvents _bhk500TabPage As System.Windows.Forms.TabPage
    Private WithEvents _k2700Panel As isr.Visa.Instruments.K2700.Panel
    Private WithEvents _k6220Panel As isr.Visa.Instruments.K6220.Panel
    Private WithEvents _bhk500Panel As isr.Visa.Instruments.K500.Panel
End Class
